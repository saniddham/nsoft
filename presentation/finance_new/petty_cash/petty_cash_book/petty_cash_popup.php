<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/finance/petty_cash/petty_cash_book/cls_petty_cash_get.php";
include_once "class/finance/petty_cash/petty_cash_item/cls_petty_cash_item_get.php";
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {
$obj_petty_cash_get				= new cls_petty_cash_get($db);
$obj_petty_cash_item_get		= new cls_petty_cash_item_get($db);

$pcInvMenuId					= 850;
//END 	- CREATE OBJECTS {
	
//BEGIN - CREATE PARAMETERS {
$location	= $_SESSION['CompanyID'];
$id			= $_REQUEST['Id'];
$date		= $_REQUEST['Date'];
//END	- CREATE PARAMETERS }

$itemArray		= $obj_petty_cash_item_get->getCategoryAndItemName($id);
$result			= $obj_petty_cash_get->getHeaderPopupDetails($id,$date,$_SESSION["HRDatabase"]);
?>
<head>
<title>Petty Cash Log</title>

<link rel="stylesheet" href="libraries/litebox/css/lightbox.css" type="text/css" media="screen" />

<script src="libraries/litebox/js/prototype.js" type="text/javascript"></script>
<script src="libraries/litebox/js/scriptaculous.js?load=effects" type="text/javascript"></script>
<script src="libraries/litebox/js/lightbox.js" type="text/javascript"></script>
</head>
<body>
<form id="frmMain" name="frmMain">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td class="reportHeader" align="center">PETTY CASH LOG</td>
    </tr>
    <tr>
      <td class="reportHeader" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td><table width="544" border="0" cellspacing="0" cellpadding="0" align="center" class="rptBordered">
          <thead>
            <tr>
              <th width="180">Category Name</th>
              <th width="182">Item Name</th>
              <th width="182">Date</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td align="center"><?php echo $itemArray["CATEGORY_NAME"]?></td>
              <td align="center"><?php echo $itemArray["ITEM_NAME"]?></td>
              <td align="center"><?php echo $date?></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="804" border="0" cellspacing="0" cellpadding="0" align="center" class="rptBordered">
          <thead>
            <tr>
              <th width="110">Serial No</th>
              <th width="258">Remarks</th>
              <th width="114">Amount</th>
              <th width="318">Requested By</th>
              <th width="508">Attached Document</th>
            </tr>
          </thead>
          <tbody>
          <?php 
		  while($row = mysqli_fetch_array($result))
		  {?>
            <tr>
              <td><a href="?q=<?php echo $pcInvMenuId; ?>&invoiceNo=<?php echo $row["SERIAL_NO"]?>&invoiceYear=<?php echo $row["SERIAL_YEAR"]?>" target="rpt_petty_cash_invoice.php"><?php echo $row["CONCAT_SERIAL"]?></a></td>
              <td><?php echo $row["REMARKS"]?></td>
              <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="text-align:center"><a href="<?php echo '/'.$_SESSION["HRDatabase"].'/'.$row['PHOTO_URL']?>" rel="lightbox" title="<?php echo $row["EMPLOYEE_NAME"]?>"><img src="<?php echo '/'.$_SESSION["HRDatabase"].'/'.$row['PHOTO_URL']?>" height="80" width="80"/></a></td>
                </tr>
                <tr>
                  <td style="text-align:center"><?php echo $row["EMPLOYEE_NAME"]?></td>
                </tr>
              </table></td>
              <td valign="top"><?php 
			  	$folderName 		= 	$row["SERIAL_NO"]."-".$row["SERIAL_YEAR"];
				filesInDir("documents/finance/petty_cash/$folderName");?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
</body>
</html>
<?php


function filesInDir($tdir)
{
	global $folderName;
	$m=0;
	
	echo "<table width=\"100%\">";
	
	
	$dirs = scandir($tdir);
	foreach($dirs as $file)
	{
		
		if (($file == '.')||($file == '..'))
		{
		}
		else
		{
			
			if (! is_dir($tdir.'/'.$file))
			{
				
			echo "	<tr class=\"normalfnt\">
						<td width=\"1\" align=\"center\">".++$m.". </td>
						<td width=\"250\" align=\"left\">$file</td>
						<td width=\"1\" align=\"center\"><a class=\"button green small\" target=\"_blank\" href=\"documents/finance/petty_cash/".$folderName."/$file\">view</a></td>
					 </tr>";
			}
		}
	}
		echo "	<tr><td colspan='3'></td></tr></table>";

}


?>
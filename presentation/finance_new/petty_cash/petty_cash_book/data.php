<?php
session_start();
ini_set('max_execution_time',60000);
ini_set('display_errors',0);

#BEGIN - INCLUDE FILES {
include "../../../../dataAccess/DBManager2.php";											$db								= new DBManager2();
include_once "../../../../class/cls_commonFunctions_get.php";								$obj_commonFunctions_get		= new cls_commonFunctions_get($db);
include_once "../../../../class/finance/petty_cash/petty_cash_book/cls_petty_cash_get.php";	$obj_petty_cash_get				= new cls_petty_cash_get($db);
include_once "../../../../class/tables/finance_pettycash_transaction.php";					$finance_pettycash_transaction	= new finance_pettycash_transaction($db);
#END 	- INCLUDE FILES }

//BEGIN - SET PARAMETERS {
$fromDate					= $_REQUEST["FromDate"];
$toDate						= $_REQUEST["ToDate"];
$location					= $_REQUEST["Location"];
$dateArray					= $obj_commonFunctions_get->getDatesBetweenTwoDateRange($fromDate,$toDate);
//END 	- SET PARAMETERS }

//BEGIN - CREATE DAY WISE ARRAY {
$i = 0 ;
$booFirst	= false;
$array		= array();
foreach($dateArray as $row)
{	
	if(!$booFirst || $test!=date('Y-m',strtotime($row)))
	{
		$booFirst	= true;
		$array[$i]['Y-m-d']	= date('Y-m-01',strtotime($row)).'-OPEN';
		$array[$i]['Y-m-d1']= date('Y-m-01',strtotime($row));
		$array[$i]['Y-F']	= 'OPEN';
		$array[$i]['d-D']	= 'OPEN';
		$array[$i]['Y']		= date('Y',strtotime($row));
		$array[$i]['m']		= date('m',strtotime($row));	
		$test				= date('Y-m',strtotime($row));
		$i++;
	}
		$array[$i]['Y-m-d']	= date('Y-m-d',strtotime($row));
		$array[$i]['Y-m-d1']= date('Y-m-d',strtotime($row));
		$array[$i]['Y-F']	= date('Y-F',strtotime($row));
		$array[$i]['d-D']	= date('d-D',strtotime($row));
		$array[$i]['Y']		= date('Y',strtotime($row));
		$array[$i]['m']		= date('m',strtotime($row));
	$i++;
}
//END 	- CREATE DAY WISE ARRAY }

//BEGIN - MAIN QUERY {
	if($location != "")
		$para = "AND I.LOCATION_ID = $location ";
		
    $sql = "SELECT
			  PC.NAME		AS CATEGORY_NAME,
			  PI.CODE		AS ITEM_CODE, ";
foreach($array as $row1){		 
	$sql .= "(SELECT ABS(SUM(CASH_BALANCE)) FROM finance_pettycash_transaction I 
			WHERE I.PETTY_CASH_ITEM_ID = PI.ID
				AND I.DOCUMENT_DATE = '".$row1["Y-m-d"]."'
				$para) 									AS '".$row1["Y-m-d"]."',";
}
	$sql .= "PI.NAME		AS ITEM_NAME,
			 PI.ID			AS ITEM_ID
			  FROM finance_mst_pettycash_item PI
			 INNER JOIN finance_mst_pettycash_category PC
				ON PC.ID = PI.CATEGORY_ID
			WHERE PI.STATUS = 1";//die($sql);
//END 	- MAIN QUERY }	
$db->connect();
	$result = $db->RunQuery($sql);
	//die('hi'.$result);
	$rowCount	= mysqli_num_rows($result);
	$arrayTotal["total"] = $rowCount;
	$data		= array();
	$booFirst	= false;
    while($row = mysqli_fetch_array($result))
	{
		if(!$booFirst)
		{
//BEGIN - OPENING BALANCE {
			$data["mainCat"] 				= '';
			$data["itemName"] 				= 'Opening Balance';
			$data["itemId"] 				= '';

			foreach($array as $row1){
				if($row1["Y-F"]=='OPEN')
				{					
					#$amount				= $obj_petty_cash_get->getOpenBalance($row1["Y-m-d1"],$location,'RunQuery');
					$amount					= $finance_pettycash_transaction->getPreviousMonthOpenBalance($row1["Y-m-d1"],$location);
					$data[$row1["Y-m-d"]] 	= ($amount==''?'':number_format($amount));
					continue;
				}
				$data[$row1["Y-m-d"]]		= '';
			}
			$a[] 							= $data;
//END 	- OPENING BALANCE }
			
//BEGIN - PETTY CASH REIMBURSMENT {
			$data["mainCat"] 				= '';
			$data["itemName"] 				= 'Petty Cash Reimbursement';
			$data["itemId"] 				= '';

			foreach($array as $row1){
				if($row1["Y-F"]=='OPEN')
				{					
					#$amount				= $obj_petty_cash_get->getMonthlyIncome($row1['Y'],$row1['m'],$location);
					$amount					= $finance_pettycash_transaction->getMonthlyIncome($row1['Y'],$row1['m'],$location);
					$data[$row1["Y-m-d"]] 	= ($amount==''?'':number_format($amount));
					continue;
				}
				$data[$row1["Y-m-d"]]		= '';
			}
			$a[] 							= $data;
			$booFirst 						= true;
//END 	- PETTY CASH REIMBURSMENT }
		}
		
		if($row["ITEM_CODE"]=='PCR')
			continue;
			
		$data["mainCat"] 					= $row['CATEGORY_NAME'];
		$data["itemName"] 					= $row['ITEM_NAME'];
		$data["itemId"] 					= $row['ITEM_ID'];
		
		foreach($array as $row1){
			if($row1["Y-F"]=='OPEN')
			{
				$amount						= $obj_petty_cash_get->getDataPeriodOpenBalance($row1['Y-m-d1'],$fromDate,$location,$row['ITEM_ID'],'RunQuery');
				$data[$row1["Y-m-d"]]  		= ($amount=='' || $amount=='0'?'':number_format($amount));
				$total[$row1["Y-m-d"]]     += $amount;
			}
			else
			{
				$data[$row1["Y-m-d"]] 		= ($row[$row1["Y-m-d"]]=='' || $row[$row1["Y-m-d"]]=='0'?'':number_format($row[$row1["Y-m-d"]],0));
				$total[$row1["Y-m-d"]]     += $row[$row1["Y-m-d"]];
			}
		}
		$a[] 					            = $data;
    }

//BEGIN - DISPLAY A EMPTY ROW {
		$data["mainCat"] 					= '';
		$data["itemName"] 					= '';
		$data["itemId"] 					= '';

		foreach($array as $row1){
				$data[$row1["Y-m-d"]] 		= '';			
		}
		$a[] 								= $data;
//END 	- DISPLAY A EMPTY ROW }

//BEGIN - TOTAL DEDUCTION {
		$data["mainCat"] 					= '';
		$data["itemName"] 					= 'TOTAL DEDUCTION';
		$data["itemId"] 					= '';

		foreach($array as $row1){
			if(strpos($row1["Y-m-d"],'OPEN'))
				$data[$row1["Y-m-d"]]   	= '';
			else
				$data[$row1["Y-m-d"]] 		= ($total[$row1["Y-m-d"]]==''?'':'('.number_format($total[$row1["Y-m-d"]]).')');			
		}
		$a[] 								= $data;
//END - TOTAL DEDUCTION }	

//BEGIN - TOTAL {
		$data["mainCat"] 					= '';
		$data["itemName"] 					= 'TOTAL BALANCE';
		$data["itemId"] 					= '';

		foreach($array as $row1){
			if(strpos($row1["Y-m-d"],'OPEN'))
			{
				$openingAmount				= $finance_pettycash_transaction->getMonthlyIncome($row1['Y'],$row1['m'],$location) + $finance_pettycash_transaction->getPreviousMonthOpenBalance($row1["Y-m-d"],$location,'RunQuery') - $total[$row1["Y-m-d"]];	
				$data[$row1["Y-m-d"]]   	= number_format($openingAmount);
			}else{
				$openingAmount 		  	   -= $total[$row1["Y-m-d"]];
				$data[$row1["Y-m-d"]] 		= number_format($openingAmount);
			}
			
		}
		$a[] 								= $data;
//END - TOTAL }		
		$arrayTotal["rows"]					= $a;
    echo json_encode($arrayTotal);
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/finance/petty_cash/petty_cash_book/cls_petty_cash_get.php";
include_once "class/cls_commonFunctions_get.php";
include_once "class/masterData/companies/cls_companies_get.php";
//include 	 "include/javascript.html";
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {
$obj_companies_get			= new cls_companies_get($db);
$obj_commonFunctions_get	= new cls_commonFunctions_get($db);
//END 	- CREATE OBJECTS {
	
//BEGIN - CREATE PARAMETERS {
$company					= $_SESSION['headCompanyId'];

if(!isset($_REQUEST["txtDateFrom"]))
{
	$fromDate				= date('Y-m-01');
	$toDate					= date('Y-m-'.cal_days_in_month(CAL_GREGORIAN,date('m'),date('Y')));
	$location				= $_SESSION["CompanyID"];
}
else
{
	$fromDate				= $_REQUEST["txtDateFrom"];
	$toDate					= $_REQUEST["txtDateTo"];
	$location				= $_REQUEST["cboLocation"];
}
$company_html				= $obj_companies_get->getLocationSelection($company,$location);
$dateArray					= $obj_commonFunctions_get->getDatesBetweenTwoDateRange($fromDate,$toDate);
$companyArray				= $obj_companies_get->GetCompanyReportHeader($location);
//END	- CREATE PARAMETERS }

//BEGIN - CREATE DAY WISE ARRAY {
$i 	= 0;
$y	= 0;
$booFirst	= false;
$booFirst1	= false;
foreach($dateArray as $row)
{	
	if(!$booFirst || $test != date('Y-m',strtotime($row)))
	{
		$booFirst	= true;
		$array[$i]['Y-m-d']	= date('Y-m-01',strtotime($row)).'-OPEN';
		$array[$i]['Y-m-d1']= date('Y-m-01',strtotime($row));
		$array[$i]['Y-m-d1']= date('Y-m-d');	
		$array[$i]['Y-F']	= 'OPEN';
		$array[$i]['d-D']	= 'OPEN';
		$test				= date('Y-m',strtotime($row));
		$i++;
	}
	
	if($booFirst1 && $test1 != date('Y-m',strtotime($row)))	{
		
		$m[$x]['COUNT']		= $y;
		$m[$x]['Y-M'] 		= $preDate;
		$y					= 0;		
		$x++;
	}
		$test1				= date('Y-m',strtotime($row));
		$preDate			= date('Y-M',strtotime($row));
		$booFirst1			= true;
		$y++;					
		$array[$i]['Y-m-d']	= date('Y-m-d',strtotime($row));
		$array[$i]['Y-m-d1']= date('Y-m-d',strtotime($row));
		$array[$i]['Y-F']	= date('Y-F',strtotime($row));
		$array[$i]['d-D']	= date('d-D',strtotime($row));
		
		$m[$x]['COUNT']		= $y;
		$m[$x]['Y-M'] 		= date('Y-M',strtotime($row));

	$i++;
}
//END 	- CREATE DAY WISE ARRAY }
?>

<title>Petty Cash Report <?php echo '['.$companyArray["BASE_CURRENCY_CODE"].']'?></title>
<link rel="stylesheet" type="text/css" href="libraries/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="libraries/easyui/demo/demo.css">
<link rel="stylesheet" type="text/css" href="css/mainstyle.css">
<link rel="stylesheet" type="text/css" href="css/button.css">


<form id="frmMain" name="frmMain" enctype="multipart/form-data" method="post">
  <table style="width:1300px;">
    <tr>
      <td width="81">Date From</td>
      <td width="120"><input type="text" name="txtDateFrom" id="txtDateFrom"  style="width:85px" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" value="<?php echo $fromDate?>" title="Default Current Date"/><input type="text" style="visibility:hidden;width:1px"   onclick="return showCalendar(this.id, '%Y-%m-%');" value="" /></td>
      <td width="79">Date To</td>
      <td width="159"><input type="text" name="txtDateTo" id="txtDateTo"  style="width:85px" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" value="<?php echo $toDate?>" title="Default Current Date"/><input type="text" style="visibility:hidden;width:1px"   onclick="return showCalendar(this.id, '%Y-%m-%');" value="" /></td>
      <td width="79">Location </td>
      <td width="236"><select name="cboLocation" id="cboLocation" style="width:220px" class="cls_formLoad">
          <option value=""></option>
          <?php echo $company_html?>
        </select></td>
      <td width="125">&nbsp;</td>
      <td width="312">&nbsp;</td>
      <td width="69"><a class="button white medium" id="butSearch">Search</a></td>
    </tr>
  </table>
  <table id="tt" title="Petty Cash Report <?php echo '['.$companyArray["BASE_CURRENCY_CODE"].']'?>" class="easyui-datagrid"  style="width:1300px;height:600px"
			url="presentation/finance_new/petty_cash/petty_cash_book/data.php?FromDate=<?php echo $fromDate?>&ToDate=<?php echo $toDate?>&Location=<?php echo $location?>" rownumbers="true" pagination="true" singleSelect="false" iconCls="icon-save" showFooter="true">
    <thead frozen="true">
      <tr>
        <th rowspan="3" field="mainCat" sortable="true" width="125">Category</th>
        <th rowspan="3"  field="itemName" sortable="true" width="200">Item</th>
      </tr>
      <tr></tr>
      <tr></tr>
    </thead>
    <thead>
      <tr>
        <?php foreach($m as $row){?>
        <th colspan="<?php  echo $row['COUNT']+1?>" ><?php  echo $row['Y-M']; ?></th>
        <?php } ?>
      </tr>
      <tr>
        <?php foreach($array as $row){?>
        <th align="right" field="<?php echo $row['Y-m-d'] ?>" <?php echo $row['Y-F']=='OPEN'?'formatter="formatBalanceColumnFont"':'formatter="showHyperlink"'?>><?php  echo $row['d-D'] ?></th>
        <?php } ?>
      </tr>
    </thead>
  </table>
</form>
<script type="text/javascript" src="libraries/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="libraries/easyui/jquery.easyui.min.js"></script>

<script>
var jq = $.noConflict(true);
</script>

<script>
	function formatBalanceColumnFont(val,row){
			return '<span style="color:blue;">'+val+'</span>';
		}
		
	function showHyperlink(val,row)
	{
		var reportId	= 920;
		if(row.itemId=='')
			return val;
		else{	
			var url = "?q="+reportId+"&Id="+row.itemId+"&Date="+$(this).attr('field');
			return '<a target="petty_cash_popup.php" href="'+url+'" class="date">'+val+'</a>';
		}
	}
		
	$(document).ready(function(e) {
        $('.cls_formLoad').live('change',loadForm);
		$('#butSearch').live('click',loadForm);
	
    });
	function loadForm()
	{
		$('#frmMain').submit();
	}
	</script>
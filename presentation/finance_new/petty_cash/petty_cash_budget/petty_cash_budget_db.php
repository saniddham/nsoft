<?php
session_start();
//ini_set('display_errors',1);
$backwardseperator 			= "../../../../";
$mainPath 					= $_SESSION['mainPath'];
$userId 					= $_SESSION['userId'];
$locationId 				= $_SESSION['CompanyID'];
$companyId 					= $_SESSION['headCompanyId'];

include_once "{$backwardseperator}dataAccess/Connector.php";
include_once "../../../../class/finance/petty_cash/petty_cash_budget/cls_petty_cash_budget_get.php";
include_once "../../../../class/finance/petty_cash/petty_cash_budget/cls_petty_cash_budget_set.php";
include_once "../../../../class/cls_commonFunctions_get.php";

$obj_common					= new cls_commonFunctions_get($db);
$obj_petty_cash_budget_get	= new cls_petty_cash_budget_get($db);
$obj_petty_cash_budget_set	= new cls_petty_cash_budget_set($db);

$requestType				= $_REQUEST['requestType'];
$programCode				= 'P0864';

$savedStatus				= true;
$savedMasseged				= '';
$error_sql					= '';

if($requestType=='saveData')
{
	$editMode				= false;
	$year					= $_REQUEST['year'];
	$arrDetails 			= json_decode($_REQUEST['arrDetails'],true);
	
	$db->begin();
	
	$permision_save			= $obj_common->Load_menupermision2($programCode,$userId,'intEdit');
	if($permision_save!=1 && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged		= 'No permission to save.';	
	}
	
	$header_arr				= $obj_petty_cash_budget_get->get_header($year,'RunQuery2');
	if($header_arr['YEAR']=='')
	{
		$resultHArr			= $obj_petty_cash_budget_set->saveHeader($year,$userId,$companyId,$locationId);
		if($resultHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultHArr['savedMassege'];
			$error_sql		= $resultHArr['error_sql'];	
		}
	}
	else
	{
		$resultUHArr		= $obj_petty_cash_budget_set->updateHeader($year,$userId,$companyId,$locationId);
		if($resultUHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultUHArr['savedMassege'];
			$error_sql		= $resultUHArr['error_sql'];	
		}
		$editMode			= true;
	}
	foreach($arrDetails as $array_loop)
	{
		$month				= $array_loop['month'];
		$location			= $array_loop['location'];
		$amount				= ($array_loop['amount']==''?0:$array_loop['amount']);
		
		$checkBudPlan		= $obj_petty_cash_budget_get->checkBudgetPlanAvailable($year,$location,$month,'RunQuery2');
		if($checkBudPlan)
		{
			$resultInsLogAr = $obj_petty_cash_budget_set->updateLog($year,$location,$month);
			if($resultInsLogAr['savedStatus']=='fail' && $savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged 	= $resultInsLogAr['savedMassege'];
				$error_sql		= $resultInsLogAr['error_sql'];	
			}
			
			$resultDUArr	= $obj_petty_cash_budget_set->updateDetails($year,$location,$month,$amount);
			if($resultDUArr['savedStatus']=='fail' && $savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged 	= $resultDUArr['savedMassege'];
				$error_sql		= $resultDUArr['error_sql'];	
			}
		}
		else
		{
			if($amount!=0)
			{
				$resultDArr		= $obj_petty_cash_budget_set->saveDetails($year,$location,$month,$amount);
				if($resultDArr['savedStatus']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged 	= $resultDArr['savedMassege'];
					$error_sql		= $resultDArr['error_sql'];	
				}
			}
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode)
			$response['msg'] 		= "Updated Successfully.";
		else
			$response['msg'] 		= "Saved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= $savedMasseged;
		$response['sql']			= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='loadData')
{
	$year			= $_REQUEST['year'];
	$html			= '';
	
	$detail_result	= $obj_petty_cash_budget_get->getLocation($companyId,'RunQuery');
	while($row = mysqli_fetch_array($detail_result))
	{
		$html	.= "<tr class=\"normalfnt\" id=\"".$row['intId']."\">";
		$html	.= "<td style=\"text-align:left\">".$row['strName']."</td>";
		
		for($i=1;$i<=12;$i++)
		{
			$disableStatus	= false;
					
			if($year<date('Y'))
				$disableStatus = true;
			
			if($year==date('Y') && $i<date('m'))
				$disableStatus = true;
				
			$allocateBudget	= $obj_petty_cash_budget_get->getAllocateBudget($year,$i,$row['intId'],'RunQuery');
			
			$html	.= "<td style=\"text-align:center\" id=\"".$i."\"><input name=\"txtAmount\" id=\"txtAmount\" class=\"clsAmount validate[custom[number]]\" type=\"text\" style=\"width:100%;text-align:right\" value=\"".$allocateBudget."\" ".($disableStatus?'disabled="disabled"':'')."/></td>";
		}
		$html	.= "</tr>";
	}
	
	$response['gridDetail'] = $html;
	echo json_encode($response);
}
?>
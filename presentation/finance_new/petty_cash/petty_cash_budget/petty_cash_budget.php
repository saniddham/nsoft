<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$userId				= $_SESSION["userId"];

include_once "class/finance/petty_cash/petty_cash_budget/cls_petty_cash_budget_get.php";
include_once "class/cls_commonFunctions_get.php";
//include 	 "include/javascript.html";

$obj_petty_cash_budget_get	= new cls_petty_cash_budget_get($db);
$obj_common					= new cls_commonFunctions_get($db);

$programCode				= 'P0864';
$year						= (!isset($_REQUEST['year'])?'':$_REQUEST['year']);

$permision_save				= $obj_common->Load_menupermision($programCode,$userId,'intEdit');

?>
<title>Petty Cash Budget Plan</title>

<!--<script type="text/javascript" src="presentation/finance_new/petty_cash/petty_cash_budget/petty_cash_budget_js.js"></script>-->

<form id="frmPettyCashBudget" name="frmPettyCashBudget" method="post">
<div align="center">
	<div class="trans_layoutXL">
	<div class="trans_text">Petty Cash Budget Plan</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
        <td>
        <table width="100%" border="0">
        <tr class="normalfnt">
        	<td width="8%">Year <span class="compulsoryRed">*</span></td> 
            <td width="92%"><select name="cboYear" id="cboYear" class="validate[required]" style="width:100px">
            <option value=""></option>
            <?php
            $result = $obj_petty_cash_budget_get->getYear('RunQuery');
			while($row = mysqli_fetch_array($result))
			{
				if($header_arr['YEAR']==$row['YEAR'])
				{
					echo "<option selected=\"selected\" value=\"".$row['YEAR']."\">".$row['YEAR']."</option>";
				}
				else if(date('Y')==$row['YEAR'])
				{
					echo "<option selected=\"selected\" value=\"".$row['YEAR']."\">".$row['YEAR']."</option>";
					$year	= $row['YEAR'];
				}
				else
					echo "<option value=\"".$row['YEAR']."\">".$row['YEAR']."</option>";
			}
			?>
            </select></td>   
        </tr>
        </table>
        </td>
    </tr>
    <tr>
    	<td colspan="2" class="normalfnt"><div  style="overflow:scroll;overflow-y:hidden;width:1260px;" id="divGrid">
        <table style="width:1500px;" class="bordered" id="tblMain" >
        <thead>
        <tr>
        	<th style="width:300px">Location</th>
            <?php
			for($i=1;$i<=12;$i++)
			{
			?>
            <th style="width:100px"><?php echo date('M', mktime(0,0,0,$i,1)); ?></th>
            
            <?php
			}
			?>  	
        </tr>
        </thead>
        <tbody>
        <?php
			
			$detail_result	= $obj_petty_cash_budget_get->getLocation($companyId,'RunQuery');
			while($row = mysqli_fetch_array($detail_result))
			{
				
		?>
				<tr class="normalfnt" id="<?php echo $row['intId']; ?>">
                	<td style="text-align:left"><?php echo $row['strName']; ?></td>
        		<?php
				for($i=1;$i<=12;$i++)
				{
					$disableStatus	= false;
					
					if($year<date('Y'))
						$disableStatus = true;
					
					if($year==date('Y') && $i<date('m'))
						$disableStatus = true;
						
					$allocateBudget	= $obj_petty_cash_budget_get->getAllocateBudget($year,$i,$row['intId'],'RunQuery');
				?>
                	<td style="text-align:center" id="<?php echo $i; ?>"><input name="txtAmount" id="txtAmount" class="clsAmount validate[custom[number]]" type="text" style="width:100%;text-align:right" value="<?php echo $allocateBudget; ?>" <?php echo($disableStatus?'disabled="disabled"':''); ?> /></td>
                <?php	
				}
				?>
                </tr>
       	<?php
			}
		?>
        </tbody>
        </table>
        </div>
    </tr>
    <tr>
    <td height="32" >
        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
        	<td align="center"><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($form_permision['edit']==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');?><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
        </tr>
        </table>
    </td>
    </tr>
    </table>
    </div>
</div>
</form>
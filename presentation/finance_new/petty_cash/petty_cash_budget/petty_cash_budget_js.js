// JavaScript Document
var basePath	= "presentation/finance_new/petty_cash/petty_cash_budget/";
$(document).ready(function(){
	
	$('#frmPettyCashBudget').validationEngine();
	
	$('#frmPettyCashBudget #butSave').die('click').live('click',saveData);
	$('#frmPettyCashBudget #cboYear').die('change').live('change',loadData);
	$('#frmPettyCashBudget #butNew').die('click').live('click',clearAll);
});
function saveData()
{
	showWaiting();
	var year	= $('#cboYear').val();
	
	if($('#frmPettyCashBudget').validationEngine('validate'))
	{
		var arrDetails	= "";
		var chkStatus	= false;
		$('#frmPettyCashBudget #tblMain .clsAmount').each(function(){
			
			var amount		= '';
			var month		= $(this).parent().attr('id');
			var location	= $(this).parent().parent().attr('id');
				amount		= $(this).val();
			
			if(amount!='')
				chkStatus	= true;
			
			arrDetails += "{";
			arrDetails += '"month":"'+ month +'",' ;
			arrDetails += '"location":"'+ location +'",' ;
			arrDetails += '"amount":"'+ amount +'"' ;
			arrDetails +=  '},';
		});
		
		if(!chkStatus)
		{
			$(this).validationEngine('showPrompt','No data to save.','fail');
			hideWaiting();	
			return;
		}
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		
		var data		= "requestType=saveData";
		var arrDetails	= '['+arrDetails+']';
		data	   	   += "&year="+year+"&arrDetails="+arrDetails;
		
		var url = basePath+"petty_cash_budget_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmPettyCashBudget #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
					$('#frmPettyCashBudget #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					hideWaiting();
					return;
				}		
		});
	}
	else
	{
		hideWaiting();
	}
}
function loadData()
{
	var year	= $('#frmPettyCashBudget #cboYear').val();
	if(year=='')
	{
		clearAll();
		return;
	}
	showWaiting();
	
	$("#frmPettyCashBudget #tblMain tr:gt(0)").remove();
	var url 	= basePath+"petty_cash_budget_db.php?requestType=loadData";
	var data 	= "year="+$('#frmPettyCashBudget #cboYear').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#tblMain tbody').html(json.gridDetail);
			}
	});
	hideWaiting();		
}
function clearAll()
{
	$('#frmPettyCashBudget #cboYear').val('');
	$('#frmPettyCashBudget #tblMain .clsAmount').val('');
	$('#frmPettyCashBudget #tblMain .clsAmount').removeAttr('disabled');
}
function alertx()
{
	$('#frmPettyCashBudget #butSave').validationEngine('hide')	;
}
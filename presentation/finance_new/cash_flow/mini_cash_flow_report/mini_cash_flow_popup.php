<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/finance/cash_flow/mini_cash_flow_report/cls_mini_cash_flow_report_get.php";
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {
$obj_mini_cash_flow_report_get	= new cls_mini_cash_flow_report_get($db);
//END 	- CREATE OBJECTS {
	
//BEGIN - CREATE PARAMETERS {
$location						= $_SESSION['CompanyID'];
$id								= $_REQUEST['Id'];
$date							= $_REQUEST['Date'];
$dateArray						= explode('-',$date);
//END	- CREATE PARAMETERS }

//BEGIN - REDIRECT TO PETTY CASH REPORT {
if($id==19){
	$_REQUEST["txtDateFrom"]	= $dateArray[0].'-'.$dateArray[1].'-01';
	$_REQUEST["txtDateTo"]		= $dateArray[0].'-'.$dateArray[1].'-'.cal_days_in_month(CAL_GREGORIAN,$dateArray[1],$dateArray[0]);
	$_REQUEST["cboLocation"]	= $location;
	include_once("presentation/finance_new/petty_cash/petty_cash_book/petty_cash.php");	
	exit;
}
//END 	- REDIRECT TO PETTY CASH REPORT }
?>
<head>
<title>Cash Flow Log</title>
</head>
<body>
<form id="frmMain" name="frmMain">
  <table width="500" border="0" cellspacing="0" cellpadding="0" align="center" >
    <tr>
      <td><table width="100%" border="0" cellspacing="2" cellpadding="0">
          <tr>
            <td colspan="3" align="center" class="reportHeader">Cash Flow Detail Log</td>
          </tr>
          <tr>
            <td width="14%" class="normalfnt">Item </td>
            <td width="2%">:</td>
            <td width="84%" class="normalfnt"><?php $itemArray = $obj_mini_cash_flow_report_get->getCashFlowItemName($id); echo $itemArray['REPORT_FIELD_NAME'];?></td>
          </tr>
          <tr>
            <td class="normalfnt">Date</td>
            <td>:</td>
            <td class="normalfnt"><?php echo $date?></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td align="center"><table width="500" border="0" cellspacing="0" cellpadding="0"  class="rptBordered">
          <thead>
            <tr>
              <th width="127">Type</th>
              <th width="152">Transaction Date</th>
              <th width="86">Amount</th>
            </tr>
          </thead>
          <tbody>
            <?php
	$amount		= 0;
  	$result 	= getDetails($date,$id,$location);
  	while($row = mysqli_fetch_array($result))
  	{
			  ?>
            <tr>
              <td><?php echo $row["DOCUMENT_TYPE"]?></td>
              <td style="text-align:center"><?php echo $row["LAST_MODIFIED_DATE"]?></td>
              <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
            </tr>
            <?php
			$amount	+= round($row["AMOUNT"],2);
	 }
		?>
                 <?php
  	$result 	= getForcastAmount($date,$location,$id);
  	while($row = mysqli_fetch_array($result))
  	{
			  ?>
            <tr>
              <td><?php echo $row["DOCUMENT_TYPE"]?></td>
              <td style="text-align:center"><?php echo $row["LAST_MODIFIED_DATE"]?></td>
              <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
            </tr>
            <?php
			$amount	+= round($row["AMOUNT"],2);
	 }
		?>
          </tbody>
          <tfoot>
          	<tr style="font-weight:bold">
              <td colspan="2" style="text-align:center">TOTAL</td>
              <td style="text-align:right"><?php echo number_format($amount,2)?></td>
            </tr>
          </tfoot>
        </table></td>
    </tr>
  </table>
</form>
</body>
<?php
function getDetails($date,$id,$location)
{
	global $db;
	$lastDate 		= date('Y-m-d',strtotime('last month',strtotime($date)));
	
	$sql = "SELECT CHART_OF_ACCOUNT_ID FROM finance_cashflow_cpanal WHERE REPORT_FIELD_ID = $id";
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	
	$sql = "SELECT
			  DOCUMENT_TYPE,
			  LAST_MODIFIED_DATE,
			  AMOUNT
			FROM finance_transaction
			WHERE CHART_OF_ACCOUNT_ID IN(".$row['CHART_OF_ACCOUNT_ID'].")
				AND TRANSACTION_TYPE = (SELECT TRANSACTION_TYPE FROM finance_cashflow_cpanal WHERE REPORT_FIELD_ID = $id)
				AND LOCATION_ID = '$location'
				AND DATE(LAST_MODIFIED_DATE) = '$date'";
	$result = $db->RunQuery($sql);
	if(mysqli_num_rows($result)>0)
		return $result;
	else
	{
		$sql = "SELECT
			  DOCUMENT_TYPE,
			  LAST_MODIFIED_DATE,
			  AMOUNT
			FROM finance_transaction
			WHERE CHART_OF_ACCOUNT_ID IN(".$row['CHART_OF_ACCOUNT_ID'].")
				AND TRANSACTION_TYPE = (SELECT TRANSACTION_TYPE FROM finance_cashflow_cpanal WHERE REPORT_FIELD_ID = $id)
				AND LOCATION_ID = '$location'
				AND DATE(LAST_MODIFIED_DATE) = '$lastDate'";
		return $db->RunQuery($sql);
	}
}

function getForcastAmount($date,$location,$cpanalId)
{
	global $db;
	
	$sql = "SELECT 'FORECAST' AS DOCUMENT_TYPE,
			D.RECEIVE_DATE	AS LAST_MODIFIED_DATE,
			D. AMOUNT		AS  AMOUNT
			FROM finance_cashflow_date_amount_header H
			  INNER JOIN finance_cashflow_date_amount D
				ON D.LOCATION_ID = H.LOCATION_ID
				  and D.REPORT_FIELD_ID = H.REPORT_FIELD_ID
				  and D.YEAR = H.YEAR
			WHERE H.STATUS = 1
			AND H.LOCATION_ID = $location
			AND D.RECEIVE_DATE = '$date'
			AND H.REPORT_FIELD_ID = $cpanalId";
	return $db->RunQuery($sql);
}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
//BEGIN - INCLUDE FILES {
include_once "class/cls_commonFunctions_get.php";
include_once "class/masterData/companies/cls_companies_get.php";
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {
$obj_companies_get			= new cls_companies_get($db);
$obj_commonFunctions_get	= new cls_commonFunctions_get($db);
//END 	- CREATE OBJECTS {
	
//BEGIN - CREATE PARAMETERS {
$company		= $_SESSION['headCompanyId'];


if(!isset($_REQUEST["txtDateFrom"]))
{
	$fromDate	= date('Y-m-01');
	$toDate		= date('Y-m-'.cal_days_in_month(CAL_GREGORIAN,date('m'),date('Y')));
	$location	= $_SESSION['CompanyID'];
}
else
{
	$fromDate	= $_REQUEST["txtDateFrom"];
	$toDate		= $_REQUEST["txtDateTo"];
	$location	= $_REQUEST["cboLocation"];
}

//END	- CREATE PARAMETERS }

$dateArray		= $obj_commonFunctions_get->getDatesBetweenTwoDateRange($fromDate,$toDate);
$monthArray2	= $obj_commonFunctions_get->getMonthBetweenTwoDateRange($fromDate,$toDate);
$companyArray	= $obj_companies_get->GetCompanyReportHeader($location);
$i = 0 ;
foreach($monthArray2 as $monthArray1)
{
	$monthArray[$i]['Y-M'] = date('Y-M',strtotime($monthArray1));
	$monthArray[$i]['DAYS'] = cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($monthArray1)),date('Y',strtotime($monthArray1)));
	$i++;
}

//BEGIN - CREATE DAY WISE ARRAY {
$i = 0 ;
$booFirst	= false;
$booFirst1	= false;
foreach($dateArray as $row)
{	
	if($booFirst1 && $test1 != date('Y-m',strtotime($row)))	{
		
		$m[$x]['COUNT']		= $y;
		$m[$x]['Y-M'] 		= $preDate;
		$y					= 0;		
		$x++;
	}
	$test1				= date('Y-m',strtotime($row));
	$preDate			= date('Y-M',strtotime($row));
	$booFirst1			= true;
	$y++;					
	$array[$i]['Y-m-d']	= date('Y-m-d',strtotime($row));	
	$array[$i]['Y-F']	= date('Y-F',strtotime($row));
	$array[$i]['d-D']	= date('d-D',strtotime($row));

	$m[$x]['COUNT']		= $y;
	$m[$x]['Y-M'] 		= date('Y-M',strtotime($row));
	$i++;
}

//END 	- CREATE DAY WISE ARRAY }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Cash Flow Report <?php echo '['.$companyArray["BASE_CURRENCY_CODE"].']'?></title>

<link rel="stylesheet" type="text/css" href="libraries/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="libraries/easyui/demo/demo.css">
<link rel="stylesheet" type="text/css" href="css/mainstyle.css">
<link rel="stylesheet" type="text/css" href="css/button.css">

<script type="text/javascript" src="libraries/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="libraries/easyui/jquery.easyui.min.js"></script>

</head>
<body>
<form id="frmMain" name="frmMain" enctype="multipart/form-data" method="post">
  <table style="width:1300px;">
    <tr>
      <td width="81">Date From</td>
      <td width="120"><input type="text" name="txtDateFrom" id="txtDateFrom"  style="width:85px" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" value="<?php echo $fromDate?>" title="Default Current Date"/><input type="text" style="visibility:hidden;width:1px"   onclick="return showCalendar(this.id, '%Y-%m-%');" value="" /></td>
      <td width="79">Date To</td>
      <td width="159"><input type="text" name="txtDateTo" id="txtDateTo"  style="width:85px" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" value="<?php echo $toDate?>" title="Default Current Date"/><input type="text" style="visibility:hidden;width:1px"   onclick="return showCalendar(this.id, '%Y-%m-%');" value="" /></td>
      <td width="79">Location </td>
      <td width="236"><select name="cboLocation" id="cboLocation" style="width:220px" class="cls_formLoad">
        <option value=""></option>
        <?php echo $obj_companies_get->getLocationSelection($company,$location)?>
      </select></td>
      <td width="125">&nbsp;</td>
      <td width="312">&nbsp;</td>
      <td width="69"><a class="button white medium" id="butSearch">Search</a></td>
    </tr>
  </table>
  <table id="tt" title="Cash Flow Report <?php echo '['.$companyArray["BASE_CURRENCY_CODE"].']'?>" class="easyui-datagrid"  style="width:1300px;height:600px"
			url="presentation/finance_new/cash_flow/mini_cash_flow_report/mini_cash_flow_report_data.php?FromDate=<?php echo $fromDate?>&ToDate=<?php echo $toDate?>&Location=<?php echo $location?>" rownumbers="true" pagination="true" singleSelect="false" iconCls="icon-save" showFooter="true">
    <thead frozen="true">
      <tr>
        <th rowspan="3"  field="itemName" sortable="true" width="200">Item</th>
      </tr>
      <tr></tr>
      <tr></tr>
    </thead>
    <thead>
      <tr>
        <?php foreach($m as $row){?>
        <th colspan="<?php  echo $row['COUNT']?>" ><?php  echo $row['Y-M']; ?></th>
        <?php } ?>
      </tr>
      <tr>
        <?php foreach($array as $row){?>
        <th align="right" field="<?php echo $row['Y-m-d'] ?>" <?php echo $row['Y-F']=='OPEN'?'formatter="formatBalanceColumnFont"':'formatter="A"'?>><?php  echo $row['d-D'] ?></th>
        <?php } ?>
      </tr>
    </thead>
  </table>
</form>
<script>
	function formatBalanceColumnFont(val,row){
		return '<span style="color:blue;">'+val+'</span>';
	}
	
	function A(val,row)
	{
		var miniCashReportId = 916;	
		if (row.itemId!='-1'){
			var url = "?q="+miniCashReportId+"&Id="+row.itemId+"&Date="+$(this).attr('field');
    		return '<a style="color:'+val[1]+'" target="mini_cash_flow_popup.php" href="'+url+'" class="date">'+val[0]+'</a>';
		}
		else
			return val;
	}
	
	$(document).ready(function(e) {
        $('.cls_formLoad').live('change',loadForm);
		$('#butSearch').live('click',loadForm);
		$('.date1').live('click',AA);	
    });
	
	function AA()
	{
		alert($(this).parent().parent().attr('id'));
	}
	function loadForm()
	{
		$('#frmMain').submit();
	}
	</script>
</body>
</html>
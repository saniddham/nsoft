<?php
session_start();
date_default_timezone_set('Asia/Colombo');
ini_set('max_execution_time',60000);
ini_set('display_errors',0);

#BEGIN 	- INCLUDE FILES {
include_once "../../../../dataAccess/DBManager2.php";														$db 							=  new DBManager2();$db->connect();	
include_once "../../../../class/cls_commonFunctions_get.php";												$obj_commonFunctions_get		= new cls_commonFunctions_get($db);
include_once "../../../../class/finance/petty_cash/petty_cash_book/cls_petty_cash_get.php";					$obj_petty_cash_get				= new cls_petty_cash_get($db);
include_once "../../../../class/finance/cash_flow/mini_cash_flow_report/cls_mini_cash_flow_report_get.php";	$obj_mini_cash_flow_report_get	= new cls_mini_cash_flow_report_get($db);
#END 	- INCLUDE FILES }

#BEGIN 	- SET PARAMETERS {
$fromDate		= $_REQUEST["FromDate"];
$toDate			= $_REQUEST["ToDate"];
$location		= $_REQUEST["Location"];
$dateArray		= $obj_commonFunctions_get->getDatesBetweenTwoDateRange($fromDate,$toDate);
$monthArray2	= $obj_commonFunctions_get->getMonthBetweenTwoDateRange($fromDate,$toDate);
#END 	- SET PARAMETERS }

$i = 0 ;
foreach($monthArray2 as $monthArray1)
{
	$monthArray[$i]['Y-m'] 	 = date('Y-m',strtotime($monthArray1)).'-01-OPEN';
	$monthArray[$i]['Y'] 	 = date('Y',strtotime($monthArray1));
	$monthArray[$i]['m'] 	 = date('m',strtotime($monthArray1));
	$monthArray[$i]['Y-m-d'] = date('Y-m-01',strtotime($monthArray1));
	$monthArray[$i]['DAYS']	 = cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($monthArray1)),date('Y',strtotime($monthArray1)));
	$i++;
}

#BEGIN - CREATE DAY WISE ARRAY {
$i = 0 ;
$booFirst	= false;
$array		= array();
foreach($dateArray as $row)
{	
		$array[$i]['Y-m-d']	= date('Y-m-d',strtotime($row));	
		$array[$i]['Y-F']	= date('Y-F',strtotime($row));
		$array[$i]['d-D']	= date('d-D',strtotime($row));
		$array[$i]['Y']		= date('Y',strtotime($row));
		$array[$i]['m']		= date('m',strtotime($row));
	$i++;
}
#END 	- CREATE DAY WISE ARRAY }

#BEGIN 	- MAIN QUERY {			
$result		= getCpanalData($location);
#END 	- MAIN QUERY }

$rowCount	 = mysqli_num_rows($result);
$arrayTotal["total"] = $rowCount;
$data		 = array();
$booFirst	 = false;
$detailArray = array(); 
	
#BEGIN 	- NORMAL ITEMS {
	$result12	= getCpanalData($location);
    while($row = mysqli_fetch_array($result12))
	{
		$detailArray[$row['REPORT_FIELD_ID']]['ItemId'] 		= $row['REPORT_FIELD_ID'];
		$detailArray[$row['REPORT_FIELD_ID']]['ItemName'] 		= $row['REPORT_FIELD_NAME'];

		foreach($array as $row1)
		{
			$amount 					= calculateAmount($row1["Y-m-d"],$row["CHART_OF_ACCOUNT_ID"],$row["TRANSACTION_TYPE"],$location,$row['REPORT_FIELD_ID']);
			$total1[$row1["Y-m-d"]]     += $amount;
			$detailArray[$row['REPORT_FIELD_ID']]['Date'][$row1["Y-m-d"]] = $amount;
			$detailArray[$row['REPORT_FIELD_ID']]['Color'][$row1["Y-m-d"]] = $color;
		}
    }
#END	- NORMAL ITEMS }

#BEGIN 	- BANK BALANCE {		
/*		$data["itemName"] 				= 'Bank Balance';
		$data["itemId"] 				= '-1';
		$bankBalance					= getBankBalance($fromDate);

		foreach($array as $row1)
		{						
			$data[$row1["Y-m-d"]][0] 	= ($bankBalance==''?'':number_format($bankBalance));
			$bankBalance               -= $total1[$row1["Y-m-d"]];
		}
		$a[] 							= $data;*/
#END 	- BANK BALANCE }

#BEGIN 	- NORMAL ITEMS {
	foreach($detailArray as $row)
	{
		$data["itemId"]					= $row['ItemId'];
		$data["itemName"]				= $row['ItemName'];
		foreach($array as $row1)
		{
			$amount						= $row['Date'][$row1["Y-m-d"]];
			$data[$row1["Y-m-d"]][0]	= ($amount=='' || $amount=='0'?'':number_format($amount));
			$total[$row1["Y-m-d"]]     += $amount;
			$data[$row1["Y-m-d"]][1]	= $row['Color'][$row1["Y-m-d"]];
		}
		$a[] 							= $data;
	}
#END	- NORMAL ITEMS }

#BEGIN	- EMPTY ROW {
		$data["itemName"] 				= '';
		$data["itemId"] 				= '-1';
		foreach($array as $row1)
		{			
			$data[$row1["Y-m-d"]] 		= '';
		}
		$a[] 							= $data;
#END	- EMPTY ROW }
	
#BEGIN 	- TOTAL DEDUCTION {
		$data["itemName"] 				= 'Total Deductions';
		$data["itemId"] 				= '-1';		
		
		foreach($array as $row1)
		{			
			$data[$row1["Y-m-d"]] 		= ($total[$row1["Y-m-d"]]==''?'':number_format($total[$row1["Y-m-d"]]));
		}
		$a[] 							= $data;
#END 	- TOTAL DEDUCTION }

#BEGIN 	- BALANCE AMOUNT {		
/*		$data["itemName"] 				= 'Balance Amount';
		$bankBalance					= getBankBalance($fromDate);
		foreach($array as $row1)
		{	
			$bankBalance				-= $total[$row1["Y-m-d"]];
			$data[$row1["Y-m-d"]] 		= ($bankBalance==''?'':number_format($bankBalance));			
		}
		$a[] 							= $data;*/
#END 	- BALANCE AMOUNT }	
		
		$arrayTotal["rows"]	= $a;
    echo json_encode($arrayTotal);

function calculateAmount($date,$glId,$transType,$location,$cpanalId)
{
	global $db;
	global $color;
	
	if($glId=='' || $transType=='')
		return 0;
	
	$color			= 'red';	
	$lastDate 		= date('Y-m-d',strtotime('last month',strtotime($date)));
	$lastDateArray	= explode('-',$lastDate);
	$dateArray		= explode('-',$date);
	
	$sql = "SELECT
			  COUNT(*)	AS ROW_COUNT
			FROM finance_transaction
			WHERE CHART_OF_ACCOUNT_ID IN($glId)
				AND TRANSACTION_TYPE = '$transType'
				AND LOCATION_ID = '$location'
				AND YEAR(LAST_MODIFIED_DATE) = ".$dateArray[0]."
				AND MONTH(LAST_MODIFIED_DATE) = ".$dateArray[1].";";
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	
	if($row['ROW_COUNT']>0)
	{
		$color = 'green';
		
		$sql = "SELECT
			  ROUND(COALESCE(SUM(AMOUNT),0),2) AS AMOUNT
			FROM finance_transaction
			WHERE CHART_OF_ACCOUNT_ID IN($glId)
				AND TRANSACTION_TYPE = '$transType'
				AND LOCATION_ID = '$location'
				AND LAST_MODIFIED_DATE = '$date'";
		$result = $db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		return $row['AMOUNT'];
	}
	else
	{
		if(!checkdate($lastDateArray[1],$lastDateArray[2],$lastDateArray[0]))
			return '0';
		else{	
			$color = 'red';
			
			$sql = "SELECT
				  ROUND(COALESCE(SUM(AMOUNT),0),2) AS AMOUNT
				FROM finance_transaction
				WHERE CHART_OF_ACCOUNT_ID IN($glId)
					AND TRANSACTION_TYPE = '$transType'
					AND LOCATION_ID = '$location'
					AND LAST_MODIFIED_DATE = '$lastDate'";
			$result = $db->RunQuery($sql);
			$row	= mysqli_fetch_array($result);
			$forcast = getForcastAmount($date,$location,$cpanalId);
			return $row['AMOUNT'] + $forcast;
		}
	}
	
}

function calculateAmount1($date,$glId,$transType,$location)
{
	global $db;
	$lastDate 		= date('Y-m-d',strtotime('last month',strtotime($date)));
	$lastDateArray	= explode('-',$lastDate);
	
	
	$sql = "SELECT
			  ROUND(COALESCE(SUM(AMOUNT),0),2) AS AMOUNT
			FROM finance_transaction
			WHERE CHART_OF_ACCOUNT_ID IN($glId)
				AND TRANSACTION_TYPE = '$transType'
				AND LOCATION_ID = '$location'
				AND LAST_MODIFIED_DATE = '$date'";
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	if($row['AMOUNT']==0)
	{
		if(!checkdate($lastDateArray[1],$lastDateArray[2],$lastDateArray[0]))
			return '0';
		else{	
			$sql = "SELECT
				  ROUND(COALESCE(SUM(AMOUNT),0),2) AS AMOUNT
				FROM finance_transaction
				WHERE CHART_OF_ACCOUNT_ID IN($glId)
					AND TRANSACTION_TYPE = '$transType'
					AND LOCATION_ID = '$location'
					AND LAST_MODIFIED_DATE = '$lastDate'";
			$result = $db->RunQuery($sql);
			$row	= mysqli_fetch_array($result);
			return $row['AMOUNT'];
		}	
	}
	else
	{
		return $row['AMOUNT'];
	}

}

function getCpanalData($location)
{
	global $db;
	
	$sql = "SELECT
			  CP.REPORT_FIELD_ID,
			  CP.REPORT_FIELD_CODE,
			  CP.REPORT_FIELD_NAME,
			  CHART_OF_ACCOUNT_ID,
			  TRANSACTION_TYPE
			FROM finance_cashflow_cpanal CP
			  INNER JOIN finance_cashflow_cpanal_location CL
				ON CL.REPORT_FIELD_ID = CP.REPORT_FIELD_ID
			WHERE CL.LOCATION_ID = $location
				AND CL.STATUS = 1
			ORDER BY ORDER_BY";
	return $db->RunQuery($sql);
}

function getForcastAmount($date,$location,$cpanalId)
{
	global $db;
	
	$sql = "SELECT sum(AMOUNT) AS TOTAL
			FROM finance_cashflow_date_amount_header H
			  INNER JOIN finance_cashflow_date_amount D
				ON D.LOCATION_ID = H.LOCATION_ID
				  and D.REPORT_FIELD_ID = H.REPORT_FIELD_ID
				  and D.YEAR = H.YEAR
			WHERE H.STATUS = 1
			AND H.LOCATION_ID = $location
			AND D.RECEIVE_DATE = '$date'
			AND H.REPORT_FIELD_ID = $cpanalId";
	$result1 = $db->RunQuery($sql);
	$row1	 = mysqli_fetch_array($result1);
	return 	$row1['TOTAL'];
}

?>
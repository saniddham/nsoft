// JavaScript Document
var basePath	= "presentation/finance_new/cash_flow/fixed_amounts_and_dates/";
var reportId	= 918;
$(document).ready(function(){
	
	$('#frmFixedAmountsAndDates').validationEngine();
	
	$('#frmRptFixedDatesAndAmounts #butRptConfirm').die('click').live('click',urlApprove);
	$('#frmRptFixedDatesAndAmounts #butRptReject').die('click').live('click',urlReject);
	$('#frmRptFixedDatesAndAmounts #butRptRevise').die('click').live('click',urlRevise);
	
	$('#frmFixedAmountsAndDates #butInsertRow').die('click').live('click',addNewRow);
	$('#frmFixedAmountsAndDates .removeRow').die('click').live('click',deleteRow);
	$('#frmFixedAmountsAndDates #butSave').die('click').live('click',saveData);
	$('#frmFixedAmountsAndDates #butNew').die('click').live('click',clearAll);
	
	$('#frmFixedAmountsAndDates #butConfirm').die('click').live('click',Confirm);
	$('#frmFixedAmountsAndDates #butRevise').die('click').live('click',Revise);
	$('#frmFixedAmountsAndDates #butReport').die('click').live('click',loadReport);
	
	$('#frmFixedAmountsAndDates #cboYear').die('change').live('change',loadData);
	$('#frmFixedAmountsAndDates #cboLocation').die('change').live('change',loadData);
	$('#frmFixedAmountsAndDates #cboItem').die('change').live('change',loadData);
	
});

function addNewRow()
{
	var lastDateIdArr 	= $('#frmFixedAmountsAndDates #tblMain tbody tr:last').find('.clsDate').attr('id').split('~');
	var lastDateNo		= parseFloat(lastDateIdArr[1]);
	$('#frmFixedAmountsAndDates #tblMain tbody tr:last').after("<tr>"+$('#frmFixedAmountsAndDates #tblMain .cls_tr_firstRow').html()+"</tr>");
	$('#frmFixedAmountsAndDates #tblMain tbody tr:last').find('.clsDate').attr('id','txtPayDate~'+(lastDateNo+1));
	$('#frmFixedAmountsAndDates #tblMain tbody tr:last').find('.clsAmount').val('');
}
function deleteRow()
{
	var delRowCount = parseInt(document.getElementById('tblMain').rows.length);
	
	if(delRowCount>4)
		$(this).parent().parent().remove();
	
	$('#frmFixedAmountsAndDates #tblMain tbody tr').removeClass('cls_tr_firstRow');
	$('#frmFixedAmountsAndDates #tblMain tbody tr:first').addClass('cls_tr_firstRow');
}
function saveData()
{
	$('#frmFixedAmountsAndDates').validationEngine();
	showWaiting();
	
	var year		= $('#frmFixedAmountsAndDates #cboYear').val();
	var location	= $('#frmFixedAmountsAndDates #cboLocation').val();
	var itemId		= $('#frmFixedAmountsAndDates #cboItem').val();
	
	if($('#frmFixedAmountsAndDates').validationEngine('validate'))
	{
		var arrHeader = "{";
							arrHeader += '"year":"'+year+'",' ;
							arrHeader += '"location":"'+location+'",' ;
							arrHeader += '"itemId":"'+itemId+'"' ;
			arrHeader  += "}";
		
		var arrDetails	= "";
		var checkDate	= false;
		$('#frmFixedAmountsAndDates #tblMain .clsAmount').each(function(){
			
			var date		= $(this).parent().parent().find('.clsDate').val();
			var amount		= $(this).val();
			var count		= 0;
			
			$('#frmFixedAmountsAndDates #tblMain .clsDate').each(function(){
				
				if(date==$(this).val())
				{
					count++;
				}
				
				if(count>=2)
				{
					checkDate = true;
					return false;
				}
				
				if(checkDate)
					return false;
			});
		
			arrDetails += "{";
			arrDetails += '"date":"'+ date +'",' ;
			arrDetails += '"amount":"'+ amount +'"' ;
			arrDetails +=  '},';
		});
		
		if(checkDate)
		{
			$('#frmFixedAmountsAndDates #butSave').validationEngine('showPrompt','System not allow to duplicate the forecast dates.','fail');
			hideWaiting();
			return;
		}
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		var arrHeader	= arrHeader;
		var data		= "requestType=saveData";
		var arrDetails	= '['+arrDetails+']';
		data	   	   += "&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		var url 	= basePath+"fixed_amounts_and_dates_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmFixedAmountsAndDates #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						$('#frmFixedAmountsAndDates #butConfirm').show();
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
					$('#frmFixedAmountsAndDates #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					hideWaiting();
					return;
				}		
		});
	}
	else
	{
		hideWaiting();
	}
	
}

function urlApprove()
{

	var val = $.prompt('Are you sure you want to approve this Fixed amounts ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = basePath+"fixed_amounts_and_dates_db.php"+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptFixedDatesAndAmounts #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptFixedDatesAndAmounts #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function urlReject()
{
	var val = $.prompt('Are you sure you want to reject this Fixed amounts ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = basePath+"fixed_amounts_and_dates_db.php"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptFixedDatesAndAmounts #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx1()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptFixedDatesAndAmounts #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx1()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function urlRevise()
{
	var val = $.prompt('Are you sure you want to revise this Fixed amounts ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = basePath+"fixed_amounts_and_dates_db.php"+window.location.search+'&requestType=revise';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptFixedDatesAndAmounts #butRptRevise').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx1()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptFixedDatesAndAmounts #butRptRevise').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx1()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function loadData()
{
	showWaiting();
	var year		= $('#frmFixedAmountsAndDates #cboYear').val();
	var location	= $('#frmFixedAmountsAndDates #cboLocation').val();
	var itemId		= $('#frmFixedAmountsAndDates #cboItem').val();
	
	$("#frmFixedAmountsAndDates #tblMain tr:gt(3)").remove();
	$('#frmFixedAmountsAndDates #tblMain .clsAmount').val('');
	
	if(year!='' && location!='' && itemId!='')
	{
		var url 	= basePath+"fixed_amounts_and_dates_db.php?requestType=loadData";
		var data 	= "year="+year+'&location='+location+'&itemId='+itemId;
		$.ajax({
				url:url,
				data:data,
				dataType:'json',
				type:'POST',
				async:false,
				success:function(json)
				{
					$('#frmFixedAmountsAndDates #tblMain tbody').html(json.html);
					hideWaiting();
				}
		});	
		
	}
	hideWaiting();
}
function Confirm()
{
	var url  = "?q="+reportId+"&location="+$('#frmFixedAmountsAndDates #cboLocation').val();
	    	url += "&year="+$('#frmFixedAmountsAndDates #cboYear').val();
			url += "&item="+$('#frmFixedAmountsAndDates #cboItem').val();
	    url += "&mode=Confirm";
	window.open(url,'rpt_fixed_amounts_and_dates.php');
}
function Revise()
{
	var url  = "?q="+reportId+"&location="+$('#frmFixedAmountsAndDates #cboLocation').val();
	    	url += "&year="+$('#frmFixedAmountsAndDates #cboYear').val();
			url += "&item="+$('#frmFixedAmountsAndDates #cboItem').val();
	    url += "&mode=Revise";
	window.open(url,'rpt_fixed_amounts_and_dates.php');
}
function loadReport()
{
	if($('#frmFixedAmountsAndDates #cboYear').val()=='')
	{
		$('#frmFixedAmountsAndDates #cboYear').validationEngine('showPrompt','Please select a year.','fail');
		return;
	}
	if($('#frmFixedAmountsAndDates #cboLocation').val()=='')
	{
		$('#frmFixedAmountsAndDates #cboLocation').validationEngine('showPrompt','Please select a location.','fail');
		return;
	}
	if($('#frmFixedAmountsAndDates #cboItem').val()=='')
	{
		$('#frmFixedAmountsAndDates #cboItem').validationEngine('showPrompt','Please select a item.','fail');
		return;
	}
	
	var url  = "?q="+reportId+"&location="+$('#frmFixedAmountsAndDates #cboLocation').val();
		url += "&year="+$('#frmFixedAmountsAndDates #cboYear').val();
		url += "&item="+$('#frmFixedAmountsAndDates #cboItem').val();
	window.open(url,'rpt_fixed_amounts_and_dates.php');
	
}
function clearAll()
{
	$('#frmFixedAmountsAndDates #cboYear').val('');
	$('#frmFixedAmountsAndDates #cboLocation').val('');
	$('#frmFixedAmountsAndDates #cboItem').val('');
	
	$("#frmFixedAmountsAndDates #tblMain tr:gt(3)").remove();
	
	$('#frmFixedAmountsAndDates #tblMain tbody tr').removeClass('cls_tr_firstRow');
	$('#frmFixedAmountsAndDates #tblMain tbody tr:first').addClass('cls_tr_firstRow');
	
	$('#frmFixedAmountsAndDates #tblMain .clsAmount').val('');
}
function alertx()
{
	$('#frmFixedAmountsAndDates #butSave').validationEngine('hide')	;
}
function alertx1()
{
	$('#frmRptFixedDatesAndAmounts #butRptConfirm').validationEngine('hide');
	$('#frmRptFixedDatesAndAmounts #butRptReject').validationEngine('hide')	;
	$('#frmRptFixedDatesAndAmounts #butRptRevise').validationEngine('hide')	;
}



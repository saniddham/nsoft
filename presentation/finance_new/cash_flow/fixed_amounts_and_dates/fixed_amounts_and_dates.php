<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId					= $_SESSION["CompanyID"];
$companyId					= $_SESSION["headCompanyId"];
$userId						= $_SESSION["userId"];

include_once "class/finance/cash_flow/cls_cf_common.php";
include_once "class/finance/cash_flow/fixed_amounts_and_dates/cls_fixed_amounts_and_dates_get.php";
include_once "class/cls_commonErrorHandeling_get.php";
include_once "class/cls_commonFunctions_get.php";
//include 	 "include/javascript.html";

$obj_cf_common				= new cls_cf_common($db);
$obj_amounts_and_dates_get	= new cls_fixed_amounts_and_dates_get($db);
$obj_commonErrHandle		= new cls_commonErrorHandeling_get($db);
$obj_common					= new cls_commonFunctions_get($db);

$programCode				= 'P0856';
$year						= (!isset($_REQUEST['year'])?'':$_REQUEST['year']);
$location					= (!isset($_REQUEST['location'])?'':$_REQUEST['location']);
$item						= (!isset($_REQUEST['item'])?'':$_REQUEST['item']);

$header_arr					= $obj_amounts_and_dates_get->get_header($location,$item,$year,'RunQuery');
$detailResult				= $obj_amounts_and_dates_get->get_details($location,$item,$year,'RunQuery');

$intStatus					= $header_arr['STATUS'];
$levels						= $header_arr['LEVELS'];

$permision_arr				= $obj_commonErrHandle->get_permision_withApproval_save($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_save				= $permision_arr['permision'];

$permision_arr				= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_confirm			= $permision_arr['permision'];

$permision_arr				= $obj_commonErrHandle->get_permision_withApproval_revise($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_revise			= $permision_arr['permision'];
?>
<title>Forecast Amounts And Dates</title>

<!--<script type="text/javascript" src="presentation/finance_new/cash_flow/fixed_amounts_and_dates/fixed_amounts_and_dates_js.js"></script>-->

<form id="frmFixedAmountsAndDates" name="frmFixedAmountsAndDates" method="post">
<div align="center">
	<div class="trans_layoutS">
	<div class="trans_text">Forecast Amounts And Dates</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
        	<td>
            <table width="100%" border="0">
            <tr class="normalfnt">
            	<td width="8%">&nbsp;</td>
            	<td width="17%">Year <span class="compulsoryRed">*</span></td>
            	<td width="61%"><select name="cboYear" id="cboYear" style="width:100px" class="validate[required]">
                <option value=""></option>
                <?php
					$resultYear	= $obj_amounts_and_dates_get->getYear();
					while($row = mysqli_fetch_array($resultYear))
					{
						if($header_arr['YEAR']==$row['YEAR'])
							echo "<option selected=\"selected\" value=\"".$row['YEAR']."\">".$row['YEAR']."</option>";
						else if(date('Y')==$row['YEAR'])
							echo "<option selected=\"selected\" value=\"".$row['YEAR']."\">".$row['YEAR']."</option>";
						else
							echo "<option value=\"".$row['itemId']."\">".$row['itemName']."</option>";
					}
				?>
          	    </select></td>
            	<td width="14%">&nbsp;</td>
            </tr>
            <tr class="normalfnt">
              <td>&nbsp;</td>
              <td>Location <span class="compulsoryRed">*</span></td>
              <td><select name="cboLocation" id="cboLocation" style="width:280px" class="validate[required]">
               <option value=""></option>
				<?php
                $result = $obj_common->loadLocation($companyId,'RunQuery');
                while($row = mysqli_fetch_array($result))
                {
					if($header_arr['LOCATION_ID']==$row['intId'])
						echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
					else if($locationId==$row['intId'])
						echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
					else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                }
                ?>
          	  </select></td>
              <td>&nbsp;</td>
            </tr>
            <tr class="normalfnt">
              <td>&nbsp;</td>
              <td>Item <span class="compulsoryRed">*</span></td>
              <td><select name="cboItem" id="cboItem" style="width:280px" class="validate[required]">
                <option value=""></option>
                <?php
                $result = $obj_amounts_and_dates_get->getFixedForecastItem();
                while($row = mysqli_fetch_array($result))
                {
					if($header_arr['ITEM_ID']==$row['REPORT_FIELD_ID'])
						echo "<option selected=\"selected\" value=\"".$row['REPORT_FIELD_ID']."\">".$row['REPORT_FIELD_NAME']."</option>";
					else
						echo "<option value=\"".$row['REPORT_FIELD_ID']."\">".$row['REPORT_FIELD_NAME']."</option>";
                }
                ?>
              </select></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            </table>
            </td>    
        </tr>
        <tr>
        <td>
            <table width="100%" class="bordered" id="tblMain">
            <thead>
            <tr class="normalfnt">
            	<th colspan="3" style="text-align:right"><a class="button white small" id="butInsertRow" name="butInsertRow">Add New Row</a></th>
            </tr>
            <tr>
            <tr class="normalfnt">
            	<th width="9%">Del</th>
            	<th width="39%">Date</th>
                <th width="52%">Amount</th>
            </tr>
            </thead>
            <tbody>
            <?php
			$count	= 2;
			if(mysqli_num_rows($detailResult)>0)
			{
				while($row = mysqli_fetch_array($detailResult))
				{
					$base_curr_res			= $obj_common->loadCompany_result($companyId,'RunQuery');
					$row_bc					= mysqli_fetch_array($base_curr_res);
					$base_currency			= $row_bc['intBaseCurrencyId'];
					$used_amount			= $obj_cf_common->get_cf_used_invoice_amounts($location,$item,$base_currency,$row['DATE'],$companyId,'RunQuery');
				?>
					<tr class="cls_tr_firstRow">
						<td style="text-align:center" class="clsDelete"><img src="images/del.png" class="removeRow mouseover"/></td>
						<td style="text-align:center"><input <?php if($used_amount > 0){ ?> disabled="disabled" <?php }  ?>   name="txtDate" type="text" value="<?php echo $row['DATE']; ?>" class="clsDate" id="txtDate~<?php echo $count; ?>" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
						<td style="text-align:center"><input name="txtAmount" type="text" id="txtAmount" style="width:150px;text-align:right" class="clsAmount validate[custom[number],min[<?php echo $used_amount; ?>]]" value="<?php echo $row['AMOUNT']; ?>"/></td>
					</tr>
				<?php
					$count++;
				}
			}
			else
			{
			?>
            <tr class="cls_tr_firstRow">
            	<td style="text-align:center" class="clsDelete"><img src="images/del.png" class="removeRow mouseover"/></td>
                <td style="text-align:center"><input name="txtDate" type="text" value="<?php echo date('Y-m-d'); ?>" class="clsDate" id="txtDate~<?php echo $count; ?>" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td style="text-align:center"><input name="txtAmount" type="text" id="txtAmount" style="width:150px;text-align:right" class="clsAmount validate[custom[number],min[0]]" value=""/></td>
            </tr>
            <?php
			}
			?>
            </tbody>
            </table>
        </td>
        </tr>
        <tr>
        	<td></td>
        </tr>
        <tr>
        <td height="32" >
        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center"><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a><a class="button white medium" id="butConfirm"  <?php if($permision_confirm!=1){ ?> style="display:none"<?php } ?>>Approve</a><a class="button white medium" id="butRevise" <?php if($permision_revise!=1){ ?>  style="display:none"<?php } ?>>Revise</a><a class="button white medium" id="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
        </tr>
        </table>
        </td>
    </tr>
    </table>
    </div>
</div>
</form>
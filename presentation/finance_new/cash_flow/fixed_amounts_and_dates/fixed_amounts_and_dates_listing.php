<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once("libraries/jqgrid2/inc/jqgrid_dist.php");
include_once "class/finance/cash_flow/fixed_amounts_and_dates/cls_fixed_amounts_and_dates_get.php";

$obj_amounts_and_dates_get	= new cls_fixed_amounts_and_dates_get($db);

$company 				= $_SESSION['headCompanyId'];
$location   			= $_SESSION['CompanyID'];
$intUser  				= $_SESSION["userId"];
$HR_DB 					= $_SESSION['HRDatabase'];
$main_DB 				= $_SESSION['Database'];

$menuId					= 856;
$reportId				= 918;

$programName			='Fixed Forcast Amounts';
$programCode			='P0856';

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'tb1.STATUS',
				'LOCATION'=>'L.strName',
				'ITEM_CODE'=>'FCC.REPORT_FIELD_CODE',
				'ITEM_NAME'=>'FCC.REPORT_FIELD_NAME',
				'USER'=>'SU.strUserName',
				'DATE'=>'tb1.CREATED_DATE'
				);
				
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2','Revised'=>'-1');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND tb1.CREATED_DATE = '".date('Y-m-d')."'";
	
// }

$max_approveLevel 		= $obj_amounts_and_dates_get->get_max_approve_levels('RunQuery');
$sql					= $obj_amounts_and_dates_get->get_listing_sql($programCode,$max_approveLevel,$intUser,$location,$where_string);
						//echo $sql;
						 
$col 	= array();
$cols 	= array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Revised:Revised" ;
$col["editoptions"] =  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

$col["title"] 	= "Year"; // caption of column
$col["name"] 	= "YEAR"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= "?q=".$menuId."&location={LOCATION_ID}&item={ITEM_ID}&year={YEAR}";	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
 
//PO No
$col["title"] 	= "Location"; // caption of column
$col["name"] 	= "LOCATION"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "4";
//searchOper
$col["align"] 	= "left";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= "?q=".$menuId."&location={LOCATION_ID}&item={ITEM_ID}&year={YEAR}";	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$formLink  = "?q=".$menuId."&location={LOCATION_ID}&item={ITEM_ID}&year={YEAR}";
$reportLink  = "?q=".$reportId."&location={LOCATION_ID}&item={ITEM_ID}&year={YEAR}";
$reportLinkApprove  = "?q=".$reportId."&location={LOCATION_ID}&item={ITEM_ID}&year={YEAR}&mode=Confirm";
$reportLinkRevise   = "?q=".$reportId."&location={LOCATION_ID}&item={ITEM_ID}&year={YEAR}&mode=Revise";
 
$col["title"] 	= "Item Code"; // caption of column
$col["name"] 	= "ITEM_CODE"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
$col["align"] 	= "left";
$cols[] = $col;	$col=NULL;

$col["title"] 	= "Item Name"; // caption of column
$col["name"] 	= "ITEM_NAME"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "4";
$col["align"] 	= "left";
$cols[] = $col;	$col=NULL;

$col["title"] 	= "Location ID"; // caption of column
$col["name"] 	= "LOCATION_ID"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
$col["align"] 	= "center";
$col["hidden"]  = true;   
$cols[] = $col;	$col=NULL;

$col["title"] 	= "Item ID"; // caption of column
$col["name"] 	= "ITEM_ID"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
$col["align"] 	= "center";
$col["hidden"] = true;   
$cols[] = $col;	$col=NULL;

//PO Year
$col["title"] = "User"; // caption of column
$col["name"] = "USER"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

 
//PO Year
$col["title"] = "Date"; // caption of column
$col["name"] = "DATE"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;
 
//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$max_approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}

//FIRST APPROVAL
$col["title"] = "Revise"; // caption of column
$col["name"] = "Revise"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkRevise;
$col['linkName']	= 'Revise';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;


//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Forecast Amount And Dates";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'ITEM_NAME'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Forecast Amount And Dates</title>
<?php
echo $out;
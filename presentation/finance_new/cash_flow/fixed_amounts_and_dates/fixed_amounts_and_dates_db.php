<?php
	session_start();
	ini_set('display_errors',0);
	$backwardseperator 		= "../../../../";
	$mainPath 				= $_SESSION['mainPath'];
	$userId 				= $_SESSION['userId'];
	$location 				= $_SESSION['CompanyID'];
	$company 				= $_SESSION['headCompanyId'];
	$HR_DB 					= $_SESSION['HRDatabase'];
	$main_DB 				= $_SESSION['Database'];

	include_once "{$backwardseperator}dataAccess/Connector.php";
 	require_once "../../../../class/cls_commonFunctions_get.php";
	require_once "../../../../class/cls_commonErrorHandeling_get.php";
	include_once "../../../../class/finance/cash_flow/cls_cf_common.php";
	include_once "../../../../class/finance/cash_flow/fixed_amounts_and_dates/cls_fixed_amounts_and_dates_get.php";
	include_once "../../../../class/finance/cash_flow/fixed_amounts_and_dates/cls_fixed_amounts_and_dates_set.php";
  	
	$obj_cf_common				= new cls_cf_common($db);
	$obj_common					= new cls_commonFunctions_get($db);
	$obj_commonErr				= new cls_commonErrorHandeling_get($db);
	
	$obj_amounts_and_dates_get	= new cls_fixed_amounts_and_dates_get($db);
	$obj_amounts_and_dates_set	= new cls_fixed_amounts_and_dates_set($db);
  
	$requestType				= $_REQUEST['requestType'];
 	
	$programName				='Fixed Forcast Amounts';
	$programCode				='P0856';

 	//---------------------------	
	
if($requestType=='approve'){
	
		$location				= $_REQUEST["location"];
		$year					= $_REQUEST["year"];
		$item					= $_REQUEST["item"];
	
		$db->begin();
		
		$rollBack_flag 			= false;
		$rollBack_msg			= '';
		$rollBack_sql			= '';
	
		$header_array			= $obj_amounts_and_dates_get->get_header($location,$item,$year,'RunQuery2');
		//$paid_flag				= $obj_amounts_and_dates_get->get_paid_flag($location,$item,$year,'RunQuery2');
		$details_result			= $obj_amounts_and_dates_get->get_details($location,$item,$year,'RunQuery2');
		$permition_arr			= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$userId,$programCode,'RunQuery2');
		$permision_confirm		= $permition_arr['permision']; 
		
		//VALIDATION--------------------------------------
		//1. check for approval permision
 		if($permition_arr['type']  == 'fail'){
			$rollBack_flag		=	true;
			$rollBack_msg	 	=	$permition_arr['msg'];	
			$rollBack_sql		=	'';
 		}
		if($rollBack_flag != true){
			while($row_d=mysqli_fetch_array($details_result)){
				if($rollBack_flag != true){
					$date_rcv	= $row_d['DATE'];
					$amount		= $row_d['AMOUNT'];
					$min_amount	= $row_d['USED_AMOUNT'];
					
					$base_curr_res			= $obj_common->loadCompany_result($company,'RunQuery2');
					$row					= mysqli_fetch_array($base_curr_res);
					$base_currency			= $row['intBaseCurrencyId'];
					$used_amount			= $obj_cf_common->get_cf_used_invoice_amounts($location,$item,$base_currency,$date_rcv,$company,'RunQuery2');	
 					
					if($amount < $used_amount){
						$rollBack_flag		=	true;
						$rollBack_msg	 	=	"amount should greater than previous amount.<br> Date : $date_rcv <br> previous Amount : $used_amount ";	
						$rollBack_sql		=	'';
					}
				}
			}
		}

		
		//SAVING--------------------------------------
		if($rollBack_flag != true){
		//3. update header status
			$status				= $header_array['STATUS']-1;
			$response_1			= $obj_amounts_and_dates_set->update_header_status($location,$item,$year,$status,'RunQuery2');
			if($response_1['type']	 == 'fail'){
				$rollBack_flag	=true;
				$rollBack_msg	=$response_1['msg'];
				$rollBack_sql	=$response_1['sql'];
			}
		}
		if($rollBack_flag != true){
		//4. approve by insert
			if($rollBack_flag != true){
				$header_array	= $obj_amounts_and_dates_get->get_header($location,$item,$year,'RunQuery2');
				$levels			= $header_array['LEVELS']+1-$header_array['STATUS'];
				$response_2		= $obj_amounts_and_dates_set->approved_by_insert($location,$item,$year,$userId,$levels,'RunQuery2');
				if($response_2['type']	 == 'fail'){
					$rollBack_flag		=true;
					$rollBack_msg		=$response_2['msg'];
					$rollBack_sql		=$response_2['sql'];
				}
			}
		}
		if($rollBack_flag != true){
		//4. approve by insert
			if($rollBack_flag != true){
				$header_array	= $obj_amounts_and_dates_get->get_header($location,$item,$year,'RunQuery2');
				if($header_array['STATUS'] ==1 ){
					$response_L		= $obj_amounts_and_dates_set->insert_to_log($location,$item,$year,'OPENING_FIXED','RunQuery2');
					if($response_L['type']	 == 'fail'){
						$rollBack_flag		=true;
						$rollBack_msg		=$response_L['msg'];
						$rollBack_sql		=$response_L['sql'];
					}
				}
			}
		}
		
   //$rollBack_flag=true;
		$header_array	= $obj_amounts_and_dates_get->get_header($location,$item,$year,'RunQuery2');

		if($rollBack_flag==true){
			$db->rollback();
			$response['type']	= 'fail';
			$response['msg']	= $rollBack_msg;
			$response['q']		= $rollBack_sql;
		}
		else if($header_array['STATUS'] ==1 ){
			$db->commit();
			$response['type']	= 'pass';
			$response['msg']	= 'Final Approval Raised successfully.';
		}
		else if($header_array['STATUS'] > 1  && $header_array['STATUS'] <= $header_array['LEVELS']){
			$db->commit();
			$response['type']	= 'pass';
			$response['msg']	= 'Approval '.($header_array['LEVELS']+2-$header_array['STATUS']).' raised successfully.';
		}
		else{
			$db->rollback();
			$response['type']	= 'fail';
			$response['msg']	= $db->errormsg;
			$response['q']		= $sql;
		}
	
		$db->CloseConnection();		
		echo json_encode($response);
	
	}
	else if($requestType=='reject'){
		
		$rollBack_flag 			= false;
		$rollBack_msg			= '';
		$rollBack_sql			= '';
		
		$location				= $_REQUEST["location"];
		$year					= $_REQUEST["year"];
		$item					= $_REQUEST["item"];
		
		$db->begin();
	
		$header_array			= $obj_amounts_and_dates_get->get_header($location,$item,$year,'RunQuery2');
		$permition_arr			= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['LEVELS'],$userId,$programCode,'RunQuery2');
		$permision_confirm		= $permition_arr['permision']; 
		//--------------------------
		//$permition_arr['type'];
		if($permition_arr['type']  == 'fail'){
			$rollBack_flag		=	true;
			$rollBack_msg	 	=	$permition_arr['msg'];	
			$rollBack_sql		=	'';
 		}
		
		if($rollBack_flag != true){
		//1. update header status
			$status				=0;
			$response_1			= $obj_amounts_and_dates_set->update_header_status($location,$item,$year,$status,'RunQuery2');
			if($response_1['type']	 == 'fail'){
				$rollBack_flag	=true;
				$rollBack_msg	=$response_1['msg'];
				$rollBack_sql	=$response_1['sql'];
			}
		}
			
		if($rollBack_flag != true){
		//2. approve by insert
			if($rollBack_flag != 1){
				$response_2		= $obj_amounts_and_dates_set->approved_by_insert($location,$item,$year,$userId,0,'RunQuery2');
				if($response_2['type']	 == 'fail'){
					$rollBack_flag		=true;
					$rollBack_msg		.=$response_2['msg'];
					$rollBack_sql		.=$response_2['sql'];
				}
			}
		}
		
		if($rollBack_flag==true){
			$db->rollback();
			$response['type']	= 'fail';
			$response['msg']	= $rollBack_msg;
			$response['q']		= $rollBack_sql;
		}
		else if($toSave==$saved){
			$header_array		= $obj_amounts_and_dates_get->get_header($location,$item,$year,'RunQuery2');
			$db->commit();
			$response['type']	= 'pass';
			if($header_array['STATUS']==0)
			$response['msg']	= 'Rejected successfully.';
			else
			$response['msg']	= 'Rejection Failed.';
		}
		else{
			$db->rollback();
			$response['type']	= 'fail';
			$response['msg']	= $db->errormsg;
			$response['q']		= $sql;
		}
	
		$db->CloseConnection();		
		echo json_encode($response);
	
	}
	else if($requestType=='revise'){
		
		$rollBack_flag 			= false;
		$rollBack_msg			= '';
		$rollBack_sql			= '';
		
		$location				= $_REQUEST["location"];
		$year					= $_REQUEST["year"];
		$item					= $_REQUEST["item"];
		
		$db->begin();
	
		$header_array			= $obj_amounts_and_dates_get->get_header($location,$item,$year,'RunQuery2');
		//$paid_flag				= $obj_amounts_and_dates_get->get_paid_flag($location,$item,$year,'RunQuery2');
		$details_result			= $obj_amounts_and_dates_get->get_details($location,$item,$year,'RunQuery2');
		$permition_arr			= $obj_commonErr->get_permision_withApproval_revise($header_array['STATUS'],$header_array['LEVELS'],$userId,$programCode,'RunQuery2');
		$permision_confirm		= $permition_arr['permision']; 
		//--------------------------
		//$permition_arr['type'];
		if($permition_arr['type']  == 'fail'){
			$rollBack_flag		=	true;
			$rollBack_msg	 	=	$permition_arr['msg'];	
			$rollBack_sql		=	'';
 		}
 		/*else if($paid_flag  == 1){
			$rollBack_flag		=	true;
			$rollBack_msg	 	=	"Payments already raised using this saved amounts.Can't revise";	
			$rollBack_sql		=	'';
 		}*/
		
		
		
		if($rollBack_flag != true){
		//1. update header status
			$status				= -1;
			$response_1			= $obj_amounts_and_dates_set->update_header_status($location,$item,$year,$status,'RunQuery2');
			if($response_1['type']	 == 'fail'){
				$rollBack_flag	=true;
				$rollBack_msg	=$response_1['msg'];
				$rollBack_sql	=$response_1['sql'];
			}
		}
			
		if($rollBack_flag != true){
		//2. approve by insert
			if($rollBack_flag != 1){
				$response_2		= $obj_amounts_and_dates_set->approved_by_insert($location,$item,$year,$userId,-1,'RunQuery2');
				if($response_2['type']	 == 'fail'){
					$rollBack_flag		=true;
					$rollBack_msg		.=$response_2['msg'];
					$rollBack_sql		.=$response_2['sql'];
				}
			}
		}
		$response_L		= $obj_amounts_and_dates_set->insert_to_log($location,$item,$year,'OPENING_FIXED_REVISE','RunQuery2');
		if($response_L['type']	 == 'fail'){
			$rollBack_flag		=true;
			$rollBack_msg		=$response_L['msg'];
			$rollBack_sql		=$response_L['sql'];
		}
		
		//$rollBack_flag		=true;
		
		if($rollBack_flag==true){
			$db->rollback();
			$response['type']	= 'fail';
			$response['msg']	= $rollBack_msg;
			$response['q']		= $rollBack_sql;
		}
		else if($toSave==$saved){
			$header_array		= $obj_amounts_and_dates_get->get_header($location,$item,$year,'RunQuery2');
			$db->commit();
			$response['type']	= 'pass';
			if($header_array['STATUS']==-1)
			$response['msg']	= 'Revised successfully.';
			else
			$response['msg']	= 'Revision Failed.';
		}
		else{
			$db->rollback();
			$response['type']	= 'fail';
			$response['msg']	= $db->errormsg;
			$response['q']		= $sql;
		}
	
		$db->CloseConnection();		
		echo json_encode($response);
	
	}
else if($requestType=='saveData')
{
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	$arrDetails 		= json_decode($_REQUEST['arrDetails'],true);
	
	$db->begin();
	
	$locationID			= $arrHeader['location'];
	$year				= $arrHeader['year'];
	$itemId				= $arrHeader['itemId'];
	$editMode			= false;
	
	$savedStatus		= true;
	$savedMasseged 		= '';
	$error_sql			= '';
	
	$validateArr		= validateBeforeSave($locationID,$year,$itemId,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	$header_arr			= $obj_amounts_and_dates_get->get_header($locationID,$itemId,$year,'RunQuery2');
	if($header_arr['LOCATION_ID']=='')
	{
		$approveLevels 		= $obj_common->getApproveLevels($programName);
		$status				= $approveLevels+1;
		
		$resultHArr			= $obj_amounts_and_dates_set->saveHeader($locationID,$year,$itemId,$userId,$status,$approveLevels);
		if($resultHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultHArr['savedMassege'];
			$error_sql		= $resultHArr['error_sql'];	
		}
	}
	else
	{
		$approveLevels 		= $obj_common->getApproveLevels($programName);
		$status				= $approveLevels+1;
		
		$resultUHArr		= $obj_amounts_and_dates_set->updateHeader($locationID,$year,$itemId,$userId,$status,$approveLevels);
		if($resultUHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultUHArr['savedMassege'];
			$error_sql		= $resultUHArr['error_sql'];	
		}
		$resultUMArr		= updateMaxStatus($locationID,$year,$itemId);
		if($resultUMArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultUMArr['savedMassege'];
			$error_sql		= $resultUMArr['error_sql'];
		}
		$resultDDArr		= $obj_amounts_and_dates_set->deleteData($locationID,$year,$itemId);
		if($resultDDArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultDDArr['savedMassege'];
			$error_sql		= $resultDDArr['error_sql'];	
		}
		$editMode			= true;
	}
	foreach($arrDetails as $array_loop)
	{
		$date				= $array_loop['date'];
		$date_year			= date("Y",strtotime($date));
		$amount				= ($array_loop['amount']==''?0:$array_loop['amount']);
		
		$base_curr_res		= $obj_common->loadCompany_result($company,'RunQuery2');
		$row				= mysqli_fetch_array($base_curr_res);
		$base_currency		= $row['intBaseCurrencyId'];
		
		$used_amount		= $obj_cf_common->get_cf_used_invoice_amounts($location,$itemId,$base_currency,$date,$company,'RunQuery2');	
		$checkAvailable		= $obj_amounts_and_dates_get->checkDetailAvailable($locationID,$year,$itemId,$date);
		
		if($date_year != $year && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= "Please enter dates according to selected year $year.";	
		}
		if($amount < $used_amount && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= "amount should greater than previous amount.<br> Date : $date <br> previous Amount : $used_amount ";	
		}
		if($checkAvailable)
		{
			$resultDArr			= $obj_amounts_and_dates_set->updateDetails($locationID,$year,$itemId,$date,$amount);
		}
		else
		{
			$resultDArr			= $obj_amounts_and_dates_set->saveDetails($locationID,$year,$itemId,$date,$amount);
		}
		
		if($resultDArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultDArr['savedMassege'];
			$error_sql		= $resultDArr['error_sql'];	
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode)
			$response['msg'] 		= "Updated Successfully.";
		else
			$response['msg'] 		= "Saved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= $savedMasseged;
		$response['sql']			= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='loadData')
{
	$locationID			= $_REQUEST['location'];
	$year				= $_REQUEST['year'];
	$itemId				= $_REQUEST['itemId'];
	$html				= '';
	$count				= 2;
	$chkStatus			= true;
	$detailResult		= $obj_amounts_and_dates_get->get_details($locationID,$itemId,$year,'RunQuery');
	while($row = mysqli_fetch_array($detailResult))
	{
		
		$base_curr_res			= $obj_common->loadCompany_result($company,'RunQuery');
		$row_bc					= mysqli_fetch_array($base_curr_res);
		$base_currency			= $row_bc['intBaseCurrencyId'];
		$used_amount			= $obj_cf_common->get_cf_used_invoice_amounts($locationID,$itemId,$base_currency,$row['DATE'],$company,'RunQuery');
		$chkStatus		= false;
		$html .= "<tr class=\"cls_tr_firstRow\">";
		$html .= "<td style=\"text-align:center\" class=\"clsDelete\"><img src=\"images/del.png\" class=\"removeRow mouseover\"/></td>";
		if($used_amount > 0)
		$html .= "<td style=\"text-align:center\"><input name=\"txtDate\" disabled=\"disabled\"  type=\"text\" value=\"".$row['DATE']."\" class=\"clsDate\" id=\"txtDate~$count\" style=\"width:100px;\" onKeyPress=\"return ControlableKeyAccess(event);\"  onclick=\"return showCalendar(this.id, '%Y-%m-%d');\"/><input type=\"reset\" value=\"\"  class=\"txtbox\" style=\"visibility:hidden;\"  onclick=\"return showCalendar(this.id, '%Y-%m-%');\" /></td>";
		else
		$html .= "<td style=\"text-align:center\"><input name=\"txtDate\" type=\"text\" value=\"".$row['DATE']."\" class=\"clsDate\" id=\"txtDate~$count\" style=\"width:100px;\" onKeyPress=\"return ControlableKeyAccess(event);\"  onclick=\"return showCalendar(this.id, '%Y-%m-%d');\"/><input type=\"reset\" value=\"\"  class=\"txtbox\" style=\"visibility:hidden;\"  onclick=\"return showCalendar(this.id, '%Y-%m-%');\" /></td>";
		$html .= "<td style=\"text-align:center\"><input name=\"txtAmount\" type=\"text\" id=\"txtAmount\" style=\"width:150px;text-align:right\" class=\"clsAmount validate[custom[number], min[$used_amount]]\" value=\"".$row['AMOUNT']."\"/></td>";
		$html .= "</tr>";
			
			$count++;			
	}
	if($chkStatus)
	{
		$html .= "<tr class=\"cls_tr_firstRow\">";
		$html .= "<td style=\"text-align:center\" class=\"clsDelete\"><img src=\"images/del.png\" class=\"removeRow mouseover\"/></td>";
		$html .= "<td style=\"text-align:center\"><input name=\"txtDate\" type=\"text\" value=\"".date('Y-m-d')."\" class=\"clsDate\" id=\"txtDate~ $count;\" style=\"width:100px;\" onKeyPress=\"return ControlableKeyAccess(event);\"  onclick=\"return showCalendar(this.id, '%Y-%m-%d');\"/><input type=\"reset\" value=\"\"  class=\"txtbox\" style=\"visibility:hidden;\"  onclick=\"return showCalendar(this.id, '%Y-%m-%');\" /></td>";
		$html .= "<td style=\"text-align:center\"><input name=\"txtAmount\" type=\"text\" id=\"txtAmount\" style=\"width:150px;text-align:right\" class=\"clsAmount validate[custom[number]]\" value=\"\"/></td>";
		$html .="</tr>";              
	}
	
	$response['html'] = $html ;
	echo json_encode($response);
}
function validateBeforeSave($locationID,$year,$itemId,$programCode,$userId)
{
	global $obj_commonErr;
	global $obj_amounts_and_dates_get;
	$checkStatus	= true;

	$header_arr		= $obj_amounts_and_dates_get->get_header($locationID,$itemId,$year,'RunQuery2');
	$validateArr 	= $obj_commonErr->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['LEVELS'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}
function updateMaxStatus($locationID,$year,$itemId)
{
	global $obj_amounts_and_dates_get;
	global $obj_amounts_and_dates_set;
	
	$maxStatus	= $obj_amounts_and_dates_get->getMaxStatus($locationID,$year,$itemId);
	$resultArr	= $obj_amounts_and_dates_set->updateApproveByStatus($locationID,$year,$itemId,$maxStatus);
	
	return $resultArr;
}
	
?>
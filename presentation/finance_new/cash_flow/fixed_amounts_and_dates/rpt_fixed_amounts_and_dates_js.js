// JavaScript Document
var basePath	= "presentation/finance_new/cash_flow/fixed_amounts_and_dates/";
$(document).ready(function(){
	
	$('#frmRptFixedDatesAndAmounts #butRptConfirm').die('click').live('click',urlApprove);
	$('#frmRptFixedDatesAndAmounts #butRptReject').die('click').live('click',urlReject);
	$('#frmRptFixedDatesAndAmounts #butRptRevise').die('click').live('click',urlRevise);	
});

function urlApprove()
{

	var val = $.prompt('Are you sure you want to approve this Fixed amounts ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = basePath+"fixed_amounts_and_dates_db.php"+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptFixedDatesAndAmounts #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptFixedDatesAndAmounts #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function urlReject()
{
	var val = $.prompt('Are you sure you want to reject this Fixed amounts ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = basePath+"fixed_amounts_and_dates_db.php"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptFixedDatesAndAmounts #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx1()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptFixedDatesAndAmounts #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx1()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function urlRevise()
{
	var val = $.prompt('Are you sure you want to revise this Fixed amounts ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = basePath+"fixed_amounts_and_dates_db.php"+window.location.search+'&requestType=revise';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptFixedDatesAndAmounts #butRptRevise').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx1()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptFixedDatesAndAmounts #butRptRevise').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx1()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}

function alertx1()
{
	$('#frmRptFixedDatesAndAmounts #butRptConfirm').validationEngine('hide')	;
	$('#frmRptFixedDatesAndAmounts #butRptReject').validationEngine('hide')	;
	$('#frmRptFixedDatesAndAmounts #butRptRevise').validationEngine('hide')	;
}


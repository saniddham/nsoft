<?php
	session_start();
	ini_set('display_errors',0);
	$backwardseperator 		= "../../../";
	$mainPath 				= $_SESSION['mainPath'];
	$userId 				= $_SESSION['userId'];
	$location 				= $_SESSION['CompanyID'];
	$company 				= $_SESSION['headCompanyId'];
	$HR_DB 					= $_SESSION['HRDatabase'];
	$main_DB 				= $_SESSION['Database'];

	include_once "{$backwardseperator}dataAccess/Connector.php";
 	require_once "../../../class/finance/cash_flow/cls_cf_common.php";
 	require_once "../../../class/finance/cash_flow/cls_cf_common_set.php";
  	
	$obj_cf_common			= new cls_cf_common($db);
	$obj_cf_common_set		= new cls_cf_common_set($db);

 	//---------------------------
	$arr_records	= NULL;
	$arr_amount		= NULL;
	$arr_currency	= NULL;
	$arr_rcv_date	= NULL;
	$records		= 0;
	$currunt_date	= date('Y-m-d');
	//$currunt_date	= '2014-06-01';
	$date_from 		= Date("Y-m-d", strtotime($currunt_date." -1 Month -0 Day"));
	//-----------------------------------------------
	$res_field = $obj_cf_common->get_previous_type_cf_ids_result('RunQuery');
	while($row_field=mysqli_fetch_array($res_field)){
		$rpt_field	= $row_field['REPORT_FIELD_ID'];

		//-----------------------------------------------
		$res_rcv = $obj_cf_common->get_cf_receive_dates_range_result($location,$date_from,$currunt_date,$rpt_field,'RunQuery');
		while($row_rcv=mysqli_fetch_array($res_rcv)){
			$rcv_date1	= $row_rcv['RECEIVE_DATE'];
			$rcv_date	= Date("Y-m-d", strtotime($rcv_date1." +1 Month +0 Day"));
			
			//-----------------------------------------------
			$res_log = $obj_cf_common->get_cf_log_invoice_for_dates_range_result($location,$rpt_field,$rcv_date1,'RunQuery');
			while($row_log=mysqli_fetch_array($res_log)){
					//echo $rpt_field."-";
					$field[$records]	= $rpt_field;
					$log_date[$records]	= Date("Y-m-d", strtotime($row_log['RECEIVE_DATE']." +1 Month +0 Day"));
					$saveAmnt[$records]	= $row_log['AMOUNT'];
					$currency[$records]	= $row_log['CURRENCY_ID'];
					$records++;
			}

		}

	}
	//echo $records;		
	
	for($r=0; $r < $records; $r++){
 		$obj_cf_common_set->insert_to_log($location,$field[$r],$rcv_date,$log_date[$r],0,0,'OPENING_PREVIOUS',$saveAmnt[$r],$currency[$r],'RunQuery');
	}
			
		

?>
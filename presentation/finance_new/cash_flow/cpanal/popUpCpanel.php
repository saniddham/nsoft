<?php
session_start();
$backwardseperator = "../../../../";

include  "{$backwardseperator}dataAccess/Connector.php";
include "../../../../class/finance/cash_flow/cpanal/cls_cpanal_get.php";

$companyId 			= $_SESSION["headCompanyId"];
$obj_cpanal_get		= new cls_cpanal_get($db);
$locationResult		= $obj_cpanal_get->getLocation($companyId,'RunQuery');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>C panel popup</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />
<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
</head>

<body>
<form id="frmPopUp" name="frmPopUp" method="post" action="">
  <div align="center">
    <div class="trans_layoutS" align="center" style="width:350px">
      <div class="trans_text"> Locations</div>
      <table width="100%" border="0" >
        <tr class="normalfnt">
        	<td><table width="350" class="bordered" id="tblLocationPopup">
                    <thead>
                      <tr>
                      <th width="24%"><input type="checkbox" id="chkCheckAll" name="chkCheckAll" /></th>
                      <th width="76%">Location</th>
                      </tr>
                    </thead>
                    <tbody>
                   
                    <?php
					while($row	= mysqli_fetch_array($locationResult))
					{
					?>
                     <tr id="<?php echo $row['intId'] ?>">
                    <td style="text-align:center"><input type="checkbox" id="chkLocation" class="chkLocation" name="chkLocation" /></td>
                    <td style="text-align:left"><?php echo $row['strName'] ?></td>
                    </tr>
                    <?php
					}?>
                    </tbody>
              	</table></td>
            </tr>
            <tr>
          <td width="100%" align="center" ><a class="button white medium" id="butSave" name="butSave">Save</a><a  class="button white medium" id="butClosePop" name="butClosePop">Close</a></td>
        </tr>
        </table>
     </div>
   </div>  
</form>
</body>
</html>
<?php
session_start();
 date_default_timezone_set('Asia/Colombo');
ini_set('max_execution_time',60000);
ini_set('display_errors',0);
//BEGIN - INCLUDE FILES {
include_once "../../../../dataAccess/DBManager2.php";
include_once "../../../../class/cls_commonFunctions_get.php";
include_once "../../../../class/finance/petty_cash/petty_cash_book/cls_petty_cash_get.php";
include_once "../../../../class/finance/cash_flow/mini_cash_flow_report/cls_mini_cash_flow_report_get.php";
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {
$db 							=  new DBManager2();
$db->connect();
$obj_commonFunctions_get		= new cls_commonFunctions_get($db);
$obj_petty_cash_get				= new cls_petty_cash_get($db);
$obj_mini_cash_flow_report_get	= new cls_mini_cash_flow_report_get($db);
//END 	- CREATE OBJECTS }

//BEGIN - SET PARAMETERS {
$fromDate		= $_REQUEST["FromDate"];
$toDate			= $_REQUEST["ToDate"];
$location		= $_REQUEST["Location"];
$dateArray		= $obj_commonFunctions_get->getDatesBetweenTwoDateRange($fromDate,$toDate);
$monthArray2	= $obj_commonFunctions_get->getMonthBetweenTwoDateRange($fromDate,$toDate);
//END 	- SET PARAMETERS }

$i = 0 ;
foreach($monthArray2 as $monthArray1)
{
	$monthArray[$i]['Y-m'] 	 = date('Y-m',strtotime($monthArray1)).'-01-OPEN';
	$monthArray[$i]['Y'] 	 = date('Y',strtotime($monthArray1));
	$monthArray[$i]['m'] 	 = date('m',strtotime($monthArray1));
	$monthArray[$i]['Y-m-d'] = date('Y-m-01',strtotime($monthArray1));
	$monthArray[$i]['DAYS']	 = cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($monthArray1)),date('Y',strtotime($monthArray1)));
	$i++;
}

//BEGIN - CREATE DAY WISE ARRAY {
$i = 0 ;
$booFirst	= false;
$array		= array();
foreach($dateArray as $row)
{	
	/*if(!$booFirst || $test!=date('Y-m',strtotime($row)))
	{
		$booFirst	= true;
		$array[$i]['Y-m-d']	= date('Y-m-d',strtotime($row)).'-OPEN';	
		$array[$i]['Y-F']	= 'OPEN';
		$array[$i]['d-D']	= 'OPEN';
		$array[$i]['Y']		= date('Y',strtotime($row));
		$array[$i]['m']		= date('m',strtotime($row));	
		$test				= date('Y-m',strtotime($row));
		$i++;
	}*/
		$array[$i]['Y-m-d']	= date('Y-m-d',strtotime($row));	
		$array[$i]['Y-F']	= date('Y-F',strtotime($row));
		$array[$i]['d-D']	= date('d-D',strtotime($row));
		$array[$i]['Y']		= date('Y',strtotime($row));
		$array[$i]['m']		= date('m',strtotime($row));
	$i++;
}
//END 	- CREATE DAY WISE ARRAY }

//BEGIN - MAIN QUERY {			
$result		= $obj_mini_cash_flow_report_get->getCpanalData($location);
//END 	- MAIN QUERY }	
	$rowCount	 = mysqli_num_rows($result);
	$arrayTotal["total"] = $rowCount;
	$data		 = array();
	$booFirst	 = false;
	$detailArray = array(); 
//BEGIN - NORMAL ITEMS {
	$result12	= $obj_mini_cash_flow_report_get->getCpanalData($location);
    while($row = mysqli_fetch_array($result12))
	{
		$detailArray[$row['REPORT_FIELD_ID']]['ItemId'] 		= $row['REPORT_FIELD_ID'];
		$detailArray[$row['REPORT_FIELD_ID']]['ItemName'] 		= $row['REPORT_FIELD_NAME'];

		foreach($array as $row1)
		{
			$amount 					= calculateAmount($row["BOO_PO_RAISED"],$row1["Y-m-d"],$row["REPORT_FIELD_ID"],$row["REPORT_FIELD_CODE"]);
			$total1[$row1["Y-m-d"]]     += $amount;
			$detailArray[$row['REPORT_FIELD_ID']]['Date'][$row1["Y-m-d"]] = $amount;
			$detailArray[$row['REPORT_FIELD_ID']]['Color'][$row1["Y-m-d"]] = $color;
		}
    }
//END	- NORMAL ITEMS }
//die(print_r($detailArray));
//BEGIN - BANK BALANCE 		{		
		$data["itemName"] 				= 'Bank Balance';
		$data["itemId"] 				= '-1';
		$bankBalance					= getBankBalance($fromDate);

		foreach($array as $row1)
		{						
			$data[$row1["Y-m-d"]][0] 	= ($bankBalance==''?'':number_format($bankBalance));
			$bankBalance               -= $total1[$row1["Y-m-d"]];
		}
		$a[] 							= $data;
//END 	- BANK BALANCE 		}

//BEGIN - NORMAL ITEMS {
/*    while($row = mysqli_fetch_array($result))
	{
		$data["itemName"] 				= $row['REPORT_FIELD_NAME'];
		$data["itemId"] 				= $row['REPORT_FIELD_ID'];
		
		foreach($array as $row1)
		{
			$amount						= $detailArray[$data["itemId"]][$row1["Y-m-d"]];
			//$amount 					= calculateAmount($row["BOO_PO_RAISED"],$row1["Y-m-d"],$row["REPORT_FIELD_ID"],$row["REPORT_FIELD_CODE"]);
			$data[$row1["Y-m-d"]][0] 	= ($amount=='' || $amount=='0'?'':number_format($amount));
			$total[$row1["Y-m-d"]]     += $amount;
			$data[$row1["Y-m-d"]][1]	= $color;
		}
		$a[] 							= $data;
    }*/
	
	foreach($detailArray as $row)
	{
		//die(print_r($row));
		$data["itemId"]					= $row['ItemId'];
		$data["itemName"]				= $row['ItemName'];
		foreach($array as $row1)
		{
			$amount						= $row['Date'][$row1["Y-m-d"]];
			$data[$row1["Y-m-d"]][0]	= ($amount=='' || $amount=='0'?'':number_format($amount));
			$total[$row1["Y-m-d"]]     += $amount;
			$data[$row1["Y-m-d"]][1]	= $row['Color'][$row1["Y-m-d"]];
		}
		$a[] 							= $data;
	}
//END	- NORMAL ITEMS }

//BEGIN - EMPTY ROW 		{
		$data["itemName"] 				= '';
		$data["itemId"] 				= '-1';
		foreach($array as $row1)
		{			
			$data[$row1["Y-m-d"]] 		= '';
		}
		$a[] 							= $data;
//END	- EMPTY ROW 		}
	
//BEGIN - TOTAL DEDUCTION 	{
		$data["itemName"] 				= 'Total Deductions';
		$data["itemId"] 				= '-1';		
		
		foreach($array as $row1)
		{			
			$data[$row1["Y-m-d"]] 		= ($total[$row1["Y-m-d"]]==''?'':number_format($total[$row1["Y-m-d"]]));
		}
		$a[] 							= $data;
//END 	- TOTAL DEDUCTION 	}
	

	
//END 	- BALANCE AMOUNT 	{		
		$data["itemName"] 				= 'Balance Amount';
		$bankBalance					= getBankBalance($fromDate);
		foreach($array as $row1)
		{	
			$bankBalance				-= $total[$row1["Y-m-d"]];
			$data[$row1["Y-m-d"]] 		= ($bankBalance==''?'':number_format($bankBalance));			
		}
		$a[] 							= $data;
//END 	- BALANCE AMOUNT 	}	
		
		$arrayTotal["rows"]	= $a;
    echo json_encode($arrayTotal);

	
/*function calculateAmount1($poraised,$date,$id,$code)
{
	global $location;
	global $obj_petty_cash_get;
	global $obj_mini_cash_flow_report_get;
	global $color;
	$color		= 'black';
	
	if($code=='PETTY_CASH')
	{
		$color		= 'blue';
		$amount 	= $obj_petty_cash_get->getPettyCashExpences($date,$location,'RunQuery');
		return $amount;
		continue;
	}
		
	$amount = 0;
	$amount1 = 0;
		
	$result = $obj_mini_cash_flow_report_get->getFinanceCashFlow($date,$id,$location);
	while($row = mysqli_fetch_array($result))
	{
		if($code=='FOOD' && $row["RECEIVE_DATE"]==$date)
		{
			
			$result1	= getDates($date,$id,$location);
			$booFirst	= true;
			$amount		= 0;
			while($row1 = mysqli_fetch_array($result1))
			{
				
					if($booFirst){
						$dateA		= explode('-',$row1["RECEIVE_DATE"]);
						$from		= $dateA[0].'-'.$dateA[1].'-01';
						$to			= $row1["RECEIVE_DATE"];
						$booFirst	= false;
					}else{
						$d			= $dateA[2]+1;
						$from		= $dateA[0].'-'.$dateA[1].'-'.$d;
						$to			= $row1["RECEIVE_DATE"];
					}
				
				if($row1["RECEIVE_DATE"]==$date)
				{
					$cashBalance 	= getMealExpences($from,$to,$location,'RunQuery');
					$amount			+= $cashBalance;
				}
			}
			
			return $amount;
			continue;
		}		
		
		
		$result1 = $obj_mini_cash_flow_report_get->getFinanceCashFlowLog($row["RECEIVE_DATE"],$id,$date,$location);
		while($row1 	= mysqli_fetch_array($result1))
		{
			if($row1["DOCUMENT_TYPE"]=='OPENING_FIXED'){
				$amount1	+= $row1["AMOUNT"];
				$color		= 'green';
			}
			if($row1["DOCUMENT_TYPE"]=='OPENING_PREVIOUS'){
				$amount1	+= $row1["AMOUNT"];
				$color		= 'green';
			}
			elseif($row1["DOCUMENT_TYPE"]=='OPENING_FIXED_REVISE'){
				$amount1	-= $row1["AMOUNT"];
				$amount1	= abs($amount1);
				$color		= 'green';
			}
			elseif($row1["DOCUMENT_TYPE"]=='INVOICE'){
				$amount1	-= $row1["AMOUNT"];
				$amount1	= abs($amount1);
				$color		= 'blue';
				$invAvailable = true;
			}
			elseif($row1["DOCUMENT_TYPE"]=='BILLINVOICE'){
				$amount1	-= $row1["AMOUNT"];
				$amount1	= abs($amount1);
				$color		= 'blue';
				$invAvailable = true;
			}
			elseif(!$invAvailable && $row1["DOCUMENT_TYPE"]=='BILL_PAYMENT'){
				$amount1	-= $row1["AMOUNT"];
				$amount1	= abs($amount1);
				$color		= 'red';
			}			
			elseif($row1["DOCUMENT_TYPE"]=='JOURNAL_ENTRY'){
				$amount1	-= $row1["AMOUNT"];
				$amount1	= abs($amount1);
				$color		= 'blue';
				$invAvailable = true;
			}
			elseif(!$invAvailable && $row1["DOCUMENT_TYPE"]=='BANK_PAYMENT'){
				$amount1	-= $row1["AMOUNT"];
				$amount1	= abs($amount1);
				$color		= 'red';
			}			
			elseif(!$invAvailable && $row1["DOCUMENT_TYPE"]=='SUPPLIER_PAYMENT'){
				$amount1	-= $row1["AMOUNT"];
				$amount1	= abs($amount1);
				$color		= 'red';
			}
			elseif($row1["DOCUMENT_TYPE"]=='ADV_PAY_SETTLE'){
				$amount1	-= $row1["AMOUNT"];
				$amount1	= abs($amount1);
				$color		= 'red';
			}			
		}	
		$amount	+= $amount1;
		$amount1 = 0;	
	}
	
	$payment 	= $obj_mini_cash_flow_report_get->getPayment($date,$id,$location);
	if($payment>0)
		$color		= 'red';
	$amount	   += $payment;
	return ($amount==''?'':abs($amount));
}*/

function calculateAmount($poraised,$date,$id,$code)
{
	global $obj_mini_cash_flow_report_get;
	global $obj_petty_cash_get;
	global $location;
	global $color;
	
	if($code=='PETTY_CASH')
	{
		$color		= 'blue';
		$amount 	= $obj_petty_cash_get->getPettyCashExpences($date,$location,'RunQuery');
		return $amount;
		continue;
	}
	
	$result = $obj_mini_cash_flow_report_get->getFinanceCashFlow($date,$id,$location);
	while($row = mysqli_fetch_array($result))
	{
		if($code=='FOOD' && $row["RECEIVE_DATE"]==$date)
		{			
			$result1	= getDates($date,$id,$location);
			$booFirst	= true;
			$amount		= 0;
			while($row1 = mysqli_fetch_array($result1))
			{
				
					if($booFirst){
						$dateA		= explode('-',$row1["RECEIVE_DATE"]);
						$from		= $dateA[0].'-'.$dateA[1].'-01';
						$to			= $row1["RECEIVE_DATE"];
						$booFirst	= false;
					}else{
						$d			= $dateA[2]+1;
						$from		= $dateA[0].'-'.$dateA[1].'-'.$d;
						$to			= $row1["RECEIVE_DATE"];
					}
				
				if($row1["RECEIVE_DATE"]==$date)
				{
					$cashBalance 	= getMealExpences($from,$to,$location,'RunQuery');
					$amount			+= $cashBalance;
				}
			}
			
			return $amount;
			continue;
		}	
		
		if($obj_mini_cash_flow_report_get->isDataAvailable($date,$row["RECEIVE_DATE"],$id,$location,"'OPENING_FIXED','OPENING_PREVIOUS'")){
			$foreCastBalance	+= getForecastBalance($row["RECEIVE_DATE"],$id,$date,$location);
			$color				 = 'green';
		}else
			$foreCastBalance	+= 0;
			
		if($obj_mini_cash_flow_report_get->isDataAvailable($date,$row["RECEIVE_DATE"],$id,$location,"'BILLINVOICE','INVOICE','JOURNAL_ENTRY'")){
			$invoiceBalance	    += getInvoiceBalance($row["RECEIVE_DATE"],$id,$date,$location);
			$color				 = 'blue';
		}else
			$invoiceBalance	    += 0;
		
	}
	$paymentAmount			+= $obj_mini_cash_flow_report_get->getPayment($date,$id,$location);
	if($paymentAmount>0)
		$color				 = 'red';
	$amount	= $foreCastBalance + $invoiceBalance + $paymentAmount;
	return $amount;
}

function getMonthlyIncome($date)
{
	global $db;
	global $location;
	
	$date = explode('-',$date);
	
	if($location != "")
			$para = "AND I.LOCATION_ID = '$location' ";
			
	$sql = "SELECT
			  COALESCE(SUM(AMOUNT),0)	AS AMOUNT
			FROM finance_pettycash_invoice I
			  INNER JOIN finance_mst_pettycash_item PI
				ON PI.ID = I.PETTY_CASH_ITEM
			WHERE PI.CODE = 'PCR'
				AND YEAR(I.DATE) = '$date[0]'
				AND MONTH(I.DATE) = '$date[1]'
				$para ";
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}

function getBankBalance($date)
{
	global $db;
	global $location;
	global $fromDate;
	global $obj_commonFunctions_get;	
	global $obj_mini_cash_flow_report_get;
	$sql11 = "SELECT 
				COALESCE(BANK_BALANCE,0)	AS BANK_BALANCE,
				BANK_BALANCE_DATE			AS BANK_BALANCE_DATE
			FROM mst_locations 
			WHERE intId = $location ;";
	$resultb = $db->RunQuery($sql11);
	$rowb	= mysqli_fetch_array($resultb);
	$dateArray		= $obj_commonFunctions_get->getDatesBetweenTwoDateRange($rowb["BANK_BALANCE_DATE"],date('Y-m-d',(strtotime('-1 day',strtotime($fromDate)))));
	$result		= $obj_mini_cash_flow_report_get->getCpanalData($location);
	$bankBalance	= $rowb["BANK_BALANCE"];
	while($rowc = mysqli_fetch_array($result))
	{
		foreach($dateArray as $dateArray1)
		{
			$amount 					= calculateAmount($rowc["BOO_PO_RAISED"],$dateArray1,$rowc["REPORT_FIELD_ID"],$rowc["REPORT_FIELD_CODE"]);
			$bankBalance				-= $amount;
		}
	}
	return $bankBalance;
}

function getMealExpences($from,$to,$location)
{
	global $db;
	
	$sql = "SELECT
			  COUNT(*) * 30 AS MEAL_COUNT
			FROM qpay.trn_dataupload_meal DU
			WHERE DATE(DU.dtTime)BETWEEN 
					('$from')
				AND ('$to')
				AND TIME(DU.dtTime)BETWEEN TIME('10:59:00')
				AND TIME('14:31:00');";// echo $sql;
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	return $row["MEAL_COUNT"];
}

function getDates($date,$id,$location)
{
	global $db;
	
	$sql = "SELECT 
				RECEIVE_DATE
			FROM finance_cashflow_date_amount D
			INNER JOIN finance_cashflow_date_amount_header H
			ON H.LOCATION_ID = D.LOCATION_ID
			AND H.REPORT_FIELD_ID = D.REPORT_FIELD_ID
			AND H.YEAR = D.YEAR
			WHERE H.LOCATION_ID = $location
			AND H.REPORT_FIELD_ID = $id
			-- AND D.RECEIVE_DATE = '$date';"; //echo $sql;
	return $db->RunQuery($sql);
}

function getForecastBalance($receiveDate,$id,$date,$location)
{
	global $obj_mini_cash_flow_report_get;
	
	return $obj_mini_cash_flow_report_get->getForcastBalance($receiveDate,$id,$location);	
}

function getInvoiceBalance($receiveDate,$id,$date,$location)
{
	global $obj_mini_cash_flow_report_get;
	
	$result = $obj_mini_cash_flow_report_get->getInvoiceDate($date,$receiveDate,$id,$location);
	while($rowA = mysqli_fetch_array($result))
	{
		$invoice_amount = $rowA['AMOUNT'];
		$payment_amount = $obj_mini_cash_flow_report_get->getPaymentAmount($rowA['INVOICE_DATE'],$id,$location);
		$amount        += $invoice_amount - $payment_amount;
	}
	return $amount;
}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/finance/cash_flow/mini_cash_flow_report/cls_mini_cash_flow_report_get.php";
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {
$obj_mini_cash_flow_report_get	= new cls_mini_cash_flow_report_get($db);
//END 	- CREATE OBJECTS {
	
//BEGIN - CREATE PARAMETERS {
$location						= $_SESSION['CompanyID'];
$id								= $_REQUEST['Id'];
$date							= $_REQUEST['Date'];
$dateArray						= explode('-',$date);
//END	- CREATE PARAMETERS }

//BEGIN - REDIRECT TO PETTY CASH REPORT {
if($id==19){
	$_REQUEST["txtDateFrom"]	= $dateArray[0].'-'.$dateArray[1].'-01';
	$_REQUEST["txtDateTo"]		= $dateArray[0].'-'.$dateArray[1].'-'.cal_days_in_month(CAL_GREGORIAN,$dateArray[1],$dateArray[0]);
	$_REQUEST["cboLocation"]	= $location;
	include_once("presentation/finance_new/petty_cash/petty_cash_book/petty_cash.php");	
	exit;
}
//END 	- REDIRECT TO PETTY CASH REPORT }
?>
<head>
<title>Cash Flow Log</title>
</head>
<body>
<form id="frmMain" name="frmMain">
  <table width="500" border="0" cellspacing="0" cellpadding="0" align="center" >
    <tr>
      <td><table width="100%" border="0" cellspacing="2" cellpadding="0">
          <tr>
            <td colspan="3" align="center" class="reportHeader">Cash Flow Detail Log</td>
          </tr>
          <tr>
            <td width="14%" class="normalfnt">Item </td>
            <td width="2%">:</td>
            <td width="84%" class="normalfnt"><?php $itemArray = $obj_mini_cash_flow_report_get->getCashFlowItemName($id); echo $itemArray['REPORT_FIELD_NAME'];?></td>
          </tr>
          <tr>
            <td class="normalfnt">Date</td>
            <td>:</td>
            <td class="normalfnt"><?php echo $date?></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td align="center"><table width="500" border="0" cellspacing="0" cellpadding="0"  class="rptBordered">
          <thead>
            <tr>
              <th width="127">Type</th>
              <th width="152">Receive Date</th>
              <th width="133">LOG Date</th>
              <th width="86">Amount</th>
            </tr>
          </thead>
          <tbody>
            <?php 
  $amount 	= 0;
  $amount1 	= 0;
  $result = $obj_mini_cash_flow_report_get->getFinanceCashFlow($date,$id,$location);
  while($row = mysqli_fetch_array($result))
  {
	  if($obj_mini_cash_flow_report_get->isDataAvailable($date,$row["RECEIVE_DATE"],$id,$location,"'OPENING_FIXED','OPENING_PREVIOUS'"))
	  {
		  $result1 = $obj_mini_cash_flow_report_get->getForcastBalanceDetails($row["RECEIVE_DATE"],$id,$date,$location);
		  while($row1 = mysqli_fetch_array($result1))
		  {
			  $forcastAvailabel = true
			  ?>
            <tr>
              <td><?php echo $row1["DOCUMENT_TYPE"]?></td>
              <td style="text-align:center"><?php echo $row1["RECEIVE_DATE"]?></td>
              <td style="text-align:center"><?php echo date('Y-m-d',strtotime($row1["LOG_DATE"]))?></td>
              <td style="text-align:right"><?php echo number_format($row1["AMOUNT"],2)?></td>
            </tr>
            <?php
			 $amount	+= round($row1["AMOUNT"],2);
		  }
	  }
	  if($forcastAvailabel)
	  {
		?>
            <tr>
              <td colspan="3"><b>Receive Date Wise Forecast Balance [<?php echo $row["RECEIVE_DATE"]?>]</b></td>
              <td style="text-align:right"><b><?php echo number_format($amount,2)?></b></td>
            </tr>
            <tr>
              <td colspan="4">&nbsp;</td>
            </tr>
            <?php
	  }
	  
	  if($obj_mini_cash_flow_report_get->isDataAvailable($date,$row["RECEIVE_DATE"],$id,$location,"'BILLINVOICE','INVOICE','JOURNAL_ENTRY'"))
	  {
		  	$result1 = $obj_mini_cash_flow_report_get->getInvoiceDate($date,$row["RECEIVE_DATE"],$id,$location);
			while($row1 = mysqli_fetch_array($result1))
			{
				$invoiceAvailabel = true;
				$invoice_amount += $row1['AMOUNT'];
				?>
            <tr>
              <td><?php echo $row1["DOCUMENT_TYPE"]?></td>
              <td style="text-align:center"><?php echo $row1["RECEIVE_DATE"]?></td>
              <td style="text-align:center"><?php echo date('Y-m-d',strtotime($row1["LOG_DATE"]))?></td>
              <td style="text-align:right"><?php echo number_format($row1["AMOUNT"],2)?></td>
            </tr>
            <?php
			$result2 = $obj_mini_cash_flow_report_get->getInvoicePaymentDetails($row1['INVOICE_DATE'],$id,$location);
			while($row2 = mysqli_fetch_array($result2))
			{
				$payment_amount += $row2['AMOUNT'];
				?>
            <tr>
              <td><?php echo $row2["DOCUMENT_TYPE"]?></td>
              <td style="text-align:center"><?php echo $row2["RECEIVE_DATE"]?></td>
              <td style="text-align:center"><?php echo date('Y-m-d',strtotime($row2["LOG_DATE"]))?></td>
              <td style="text-align:right">-<?php echo number_format($row2["AMOUNT"],2)?></td>
            </tr>
            <?php
			}
				$invoBalance += $invoice_amount - $payment_amount;
				$invoice_amount = 0;
				$payment_amount = 0;
			}
			if($invoiceAvailabel)
		  {
			?>
            <tr>
              <td colspan="3"><b>Invoice Balance [<?php echo $row["RECEIVE_DATE"]?>]</b></td>
              <td style="text-align:right"><b><?php echo number_format($invoBalance,2)?></b></td>
            </tr>
            <tr>
              <td colspan="4">&nbsp;</td>
            </tr>
            <?php
		  }			
	  }
		$totAmount	+= $amount + $invoBalance;
		$invoBalance = 0;
		$amount  	= 0;
	  } 
	  ?>
            <?php 
	  $result = $obj_mini_cash_flow_report_get->getPaymentDetails($date,$id,$location);
	  while($row = mysqli_fetch_array($result)){
		 $paymentAvailable = true; 
		 ?>
            <tr>
              <td><?php echo $row["DOCUMENT_TYPE"]?></td>
              <td style="text-align:center"><?php echo $row["RECEIVE_DATE"]?></td>
              <td style="text-align:center"><?php echo date('Y-m-d',strtotime($row["LOG_DATE"]))?></td>
              <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
            </tr>
            <?php 
	  		$amount	+= round($row["AMOUNT"],2);
		} 
		?>
            <?php if($paymentAvailable){?>
            <tr>
              <td colspan="3"><b>Total Payments</b></td>
              <td style="text-align:right"><b><?php echo number_format($amount,2)?></b></td>
            </tr>
            <?php }?>
            <tr>
              <td colspan="3"><b>Cash Flow Balance</b></td>
              <td style="text-align:right"><b><?php echo number_format($amount + $totAmount,2)?></b></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </table>
</form>
</body>
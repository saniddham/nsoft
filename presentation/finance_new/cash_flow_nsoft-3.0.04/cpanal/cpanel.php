<?php
	ini_set('max_execution_time', 11111111) ;
	session_start();
	$backwardseperator 		= "../../../../";
	$mainPath 				= $_SESSION['mainPath'];
	$userId 				= $_SESSION['userId'];
	$location 				= $_SESSION['CompanyID'];
	$company 				= $_SESSION['headCompanyId'];
	$HR_DB 					= $_SESSION['HRDatabase'];
	$main_DB 				= $_SESSION['Database'];

	include_once "{$backwardseperator}dataAccess/Connector.php";
 	require_once "../../../../class/cls_commonFunctions_get.php";
	require_once "../../../../class/cls_commonErrorHandeling_get.php";
	require_once "../../../../class/finance/cash_flow/cpanal/cls_cpanal_get.php";
 
	$obj_common				= new cls_commonFunctions_get($db);
	$obj_commonErr			= new cls_commonErrorHandeling_get($db);
	$obj_cpanel_get			= new cls_cpanal_get($db);
 	
	$programCode			= 'P0863';
	
	$permision_save			= $obj_common->boolPermision($userId,$programCode,'intEdit');
	$details_results		= $obj_cpanel_get->get_details_results('RunQuery');
	//------
	$result_items			= $obj_cpanel_get->get_items_result('','RunQuery');
	$i=0;
	while($row=mysqli_fetch_array($result_items))
	{
		$row_items_arr[$i]=$row;
		$i++;
	}
	//------
	$result_gl			= $obj_cpanel_get->get_gl_result('','RunQuery');
	$i=0;
	$row = NULL;
	while($row=mysqli_fetch_array($result_gl))
	{
		$row_gl_arr[$i]=$row;
		$i++;
	}
 	//$result_gl			= $obj_cpanel_get->get_gl_result('','RunQuery');
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Finance Cash Flow CPanel</title>
<link rel="stylesheet" type="text/css" href="../../../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../../../css/button.css"/>
<link rel="stylesheet" type="text/css" href="../../../../css/promt.css"/>

<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<link rel="stylesheet" href="../../../../libraries/chosen/chosen/chosen.css" />


</head>
<body>
<form id="frmCPanel" name="frmCPanel" autocomplete="off" action="loan_schedule.php" method="post">
  <table width="100%" border="0" align="center">
    <tr>
      <td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
    </tr>
  </table>
   	<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script> 
	<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
   
    <link rel="stylesheet" href="../../../../libraries/chosen/chosen/chosen.css" />
    
    <script src="../../../../libraries/chosen/chosen/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script> 
    <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script> 
    <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script> 
    <script type="application/javascript" src="cpanel-js.js"></script>

<script>
	jQuery.noConflict();
</script>    
  <div align="center">
    <div class="trans_layoutXL">
      <div class="trans_text">Finance Cash Flow Configuration Panel</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" class="bordered" id="tblMain">
              <thead>
                <tr>
                  <th colspan="10">Configuration Panel
                    <div style="float:right"><a class="button white small" id="butInsertRow">Add New Row</a></div></th>
                </tr>
                <tr>
                <th>Locations</th>
                <th>Code</th>
                <th>Name</th>
                <th>PO Raise</th>
                <th>PO Item</th>
                <th>GL Item</th>
                <th>GL</th>
                <th>Amount Category</th>
                <th>Order By</th>
                <th>Active</th>
                </tr>
              </thead>
            <tbody>
            <?php
			$saved_records =0;
			while($row_d=mysqli_fetch_array($details_results))
			{
				//if($saved_records ==0){
				$saved_records ++;
				$field = $row_d['REPORT_FIELD_ID'];
				$code = $row_d['REPORT_FIELD_CODE'];
				$name = $row_d['REPORT_FIELD_NAME'];
			?>
            <tr>
            <td class="clsAdd" id="<?php echo $field ?>"><a class="clsButAddLocations button green medium"  id="butAddLocations" name="butAddLocations">Add</a></td>
            <td><span style="text-align:center">
              <input name="txtCode" type="text" id="txtCode" style="width:clsButAddLocations90px;text-align:left" class="clsCode validate[required]" value="<?php echo $code ?>"/>
            </span></td>
            <td><span style="text-align:center">
              <input name="txtName" type="text" id="txtName" style="width:150px;text-align:left" class="clsName validate[required]" value="<?php echo $name ?>"/>
            </span></td>
            <td align="center"><input type="radio" name="chkPOGLType<?php echo $saved_records ?>" id="chkPOGLType<?php echo $saved_records ?>" class="clsChkPO clsChkPOGL" <?php if($row_d['BOO_PO_RAISED']==1){ ?> checked="checked"  <?php } ?> value="1" /></td>
            <td class="poCombo"><select name="txtPOItem<?php echo $saved_records ?>" id="txtPOItem<?php echo $saved_records ?>"  multiple="multiple" class="txtPOItem normalfnt chosen-select" style="width:300px;height:50px" tabindex="4">
		  <?php
		$row  = NULL;  
		$itemsArr = NULL;
		$html ='';	
		//echo $saved_records."-".$result_items."/";	
 		for($i=0; $i<count($row_items_arr); $i++)
		{
					$row	  = $row_items_arr[$i];
					$itemsArr = explode(',',$row_d['PO_ITEM_ID']);
					if (in_array($row['intId'], $itemsArr)) {
					  $selected = 1;
					} else {
						$selected = 0;
					}			
					  	
					if($selected==1)
 						$html .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
					else
						$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";					
 		}
		echo $html;
						?>
                  </select></td>
            <td align="center"><input type="radio" name="chkPOGLType<?php echo $saved_records ?>" id="chkPOGLType<?php echo $saved_records ?>" <?php if($row_d['BOO_GL_ITEM']==1){ ?> checked="checked"  <?php } ?> class="clsChkGL clsChkPOGL" value="2"/></td>
            <td class="glCombo"><select name="txtGLItem<?php echo $saved_records ?>" id="txtGLItem<?php echo $saved_records ?>"  multiple="multiple" class="txtGLItem normalfnt chosen-select" style="width:300px;height:50px" tabindex="4" data-placeholder="">
		  <?php
		$row  = NULL;  
		$glArr = NULL;
		$html ='';	
		//echo $saved_records."-".$result_items."/";	
 		for($i=0; $i<count($row_gl_arr); $i++)
		{
					$row	  = $row_gl_arr[$i];
					$glArr = explode(',',$row_d['GL_ID']);
					if (in_array($row['CHART_OF_ACCOUNT_ID'], $glArr)) {
					  $selected = 1;
					} else {
						$selected = 0;
					}			
					  	
					if($selected==1)
 						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";
					else
						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";					
 		}
		echo $html;
						?>
                  </select></td>
            <td><select name="txtAmountCategory<?php //echo $saved_records ?>" id="txtAmountCategory<?php echo $saved_records ?>"  class="txtAmountCategory validate[required] normalfnt chosen-select  " style="width:120px;height:50px" tabindex="4" data-placeholder="">
                 <?php
					$html = NULL;
					if ($row_d['AMOUNT_FILLING_CATEGORY']=='F') {
 						$html .= "<option value=\"F\" selected=\"selected\">Fixed</option>";
 						$html .= "<option value=\"P\">Previous</option>";
					}
					else if ($row_d['AMOUNT_FILLING_CATEGORY']=='P') {
 						$html .= "<option value=\"F\">Fixed</option>";
 						$html .= "<option value=\"P\" selected=\"selected\">Previous</option>";
					}
					else{
 						$html .= "<option value=\"F\">Fixed</option>";
 						$html .= "<option value=\"P\">Previous</option>";
					}
					  	
  				echo $html;
		?>
                  </select></td>
            <td><span style="text-align:center">
              <input name="txtOrderBy" type="text" id="txtOrderBy" style="width:40px;text-align:right" class="clsOrderBy validate[required]" value="<?php echo $row_d['ORDER_BY']; ?>"/>
            </span></td>
            <td align="center"><input type="checkbox" name="chkActive" id="chkActive" <?php if($row_d['STATUS']==1){ ?> checked="checked"  <?php } ?> class="clsActive"/></td>
            </tr>
            <?php
			//}
			}
			?>
            
            <?php
			if($saved_records==0){
			?>
            <tr>
            <td class="clsAdd" id="<?php echo $field ?>"><a class="clsButAddLocations button green medium" id="butAddLocations" name="butAddLocations">Add</a></td>
            <td><span style="text-align:center">
              <input name="txtCode" type="text" id="txtCode" style="width:90px;text-align:left" class="clsCode validate[required]" value="<?php echo $code ?>"/>
            </span></td>
            <td><span style="text-align:center">
              <input name="txtName" type="text" id="txtName" style="width:150px;text-align:left" class="clsName validate[required]" value="<?php echo $name ?>"/>
            </span></td>
            <td align="center"><input type="radio" name="chkPOGLType<?php echo $saved_records ?>" id="chkPOGLType<?php echo $saved_records ?>" class="clsChkPO clsChkPOGL" <?php if($row_d['BOO_PO_RAISED']==1){ ?> checked="checked"  <?php } ?> value="1" /></td>
            <td class="poCombo"><select name="txtPOItem<?php echo $saved_records ?>" id="txtPOItem<?php echo $saved_records ?>"  multiple="multiple" class="txtPOItem normalfnt chosen-select" style="width:300px;height:50px" tabindex="4">
		  <?php
		$row  = NULL;  
		$itemsArr = NULL;
		$html ='';	
		//echo $saved_records."-".$result_items."/";	
 		for($i=0; $i<count($row_items_arr); $i++)
		{
					$row	  = $row_items_arr[$i];
					$itemsArr = explode(',',$row_d['PO_ITEM_ID']);
					if (in_array($row['intId'], $itemsArr)) {
					  $selected = 1;
					} else {
						$selected = 0;
					}			
					  	
					if($selected==1)
 						$html .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
					else
						$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";					
 		}
		echo $html;
						?>
                  </select></td>
            <td align="center"><input type="radio" name="chkPOGLType<?php echo $saved_records ?>" id="chkPOGLType<?php echo $saved_records ?>" <?php if($row_d['BOO_GL_ITEM']==1){ ?> checked="checked"  <?php } ?> class="clsChkGL clsChkPOGL" value="2"/></td>
            <td class="glCombo"><select name="txtGLItem<?php echo $saved_records ?>" id="txtGLItem<?php echo $saved_records ?>"  multiple="multiple" class="txtGLItem normalfnt chosen-select" style="width:300px;height:50px" tabindex="4" data-placeholder="">
		  <?php
		$row  = NULL;  
		$glArr = NULL;
		$html ='';	
		//echo $saved_records."-".$result_items."/";	
 		for($i=0; $i<count($row_gl_arr); $i++)
		{
					$row	  = $row_gl_arr[$i];
					$glArr = explode(',',$row_d['GL_ID']);
					if (in_array($row['CHART_OF_ACCOUNT_ID'], $glArr)) {
					  $selected = 1;
					} else {
						$selected = 0;
					}			
					  	
					if($selected==1)
 						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";
					else
						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";					
 		}
		echo $html;
						?>
                  </select></td>
            <td><select name="txtAmountCategory<?php echo $saved_records ?>" id="txtAmountCategory<?php echo $saved_records ?>"  class="txtAmountCategory validate[required] normalfnt chosen-select " style="width:120px;height:50px" tabindex="4" data-placeholder="">
                 <?php
					$html = NULL;
					if ($row_d['AMOUNT_FILLING_CATEGORY']=='F') {
 						$html .= "<option value=\"F\" selected=\"selected\">Fixed</option>";
 						$html .= "<option value=\"P\">Previous</option>";
					}
					else if ($row_d['AMOUNT_FILLING_CATEGORY']=='P') {
 						$html .= "<option value=\"F\">Fixed</option>";
 						$html .= "<option value=\"P\" selected=\"selected\">Previous</option>";
					}
					else{
 						$html .= "<option value=\"F\">Fixed</option>";
 						$html .= "<option value=\"P\">Previous</option>";
					}
					  	
  				echo $html;
		?>
                  </select></td>
            <td><span style="text-align:center">
              <input name="txtOrderBy" type="text" id="txtOrderBy" style="width:40px;text-align:right" class="clsOrderBy validate[required]" value="<?php echo $row_d['ORDER_BY']; ?>"/>
            </span></td>
            <td align="center"><input type="checkbox" name="chkActive" id="chkActive" <?php if($row_d['STATUS']==1){ ?> checked="checked"  <?php } ?> class="clsActive"/></td>
            </tr>
            <?php
			}
			?>
            </tbody>
      </table></td>
    </tr>
    <tr><td>
    <table width="100%" border="0" class="bordered" id="tblMain">
        <tr>
          <td height="32" style="text-align:center"><?php if($permision_save ==1){ ?><a class="button green" id="butSave">SAVE</a><?php } ?></td>
        </tr>
      </table>
    </td>
    </tr>
    </table>
    </div>
  </div>
</form>
</body>
<div style="width:500px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</html>
<?php
?>
<?php

session_start();
$backwardseperator	=	"../../../../";
$mainPath			=	$_SESSION['mainPath'];
$userId				=	$_SESSION['userId'];
$location			=	$_SESSION['CompanyID'];
$company			=	$_SESSION['headCompanyId'];

include '../../../../dataAccess/Connector.php';
require_once "../../../../class/cls_commonFunctions_get.php";
require_once "../../../../class/finance/cash_flow/cpanal/cls_cpanal_get.php";
require_once "../../../../class/finance/cash_flow/cpanal/cls_cpanal_set.php";

$requestType		= $_REQUEST["RequestType"];
$programCode		= 'P0863';

$obj_common			= new cls_commonFunctions_get($db);
$obj_cpanal_set		= new cls_cpanal_set($db);
$obj_cpanal_get		= new cls_cpanal_get($db);
//die($requestType);

$savedMasseged			= "";
$error_sql				= "";
$savedStatus			= true;

 	if($requestType	==	'save_cpanel')
	{
		$arrHeader 		=	$_REQUEST['arrHeader'];
		$arrDetails		=	$_REQUEST['arrDetails'];
		$arrDetails 	=	json_decode($arrDetails,true);
		$permision_save = $obj_common->boolPermision($userId,$programCode,'intEdit');
		
		$db->begin();
		$rollBack_flag	= false;
		
		if($permision_save != 1){
			$rollBack_flag	= true;
			$rollBack_msg	= "No permision to save.";
		}
		
 		if(($rollBack_flag !=	true)){
 			$saved	=	0;
			$toSave	=	0;
			$i		=	0;
			
			foreach($arrDetails as $arrVal)
			{
 				
				if($rollBack_flag	!=	true){

					$toSave++;
					$id				= $arrVal['id'];
					$code			= $arrVal['code'];
					$name			= $arrVal['name'];
					$boo_po			= $arrVal['boo_po'];
					$po_items		= $arrVal['po_items'];
					$boo_gl			= $arrVal['boo_gl'];
					$gl_items		= $arrVal['gl_items'];
					$amnt_category	= $arrVal['amnt_category'];
					$order_by		= $arrVal['order_by'];
					$active			= $arrVal['active'];

					$duplicate_flag 	=	$obj_cpanal_get->chk_duplicate_codes($id,$code,'RunQuery2');
					if($duplicate_flag	 == true){
						$rollBack_flag	=true;
						$rollBack_msg	="Can't exists duplicate Codes";
					}
					
					if($rollBack_flag != true){
						if($id!=''){	
							$response_1	=	$obj_cpanal_set->update_cpanel($id,$code,$name,$boo_po,$po_items,$boo_gl,$gl_items,$amnt_category,$order_by,$active,'RunQuery2');
						}
						else{
							$response_1	=	$obj_cpanal_set->insert_cpanel($id,$code,$name,$boo_po,$po_items,$boo_gl,$gl_items,$amnt_category,$order_by,$active,'RunQuery2');
						}
 						
 					}
						
					if($response_1['type']	!=	'fail'){
					$saved ++;
					}
					if($response_1['type']	 == 'fail'){
						$rollBack_flag		=true;
						$rollBack_msg		=$response_1['msg'];
						$rollBack_sql		=$response_1['sql'];
					}
				}
			}
  		
		if($rollBackFlag	==	true){
			$db->rollback();
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBack_msg;
			$response['q'] 			= $sqlE;
		}
		else if($saved	==	0){
			$db->rollback();
			$response['type'] 		= 'fail';
			$response['msg'] 		= 'Error with saving Data';
			$response['q'] 			= $sql;
		}
		else if(($toSave	==	$saved)){
			$db->commit();
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$db->rollback();
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlE;
		}
		}
		
		$db->CloseConnection();		
		echo json_encode($response);
 	}
else if($requestType == 'saveData')
{
	$headerArray 		= json_decode($_REQUEST["HeaderArray"],true);
	$detailArray		= json_decode($_REQUEST["DetailArray"],true);
	
	$fieldID		= $headerArray['fieldID'];
	$status			= $headerArray['status'];
	//$LocationID		= $detailArray['popLocationId'];
	
	
	$db->begin();
	$deleteArr		= $obj_cpanal_set->deleteLocation($fieldID,'RunQuery2');
	if($deleteArr['savedStatus'] == 'fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged	= $deleteArr['savedMassege'];
		$error_sql		= $deleteArr['error_sql'];	
	}
	foreach($detailArray as $detailArray)
	{
		$LocationID		= ($detailArray["popLocationId"]==""?0:$detailArray["popLocationId"]);
		
		$saveArr	= $obj_cpanal_set->saveLocation($fieldID,$LocationID,$status,'RunQuery2');
		if($saveArr['savedStatus'] == 'fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $saveArr['savedMassege'];
			$error_sql		= $saveArr['error_sql'];	
		}
				
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type']	= 'pass';
		$response['msg']	= "Saved Successfully";			
	}
	else
	{
		$db->rollback();
		$response['type']	= 'fail';
		$response['msg']	= $savedMasseged;
		$response['sql']	= $error_sql;	
	}
	echo json_encode($response);
				
}
else if($requestType == 'loadData')
{
	$fieldID					= $_REQUEST['reportField'];
	$result						= $obj_cpanal_get->getData($fieldID,'RunQuery');	
	while($row = mysqli_fetch_array($result))
	{
		$data['locationId']	= $row['LOCATION_ID'];
		
		$arrDetail[]		= $data;	
	}
	$response['arrDetail']	= $arrDetail;
	//$response['STATUS']			=( $searchData['STATUS']==1?true:false);
	
	echo json_encode($response);		
}
?>
var para;
var objName; 
var reportField;
// JavaScript Document
 
$(document).ready(function() {
  	
	
  	$("#frmCPanel").validationEngine();
	$('#frmPopUp').validationEngine();
	$(".txtGLItem").chosen({allow_single_deselect: false , no_results_text: "Oops, nothing found!"}); 
	$(".txtPOItem").chosen({allow_single_deselect: false , no_results_text: "Oops, nothing found!"});
	$(".txtAmountCategory").chosen({allow_single_deselect: true , no_results_text: "Oops, nothing found!"});
	$(".clsButAddLocations").live('click',setToAddNew);
	$('#frmPopUp #butSave').live('click',locationSave);
	$('#frmPopUp #chkCheckAll').live('click',function(){
		CheckAll_Approve($(this).is(':checked'));
	});
	$('#frmCPanel #butSave').live('click',save_cpanel);
	$('#frmCPanel #butInsertRow').live('click',add_row);
})


function setToAddNew()
{
	reportField		= $(this).parent().parent().find('.clsAdd').attr('id');
	var loop	= 0;
	popupWindow3('1');
	//alert(aa);
	$('#popupContact1').load('popUpCpanel.php',function(){
		
			checkedLocation();
			$('#frmPopUp #butClosePop').click(disablePopup);
	});
			
}
function checkedLocation()
{
var url 		= "cpanel_db.php?RequestType=loadData&reportField="+reportField;
var httpobj 	= $.ajax({
			url:url,
			dataType:'json',
			async:false,
			success:function(json){
				var length		= json.arrDetail.length;
				var searchArr	= json.arrDetail;
				
				for(i = 0; i<length; i++)
				{
					$('.chkLocation').each(function(){
						
						if(searchArr[i]['locationId']==$(this).parent().parent().attr('id'))
						{
							$(this).attr('checked',true);
						}
					})
				}
			},
			error: function()
			{
				
			}
			
			});	
}
function locationSave()
{
	var fieldID			= reportField;
	var status			= 1;
	var	headerArray  = "{";
			headerArray += '"fieldID":"'+fieldID+'",' ;
			headerArray += '"status":"'+status+'"' ;
			headerArray += "}";
			
	var detailArray	 = "[";
	$('.chkLocation:checked').each(function(){
		var popObj	 = $(this);
			detailArray += "{";
			detailArray += '"popLocationId":"'+popObj.parent().parent().attr('id')+'"' ;
			detailArray +=  '},';
		});
		detailArray 	 = detailArray.substr(0,detailArray.length-1);
		detailArray 	+= " ]";
	
		var url 	= "cpanel_db.php?RequestType=saveData";
		var data 	= "HeaderArray="+headerArray;
			data   += "&DetailArray="+detailArray;
		$.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:data,
			async:false,
			success: function(json)
			{
				$('#frmPopUp #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
			},
			error: function(xhr,status)
			{
				
			}
	});
}

function CheckAll_Approve(obj,obj1)
{
	$('.chkLocation').each(function(){
			if(!$(this).is(':disabled')){
				if(obj)
					$(this).attr('checked','checked');
				else
					$(this).removeAttr('checked');
			}
		});
}

function save_cpanel(){
  	$("#frmCPanel").validationEngine();
		
		showWaiting();
		
		
		var data = "";
 		var arrHeader = "{";
		arrHeader += ' }';
		
		var i=0;
		var rcds=0;
		var errorDescFlag=0;
		var errorActionByFlag=0;
		var arrDetails="";
		
		
		$('.clsCode').each(function(){
			i++;
			var code	 			=	$(this).val();
			var id 					=	$(this).parent().parent().parent().find('.clsAdd').attr('id');
			var name				=	$(this).parent().parent().parent().find('.clsName').val();
 			var po_items	 		=	$(this).parent().parent().parent().find('.txtPOItem').val(); 
			var boo_val	 			=	$(this).parent().parent().parent().find('.clsChkPO').val();
			var boo_val	 			=	$(this).parent().parent().parent().find('.clsChkGL').val();
			var gl_items 			=	$(this).parent().parent().parent().find('.txtGLItem ').val();
			var amnt_category		=	$(this).parent().parent().parent().find('.txtAmountCategory ').val();
			var order_by 			=	$(this).parent().parent().parent().find('.clsOrderBy ').val();
			var active				=	0;
			if($(this).parent().parent().parent().find('.clsActive').attr('checked')==true) 
			var active	 			=	1;
			
			var boo_po				= 0;
			var boo_gl 				= 0;
			if($(this).parent().parent().parent().find('.clsChkPO').attr('checked')==true) 
			var boo_po	 			=	1;
			if($(this).parent().parent().parent().find('.clsChkGL').attr('checked')==true) 
			var boo_gl	 			=	1;

			if(active==true){active=1;}else{active=0;}
			if(po_items==null){po_items='';}
			if(gl_items==null){gl_items='';}
			if(amnt_category==null){alert('Pls select amount category');return false;}
			
 
			if((code!='') &&(name !='') && (amnt_category!=null))
			{
				rcds++;
				arrDetails += "{";
							arrDetails += '"id":"'+ id +'",' ;
							arrDetails += '"code":"'+ code +'",' ;
							arrDetails += '"name":"'+ name +'",' ;
							arrDetails += '"boo_po":"'+ boo_po +'",' ;
							arrDetails += '"po_items":"'+ po_items +'",' ;
							arrDetails += '"boo_gl":"'+ boo_gl +'",' ;
							arrDetails += '"gl_items":"'+ gl_items +'",' ;
							arrDetails += '"amnt_category":"'+ amnt_category +'",' ;
							arrDetails += '"order_by":"'+ order_by +'",' ;
							arrDetails += '"active":"'+ active +'"' ;
							arrDetails +=  '},';
			}
		})
		
		if(rcds==0){
			alert("There is no records to save");
			hideWaiting();
			return false;
		}
		
		arrDetails = arrDetails.substr(0,arrDetails.length-1);
		
		var arrHeader=arrHeader;
		var arrDetails='['+arrDetails+']';
 		data+="&arrHeader="	+	arrHeader+"&arrDetails="	+	arrDetails;
 		
		var url = "cpanel_db.php?RequestType=save_cpanel";
		$.ajax({
				url:url,
				async:false,
				dataType:'json',
				type:'post',
				data:data,
		success:function(json){
				$('#frmCPanel #butSave').validationEngine('showPrompt', json.msg,json.type );
				if(json.type=='pass')
				{
					var t=setTimeout("alertx()",1000);
					hideWaiting();
					return;
				}
				hideWaiting();
			},
		error:function(xhr,status){
				$('#frmCPanel #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t=setTimeout("alertx()",1000);
				hideWaiting();
				return;
			}
			
		});
 }
 
 function add_row(){
	 
	var rowCount = document.getElementById('tblMain').rows.length;
	var a = rowCount;

	var select1 = '<select name="txtPOItem'+a+'" id="txtPOItem'+a+'"  multiple="multiple" class="txtPOItem normalfnt chosen-select" style="width:300px;height:50px" tabindex="4"></select>'
	var select2 = '<select name="txtGLItem'+a+'" id="txtGLItem'+a+'"  multiple="multiple" class="txtGLItem normalfnt chosen-select" style="width:300px;height:50px" tabindex="4"></select>'
	var select3 = '<select name="txtAmountCategory'+a+'" id="txtAmountCategory'+a+'" class="txtAmountCategory normalfnt chosen-select" style="width:120px;height:50px" tabindex="4"></select>'

	if($('#tblMain tr:eq('+(a-1)+')').find('.clsChkPO').attr('checked')==true) 
	var boo_po	 			=	1;
	if($('#tblMain tr:eq('+(a-1)+')').find('.clsChkGL').attr('checked')==true) 
	var boo_gl	 			=	1;
	

	document.getElementById('tblMain').insertRow(rowCount);
	document.getElementById('tblMain').rows[rowCount].innerHTML = document.getElementById('tblMain').rows[rowCount-1].innerHTML;
	
  	$('#tblMain tr:eq('+a+')').find('.clsChkPOGL').attr('id',('chkPOGLType'+a));
  	$('#tblMain tr:eq('+a+')').find('.clsChkPOGL').attr('name',('chkPOGLType'+a));

  	$('#tblMain tr:eq('+a+')').find('.txtPOItem').attr('id',('txtPOItem'+a));
 	$('#tblMain tr:eq('+a+')').find('.txtPOItem').attr('name',('txtPOItem'+a));
 	$('#tblMain tr:eq('+a+')').find('.poCombo').html(select1);
 	$('#tblMain tr:eq('+a+')').find('.txtPOItem').html($('#tblMain tr:eq('+(a-1)+')').find('.txtPOItem').html());

  	$('#tblMain tr:eq('+a+')').find('.txtGLItem').attr('id',('txtGLItem'+a));
 	$('#tblMain tr:eq('+a+')').find('.txtGLItem').attr('name',('txtGLItem'+a));
 	$('#tblMain tr:eq('+a+')').find('.glCombo').html(select2);
 	$('#tblMain tr:eq('+a+')').find('.txtGLItem').html($('#tblMain tr:eq('+(a-1)+')').find('.txtGLItem').html());

  	$('#tblMain tr:eq('+a+')').find('.txtAmountCategory').attr('id',('txtAmountCategory'+a));
 	$('#tblMain tr:eq('+a+')').find('.txtAmountCategory').attr('name',('txtAmountCategory'+a));
 	$('#tblMain tr:eq('+a+')').find('.txtAmountCategory').parent().html(select3);
 	$('#tblMain tr:eq('+a+')').find('.txtAmountCategory').html($('#tblMain tr:eq('+(a-1)+')').find('.txtAmountCategory').html());

 
 	$('#tblMain tr:eq('+a+')').find('.txtGLItem').chosen({allow_single_deselect: false , no_results_text: "Oops, nothing found!"}); 
 	$('#tblMain tr:eq('+a+')').find('.txtPOItem').chosen({allow_single_deselect: false , no_results_text: "Oops, nothing found!"}); 
	$('#tblMain tr:eq('+a+')').find('.txtAmountCategory').chosen({allow_single_deselect: true , no_results_text: "Oops, nothing found!"});
	
	$('#tblMain tr:eq('+a+')').find('.clsCode').val('');
	$('#tblMain tr:eq('+a+')').find('.clsAdd').attr('id','');
	$('#tblMain tr:eq('+a+')').find('.clsName').val('');
	//$('#tblMain tr:eq('+a+')').find('.clsChkPO').val(0);
	$('#tblMain tr:eq('+a+')').find('.txtPOItem').val(null); 
	//$('#tblMain tr:eq('+a+')').find('.clsChkGL').val(0);
	$('#tblMain tr:eq('+a+')').find('.txtGLItem').val(null);
	$('#tblMain tr:eq('+a+')').find('.txtAmountCategory').val(null);
	$('#tblMain tr:eq('+a+')').find('.clsOrderBy').val('');
	$('#tblMain tr:eq('+a+')').find('.clsActive').val('');
	$('#tblMain tr:eq('+a+')').find('.clsAdd').html('');
	$('#tblMain tr:eq('+a+')').find('.clsChkPOGL').attr('checked',false);
	
	
	/*var ch = '#txtPOItem'+a+'_chzn';
	$('#tblMain tr:eq('+a+')').find(ch).html('');
 	$('#tblMain tr:eq('+a+')').find('.txtPOItem').chosen({allow_single_deselect: false , no_results_text: "Oops, nothing found!"}); 
	*/
	/*var ch = '#txtPOItem'+a;
	$('#tblMain tr:eq('+a+')').find('.txtPOItem').val('').trigger('chosen:updated');
	*/
	if(boo_po == 1)
		$('#tblMain tr:eq('+(a-1)+')').find('.clsChkPO').attr('checked',true);
	if(boo_gl == 1)
		$('#tblMain tr:eq('+(a-1)+')').find('.clsChkGL').attr('checked',true);

 }
 
 function alertx()
{
	$('#frmCPanel #butSave').validationEngine('hide')	;
	
}


function add_new_row(table,rowcontent){
        if (jQuery(table).length>0){
            if (jQuery(table+' > tbody').length==0) jQuery(table).append('<tbody />');
            (jQuery(table+' > tr').length>0)?jQuery(table).children('tbody:last').children('tr:last').append(rowcontent):jQuery(table).children('tbody:last').append(rowcontent);
        }
    }

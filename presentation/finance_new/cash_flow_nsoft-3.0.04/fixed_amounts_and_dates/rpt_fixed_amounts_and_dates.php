<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId 				= $_SESSION['headCompanyId'];
$locationId				= $_SESSION['CompanyID'];
$mainPath				= $_SESSION['mainPath'];
$session_userId			= $_SESSION["userId"];
$intUser				= $_SESSION["userId"];

require_once 			"class/cls_commonFunctions_get.php";
require_once 			"class/cls_commonErrorHandeling_get.php";
include_once 			"class/finance/cash_flow/fixed_amounts_and_dates/cls_fixed_amounts_and_dates_get.php";

$obj_common					= new cls_commonFunctions_get($db);
$obj_commonErr				= new cls_commonErrorHandeling_get($db);
$obj_amounts_and_dates_get	= new cls_fixed_amounts_and_dates_get($db);

$programName			='Fixed Forcast Amounts';
$programCode			='P0856';
 
$location				= (!isset($_REQUEST['location'])?'':$_REQUEST['location']);
$year					= (!isset($_REQUEST['year'])?'':$_REQUEST['year']);
$item					= (!isset($_REQUEST['item'])?'':$_REQUEST['item']);

//$location				= 2;
//$year					= 2014;
//$item					= 5;

$mode					= (!isset($_REQUEST['mode'])?'':$_REQUEST['mode']);

 
$header_array			= $obj_amounts_and_dates_get->get_header($location,$item,$year,'RunQuery');

$permition_arr			= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$intUser,$programCode,'RunQuery');
$permision_save			= $permition_arr['permision'];
$permition_arr			= $obj_commonErr->get_permision_withApproval_cancel($header_array['STATUS'],$header_array['LEVELS'],$intUser,$programCode,'RunQuery');
$permision_cancel		= $permition_arr['permision'];
$permition_arr			= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['LEVELS'],$intUser,$programCode,'RunQuery');
$permision_reject		= $permition_arr['permision'];
$permition_arr			= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$intUser,$programCode,'RunQuery');
$permision_confirm		= $permition_arr['permision'];
$permition_arr			= $obj_commonErr->get_permision_withApproval_revise($header_array['STATUS'],$header_array['LEVELS'],$intUser,$programCode,'RunQuery');
$permision_revise		= $permition_arr['permision'];

$no_of_dates	= 10;
$dates_array	= $obj_amounts_and_dates_get->get_saved_fixed_amounts_array($location,$item,$year,'RunQuery');
$dates_array1	= $dates_array;
$no_of_dates	= count($dates_array);

?>
<head>
<title>Forecast fixed amounts and dates</title>

<script type="text/javascript" src="presentation/finance_new/cash_flow/fixed_amounts_and_dates/rpt_fixed_amounts_and_dates_js.js"></script>

</head>
<body>
<style type="text/css">
.apDiv1 {
	position:absolute;
	left:380px;
	top:100px;
	width:auto;
	height:auto;
	z-index:0;
	opacity:0.1;
}
</style>
 
 <form id="frmRptFixedDatesAndAmounts" name="frmRptFixedDatesAndAmounts" method="post">
  <table width="900" align="center">
    <tr>
      <td><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
      <td class="reportHeader" align="center">Fixed Forecast Amounts And Dates</td>
    </tr>
	<?php
		include "presentation/report_approve_status_and_buttons.php"
     ?>
<tr>
      <td><table width="100%" border="0" cellspacing="2" cellpadding="2" class="normalfnt">
                    	  <tr>
                    	    <td width="15%">Location</td>
                    	    <td width="2%">:</td>
                    	    <td width="39%"><?php echo $header_array['LOCATION_NAME']; ?></td>
                    	    <td width="7%">Year</td>
                    	    <td width="2%">:</td>
                    	    <td width="35%" align="left"><?php echo $header_array['YEAR']; ?></td>
                  	    </tr>
                        <tr>
                        <td>Item</td>
                        <td>:</td>
                        <td><?php  echo $header_array['ITEM_NAME']?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        </tr>
       	              </table></td>
    </tr>
    <tr>
    <td>
<table width="100%%" border="0" class="rptBordered" id="tblMain">
          <thead>
            <tr>
              <th width="6%"></th>
              <?php
			  while (list($key_i, $value_i) = each($dates_array)){
			  ?>
              <th width="8%"><?php echo $key_i ?></th>
              <?php
			  }
			  ?>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Amount</td>
              <?php
			  while (list($key_i, $value_i) = each($dates_array1)){
			  ?>
              <td align="right"><?php echo number_format($value_i,2); ?></td>
              <?php
			  }
			  ?>
            </tr>
          </tbody>
        </table>    </td>
    </tr>
    <tr>
    <td>
    <?php
			$creator		= $header_array['USER'];
			$createdDate	= $header_array['CREATED_DATE'];
 			$resultA 		= $obj_amounts_and_dates_get->get_report_approval_details_result($location,$item,$year,'RunQuery');
			include "presentation/report_approvedBy_details.php"
 	?>
    </td>
    </tr>
    <tr>
    <td align="center" class="normalfntMid"><strong>Printed Date : </strong><?php echo date('Y-m-d H:i:s');?></td>
    </tr>
  </table>
</form>
</body>


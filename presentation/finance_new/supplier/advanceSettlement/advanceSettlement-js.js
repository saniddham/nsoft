// JavaScript Document
var basePath	= "presentation/finance_new/supplier/advanceSettlement/";
var menuId		= 778;
$(document).ready(function(){
	
	$("#frmSupAdvanceSettlement").validationEngine();
	
	$('#frmSupAdvanceSettlement #cboSupplier').die('change').live('change',loadData);
	$('#frmSupAdvanceSettlement #cboCurrency').die('change').live('change',loadData);
	$('#frmSupAdvanceSettlement .clsLedgerAc').die('change').live('change',loadAmount);
	$('#frmSupAdvanceSettlement .clsAdvanceNo').die('change').live('change',loadGLCombo);
	$('#frmSupAdvanceSettlement .clsSettleAmt').die('focus').live('focus',chkAdvanceCombo);
	$('#frmSupAdvanceSettlement .clsSettleAmt').die('keyup').live('keyup',setMaxAmount);
	$('#frmSupAdvanceSettlement .clsSave').die('click').live('click',saveData);
	$('#frmSupAdvanceSettlement .cboAdvanceNoformError').die('click').live('click',alertx);
	$('#frmSupAdvanceSettlement .cboLedgerAcformError').die('click').live('click',alertx3); 
	$('#frmSupAdvanceSettlement .txtSettleAmtformError').die('click').live('click',alertx1);
	$('#frmSupAdvanceSettlement #butNew').die('click').live('click',clearAll);
	$('#frmSupAdvanceSettlement .clsChkAdvanceAll').die('click').live('click',getAllAdvance);
	
});
function loadData()
{
	var supplierId = $('#frmSupAdvanceSettlement #cboSupplier').val();
	var currencyId = $('#frmSupAdvanceSettlement #cboCurrency').val();
	$("#frmSupAdvanceSettlement #tblMain tr:gt(0)").remove();
	
	if(supplierId=='' || currencyId=='')
	{
		$('#frmSupAdvanceSettlement #txtCurrencyRate').val('0.0000');
		return;
	}
	
	var url = basePath+"advanceSettlement-db-get.php?requestType=loadData";
	var obj = $.ajax({
		url:url,
		dataType: "json", 
		type:'POST', 
		data:"&supplierId="+supplierId+'&currencyId='+currencyId,
		async:false,
		success:function(json){
			if(json.type=='pass')
			{
				$('#frmSupAdvanceSettlement #txtCurrencyRate').val(json.exchgRate);
				
				var lengthDetail   = json.arrDetailData.length;
				var arrDetailData  = json.arrDetailData;	
				for(var i=0;i<lengthDetail;i++)
				{
					var invoiceNo		= arrDetailData[i]['invoiceNo'];
					var invoiceYear		= arrDetailData[i]['invoiceYear'];
					var Invoice_No		= arrDetailData[i]['INVOICE_NO'];	
					var PONo			= arrDetailData[i]['PONo'];
					var POYear			= arrDetailData[i]['POYear'];
					var invoiceAmount	= arrDetailData[i]['invoiceAmount'];
					var paidAmount		= arrDetailData[i]['paidAmount'];
					var debitAmount		= arrDetailData[i]['debitAmount'];
					var creditAmount	= arrDetailData[i]['creditAmount'];
					var advancedAmount	= arrDetailData[i]['advancedAmount'];
					var advanceCombo	= arrDetailData[i]['advanceCombo'];
					var status			= arrDetailData[i]['status'];
					
					if(status!='complete')
						createGrid(invoiceNo,invoiceYear,Invoice_No,PONo,POYear,invoiceAmount,paidAmount,debitAmount,creditAmount,advancedAmount,advanceCombo);
				}
			}			
		},
		error:function(xhr,status){
				
			}		
	});
	
}
function createGrid(invoiceNo,invoiceYear,Invoice_No,PONo,POYear,invoiceAmount,paidAmount,debitAmount,creditAmount,advancedAmount,advanceCombo)
{
	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);
	row.id			= invoiceNo+"/"+invoiceYear;
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'clsInvoiceNo';
	cell.innerHTML  = Invoice_No;
	
	var cell		= row.insertCell(1);
	cell.setAttribute("style",'text-align:center');
	cell.id			= PONo+"/"+POYear;
	cell.className	= 'clsPONo';
	cell.innerHTML  = PONo+" - "+POYear;
	
	var cell		= row.insertCell(2);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsInvoiceAmt';
	cell.innerHTML  = invoiceAmount;
	
	var cell		= row.insertCell(3);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsPaidAmt';
	cell.innerHTML  = (paidAmount==null?0:paidAmount);
	
	var cell		= row.insertCell(4);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsDebitAmt';
	cell.innerHTML  = (debitAmount==null?0:debitAmount);
	
	var cell		= row.insertCell(5);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsCreditAmt';
	cell.innerHTML  = (creditAmount==null?0:creditAmount);
	
	var cell		= row.insertCell(6);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsAdvanceAmt';
	cell.innerHTML  = (advancedAmount==null?0:advancedAmount);
	
	var cell		= row.insertCell(7);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsAdvanceValue';
	cell.innerHTML  = '&nbsp;';
	
	var cell		= row.insertCell(8);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML  = '<input type=\"checkbox\" id=\"chkAdvanceAll\" name=\"chkItem\" class=\"clsChkAdvanceAll\">';
	
	var cell		= row.insertCell(9);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'clsCboAdvance';
	cell.innerHTML  = advanceCombo;
	
	var cell		= row.insertCell(10);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML  = "<select style=\"width:100%\" class=\"clsLedgerAc\" id=\"cboLedgerAc\" name=\"cboLedgerAc\" ></select>";
	
	var cell		= row.insertCell(11);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML  = "<input type=\"textbox\" id=\"txtSettleAmt\" class=\"clsSettleAmt validate[custom[number]]\" style=\"width:90px;text-align:right\" value=\"0\"><input type=\"hidden\" id=\"txtMaxAmount\" class=\"clsMaxAmount\" value=\"0\">";
	
	var cell		= row.insertCell(12);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML  = (saveMode==1?'<a class=\"button green small clsSave\" id=\"butSave\" name=\"butSave\">&nbsp;Save&nbsp;</a>':'&nbsp;');
	
	/*var cell		= row.insertCell(11);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML  = (cancelMode==1?'<a class=\"button green small clsCancel\" id=\"butCancel\" name=\"butCancel\">&nbsp;Cancel&nbsp;</a>':'&nbsp;');*/
}
function loadAmount()
{
	var supplierId 		= $('#frmSupAdvanceSettlement #cboSupplier').val();
	var currencyId 		= $('#frmSupAdvanceSettlement #cboCurrency').val();
	var advanceNoArr 	= $(this).parent().parent().find('.clsAdvanceNo') .val();
	var obj		   = this;
	$(this).parent().parent().find('.clsSettleAmt').val(0);	
	
	if($(this).val()=='')
	{
		$(this).parent().parent().find('.clsMaxAmount').val(0);
		$(obj).parent().parent().find('.clsAdvanceValue').html('&nbsp;');
		return;
	}
	var url = basePath+"advanceSettlement-db-get.php?requestType=loadMaxAmount";
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',  
		data:"&advanceNoArr="+advanceNoArr+"&GLAccId="+$(this).val()+"&supplierId="+supplierId+"&currencyId="+currencyId,
		async:false,
		success:function(json){
			
		var invoiceValue 	= parseFloat($(obj).parent().parent().find('.clsInvoiceAmt').html());
		var paidAmonut 		= parseFloat($(obj).parent().parent().find('.clsPaidAmt').html());
		var advancedAmount 	= parseFloat($(obj).parent().parent().find('.clsAdvanceAmt').html());
		var debitAmount 	= parseFloat($(obj).parent().parent().find('.clsDebitAmt').html());
		
		var maxAmount	 	= parseFloat(json.maxAmount);
		
		var balAmonut		= invoiceValue-(paidAmonut+advancedAmount+debitAmount);
		
		if(maxAmount>balAmonut)
			$(obj).parent().parent().find('.clsMaxAmount').val(RoundNumber(balAmonut,2));
		else
			$(obj).parent().parent().find('.clsMaxAmount').val(RoundNumber(maxAmount,2));
		
		$(obj).parent().parent().find('.clsAdvanceValue').html(RoundNumber(maxAmount,2));
		},
		error:function(xhr,status){
				
			}		
	});
}
function loadAmountAuto(obj,GLAccId)
{
	var supplierId 		= $('#frmSupAdvanceSettlement #cboSupplier').val();
	var currencyId 		= $('#frmSupAdvanceSettlement #cboCurrency').val();
	var advanceNoArr 	= $(obj).parent().parent().find('.clsAdvanceNo') .val();

	$(obj).parent().parent().find('.clsSettleAmt').val(0);	
	
	var url = basePath+"advanceSettlement-db-get.php?requestType=loadMaxAmount";
	var obj = $.ajax({
		url:url,
		dataType: "json",  
		data:"&advanceNoArr="+advanceNoArr+"&GLAccId="+GLAccId+"&supplierId="+supplierId+"&currencyId="+currencyId,
		async:false,
		success:function(json){
			
		var invoiceValue 	= parseFloat($(obj).parent().parent().find('.clsInvoiceAmt').html());
		var paidAmonut 		= parseFloat($(obj).parent().parent().find('.clsPaidAmt').html());
		var advancedAmount 	= parseFloat($(obj).parent().parent().find('.clsAdvanceAmt').html());
		var debitAmount 	= parseFloat($(obj).parent().parent().find('.clsDebitAmt').html());
		var creditAmount 	= parseFloat($(obj).parent().parent().find('.clsCreditAmt').html());
		
		var maxAmount	 	= parseFloat(json.maxAmount);
		
		var balAmonut		= invoiceValue+creditAmount-(paidAmonut+advancedAmount+debitAmount);
		
		if(maxAmount>balAmonut)
			$(obj).parent().parent().find('.clsMaxAmount').val(RoundNumber(balAmonut,2));
		else
			$(obj).parent().parent().find('.clsMaxAmount').val(RoundNumber(maxAmount,2));
		
		$(obj).parent().parent().find('.clsAdvanceValue').html(RoundNumber(maxAmount,2));
		},
		error:function(xhr,status){
				
			}		
	});
}
function loadGLCombo()
{
	var obj		   = this;
	$(this).parent().parent().find('.clsSettleAmt').val(0);
	$(this).parent().parent().find('.clsAdvanceValue').html('&nbsp;');
	$(this).parent().parent().find('.clsMaxAmount').val(0);
	
	if($(this).val()=='')
	{
		$(this).parent().parent().find('.clsLedgerAc').html('');
		return;
	}
	var url = basePath+"advanceSettlement-db-get.php?requestType=loadGLCombo";
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',  
		data:"&advanceNoArr="+$(this).val(),
		async:false,
		success:function(json){
			
			$(obj).parent().parent().find('.clsLedgerAc').html(json.GLCombo);
			loadAmountAuto(obj,$(obj).parent().parent().find('.clsLedgerAc').val());
		},
		error:function(xhr,status){
				
			}		
	});
}
function getAllAdvance()
{
	var supplierId 		= $('#frmSupAdvanceSettlement #cboSupplier').val();
	var currencyId 		= $('#frmSupAdvanceSettlement #cboCurrency').val();
	var PONoArr			= $(this).parent().parent().find('.clsPONo').attr('id');
	var obj				= this;
	
	$(this).parent().parent().find('.clsSettleAmt').val(0);
	$(this).parent().parent().find('.clsAdvanceValue').html('&nbsp;');
	$(this).parent().parent().find('.clsMaxAmount').val(0);
	$(this).parent().parent().find('.clsLedgerAc').html('');
	
	if($(this).attr('checked'))
	{
		var PONoArr = '';
	}
	var url = basePath+"advanceSettlement-db-get.php?requestType=getAllAdvanceNo";
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',  
		data:"&supplierId="+supplierId+"&currencyId="+currencyId+"&PONoArr="+PONoArr,
		async:false,
		success:function(json){
			
			$(obj).parent().parent().find('.clsCboAdvance').html(json.AdvanceCombo);
			
		},
		error:function(xhr,status){
			
		}		
	});
	
}
function chkAdvanceCombo()
{
	var advanceId	= $(this).parent().parent().find('.clsAdvanceNo').val();
	var GLAccId 	= $(this).parent().parent().find('.clsLedgerAc').val();

	if(advanceId=='')
	{
		$(this).parent().parent().find('.clsAdvanceNo').validationEngine('showPrompt','Please select a Advance No.','fail');
		$(this).parent().parent().find('.clsAdvanceNo').focus();
		return;
	}
	else if(GLAccId=='')
	{
		$(this).parent().parent().find('.clsLedgerAc').validationEngine('showPrompt','Please select a GL Account.','fail');
		$(this).parent().parent().find('.clsLedgerAc').focus();
		return;
	}
}
function setMaxAmount()
{
	var maxAmount = parseFloat($(this).parent().parent().find('.clsMaxAmount').val());
	if(parseFloat($(this).val())>maxAmount)
		$(this).val(maxAmount);
}
function saveData()
{
	showWaiting();
	if($('#frmSupAdvanceSettlement').validationEngine('validate'))
	{
		if($(this).parent().parent().find('.clsSettleAmt').val()==0 || $(this).parent().parent().find('.clsSettleAmt').val()=='')
		{
			$(this).parent().parent().find('.clsSettleAmt').validationEngine('showPrompt','Please enter a amonut.','fail');
			hideWaiting();
			return;
		}
		
		var purInvNoArr		= $(this).parent().parent().attr('id');
		var PONo			= $(this).parent().parent().find('.clsPONo').attr('id');
		var invAmount		= $(this).parent().parent().find('.clsInvoiceAmt').html();
		var advanceNoArr	= $(this).parent().parent().find('.clsAdvanceNo').val();
		var GLAccountId		= $(this).parent().parent().find('.clsLedgerAc').val();
		var settleAmonut	= $(this).parent().parent().find('.clsSettleAmt').val();
		var supplierId 		= $('#frmSupAdvanceSettlement #cboSupplier').val();
		var currencyId 		= $('#frmSupAdvanceSettlement #cboCurrency').val();
		var obj  			= this;
		
		var url = basePath+"advanceSettlement-db-set.php?requestType=saveData";
		var obj = $.ajax({
			url:url,
			dataType: "json",  
			type:'POST',
			data:"&purInvNoArr="+purInvNoArr+"&invAmount="+invAmount+"&advanceNoArr="+advanceNoArr+"&GLAccountId="+GLAccountId+"&settleAmonut="+settleAmonut+"&supplierId="+supplierId+"&currencyId="+currencyId+"&PONo="+PONo,
			async:false,
			success:function(json){
				$(obj).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				if(json.type=='pass')
				{
					var t = setTimeout("alertx2()",3000);
					hideWaiting();
					$(obj).parent().parent().find('.clsAdvanceAmt').html(RoundNumber(json.advancedAmt,2));
					$(obj).parent().parent().find('.clsSettleAmt').val(0);
					
					var maxAmount = parseFloat($(obj).parent().parent().find('.clsMaxAmount').val());
					$(obj).parent().parent().find('.clsMaxAmount').val(maxAmount-parseFloat(settleAmonut));
					return;
				}
				else
				{
					hideWaiting();
				}
			},
			error:function(xhr,status){
					
					$(obj).validationEngine('showPrompt', errormsg(xhr.status),'fail');
					hideWaiting();
					return;
				}		
		});
	}
	else
	{
		hideWaiting();
	}
	
}
function clearAll()
{
	document.location.href = "?q="+menuId;
}
function alertx()
{
	$('#frmSupAdvanceSettlement .clsAdvanceNo').validationEngine('hide') ;
}
function alertx1()
{
	$('#frmSupAdvanceSettlement .clsSettleAmt').validationEngine('hide') ;
}
function alertx2()
{
	$('#frmSupAdvanceSettlement .clsSave').validationEngine('hide') ;
}
function alertx3()
{
	$('#frmSupAdvanceSettlement .clsLedgerAc').validationEngine('hide') ;
}


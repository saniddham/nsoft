<?php 
session_start();
//ini_set('display_errors',1);
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$requestType 		= $_REQUEST['requestType'];

include "../../../../dataAccess/Connector.php";
require_once "../../../../class/cls_commonFunctions_get.php";
require_once  "../../../../class/finance/cash_flow/cls_cf_common.php";
require_once  "../../../../class/finance/cash_flow/cls_cf_common_set.php";

$obj_commonfunc_get	= new cls_commonFunctions_get($db);
$obj_cf_common		= new cls_cf_common($db);
$obj_cf_common_set	= new cls_cf_common_set($db);

if($requestType=='saveData')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
		
	$purInvNoArr 	= explode('/',$_REQUEST['purInvNoArr']);
	$purInvNo    	= $purInvNoArr[0];
	$purInvYear  	= $purInvNoArr[1];
	$advanceNoArr	= explode('/',$_REQUEST['advanceNoArr']);
	$advanceNo    	= $advanceNoArr[0];
	$advanceYear  	= $advanceNoArr[1];
	$PONoArr		= explode('/',$_REQUEST['PONo']);
	$PONo    		= $PONoArr[0];
	$POYear  		= $PONoArr[1];
	$GLAccountId	= $_REQUEST['GLAccountId'];
	$settleAmonut	= $_REQUEST['settleAmonut'];
	$supplierId		= $_REQUEST['supplierId'];
	$currencyId		= $_REQUEST['currencyId'];
	$invAmount		= trim($_REQUEST['invAmount']);
	$rollBackFlag 	= 0;
	$bankRefNo		= 0;
	
	$response_arr	= validateBeforeSave($advanceNo,$advanceYear,$supplierId,$currencyId,$GLAccountId,$settleAmonut);
	
	if($response_arr['type'] == 'fail')
	{
		$rollBackFlag = 1;
		$rollBackMsg  = $response_arr['msg'];
	}
	
	$balToPaid		  = getBalanceToPaid($purInvNo,$purInvYear,$supplierId,$currencyId,$companyId);
	if($settleAmonut>$balToPaid && $rollBackFlag==0)
	{
		$rollBackFlag = 1;
		$rollBackMsg  = "Invoice Balance Exceed Settle Amount </br>Settle Amonut : ".$settleAmonut."</br>Invoice Balance Amonut : ".$balToPaid;
	}
	
	$data['arrData'] = $obj_commonfunc_get->GetSystemMaxNo('intAdvanceSettleNo',$locationId);
	if(($data['arrData']['rollBackFlag']==1) && ($rollBackFlag==0))
	{
		$rollBackFlag		= 1;
		$rollBackMsg		= $data['arrData']['msg'];
	}
	else
	{
		$advSettleNo		= $data['arrData']['max_no'];
		$advSettleYear		= date('Y');
	}
	
	$responceArr 	= saveHederData($advSettleNo,$advSettleYear,$purInvNo,$purInvYear,$advanceNo,$advanceYear,$PONo,$POYear,$GLAccountId,$settleAmonut,$supplierId,$currencyId);

	if(($responceArr['savedStatus']=='fail') && ($rollBackFlag==0))
	{
		$rollBackFlag		= 1;
		$rollBackMsg		= $responceArr['savedMassege'];
		$q					= $responceArr['error_sql'];
	}
	
	$sql = "SELECT * FROM finance_supplier_transaction 
			WHERE DOCUMENT_NO='$advanceNo' AND 
			DOCUMENT_YEAR='$advanceYear' AND 
			DOCUMENT_TYPE='ADVANCE' AND
			LEDGER_ID='$GLAccountId' AND
			COMPANY_ID='$companyId' AND
			SUPPLIER_ID='$supplierId' AND
			CURRENCY_ID='$currencyId' ";
	
	$result = $db->RunQuery2($sql);
	$row    = mysqli_fetch_array($result);
	
	$value	= ($settleAmonut*-1);
	$sqlIns = "INSERT INTO finance_supplier_transaction 
				(
				PO_YEAR, PO_NO, SUPPLIER_ID, 
				CURRENCY_ID, DOCUMENT_YEAR, DOCUMENT_NO, 
				DOCUMENT_TYPE, PURCHASE_INVOICE_YEAR, PURCHASE_INVOICE_NO, LEDGER_ID, VALUE, COMPANY_ID, 
				LOCATION_ID, USER_ID, TRANSACTION_DATE_TIME
				)
				VALUES
				( 
				'".$row['PO_YEAR']."', '".$row['PO_NO']."', '$supplierId', 
				'$currencyId', '$advanceYear', '$advanceNo', 
				'ADVANCE', '$purInvYear', '$purInvNo', '$GLAccountId', '$value', '$companyId', 
				'$locationId', '$userId', now()
				);";
	$resultIns = $db->RunQuery2($sqlIns);
	if(!$resultIns)
	{
		$rollBackFlag	= 1;
		$rollBackMsg	= $db->errormsg;
	}
	else
	{
		$sqlUpd = "UPDATE finance_supplier_transaction 
					SET 
					VALUE = VALUE+$settleAmonut 
					WHERE
					DOCUMENT_YEAR='$advanceYear' AND
					DOCUMENT_NO='$advanceNo' AND
					DOCUMENT_TYPE='ADVANCE' AND
					LEDGER_ID='$GLAccountId' AND
					SUPPLIER_ID='$supplierId' AND
					CURRENCY_ID='$currencyId' AND
					COMPANY_ID='$companyId' AND
					PURCHASE_INVOICE_YEAR is NULL AND
					PURCHASE_INVOICE_NO is NULL";
		$resultUpd = $db->RunQuery2($sqlUpd);
		if(!$resultUpd)
		{
			$rollBackFlag	= 1;
			$rollBackMsg	= $db->errormsg;
		}
	}
	if($rollBackFlag==0)
	{
		$sql = "SELECT (SUM(VALUE)*-1) AS advancedAmt
				FROM finance_supplier_transaction
				WHERE DOCUMENT_TYPE='ADVANCE' AND
				PURCHASE_INVOICE_NO='$purInvNo' AND
				PURCHASE_INVOICE_YEAR='$purInvYear' AND
				SUPPLIER_ID='$supplierId' AND
				CURRENCY_ID='$currencyId' ";
		
		$result = $db->RunQuery2($sql); 
		$row = mysqli_fetch_array($result);
		$advancedAmt = $row['advancedAmt'];
		
		/*$sql = "SELECT * FROM finance_transaction
				WHERE INVOICE_NO='$purInvNo' AND
				INVOICE_YEAR='$purInvYear' AND
				TRANSACTION_TYPE='D' AND
				DOCUMENT_TYPE='INVOICE' AND
				TRANSACTION_CATEGORY_ID='$supplierId' ";
		
		$result = $db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result))
		{
			$bankRefNo = $row['BANK_REFERENCE_NO'];
			$dataSvat['arrData'] = getGLData($supplierId);
			if($dataSvat['arrData']['invoiceType']==3 && $dataSvat['arrData']['chartOfAccId']==$row['CHART_OF_ACCOUNT_ID'])
			{
				$svatAmount			 = getSvatValue($row['AMOUNT']);
				//$data['finalResult'] = insertGLData('D',$row['CHART_OF_ACCOUNT_ID'],$svatAmount,$advanceNo,$advanceYear,$purInvNo,$purInvYear,$currencyId,$bankRefNo,$supplierId);
				/*if($data['finalResult']['rollBackFlag']==0)
				{
					$data['finalResult'] = insertGLData('C',$row['CHART_OF_ACCOUNT_ID'],$svatAmount,$advanceNo,$advanceYear,$purInvNo,$purInvYear,$currencyId,$bankRefNo,$supplierId);
					if($data['finalResult']['rollBackFlag']==1)
					{
						$rollBackFlag	= 1;
						$rollBackMsg	= $data['finalResult']['msg'];
						$q				= $data['finalResult']['q'];
					}	
				}
				else
				{
					$rollBackFlag	= 1;
					$rollBackMsg	= $data['finalResult']['msg'];
					$q				= $data['finalResult']['q'];
				}	
			}
			else
			{
				$ledgerAmount 		 = $row['AMOUNT'];
				$newLedgerAmt 		 = round(($ledgerAmount/$invAmount)*$settleAmonut,2);
				$data['finalResult'] = insertGLData('D',$row['CHART_OF_ACCOUNT_ID'],$newLedgerAmt,$advanceNo,$advanceYear,$purInvNo,$purInvYear,$currencyId,$bankRefNo,$supplierId);
				if($data['finalResult']['rollBackFlag']==1)
				{
					$rollBackFlag	= 1;
					$rollBackMsg	= $data['finalResult']['msg'];
					$q				= $data['finalResult']['q'];
				}
			}
		}
		$dataSGL['arrData'] = getGLData($supplierId);
		$data['finalResult'] = insertGLData('C',$dataSGL['arrData']['supplierGLId'],$settleAmonut,$advanceNo,$advanceYear,$purInvNo,$purInvYear,$currencyId,$bankRefNo,$supplierId);
		if($data['finalResult']['rollBackFlag']==1)
		{
			$rollBackFlag	= 1;
			$rollBackMsg	= $data['finalResult']['msg'];
			$q				= $data['finalResult']['q'];
		}*/
	}
	
	//-------------------------- cash flow update 27/05/2014 by lahiru --------------------------------
		
	$CFUpdResult	= getDetailData($advSettleNo,$advSettleYear);
	while($row = mysqli_fetch_array($CFUpdResult))
	{
		$amount	   		= $row['SETTLE_AMOUNT'];	
		$currencyId		= $row['CURRENCY_ID'];
		$entryDate		= $row['SETTLE_DATE'];
		$company		= $row['COMPANY_ID'];
		$location		= $row['LOCATION_ID'];
		//$rpt_field		= $row['REPORT_FIELD_ID'];
		$purInvNo		= $row['PURCHASE_INVOICE_NO'];
		$purInvYear		= $row['PURCHASE_INVOICE_YEAR'];
		
		$tot_to_save	= $amount;
		
		$resultInvWise	= getInvoicedDetails($purInvNo,$purInvYear);
		while($rowInv = mysqli_fetch_array($resultInvWise))
		{
			$rpt_field		= $rowInv['REPORT_FIELD_ID'];
			$invAmount	   	= $rowInv['invoiceAmt'];
			$invoiceDate	= $rowInv['INVOICE_DATE'];
			
			if($tot_to_save>0)
			{
				if($invAmount<=$tot_to_save)
				{
					$saveInvAmount 	 	= $invAmount;
					$tot_to_save 		= $tot_to_save-$invAmount;
				}
				else if($invAmount>$tot_to_save)
				{
					$saveInvAmount 		= $tot_to_save;
					$tot_to_save		= 0;
				}
				$cfAmountResult	= $obj_cf_common->get_cf_log_bal_to_payment_result($location,$rpt_field,$currencyId,$entryDate,$company,'RunQuery2');
				while($rowAmt = mysqli_fetch_array($cfAmountResult))
				{
					$balAmount	= round($rowAmt['BAL'],2);
					if(($saveInvAmount>0) && ($balAmount>0))
					{
						if($saveInvAmount<=$balAmount)
						{
							$saveAmnt 	 	= $saveInvAmount;
							$saveInvAmount 	= 0;
						}
						else if($saveInvAmount>$balAmount)
						{
							$saveAmnt 		= $balAmount;
							$saveInvAmount	= $saveInvAmount-$saveAmnt;
						}
						
						$rcv_date	= $rowAmt['RECEIVE_DATE'];
						$currency	= $rowAmt['CURRENCY_ID'];
						
						$resultArr	= $obj_cf_common_set->insert_to_log($location,$rpt_field,$rcv_date,$invoiceDate,$entryDate,$advSettleNo,$advSettleYear,'ADV_PAY_SETTLE',$saveAmnt,$currency,'RunQuery2');
						if($resultArr['type']=='fail' && $rollBackFlag==0)
						{
							$rollBackFlag	= 1;
							$rollBackMsg	= $resultArr['msg'];
							$q				= $resultArr['sql'];
						}
					}
				}
			}
		}
		if($saveInvAmount>0)
		{
			if($rcv_date == '')
				$rcv_date = $entryDate;
			
			if($currency == '')
				$currency = $currencyId;
							
			$resultInvWise	= getInvoicedDetails($purInvNo,$purInvYear);
			while($rowInv = mysqli_fetch_array($resultInvWise))
			{
				$rpt_field		= $rowInv['REPORT_FIELD_ID'];
				$invAmount	   	= $rowInv['invoiceAmt'];
				$invoiceDate	= $rowInv['INVOICE_DATE'];
				
				if($saveInvAmount>0)
				{
					if($invAmount<=$saveInvAmount)
					{
						$saveAmnt 	 	= $invAmount;
						$saveInvAmount 	= $saveInvAmount-$invAmount;
					}
					else if($invAmount>$saveInvAmount)
					{
						$saveAmnt 		= $saveInvAmount;
						$saveInvAmount	= 0;
					}
					
					$resultArr	= $obj_cf_common_set->insert_to_log($location,$rpt_field,$rcv_date,$invoiceDate,$entryDate,$advSettleNo,$advSettleYear,'ADV_PAY_SETTLE',$saveAmnt,$currency,'RunQuery2');
					if($resultArr['type']=='fail' && $rollBackFlag==0)
					{
						$rollBackFlag	= 1;
						$rollBackMsg	= $resultArr['msg'];
						$q				= $resultArr['sql'];
					}
				}
			}
		}
		if($tot_to_save>0)
		{
			if($rcv_date == '')
				$rcv_date = $entryDate;
							
			$resultInvWise	= getInvoicedDetails($purInvNo,$purInvYear);
			while($rowInv = mysqli_fetch_array($resultInvWise))
			{
				$rpt_field		= $rowInv['REPORT_FIELD_ID'];
				$invAmount	   	= $rowInv['invoiceAmt'];
				$invoiceDate	= $rowInv['INVOICE_DATE'];
				
				if($tot_to_save>0)
				{	
					if($invAmount<=$tot_to_save)
					{
						$saveAmnt 	 	= $invAmount;
						$tot_to_save 	= $tot_to_save-$invAmount;
					}
					else if($invAmount>$tot_to_save)
					{
						$saveAmnt 		= $tot_to_save;
						$tot_to_save	= 0;
					}
					
					$resultArr	= $obj_cf_common_set->insert_to_log($location,$rpt_field,$rcv_date,$invoiceDate,$entryDate,$advSettleNo,$advSettleYear,'ADV_PAY_SETTLE',$saveAmnt,$currencyId,'RunQuery2');
					if($resultArr['type']=='fail' && $rollBackFlag==0)
					{
						$rollBackFlag	= 1;
						$rollBackMsg	= $resultArr['msg'];
						$q				= $resultArr['sql'];
					}
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------
	$resultArr	= $obj_commonfunc_get->validateDuplicateSerialNoWithSysNo($advSettleNo,'intAdvanceSettleNo',$locationId);
	if($resultArr['type']=='fail' && $rollBackFlag==0)
	{
		$rollBackFlag	= 1;
		$rollBackMsg	= $resultArr['msg'];
	}
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		 = 'fail';
		$response['msg'] 		 = $rollBackMsg;
		$response['q'] 		 	 = $q;
	}
	else if($rollBackFlag==0)
	{
		$db->RunQuery2('Commit');
		$response['type'] 		 = 'pass';
		$response['msg'] 		 = 'Saved successfully.';
		$response['advancedAmt'] = $advancedAmt;
	}
	else
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
		$response['q'] 		 	 = $q;
	}
	$db->CloseConnection();
	echo json_encode($response);
}
function insertGLData($transationType,$GLId,$newLedgerAmt,$advanceNo,$advanceYear,$purInvNo,$purInvYear,$currencyId,$banRefNo,$supplierId)
{
	global $db;
	global $locationId;
	global $companyId;
	global $userId;

	$sql = "INSERT INTO finance_transaction 
			(
			CHART_OF_ACCOUNT_ID, 
			AMOUNT, DOCUMENT_NO, DOCUMENT_YEAR, 
			DOCUMENT_TYPE, INVOICE_NO, INVOICE_YEAR, 
			TRANSACTION_TYPE, TRANSACTION_CATEGORY, TRANSACTION_CATEGORY_ID, 
			CURRENCY_ID, BANK_REFERENCE_NO, LOCATION_ID, COMPANY_ID, LAST_MODIFIED_BY
			)
			VALUES
			(
			'$GLId', 
			'$newLedgerAmt', '$advanceNo', '$advanceYear', 
			'ADVANCE', '$purInvNo', '$purInvYear', 
			'$transationType', 'SU', '$supplierId', 
			'$currencyId', '$banRefNo', '$locationId', '$companyId', '$userId'
			);";
	
	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$data['rollBackFlag'] 	= 1 ;
		$data['msg'] 			= $db->errormsg;
		$data['q'] 				= $sql ;
	}
	else
	{
		$data['rollBackFlag'] 	= 0 ;
	}
	return $data;
}
function getGLData($supplierId)
{
	global $db;
	$sql = "SELECT IFNULL(intInvoiceType,0) AS invoiceType ,
			(SELECT CHART_OF_ACCOUNT_ID FROM finance_mst_chartofaccount_invoice_type IT
			WHERE IT.TRANSACTION_CATEGORY='SU' AND
			IT.TRANSACTION_TYPE='D' AND
			IT.INVOICE_TYPE_ID=MS.intInvoiceType) chartOfAccId,
			(SELECT CHART_OF_ACCOUNT_ID FROM finance_mst_chartofaccount
			WHERE CATEGORY_TYPE='S' AND
			CATEGORY_ID='153' AND
			STATUS=1) supplierGLId
			FROM mst_supplier MS
			WHERE intId='$supplierId' ";	
						
	$result = $db->RunQuery2($sql);
	$row 	= mysqli_fetch_array($result);
	$data['invoiceType'] 	= $row['invoiceType'] ;
	$data['chartOfAccId'] 	= $row['chartOfAccId'] ;
	$data['supplierGLId'] 	= $row['supplierGLId'] ;
	
	return $data;
}
function getSvatValue($value)
{
	global $db;
	
	$sql = "SELECT dblRate FROM mst_financetaxisolated WHERE intId='3' ";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	
	$svatValue = $value*($row['dblRate']/100);
	return $svatValue;
}
function validateBeforeSave($advanceNo,$advanceYear,$supplierId,$currencyId,$GLAccountId,$settleAmonut)
{
	global $db;
	
	$sql = "SELECT IFNULL((SUM(VALUE)*-1),0) AS totAmount
			FROM finance_supplier_transaction
			WHERE DOCUMENT_NO='$advanceNo' AND
			DOCUMENT_YEAR='$advanceYear' AND
			SUPPLIER_ID='$supplierId' AND
			CURRENCY_ID='$currencyId' AND
			DOCUMENT_TYPE='ADVANCE' AND
			LEDGER_ID='$GLAccountId' AND
			PURCHASE_INVOICE_YEAR IS NULL AND
			PURCHASE_INVOICE_NO IS NULL ";
	
	$result 				= $db->RunQuery2($sql);
	$row 					= mysqli_fetch_array($result);
	$data['type'] 			= 'pass';
	
	if($settleAmonut>$row['totAmount'])
	{
		$data['type'] 		= 'fail';
		$data['msg'] 		= 'Settlement amount greater than Advance amount.' ;
	}
	return $data;
}
function saveHederData($advSettleNo,$advSettleYear,$purInvNo,$purInvYear,$advanceNo,$advanceYear,$PONo,$POYear,$GLAccountId,$settleAmonut,$supplierId,$currencyId)
{
	global $db;
	global $companyId;
	global $locationId;
	global $userId;
	
	$sql = "INSERT INTO finance_supplier_settlement_header 
			(
			SETTLE_NO, 
			SETTLE_YEAR, 
			SUPPLIER_NO, 
			CURRENCY_ID, 
			INVOICE_NO, 
			INVOICE_YEAR, 
			PO_NO, 
			PO_YEAR, 
			ADVANCE_NO, 
			ADVANCE_YEAR, 
			CHAT_OF_ACCOUNT_ID, 
			SETTLE_AMOUNT, 
			SETTLE_DATE, 
			COMPANY_ID, 
			LOCATION_ID, 
			CREATED_BY, 
			CREATED_DATE
			)
			VALUES
			(
			'$advSettleNo', 
			'$advSettleYear', 
			'$supplierId', 
			'$currencyId', 
			'$purInvNo', 
			'$purInvYear', 
			'$PONo', 
			'$POYear', 
			'$advanceNo', 
			'$advanceYear', 
			'$GLAccountId', 
			'$settleAmonut', 
			 NOW(), 
			'$companyId', 
			'$locationId', 
			'$userId', 
			NOW()
			)";

	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $db->errormsg;
		$data['error_sql']		= $sql;
	}
	return $data;	
}
function getDetailData($advSettleNo,$advSettleYear)
{
	global $db;
	$sql = "SELECT
			SSH.COMPANY_ID,
			CFCP.REPORT_FIELD_ID, 
			SSH.CURRENCY_ID, 
			SSH.LOCATION_ID, 
			SPID.ITEM_ID ,  
			SSH.SETTLE_AMOUNT, 
			SSH.SETTLE_DATE,
			SPID.PURCHASE_INVOICE_NO,
			SPID.PURCHASE_INVOICE_YEAR
			
			FROM
			finance_supplier_settlement_header SSH
			INNER JOIN finance_supplier_purchaseinvoice_details SPID ON SPID.PURCHASE_INVOICE_NO=SSH.INVOICE_NO
			AND SPID.PURCHASE_INVOICE_YEAR=SSH.INVOICE_YEAR
			INNER JOIN finance_cashflow_cpanal CFCP ON FIND_IN_SET(SPID.ITEM_ID,CFCP.PO_ITEM_ID)
			WHERE
			CFCP.BOO_PO_RAISED = 1 AND 
			SSH.SETTLE_NO = '$advSettleNo' AND 
			SSH.SETTLE_YEAR = '$advSettleYear' ";
	
	$result = $db->RunQuery2($sql);
	return $result;	
}
function getInvoicedDetails($purInvNo,$purInvYear)
{
	global $db;
	$sql = "SELECT
			CFCP.REPORT_FIELD_ID, 
			ROUND((((SPID.QTY*SPID.UNIT_PRICE*(100-IFNULL(SPID.DISCOUNT,0)))/100)+SPID.TAX_AMOUNT),2) AS invoiceAmt	,
			SPIH.PURCHASE_DATE as INVOICE_DATE		
			FROM
			finance_supplier_purchaseinvoice_header SPIH
			INNER JOIN finance_supplier_purchaseinvoice_details SPID ON SPIH.PURCHASE_INVOICE_NO = SPID.PURCHASE_INVOICE_NO AND 
			SPIH.PURCHASE_INVOICE_YEAR = SPID.PURCHASE_INVOICE_YEAR
			INNER JOIN finance_cashflow_cpanal CFCP ON FIND_IN_SET(SPID.ITEM_ID,CFCP.PO_ITEM_ID)
			INNER JOIN ware_grnheader GH ON GH.strInvoiceNo=SPIH.INVOICE_NO
			INNER JOIN trn_poheader PO ON PO.intPONo=GH.intPoNo AND PO.intPOYear=GH.intPoYear
			INNER JOIN mst_financepaymentsterms FPT ON FPT.intId=PO.intPaymentTerm
			WHERE
			CFCP.BOO_PO_RAISED = 1 AND 
			SPID.PURCHASE_INVOICE_NO = '$purInvNo' AND
			SPID.PURCHASE_INVOICE_YEAR = '$purInvYear' ";
	
	$result = $db->RunQuery2($sql);
	return $result;	
}
function getBalanceToPaid($purInvNo,$purInvYear,$supplierId,$currencyId,$companyId)
{
	global $db;
	$sql = "SELECT ROUND(SUM(ST.VALUE),2) AS toBePaid
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO='$purInvNo' AND
			ST.PURCHASE_INVOICE_YEAR='$purInvYear' AND
			ST.CURRENCY_ID='$currencyId' AND
			ST.COMPANY_ID='$companyId' AND
			ST.SUPPLIER_ID='$supplierId' ";
	
	$result = $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['toBePaid'];	
}
?>
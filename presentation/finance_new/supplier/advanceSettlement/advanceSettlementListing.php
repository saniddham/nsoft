<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId			= $_SESSION["headCompanyId"];

$reportId			= 963;

require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'advanceSettleNo'=>"CONCAT(SSH.SETTLE_NO,' / ',SSH.SETTLE_YEAR)",
				'invoiceNo'=>"CONCAT(SSH.INVOICE_NO,' / ',SSH.INVOICE_YEAR)",
				'PONo'=>"CONCAT(SSH.PO_NO,' / ',SSH.PO_YEAR)",
				'avanceNo'=>"CONCAT(SSH.ADVANCE_NO,' / ',SSH.ADVANCE_YEAR)",
				'SUPPLIER_NO'=>'SSH.SUPPLIER_NO',
				'CURRENCY_ID'=>'SSH.CURRENCY_ID',
				'CHAT_OF_ACCOUNT_ID'=>'SSH.CHAT_OF_ACCOUNT_ID'
				);
				
foreach($arr as $k=>$v)
{
	if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND SSH.SETTLE_DATE = '".date('Y-m-d')."'";
	
// }

$sql = "SELECT SUB_1.* FROM
		(
			SELECT 	SSH.SETTLE_NO,
			SSH.SETTLE_YEAR,
			CONCAT(SSH.SETTLE_NO,' / ',SSH.SETTLE_YEAR) AS advanceSettleNo,
			CONCAT(SSH.INVOICE_NO,' / ',SSH.INVOICE_YEAR) as invoiceNo,
			CONCAT(SSH.PO_NO,' / ',SSH.PO_YEAR) as PONo,
			CONCAT(SSH.ADVANCE_NO,' / ',SSH.ADVANCE_YEAR) as avanceNo,
			round(SSH.SETTLE_AMOUNT,2) as Amonut, 
			SSH.SETTLE_DATE,
			SSH.SUPPLIER_NO,
			SSH.CURRENCY_ID,
			FC.strCode AS currency,
			SSH.CHAT_OF_ACCOUNT_ID,
			(
				SELECT CONCAT(FCOA.CHART_OF_ACCOUNT_NAME,' | ',FMT.FINANCE_TYPE_CODE,FMMT.MAIN_TYPE_CODE,FMST.SUB_TYPE_CODE,FCOA.CHART_OF_ACCOUNT_CODE)
				FROM finance_mst_chartofaccount FCOA
				INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
				INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
				INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
				WHERE FCOA.CHART_OF_ACCOUNT_ID = SSH.CHAT_OF_ACCOUNT_ID
			) AS chartOfAccount,
			MS.strName AS supplier,
			'View' AS VIEW
			FROM 
			finance_supplier_settlement_header AS SSH
			INNER JOIN mst_financecurrency FC ON FC.intId=SSH.CURRENCY_ID
			INNER JOIN mst_supplier MS ON MS.intId=SSH.SUPPLIER_NO
			INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=SSH.CHAT_OF_ACCOUNT_ID
			WHERE SSH.COMPANY_ID = '$companyId'
			$where_string
		)  
		AS SUB_1 WHERE 1=1";

$jq 	= new jqgrid('',$db);	
$col 	= array();
$cols 	= array();

$col["title"] 			= "AdvanceSettle No";
$col["name"] 			= "SETTLE_NO";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "AdvanceSettle Year";
$col["name"] 			= "SETTLE_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "AdvanceSettle No";
$col["name"] 			= "advanceSettleNo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Invoice No";
$col["name"] 			= "invoiceNo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "PO No";
$col["name"] 			= "PONo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Advance No";
$col["name"] 			= "avanceNo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Supplier";
$col["name"] 			= "supplier";
$col["width"] 			= "7";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["dbname"] 			= "SUPPLIER_NO";
$supplier_lookup 		= $jq->get_dropdown_values("SELECT DISTINCT 
													intId AS k,
													strName AS v
													FROM mst_supplier
													WHERE intStatus = 1
													ORDER BY strName");
$col["stype"] 			= "select";
$str 					= ":;".$supplier_lookup;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "currency";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["dbname"] 			= "CURRENCY_ID";
$currency_lookup 		= $jq->get_dropdown_values("SELECT DISTINCT 
													intId AS k,
													strCode AS v
													FROM mst_financecurrency
													WHERE intStatus = 1
													ORDER BY intId");
$col["stype"] 			= "select";
$str 					= ":;".$currency_lookup;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;
$col					= NULL;

$col["title"] 				= "GL Account";
$col["name"] 				= "chartOfAccount";
$col["width"] 				= "7"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "CHAT_OF_ACCOUNT_ID";
$client_lookup 				= $jq->get_dropdown_values("SELECT DISTINCT 
														FCOA.CHART_OF_ACCOUNT_ID AS k,
														CONCAT(FCOA.CHART_OF_ACCOUNT_NAME,' | ',FMT.FINANCE_TYPE_CODE,FMMT.MAIN_TYPE_CODE,FMST.SUB_TYPE_CODE,FCOA.CHART_OF_ACCOUNT_CODE) AS v
														
														FROM finance_mst_chartofaccount FCOA
														INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
														INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
														INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
														INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
														WHERE FCOAC.COMPANY_ID = '$companyId' AND FCOA.BANK = '1'
														ORDER BY CHART_OF_ACCOUNT_NAME ");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;

$col["title"] 			= "Settle Date";
$col["name"] 			= "SETTLE_DATE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Amount";
$col["name"] 			= "Amonut";
$col["width"] 			= "3";
$col["align"] 			= "right"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "View";
$col["name"] 			= "VIEW";
$col["width"] 			= "1";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= '?q='.$reportId.'&advSettleNo={SETTLE_NO}&advSettleYear={SETTLE_YEAR}';
$col["linkoptions"] 	= "target='rptAdvanceSettlement.php'";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Advance Settlement Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'SETTLE_YEAR,SETTLE_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Advance Settlement Listing</title>
<?php
echo $out;
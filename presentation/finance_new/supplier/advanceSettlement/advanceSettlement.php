<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$userId 			= $_SESSION['userId'];

include_once "class/masterData/exchange_rate/cls_exchange_rate_get.php";
//include 	 "include/javascript.html";

$obj_exchgRate_get	= new cls_exchange_rate_get($db);

$supplierId  		= (!isset($_REQUEST['supplierId'])?'':$_REQUEST['supplierId']);
$currencyId  		= (!isset($_REQUEST['currencyId'])?'':$_REQUEST['currencyId']);
$programCode 		= 'P0778';

$row				= $obj_exchgRate_get->GetAllValues($currencyId,4,date('Y-m-d'),$companyId,'RunQuery');
$exchgRate			= $row['AVERAGE_RATE'];

//$cancleMode  = loadCancleMode($programCode,$userId);
$saveMode  	 = loadSaveMode($programCode,$userId);

?>
<title>Advance Payment Settlement</title>
<script type="text/javascript">
saveMode	= '<?php echo $saveMode;?>';
</script>
<!--<script type="text/javascript" src="presentation/finance_new/supplier/advanceSettlement/advanceSettlement-js.js"></script>-->

<form id="frmSupAdvanceSettlement" name="frmSupAdvanceSettlement" method="post">
<div align="center">
<div class="trans_layoutXL">
<div class="trans_text">Advance Payment Settlement</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
              <td width="11%" class="normalfnt">Supplier <span class="compulsoryRed">*</span></td>
              <td width="31%" valign="top" class="normalfnt"><select name="cboSupplier" id="cboSupplier"  style="width:270px" class="validate[required]" >
                <?php
				
					$sql = "SELECT distinct PO.intSupplier,MS.strName
							FROM ware_grnheader GH
							INNER JOIN mst_locations ML ON ML.intId=GH.intCompanyId
							INNER JOIN trn_poheader PO ON GH.intPoNo=PO.intPONo AND GH.intPoYear=PO.intPOYear
							INNER JOIN mst_supplier MS ON PO.intSupplier=MS.intId
							WHERE GH.intStatus=1 AND
							ML.intCompanyId='$companyId'
							ORDER BY MS.strName ";
					$result = $db->RunQuery($sql);
					echo "<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						if($row['intSupplier']==$supplierId)
							echo "<option value=\"".$row['intSupplier']."\" selected=\"selected\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intSupplier']."\" >".$row['strName']."</option>"; 
					}
			  ?>          
                </select></td>
              <td width="11%" class="normalfnt">Currency <span class="compulsoryRed">*</span></td>
              <td width="17%" class="normalfnt"><select name="cboCurrency" id="cboCurrency"  style="width:100px" class="validate[required]" >
                <option value=""></option>
                <?php
				$sql = "SELECT intId,strCode FROM mst_financecurrency WHERE intStatus=1 ";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($row['intId']==$currencyId)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";
					else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
				}
			  ?>
                </select></td>
              <td width="10%" class="normalfnt">Rate</td>
              <td width="20%" class="normalfnt"><input type="text" name="txtCurrencyRate" id="txtCurrencyRate" style="width:100px" disabled="disabled" value="<?php echo($currencyId==''?'0.0000':$exchgRate); ?>" /></td>
            </tr>
            <tr>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt" valign="top">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="6" class="normalfnt">
                <table width="100%" class="bordered" id="tblMain" >
                  <tr>
                    <th width="8%" >Invoice No</th>
                    <th width="8%"  >PO No </th>
                    <th width="8%" >Invoice Amt</th>
                    <th width="8%" >Paid Amt</th>
                    <th width="8%" >Debit Note</th>
                    <th width="8%" >Credit Note</th>
                    <th width="8%" >Adv Settle Amt</th>
                    <th width="8%" >Adv Amt</th>
                    <th width="2%" >Load All</th>
                    <th width="11%" >Advance No <span class="compulsoryRed">*</span></th>
                    <th width="20%" >GL Account <span class="compulsoryRed">*</span></th>
                    <th width="7%" >Settle Amt <span class="compulsoryRed">*</span></th>
                    <th width="4%"  >Save</th>
                    <!--<th width="5%"  >Cancel</th>-->
                    </tr>
                    <?php
					if($supplierId!='' && $currencyId!='')
					{
						$result = getData($supplierId,$currencyId);
						while($row=mysqli_fetch_array($result))
						{
						?>
                        <tr id="<?php echo $row['PURCHASE_INVOICE_NO'].'/'.$row['PURCHASE_INVOICE_YEAR']; ?>">
                        <td class="normalfnt clsInvoiceNo" style="text-align:center"><?php echo $row['INVOICE_NO']; ?></td>
                        <td class="normalfnt clsPONo" style="text-align:center" id="<?php echo $row['intPoNo'].'/'.$row['intPoYear']; ?>"><?php echo $row['intPoNo'].' - '.$row['intPoYear']; ?></td>
                        <td class="normalfnt clsInvoiceAmt" style="text-align:right"><?php echo number_format($row['invoiceAmount'],2,'.',''); ?></td>
                        <td class="normalfnt clsPaidAmt" style="text-align:right"><?php echo number_format($row['paidAmount'],2,'.',''); ?></td>
                        <td class="normalfnt clsDebitAmt" style="text-align:right"><?php echo number_format($row['debitAmount'],2,'.',''); ?></td>
                        <td class="normalfnt clsCreditAmt" style="text-align:right"><?php echo number_format($row['creditAmount'],2,'.',''); ?></td>
                        <td class="normalfnt clsAdvanceAmt" style="text-align:right"><?php echo number_format($row['advancedAmount'],2,'.',''); ?></td>
                        <td class="normalfnt clsAdvanceValue" style="text-align:right">&nbsp;</td>
                        <td class="normalfnt" style="text-align:center"><input type="checkbox" id="chkAdvanceAll" name="chkItem" class="clsChkAdvanceAll"></td>
                        <td class="normalfnt clsCboAdvance" style="text-align:center"><select name="cboAdvanceNo" id="cboAdvanceNo" class="clsAdvanceNo" style="width:100%">
                        <option value=""></option>
                        <?php
							$resultA = getAdvanceCombo($supplierId,$currencyId,$row['intPoNo'],$row['intPoYear']);
							while($rowA=mysqli_fetch_array($resultA))
							{
                            	if($rowA['advancedValue']!=0)
									echo "<option value=\"".$rowA['ADVANCE_PAYMENT_NO'].'/'.$rowA['ADVANCE_PAYMENT_YEAR']."\" >".$rowA['ADVANCE_PAYMENT_NO'].'/'.$rowA['ADVANCE_PAYMENT_YEAR'].' - '.$row['intPoNo'].'/'.$row['intPoYear']."</option>";
							}
						?>
                        </select>
						</td>
                       <td class="normalfnt" style="text-align:center"><select style="width:100%" class="clsLedgerAc" id="cboLedgerAc" name="cboLedgerAc" ></select></td>
                       <td class="normalfnt" style="text-align:center"><input type="textbox" id="txtSettleAmt" class="clsSettleAmt validate[custom[number]]" style="width:90px;text-align:right" value="0"><input type="hidden" id="txtMaxAmount" class="clsMaxAmount" value="0"></td>
                       <td class="normalfnt" style="text-align:center"><?php echo($saveMode!=1?'&nbsp;':'<a class="button green small clsSave" id="butSave" name="butSave">&nbsp;Save&nbsp;</a>'); ?></td>
                      <!-- <td class="normalfnt" style="text-align:center"><?php echo($cancleMode!=1?'&nbsp;':'<a class="button green small clsCancel" id="butCancel" name="butCancel">&nbsp;Cancel&nbsp;</a>');?></td>-->
                        </tr>
                        <?php
						}
					}
					?>
                </table>
                </td>
            </tr>
            </table>
        </td>
    </tr>
    
    <tr>
    	<td height="32" >
    		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td align="center" class="tableBorder_allRound"><a class="button white medium" id="butNew" name="butNew">&nbsp;New&nbsp;</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
              </tr>
            </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>
<?php
function getData($supplierId,$currencyId)
{
	global $db;
	
	$sql = "SELECT SPIH.PURCHASE_INVOICE_NO,
			SPIH.PURCHASE_INVOICE_YEAR,
			SPIH.INVOICE_NO,
			GH.intPoNo,
			GH.intPoYear,
			
			(SELECT ROUND(SUM(ST.VALUE),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='INVOICE') AS invoiceAmount,
			
			IFNULL((SELECT ROUND((SUM(ST.VALUE)*-1),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='PAYMENT'),0) AS paidAmount,
			
			IFNULL((SELECT ROUND((SUM(ST.VALUE)*-1),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='DEBIT'),0) AS debitAmount,
			
			IFNULL((SELECT ROUND((SUM(ST.VALUE)),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='CREDIT'),0) AS creditAmount,
			
			IFNULL((SELECT ROUND((SUM(ST.VALUE)*-1),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='ADVANCE'),0) AS advancedAmount
			
			FROM finance_supplier_purchaseinvoice_header SPIH
			INNER JOIN ware_grnheader GH ON GH.strInvoiceNo=SPIH.INVOICE_NO
			WHERE SPIH.SUPPLIER_ID='$supplierId' AND
			SPIH.CURRENCY_ID='$currencyId' ";
	$result = $db->RunQuery($sql);
	return $result;
}
function getAdvanceCombo($supplierId,$currencyId,$PONo,$POYear)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT
			(SELECT SUM((ST.VALUE)*-1)
			FROM finance_supplier_transaction ST
			WHERE ST.PO_NO=SAPH.PO_NO AND
			ST.PO_YEAR=SAPH.PO_YEAR AND
			ST.DOCUMENT_NO=SAPH.ADVANCE_PAYMENT_NO AND
			ST.DOCUMENT_YEAR=SAPH.ADVANCE_PAYMENT_YEAR AND
			ST.PURCHASE_INVOICE_NO IS NULL AND
			ST.PURCHASE_INVOICE_YEAR IS NULL AND
			ST.DOCUMENT_TYPE='ADVANCE') AS advancedValue,
			SAPH.ADVANCE_PAYMENT_NO,SAPH.ADVANCE_PAYMENT_YEAR
			FROM finance_supplier_advancepayment_header SAPH
			WHERE SAPH.SUPPLIER_ID='$supplierId' AND
			SAPH.CURRENCY_ID='$currencyId' AND
			SAPH.PO_NO='$PONo' AND
			SAPH.PO_YEAR='$POYear' AND
			SAPH.COMPANY_ID = '$companyId' AND
			STATUS!='-2'";
	
	return $db->RunQuery($sql);
}
function loadCancleMode($programCode,$userId)
{
	global $db;
	$Mode	= 0;
	$sql = "SELECT
			menupermision.intCancel 
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId = '$userId' ";

	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	if($row['intCancel']==1)
	{
		$Mode = 1;
	}
	return $Mode;
}
function loadSaveMode($programCode,$userId)
{
	global $db;
	
	$Mode	= 0;
	$sql = "SELECT
			menupermision.intAdd 
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId = '$userId' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	if($row['intAdd']==1)
	{
		$Mode = 1;
	}
	return $Mode;
}
?>
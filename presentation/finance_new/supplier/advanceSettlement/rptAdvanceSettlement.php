<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];

$advSettleNo 		= $_REQUEST['advSettleNo'];
$advSettleYear 		= $_REQUEST['advSettleYear'];

$detailArr 			= GetDetails($advSettleNo,$advSettleYear);

?><head>

<title>Supplier Advance Settlement</title>



<form id="frmAdvanceSettlement" name="frmAdvanceSettlement" method="post">
    <table width="900" align="center">
      <tr>
        <td width="888"><?php include 'reportHeader.php'?></td>
      </tr>
      <tr>
        <td style="text-align:center">&nbsp;</td>
      </tr>
      <tr>
        <td style="text-align:center"><strong>SUPPLIER ADVANCE SETTLEMENT</strong></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td width="15%">&nbsp;</td>
            <td width="1%">&nbsp;</td>
            <td width="40%">&nbsp;</td>
            <td width="17%">&nbsp;</td>
            <td width="1%">&nbsp;</td>
            <td width="26%">&nbsp;</td>
          </tr>
          <tr>
            <td>Advance Settle No</td>
            <td>:</td>
            <td><?php echo $advSettleNo.' / '.$advSettleYear; ?></td>
            <td>Settle Date</td>
            <td>:</td>
            <td><?php echo $detailArr["SETTLE_DATE"]?></td>
          </tr>
          <tr>
            <td>Supplier</td>
            <td>:</td>
            <td><?php echo $detailArr["supplier"]; ?></td>
            <td>Currency</td>
            <td>:</td>
            <td><?php echo $detailArr["currency"]; ?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td ><table width="100%" border="0" class="rptBordered" id="tblMain">
        <thead>
            <tr>
                <th width="16%">Invoice No</th>
                <th width="16%">PO No</th>
                <th width="16%">Advance No</th>
                <th width="39%">GL Account</th>
                <th width="13%">Amount</th>
            </tr>
        </thead>        
        <tbody>
            <tr>
                <td style="text-align:center"><?php echo $detailArr['INVOICE_NO'].' - '.$detailArr['INVOICE_YEAR']; ?></td>
                <td style="text-align:center"><?php echo $detailArr['PO_NO'].' - '.$detailArr['PO_YEAR']; ?></td>
                <td style="text-align:center"><?php echo $detailArr['ADVANCE_NO'].' - '.$detailArr['ADVANCE_YEAR']; ?></td>
                <td style="text-align:left"><?php echo $detailArr['chartOfAccount']; ?></td>
                <td style="text-align:right"><?php echo number_format($detailArr['SETTLE_AMOUNT'],2); ?></td>
            </tr>
        </tbody>
        </table></td>
      </tr>
      <tr>
        <td class="normalfnt">&nbsp;</td>
      </tr>
      </table>
</form>
</body>
<?php
function GetDetails($advSettleNo,$advSettleYear)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT 	SSH.INVOICE_NO, 
			SSH.INVOICE_YEAR, 
			SSH.PO_NO, 
			SSH.PO_YEAR, 
			SSH.ADVANCE_NO, 
			SSH.ADVANCE_YEAR, 
			SSH.SETTLE_AMOUNT, 
			SSH.SETTLE_DATE,
			FC.strCode AS currency,
			(
			SELECT CONCAT(FCOA.CHART_OF_ACCOUNT_NAME,' | ',FMT.FINANCE_TYPE_CODE,FMMT.MAIN_TYPE_CODE,FMST.SUB_TYPE_CODE,FCOA.CHART_OF_ACCOUNT_CODE)
			FROM finance_mst_chartofaccount FCOA
			INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
			WHERE FCOA.CHART_OF_ACCOUNT_ID = SSH.CHAT_OF_ACCOUNT_ID
			) AS chartOfAccount,
			MS.strName AS supplier
			FROM 
			finance_supplier_settlement_header AS SSH
			INNER JOIN mst_financecurrency FC ON FC.intId=SSH.CURRENCY_ID
			INNER JOIN mst_supplier MS ON MS.intId=SSH.SUPPLIER_NO
			WHERE SSH.SETTLE_NO = '$advSettleNo' AND
			SSH.SETTLE_YEAR = '$advSettleYear' AND
			SSH.COMPANY_ID = '$companyId' ";
	
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	
	return $row;
}
?>
<?php 
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$requestType 		= $_REQUEST['requestType'];

include "../../../../dataAccess/Connector.php";
include_once "../../../../class/masterData/exchange_rate/cls_exchange_rate_get.php";

$obj_exchgRate_get	= new cls_exchange_rate_get($db);

if($requestType=='loadData')
{
	$supplierId = $_REQUEST['supplierId'];
	$currencyId = $_REQUEST['currencyId'];
	
	$response['type']			= 'pass';
	$response['arrDetailData']	= getDetailData($supplierId,$currencyId);
	$row						= $obj_exchgRate_get->GetAllValues($currencyId,4,date('Y-m-d'),$companyId,'RunQuery');
	$response['exchgRate']		= $row['AVERAGE_RATE'];
	
	echo json_encode($response);	
}
else if($requestType=='loadMaxAmount')
{
	$advanceNoArr 	= explode('/',$_REQUEST['advanceNoArr']);
	$advanceNo		= $advanceNoArr[0];
	$advanceYear	= $advanceNoArr[1];
	$GLAccId		= $_REQUEST['GLAccId'];
	$supplierId 	= $_REQUEST['supplierId'];
	$currencyId 	= $_REQUEST['currencyId'];
	
	$sql = "SELECT IFNULL((SUM(VALUE)*-1),0) AS totAmount
			FROM finance_supplier_transaction
			WHERE DOCUMENT_NO='$advanceNo' AND
			DOCUMENT_YEAR='$advanceYear' AND
			SUPPLIER_ID='$supplierId' AND
			CURRENCY_ID='$currencyId' AND
			DOCUMENT_TYPE='ADVANCE' AND
			LEDGER_ID='$GLAccId' AND
			PURCHASE_INVOICE_YEAR IS NULL AND
			PURCHASE_INVOICE_NO IS NULL ";
	
	$result 				= $db->RunQuery($sql);
	$row 					= mysqli_fetch_array($result);
	$response['maxAmount'] 	= $row['totAmount'];
	
	echo json_encode($response);
}
else if($requestType=='loadGLCombo')
{
	$supplierId 	= $_REQUEST['supplierId'];
	$currencyId 	= $_REQUEST['currencyId'];
	$advanceNoArr 	= explode('/',$_REQUEST['advanceNoArr']);
	$advanceNo		= $advanceNoArr[0];
	$advanceYear	= $advanceNoArr[1];
	
	$response['GLCombo']  = getGLCombo($advanceNo,$advanceYear,$companyId);
	echo json_encode($response);
	
}
else if($requestType=='getAllAdvanceNo')
{
	if($_REQUEST['PONoArr']=='')
	{
		$PONo 		= '';
		$POYear 	= '';
	}
	else
	{
		$PONoArr 	= explode('/',$_REQUEST['PONoArr']);
		$PONo	 	= $PONoArr[0];
		$POYear 	= $PONoArr[1];
	}
	$supplierId 	= $_REQUEST['supplierId'];
	$currencyId 	= $_REQUEST['currencyId'];
	
	$response['AdvanceCombo'] = getAdvanceCombo($supplierId,$currencyId,$PONo,$POYear);
	echo json_encode($response);
}
function getDetailData($supplier,$currency)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT SPIH.PURCHASE_INVOICE_NO,
			SPIH.PURCHASE_INVOICE_YEAR,
			SPIH.INVOICE_NO,
			GH.intPoNo,
			GH.intPoYear,
			
			(SELECT ROUND(SUM(ST.VALUE),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='INVOICE') AS invoiceAmount,
			
			(SELECT ROUND((SUM(ST.VALUE)*-1),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='PAYMENT') AS paidAmount,
			
			(SELECT ROUND((SUM(ST.VALUE)*-1),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='DEBIT') AS debitAmount,
			
			(SELECT ROUND((SUM(ST.VALUE)),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='CREDIT') AS creditAmount,
			
			(SELECT ROUND((SUM(ST.VALUE)*-1),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='ADVANCE') AS advancedAmount
			
			FROM finance_supplier_purchaseinvoice_header SPIH
			INNER JOIN ware_grnheader GH ON GH.strInvoiceNo=SPIH.INVOICE_NO
			WHERE SPIH.SUPPLIER_ID='$supplier' AND
			SPIH.CURRENCY_ID='$currency' AND
			SPIH.COMPANY_ID = '$companyId' ";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$data['invoiceNo'] 	 	= $row['PURCHASE_INVOICE_NO'];
		$data['invoiceYear'] 	= $row['PURCHASE_INVOICE_YEAR'];
		$data['INVOICE_NO']		= $row['INVOICE_NO'];
		$data['PONo'] 			= $row['intPoNo'];
		$data['POYear'] 		= $row['intPoYear'];
		$data['invoiceAmount'] 	= number_format($row['invoiceAmount'],2,'.','');
		$data['paidAmount'] 	= number_format($row['paidAmount'],2,'.','');
		$data['debitAmount'] 	= number_format($row['debitAmount'],2,'.','');
		$data['creditAmount'] 	= number_format($row['creditAmount'],2,'.','');
		$data['advancedAmount'] = number_format($row['advancedAmount'],2,'.','');
		$data['advanceCombo']   = getAdvanceCombo($supplier,$currency,$row['intPoNo'],$row['intPoYear']);
		$completeAmount			= $row['paidAmount']+$row['debitAmount']+$row['advancedAmount'];
		$invoiceAmt				= number_format($row['invoiceAmount'],2,'.','');
		
		if($invoiceAmt==round($completeAmount,2))
			$data['status'] 	= 'complete';
		else
			$data['status'] 	= 'uncomplete';
		$arrDetailData[] 		= $data;
	}
	return $arrDetailData;
}
function getAdvanceCombo($supplier,$currency,$PONo,$POYear)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT
			(SELECT SUM((ST.VALUE)*-1)
			FROM finance_supplier_transaction ST
			WHERE ST.PO_NO=SAPH.PO_NO AND
			ST.PO_YEAR=SAPH.PO_YEAR AND
			ST.DOCUMENT_NO=SAPH.ADVANCE_PAYMENT_NO AND
			ST.DOCUMENT_YEAR=SAPH.ADVANCE_PAYMENT_YEAR AND
			ST.PURCHASE_INVOICE_NO IS NULL AND
			ST.PURCHASE_INVOICE_YEAR IS NULL AND
			ST.DOCUMENT_TYPE='ADVANCE') AS advancedValue,
			SAPH.ADVANCE_PAYMENT_NO,SAPH.ADVANCE_PAYMENT_YEAR,
			SAPH.PO_NO,SAPH.PO_YEAR
			FROM finance_supplier_advancepayment_header SAPH
			WHERE SAPH.SUPPLIER_ID='$supplier' AND
			SAPH.CURRENCY_ID='$currency' AND 
			SAPH.COMPANY_ID='$companyId' AND ";
	if($PONo!='' && $POYear!='')
	{
		$sql.="SAPH.PO_NO='$PONo' AND SAPH.PO_YEAR='$POYear' AND " ;
	}
	$sql.="STATUS!='-2' ";
		
	$result = $db->RunQuery($sql);
	$html = "<select name=\"cboAdvanceNo\" id=\"cboAdvanceNo\" class=\"clsAdvanceNo\" style=\"width:100%\">";
	$html .= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		if($row['advancedValue']!=0)
			$html.="<option value=\"".$row['ADVANCE_PAYMENT_NO'].'/'.$row['ADVANCE_PAYMENT_YEAR']."\" >".$row['ADVANCE_PAYMENT_NO'].'/'.$row['ADVANCE_PAYMENT_YEAR'].' - '.$row['PO_NO'].'/'.$row['PO_YEAR']."</option>";
	}
	return $html;
}
function getGLCombo($advanceNo,$advanceYear,$companyId)
{
	global $db;
	
	$sql = "SELECT finance_supplier_advancepayment_details.ACCOUNT_ID,
			(SELECT CONCAT(CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE),'-',FCOA.CHART_OF_ACCOUNT_NAME)
			FROM finance_mst_chartofaccount FCOA
			INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
			INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
			WHERE FCOA.CHART_OF_ACCOUNT_ID=finance_supplier_advancepayment_details.ACCOUNT_ID AND 
			FCOAC.COMPANY_ID='$companyId'
			ORDER BY FCOA.CHART_OF_ACCOUNT_CODE) AS GLAccount
			FROM finance_supplier_advancepayment_details
			WHERE ADVANCE_PAYMENT_NO='$advanceNo' AND
			ADVANCE_PAYMENT_YEAR='$advanceYear' ";
	$result = $db->RunQuery($sql);
	$html .= "<option value=\"\"></option>";
	$chkSts	= true;
	while($row=mysqli_fetch_array($result))
	{
		if($chkSts)
			$html.="<option value=\"".$row['ACCOUNT_ID']."\" selected=\"selected\" >".$row['GLAccount']."</option>";
		else
			$html.="<option value=\"".$row['ACCOUNT_ID']."\" >".$row['GLAccount']."</option>";
		
		$chkSts = false;
	}
	return $html;
}
?>
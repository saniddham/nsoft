<?php 
session_start();

$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$requestType 		= $_REQUEST['RequestType'];
$programCode		= 'P0780';

include_once "{$backwardseperator}dataAccess/Connector.php";
include_once "../../../../class/finance/supplier/svatSettlement/cls_svat_settlement_set.php";
include_once "../../../../class/finance/supplier/svatSettlement/cls_svat_settlement_get.php";
include_once "../../../../class/finance/cls_common_get.php";
include_once "../../../../class/cls_commonFunctions_get.php";

$obj_get_settle_set	= new Cls_svat_settlement_Set($db);
$obj_get_settle_get	= new Cls_svat_settlement_Get($db);
$obj_fin_comm_get	= new Cls_Common_Get($db);
$obj_comm_get		= new cls_commonFunctions_get($db);

$dtFrom				= $_REQUEST['dtFrom'];
$dtTo				= $_REQUEST['dtTo'];
$arr 				= json_decode($_REQUEST['arr'], true);

if($requestType=='URLSave')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
		
 					foreach($arr as $arrVal)
					{
						$sysInvNo 	 	= $arrVal['sysInvNo'];
						$sysInvNoArray  = explode('/',$sysInvNo);
						$invoiceNo 		= $sysInvNoArray[0];
						$invoiceYear  	= $sysInvNoArray[1];
						$curruncyId	 	= $arrVal['curruncyId'];
						$supId 		 	= $arrVal['supId'];
						$ammount 		= $arrVal['ammount'];
						
						$result			=$obj_get_settle_get->getInvoiceResult($supId,$dtFrom,$dtTo,$invoiceNo,$invoiceYear,'RunQuery2');
						$row			=mysqli_fetch_array($result);
						
						if($row['BAL_AMOUNT']!=$ammount){
							$rollBackFlag	= 1;
							$rollBackMsg	= 'No Balance to settle for Invoice No : ('.$invoiceNo.'/'.$invoiceYear.')';
						}
						
						$serial_array	=$obj_comm_get->GetSystemMaxNo('SUP_INVOICE_SETTLEMENT',$companyId);
						$serialNo		= $serial_array["max_no"];
						$serialYear		= date('Y');
 						
						$response1		=$obj_fin_comm_get->Save_Jurnal_Transaction(97,$ammount,$serialNo,$serialYear,'INVOICE_SETTLEMENT',$invoiceNo,$invoiceYear,'C','SU',$supId,$curruncyId);
						$response2		=$obj_fin_comm_get->Save_Jurnal_Transaction(101,$ammount,$serialNo,$serialYear,'INVOICE_SETTLEMENT',$invoiceNo,$invoiceYear,'D','SU',$supId,$curruncyId);
						
						$result 		= $db->RunQuery2($sql);
						if(!$response1)
						{
							$saved++;
						}
						else
						{
							$rollBackFlag	= 1;
							$rollBackMsg	= $response1['ErrorMsg'];
						}
						if(!$response2)
						{
							$saved++;
						}
						else if(!$response1)
						{
							$rollBackFlag	= 1;
							$rollBackMsg	= $response2['ErrorMsg'];
						}
						$toSave +=2;
					}
					
  	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	else if(($saved==$toSave))
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Saved successfully.';
		$response['serialNo'] 	= $serialNo;
		$response['serialYear'] = $serialYear;
	}
	else
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	$db->CloseConnection();
	echo json_encode($response);
}

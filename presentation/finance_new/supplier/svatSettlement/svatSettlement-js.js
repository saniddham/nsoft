// JavaScript Document
$(document).ready(function(){
	
	$("#frmSvat").validationEngine();
	
	if(intAddx)
	{
		$('#frmSvat #butNew').show();
	}
	if(intEditx)
	{
			$('#frmSvat #butSave').show();
	}
	if(intViewx)
	{
		$('#frmSvat #cboSearch').removeAttr('disabled');
	}
	
	$('#butSearch').live('click',loadData);
	$('#butSave').live('click',Save);
	
	$('#frmSvat #butNew').live('click',New);
	$('#frmSvat .cls_chk').live('click',calculateTotAmnt);
});


function loadData()
{
	$("#tblMain tr:gt(0)").remove();
	var supplierId 	= $('#cboSupplier').val();
	var dtFrom 		= $('#txtDateFrom').val();
	var dtTo  		= $('#txtDateTo').val();
	var tot_amount =0;
  	
	var url = "svatSettlement-db-get.php?requestType=loadData";
	var data 		= "supplierId="+supplierId;
		data	   += "&dtFrom="+dtFrom;
		data	   += "&dtTo="+dtTo;
	
	var obj = $.ajax({
		url:url,
		dataType: "json",  
		data:data,
		async:false,
		success:function(json){
			
 				var length	= json.arrData.length;
				var arrData	= json.arrData;
 				for(var i=0;i<length;i++)
				{
 					var supId			= arrData[i]['supId'];
 					var supName			= arrData[i]['supName'];	
					var supInv			= arrData[i]['supInv'];
					var sysInvNo		= arrData[i]['sysInvNo'];
					var sysInvYear		= arrData[i]['sysInvYear'];
					var currency_id		= arrData[i]['currency_id'];
					var ammount			= parseFloat(arrData[i]['ammount']);
					tot_amount			+=ammount;
					createGrid(supId,supName,supInv,sysInvNo,sysInvYear,currency_id,ammount);
				}
				createLastRow(tot_amount);
				
		},
		error:function(xhr,status){
			
			}		
	});
}

 
 
function Save()
{
	
	var dtFrom = $('#txtDateFrom').val();
	var dtTo   = $('#txtDateTo').val();
		
	showWaiting();	
  
	var errFlag = 0;
	var arr		= "[";
	
	var i=0;
	$('#tblMain .cls_supplier').each(function(){
		var obj= this;
 		if(($(this).parent().find('.cls_chk').is(':checked')))
		{
			i++;
			arr += "{";
 			arr += '"sysInvNo":'+URLEncode_json($(this).parent().find('.cls_sys_inv').html())+',';
			arr += '"supId":"'+$(this).attr('id')+'",';
 			arr += '"curruncyId":'+$(this).parent().find('.cls_amount').attr('id')+',';
 			arr += '"ammount":"'+$(this).parent().find('.cls_amount').html()+'"';
  			arr +=  '},';
		}
	});
	
	if(i==0){
		alert("No details to save");
		hideWaiting();
		return;
	}

	arr 		= arr.substr(0,arr.length-1);
	arr 		+= " ]";
 
 	var url 	= "svatSettlement-db-set.php?RequestType=URLSave";
	var data	= "&dtFrom="+dtFrom;
		data	+= "&dtTo="+dtTo;
	  	data 	+= "&arr="+arr;
  		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	async:false,
	success:function(json)
		{
			$('#frmSvat #butSave').validationEngine('showPrompt', json.msg,json.type );
			if(json.type=='pass')
			{
				$('#frmSvat #txtSerialNo').val(json.serialNo);
				$('#frmSvat #txtSerialYear').val(json.serialYear);
				$('#frmSvat #butSave').hide();
			}
		},
	error:function(xhr,status)
		{
			$('#frmSvat #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
		}
	});
	var t = setTimeout("alertx('#butSave')",4000);
	hideWaiting();
}

function createGrid(supId,supName,supInv,sysInvNo,sysInvYear,currency_id,ammount)
{
	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML  = lastRow;
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'cls_supplier';
	cell.id			= supId;
	cell.innerHTML 	= supName;
	
	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'cls_sup_inv';
	cell.id			= supInv;
	cell.innerHTML 	= supInv;
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'cls_sys_inv';
	cell.id			= sysInvNo+'/'+sysInvYear;
	cell.innerHTML 	= sysInvNo+'/'+sysInvYear;
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'cls_amount';
	cell.id			= currency_id;
	cell.innerHTML 	= ammount;
	
	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'cls_select';
 	cell.innerHTML 	= '<input type="checkbox" class="cls_chk" name="chkSelect" checked="checked" id="chkSelect" />';
 }
function createLastRow(tot_amount)
{
	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML 	= '&nbsp;';
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:left');
	cell.className	= '';
	cell.id			= '';
 	cell.innerHTML 	= '&nbsp;';
	
	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:left');
	cell.className	= '';
	cell.id			= '';
 	cell.innerHTML 	= '&nbsp;';
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:left');
	cell.className	= '';
	cell.id			= '';
 	cell.innerHTML 	= '&nbsp;';
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'tot_amount';
	cell.innerHTML 	= tot_amount;
	
	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:left');
	cell.className	= '';
 	cell.innerHTML 	= '&nbsp;';
	
 }
function alertx()
{
	$('#frmSvat #butSave').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmSvat #butCancle').validationEngine('hide')	;
}
function New()
{
	document.location.href = "main.php";
}

function calculateTotAmnt(){
	var totAmnt=0;
	$('#tblMain .cls_supplier').each(function(){
  		if(($(this).parent().find('.cls_chk').is(':checked')))
		{
 			totAmnt +=parseFloat($(this).parent().find('.cls_amount').html());
		}
	});
	$('#frmSvat .tot_amount').html(totAmnt);
}

<?php
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$thisFilePath 		= $_SERVER['PHP_SELF'];
$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$userId 			= $_SESSION['userId'];
include_once "{$backwardseperator}dataAccess/permisionCheck.inc";
require_once "{$backwardseperator}class/finance/supplier/svatSettlement/cls_svat_settlement_get.php";
require_once "{$backwardseperator}class/cls_commonFunctions_get.php";

$obj_get_settle		= new Cls_svat_settlement_Get($db);
$obj_common 		= new cls_commonFunctions_get($db);

$serialNo 			= $_REQUEST['serialNo'];
$serialYear 		= $_REQUEST['serialYear'];
$programCode		= 'P0780';

$permision_save		= $obj_common->boolPermision($userId,$programCode,'intEdit');
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SVAT Settlement</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<script type="application/javascript" src="svatSettlement-js.js"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<style>
#apDiv1 {
	position: absolute;
	left: 193px;
	top: 206px;
	width: auto;
	height: auto;
	z-index: 1;
}
</style>



<form id="frmSvat" name="frmSvat" autocomplete="off" action="svatSettlement.php" method="post">
<table width="100%" border="0" align="center">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
<div class="trans_layoutL" style="width:900px">
<div class="trans_text">SVAT Settlement</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%"  border="0" class="normalfnt" cellpadding="1" cellspacing="1" align="center" bgcolor="#FFFFFF">
<tr>
              <td colspan="10" class="normalfnt">
              <table width="100%"  border="0" class="normalfnt" cellpadding="1" cellspacing="1" align="center" bgcolor="#FFFFFF">
             <tr height="10">
             <td width="9%" class="normalfnt">Serial No</td>
             <td width="24%"><input type="text" name="txtSerialNo" id="txtSerialNo" style="width:85px" disabled="disabled" value="<?php echo $header_array["SERIAL_NO"]?>"/>&nbsp;<input type="text" name="txtSerialYear" id="txtSerialYear" style="width:60px" disabled="disabled" value="<?php echo $header_array["SERIAL_YEAR"]?>"/></td>
             <td width="67%"></td>
             </tr>
              </table>
              </td>
              </tr>            <tr height="10">
              <td width="9%" class="normalfnt">&nbsp;Supplier</td>
              <td colspan="3" class="normalfnt">&nbsp;<select name="cboSupplier" id="cboSupplier"  style="width:240px">
              <option value=""></option>
                <?php
					$result = $obj_get_settle->getSupplierResult('RunQuery');
					echo "<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$supplierId)
							echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intId']."\" >".$row['strName']."</option>"; 
					}
			  ?>
              </select></td>
              <td width="13%" class="normalfnt">&nbsp;</td>
              <td width="9%" class="normalfnt">Date From</td>
              <td width="14%" class="normalfnt"><input name="txtDateFrom" type="text" value="<?php echo date("Y-m-d") ; ?>" id="txtDateFrom" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              <td width="2%" class="normalfnt"> To</td>
              <td width="14%" class="normalfnt"><input name="txtDateTo" type="text" value="<?php echo date("Y-m-d") ; ?>" id="txtDateTo" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              <td width="10%" class="normalfnt"><a class="button green medium" id="butSearch" name="butSearch" >&nbsp;Search&nbsp;</a></td>
            </tr>
            <tr>
              <td colspan="10" class="normalfnt"><div style="overflow:scroll;width:900px;height:250px;" id="divGrid">
                <table width="100%" class="bordered" id="tblMain" >
                  <tr>
                    <th width="4%" height="30" >No</th>
                    <th width="38%" >Supplier</th>
                    <th width="23%" >Supplier Invoice No</th>
                    <th width="19%" >System Invoice No</th>
                    <th width="12%" >SVAT Amount</th>
                    <th width="4%">&nbsp;</th>
                    </tr>
                  <?php
					while($row=mysqli_fetch_array($detail_result))
					{
						$i++;
					?>
                      <tr class="normalfnt">
                        <td align="left"><?php echo $i; ?></td>
                        <td class="cls_supplier" align="left"><?php echo $row['supplier']; ?></td>
                        <td class="cls_sup_inv" align="left"><?php echo $row['sup_invoice']; ?></td>
                        <td class="cls_sys_inv" align="left"><?php echo $row['sys_invoice']; ?></td>
                        <td class="cls_amount" align="right"><?php echo $row['amount']; ?></td>
                        <td class="cls_select" align="center"><input type="checkbox" class="cls_chk" name="chkSelect" <?php if($chk==1){ ?> checked="checked" <?php } ?> id="chkSelect" /></td>
                       </tr>
                    <?php	
					}
				   ?>
                    </table>
                </div></td>
            </tr>
            </table>
        </td>
    </tr>
    
    <tr>
    	<td height="32" >
    		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td align="center" class="tableBorder_allRound"><a class="button white medium" id="butNew" name="butNew" style="display:none">&nbsp;New&nbsp;</a><a class="button white medium" id="butSave" name="butSave" <?php echo(($permision_save!=1)?'style="display:none"':''); ?> >Save</a><a href="../../../../main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
              </tr>
            </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>
</body>
</html>

<?php 
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$requestType 		= $_REQUEST['requestType'];

include_once "../../../../dataAccess/Connector.php";
include_once "../../../../class/finance/supplier/svatSettlement/cls_svat_settlement_get.php";

$obj_get_settle		= new Cls_svat_settlement_Get($db);

 
if($requestType=='loadData')
{
		$supplier 		= $_REQUEST['supplierId'];
		$dtFrom 		= $_REQUEST['dtFrom'];
		$dtTo 			= $_REQUEST['dtTo'];
		$invoiceNo 		= '';
		$invoiceYear 	= '';
	
		$result			= $obj_get_settle->getInvoiceResult($supplier,$dtFrom,$dtTo,$invoiceNo,$invoiceYear,'RunQuery');
		while($row=mysqli_fetch_array($result))
		{
			$data['supId'] 			= $row['SUP_ID'];
			$data['supName'] 		= $row['SUPPLIER'];
			$data['supInv'] 		= $row['SUP_INV_NO'];
			$data['sysInvNo'] 		= $row['INVOICE_NO'];
			$data['sysInvYear'] 	= $row['INVOICE_YEAR'];
			$data['currency_id'] 	= $row['CURRENCY_ID'];
			$data['ammount'] 		= $row['BAL_AMOUNT'];
			
			$arrDetails[] 			= $data;
		}
		$response['arrData'] 		= $arrDetails;
		echo json_encode($response);
}  
?>
// JavaScript Document
var basePath		= "presentation/finance_new/supplier/supplierPayment/";
$(document).ready(function(){
	
	try
	{
		$("#frmRptSupplierPayment").validationEngine();
	}
	catch(err)
	{
		
	}
	
	$('#frmRptSupplierPayment #butRptConfirm').die('click').live('click',ConfirmRpt);
	$('#frmRptSupplierPayment #butRptReject').die('click').live('click',RejectRpt);
	$('#frmRptSupplierPayment #butRptCancel').die('click').live('click',CancelRpt);
	$('#frmlisting #butRConfirm').die('click').live('click',confirmPayment);

});
function ConfirmRpt()
{
	var val = $.prompt('Are you sure you want to Approve this Payment ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"supplierPayment-db-set.php"+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptSupplierPayment #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx1()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptSupplierPayment #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx1()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function RejectRpt()
{
	var val = $.prompt('Are you sure you want to Reject this Payment ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"supplierPayment-db-set.php"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptSupplierPayment #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptSupplierPayment #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx2()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function CancelRpt()
{
	var rejectState = {
			state0: {
				html:'<label>Are you sure you want to Cancel this Payment ?</label></br><label>Enter the Reason : <textarea name="txtReason" id="txtReason" cols="50" rows="3"></textarea></label><br />',
				buttons: { Ok: true, Cancel: false },
				persistent: true,
				submit:function(v,m,f){
					if(v)
					{
					showWaiting();
					
					var reason 		= $.trim($('#txtReason').val());
					var arrHeader 	= "{";
					arrHeader 		+= '"reason":'+URLEncode_json(reason)+'';
					arrHeader 		+= "}";
					
					if(reason==''){
						alert('Please enter the reason');
					    hideWaiting();
						return false;
					}
					
					var url = basePath+"supplierPayment-db-set.php"+window.location.search+'&requestType=cancel';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:window.location.search+'&status=cancel&arrHeader='+arrHeader,
						async:false,
						
						success:function(json){
								$('#frmRptSupplierPayment #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx3()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptSupplierPayment #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx3()",3000);
							}		
						});

						}
					   hideWaiting();
				}
			}
		};
		$.prompt(rejectState);
		
}
function confirmPayment()
{
	var x = confirm("Are you sure you want to Confirm this Payment/s.");
	if(!x)
		return;
					
	var data = "requestType=confirmPayment";
	value = "[";
	$('.cls_chkConfirm').children().each(function(){
		
		if($(this).attr('checked'))
		{
			var paymentNo 	= $(this).parent().parent().find('.cls_paymentNo').html();
			var paymentYear	= $(this).parent().parent().find('.cls_paymentYear').html();
			
			value += "{";
			value += '"paymentNo":"'+ paymentNo +'",' ;
			value += '"paymentYear":"'+ paymentYear +'"' ;
			value += "},";
		}
	});
	value = value.substr(0,value.length-1);
	value += "]";
	
	data += "&confirmDetails="+value;
	
	var url = basePath+"supplierPayment-db-set.php";
	var obj = $.ajax({
		url:url,
		type:'post',
		dataType:'json',  
		data:data,
		async:false,
		success:function(json){
				if(json.type=='pass')
				{
					alert("Confirmed Sucessfully");
					window.location.href = 'voucher_confirmation.php';
				}
				else
				{
				alert(json.msg);
				}
			}	
		});
}
function alertx1()
{
	$('#frmRptSupplierPayment #butRptConfirm').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmRptSupplierPayment #butRptReject').validationEngine('hide')	;
}
function alertx3()
{
	$('#frmRptSupplierPayment #butRptCancel').validationEngine('hide')	;
}

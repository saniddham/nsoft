<?php 
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$requestType 		= $_REQUEST['requestType'];

include_once ("../../../../dataAccess/Connector.php");
include_once ("../../../../class/masterData/exchange_rate/cls_exchange_rate_get.php");
include_once ("../../../../class/finance/masterData/chartOfAccount_bank/cls_chartofaccount_bank_get.php");

$obj_exchgRate_get				= new cls_exchange_rate_get($db);
$obj_chartOfAccount_bank_get	= new cls_chartOfAccount_bank_get($db);

if($requestType=='loadData')
{
	$supplier 		= $_REQUEST['supplier'];
	$currency 		= $_REQUEST['currency'];
	$grnDateFrom 	= $_REQUEST['grnDateFrom'];
	$grnDateTo 	= $_REQUEST['grnDateTo'];	
	
	loadDetalis($supplier,$currency,$companyId,$grnDateFrom,$grnDateTo);
}
else if($requestType=='loadExchangeRate')
{
	$currency				= $_REQUEST['currencyId'];
	$date	  				= $_REQUEST['date'];
	
	$row					= $obj_exchgRate_get->GetAllValues($currency,4,$date,$companyId,'RunQuery');
	$response['exchgRate']	= $row['AVERAGE_RATE'];
	
	echo json_encode($response);
}
else if($requestType == "CheckBankExRate")
{
	$result		 			= $obj_chartOfAccount_bank_get->GetDetails($_REQUEST["GLID"],'RunQuery');
	$data					= $obj_exchgRate_get->GetAllBankValues($result["CURRENCY_ID"],$result["BANK_ID"],2,$_REQUEST["Date"],$companyId,'RunQuery');
	$data1					= $obj_exchgRate_get->GetAllBankValues($_REQUEST["CurrId"],$result["BANK_ID"],2,$_REQUEST["Date"],$companyId,'RunQuery');
	$response["RATE"]		= round(($data["AVERAGE_RATE"]/1)*$data1["AVERAGE_RATE"],2);
	$response["CURRENCY_ID"]= $result["CURRENCY_ID"];
	echo json_encode($response);
}

function loadDetalis($supplier,$currency,$companyId,$grnDateFrom,$grnDateTo)
{
	//$ledgerAccount				= getLedgerAccount($supplier,$companyId);
	$response['arrDetailData']	= getDetailData($supplier,$currency,$grnDateFrom,$grnDateTo);
	$paymetMethod				= getPayMethod($supplier);
	
	$response['type']			= 'pass';
	//$response['ledgerAccount']	= $ledgerAccount;
	$response['paymentMethod']	= $paymetMethod;

	echo json_encode($response);
}
function getLedgerAccount($spplierId,$companyId)
{
	global $db;
	
	$sql = "SELECT DISTINCT intChartOfAccountId
			FROM mst_financesupplieractivate SA
			WHERE intSupplierId='$spplierId' AND
			intCompanyId='$companyId' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['intChartOfAccountId'];	
}
function getDetailData($supplier,$currency,$grnDateFrom,$grnDateTo)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT CONCAT(SPIH.PURCHASE_INVOICE_NO,'/',SPIH.PURCHASE_INVOICE_YEAR) AS invoiceNo,
			SPIH.PURCHASE_DATE,
			SPIH.INVOICE_NO,
			(SELECT ROUND(SUM(ST.VALUE),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='INVOICE') AS invoiceAmount,
			
			(SELECT ROUND((SUM(ST.VALUE)*-1),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='ADVANCE') AS advancedAmount,
			
			(SELECT ROUND(SUM(ST.VALUE),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='CREDIT') AS creditAmount,
			
			(SELECT ROUND((SUM(ST.VALUE)*-1),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='DEBIT') AS debitAmount,
			
			(SELECT ROUND(SUM(ST.VALUE),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR) AS toBePaid,
			
			CONCAT(GH.intPoNo,'/',GH.intPoYear) PONo,
			
			(SELECT ROUND((SUM(ST.VALUE)*-1),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='PAYMENT') AS paidAmount,
			
			IFNULL((SELECT ROUND((SUM(ST.VALUE)*-1),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PO_NO=GH.intPoNo AND
			ST.PO_YEAR=GH.intPoYear AND
			ST.DOCUMENT_TYPE='ADVANCE' AND
			ST.PURCHASE_INVOICE_NO IS NULL AND
			ST.PURCHASE_INVOICE_YEAR IS NULL),0) AS advancedSettle

			FROM (select * from finance_supplier_purchaseinvoice_header as sh where sh.SUPPLIER_ID='$supplier' AND
			sh.CURRENCY_ID='$currency' ) SPIH
			INNER JOIN finance_supplier_purchaseinvoice_details SPID ON SPID.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			SPID.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR
			INNER JOIN (select * from ware_grnheader where ware_grnheader.datdate >='$grnDateFrom' and ware_grnheader.datdate <='$grnDateTo') GH ON GH.strInvoiceNo=SPIH.INVOICE_NO
			INNER JOIN trn_poheader POH ON POH.intPONo=GH.intPoNo AND
			POH.intPOYear=GH.intPoYear AND POH.intSupplier= SPIH.SUPPLIER_ID
			WHERE SPIH.STATUS!='-2' AND
			GH.intStatus = 1 AND
			SPIH.SUPPLIER_ID='$supplier' AND
			SPIH.CURRENCY_ID='$currency' AND
			POH.intSupplier='$supplier' AND
			SPIH.COMPANY_ID = '$companyId'
			GROUP BY SPIH.PURCHASE_INVOICE_NO,SPIH.PURCHASE_INVOICE_YEAR
			HAVING toBePaid > 0
			 ";
	
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$data['invoiceNo'] 	 	= $row['invoiceNo'];
		$data['invoiceDate'] 	= $row['PURCHASE_DATE'];
		$data['supInvoiceNo'] 	= $row['INVOICE_NO'];
		$data['invoiceAmount'] 	= number_format($row['invoiceAmount'],2,'.','');
		$data['advancedAmount'] = number_format($row['advancedAmount'],2,'.','');
		$data['creditAmount'] 	= number_format($row['creditAmount'],2,'.','');
		$data['debitAmount'] 	= number_format($row['debitAmount'],2,'.','');
		$data['toBePaid'] 		= number_format($row['toBePaid'],2,'.','');
		$data['paidAmount'] 	= number_format($row['paidAmount'],2,'.','');
		$data['PONo'] 			= $row['PONo'];
		$data['advancedSettle'] = $row['advancedSettle'];
		
		$arrDetailData[] 		= $data;
	}
	return $arrDetailData;
}
function getPayMethod($supplier)
{
	global $db;
	
	$sql = "SELECT IFNULL(intPaymentsMethodsId,'') AS payMethod
			FROM mst_supplier
			WHERE intStatus=1 AND
			intId='$supplier' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['payMethod'];
}
?>
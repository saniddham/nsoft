<?php 
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$requestType 		= $_REQUEST['requestType'];

$programName		= 'Supplier Payment Approval';
$programCode		= 'P0691';

$savedStatus		= true;
$savedMasseged		= '';
$error_sql			= '';

require_once "../../../../dataAccess/Connector.php";
require_once "../../../../class/cls_commonFunctions_get.php";
require_once "../../../../class/finance/cls_common_get.php";
include_once "../../../../class/cls_commonErrorHandeling_get.php";
require_once "../../../../class/finance/supplier/supplierPayment/cls_supplier_payment_get.php";
require_once "../../../../class/finance/supplier/supplierPayment/cls_supplier_payment_set.php";
//include_once "../../../../class/masterData/exchange_rate/cls_exchange_rate_get.php";
require_once "../../../../class/finance/cash_flow/cls_cf_common.php";
require_once "../../../../class/finance/cash_flow/cls_cf_common_set.php";

$obj_exchange_rate_get	= new cls_exchange_rate_get($db);
$obj_common	   		= new cls_commonFunctions_get($db);
$obj_errorHandeling	= new cls_commonErrorHandeling_get($db);
$finance_common_get = new cls_common_get($db);
$obj_cf_common		= new cls_cf_common($db);
$obj_cf_common_set	= new cls_cf_common_set($db);
$obj_payment_get	= new Cls_Supplier_Payment_Get($db,$obj_errorHandeling);
$obj_payment_set	= new Cls_Supplier_Payment_Set($db,$finance_common_get,$obj_exchange_rate_get,$obj_cf_common,$obj_cf_common_set);

$arrHeader			= json_decode($_REQUEST['arrHeader'], true);
$arr 				= json_decode($_REQUEST['invDetails'], true);
$arrGL 				= json_decode($_REQUEST['GLDetails'], true);

if($requestType=='saveData')
{
	$paymentNo  	 = $arrHeader['paymentNo'];
	$paymentYear  	 = $arrHeader['paymentYear'];
	$supplierId  	 = $arrHeader['supplierId'];
	$payDate  		 = $arrHeader['payDate'];
	//$ledgerAccountId = ($_REQUEST['ledgerAccountId']==''?'null':$_REQUEST['ledgerAccountId']);
	$currency  		 = $arrHeader['currency'];
	$payMethod  	 = ($arrHeader['payMethod']==''?'null':$arrHeader['payMethod']);
	$totPayAmount  	 = trim($arrHeader['totPayAmount']);
	$Remarks  		 = $obj_common->replace($arrHeader['Remarks']);
	$bankRefNo  	 = $arrHeader['bankRefNo'];
	//$rollBackFlag 	 = 0;
	
	$db->begin();
	
	$exchngRateArr	 = $obj_exchange_rate_get->GetAllValues($currency,2,$payDate,$companyId,'RunQuery2');
	if($exchngRateArr['AVERAGE_RATE']=='' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged	= 'Please enter exchange rates for '.$payDate.' before save.';
	}

	$responseChk 	 = $obj_payment_get->validateBeforeSave($paymentNo,$paymentYear,$userId,$programCode);
	if($responseChk["type"]=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $responseChk["msg"];
	}
	
	if($paymentNo=='' && $paymentYear=='')
	{
		$data['arrData'] = $obj_common->GetSystemMaxNo('intSupplierPaymentNo',$locationId);
		if($data['arrData']['rollBackFlag']==1 && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $data['arrData']['msg'];
			$error_sql		= $data['arrData']['q'];
		}
		
		$paymentNo			= $data['arrData']['max_no'];
		$paymentYear		= date('Y');
		$approveLevels 		= $obj_common->getApproveLevels($programName);
		$status				= $approveLevels+1;
		
		$responseArr 		= $obj_payment_set->saveHeader($paymentNo,$paymentYear,$approveLevels,$status,$supplierId,$payDate,$currency,$payMethod,$Remarks,$bankRefNo,$userId,$companyId,$locationId);
		if($responseArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $responseArr['savedMassege'];
			$error_sql		= $responseArr['error_sql'];
		}
	}
	else
	{
		$approveLevels 		= $obj_common->getApproveLevels($programName);
		$status				= $approveLevels+1;
		$responseArr 		= $obj_payment_set->updateHeader($paymentNo,$paymentYear,$approveLevels,$status,$supplierId,$payDate,$currency,$payMethod,$Remarks,$bankRefNo,$userId,$companyId,$locationId);
		if($responseArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $responseArr['savedMassege'];
			$error_sql		= $responseArr['error_sql'];
		}
		
		$responseArrMax		= $obj_payment_set->updateMaxStatus($paymentNo,$paymentYear);
		if($responseArrMax['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $responseArrMax['savedMassege'];
			$error_sql		= $responseArrMax['error_sql'];
		}
		
		$responseArrDel 	= $obj_payment_set->deleteDetails($paymentNo,$paymentYear);
		if($responseArrDel['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $responseArrDel['savedMassege'];
			$error_sql		= $responseArrDel['error_sql'];
		}
		
		$responseArrDel 	= $obj_payment_set->deleteGLDetails($paymentNo,$paymentYear);
		if($responseArrDel['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $responseArrDel['savedMassege'];
			$error_sql		= $responseArrDel['error_sql'];
		}
		$editMode				= true;
	}
				
	foreach($arr as $arrVal)
	{
		$invoiceNoArr 	= explode('/',trim($arrVal['invoiceNoArr']));
		$PONoArr 		= explode('/',trim($arrVal['PONoArr']));
		$PONo			= $PONoArr[0];
		$POYear			= $PONoArr[1];
		$invoiceNo		= $invoiceNoArr[0];
		$invoiceYear	= $invoiceNoArr[1];
		$invoice		= $obj_common->replace(trim($arrVal["invoice"]));
		$payAmount 		= $arrVal['payAmount'];
					
		$balAmount 		= getValidateQty($supplierId,$currency,$invoiceNo,$invoiceYear,$companyId);
					
		if(($payAmount>$balAmount) && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= 'Some Paying amount exceed Invoice amount.';
		}
		$creditAmount	= getCreditValue($supplierId,$currency,$invoiceNo,$invoiceYear,$companyId);
		$chkAmountArr	= $obj_common->checkAmountWithRS($payAmount,$PONo,$POYear,$supplierId,$invoice,'RunQuery2');
		if($chkAmountArr['chkStatus']=='fail' && $savedStatus)
		{
			if(($creditAmount+$chkAmountArr['balToInvoice'])<$payAmount)
			{
				$savedStatus	= false;
				$savedMasseged	= "Can not raise Payment due to Return to Supplier.<br>PO Amount = ".$chkAmountArr['POValue']."<br>Return Amount = ".$chkAmountArr['RSValue']."<br>Balance to Payment = ".$chkAmountArr['balToInvoice']+$creditAmount." ";
			}
		}
		
		$responseArr 	= $obj_payment_set->saveDetails($paymentNo,$paymentYear,$invoiceNo,$invoiceYear,$payAmount);
		if($responseArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $responseArr['savedMassege'];
			$error_sql		= $responseArr['error_sql'];
		}
	}				
	foreach($arrGL as $arrValGL)
	{
		$accountId 		= $arrValGL['accountId'];
		$amount 		= $arrValGL['amount'];
		$memo 			= $obj_common->replace($arrValGL['memo']);
		$costCenter 	= $arrValGL['costCenter'];
					
		$responseV		= $finance_common_get->getPaymentVoucherNo($payDate,$accountId,$costCenter,'RunQuery2');
		if($responseV['type']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $responseV['msg'];
		}
		
		$voucherNo 		= $responseV['voucherNo'];
		$responseArr	= $obj_payment_set->updateVoucherNo($paymentNo,$paymentYear,$voucherNo);			
		if($responseArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $responseArr['savedMassege'];
			$error_sql		= $responseArr['error_sql'];
		}
		
		$responseGLArr	= $obj_payment_set->saveGLDetails($paymentNo,$paymentYear,$accountId,$amount,$memo,$costCenter);
		if($responseGLArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $responseGLArr['savedMassege'];
			$error_sql		= $responseGLArr['error_sql'];
		}				
	}
	$resultArr	= $obj_common->validateDuplicateSerialNoWithSysNo($paymentNo,'intSupplierPaymentNo',$locationId);
	if($resultArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged	= $resultArr['msg'];
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode)
			$response['msg'] 		= "Updated Successfully.";
		else
			$response['msg'] 		= "Saved Successfully.";
		
		$response['paymentNo']		= $paymentNo;
		$response['paymentYear']	= $paymentYear;
	}
	else
	{
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= $savedMasseged;
		$response['sql']			= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='approve')
{
	$paymentNo  	 = $_REQUEST['paymentNo'];
	$paymentYear  	 = $_REQUEST['paymentYear'];
	
	$response		 = $obj_payment_get->validateBeforeApprove($paymentNo,$paymentYear,$programCode);
	
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_payment_set->approve($paymentNo,$paymentYear);
}
else if($requestType=='reject')
{
	$paymentNo  	 = $_REQUEST['paymentNo'];
	$paymentYear  	 = $_REQUEST['paymentYear'];
	
	$response		 = $obj_payment_get->validateBeforeReject($paymentNo,$paymentYear,$programCode);
	 
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_payment_set->reject($paymentNo,$paymentYear);
}
elseif($requestType == 'cancel')
{
	$paymentNo  	 = $_REQUEST['paymentNo'];
	$paymentYear  	 = $_REQUEST['paymentYear'];
	$arrHeader		= json_decode($_REQUEST['arrHeader'], true);
	$cancel_reason	= $obj_common->replace($arrHeader['reason']);

#############################################################################################################################	
## CHECK CANCEL PERMISSIONS.
	$response		 = $obj_payment_get->validateBeforeCancel($paymentNo,$paymentYear,$programCode);
	
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}	

	echo $obj_payment_set->cancel($paymentNo,$paymentYear,$cancel_reason);
}
else if($requestType=='confirmPayment')
{
	$savedStatus	= true;
	$savedMsg		= '';
	$errorSql		= '';
	
	$arr = json_decode($_REQUEST['confirmDetails'], true);
	$db->begin();
	
	foreach($arr as $arrVal)
	{
		$paymentNo 	   = $arrVal['paymentNo'];
		$paymentYear   = $arrVal['paymentYear'];
		
		$dataArr	= $obj_payment_set->confirmPaymnet($paymentNo,$paymentYear);	
		if($dataArr['savedStatus']=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMsg 		= $dataArr['savedMassege'];
			$errorSql 		= $dataArr['error_sql'];
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
}
function loadSaveMode($programCode,$userId)
{
	global $db;
	$Mode	= 0;
	$sql = "SELECT
			menupermision.intAdd 
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId = '$userId' ";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	if($row['intAdd']==1)
	{
		$Mode = 1;
	}
	return $Mode;
}
function inserTransaction($PONo,$POYear,$supplierId,$currency,$invoiceNo,$invoiceYear,$value,$paymentNo,$paymentYear,$accountId,$companyId,$locationId,$userId,$payDate)
{
	global $db;
	
	if($value<=0)
	{
		return true;
	}
	$value = $value*-1;
	$sqlIns = "INSERT INTO finance_supplier_transaction 
				(
				PO_YEAR, PO_NO, SUPPLIER_ID, 
				CURRENCY_ID, DOCUMENT_YEAR, DOCUMENT_NO, 
				DOCUMENT_TYPE, PURCHASE_INVOICE_YEAR, PURCHASE_INVOICE_NO, 
				LEDGER_ID, VALUE, COMPANY_ID, 
				LOCATION_ID, USER_ID, TRANSACTION_DATE_TIME
				)
				VALUES
				( 
				'$POYear', '$PONo', '$supplierId', 
				'$currency', '$paymentYear', '$paymentNo', 
				'PAYMENT', '$invoiceYear', '$invoiceNo', 
				'$accountId', '$value', '$companyId', 
				'$locationId', '$userId', '$payDate'
				);";
	$resultIns = $db->RunQuery2($sqlIns);
	return $resultIns;
}
function getValidateQty($supplierId,$currency,$invoiceNo,$invoiceYear,$companyId)
{
	global $db;
	
	$sql = "SELECT ROUND(SUM(ST.VALUE),4) AS toBePaid
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO='$invoiceNo' AND
			ST.PURCHASE_INVOICE_YEAR='$invoiceYear' AND
			ST.CURRENCY_ID='$currency' AND
			ST.COMPANY_ID='$companyId' AND
			ST.SUPPLIER_ID='$supplierId' ";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['toBePaid'];
}
function getCreditValue($supplierId,$currency,$invoiceNo,$invoiceYear,$companyId)
{
	global $db;
	
	$sql = "SELECT COALESCE(ROUND(SUM(ST.VALUE),4),0) AS creditValue
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO='$invoiceNo' AND
			ST.PURCHASE_INVOICE_YEAR='$invoiceYear' AND
			ST.CURRENCY_ID='$currency' AND
			ST.COMPANY_ID='$companyId' AND
			ST.SUPPLIER_ID='$supplierId' AND
			ST.DOCUMENT_TYPE='CREDIT' ";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['creditValue'];
}
function insertLedger($transactionType,$GLId,$amount,$paymentNo,$paymentYear,$invoiceNo,$invoiceYear,$currency,$bankRefNo,$supplierId,$remarks,$payDate)
{
	global $db;
	global $locationId;
	global $companyId;
	global $userId;
	
	$sql = "INSERT INTO finance_transaction 
			(
			CHART_OF_ACCOUNT_ID, 
			AMOUNT, DOCUMENT_NO, DOCUMENT_YEAR, 
			DOCUMENT_TYPE, INVOICE_NO, INVOICE_YEAR,
			TRANSACTION_TYPE, TRANSACTION_CATEGORY, 
			TRANSACTION_CATEGORY_ID, CURRENCY_ID, BANK_REFERENCE_NO, REMARKS, LOCATION_ID, COMPANY_ID, 
			LAST_MODIFIED_BY,
			LAST_MODIFIED_DATE
			)
			VALUES
			(
			'$GLId', 
			'$amount', '$paymentNo', '$paymentYear', 
			'PAYMENT', '$invoiceNo', '$invoiceYear',
			'$transactionType', 'SU', 
			'$supplierId', '$currency', '$bankRefNo', '$remarks', '$locationId', '$companyId', 
			'$userId',
			'$payDate'
			)";
	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$data['rollBackFlag'] 	= 1 ;
		$data['msg'] 			= $db->errormsg;
		$data['q'] 				= $sql ;
	}
	else
	{
		$data['rollBackFlag'] 	= 0 ;
	}
	return $data;
}
function getSupplierLedger($supplierId)
{
	global $db;
	
	$sql = "SELECT CHART_OF_ACCOUNT_ID FROM finance_mst_chartofaccount
			WHERE CATEGORY_TYPE='S' AND
			CATEGORY_ID='$supplierId' AND
			STATUS='1' ";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['CHART_OF_ACCOUNT_ID'];
}

?>
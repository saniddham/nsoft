<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$intUser  			= $_SESSION["userId"];

$menuId				= 691;
$reportId			= 943;

$programCode		= 'P0691';

require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'tb1.STATUS',
				'CONCAT_PAYMENT_NO'=>"CONCAT(tb1.PAYMENT_NO,'/',tb1.PAYMENT_YEAR)",
				'supplier'=>'MS.strName',
				'PAY_DATE'=>'tb1.PAY_DATE',
				'strCode'=>'FC.strCode',
				'REMARKS'=>'tb1.REMARKS'
				);
				
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2','Cancelled'=>'-2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND tb1.PAY_DATE = '".date('Y-m-d')."'";
	
// }

$approveLevel = (int)getMaxApproveLevel();

$sql = "SELECT SUB_1.* FROM
			(SELECT
				CONCAT(tb1.PAYMENT_NO,'/',tb1.PAYMENT_YEAR)	AS CONCAT_PAYMENT_NO,
				tb1.PAYMENT_NO,
				tb1.PAYMENT_YEAR,
				tb1.REMARKS,
				tb1.PAY_DATE,
				FC.strCode,
				'Voucher' AS VOUCHER,
				(SELECT ROUND(SUM(SPD.AMOUNT),2) FROM finance_supplier_payment_details SPD
				WHERE SPD.PAYMENT_NO=tb1.PAYMENT_NO AND
				SPD.PAYMENT_YEAR=tb1.PAYMENT_YEAR) AS totAmount,
				MS.strName AS supplier,
				tb1.COMPANY_ID,
				tb1.CREATED_BY,
				tb1.CREATED_DATE,
				tb1.APPROVE_LEVELS,
				if(tb1.STATUS=1,'Approved',if(tb1.STATUS=0,'Rejected',if(tb1.STATUS=-1,'Revised',if(tb1.STATUS=-2,'Cancelled','Pending')))) as Status,
				sys_users.strUserName AS CREATOR,
				
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(finance_supplier_payment_approveby.APPROVED_DATE),')' )
								FROM
								finance_supplier_payment_approveby
								Inner Join sys_users ON finance_supplier_payment_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								finance_supplier_payment_approveby.PAYMENT_NO  = tb1.PAYMENT_NO AND
								finance_supplier_payment_approveby.PAYMENT_YEAR =  tb1.PAYMENT_YEAR AND
								finance_supplier_payment_approveby.APPROVE_LEVEL_NO = '1' AND
								finance_supplier_payment_approveby.STATUS =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
								$approval	= "2nd_Approval";
							}
							else if($i==3){
								$approval	= "3rd_Approval";
							}
							else {
								$approval	= $i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(finance_supplier_payment_approveby.APPROVED_DATE),')' )
								FROM
								finance_supplier_payment_approveby
								Inner Join sys_users ON finance_supplier_payment_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								finance_supplier_payment_approveby.PAYMENT_NO  = tb1.PAYMENT_NO AND
								finance_supplier_payment_approveby.PAYMENT_YEAR =  tb1.PAYMENT_YEAR AND
								finance_supplier_payment_approveby.APPROVE_LEVEL_NO =  '$i' AND
								finance_supplier_payment_approveby.STATUS =  '0' 
							),
							IF(
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.STATUS>1) AND (tb1.STATUS<=tb1.APPROVE_LEVELS) AND ((SELECT
								concat(sys_users.strUserName )
								FROM
								finance_supplier_payment_approveby
								Inner Join sys_users ON finance_supplier_payment_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								finance_supplier_payment_approveby.PAYMENT_NO  = tb1.PAYMENT_NO AND
								finance_supplier_payment_approveby.PAYMENT_YEAR =  tb1.PAYMENT_YEAR AND
								finance_supplier_payment_approveby.APPROVE_LEVEL_NO =  ($i-1) AND 
								finance_supplier_payment_approveby.STATUS='0' )<>'')),
								
								'Approve',
								 if($i>tb1.APPROVE_LEVELS,'-----',''))
								
								) as `".$approval."`, "; 
									
								}
								
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(finance_supplier_payment_approveby.APPROVED_DATE),')' )
								FROM
								finance_supplier_payment_approveby
								Inner Join sys_users ON finance_supplier_payment_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								finance_supplier_payment_approveby.PAYMENT_NO  = tb1.PAYMENT_NO AND
								finance_supplier_payment_approveby.PAYMENT_YEAR =  tb1.PAYMENT_YEAR AND
								finance_supplier_payment_approveby.APPROVE_LEVEL_NO =  '0' AND
								finance_supplier_payment_approveby.STATUS =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS<=tb1.APPROVE_LEVELS AND 
								tb1.STATUS>1),'Reject', '')) as `Reject`,
								
								IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(finance_supplier_payment_approveby.APPROVED_DATE),')' )
								FROM
								finance_supplier_payment_approveby
								Inner Join sys_users ON finance_supplier_payment_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								finance_supplier_payment_approveby.PAYMENT_NO  = tb1.PAYMENT_NO AND
								finance_supplier_payment_approveby.PAYMENT_YEAR =  tb1.PAYMENT_YEAR AND
								finance_supplier_payment_approveby.APPROVE_LEVEL_NO =  '-2' AND
								finance_supplier_payment_approveby.STATUS =  '0'
							),IF(((SELECT
								menupermision.intCancel 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 
								AND tb1.STATUS = 1),'Cancel', '')) as `Cancel`,";
								
							$sql .= "'View' as `VIEW`   
				
				FROM finance_supplier_payment_header as tb1 
				INNER JOIN mst_supplier MS ON MS.intId=tb1.SUPPLIER_ID
				INNER JOIN sys_users ON tb1.CREATED_BY = sys_users.intUserId
				LEFT JOIN mst_financecurrency FC ON FC.intId=tb1.CURRENCY_ID
				WHERE tb1.COMPANY_ID='$session_companyId'
				$where_string
 
		)  
		AS SUB_1 WHERE 1=1";

$jq		= new jqgrid('',$db);	
$col 	= array();
$cols 	= array();

$col["title"] 			= "Payment No";
$col["name"] 			= "PAYMENT_NO";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Payment Year";
$col["name"] 			= "PAYMENT_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Status";
$col["name"] 			= "Status";
$col["width"] 			= "3"; 						// not specifying width will expand to fill space
$col["align"] 			= "center";
$col["sortable"] 		= true; 						// this column is not sortable
$col["search"] 			= true; 						// this column is not searchable
$col["editable"] 		= false;
$col["stype"] 			= "select";
$str 					= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Cancelled:Cancelled" ;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Payment No";
$col["name"] 			= "CONCAT_PAYMENT_NO";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 
$col['link']			= '?q='.$menuId.'&paymentNo={PAYMENT_NO}&paymentYear={PAYMENT_YEAR}';
$col["linkoptions"] 	= "target='supplierPayment.php'";					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Supplier";
$col["name"] 			= "supplier";
$col["width"] 			= "6";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Date";
$col["name"] 			= "PAY_DATE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "strCode";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Remarks";
$col["name"] 			= "REMARKS";
$col["width"] 			= "7";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Amount";
$col["name"] 			= "totAmount";
$col["width"] 			= "3";
$col["align"] 			= "right";
$col["sortable"] 		= true; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

//BEGIN - FIRST APPROVAL {
$col["title"] 			= "1st Approval";
$col["name"] 			= "1st_Approval";
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportId.'&paymentNo={PAYMENT_NO}&paymentYear={PAYMENT_YEAR}&mode=Confirm';
$col["linkoptions"] 	= "target='rptSupplierPayment.php'";
$col['linkName']		= 'Approve';
$cols[] 				= $col;	
$col					= NULL;
//END	- FIRST APPROVEL }

//BEGIN - SECOND & MORE APPROVAL {
for($i=2; $i<=$approveLevel; $i++)
{
	if($i==2){
		$ap		= "2nd Approval";
		$ap1	= "2nd_Approval";
	}
	else if($i==3){
		$ap		= "3rd Approval";
		$ap1	= "3rd_Approval";
	}
	else {
		$ap		= $i."th Approval";
		$ap1	= $i."th_Approval";
	}

$col["title"] 				= $ap; // caption of column
$col["name"] 				= $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q='.$reportId.'&paymentNo={PAYMENT_NO}&paymentYear={PAYMENT_YEAR}&mode=Confirm';
$col["linkoptions"] 		= "target='rptSupplierPayment.php'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;
}
//END 	- SECOND & MORE APPROVAL {
	
//BEGIN - CANCEL {
$col["title"] 			= "Cancel";
$col["name"] 			= "Cancel";
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportId.'&paymentNo={PAYMENT_NO}&paymentYear={PAYMENT_YEAR}&mode=Cancel';
$col["linkoptions"] 	= "target='rptSupplierPayment.php'";
$col['linkName']		= 'Cancel';
$cols[] 				= $col;	
$col					= NULL;
//END	- CANCEL }

$col["title"] 			= "Payment Voucher";
$col["name"] 			= "VOUCHER";
$col["width"] 			= "3";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= 'presentation/finance_new/reports/rpt_payment_voucher_pdf.php?SerialNo={PAYMENT_NO}&SerialYear={PAYMENT_YEAR}&Type=PAYMENT';
$col["linkoptions"] 	= "target='rptPayment_voucher.php'";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "View";
$col["name"] 			= "VIEW";
$col["width"] 			= "2";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= '?q='.$reportId.'&paymentNo={PAYMENT_NO}&paymentYear={PAYMENT_YEAR}';
$col["linkoptions"] 	= "target='rptSupplierPayment.php'";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Supplier Payment Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'PAYMENT_YEAR,PAYMENT_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;

//$grid["postData"] = array("filters" => $sarr ); 

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Supplier Payment Listing</title>
<?php
echo $out;

function getMaxApproveLevel()
{
	global $db;
	$appLevel = 0;
	
	$sqlp = "SELECT
			Max(finance_supplier_payment_header.APPROVE_LEVELS) AS appLevel
			FROM finance_supplier_payment_header";					
	$resultp 	= $db->RunQuery($sqlp);
	$rowp		= mysqli_fetch_array($resultp);			 
	return $rowp['appLevel'];
}
?>
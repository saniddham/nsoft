<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once "class/finance/cls_convert_amount_to_word.php";
include_once "class/cls_commonErrorHandeling_get.php";
require_once "class/finance/cls_common_get.php";

$obj_fin_com		= new Cls_Common_Get($db); 
$obj_AmtName	   	= new Cls_Convert_Amount_To_Word($db);
$obj_commonErrHandle = new cls_commonErrorHandeling_get($db);

$mainPath 			= $_SESSION['mainPath'];
$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$session_userId 	= $_SESSION["userId"];
$thisFilePath 		= $_SERVER['PHP_SELF'];
$programCode		= 'P0691';

$paymentNo 			= $_REQUEST['paymentNo'];
$paymentYear 		= $_REQUEST['paymentYear'];
$mode				= (!isset($_REQUEST["mode"])?'':$_REQUEST["mode"]);

$header_array 		= GetHeaderDetails($paymentNo,$paymentYear,$companyId);
$detail_result 		= GetGridDetails($paymentNo,$paymentYear,$header_array['SUPPLIER_ID'],$header_array['CURRENCY_ID']);
$GL_result 			= GetGLGridDetails($paymentNo,$paymentYear);

$intStatus			= $header_array['STATUS'];
$levels				= $header_array['APPROVE_LEVELS'];

$permition_arr		= $obj_commonErrHandle->get_permision_withApproval_reject($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_reject	= $permition_arr['permision'];

$permition_arr		= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_confirm	= $permition_arr['permision'];

$permition_arr		= $obj_commonErrHandle->get_permision_withApproval_cancel($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_cancel	= $permition_arr['permision'];

$company_Id			= $header_array["COMPANY_ID"];
?>
<head>
<title>Supplier Paymnet Report</title>

<script type="text/javascript" src="presentation/finance_new/supplier/supplierPayment/rptSupplierPayment-js.js"></script>

<style>
#apDiv1 {
	position: absolute;
	left: 468px;
	top: 117px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
</style>

</head>
<body>
<?php
if($header_array['STATUS']==-2)
{
?>
<div id="apDiv1" ><img src="images/cancelled.png" style="opacity:0.3" /></div>
<?php
}
?>
<form id="frmRptSupplierPayment" name="frmRptSupplierPayment" method="post">
    <table width="1000" align="center">
      <tr>
        <td width="888"><?php include 'reportHeader.php'?></td>
      </tr>
      <tr>
        <td style="text-align:center">&nbsp;</td>
      </tr>
      <tr>
        <td style="text-align:center"><strong>SUPPLIER PAYMENT REPORT</strong></td>
      </tr>
       <?php
		include "presentation/report_approve_status_and_buttons.php"
     	?>
      <tr>
        <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>Payment No</td>
            <td>:</td>
            <td><?php echo $obj_fin_com->getSerialNo($header_array["SERIAL_NO"],$header_array["SERIAL_DATE"],$company_Id,'RunQuery')//echo $header_array["ADVANCE_NO"]; ?></td>
            <td>Payment Date</td>
            <td>:</td>
            <td><?php echo $header_array["PAY_DATE"]; ?></td>
          </tr>
          <tr>
            <td>Supplier</td>
            <td>:</td>
            <td><?php echo $header_array["supplier"]; ?></td>
            <td>Currency</td>
            <td>:</td>
            <td><?php echo $header_array["currency"]; ?></td>
          </tr>
          <tr>
            <td width="15%">Amount</td>
            <td width="1%">:</td>
            <td width="41%"><?php echo number_format($header_array["totPaidAmount"],2,'.',''); ?></td>
            <td width="14%">Payment Method</td>
            <td width="1%">:</td>
            <td width="28%"><?php echo $header_array["paymentMethod"]; ?></td>
          </tr>
          <tr>
            <td>Narration</td>
            <td>:</td>
            <td rowspan="2" valign="top"><?php echo $header_array["REMARKS"]; ?></td>
            <td>Bank Reference No</td>
            <td>:</td>
            <td><?php echo $header_array["BANK_REFERENCE_NO"]; ?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2"><span <?php echo ($intStatus==-2?'':'style="display:none"'); ?>><fieldset class="bordered normalfnt" style="width:93%;border-color:#F00">
  	<legend style="color:#F00">Cancelled Reason:</legend><?php echo $header_array["CANCEL_REASON"];  ?></fieldset></span></td>
          </tr>
        </table></td>
      </tr>
       <tr>
         <td class="normalfnt"><table width="100%" border="0" class="rptBordered" id="tblMain2">
           <thead>
             <tr>
               <th width="3%">&nbsp;</th>
                <th width="16%">Invoice No</th>
                <th width="11%">Invoiced Date</th>
                <th width="13%">Invoiced Amount</th>
                <th width="10%">Debit Note</th>
                <th width="9%">Credit Note</th>
                <th width="9%">Advance</th>
                 <th width="10%">Paid Amount</th>
                <th width="8%">To Be Paid</th>
                <th width="11%">Paying Amount</th>
             </tr>
           </thead>
           <tbody>
             <?php
$totAmount 		= 0;
$i				= 0;

while($row = mysqli_fetch_array($detail_result))
{ 
?>
            <tr>
                <td align="center"><?php echo ++$i;?>.</td>
                <td class="clsInvoice" style="text-align:left"><?php echo $row["INVOICE_NO"]?></td>
                <td class="clsInvoiceDate" style="text-align:center"><?php echo $row["PURCHASE_DATE"]?></td>
                <td class="clsInvoiceAmt" style="text-align:right"><?php echo $row["invoiceAmount"]?></td>
                <td class="clsDebitNote" style="text-align:right"><?php echo ($row["debitAmount"]==null?'&nbsp;':$row["debitAmount"]);?></td>
                <td class="clsCreditNote" style="text-align:right"><?php echo ($row["creditAmount"]==null?'&nbsp;':$row["creditAmount"]);?></td>
                <td class="clsAdvanceAmt" style="text-align:right"><?php echo ($row["advancedAmount"]==null?'&nbsp;':$row["advancedAmount"]);?></td>
                <td class="clsPaidAmt" style="text-align:right"><?php echo ($row["paidAmount"]==null?'&nbsp;':$row["paidAmount"]);?></td>
                <td class="clsToBePaid" style="text-align:right"><?php echo $row["toBePaid"]?></td>
                <td class="clsPayAmount" style="text-align:right"><?php echo number_format($row['payAmount'],2); ?></td>
            </tr>
             <?php
}
?>
             <tr>
               <td colspan="9" align="right"><b>Total :</b></td>
               <td width="10%" style="text-align:right"><b><?php echo number_format($header_array["totPaidAmount"],2); ?></b></td>
             </tr>
           </tbody>
         </table></td>
       </tr>
       <tr>
        <td class="normalfnt">&nbsp;</td>
      </tr>
      <tr>
        <td ><table width="100%" border="0" class="rptBordered" id="tblMain">
        <thead>
          <tr>
            <th width="3%">&nbsp;</th>
            <th width="29%">Account</th>
            <th width="42%">Memo</th>
            <th width="15%">Cost Center</th>
            <th width="11%">Amount</th>
          </tr>
        </thead>        
        <tbody>
<?php
$totAmount 		= 0;
$j				= 0;
while($row = mysqli_fetch_array($GL_result))
{ 
	$totAmount+=$row['AMOUNT'];
?>
		<tr>
        	<td class="cls_td_check" align="center"><?php echo ++$j;?>.</td>
        	<td class="cls_td_salesOrderNo"><?php echo $row["account"]?></td>
       	 	<td width="42%"><?php echo $row["MEMO"]?></td>
        	<td width="15%"><?php echo $row["costCenter"]?></td>
        	<td width="11%" style="text-align:right"><?php echo number_format($row["AMOUNT"],2);?></td>
        </tr>
<?php
}
?>
		<tr>
        	<td colspan="4" align="right"><b>Total :</b></td>
        	<td width="11%" style="text-align:right"><b><?php echo number_format($totAmount,2);?></b></td>
        </tr>
        </tbody>
        </table></td>
      </tr>
      <tr>
        <td class="normalfnt"><b><?php echo $obj_AmtName->Convert_Amount($header_array["totPaidAmount"],$header_array["currency"]); ?></b></td>
      </tr>
      <tr>
        <td class="normalfnt">&nbsp;</td>
      </tr>
      <tr>
        <td>
			<?php
            $creator		= $header_array['CREATOR'];
            $createdDate	= $header_array['CREATED_DATE'];
            $resultA 		= getRptApproveDetails($paymentNo,$paymentYear);
            include "presentation/report_approvedBy_details.php"
            ?>
        </td>
    </tr>
      </table>
</form>
</body>
</html>
<?php
function GetHeaderDetails($paymentNo,$paymentYear,$companyId)
{
	global $db;
	
	$sql = "SELECT 
			SPH.PAYMENT_NO				AS SERIAL_NO,
			SPH.COMPANY_ID				AS COMPANY_ID,
			SPH.PAY_DATE				AS SERIAL_DATE,
			MS.strName AS supplier,
			SUPPLIER_ID,
			CURRENCY_ID, 
			PAY_DATE,  
			REMARKS, 
			CANCEL_REASON, 
			BANK_REFERENCE_NO, 
			STATUS,
			APPROVE_LEVELS,
			APPROVE_LEVELS AS LEVELS,
			FC.strDescription AS currency,
			FPM.strName AS paymentMethod,
			SU.strUserName AS CREATOR,
			CREATED_DATE AS CREATED_DATE,
			(SELECT SUM(AMOUNT)
			FROM finance_supplier_payment_details
			WHERE PAYMENT_NO=SPH.PAYMENT_NO AND
			PAYMENT_YEAR=SPH.PAYMENT_YEAR) totPaidAmount
			FROM finance_supplier_payment_header SPH
			INNER JOIN mst_supplier MS ON MS.intId=SPH.SUPPLIER_ID
			INNER JOIN sys_users SU ON SPH.CREATED_BY = SU.intUserId
			LEFT JOIN mst_financecurrency FC ON FC.intId=SPH.CURRENCY_ID
			LEFT JOIN mst_financepaymentsmethods FPM ON FPM.intId=SPH.PAY_METHOD
			WHERE PAYMENT_NO='$paymentNo' AND
			PAYMENT_YEAR='$paymentYear' ";
				
	$result = $db->RunQuery($sql);
	$header_array = mysqli_fetch_array($result);
	return $header_array;
}

function GetGridDetails($paymentNo,$paymentYear,$supplierId,$currencyId)
{
	global $db;
	
	$sql = "SELECT CONCAT(SPIH.PURCHASE_INVOICE_NO,' / ',SPIH.PURCHASE_INVOICE_YEAR) AS invoiceNo,
			SPIH.PURCHASE_DATE,
			SPIH.INVOICE_NO,
			(SELECT ROUND(SUM(ST.VALUE),2)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='INVOICE') AS invoiceAmount,
			
			(SELECT ROUND((SUM(ST.VALUE)*-1),2)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='ADVANCE') AS advancedAmount,
			
			(SELECT ROUND(SUM(ST.VALUE),2)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='CREDIT') AS creditAmount,
			
			(SELECT ROUND(SUM(ST.VALUE)*-1,2)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='DEBIT') AS debitAmount,
			
			(SELECT ROUND(SUM(ST.VALUE),2)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR) AS toBePaid,
			
			CONCAT(GH.intPoNo,'/',GH.intPoYear) PONo,
			
			(SELECT ROUND((SUM(ST.VALUE)*-1),2)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='PAYMENT') AS paidAmount,
			
			(SELECT SUM(AMOUNT)
			FROM finance_supplier_payment_details SPD
			WHERE SPD.PAYMENT_NO='$paymentNo' AND
			SPD.PAYMENT_YEAR='$paymentYear' AND
			SPD.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			SPD.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR ) payAmount,
			
			IFNULL((SELECT COUNT(SERIAL_NO) 
			FROM finance_supplier_transaction ST
			WHERE ST.PO_NO=GH.intPoNo AND ST.PO_YEAR=GH.intPoYear AND ST.DOCUMENT_TYPE='ADVANCE' HAVING SUM(VALUE)!=0),0) advPaymentCount,
			
			IFNULL((SELECT COUNT(SERIAL_NO)
			FROM finance_supplier_transaction ST
			WHERE ST.PO_NO=GH.intPoNo AND ST.PO_YEAR=GH.intPoYear AND 
			ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND 
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND ST.DOCUMENT_TYPE='ADVANCE' HAVING SUM(VALUE)!=0 ),0) settelmentCount

			FROM finance_supplier_payment_header SPH
			INNER JOIN finance_supplier_payment_details SPD ON SPD.PAYMENT_NO=SPH.PAYMENT_NO AND
			SPD.PAYMENT_YEAR=SPH.PAYMENT_YEAR
			INNER JOIN finance_supplier_purchaseinvoice_header SPIH ON SPD.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			SPD.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR
			INNER JOIN finance_supplier_purchaseinvoice_details SPID ON SPID.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			SPID.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR
			INNER JOIN ware_grnheader GH ON GH.strInvoiceNo=SPIH.INVOICE_NO
			WHERE 
			SPH.PAYMENT_NO='$paymentNo' AND
			SPH.PAYMENT_YEAR='$paymentYear'
			GROUP BY SPIH.PURCHASE_INVOICE_NO,SPIH.PURCHASE_INVOICE_YEAR ";
	return $db->RunQuery($sql);
}
function GetGLGridDetails($paymentNo,$paymentYear)
{
	global $db;
	
	$sql = "SELECT 	
			(SELECT CONCAT(CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE),' - ',
			FCOA.CHART_OF_ACCOUNT_NAME)
			FROM finance_mst_chartofaccount FCOA
			INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
			-- INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
			WHERE FCOA.CHART_OF_ACCOUNT_ID=SPGL.ACCOUNT_ID) AS account,
			FD.strName AS costCenter, 
			AMOUNT, 
			MEMO
			FROM finance_supplier_payment_gl SPGL
			INNER JOIN mst_financedimension FD ON FD.intId=SPGL.COST_CENTER
			WHERE PAYMENT_NO='$paymentNo' AND PAYMENT_YEAR='$paymentYear' ";
	
	return $db->RunQuery($sql);
}
function getRptApproveDetails($paymentNo,$paymentYear)
{
	global $db;
	
	$sql = "SELECT
			AB.APPROVED_BY,
			AB.APPROVED_DATE AS dtApprovedDate,
			SU.strUserName AS UserName,
			AB.APPROVE_LEVEL_NO AS intApproveLevelNo
			FROM
			finance_supplier_payment_approveby AB
			INNER JOIN sys_users SU ON AB.APPROVED_BY = SU.intUserId
			WHERE AB.PAYMENT_NO ='$paymentNo' AND
			AB.PAYMENT_YEAR ='$paymentYear'      
			ORDER BY AB.APPROVED_DATE ASC ";
	
	return $db->RunQuery($sql);
}
?>
// JavaScript Document
var basePath			= "presentation/finance_new/supplier/supplierPayment/";
var voucherRptPath		= "presentation/finance_new/reports/";
var advSettleMenuId		= 778;
var reportId			= 943;
var menuId				= 691;
$(document).ready(function(){
	
	$("#frmSupplierPayment").validationEngine();
	
	/*if(intAddx)
	{
		$('#frmSupplierPayment #butNew').show();
		if(status==-2)
			$('#frmSupplierPayment #butSave').hide();
		else
			$('#frmSupplierPayment #butSave').show();
	}
	//permision for edit 
	if(intEditx)
	{
		if(status==-2)
			$('#frmSupplierPayment #butSave').hide();
		else
			$('#frmSupplierPayment #butSave').show();
	}
	
	//permision for delete
	if(intDeletex)
	{
		$('#frmSupplierPayment #butDelete').show();
	}
	
	//permision for view
	if(intViewx)
	{
		$('#frmSupplierPayment #cboSearch').removeAttr('disabled');
	}*/
	
	$('#frmSupplierPayment #cboSupplier').die('change').live('change',loadData);
	$('#frmSupplierPayment #cboCurrency').die('change').live('change',loadData);
	$('#frmSupplierPayment #search').die('click').live('click',loadData);
	$('#frmSupplierPayment .clsPayAmt').die('keyup').live('keyup',setAmount);
	$('#frmSupplierPayment .clsChkInvoice').die('click').live('click',setCheckAmount);
	$('#frmSupplierPayment .clsAmount').die('keyup').live('keyup',setGLAmount);
	$('#frmSupplierPayment #butInsertRow').die('click').live('click',addNewRow);
	//$('.clsDel').live('click',deleteRow);
	$('#frmSupplierPayment #chkAll').die('click').live('click',checkAll);
	$('#frmSupplierPayment #butSave').die('click').live('click',saveData);
	$('#frmSupplierPayment #butViewAPS').die('click').live('click',viewAdvPaySettle);
	$('#frmSupplierPayment #butNew').die('click').live('click',clearAll);
	$('#frmSupplierPayment #butVoucher').die('click').live('click',viewVoucherReport);
	
	$('#frmSupplierPayment #butConfirm').die('click').live('click',Confirm);
	$('#frmSupplierPayment #butReport').die('click').live('click',Report);
	
	$('#frmSupplierPayment #tblGLMain .clsAccounts').die('change').live('change',GetExRate);
	
	if(supplierIdAL!='')
		LoadAutoData(supplierIdAL,currencyIdAL)

});
function loadData()
{
	if($('#frmSupplierPayment #cboSupplier').val()=='')
	{
		clearData('cboSupplier');
		return;
	}
	if($('#frmSupplierPayment #cboCurrency').val()=='')
	{
		clearData('cboCurrency');
		return;
	}
	clearData('Some');
	
	var currency = $('#frmSupplierPayment #cboCurrency').val();
	var supplier = $('#frmSupplierPayment #cboSupplier').val();
	var grnDateFrom	=$('#frmSupplierPayment #txtDateFrom').val();
	var grnDateTo	=$('#frmSupplierPayment #txtDateTo').val();
	
	if(currency=='' || supplier=='' )
	{
		return;
	}
	showWaiting();
	var url = basePath+"supplierPayment-db-get.php?requestType=loadData";
	var obj = $.ajax({
		url:url,
		dataType: "json", 
		type:'POST', 
		data:'&supplier='+supplier+'&currency='+currency+'&grnDateFrom='+grnDateFrom+'&grnDateTo='+grnDateTo,
		async:false,
		success:function(json){
			if(json.type=='pass')
			{
				//$('#cboLedgerAccount').val(json.ledgerAccount);
				$('#frmSupplierPayment #cboPaymentMethod').val(json.paymentMethod);
				
				if(json.arrDetailData==null)
				{
					hideWaiting();
					return;
				}
				
				var lengthDetail   		= json.arrDetailData.length;
				var arrDetailData  		= json.arrDetailData;	
				
				for(var i=0;i<lengthDetail;i++)
				{
					var invoiceNo		= arrDetailData[i]['invoiceNo'];
					var invoiceDate		= arrDetailData[i]['invoiceDate'];	
					var supInvoiceNo	= arrDetailData[i]['supInvoiceNo'];
					var invoiceAmount	= arrDetailData[i]['invoiceAmount'];
					var advancedAmount	= arrDetailData[i]['advancedAmount'];
					var creditAmount	= arrDetailData[i]['creditAmount'];
					var debitAmount		= arrDetailData[i]['debitAmount'];
					var toBePaid		= arrDetailData[i]['toBePaid'];
					var paidAmount		= arrDetailData[i]['paidAmount'];
					var PONo			= arrDetailData[i]['PONo'];
					var advancedSettle	= arrDetailData[i]['advancedSettle'];
					
					if(invoiceNo!=null)
						createGrid(invoiceNo,invoiceDate,supInvoiceNo,invoiceAmount,advancedAmount,creditAmount,debitAmount,toBePaid,paidAmount,PONo,advancedSettle);
					else
						hideWaiting();
				}
				hideWaiting();
			}
			else
			{
				hideWaiting();
			}
					
		},
		error:function(xhr,status){
			
			hideWaiting();
				
		}		
	});	
}
function createGrid(invoiceNo,invoiceDate,supInvoiceNo,invoiceAmount,advancedAmount,creditAmount,debitAmount,toBePaid,paidAmount,PONo,advancedSettle)
{
	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);
	row.id			= PONo;
	if(parseFloat(advancedSettle)>0)
		row.setAttribute("bgcolor",'#CDFF9B');
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
	cell.id			= invoiceNo;
	cell.className	= 'td_clsInvoiceNo';
	if(parseFloat(advancedSettle)>0)
	{
		cell.innerHTML  = "&nbsp;";
	}
	else
	{
		cell.innerHTML  = "<input type=\"checkbox\" id=\"chkInvoice\" name=\"chkInvoice\" class=\"clsChkInvoice validate[minCheckbox[1]]\">";
	}
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsInvoice';
	cell.innerHTML 	= supInvoiceNo;
	
	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'clsInvoiceDate';
	cell.innerHTML 	= invoiceDate;
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsInvoiceAmt';
	cell.innerHTML 	= invoiceAmount;
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsDebitNote';
	cell.innerHTML 	=(debitAmount==null?'&nbsp;':debitAmount);
	
	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsCreditNote';
	cell.innerHTML 	=(creditAmount==null?'&nbsp;':creditAmount);
	
	var cell 		= row.insertCell(6);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsAdvanceAmt';
	cell.innerHTML 	= (advancedAmount==null?'&nbsp;':advancedAmount);
	
	var cell 		= row.insertCell(7);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsPaidAmt';
	cell.innerHTML 	= (paidAmount==null?'&nbsp;':paidAmount);
	
	var cell 		= row.insertCell(8);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsToBePaid';
	cell.innerHTML 	= toBePaid;
	
	var cell 		= row.insertCell(9);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'clsPayAmount';
	cell.innerHTML 	= "<input type=\"textbox\" id=\"txtPayAmt\" name=\"txtPayAmt\" style=\"width:110px;text-align:right\" value=\""+0+"\" class=\"clsPayAmt\" "+((parseFloat(advancedSettle)>0)?'disabled=\"disabled\"':'')+" >";
}
function saveData()
{
	if(!IsProcessMonthLocked_date($('#txtDate').val()))
		return;
		
	showWaiting();
	var paymentNo 		= $('#frmSupplierPayment #txtPaymentNo').val();
	var paymentYear 	= $('#frmSupplierPayment #txtPaymentYear').val();
	var supplierId 		= $('#frmSupplierPayment #cboSupplier').val();
	var payDate 		= $('#frmSupplierPayment #txtDate').val();
	//var ledgerAccountId = $('#cboLedgerAccount').val();
	var currency 		= $('#frmSupplierPayment #cboCurrency').val();
	var Remarks 		= $('#frmSupplierPayment #txtRemarks').val();
	var payMethod 		= $('#frmSupplierPayment #cboPaymentMethod').val();
	var bankRefNo 		= $('#frmSupplierPayment #txtBankRefNo').val();
	var totPayAmount 	= parseFloat($('#frmSupplierPayment #totPayAmount').html());
	
	if($('#frmSupplierPayment').validationEngine('validate'))
	{
		var totPayAmount 		= parseFloat($('#frmSupplierPayment #totPayAmount').html());
		var totGLAlocAmount 	= parseFloat($('#frmSupplierPayment #totGLAlocAmount').html());
		
		if(totPayAmount!=totGLAlocAmount)
		{
			$('#frmSupplierPayment #totPayAmount').validationEngine('showPrompt','Total Pay amount must equal to Total GL Alocated amonut.','fail');
			hideWaiting();
			return;
		}
		if(totPayAmount<=0 || totGLAlocAmount<=0)
		{
			$('#frmSupplierPayment #butSave').validationEngine('showPrompt','Totals must greater than 0.','fail');
			hideWaiting();
			return;
		}
		var arrHeader = "{";
							arrHeader += '"paymentNo":"'+paymentNo+'",' ;
							arrHeader += '"paymentYear":"'+paymentYear+'",' ;
							arrHeader += '"supplierId":"'+supplierId+'",' ;
							arrHeader += '"payDate":"'+payDate+'",' ;
							arrHeader += '"currency":"'+currency+'",' ;
							arrHeader += '"Remarks":'+URLEncode_json(Remarks)+',';
							arrHeader += '"payMethod":"'+payMethod+'",' ;
							arrHeader += '"totPayAmount":"'+totPayAmount+'",' ;
							arrHeader += '"bankRefNo":"'+bankRefNo+'"' ;

			arrHeader += "}";
			
		value = "[ ";
		var chkStatus  = false;
		var glStatus   = false;
		var amtStatus  = false;
		$('#frmSupplierPayment #tblMain .clsChkInvoice').each(function(){
			
			chkStatus = true;
			if($(this).attr('checked'))
			{
				var invoiceNoArr = $(this).parent().parent().find('.td_clsInvoiceNo').attr('id');
				var PONoArr		 = $(this).parent().parent().attr('id');
				var payAmount 	 = $(this).parent().parent().find('.clsPayAmt').val();
				var invoice		 = $(this).parent().parent().find('.clsInvoice').html();
				
				if(payAmount<=0)
				{
					$(this).parent().parent().find('.clsPayAmt').validationEngine('showPrompt','Amount must greater than 0.','fail');
					amtStatus = true;
					hideWaiting();
					return false;
					
				}
					
				value += "{";
				value += '"invoiceNoArr":"'+ invoiceNoArr +'",' ;
				value += '"PONoArr":"'+ PONoArr +'",' ;
				value += '"invoice":'+URLEncode_json(invoice)+',';
				value += '"payAmount":"'+payAmount+'"' ;
				value += "},";
				
				//value += '{"invoiceNoArr": "'+invoiceNoArr+'", "PONoArr": "'+PONoArr+'", "invoice": "'+invoice+'", "payAmount": "'+payAmount+'" },';
			}
		});
		
		value = value.substr(0,value.length-1);
		value += " ]";
		if(amtStatus)
		{
			return;
		}
		if(!chkStatus)
		{
			$(this).validationEngine('showPrompt','No records to save.','fail');
			hideWaiting();	
			return;
		}
		GLValue = "[ ";
		$('#tblGLMain .clsAmount').each(function(){
				
				glStatus		= true;
				var accountId 	= $(this).parent().parent().find('.clsAccounts').val();
				var amount 		= $(this).val();
				var memo 		= $(this).parent().parent().find('.clsMemo').val();
				var costCenter 	= $(this).parent().parent().find('.clsCostCener').val();
				
				if(amount!=0)
				{
					GLValue += "{";
					GLValue += '"accountId":"'+ accountId +'",' ;
					GLValue += '"amount":"'+ amount +'",' ;
					GLValue += '"memo":'+URLEncode_json(memo)+',';
					GLValue += '"costCenter":"'+costCenter+'"' ;
					GLValue += "},";
				}
		});	
		GLValue = GLValue.substr(0,GLValue.length-1);
		GLValue += " ]";
		
		if(!glStatus)
		{
			$(this).validationEngine('showPrompt','No GL records to save.','fail');
			hideWaiting();	
			return;
		}
		
		var url = basePath+"supplierPayment-db-set.php?requestType=saveData";
			var obj = $.ajax({
				url:url,
				dataType: "json",
				data:'&arrHeader='+arrHeader+'&invDetails='+value+'&GLDetails='+GLValue,
				async:false,
				type:'POST',
				success:function(json){
						$('#frmSupplierPayment #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							var t		= setTimeout("alertx3()",3000);
							$('#frmSupplierPayment #txtPaymentNo').val(json.paymentNo);
							$('#frmSupplierPayment #txtPaymentYear').val(json.paymentYear);
							$('#frmSupplierPayment #butConfirm').show();
							$('#frmSupplierPayment #butVoucher').show();
							hideWaiting();
							return;
						}
						else
						{
							hideWaiting();
						}
					},
				error:function(xhr,status){
						
						$('#frmSupplierPayment #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
					}		
				});
	}
	else
	{
		hideWaiting();
	}
}
function checkAll()
{
	if($(this).attr('checked'))
	{
		$('#frmSupplierPayment .clsChkInvoice').attr("checked",true);
		$('#frmSupplierPayment #tblMain .clsChkInvoice').each(function(){
            
			if($(this).parent().parent().find('.clsPayAmt').val()=='' || $(this).parent().parent().find('.clsPayAmt').val()==0)
			{
				var tobePaid = parseFloat($(this).parent().parent().find('.clsToBePaid').html());
				$(this).parent().parent().find('.clsPayAmt').val(tobePaid);	
			}
			
        });
	}
	else
	{
		$('#frmSupplierPayment .clsAmount').val('');
		$('#frmSupplierPayment #totGLAlocAmount').html('');
		$('#frmSupplierPayment #totPayAmount').html('');
		$('#frmSupplierPayment .clsChkInvoice').attr("checked",false);
		$('#frmSupplierPayment .clsPayAmt').val(0);
	}
	CaculateTotalValue();
}
function clearData(type)
{
	$("#frmSupplierPayment #tblMain tr:gt(0)").remove();
	$('#frmSupplierPayment .clsAmount').val('');
	$('#frmSupplierPayment #totPayAmount').html(0.00);
	$('#frmSupplierPayment #totGLAlocAmount').html(0.00);
}
function setAmount()
{
	var tobePaid = parseFloat($(this).parent().parent().find('.clsToBePaid').html());
	if(parseFloat($(this).val())>tobePaid)
	{
		$(this).val(tobePaid);
	}
	$('.clsAmount').val('');
	CaculateTotalValue();
}
function setCheckAmount()
{
	$('#frmSupplierPayment .clsAmount').val('');
	$('#frmSupplierPayment #totGLAlocAmount').html('');
	
	var tobePaid = parseFloat($(this).parent().parent().find('.clsToBePaid').html());		
	$(this).parent().parent().find('.clsPayAmt').val(tobePaid);
	
	$('#frmSupplierPayment .clsChkInvoice').each(function(){
        
		if($(this).attr('checked'))
		{
			$(this).parent().parent().find('.clsPayAmt').addClass('validate[required]');
		}
		else
		{
			$(this).parent().parent().find('.clsPayAmt').removeClass('validate[required]');
			$(this).parent().parent().find('.clsPayAmt').val(0);
			$('#chkAll').attr('checked' , false);
		}
    });
	CaculateTotalValue();
}
function CaculateTotalValue()
{
	var totalValue		= 0;
	var totGLValue		= 0;
	
	$('#frmSupplierPayment #tblMain .clsPayAmt').each(function(){
		if($(this).parent().parent().find('.clsChkInvoice').attr('checked'))
		{
			totalValue += parseFloat($(this).val());
		}
	});
	
	$('#frmSupplierPayment #tblGLMain .clsAmount').val(RoundNumber(totalValue,2));
	
	$('#frmSupplierPayment #tblGLMain .clsAmount').each(function(){
		
		totGLValue += parseFloat($(this).val()==''?0:$(this).val());	
		
	});
	totalValue	= RoundNumber(totalValue,2);
	totGLValue	= RoundNumber(totGLValue,2);
	
	$('#frmSupplierPayment #totPayAmount').html(totalValue);	
	$('#frmSupplierPayment #totGLAlocAmount').html(totGLValue);	
}
function setGLAmount()
{
	var obj			 = this;
	var totGLAmount  = 0;
	var totAmount 	 = parseFloat($('#frmSupplierPayment #totPayAmount').html());
	$('#frmSupplierPayment .clsAmount').each(function(){

		if($(this).val()!="")
			totGLAmount += parseFloat($(this).val());
	});
	if(totGLAmount>totAmount)
	{
		obj.value = (parseFloat(obj.value)-parseFloat(parseFloat(totGLAmount)-parseFloat(totAmount)));
		totLedAmount = totAmount;
	}
	CaculateTotalValue();
}
function addNewRow()
{
	$('#frmSupplierPayment #tblGLMain tbody tr:last').after("<tr>"+$('#frmSupplierPayment #tblGLMain .cls_tr_firstRow').html()+"</tr>");	
}
function deleteRow()
{
	var delRowCount = parseInt(document.getElementById('tblGLMain').rows.length);
	
	if(delRowCount>3)
		$(this).parent().parent().remove();
	
	if(parseInt(document.getElementById('tblGLMain').rows.length)==3)
		$('#frmSupplierPayment #tblGLMain tbody tr:last').addClass('cls_tr_firstRow');
	
	CaculateTotalValue();
}


function viewAdvPaySettle()
{
	var supplierId = $('#frmSupplierPayment #cboSupplier').val();
	var currencyId = $('#frmSupplierPayment #cboCurrency').val();
	
	window.open('?q='+advSettleMenuId+'&supplierId='+supplierId+'&currencyId='+currencyId);
}
function viewVoucherReport()
{
	var url  = voucherRptPath+"rpt_payment_voucher_pdf.php?SerialNo="+$('#frmSupplierPayment #txtPaymentNo').val();
		url += "&SerialYear="+$('#frmSupplierPayment #txtPaymentYear').val();
		url += "&Type=PAYMENT";
	
	window.open(url,'rpt_payment_voucher_pdf.php');
	
}
function Confirm()
{
	var url  = "?q="+reportId+"&paymentNo="+$('#frmSupplierPayment #txtPaymentNo').val();
	    url += "&paymentYear="+$('#frmSupplierPayment #txtPaymentYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rptSupplierPayment.php');
}
function Report()
{
	if($('#frmSupplierPayment #txtPaymentNo').val()=='')
	{
		$('#frmSupplierPayment #butReport').validationEngine('showPrompt','No payment no to view Report','fail');
		return;	
	}
	var url  = "?q="+reportId+"&paymentNo="+$('#frmSupplierPayment #txtPaymentNo').val();
	     url += "&paymentYear="+$('#frmSupplierPayment #txtPaymentYear').val();
	window.open(url,'rptSupplierPayment.php');
}
function LoadAutoData(supplierId,currency)
{
	$('#frmSupplierPayment #cboSupplier').val(supplierId);
	$('#frmSupplierPayment #cboCurrency').val(currency);
	loadData();	
}
function clearAll()
{
	document.location.href = "?q="+menuId;
}
function alertx3()
{
	$('#frmSupplierPayment #butSave').validationEngine('hide')	;
}

function GetExRate()
{
		var obj	  = this;
	var data  = "Date="+$('#frmSupplierPayment #txtDate').val();
		data += "&GLID="+$(obj).val();
		data += "&CurrId="+$('#frmSupplierPayment #cboCurrency').val();
	
	var httpobj = $.ajax({
	url:basePath+"supplierPayment-db-get.php?requestType=CheckBankExRate",
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		//$(obj).parent().attr('id',json.RATE);
		if($('#frmSupplierPayment #cboCurrency').val()!=json.CURRENCY_ID)
			$('#frmSupplierPayment #txtCurrencyRate').val(json.RATE);
		else
			GetCommonExchangeRate1();
  	}
	});	
	
}

function GetCommonExchangeRate1()
{
	var url 		= "libraries/php/common.php?RequestType=URLGetExchangeRate";
		url 	   += "&Date="+$('#frmSupplierPayment #txtDate').val();
		url 	   += "&CurrId="+$('#frmSupplierPayment #cboCurrency').val();
		
	var httpobj 	= $.ajax({url:url,type:'POST',async:false})
	$('#frmSupplierPayment #txtCurrencyRate').val(httpobj.responseText);
}
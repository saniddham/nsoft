<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId				= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$userId 				= $_SESSION['userId'];

include  		"class/finance/cls_get_gldetails.php";
include_once 	"class/masterData/exchange_rate/cls_exchange_rate_get.php";
include_once  	"class/cls_commonErrorHandeling_get.php";
include_once  	"class/cls_commonFunctions_get.php";
//include 		"include/javascript.html";

$obj_get_GLCombo		= new Cls_Get_GLDetails($db);
$obj_exchgRate_get		= new cls_exchange_rate_get($db);
$obj_common				= new cls_commonFunctions_get($db);
$obj_commonErrHandle	= new cls_commonErrorHandeling_get($db);

$paymentNo 				= (!isset($_REQUEST['paymentNo'])?'':$_REQUEST['paymentNo']);
$paymentYear 			= (!isset($_REQUEST['paymentYear'])?'':$_REQUEST['paymentYear']);

$programCode			= 'P0691';
$totPaidAmount			= 0.00;

$supplierIdAL			= (!isset($_REQUEST['supplierIdAL'])?'':$_REQUEST['supplierIdAL']);
$currencyIdAL			= (!isset($_REQUEST['currencyIdAL'])?'':$_REQUEST['currencyIdAL']);

$fromDate				=date('Y-m-01', strtotime(date("Y-m-d")));
$toDate					=date('Y-m-t',  strtotime(date("Y-m-d")));

if($paymentNo!='' && $paymentYear!='')
{
	$sql = "SELECT 	SUPPLIER_ID, 
			PAY_DATE,  
			CURRENCY_ID, 
			PAY_METHOD, 
			REMARKS, 
			BANK_REFERENCE_NO, 
			STATUS,
			APPROVE_LEVELS,
			(SELECT SUM(AMOUNT)
			FROM finance_supplier_payment_details
			WHERE PAYMENT_NO=SPH.PAYMENT_NO AND
			PAYMENT_YEAR=SPH.PAYMENT_YEAR) totPaidAmount
			FROM finance_supplier_payment_header SPH
			WHERE PAYMENT_NO='$paymentNo' AND
			PAYMENT_YEAR='$paymentYear' ";
	
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$supplierId 	= $row['SUPPLIER_ID'];
		$payDate 		= $row['PAY_DATE'];
		//$ledgerAccId 	= $row['LEDGER_ACCOUNT_ID'];
		$payMethod 		= $row['PAY_METHOD'];
		$bankRefNo 		= $row['BANK_REFERENCE_NO'];
		$remarks 		= $row['REMARKS'];
		$currencyId 	= $row['CURRENCY_ID'];
		$status 		= $row['STATUS'];
		$levels 		= $row['APPROVE_LEVELS'];	
		$totPaidAmount 	= $row['totPaidAmount'];
		
		$row			= $obj_exchgRate_get->GetAllValues($currencyId,4,$payDate,$companyId,'RunQuery');
		$exchgRate		= $row['AVERAGE_RATE'];	
	}
	$detail_result 	= GetGridDetails($paymentNo,$paymentYear,$supplierId,$currencyId);
	$GL_result 		= GetGLGridDetails($paymentNo,$paymentYear);
}
$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_save($status,$levels,$userId,$programCode,'RunQuery');
$permision_save			= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_reject($status,$levels,$userId,$programCode,'RunQuery');
$permision_reject		= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_confirm($status,$levels,$userId,$programCode,'RunQuery');
$permision_confirm		= $permition_arr['permision'];

$dateChangeMode = $obj_common->ValidateSpecialPermission('12',$userId,'RunQuery');
$cancleMode 	= loadCancleMode($programCode,$userId);

?>
<title>Supplier Payments</title>

<!--<script type="text/javascript" src="presentation/finance_new/supplier/supplierPayment/supplierPayment-js.js"></script>-->

<style>
#apDiv1 {
	position: absolute;
	left: 144px;
	top: 116px;
	width: auto;
	height: auto;
	z-index: 1;
}
</style>

<script>
	var supplierIdAL	='<?php echo $supplierIdAL; ?>';
	var currencyIdAL	='<?php echo $currencyIdAL; ?>';
</script>

<?php
if($status==-2)
{
?>
	
<div id="apDiv1" style="display:none"><img src="images/cancelled.png" /></div>
<?php
}
?>
<form id="frmSupplierPayment" name="frmSupplierPayment" method="post">
<div align="center">
<div class="trans_layoutL" style="width:1000px">
<div class="trans_text">Supplier Payments</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
              <td width="17%" class="normalfnt">Payment No</td>
              <td width="36%" class="normalfnt"><input name="txtPaymentNo" type="text" disabled="disabled" id="txtPaymentNo" style="width:60px" value="<?php echo $paymentNo; ?>" />&nbsp;<input name="txtPaymentYear" type="text" disabled="disabled" id="txtPaymentYear" style="width:35px" value="<?php echo $paymentYear; ?>" /></td>
              <td width="21%" class="normalfnt">&nbsp;</td>
              <td width="26%" class="normalfnt"></td>
            </tr>
            <tr>
              <td class="normalfnt">Supplier <span class="compulsoryRed">*</span></td>
              <td class="normalfnt" valign="top"><select name="cboSupplier" id="cboSupplier"  style="width:220px" class="validate[required] cls_ex" >
              <?php
				
					$sql = "SELECT distinct PO.intSupplier,MS.strName
							FROM ware_grnheader GH
							INNER JOIN mst_locations ML ON ML.intId=GH.intCompanyId
							INNER JOIN trn_poheader PO ON GH.intPoNo=PO.intPONo AND GH.intPoYear=PO.intPOYear
							INNER JOIN mst_supplier MS ON PO.intSupplier=MS.intId
							WHERE GH.intStatus=1 AND
							ML.intCompanyId='$companyId'
							ORDER BY MS.strName ";
					$result = $db->RunQuery($sql);
					echo "<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						if($row['intSupplier']==$supplierId)
							echo "<option value=\"".$row['intSupplier']."\" selected=\"selected\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intSupplier']."\" >".$row['strName']."</option>"; 
					}
			  ?>          
              </select></td>
              <td class="normalfnt">Date</td>
              <td class="normalfnt"><input name="txtDate" type="text" value="<?php echo($payDate==''?date("Y-m-d"):$payDate); ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtDate" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($dateChangeMode!=1?'disabled="disabled"':''); ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
            </tr>
            <tr>
              <td class="normalfnt">Currency <span class="compulsoryRed">*</span></td>
              <td class="normalfnt" valign="top"><select name="cboCurrency" id="cboCurrency" style="width:100px" class="validate[required] cls_cbo_CurrencyCalExchangeRate" >
                <option value=""></option>
                <?php
				$sql = "SELECT intId,strCode FROM mst_financecurrency WHERE intStatus=1 ";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($row['intId']==$currencyId)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";
					else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
				}
			  ?>
              </select></td>
              <td class="normalfnt">Rate</td>
              <td class="normalfnt"><input type="text" name="txtCurrencyRate" id="txtCurrencyRate" style="width:100px;text-align:right" disabled="disabled" value="<?php echo($paymentNo==''?'0.0000':$exchgRate); ?>" class="cls_txt_exchangeRate" /></td>
            </tr>
            <tr>
              <td class="normalfnt" valign="top">GRN Date Range</td>
              <td class="normalfnt" valign="top" ><input name="txtDateFrom" type="text" value="<?php echo($fromDate==''?date("Y-m-d"):$fromDate); ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtDateFrom" style="width:80px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" />&nbsp;To&nbsp;&nbsp;&nbsp;&nbsp;<input name="txtDateTo" type="text" value="<?php echo($toDate==''?date("Y-m-d"):$toDate); ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtDateTo" style="width:80px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"  /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /><a id="search" class="button green small" title="Click here to search balance to payments records" name="search" >Search</a></td>
              <td class="normalfnt" valign="top">Remarks</td>
              <td rowspan="3" valign="top" class="normalfnt"><textarea name="txtRemarks" id="txtRemarks" style="width:220px" rows="3"><?php echo $remarks; ?></textarea></td>
            </tr>
            <tr>
              <td class="normalfnt" valign="top" >Payment Method <span class="compulsoryRed">*</span></td>
              <td class="normalfnt" valign="top" ><select name="cboPaymentMethod" id="cboPaymentMethod" style="width:220px" class="validate[required]" >
                <option value=""></option>
                <?php
					$sql = "SELECT
							mst_financepaymentsmethods.intId,
							mst_financepaymentsmethods.strName
							FROM mst_financepaymentsmethods
							WHERE
							mst_financepaymentsmethods.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$payMethod)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
					?>
              </select></td>
              <td class="normalfnt" valign="top">&nbsp;</td>
              </tr>
            <tr>
              <td class="normalfnt" valign="top" >Bank Reference Number</td>
              <td class="normalfnt" valign="top" ><input type="text" name="txtBankRefNo" id="txtBankRefNo" style="width:215px" value="<?php echo $bankRefNo; ?>" /></td>
              <td class="normalfnt" valign="top" >&nbsp;</td>
              </tr>
            <tr>
              <td colspan="2" class="normalfnt"><table width="100%" border="0">
                <tr>
                  <td width="5%" align="center"><div style="width:10px;height:10px;border: 1px solid #000;background-color:#CDFF9B"></div></td>
                  <td width="95%" class="normalfnt">Awaiting Advance payment settlement</td>
                </tr>
              </table></td>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt" style="text-align:right"><a class="button green small" id="butViewAPS" name="butViewAPS">Advance Payment Settlement</a></td>
            </tr>
            <tr>
              <td colspan="4" class="normalfnt"><div style="overflow:scroll;overflow-x:hidden;width:1000px;height:250px;">
              <table width="100%" border="0" class="bordered" id="tblMain">
        <thead>
          <tr>
            <th width="3%"><input type="checkbox" id="chkAll" name="chkAll" class="clsChkAll"></th>
            <th width="12%">Invoice No</th>
            <th width="11%">Invoiced Date</th>
            <th width="13%">Invoiced Amount</th>
            <th width="10%">Debit Note</th>
            <th width="10%">Credit Note</th>
            <th width="9%">Advance</th>
             <th width="10%">Paid Amount</th>
            <th width="9%">To Be Paid</th>
            <th width="13%">Paying Amount <span class="compulsoryRed">*</span></th>
            </tr>
        </thead>        
        <tbody>
		<?php
        while($row = mysqli_fetch_array($detail_result))
        {
			if($row['advPaymentCount']>0 && $row['settelmentCount']==0)
			{
				$rowColor = "#CDFF9B";
				$settelemnet = true;
			}
			else
			{
				$rowColor = "#FFFFFF"; 
				$settelemnet = false;
			}
        ?>
		<tr bgcolor="<?php echo $rowColor; ?>" id="<?php echo $row['PONo']; ?>">
            <td id="<?php echo $row['invoiceNo']; ?>" style="text-align:center" class="td_clsInvoiceNo">
			<?php
			if($settelemnet)
			{
				echo '&nbsp;';
			}
			else
			{
				if($row['payAmount']>0)
					echo '<input type="checkbox" id="chkInvoice" name="chkInvoice" class="clsChkInvoice validate[minCheckbox[1]]" checked="checked">';
				else
					echo '&nbsp;';	
			}
			?></td>
            <td class="clsInvoice" style="text-align:left"><?php echo $row["INVOICE_NO"]?></td>
            <td class="clsInvoiceDate" style="text-align:center"><?php echo $row["PURCHASE_DATE"]?></td>
            <td class="clsInvoiceAmt" style="text-align:right"><?php echo number_format($row['invoiceAmount'],2,'.','')?></td>
            <td class="clsDebitNote" style="text-align:right"><?php echo ($row["debitAmount"]==null?'&nbsp;':number_format($row['debitAmount'],2,'.',''));?></td>
            <td class="clsCreditNote" style="text-align:right"><?php echo ($row["creditAmount"]==null?'&nbsp;':number_format($row['creditAmount'],2,'.',''));?></td>
            <td class="clsAdvanceAmt" style="text-align:right"><?php echo ($row["advancedAmount"]==null?'&nbsp;':number_format($row['advancedAmount'],2,'.',''));?></td>
            <td class="clsPaidAmt" style="text-align:right"><?php echo ($row["paidAmount"]==null?'&nbsp;':number_format($row['paidAmount'],2,'.',''));?></td>
            <td class="clsToBePaid" style="text-align:right"><?php echo number_format($row['toBePaid'],2,'.','')?></td>
            <td class="clsPayAmount" style="text-align:center"><input type="textbox" id="txtPayAmt" name="txtPayAmt" style="width:110px;text-align:right" value="<?php echo number_format($row['payAmount'],2,'.','') ?>" class="clsPayAmt validate[custom[number]]" ></td>
            </tr>
		<?php
        }
        ?>
        </tbody>
        </table></div></td>
            </tr>
            <tr>
              <td colspan="4" class="normalfnt">
                <table width="100%" class="bordered" id="tblGLMain" >
                  <thead>
                    <tr>
                      <th width="29%" height="30" >Account <span class="compulsoryRed">*</span></th>
                      <th width="13%" >Amount <span class="compulsoryRed">*</span></th>
                      <th width="41%" >Memo </th>
                      <th width="14%" >Cost Center <span class="compulsoryRed">*</span></th>
                      </tr>
                    </thead>
                  <tbody>
                    <?php
				   $totGLAmount = 0.00;
				   if($paymentNo!='' && $paymentYear!='')
				   {
						while($row=mysqli_fetch_array($GL_result))
						{
							$totGLAmount = $totGLAmount+$row['AMOUNT'];
						?>
                    <tr class="cls_tr_firstRow">
                      <td width="29%" style="text-align:center" ><select name="cboAccounts" id="cboAccounts" style="width:100%" class="clsAccounts validate[required]">
                        <?php
                        	echo $obj_get_GLCombo->getGLCombo('SUPPLIER_PAYMENT',$row['ACCOUNT_ID']);
                        ?>
                      </select></td>
                      <td width="13%" style="text-align:center" ><input name="txtAmmount" type="text" id="txtAmmount" style="width:100%;text-align:right" class="validate[required,custom[number]] clsAmount" value="<?php echo number_format($row['AMOUNT'], 2, '.', ''); ?>" /></td>
                      <td width="41%" style="text-align:center" ><input type="text" name="txtMemo" id="txtMemo" style="width:100%" class="clsMemo" value="<?php echo $row['MEMO']; ?>" /></td>
                      <td width="14%" style="text-align:center" ><select name="cboCostCenter" id="cboCostCenter" class="clsCostCener validate[required]" style="width:100%">
                        <option value=""></option>
                        <?php
						$sqlCC = "SELECT intId,strName
									FROM mst_financedimension
									WHERE intStatus=1";
						$resultCC = $db->RunQuery($sqlCC);
						while($rowCC=mysqli_fetch_array($resultCC))
						{
							if($rowCC['intId']==$row['COST_CENTER'])
								echo "<option value=\"".$rowCC['intId']."\" selected=\"selected\">".$rowCC['strName']."</option>";
							else
								echo "<option value=\"".$rowCC['intId']."\">".$rowCC['strName']."</option>";
						}
						?>
                        </select></td>
                      </tr>
                    <?php
						}
				   }
				   else
				   {
				   ?>
                    <tr class="cls_tr_firstRow">
                      <td width="29%" style="text-align:center" ><select name="cboAccounts" id="cboAccounts" style="width:100%" class="clsAccounts validate[required]">
                        <?php
						echo $obj_get_GLCombo->getGLCombo('SUPPLIER_PAYMENT','');
					?>
                      </select></td>
                      <td width="13%" style="text-align:center" ><input name="txtAmmount" type="text" id="txtAmmount" style="width:100%;text-align:right" class="validate[required,custom[number]] clsAmount" /></td>
                      <td width="41%" style="text-align:center" ><input type="text" name="txtMemo" id="txtMemo" style="width:100%" class="clsMemo" /></td>
                      <td width="14%" style="text-align:center" ><select name="cboCostCenter" id="cboCostCenter" class="clsCostCener validate[required]" style="width:100%">
                        <option value=""></option>
                        <?php
						$sqlCC = "SELECT intId,strName
									FROM mst_financedimension
									WHERE intStatus=1";
						$resultCC = $db->RunQuery($sqlCC);
						while($rowCC=mysqli_fetch_array($resultCC))
						{
							if($rowCC['intId']==$row['COST_CENTER'])
								echo "<option value=\"".$rowCC['intId']."\" selected=\"selected\">".$rowCC['strName']."</option>";
							else
								echo "<option value=\"".$rowCC['intId']."\">".$rowCC['strName']."</option>";
						}
						?>
                        </select></td>
                      </tr>
                    <?php
				   }
				   ?>
                    </tbody>
                  </table>
                </td>
            </tr>
            <tr>
              <td colspan="2" class="normalfnt" valign="top">&nbsp;</td>
              <td colspan="2" class="normalfnt" style="text-align:right"><table width="301" border="0" align="right" class="normalfnt">
          <tr>
            <td width="166">Total Paying Amount</td>
            <td width="11" style="text-align:center">:</td>
            <td width="91" style="text-align:right;" id="totPayAmount"><?php echo number_format($totPaidAmount, 2, '.', ''); ?></td>
          </tr>
          <tr>
            <td>Total GL Allocated Amount</td>
            <td style="text-align:center"><b>:</b></td>
            <td style="text-align:right" id="totGLAlocAmount"><?php echo number_format($totGLAmount, 2, '.', ''); ?></td>
          </tr>
        </table></td>
              </tr>
            </table>
        </td>
    </tr>
    
    <tr>
    	<td height="32" >
    		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td align="center" class="tableBorder_allRound"><a class="button white medium" id="butNew" name="butNew">&nbsp;New&nbsp;</a><a class="button white medium" id="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a><a class="button white medium" id="butConfirm"  <?php if($permision_confirm!=1){ ?> style="display:none"<?php } ?>>Approve</a><a class="button white medium" id="butVoucher" name="butVoucher" <?php echo (($paymentNo!='' && $paymentYear!='')?'':'style="display:none"');?>>Voucher</a><a class="button white medium" id="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
              </tr>
            </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>
<?php
function loadDateChangeMode($programCode,$userId)
{
	global $db;
	
	$Mode	= 0;
	$sql = "SELECT MS.intStatus
			FROM menus M
			INNER JOIN menus_special MS ON M.intId=MS.intMenuId
			INNER JOIN menus_special_permision SP ON MS.intId=SP.intSpMenuId
			WHERE M.strCode='$programCode' AND
			SP.intUser='$userId' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	if($row['intStatus']==1)
	{
		$Mode = 1;
	}
	return $Mode;
}
function loadCancleMode($programCode,$userId)
{
	global $db;
	$Mode	= 0;
	$sql = "SELECT
			menupermision.intCancel 
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId = '$userId' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	if($row['intCancel']==1)
	{
		$Mode = 1;
	}
	return $Mode;
}
function getAdvancedAmount($PONo,$POYear)
{
	global $db;
	
	$sql = "SELECT SUM(dblUnitPrice*dblQty) AS POAmount,
			(SELECT IFNULL(SUM(PAY_AMOUNT),0) FROM finance_supplier_advancepayment_header SAPH
			WHERE SAPH.PO_NO=POD.intPONo AND SAPH.PO_YEAR=POD.intPOYear ) AS advancedAmunt
			FROM trn_podetails POD
			WHERE POD.intPONo='$PONo' AND POD.intPOYear='$POYear' ";
		
	return $db->RunQuery($sql);
}
function GetGridDetails($paymentNo,$paymentYear,$supplierId,$currencyId)
{
	global $db;
	
	$sql = "SELECT CONCAT(SPIH.PURCHASE_INVOICE_NO,'/',SPIH.PURCHASE_INVOICE_YEAR) AS invoiceNo,
			SPIH.PURCHASE_DATE,
			SPIH.INVOICE_NO,
			(SELECT ROUND(SUM(ST.VALUE),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='INVOICE') AS invoiceAmount,
			
			(SELECT ROUND((SUM(ST.VALUE)*-1),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='ADVANCE') AS advancedAmount,
			
			(SELECT ROUND(SUM(ST.VALUE),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='CREDIT') AS creditAmount,
			
			(SELECT ROUND((SUM(ST.VALUE)*-1),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='DEBIT') AS debitAmount,
			
			(SELECT ROUND(SUM(ST.VALUE),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR) AS toBePaid,
			
			CONCAT(GH.intPoNo,'/',GH.intPoYear) PONo,
			
			(SELECT ROUND((SUM(ST.VALUE)*-1),4)
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
			ST.DOCUMENT_TYPE='PAYMENT') AS paidAmount,
			
			(SELECT SUM(AMOUNT)
			FROM finance_supplier_payment_details SPD
			WHERE SPD.PAYMENT_NO='$paymentNo' AND
			SPD.PAYMENT_YEAR='$paymentYear' AND
			SPD.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			SPD.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR ) payAmount,
			
			IFNULL((SELECT COUNT(SERIAL_NO) 
			FROM finance_supplier_transaction ST
			WHERE ST.PO_NO=GH.intPoNo AND ST.PO_YEAR=GH.intPoYear AND ST.DOCUMENT_TYPE='ADVANCE' HAVING SUM(VALUE)!=0),0) advPaymentCount,
			
			IFNULL((SELECT COUNT(SERIAL_NO)
			FROM finance_supplier_transaction ST
			WHERE ST.PO_NO=GH.intPoNo AND ST.PO_YEAR=GH.intPoYear AND 
			ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND 
			ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND ST.DOCUMENT_TYPE='ADVANCE' HAVING SUM(VALUE)!=0 ),0) settelmentCount

			FROM finance_supplier_purchaseinvoice_header SPIH
			INNER JOIN finance_supplier_purchaseinvoice_details SPID ON SPID.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
			SPID.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR
			INNER JOIN ware_grnheader GH ON GH.strInvoiceNo=SPIH.INVOICE_NO
			INNER JOIN trn_poheader POH ON POH.intPONo=GH.intPoNo AND
			POH.intPOYear=GH.intPoYear AND POH.intSupplier= SPIH.SUPPLIER_ID
			WHERE SPIH.STATUS!='-2' AND
			SPIH.SUPPLIER_ID='$supplierId' AND
			SPIH.CURRENCY_ID='$currencyId' AND
			POH.intSupplier='$supplierId'
			GROUP BY SPIH.PURCHASE_INVOICE_NO,SPIH.PURCHASE_INVOICE_YEAR
			 ";	
	return $db->RunQuery($sql);
}
function GetGLGridDetails($paymentNo,$paymentYear)
{
	global $db;
	
	$sql = "SELECT 	
			ACCOUNT_ID, 
			AMOUNT, 
			MEMO, 
			COST_CENTER
			FROM finance_supplier_payment_gl 
			WHERE PAYMENT_NO='$paymentNo' AND PAYMENT_YEAR='$paymentYear' ";
	
	return $db->RunQuery($sql);
}
?>

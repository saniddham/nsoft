<?php 
//ini_set('display_errors',1);
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$requestType 		= $_REQUEST['requestType'];

include "../../../../dataAccess/Connector.php";
include_once "../../../../class/masterData/exchange_rate/cls_exchange_rate_get.php";
include_once ("../../../../class/finance/masterData/chartOfAccount_bank/cls_chartofaccount_bank_get.php");

$obj_exchgRate_get	= new cls_exchange_rate_get($db);
$obj_chartOfAccount_bank_get	= new cls_chartOfAccount_bank_get($db);

if($requestType=='loadData')
{
	$supplier 	= $_REQUEST['supplierId'];
	$date 		= $_REQUEST['date'];
	
	$sql = "SELECT DISTINCT intPONo,intPOYear
			FROM trn_poheader
			INNER JOIN mst_locations ON mst_locations.intId=trn_poheader.intCompany
			WHERE intSupplier='$supplier' AND
			trn_poheader.intStatus=1 AND
			PAYMENT_COMPLETED_FLAG = 0 AND
			mst_locations.intCompanyId = '$companyId' ";
	$result = $db->RunQuery($sql);
	$html 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$html.="<option value=\"".$row['intPONo'].'/'.$row['intPOYear']."\">".$row['intPONo'].'/'.$row['intPOYear']."</option>";
		$PONo = $row['intPONo'].'/'.$row['intPOYear'];
	}
	if(mysqli_num_rows($result)>1)
	{
		$response['type']			= 'pass';
		$response['POCombo']		= $html;
		$response['PONo']			= '';
		echo json_encode($response);
	}
	else if(mysqli_num_rows($result)==1)
	{
		loadPODetails($supplier,$PONo,$companyId,$html,$date);
	}
}
else if($requestType=='loadPOData')
{
	$PONo 		= $_REQUEST['PONo'];
	$supplier 	= $_REQUEST['supplier'];
	$date 		= $_REQUEST['date'];
	
	loadPODetails($supplier,$PONo,$companyId,'',$date);
}
else if($requestType=='loadExchangeRate')
{
	$currency				= $_REQUEST['currencyId'];
	$date	  				= $_REQUEST['date'];
	
	$row					= $obj_exchgRate_get->GetAllValues($currency,4,$date,$companyId,'RunQuery');
	$response['exchgRate']	= $row['AVERAGE_RATE'];
	
	echo json_encode($response);
}
else if($requestType == "CheckBankExRate")
{
	$result		 			= $obj_chartOfAccount_bank_get->GetDetails($_REQUEST["GLID"],'RunQuery');
	$data					= $obj_exchgRate_get->GetAllBankValues($result["CURRENCY_ID"],$result["BANK_ID"],2,$_REQUEST["Date"],$companyId,'RunQuery');
	$data1					= $obj_exchgRate_get->GetAllBankValues($_REQUEST["CurrId"],$result["BANK_ID"],2,$_REQUEST["Date"],$companyId,'RunQuery');
	//$response["RATE"]		= round(($data["AVERAGE_RATE"]/1)*$data1["AVERAGE_RATE"],2);
	$response["RATE"]		= round($data1["AVERAGE_RATE"]*$data["AVERAGE_RATE"],2);
	$response["CURRENCY_ID"]= $result["CURRENCY_ID"];
	echo json_encode($response);
}
function loadPODetails($supplier,$PONo,$companyId,$html,$date)
{
	global $obj_exchgRate_get;
	
	$PONoArr						= explode('/',$PONo);
	//$ledgerAccount					= getLedgerAccount($supplier,$companyId);
	$currency						= getCurrency($PONoArr[0],$PONoArr[1]);
	$paymetMethod					= getPayMethod($PONoArr[0],$PONoArr[1]);
	$costCenter						= getCostCenter($PONoArr[0],$PONoArr[1]);
	$result							= getAdvancedAmount($PONoArr[0],$PONoArr[1]);
	$row							= mysqli_fetch_array($result);

	$response['recievedAmonut']		= $row['recievedAmonut'];
	$response['balAmount']			= $row['POAmount']-($row['recievedAmonut']);
	$response['POAmount']			= $row['POAmount'];
	$response['PONo']				= $PONoArr[0];
	$response['POYear']				= $PONoArr[1];
	
	$rowExchng						= $obj_exchgRate_get->GetAllValues($currency,4,$date,$companyId,'RunQuery');
	$response['exchgRate']			= $rowExchng['AVERAGE_RATE'];

	$response['type']				= 'pass';
	if($html!='')
	{
		$response['POCombo']		= $html;
		$response['PONo']			= $PONo;
	}
	//$response['ledgerAccount']		= $ledgerAccount;
	$response['currency']			= $currency;
	$response['paymentMethod']		= $paymetMethod;
	$response['costCenter']			= $costCenter;
	
	echo json_encode($response);
}
function getLedgerAccount($spplierId,$companyId)
{
	global $db;
	
	$sql = "SELECT DISTINCT intChartOfAccountId
			FROM mst_financesupplieractivate SA
			WHERE intSupplierId='$spplierId' AND
			intCompanyId='$companyId' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['intChartOfAccountId'];	
}
function getCurrency($PONo,$POYear)
{
	global $db;
	
	$sql = "SELECT intCurrency FROM trn_poheader WHERE intPONo='$PONo' AND intPOYear='$POYear' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['intCurrency'];
}
function getPayMethod($PONo,$POYear)
{
	global $db;
	
	$sql = "SELECT IFNULL(intPaymentMode,'') as paymentMode FROM trn_poheader
			WHERE intPONo='$PONo' AND
			intPOYear='$POYear' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['paymentMode'];
}
function getAdvancedAmount($PONo,$POYear)
{
	global $db;
	
	/*$sql = "SELECT SUM(dblUnitPrice*dblQty) AS POAmount,
			(SELECT IFNULL(SUM((VALUE)*-1),0) FROM finance_supplier_transaction
			WHERE PO_NO=POD.intPONo AND
			PO_YEAR=POD.intPOYear AND
			DOCUMENT_TYPE='ADVANCE') AdvanceAmonut,
			(SELECT IFNULL(SUM((VALUE)*-1),0) FROM finance_supplier_transaction
			WHERE PO_NO=POD.intPONo AND
			PO_YEAR=POD.intPOYear AND
			DOCUMENT_TYPE='PAYMENT') PaidAmount
			FROM trn_podetails POD
			WHERE POD.intPONo='$PONo' AND 
			POD.intPOYear='$POYear' ";*/
	
	$sql = "SELECT SUM(ROUND(((dblQty*dblUnitPrice)*((100-dblDiscount)/100)+dblTaxAmmount),2)) AS POAmount,
			(SELECT IFNULL((SUM(VALUE)*-1),0) FROM finance_supplier_transaction
			WHERE PO_NO=POD.intPONo AND
			PO_YEAR=POD.intPOYear AND
			DOCUMENT_TYPE!='INVOICE') recievedAmonut
			FROM trn_podetails POD
			WHERE POD.intPONo='$PONo' AND 
			POD.intPOYear='$POYear'";
		
	return $db->RunQuery($sql);
}
function getCostCenter($PONo,$POYear)
{
	global $db;
	
	$sql = "SELECT DISTINCT FD.intId
			FROM mst_financedimension FD
			INNER JOIN trn_poheader PH ON PH.intCompany=FD.intLocation
			WHERE PH.intPONo='$PONo' AND
			PH.intPOYear='$POYear' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['intId'];	
}
?>
<?php 

session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$requestType 		= $_REQUEST['requestType'];
$programCode		= 'P0683';

include "{$backwardseperator}dataAccess/Connector.php";
require_once "../../../../class/cls_commonFunctions_get.php";
require_once "../../../../class/finance/cls_common_get.php";
//include_once "../../../../class/masterData/exchange_rate/cls_exchange_rate_get.php";

$obj_common	   		= new cls_commonFunctions_get($db);
$finance_common_get = new cls_common_get($db);
$obj_exchgRate_get	= new cls_exchange_rate_get($db);

$savedStatus		= true;
$savedMasseged		= '';
$error_sql			= '';

$arrHeader			= json_decode($_REQUEST['arrHeader'], true);
$arr 				= json_decode($_REQUEST['accDetails'], true);

if($requestType=='saveData')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	
	$advPayNo  	 	 = $arrHeader['advPayNo'];
	$advPayYear  	 = $arrHeader['advPayYear'];
	$payDate  	 	 = $arrHeader['payDate'];
	$supplierId  	 = $arrHeader['supplierId'];
	//$ledgerAccId 	 = ($_REQUEST['ledgerAccId']==''?'null':$_REQUEST['ledgerAccId']);
	$currency  		 = ($arrHeader['currency']==''?'null':$arrHeader['currency']);
	$payMethod  	 = ($arrHeader['payMethod']==''?'null':$arrHeader['payMethod']);
	$payAmount  	 = round($arrHeader['payAmount'],2);
	$Remarks  		 = $obj_common->replace($arrHeader['Remarks']);
	$bankRef  		 = $arrHeader['bankRef'];
	$POArr			 = explode('/',$arrHeader['PONoArr']);
	$PONo			 = $POArr[0];
	$POYear		 	 = $POArr[1];
	$rollBackFlag 	 = 0;
	$saveMode		 = loadSaveMode($programCode,$userId);
	
	$exchngRateArr	 = $obj_exchgRate_get->GetAllValues($currency,2,$payDate,$companyId,'RunQuery2');
	if($exchngRateArr['AVERAGE_RATE']=='' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged	= 'Please enter exchange rates for '.$payDate.' before save.';
	}
	if($saveMode==1)
	{
		if($advPayNo=='' && $advPayYear=='')
		{
			$balQty = getValidateQty($PONo,$POYear);
			if(($payAmount>$balQty) && $savedStatus)
			{
				$savedStatus		= false;
				$savedMasseged 		= 'Advance amount exceed PO Amount.';
			}
			
			$data['arrData'] = $obj_common->GetSystemMaxNo('intAdvancePaymentNo',$locationId);
			if($data['arrData']['rollBackFlag']==1 && $savedStatus)
			{
				$savedStatus		= false;
				$savedMasseged		= $data['arrData']['msg'];
				$error_sql			= $data['arrData']['q'];
			}
			
			$advPayNo		= $data['arrData']['max_no'];
			$advPayYear		= date('Y');
				
			$sql = "INSERT INTO finance_supplier_advancepayment_header 
					(
					ADVANCE_PAYMENT_NO, ADVANCE_PAYMENT_YEAR, SUPPLIER_ID, 
					PO_NO, PO_YEAR, PAY_DATE, 
					COMPANY_ID, CURRENCY_ID, 
					PAY_AMOUNT, PAY_METHOD, REMARKS, BANK_REFERENCE_NO, 
					STATUS, CREATED_BY, CREATED_DATE
					)
					VALUES
					(
					'$advPayNo', '$advPayYear', '$supplierId', 
					'$PONo', '$POYear', '$payDate', 
					'$companyId', $currency, 
					'$payAmount', $payMethod, '$Remarks', '$bankRef', 
					'1', '$userId', now()
					);";
			$result = $db->RunQuery2($sql);
			if(!$result && $savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged 	= $db->errormsg;
				$error_sql		= $sql;
			}
			
			$supplierLedgerId = getSupplierLedger($supplierId);		
			if($supplierLedgerId=='' && $savedStatus)
			{
				$savedStatus		= false;
				$savedMasseged		= 'No GL account allocate for the selected Supplier.';
			}
			
			$data['finalResult'] = insertLedger('D',$supplierLedgerId,$payAmount,$advPayNo,$advPayYear,$currency,$bankRef,$supplierId,'',$payDate);
			if($data['finalResult']['rollBackFlag']==1 && $savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $data['finalResult']['msg'];
				$error_sql		= $data['finalResult']['q'];
			}
					
			foreach($arr as $arrVal)
			{
				$accId 	 		= $arrVal['accId'];
				$amount 		= $arrVal['amount'];
				$memo 			= $obj_common->replace($arrVal['memo']);
				$costCenter 	= $arrVal['costCenter'];
				
				$responsePV		 = $finance_common_get->getPaymentVoucherNo($payDate,$accId,$costCenter,'RunQuery2');
				if($responsePV['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $responsePV['msg'];
				}
				
				$voucherNo = $responsePV['voucherNo'];
				
				$sql = "UPDATE finance_supplier_advancepayment_header 
						SET
						VOUCHER_NO = '$voucherNo'
						WHERE
						ADVANCE_PAYMENT_NO = '$advPayNo' AND 
						ADVANCE_PAYMENT_YEAR = '$advPayYear' ";
				
				$result = $db->RunQuery2($sql);
				if(!$result && $savedStatus)
				{	
					$savedStatus	= false;
					$savedMasseged	= $db->errormsg;
					$error_sql		= $sql;
				}
			
				$sql = "INSERT INTO finance_supplier_advancepayment_details 
						(
						ADVANCE_PAYMENT_NO, 
						ADVANCE_PAYMENT_YEAR, 
						ACCOUNT_ID, 
						AMOUNT, 
						MEMO, 
						COST_CENTER
						)
						VALUES
						(
						'$advPayNo', 
						'$advPayYear', 
						'$accId', 
						'$amount', 
						'$memo', 
						'$costCenter'
						)";
				$result = $db->RunQuery2($sql);
				if(!$result && $savedStatus)
				{	
					$savedStatus	= false;
					$savedMasseged	= $db->errormsg;
					$error_sql		= $sql;
				}
				
				$advnceAmount = ($amount*-1);
				$sql = "INSERT INTO finance_supplier_transaction 
						(
						PO_YEAR, PO_NO, SUPPLIER_ID, 
						CURRENCY_ID, DOCUMENT_YEAR, DOCUMENT_NO, 
						DOCUMENT_TYPE, LEDGER_ID, VALUE, 
						COMPANY_ID, LOCATION_ID, USER_ID, TRANSACTION_DATE_TIME
						)
						VALUES
						( 
						'$POYear', '$PONo', '$supplierId', 
						$currency, '$advPayYear', '$advPayNo', 
						'ADVANCE',$accId, '$advnceAmount', 
						'$companyId', '$locationId', '$userId', '$payDate'
						);";
				$result = $db->RunQuery2($sql);
				if(!$result && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $db->errormsg;
					$error_sql		= $sql;
				}
				//BEGIN - BANK EXCHANGE REATE CONVERSIONS {
				$bankArray		= $finance_common_get->GetBankExRate($accId,'RunQuery2');
				if($currency != $bankArray["CURRENCY_ID"] && $accId != '395')
				{
					$rateTarget		= $obj_exchgRate_get->GetAllBankValues($bankArray["CURRENCY_ID"],$bankArray["BANK_ID"],2,$payDate,$companyId,'RunQuery2');
					$rateSource		= $obj_exchgRate_get->GetAllBankValues($currency,$bankArray["BANK_ID"],2,$payDate,$companyId,'RunQuery2');
					$currency		= $bankArray["CURRENCY_ID"];
					$amount			= round((($amount/$rateTarget["AVERAGE_RATE"])*$rateSource["AVERAGE_RATE"]),2);
					//die($totPayAmount.'/'.$rateTarget["AVERAGE_RATE"].'*'.$rateSource["AVERAGE_RATE"]);
					if($amount==0 && $savedStatus)
					{
						$savedStatus	= false;
						$savedMasseged	= "Unable to save.Bank exchange rate not available in the system.";
					}
				}
				else
				{
					$currency		= $currency;
				}
				//END 	- BANK EXCHANGE REATE CONVERSIONS }
				$data['finalResult'] = insertLedger('C',$accId,$amount,$advPayNo,$advPayYear,$currency,$bankRef,$supplierId,$memo,$payDate);
				if($data['finalResult']['rollBackFlag']==1 && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $data['finalResult']['msg'];
					$error_sql		= $data['finalResult']['q'];	
				}	
			}						
			//BEGIN - CASH FLOW FORECAST UPDATION { 
			$result = $finance_common_get->UpdateSupplierForcast($payAmount,$supplierId,$currency,$payDate);				
			if(!$result['type'])
			{
				$savedStatus	= false;
				$savedMasseged	= $result['msg'];
				$error_sql		= $result['sql'];
			}
			//END 	- CASH FLOW FORECAST UPDATION }	
		}
		else
		{
			$savedStatus	= false;
			$savedMasseged	= 'You can not update Advance Payment.';
		}
	}
	else
	{
		$savedStatus	= false;
		$savedMasseged	= 'You do not have permission to Save Advance Payment.';
	}
	
	$resultArr	= $obj_common->validateDuplicateSerialNoWithSysNo($advPayNo,'intAdvancePaymentNo',$locationId);
	if($resultArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged	= $resultArr['msg'];
	}
	if($savedStatus)
	{
		$db->RunQuery2('Commit');
		$response['type'] 			= 'pass';
		$response['msg'] 			= 'Saved successfully.';
		$response['advancePayNo']	= $advPayNo;
		$response['advancePayYear']	= $advPayYear;
	}
	else
	{
		$db->RunQuery2('Rollback');
		$response['type'] 			= 'fail';
		$response['msg'] 			= $savedMasseged;
		$response['q'] 				= $error_sql;
	}
	$db->CloseConnection();
	echo json_encode($response);
}
else if($requestType=='cancelAdvancePayment')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
		
	$advPaymentNo  	 = $_REQUEST['advPaymentNo'];
	$advPaymentYear  = $_REQUEST['advPaymentYear'];
	$rollBackFlag	 = 0;
	
	$cancleMode 	 = loadCancleMode($programCode,$userId);
	
	if($cancleMode==1)
	{
		$data['arrData'] = CancleValidation($advPaymentNo,$advPaymentYear);
		if($data['arrData']['rollBackFlag']==1)
		{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $data['arrData']['rollBackMsg'];
			
			$db->CloseConnection();
			echo json_encode($response);
			return;
		}
		
		$sqlUpdH = "UPDATE finance_supplier_advancepayment_header 
					SET
					STATUS = '-2',
					LAST_MODIFY_BY='$userId',
					LAST_MODIFY_DATE= NOW()		
					WHERE
					ADVANCE_PAYMENT_NO = '$advPaymentNo' AND 
					ADVANCE_PAYMENT_YEAR = '$advPaymentYear' ";
		$resultUpdH = $db->RunQuery2($sqlUpdH);
		if(!$resultUpdH)
		{
			$rollBackFlag	= 1;
			$rollBackMsg	= $db->errormsg;
		}
		else
		{
			$sql = "DELETE FROM finance_supplier_transaction 
					WHERE DOCUMENT_NO='$advPaymentNo' AND 
					DOCUMENT_YEAR='$advPaymentYear' AND 
					DOCUMENT_TYPE='ADVANCE' AND
					COMPANY_ID='$companyId' ";
			$result = $db->RunQuery2($sql);
			
			if(!$result)
			{
				$rollBackFlag	= 1;
				$rollBackMsg	= $db->errormsg;
			}
			
			$sql = "DELETE FROM finance_transaction 
					WHERE DOCUMENT_NO ='$advPaymentNo' AND
					DOCUMENT_YEAR='$advPaymentYear' AND
					DOCUMENT_TYPE='ADVANCE' AND
					TRANSACTION_CATEGORY = 'SU' AND
					COMPANY_ID='$companyId' ";
			$result = $db->RunQuery2($sql);
			
			if(!$result)
			{
				$rollBackFlag	= 1;
				$rollBackMsg	= $db->errormsg;
			}	
		}
	}
	else
	{
		$rollBackFlag	 = 1;
		$rollBackMsg	 = "You do not have permission to Cancle Advance Payment.";
	}
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
		$response['q'] 			= $q;
	}
	else if($rollBackFlag==0)
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Cancelled successfully.';
	}
	else
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
		$response['q'] 			= $q;
	}
	$db->CloseConnection();
	echo json_encode($response);
}
function loadSaveMode($programCode,$userId)
{
	global $db;
	$Mode	= 0;
	$sql = "SELECT
			menupermision.intAdd 
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId = '$userId' ";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	if($row['intAdd']==1)
	{
		$Mode = 1;
	}
	return $Mode;
}
function loadCancleMode($programCode,$userId)
{
	global $db;
	$Mode	= 0;
	$sql = "SELECT
			menupermision.intCancel 
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId = '$userId' ";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	if($row['intCancel']==1)
	{
		$Mode = 1;
	}
	return $Mode;
}
function getValidateQty($PONo,$POYear)
{
	global $db;
	$sql = "SELECT SUM(ROUND(((dblQty*dblUnitPrice)*((100-dblDiscount)/100)+dblTaxAmmount),2)) AS POAmount,
			(SELECT IFNULL((SUM(VALUE)*-1),0) FROM finance_supplier_transaction
			WHERE PO_NO=POD.intPONo AND
			PO_YEAR=POD.intPOYear AND
			DOCUMENT_TYPE!='INVOICE') recievedAmonut
			FROM trn_podetails POD
			WHERE POD.intPONo='$PONo' AND 
			POD.intPOYear='$POYear' ";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	$balanceAmt = $row['POAmount']-$row['recievedAmonut'];
	
	return $balanceAmt;
}
function CancleValidation($advPaymentNo,$advPaymentYear)
{
	global $db;
	
	$data['rollBackFlag'] 	= 0 ;
	
	$sql = "SELECT STATUS
			FROM finance_supplier_advancepayment_header
			WHERE ADVANCE_PAYMENT_NO='$advPaymentNo' AND
			ADVANCE_PAYMENT_YEAR='$advPaymentYear' ";
	
	$result = $db->RunQuery2($sql);
	$row 	= mysqli_fetch_array($result);
	
	if($row['STATUS']=='-2')
	{
		$data['rollBackFlag'] 	= 1 ;
		$data['rollBackMsg'] 	= 'Already Cancelled.';
	}
	
		$sql1 = "SELECT IFNULL((SUM(ST.VALUE)*-1),0) totQty
				FROM finance_supplier_transaction ST
				WHERE ST.PURCHASE_INVOICE_NO IS NOT NULL AND
				ST.PURCHASE_INVOICE_YEAR IS NOT NULL AND
				ST.DOCUMENT_NO='$advPaymentNo' AND
				ST.DOCUMENT_YEAR='$advPaymentYear' AND
				ST.DOCUMENT_TYPE='ADVANCE' ";
			
	$result1 = $db->RunQuery2($sql1);
	$row1 	 = mysqli_fetch_array($result1);
	
	if($row1['totQty']>0)
	{	
		$data['rollBackFlag'] 	= 1 ;
		$data['rollBackMsg'] 	= 'Cannot cancel this Advance.Some payment or settlement raised.';	
	}
	
	return $data;
}
function insertLedger($transactionType,$GLId,$amount,$advPayNo,$advPayYear,$currency,$bankRef,$supplierId,$remarks,$payDate)
{
	global $db;
	global $locationId;
	global $companyId;
	global $userId;
	
	$sql = "INSERT INTO finance_transaction 
			(
			CHART_OF_ACCOUNT_ID, 
			AMOUNT, DOCUMENT_NO, DOCUMENT_YEAR, 
			DOCUMENT_TYPE, TRANSACTION_TYPE, TRANSACTION_CATEGORY, 
			TRANSACTION_CATEGORY_ID, CURRENCY_ID, BANK_REFERENCE_NO, REMARKS,
			LOCATION_ID, COMPANY_ID, 
			LAST_MODIFIED_BY,
			LAST_MODIFIED_DATE
			)
			VALUES
			(
			'$GLId', 
			'$amount', '$advPayNo', '$advPayYear', 
			'ADVANCE', '$transactionType', 'SU', 
			'$supplierId', '$currency', '$bankRef', '$remarks',
			'$locationId', '$companyId', 
			'$userId',
			'$payDate'
			)";
	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$data['rollBackFlag'] 	= 1 ;
		$data['msg'] 			= $db->errormsg;
		$data['q'] 				= $sql ;
	}
	else
	{
		$data['rollBackFlag'] 	= 0 ;
	}
	return $data;
}
function getSupplierLedger($supplierId)
{
	global $db;
	
	$sql = "SELECT CHART_OF_ACCOUNT_ID FROM finance_mst_chartofaccount
			WHERE CATEGORY_TYPE='S' AND
			CATEGORY_ID='$supplierId' AND
			STATUS='1' ";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);

	return $row['CHART_OF_ACCOUNT_ID'];
}
?>
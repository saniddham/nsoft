<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once "class/finance/cls_convert_amount_to_word.php";
require_once "class/finance/cls_common_get.php";

$obj_fin_com		= new Cls_Common_Get($db); 
$obj_AmtName	   	= new Cls_Convert_Amount_To_Word($db);

$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];

$advancePayNo 		= $_REQUEST['advancePayNo'];
$advancePayYear 	= $_REQUEST['advancePayYear'];

$header_array 		= GetHeaderDetails($advancePayNo,$advancePayYear,$companyId);
$detail_result 		= GetGridDetails($advancePayNo,$advancePayYear);

$company_Id			= $header_array["COMPANY_ID"];
?>
<head>

<title>Supplier Advance Paymnet</title>

<style>
#apDiv1 {
	position: absolute;
	left: 428px;
	top: 116px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
</style>

</head>
<body>
<?php
if($header_array['STATUS']==-2)
{
?>
	<div id="apDiv1"><img src="images/cancelled.png" style="opacity:0.3" /></div>
<?php
}
?>
<form id="frmSalesInvoice" name="frmSalesInvoice" method="post">
    <table width="900" align="center">
      <tr>
        <td width="888"><?php include 'reportHeader.php'?></td>
      </tr>
      <tr>
        <td style="text-align:center">&nbsp;</td>
      </tr>
      <tr>
        <td style="text-align:center"><strong>SUPPLIER ADVANCE PAYMENT REPORT</strong></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>Advance Payment No</td>
            <td>:</td>
            <td><?php echo $obj_fin_com->getSerialNo($header_array["SERIAL_NO"],$header_array["SERIAL_DATE"],$company_Id,'RunQuery')//echo $header_array["ADVANCE_NO"]; ?></td>
            <td>PO No</td>
            <td>:</td>
            <td><?php echo $header_array["PONo"]?></td>
          </tr>
          <tr>
            <td>Supplier</td>
            <td>:</td>
            <td><?php echo $header_array["supplier"]; ?></td>
            <td>Payment Date</td>
            <td>:</td>
            <td><?php echo $header_array["PAY_DATE"]; ?></td>
          </tr>
          <tr>
            <td width="15%">Amount</td>
            <td width="1%">:</td>
            <td width="43%"><?php echo number_format($header_array["PAY_AMOUNT"],2); ?></td>
            <td width="15%">Currency</td>
            <td width="1%">:</td>
            <td width="25%"><?php echo $header_array["currency"]; ?></td>
          </tr>
          <tr>
            <td>Remarks</td>
            <td>:</td>
            <td rowspan="3" valign="top"><?php echo $header_array["REMARKS"]; ?></td>
            <td>Payment Method</td>
            <td>:</td>
            <td><?php echo $header_array["paymentMethod"]; ?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Bank Reference No</td>
            <td>:</td>
            <td><?php echo $header_array["BANK_REFERENCE_NO"]; ?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td ><table width="100%" border="0" class="rptBordered" id="tblMain">
        <thead>
          <tr>
            <th width="4%">&nbsp;</th>
            <th width="30%">Account</th>
            <th width="41%">Memo</th>
            <th width="13%">Cost Center</th>
            <th width="12%">Amount</th>
            </tr>
        </thead>        
        <tbody>
<?php
$totAmount 		= 0;
$i				= 0;

while($row = mysqli_fetch_array($detail_result))
{ 
	$totAmount+=$row['AMOUNT'];
?>
		<tr>
        	<td class="cls_td_check" align="center"><?php echo ++$i;?>.</td>
        	<td class="cls_td_salesOrderNo"><?php echo $row["account"]?></td>
       	 	<td width="41%"><?php echo $row["MEMO"]?></td>
        	<td width="13%"><?php echo $row["costCenter"]?></td>
        	<td width="12%" style="text-align:right"><?php echo number_format($row["AMOUNT"],2);?></td>
        </tr>
<?php
}
?>
		<tr>
        	<td colspan="4" align="right"><b>Total :</b></td>
        	<td width="12%" style="text-align:right"><b><?php echo number_format($totAmount,2);?></b></td>
        </tr>
        </tbody>
        </table></td>
      </tr>
      <tr>
        <td class="normalfnt"><b><?php echo $obj_AmtName->Convert_Amount($header_array["PAY_AMOUNT"],$header_array["currency"]); ?></b></td>
      </tr>
      <tr>
        <td class="normalfnt">&nbsp;</td>
      </tr>
      </table>
</form>
</body>
<?php
function GetHeaderDetails($advancePayNo,$advancePayYear,$companyId)
{
	global $db;
	
	$sql = "SELECT 
			SAPH.ADVANCE_PAYMENT_NO		AS SERIAL_NO,
			SAPH.COMPANY_ID				AS COMPANY_ID,
			SAPH.PAY_DATE				AS SERIAL_DATE,
			MS.strName AS supplier,
			SAPH.PAY_DATE,
			SAPH.PAY_AMOUNT,
			SAPH.BANK_REFERENCE_NO,
			SAPH.BANK_REFERENCE_NO,
			CONCAT(SAPH.PO_NO,' / ',SAPH.PO_YEAR) AS PONo,
			FC.strDescription AS currency,
			FPM.strName AS paymentMethod,
			SAPH.REMARKS,
			SAPH.STATUS
			FROM finance_supplier_advancepayment_header SAPH
			INNER JOIN mst_supplier MS ON MS.intId=SAPH.SUPPLIER_ID
			LEFT JOIN mst_financecurrency FC ON FC.intId=SAPH.CURRENCY_ID
			INNER JOIN mst_financepaymentsmethods FPM ON FPM.intId=SAPH.PAY_METHOD
			WHERE SAPH.COMPANY_ID='$companyId' AND
			SAPH.ADVANCE_PAYMENT_NO='$advancePayNo' AND
			SAPH.ADVANCE_PAYMENT_YEAR='$advancePayYear' ";
				
	$result = $db->RunQuery($sql);
	$header_array = mysqli_fetch_array($result);
	return $header_array;
}

function GetGridDetails($advancePayNo,$advancePayYear)
{
	global $db;
	
	$sql = "SELECT FSAD.AMOUNT,
			FSAD.MEMO,
			(SELECT distinct CONCAT(CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE),' - ',
			FCOA.CHART_OF_ACCOUNT_NAME)
			FROM finance_mst_chartofaccount FCOA
			INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
			INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
			WHERE FCOA.CHART_OF_ACCOUNT_ID=FSAD.ACCOUNT_ID) AS account,
			FD.strName AS costCenter
			FROM finance_supplier_advancepayment_details FSAD
			INNER JOIN mst_financedimension FD ON FD.intId=FSAD.COST_CENTER
			WHERE FSAD.ADVANCE_PAYMENT_NO='$advancePayNo' AND
			FSAD.ADVANCE_PAYMENT_YEAR='$advancePayYear' ";
	return $db->RunQuery($sql);
}

function test($number)
{
	$currencyTitle	= 'USD';
	$currencyFraction	= "CENT";
	$value1 = Convert_number($number);
	$convrt = explode(".",round($number,2));
	$cents =  $convrt[1];
	if(strlen($cents)<=1)
		$cents = $convrt[1] . "0";
	$value2	= ConvertDecimals($cents);
	
	return $value1." $currencyTitle and ".$value2 ." $currencyFraction only.";
}

function Convert_number($number) 
{ 
    if (($number < 0) || ($number > 999999999)) 
    { 
        return "$number"; 
    } 

    $Gn = floor($number / 1000000);  /* Millions (giga) */ 
    $number -= $Gn * 1000000; 
    $kn = floor($number / 1000);     /* Thousands (kilo) */ 
    $number -= $kn * 1000; 
    $Hn = floor($number / 100);      /* Hundreds (hecto) */ 
    $number -= $Hn * 100; 
    $Dn = floor($number / 10);       /* Tens (deca) */ 
    $n = $number % 10;               /* Ones */ 
    $res = ""; 

    if ($Gn) 
    { 
        $res .= convert_number($Gn) . " Million"; 
    } 

    if ($kn) 
    { 
        $res .= (empty($res) ? "" : " ") . 
            convert_number($kn) . " Thousand"; 
    } 

    if ($Hn) 
    { 
        $res .= (empty($res) ? "" : " ") . 
            convert_number($Hn) . " Hundred"; 
    } 

    $ones = array("", "One", "Two", "Three", "Four", "Five", "Six", 
        "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", 
        "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", 
        "Nineteen"); 
    $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", 
        "Seventy", "Eighty", "Ninety"); 

    if ($Dn || $n) 
    { 
        if (!empty($res)) 
        { 
            $res .= " and "; 
        } 

        if ($Dn < 2) 
        { 
            $res .= $ones[$Dn * 10 + $n]; 
        } 
        else 
        { 
            $res .= $tens[$Dn]; 

            if ($n) 
            { 
                $res .= "-" . $ones[$n]; 
            } 
        } 
    } 

    if (empty($res)) 
    { 
        $res = "zero"; 
    } 

    return $res;	
} 

function ConvertDecimals($number)
{		
      $Dn = floor($number / 10);       /* -10 (deci) */ 
      $n = $number % 10;               /* .0 */ 
	  
	   $ones = array("", "One", "Two", "Three", "Four", "Five", "Six", 
        "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", 
        "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", 
        "Nineteen"); 
    $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", 
        "Seventy", "Eighty", "Ninety"); 
		
 if ($Dn || $n) 
    { 
        if (!empty($res)) 
        { 
            $res .= " and "; 
        } 

        if ($Dn < 2) 
        { 
            $res .= $ones[$Dn * 10 + $n]; 
        } 
        else 
        { 
            $res .= $tens[$Dn]; 

            if ($n) 
            { 
                $res .= "-" . $ones[$n]; 
            } 
        } 
    } 
	
	if (empty($res)) 
    { 
        $res = "zero"; 
    } 

    return $res; 
	
}
?>
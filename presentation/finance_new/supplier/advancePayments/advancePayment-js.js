// JavaScript Document
var costCenterAL 	= '';
var basePath	 	= "presentation/finance_new/supplier/advancePayments/";
var voucherRptPath	= "presentation/finance_new/reports/";
var poReportId		= 934;
var menuId			= 683;
$(document).ready(function(){
	
	$("#frmSupAdvancePayment").validationEngine();
	
	/*if(intAddx)
	{
		$('#frmAdvancePayment #butNew').show();
		if(status==-2)
			$('#frmAdvancePayment #butSave').hide();
		else
			$('#frmAdvancePayment #butSave').show();
	}
	//permision for edit 
	if(intEditx)
	{
		if(status==-2)
			$('#frmAdvancePayment #butSave').hide();
		else
			$('#frmAdvancePayment #butSave').show();
	}
	
	//permision for delete
	if(intDeletex)
	{
		$('#frmAdvancePayment #butDelete').show();
	}
	
	//permision for view
	if(intViewx)
	{
		$('#frmAdvancePayment #cboSearch').removeAttr('disabled');
	}*/
	
	$('#frmSupAdvancePayment #cboSupplier').die('change').live('change',loadData);
	$('#frmSupAdvancePayment #cboPONo').die('change').live('change',loadPOData);
	$('#frmSupAdvancePayment #txtPayAmount').die('keyup').live('keyup',setPOAmount);
	$('#frmSupAdvancePayment #butInsertRow').die('click').live('click',addNewRow);	
	$('#frmSupAdvancePayment #txtPayAmount').die('blur').live('blur',setPayAmount);
	$('#frmSupAdvancePayment .clsDel').die('click').live('click',deleteRow);
	$('#frmSupAdvancePayment #butSave').die('click').live('click',saveData);	
	$('#frmSupAdvancePayment .clsAmount').die('keyup').live('keyup',setAmount);
	$('#frmSupAdvancePayment .clsAmount').die('focus').live('focus',chkAmount);
	$('#frmSupAdvancePayment #txtPayAmount').die('keyup').live('keyup',setAllAmount);
	$('#frmSupAdvancePayment #butNew').die('click').live('click',reloadPage);
	$('#frmSupAdvancePayment #butCancle').die('click').live('click',cancelAdvPayment);
	$('#frmSupAdvancePayment #butVoucher').die('click').live('click',viewVoucherReport);
	$('#frmSupAdvancePayment #txtDate').die('blur').live('blur',loadExchangeRate);
	$('#frmSupAdvancePayment #tblMain .clsAccounts').die('change').live('change',GetExRate);
	
	if(supplierIdAL!='')
		LoadAutoData(supplierIdAL,PONoAL)
});

function loadData()
{
	if($('#frmSupAdvancePayment #cboSupplier').val()=='')
	{
		clearData('Supplier');
		return;
	}
	$('#frmSupAdvancePayment #txtPOAmount').val(0);
	$('#frmSupAdvancePayment #txtAdvancedAmunt').val(0);
	$('#frmSupAdvancePayment .clsViewPO').removeAttr('href');
	
	var url = basePath+"advancePayment-db-get.php?requestType=loadData";
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',  
		data:"&supplierId="+$('#frmSupAdvancePayment #cboSupplier').val()+"&date="+$('#frmSupAdvancePayment #txtDate').val(),
		async:false,
		success:function(json){
			if(json.type=='pass')
			{
				$('#frmSupAdvancePayment #cboPONo').html(json.POCombo);
				$('#frmSupAdvancePayment #cboPONo').val(json.PONo);
				$('#frmSupAdvancePayment #txtCurrencyRate').val(json.exchgRate);
				$('#frmSupAdvancePayment #cboCurrency').val(json.currency);
				$('#frmSupAdvancePayment #cboPaymentMethod').val(json.paymentMethod);
				GetExRate();
				
				if(json.PONo!='')
				{
					loadPOData();
				}
			}			
		},
		error:function(xhr,status){
				
			}		
	});
}
function loadPOData()
{
	if($('#frmSupAdvancePayment #cboPONo').val()=='')
	{
		clearData('PO');
		return;
	}
	$('#frmSupAdvancePayment #txtPayAmount').val('');
	var url = basePath+"advancePayment-db-get.php?requestType=loadPOData";
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',  
		data:"&PONo="+$('#frmSupAdvancePayment #cboPONo').val()+'&supplier='+$('#frmSupAdvancePayment #cboSupplier').val()+"&date="+$('#frmSupAdvancePayment #txtDate').val(),
		async:false,
		success:function(json){
			if(json.type=='pass')
			{
				$('#frmSupAdvancePayment #txtCurrencyRate').val(json.exchgRate);
				$('#frmSupAdvancePayment #cboCurrency').val(json.currency);
				$('#frmSupAdvancePayment #cboPaymentMethod').val(json.paymentMethod);
				$('#frmSupAdvancePayment #txtAdvancedAmunt').val(RoundNumber(json.balAmount,2));
				$('#frmSupAdvancePayment #txtBalAmunt').val(RoundNumber(json.balAmount,2));
				$('#frmSupAdvancePayment #txtPOAmount').val(RoundNumber(json.POAmount,2));
				$('#frmSupAdvancePayment #cboCostCenter').val(json.costCenter);
				costCenterAL = json.costCenter;
				//GetExRate();
				$('.clsViewPO').attr('href','?q='+poReportId+'&poNo='+json.PONo+'&year='+json.POYear+'');
			}			
		},
		error:function(xhr,status){
				
			}		
	});
}
function clearData(type)
{
	//$('#cboLedgerAccount').val('');
	$('#frmSupAdvancePayment #cboCurrency').val('');
	$('#frmSupAdvancePayment #txtPOAmount').val(0);
	$('#frmSupAdvancePayment #txtAdvancedAmunt').val(0);
	$('#frmSupAdvancePayment .clsViewPO').removeAttr('href');
	
	if(type=='Supplier')
	{
		$('#frmSupAdvancePayment #cboPONo').html('');
	}
}
function addNewRow()
{
	if($('#frmSupAdvancePayment').validationEngine('validate'))
	{
		$('#frmSupAdvancePayment #tblMain tbody tr:last').after("<tr>"+$('#frmSupAdvancePayment #tblMain .cls_tr_firstRow').html()+"</tr>");
		$('#frmSupAdvancePayment #tblMain tbody tr:last').find('.clsAccounts').val('');
		$('#frmSupAdvancePayment #tblMain tbody tr:last').find('.clsAmount').val('');
		$('#frmSupAdvancePayment #tblMain tbody tr:last').find('.clsMemo').val('');
		$('#frmSupAdvancePayment #tblMain tbody tr:last').find('.clsCostCener').val(costCenterAL);
	}
}
function deleteRow()
{
	var delRowCount = parseInt(document.getElementById('tblMain').rows.length);
	
	if(delRowCount>3)
		$(this).parent().parent().remove();
	
	if(parseInt(document.getElementById('tblMain').rows.length)==3)
		$('#frmSupAdvancePayment #tblMain tbody tr:last').addClass('cls_tr_firstRow');
}
function saveData()
{
	if(!IsProcessMonthLocked_date($('#txtDate').val()))
		return;
	
	if($('#frmSupAdvancePayment #txtCurrencyRate').val()=='0' && $('#frmSupAdvancePayment #tblMain .clsAmount').val()=='395')
	{
		alert('Please enter bank exchange rate.');
		return;
	}
	showWaiting();
	var advPayNo 		= $('#frmSupAdvancePayment #txtAdvPayNo').val();
	var advPayYear 		= $('#frmSupAdvancePayment #txtAdvPayYear').val();
	var payDate 		= $('#frmSupAdvancePayment #txtDate').val();
	var supplierId 		= $('#frmSupAdvancePayment #cboSupplier').val();
	var PONoArr			= $('#frmSupAdvancePayment #cboPONo').val();
	//var ledgerAccId 	= $('#cboLedgerAccount').val();
	var currency 		= $('#frmSupAdvancePayment #cboCurrency').val();
	var payAmount 		= $('#frmSupAdvancePayment #txtPayAmount').val();
	var payMethod 		= $('#frmSupAdvancePayment #cboPaymentMethod').val();
	var Remarks 		= $('#frmSupAdvancePayment #txtRemarks').val();
	var bankRef         = $('#frmSupAdvancePayment #txtBankRefNo').val();
	
	if($('#frmSupAdvancePayment').validationEngine('validate'))
	{
		var payAmount 		= parseFloat($('#frmSupAdvancePayment #txtPayAmount').val());
		var totalAmount 	= parseFloat($('#frmSupAdvancePayment #txtTotalAmount').val());
		
		if(payAmount!=totalAmount)
		{
			$('#frmSupAdvancePayment #txtTotalAmount').validationEngine('showPrompt','Total Amount must equal to Pay Amonut.','fail');
			hideWaiting();
			return;
		}
		if(advPayNo!='' && advPayYear!='')
		{
			$('#frmSupAdvancePayment #butSave').validationEngine('showPrompt','You can not update this Advance Payment.','fail');
			hideWaiting();
			return;
		}
		var arrHeader = "{";
							arrHeader += '"advPayNo":"'+advPayNo+'",' ;
							arrHeader += '"advPayYear":"'+advPayYear+'",' ;
							arrHeader += '"payDate":"'+payDate+'",' ;
							arrHeader += '"supplierId":"'+supplierId+'",' ;
							arrHeader += '"PONoArr":"'+PONoArr+'",' ;
							arrHeader += '"currency":"'+currency+'",' ;
							arrHeader += '"payAmount":"'+payAmount+'",' ;
							arrHeader += '"payMethod":"'+payMethod+'",' ;
							arrHeader += '"Remarks":'+URLEncode_json(Remarks)+',';
							arrHeader += '"bankRef":"'+bankRef+'"' ;

			arrHeader += "}";
			
		value = "[ ";
		
		$('#frmSupAdvancePayment #tblMain .clsAmount').each(function(){
			
			var accId 		= $(this).parent().parent().find('.clsAccounts').val();
			var amount 		= $(this).val();
			var memo 		= $(this).parent().parent().find('.clsMemo').val();
			var costCenter 	= $(this).parent().parent().find('.clsCostCener').val();
				
			value += "{";
			value += '"accId":"'+ accId +'",' ;
			value += '"amount":"'+ amount +'",' ;
			value += '"memo":'+URLEncode_json(memo)+',';
			value += '"costCenter":"'+costCenter+'"' ;
			value += "},";	
		});
			
		value = value.substr(0,value.length-1);
		value += " ]";
		
		var url = basePath+"advancePayment-db-set.php?requestType=saveData";
			var obj = $.ajax({
				url:url,
				dataType: "json",
				type:'POST',  
				data:'&arrHeader='+arrHeader+'&accDetails='+value,
				async:false,
				success:function(json){
						$('#frmSupAdvancePayment #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							var t = setTimeout("alertx3()",3000);
							$('#frmSupAdvancePayment #txtAdvPayNo').val(json.advancePayNo);
							$('#frmSupAdvancePayment #txtAdvPayYear').val(json.advancePayYear);
							$('#frmSupAdvancePayment #butSave').hide();
							$('#frmSupAdvancePayment #butVoucher').show();
							hideWaiting();
							return;
						}
						else
						{
							hideWaiting();
						}
					},
				error:function(xhr,status){
						
						$('#frmSupAdvancePayment #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
					}		
				});
			
	}
	else
	{
		hideWaiting();
	}
		
}
function setAmount()
{
	var obj			 = this;
	var Amount 	 	 = parseFloat($('#frmSupAdvancePayment #txtPayAmount').val());
	var totAmount	 = 0;
	$('#frmSupAdvancePayment .clsAmount').each(function(){

		if($(this).val()!="")
			totAmount += parseFloat($(this).val());
	});
	if(totAmount>Amount)
	{
		obj.value = parseFloat(obj.value)-parseFloat(parseFloat(totAmount)-parseFloat(Amount));
		totAmount = Amount;
	}
	$('#frmSupAdvancePayment #txtTotalAmount').val(RoundNumber(totAmount,2));
}
function setPOAmount()
{
	var balAmount = parseFloat($('#txtBalAmunt').val());
	if(parseFloat($(this).val())>balAmount)
		$(this).val(balAmount);
}
function chkAmount()
{
	if($('#frmSupAdvancePayment #txtPayAmount').val()=='')
	{
		$('#frmSupAdvancePayment #txtPayAmount').validationEngine('showPrompt','Please Enter a amount.','fail');
		$('#frmSupAdvancePayment #txtPayAmount').focus();
		return;
	}
}
function setAllAmount()
{
	$('#frmSupAdvancePayment .clsAmount').val('');
}
function cancelAdvPayment()
{
	var advPaymentNo 	= $('#frmSupAdvancePayment #txtAdvPayNo').val();
	var advPaymentYear 	= $('#frmSupAdvancePayment #txtAdvPayYear').val();
	
	if(advPaymentNo=='')
	{
		$('#frmSupAdvancePayment #butCancle').validationEngine('showPrompt','No advance payment No to Cancel.','fail');
		return;
	}
	var val = $.prompt('Are you sure you want to Cancle "'+advPaymentNo+'/'+advPaymentYear+'" ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
					showWaiting();
					var url = basePath+"advancePayment-db-set.php?requestType=cancelAdvancePayment&advPaymentNo="+advPaymentNo+"&advPaymentYear="+advPaymentYear;
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: 'json',  
						data:'',
						async:false,
						success:function(json){
								$('#frmSupAdvancePayment #butCancle').validationEngine('showPrompt', json.msg,json.type );
								if(json.type=='pass')
								{
									hideWaiting();
									var t=setTimeout("alertx4()",2000);
									var t=setTimeout("window.location.reload();",2000);
									return;
								}
								else
								{
									hideWaiting();
								}
							},
						error:function(xhr,status){
								
								$('#frmSupAdvancePayment #butCancle').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								hideWaiting();
								return;
							}		
						});
					
					}
				
			}});
}
function loadExchangeRate()
{
	if($('#frmSupAdvancePayment #cboCurrency').val()=='')
	{
		$('#frmSupAdvancePayment #txtCurrencyRate').val('0.0000');
		return;
	}
	var url 	= basePath+"advancePayment-db-get.php?requestType=loadExchangeRate";
	var data 	= "currencyId="+$('#cboCurrency').val()+"&date="+$(this).val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmSupAdvancePayment #txtCurrencyRate').val(json.exchgRate);
			}
	});	
}
function viewVoucherReport()
{
	var url  = voucherRptPath+"rpt_payment_voucher_pdf.php?SerialNo="+$('#frmSupAdvancePayment #txtAdvPayNo').val();
		url += "&SerialYear="+$('#frmSupAdvancePayment #txtAdvPayYear').val();
		url += "&Type=ADVANCE";
	
	window.open(url,'rpt_payment_voucher_pdf.php');
	
}
function LoadAutoData(supplierId,cbPONo)
{
	$('#frmSupAdvancePayment #cboSupplier').val(supplierId);
	loadData();
	$('#frmSupAdvancePayment #cboPONo').val(cbPONo);
	loadPOData();	
}
function setPayAmount()
{
	$('#frmSupAdvancePayment .clsAmount').val($(this).val());
	$('#frmSupAdvancePayment #txtTotalAmount').val($(this).val());
}
function reloadPage()
{
	document.location.href = "?q="+menuId;
}
function alertx3()
{
	$('#frmSupAdvancePayment #butSave').validationEngine('hide')	;
}
function alertx4()
{
	$('#frmSupAdvancePayment #butCancle').validationEngine('hide')	;
}

function GetExRate()
{
	var obj	  = this;
	var data  = "Date="+$('#frmSupAdvancePayment #txtDate').val();
		data += "&GLID="+$('#frmSupAdvancePayment #tblMain .clsAccounts').val();
		data += "&CurrId="+$('#frmSupAdvancePayment #cboCurrency').val();
	
	var httpobj = $.ajax({
	url:basePath+"advancePayment-db-get.php?requestType=CheckBankExRate",
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		//$(obj).parent().attr('id',json.RATE);
		if($('#frmSupAdvancePayment #cboCurrency').val()!=json.CURRENCY_ID)
			$('#frmSupAdvancePayment #txtCurrencyRate').val(json.RATE);
		else
			GetCommonExchangeRate1();
  	}
	});	
	
}

function GetCommonExchangeRate1()
{
	var url 		= "libraries/php/common.php?RequestType=URLGetExchangeRate";
		url 	   += "&Date="+$('#frmSupAdvancePayment #txtDate').val();
		url 	   += "&CurrId="+$('#frmSupAdvancePayment #cboCurrency').val();
		
	var httpobj 	= $.ajax({url:url,type:'POST',async:false})
	$('#frmSupAdvancePayment #txtCurrencyRate').val(httpobj.responseText);
}
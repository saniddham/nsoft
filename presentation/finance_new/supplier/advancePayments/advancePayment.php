<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$userId 			= $_SESSION['userId'];

$POReportId			= 934;

include  	  "class/finance/cls_get_gldetails.php";
include_once  "class/masterData/exchange_rate/cls_exchange_rate_get.php";
include_once  "class/cls_commonFunctions_get.php";
//include 	  "include/javascript.html";

$obj_get_GLCombo	= new Cls_Get_GLDetails($db);
$obj_exchgRate_get	= new cls_exchange_rate_get($db);
$obj_common			= new cls_commonFunctions_get($db);

$supplierIdAL		= (!isset($_REQUEST['supplierIdAL'])?'':$_REQUEST['supplierIdAL']);
$PONoAL				= (!isset($_REQUEST['PONoAL'])?'':$_REQUEST['PONoAL']);

$advancePayNo 		= (!isset($_REQUEST['advancePayNo'])?'':$_REQUEST['advancePayNo']);
$advancePayYear 	= (!isset($_REQUEST['advancePayYear'])?'':$_REQUEST['advancePayYear']);
$programCode		= 'P0683';

if($advancePayNo!='' && $advancePayYear!='')
{
	$sql = "SELECT 	SUPPLIER_ID, 
			PO_NO, 
			PO_YEAR,  
			PAY_DATE, 
			CURRENCY_ID, 
			PAY_AMOUNT,
			PAY_METHOD,
			REMARKS,
			BANK_REFERENCE_NO, 
			STATUS
			FROM 
			finance_supplier_advancepayment_header 
			WHERE ADVANCE_PAYMENT_NO= '$advancePayNo' AND
			ADVANCE_PAYMENT_YEAR = '$advancePayYear' ";
	
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$supplierId 	= $row['SUPPLIER_ID'];
		$PONo 			= $row['PO_NO'];
		$POYear 		= $row['PO_YEAR'];
		$payDate 		= $row['PAY_DATE'];
		$payAmount 		= $row['PAY_AMOUNT'];
		$payMethod 		= $row['PAY_METHOD'];
		$bankRefNo 		= $row['BANK_REFERENCE_NO'];
		$remarks 		= $row['REMARKS'];
		$currencyId 	= $row['CURRENCY_ID'];
		$status 		= $row['STATUS'];
		
		$row			= $obj_exchgRate_get->GetAllValues($currencyId,4,$payDate,$companyId,'RunQuery');
		$exchgRate		= $row['AVERAGE_RATE'];	
	}
	$resultADV  = getAdvancedAmount($PONo,$POYear);
	$rowADV 	= mysqli_fetch_array($resultADV);
	
	$advancedAmount		= $rowADV['AdvanceAmonut'];
	$balAmount			= $rowADV['POAmount']-($rowADV['PaidAmount']+$rowADV['AdvanceAmonut']);
	$POAmount			= $rowADV['POAmount'];
}
$dateChangeMode = $obj_common->ValidateSpecialPermission('11',$userId,'RunQuery');
$cancleMode 	= loadCancleMode($programCode,$userId);

?>
<title>Supplier Advance Payments</title>

<!--<script type="text/javascript" src="presentation/finance_new/supplier/advancePayments/advancePayment-js.js"></script>-->

<style>
#apDiv1 {
	position: absolute;
	left: 144px;
	top: 116px;
	width: auto;
	height: auto;
	z-index: 1;
}
</style>

<script>
	var supplierIdAL	='<?php echo $supplierIdAL; ?>';
	var PONoAL			='<?php echo $PONoAL; ?>';
</script>

<?php
if($status==-2)//pending
{
?>
<div id="apDiv1" style="display:none"><img src="images/cancelled.png" /></div>
<?php
}
?>
<form id="frmSupAdvancePayment" name="frmSupAdvancePayment" method="post">
<div align="center">
<div class="trans_layoutL">
<div class="trans_text">Supplier Advance Payments</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
              <td width="17%" class="normalfnt">Advance Payment No</td>
              <td width="34%" class="normalfnt"><input name="txtAdvPayNo" type="text" disabled="disabled" id="txtAdvPayNo" style="width:60px" value="<?php echo $advancePayNo; ?>" />&nbsp;<input name="txtAdvPayYear" type="text" disabled="disabled" id="txtAdvPayYear" style="width:35px" value="<?php echo $advancePayYear; ?>" /></td>
              <td width="21%" class="normalfnt">Date</td>
              <td width="28%" class="normalfnt"><input name="txtDate" type="text" value="<?php echo($payDate==''?date("Y-m-d"):$payDate); ?>" class="validate[required]" id="txtDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($dateChangeMode!=1?'disabled="disabled"':''); ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
            </tr>
            <tr>
              <td class="normalfnt">Supplier <span class="compulsoryRed">*</span></td>
              <td class="normalfnt" valign="top"><select name="cboSupplier" id="cboSupplier"  style="width:220px" class="validate[required]" >
              <?php
				
					$sql = "SELECT DISTINCT PO.intSupplier,MS.strName
							FROM trn_poheader PO
							INNER JOIN mst_locations ML ON ML.intId=PO.intCompany
							INNER JOIN mst_supplier MS ON PO.intSupplier=MS.intId
							WHERE PO.intStatus = 1 AND
								ML.intCompanyId = '$companyId'
							ORDER BY MS.strName ";
					$result = $db->RunQuery($sql);
					echo "<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						if($row['intSupplier']==$supplierId)
							echo "<option value=\"".$row['intSupplier']."\" selected=\"selected\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intSupplier']."\" >".$row['strName']."</option>"; 
					}
			  ?>          
              </select></td>
              <td class="normalfnt">PO No <span class="compulsoryRed">*</span></td>
              <td class="normalfnt" valign="top"><select name="cboPONo" id="cboPONo"  style="width:180px" class="validate[required]" >
              <?php
				if($advancePayNo!='' && $advancePayYear!='')
				{
					$sql = "SELECT DISTINCT intPONo,intPOYear
							FROM trn_poheader
							WHERE intSupplier='$supplierId' AND
							intStatus=1 AND
							PAYMENT_COMPLETED_FLAG = 0 ";
					$result = $db->RunQuery($sql);
					echo "<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						if($row['intPONo']==$PONo && $row['intPOYear']==$POYear)
							echo "<option value=\"".$row['intPONo'].'/'.$row['intPOYear']."\" selected=\"selected\">".$row['intPONo'].'/'.$row['intPOYear']."</option>";
						else
							echo "<option value=\"".$row['intPONo'].'/'.$row['intPOYear']."\" >".$row['intPONo'].'/'.$row['intPOYear']."</option>"; 
					}
				}
			  ?> 
              </select><a class="button green small clsViewPO" style="vertical-align:top" id="butViewPO" name="butViewPO" target="_blank" href="?q=<?php echo $POReportId; ?>&poNo=<?php echo $PONo;?>&year=<?php echo $POYear; ?>">View</a></td>
            </tr>
            <tr>
              <td class="normalfnt">Currency <span class="compulsoryRed">*</span></td>
              <td class="normalfnt" valign="top"><select name="cboCurrency" id="cboCurrency"  style="width:100px" disabled="disabled" class="validate[required]" >
                <option value=""></option>
                <?php
				$sql = "SELECT intId,strCode FROM mst_financecurrency WHERE intStatus=1 ";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($row['intId']==$currencyId)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";
					else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
				}
			  ?>
              </select></td>
              <td class="normalfnt">Balance Amount</td>
              <td class="normalfnt"><input type="text" name="txtAdvancedAmunt" id="txtAdvancedAmunt" style="width:100px;text-align:right" disabled="disabled"  value="<?php echo number_format($balAmount,2,'.',''); ?>" /><input type="hidden" id="txtBalAmunt" name="txtBalAmunt" value="<?php echo $balAmount; ?>" /></td>
            </tr>
            <tr>
              <td class="normalfnt">Rate</td>
              <td class="normalfnt"><input type="text" name="txtCurrencyRate" id="txtCurrencyRate" style="width:100px;text-align:right" disabled="disabled" value="<?php echo($advancePayNo==''?'0.0000':$exchgRate); ?>" class="validate[required]" /></td>
              <td class="normalfnt">PO Amount</td>
              <td class="normalfnt"><input type="text" name="txtPOAmount" id="txtPOAmount" style="width:100px;text-align:right" disabled="disabled"  value="<?php echo number_format($POAmount,2,'.',''); ?>" /></td>
            </tr>
            <tr>
              <td class="normalfnt">Pay Amount <span class="compulsoryRed">*</span></td>
              <td valign="top" class="normalfnt"><input type="text" name="txtPayAmount" id="txtPayAmount" style="width:100px;text-align:right" class="validate[required,custom[number]]" value="<?php echo number_format($payAmount, 2, '.', ''); ?>" /></td>
              <td class="normalfnt">Payment Method</td>
              <td class="normalfnt"><select name="cboPaymentMethod" id="cboPaymentMethod" style="width:180px" >
                <option value=""></option>
                <?php
					$sql = "SELECT
							mst_financepaymentsmethods.intId,
							mst_financepaymentsmethods.strName
							FROM mst_financepaymentsmethods
							WHERE
							mst_financepaymentsmethods.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$payMethod)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
					?>
              </select></td>
            </tr>
            <tr>
              <td class="normalfnt" valign="top">Remarks</td>
              <td rowspan="2" valign="top" class="normalfnt"><textarea name="txtRemarks" id="txtRemarks" style="width:220px" rows="3"><?php echo $remarks; ?></textarea></td>
              <td class="normalfnt" valign="top">Bank Reference Number</td>
              <td class="normalfnt" valign="top"><input type="text" name="txtBankRefNo" id="txtBankRefNo" style="width:180px" value="<?php echo $bankRefNo; ?>" /></td>
            </tr>
            <tr>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="4" class="normalfnt">
                <table width="100%" class="bordered" id="tblMain" >
                  <thead>
                    <tr>
                      <th colspan="5" >GL Allocation
                        <div style="float:right"><a class="button white small" id="butInsertRow" style="display:none">Add New GL</a></div></th>
                      </tr>
                    <tr>
                      <th width="28%" >Account <span class="compulsoryRed">*</span></th>
                      <th width="14%" >Amount <span class="compulsoryRed">*</span></th>
                      <th width="42%" >Memo </th>
                      <th width="12%" >Cost Center <span class="compulsoryRed">*</span></th>
                      </tr>
                    </thead>
                  <tbody>
                    <?php
				   $totAmount = 0;
				   if($advancePayNo!='' && $advancePayYear!='')
				   {
					   $sql = "SELECT ACCOUNT_ID,
								AMOUNT,
								MEMO,
								COST_CENTER
								FROM finance_supplier_advancepayment_details
								WHERE ADVANCE_PAYMENT_NO='$advancePayNo' AND
								ADVANCE_PAYMENT_YEAR='$advancePayYear' ";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							$totAmount = $totAmount+$row['AMOUNT'];
						?>
                    <tr class="cls_tr_firstRow">
                      <td width="28%" style="text-align:center" ><select name="cboAccounts" id="cboAccounts" style="width:100%" class="clsAccounts validate[required]">
                        <?php
						echo $obj_get_GLCombo->getGLCombo('SUPPLIER_ADVANCE',$row['ACCOUNT_ID']);
                    ?>
                        </select></td>
                      <td width="14%" style="text-align:center" ><input name="txtAmmount" type="text" id="txtAmmount" style="width:100%;text-align:right" class="validate[required,custom[number]] clsAmount" value="<?php echo number_format($row['AMOUNT'], 2, '.', ''); ?>" /></td>
                      <td width="42%" style="text-align:center" ><input type="text" name="txtMemo" id="txtMemo" style="width:100%" class="clsMemo" value="<?php echo $row['MEMO']; ?>" /></td>
                      <td width="12%" style="text-align:center" ><select name="cboCostCenter" id="cboCostCenter" class="clsCostCener validate[required]" style="width:100%">
                        <option value=""></option>
                        <?php
						$sqlCC = "SELECT intId,strName
									FROM mst_financedimension
									WHERE intStatus=1";
						$resultCC = $db->RunQuery($sqlCC);
						while($rowCC=mysqli_fetch_array($resultCC))
						{
							if($rowCC['intId']==$row['COST_CENTER'])
								echo "<option value=\"".$rowCC['intId']."\" selected=\"selected\">".$rowCC['strName']."</option>";
							else
								echo "<option value=\"".$rowCC['intId']."\">".$rowCC['strName']."</option>";
						}
						?>
                        </select></td>
                      </tr>
                    <?php
						}
				   }
				   else
				   {
				   ?>
                    <tr class="cls_tr_firstRow">
                      <td width="28%" style="text-align:center" ><select name="cboAccounts" id="cboAccounts" style="width:100%" class="clsAccounts validate[required]">
                        <?php
						echo $obj_get_GLCombo->getGLCombo('SUPPLIER_ADVANCE','');
                    ?>
                        </select></td>
                      <td width="14%" style="text-align:center" ><input name="txtAmmount" type="text" id="txtAmmount" style="width:100%;text-align:right" class="validate[required,custom[number]] clsAmount" /></td>
                      <td width="42%" style="text-align:center" ><input type="text" name="txtMemo" id="txtMemo" style="width:100%" class="clsMemo" /></td>
                      <td width="12%" style="text-align:center" ><select name="cboCostCenter" id="cboCostCenter" class="clsCostCener validate[required]" style="width:100%">
                        <option value=""></option>
                        <?php
						$sqlCC = "SELECT intId,strName
									FROM mst_financedimension
									WHERE intStatus=1";
						$resultCC = $db->RunQuery($sqlCC);
						while($rowCC=mysqli_fetch_array($resultCC))
						{
							if($rowCC['intId']==$row['COST_CENTER'])
								echo "<option value=\"".$rowCC['intId']."\" selected=\"selected\">".$rowCC['strName']."</option>";
							else
								echo "<option value=\"".$rowCC['intId']."\">".$rowCC['strName']."</option>";
						}
						?>
                        </select></td>
                      </tr>
                    <?php
				   }
				   ?>
                    </tbody>
                  </table>
                </td>
            </tr>
            <tr>
              <td colspan="2" class="normalfnt" valign="top">&nbsp;</td>
              <td class="normalfnt" style="text-align:right">Total :</td>
              <td class="normalfnt" style="text-align:right"><input name="txtTotalAmount" type="text" id="txtTotalAmount" style="width:120px;text-align:right" disabled="disabled" value="<?php echo number_format($totAmount, 2, '.', ''); ?>" /></td>
            </tr>
            </table>
        </td>
    </tr>
    
    <tr>
    	<td height="32" >
    		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td align="center" class="tableBorder_allRound"><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($form_permision['edit']==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');?><a class="button white medium" id="butCancle" name="butCancle" <?php echo(($cancleMode!=1 || $status==-2)?'style="display:none"':''); ?> >Cancel</a><a class="button white medium" id="butVoucher" name="butVoucher" <?php echo(($advancePayNo!='' && $advancePayYear!='')?'':'style="display:none"'); ?>>Voucher</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
              </tr>
            </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>
<?php
function loadDateChangeMode($programCode,$userId)
{
	global $db;
	
	$Mode	= 0;
	$sql = "SELECT MS.intStatus
			FROM menus M
			INNER JOIN menus_special MS ON M.intId=MS.intMenuId
			INNER JOIN menus_special_permision SP ON MS.intId=SP.intSpMenuId
			WHERE M.strCode='$programCode' AND
			SP.intUser='$userId' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	if($row['intStatus']==1)
	{
		$Mode = 1;
	}
	return $Mode;
}
function loadCancleMode($programCode,$userId)
{
	global $db;
	$Mode	= 0;
	$sql = "SELECT
			menupermision.intCancel 
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId = '$userId' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	if($row['intCancel']==1)
	{
		$Mode = 1;
	}
	return $Mode;
}
function getAdvancedAmount($PONo,$POYear)
{
	global $db;
	
	$sql = "SELECT SUM(ROUND(((dblQty*dblUnitPrice)*((100-dblDiscount)/100)+dblTaxAmmount),2)) AS POAmount,
			(SELECT IFNULL(SUM((VALUE)*-1),0) FROM finance_supplier_transaction
			WHERE PO_NO=POD.intPONo AND
			PO_YEAR=POD.intPOYear AND
			DOCUMENT_TYPE='ADVANCE') AdvanceAmonut,
			(SELECT IFNULL(SUM((VALUE)*-1),0) FROM finance_supplier_transaction
			WHERE PO_NO=POD.intPONo AND
			PO_YEAR=POD.intPOYear AND
			DOCUMENT_TYPE='PAYMENT') PaidAmount
			FROM trn_podetails POD
			WHERE POD.intPONo='$PONo' AND 
			POD.intPOYear='$POYear' ";
		
	return $db->RunQuery($sql);
}
?>

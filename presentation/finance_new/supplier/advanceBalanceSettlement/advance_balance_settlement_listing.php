<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$session_locationId = $sessions->getLocationId();
$session_companyId 	= $sessions->getCompanyId();
$intUser  			= $sessions->getUserId();

$programCode		= 'P1101';

include_once "class/tables/finance_supplier_advance_balance_settlement_header.php";		$finance_supplier_advance_balance_settlement_header 	= new finance_supplier_advance_balance_settlement_header($db);
include_once "libraries/jqgrid2/inc/jqgrid_dist.php";

//BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
$arr =  json_decode($_REQUEST['filters'],true);

$arr = $arr['rules'];

$where_string = '';
$where_array = array(
					'STATUS'=>'SABSH.STATUS',
					'CONCAT_SETTLE_NO'=>"CONCAT(SABSH.SETTLE_NO,' / ',SABSH.SETTLE_YEAR)",
					'SUPPLIER'=>"SABSH.SUPPLIER_ID",
					'CURRENCY'=>"SABSH.CURRENCY_ID",
					'RAISED_BY'=>"sys_users.strUserName",
					'CREATED_DATE'=>"DATE(SABSH.SETTLE_DATE)"
					);
$arr_status 	= array('Approved'=>'1','Rejected'=>'0','Revised'=>'-1','Cancelled'=>'-2','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='STATUS')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0)					 
		$where_string .= "AND DATE(SABSH.SETTLE_DATE) = '".date('Y-m-d')."'";
//END }

$sql = "SELECT SUB_1.* FROM
			(SELECT
				if(SABSH.STATUS=1,'Approved',if(SABSH.STATUS=-2,'Cancelled',if(SABSH.STATUS=0,'Rejected','Pending'))) as STATUS,
				CONCAT(SABSH.SETTLE_NO,' / ',SABSH.SETTLE_YEAR) AS CONCAT_SETTLE_NO,
				SABSH.SETTLE_NO,
				SABSH.SETTLE_YEAR,
				SABSH.REMARKS,
				SABSH.SUPPLIER_ID,
				SABSH.CURRENCY_ID,
				DATE(SABSH.SETTLE_DATE) AS SETTLE_DATE,
				MS.strName AS SUPPLIER,
				FC.strCode AS CURRENCY,
				SABSH.CREATED_BY,
				sys_users.strUserName AS RAISED_BY,
				(
				SELECT ROUND(SUM(AMOUNT),2) AS totAmount
				FROM finance_supplier_advance_balance_settlement_details
				WHERE SETTLE_NO = SABSH.SETTLE_NO AND
				SETTLE_YEAR = SABSH.SETTLE_YEAR
				) AS AMOUNT, ";
			
				$sql .= "IFNULL((
				SELECT
				concat(sys_users.strUserName,'(',max(finance_supplier_advance_balance_settlement_approve_by.APPROVE_DATE),')' )
				FROM
				finance_supplier_advance_balance_settlement_approve_by
				Inner Join sys_users ON finance_supplier_advance_balance_settlement_approve_by.APPROVE_BY = sys_users.intUserId
				WHERE
				finance_supplier_advance_balance_settlement_approve_by.SETTLE_NO  = SABSH.SETTLE_NO AND
				finance_supplier_advance_balance_settlement_approve_by.SETTLE_YEAR =  SABSH.SETTLE_YEAR AND
				finance_supplier_advance_balance_settlement_approve_by.APPROVE_LEVEL = '-2' AND
				finance_supplier_advance_balance_settlement_approve_by.STATUS =  '0'
				),IF(((SELECT
				menupermision.intCancel 
				FROM menupermision 
				Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode = '$programCode' AND
				menupermision.intUserId =  '$intUser')=1 AND
				SABSH.STATUS=1),'Cancel', '')) as `Cancel`,";
								
							$sql .= "'View' as `View`   
				
				FROM finance_supplier_advance_balance_settlement_header as SABSH 
				INNER JOIN sys_users ON sys_users.intUserId = SABSH.CREATED_BY
				INNER JOIN mst_supplier MS ON MS.intId=SABSH.SUPPLIER_ID
				LEFT JOIN mst_financecurrency FC ON FC.intId=SABSH.CURRENCY_ID
				WHERE SABSH.COMPANY_ID='$session_companyId'
				$where_string
		)  
		AS SUB_1 WHERE 1=1";
//echo $sql.'<br>';
$jq = new jqgrid('',$db);	

$cols	= array();
$col	= array();

$col["title"] 				= "Status";
$col["name"] 				= "STATUS";
$col["width"] 				= "2"; 						
$col["align"] 				= "center";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["stype"] 				= "select";
$str 						= ":All;Approved:Approved;Cancelled:Cancelled" ;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "settleNo";
$col["name"] 				= "SETTLE_NO";	
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;				
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "settleYear";
$col["name"] 				= "SETTLE_YEAR";
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;					
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Settle No";
$col["name"] 				= "CONCAT_SETTLE_NO";
$col["width"] 				= "3";
$col["align"] 				= "center";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col['link']				= '?q=1101&settleNo={SETTLE_NO}&settleYear={SETTLE_YEAR}';
$col["linkoptions"] 		= "target='advance_balance_settlement.php'";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Supplier";
$col["name"] 				= "SUPPLIER";
$col["width"] 				= "4"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "SUPPLIER_ID";
$client_lookup 				= $jq->get_dropdown_values("SELECT
														DISTINCT
														MS.intId   	AS k,
														MS.strName 	AS v
														FROM finance_supplier_advancepayment_header SAH
														INNER JOIN  mst_supplier MS ON MS.intId = SAH.SUPPLIER_ID
														WHERE
														SAH.STATUS = 1
														AND SAH.COMPANY_ID = '$session_companyId'
														ORDER BY MS.strName");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;

$col["title"] 				= "Currency";
$col["name"] 				= "CURRENCY";
$col["width"] 				= "2"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "CURRENCY_ID";
$client_lookup 				= $jq->get_dropdown_values("SELECT
														DISTINCT
														FC.intId   	AS k,
														FC.strCode 	AS v 
														FROM mst_financecurrency FC
														WHERE
														FC.intStatus = 1
														ORDER BY 
														FC.strCode");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;

$col["title"] 				= "Remarks";
$col["name"] 				= "REMARKS";
$col["width"] 				= "4";
$col["align"] 				= "left";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Date";
$col["name"] 				= "SETTLE_DATE";
$col["width"] 				= "3";
$col["align"] 				= "center";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Raised By";
$col["name"] 				= "RAISED_BY";
$col["width"] 				= "2";
$col["align"] 				= "center";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Amount";
$col["name"] 				= "AMOUNT";
$col["width"] 				= "2";
$col["align"] 				= "right";
$col["sortable"] 			= true; 					
$col["search"] 				= false; 					
$col["editable"] 			= false;
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Cancel"; 
$col["name"] 				= "Cancel"; 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q=1104&settleNo={SETTLE_NO}&settleYear={SETTLE_YEAR}&mode=Cancel';
$col["linkoptions"] 		= "target='advance_balance_settlement_report.php'";
$col['linkName']			= 'Cancel';
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Report";
$col["name"] 				= "View";
$col["width"] 				= "2";
$col["align"] 				= "center"; 
$col["sortable"]			= false;
$col["editable"] 			= false; 	
$col["search"] 				= false; 
$col['link']				= '?q=1104&settleNo={SETTLE_NO}&settleYear={SETTLE_YEAR}';
$col["linkoptions"] 		= "target='advance_balance_settlement_report.php'";
$col['linkName']			= 'View';
$cols[] 					= $col;	
$col						= NULL;

$grid["caption"] 			= "Supplier Advance Balance Settlement Listing";
$grid["multiselect"] 		= false;
$grid["rowNum"] 			= 20; 
$grid["sortname"] 			= 'SETTLE_YEAR,SETTLE_NO'; 
$grid["sortorder"] 			= "DESC"; 
$grid["autowidth"] 			= true; 
$grid["multiselect"] 		= false; 
$grid["search"] 			= true; 
$grid["postData"] 			= array("filters" => $sarr ); 
$grid["export"] 			= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Supplier Advance Balance Settlement Listing</title>
<?php
echo $out;

?>
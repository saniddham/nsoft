<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
try
{
	/*
	SAVE
	1. check permissions.
	2. check exchange rate.
	3. check advance status (its should be approved).
	4. deposit amount should equal or less than bal to deposit amount
	5. insert into `finance_supplier_transaction` [DocNo = AdvanceNo,DocYear = AdvanceYear,Document Type = 'ADVANCE' , Amount '+',].
	6. insert into `finance_transaction` [Document Type = 'ADVANCE',TRANSACTION_CATEGORY 'SU',TRANSACTION_CATEGORY_ID 'GL_ID'] [SUPPLIER 'C' , BANK_GL 'D']
	
	CANCEL
	1. check permissions.
	2. check saved location when cancel.
	3. update header status as '-2'.
	4. rollback 'finance_supplier_transaction'
	5. rollback 'finance_transaction'
	*/
	
	$requestType 		= $_REQUEST['requestType']; // request parameter
	$programCode		= 'P1101'; // program code
	$spMenuId			= 68; // back date permission menu id
	$tabIndex			= 5;
	
	include_once "class/tables/menupermision.php";								$menupermision 				= new menupermision($db);
	include_once "class/tables/sys_no.php";										$sys_no 					= new sys_no($db);
	include_once "class/tables/menus_special.php";								$menus_special 				= new menus_special($db);
	include_once "class/tables/mst_financeexchangerate.php";					$mst_financeexchangerate	= new mst_financeexchangerate($db);
	include_once "class/finance/cls_get_gldetails.php";							$obj_get_GLCombo			= new Cls_Get_GLDetails($db);
	include_once "class/dateTime.php";											$dateTimes 					= new dateTimes($db);
	include_once "class/tables/finance_month_end_process.php";					$finance_month_end_process	= new finance_month_end_process($db);
	include_once "class/tables/mst_financeexchangerate.php";					$mst_financeexchangerate	= new mst_financeexchangerate($db);
	include_once "class/tables/finance_mst_chartofaccount.php";					$finance_mst_chartofaccount	= new finance_mst_chartofaccount($db);
	include_once "class/tables/finance_transaction.php";						$finance_transaction		= new finance_transaction($db);
	include_once "class/tables/finance_mst_chartofaccount.php";					$finance_mst_chartofaccount	= new finance_mst_chartofaccount($db);
	include_once "class/tables/finance_supplier_transaction.php";							$finance_supplier_transaction							= new finance_supplier_transaction($db);
	include_once "class/tables/finance_supplier_advancepayment_header.php";					$finance_supplier_advancepayment_header					= new finance_supplier_advancepayment_header($db);
	include_once "class/tables/finance_supplier_advance_balance_settlement_header.php";		$finance_supplier_advance_balance_settlement_header		= new finance_supplier_advance_balance_settlement_header($db);
	include_once "class/tables/finance_supplier_advance_balance_settlement_details.php";	$finance_supplier_advance_balance_settlement_details	= new finance_supplier_advance_balance_settlement_details($db);
	include_once "class/tables/finance_supplier_advance_balance_settlement_approve_by.php";	$finance_supplier_advance_balance_settlement_approve_by	= new finance_supplier_advance_balance_settlement_approve_by($db);
	
	if($requestType=='loadCurrency')
	{
		$db->connect();
		
		$supplierId		= $_REQUEST['supplierId'];
		$date			= $_REQUEST['date'];
		
		$currencyId		= '';
		$exchgRate		= '0.0000';
		
		$result			= $finance_supplier_advancepayment_header->getSupplierWiseCurrency($supplierId,$sessions->getCompanyId());
		$count			= mysqli_num_rows($result);
		$html 			= "<option value=\"\">&nbsp;</option>";	
		while($row = mysqli_fetch_array($result))
		{
			if($count==1)
			{
				$html .= "<option selected=\"selected\" value=\"".$row['CURRENCY_ID']."\">".$row['CURRENCY_CODE']."</option>";
				$currencyId	= $row['CURRENCY_ID'];
			}
			else
				$html .= "<option value=\"".$row['CURRENCY_ID']."\">".$row['CURRENCY_CODE']."</option>";
		}
		if($mst_financeexchangerate->checkCurrencyAvailableForSelectedDate($date,$sessions->getCompanyId()) && $currencyId!='')
		{
			$mst_financeexchangerate->set($currencyId,$date,$sessions->getCompanyId());
			$exchgRate	= $mst_financeexchangerate->getdblExcAvgRate();
		}
		
		$response['currency'] 		= $html;
		$response['exchgRate'] 		= $exchgRate;
		$response['detailData']		= loadDetailData($supplierId,$currencyId);	
	}
	else if($requestType=='loadDetails')
	{
		$db->connect();
		
		$supplierId					= $_REQUEST['supplierId'];
		$currencyId					= $_REQUEST['currencyId'];
		
		$response['detailData']		= loadDetailData($supplierId,$currencyId);	
	}
	else if($requestType=='saveData')
	{
		$arrHeader 		= json_decode($_REQUEST['arrHeader'],true);
		$arrDetails 	= json_decode($_REQUEST['arrDetails'],true);
		
		$settleNo		= $arrHeader['settleNo'];
		$settleYear		= $arrHeader['settleYear'];
		$settleDate		= $arrHeader['settleDate'];
		$supplierId		= $arrHeader['supplierId'];
		$currency		= $arrHeader['currency'];
		$Remarks		= $arrHeader['Remarks'];
		$detailStatus	= false;
		
		$db->connect();$db->begin();//open connection.
		
		//check back date permission
		if($settleDate<$dateTimes->getCurruntDate())
		{
			$backDate_perm	= $menus_special->loadSpecialMenuPermission($spMenuId,$sessions->getUserId());
			
			if($backDate_perm!=1)
				throw new Exception('No permission to backdate advance balance bank sttlement.');
		}
		//check month end process locked
		$mep_status			= $finance_month_end_process->getIsMonthEndProcessLocked($settleDate,$sessions->getCompanyId());
		
		if($mep_status)
				throw new Exception("Month end process locked for the date '".$settleDate."' .");
		
		//check save or update
		if($settleNo=='' && $settleYear=='')
		{
			#########################################################
			#	1. check permissions.								#
			#########################################################
			$menupermision->set($programCode,$sessions->getUserId());
			$permissionArr	= $menupermision->checkMenuPermision('Edit','','');
			
			if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);	
			
			#########################################################
			#	2. check exchange rate.								#
			#########################################################
			if(!$mst_financeexchangerate->checkCurrencyAvailableForSelectedDate($settleDate,$sessions->getCompanyId()))
				throw new Exception("Please enter exchange rates for '".$settleDate."' before save.");
			
			//get sys max no
			$settleNo		= $sys_no->getSerialNoAndUpdateSysNo('SUPPLIER_ADVANCE_BANK_SETTLE_NO',$sessions->getLocationId());
			$settleYear		= $dateTimes->getCurruntYear();
			
			$result_arr		= $finance_supplier_advance_balance_settlement_header->insertRec($settleNo,$settleYear,$supplierId,$currency,$settleDate,1,$Remarks,$sessions->getCompanyId(),$sessions->getLocationId(),$sessions->getUserId(),$dateTimes->getCurruntDateTime(),'NULL',$dateTimes->getCurruntDateTime());
			
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
		}
		else
		{
			throw new Exception("Cannot edit advance balance bank settlement.");
		}
		foreach($arrDetails as $array_loop)
		{
			$detailStatus	= true;
			$advanceNoArr	= explode('/',$array_loop['advanceNoArr']);
			$advanceNo		= $advanceNoArr[0];
			$advanceYear	= $advanceNoArr[1];
			$PONoArr		= explode('/',$array_loop['PONoArr']);
			$PONo			= $PONoArr[0];
			$POYear			= $PONoArr[1];
			$bankGLId		= $array_loop['bankGLId'];
			$amonut			= $array_loop['amonut'];
			
			#########################################################
			#	3. check advance status (its should be approved).	#
			#########################################################
			$finance_supplier_advancepayment_header->set($advanceNo,$advanceYear);
			
			if($finance_supplier_advancepayment_header->getSTATUS()!=1)
				throw new Exception("Supplier Advance '".$advanceNoArr."' is not approved.");
			
			#########################################################################
			#	4. deposit amount should equal or less than bal to deposit amount.	#
			#########################################################################
			$balToDeposit	= $finance_supplier_transaction->getAdvanceBalance($advanceNo,$advanceYear);
			
			if($balToDeposit<$amonut)
				throw new Exception("Entered amount exceed balance to deposit amount</br>Advance No : ".$array_loop['advanceNoArr']."<br>Balance amount : ".$balToDeposit."<br>Amount : ".$amonut);
			
			//save detail data
			$result_arr = $finance_supplier_advance_balance_settlement_details->insertRec($settleNo,$settleYear,$advanceNo,$advanceYear,$PONo,$POYear,0,$bankGLId,$amonut);
			
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
			
			#########################################################
			#	5. insert into finance_supplier_transaction			#
			#########################################################
			$resultSTArr	= $finance_supplier_transaction->insertRec($POYear,$PONo,$supplierId,$currency,$advanceYear,$advanceNo,'ADVANCE','NULL','NULL',$bankGLId,$amonut,$sessions->getCompanyId(),$sessions->getLocationId(),$sessions->getUserId(),$settleDate);
		
			if(!$resultSTArr['status'])
				throw new Exception($resultSTArr['msg']);
			
			$transactionId	= $resultSTArr['insertId'];
			
			//update supplier transaction id in detail table
			$finance_supplier_advance_balance_settlement_details->set($settleNo,$settleYear,$advanceNo,$advanceYear);
			$finance_supplier_advance_balance_settlement_details->setSUPPLIER_TRANSACTION_ID($transactionId);
			$finance_supplier_advance_balance_settlement_details->commit();
			
			#########################################################
			#	6. insert into finance_transaction					#
			#########################################################
			//finance credit transaction SUPPLIER 'C'
			$supplierGLId	= $finance_mst_chartofaccount->getCatogoryTypeWiseGL('S',$supplierId);
			$resultFTArr	= $finance_transaction->insertRec($supplierGLId,$amonut,$settleNo,$settleYear,'ADVANCE','NULL','NULL','C','SU',$supplierId,$currency,'NULL','NULL',0,$sessions->getLocationId(),$sessions->getCompanyId(),$sessions->getUserId(),$settleDate,$dateTimes->getCurruntDateTime());
		
			if(!$resultFTArr['status'])
				throw new Exception($resultFTArr['msg']);	
			
			$transactionId	= $resultFTArr['insertId'];
			//update finance transaction ADVANCE_BALANCE_DEPOSIT_STATUS column
			$finance_transaction->set($transactionId);
			$finance_transaction->setADVANCE_BALANCE_DEPOSIT_STATUS(1);
			$finance_transaction->commit();
			
			//finance debit transaction BANK 'D'
			$resultFTArr	= $finance_transaction->insertRec($bankGLId,$amonut,$settleNo,$settleYear,'ADVANCE','NULL','NULL','D','SU',$supplierId,$currency,'NULL','NULL',0,$sessions->getLocationId(),$sessions->getCompanyId(),$sessions->getUserId(),$settleDate,$dateTimes->getCurruntDateTime());
		
			if(!$resultFTArr['status'])
				throw new Exception($resultFTArr['msg']);	
			
			$transactionId	= $resultFTArr['insertId'];
			//update finance transaction ADVANCE_BALANCE_DEPOSIT_STATUS column
			$finance_transaction->set($transactionId);
			$finance_transaction->setADVANCE_BALANCE_DEPOSIT_STATUS(1);
			$finance_transaction->commit();
		}
		
		if(!$detailStatus)
			throw new Exception('No detail data to save.');
			
		$finance_supplier_advance_balance_settlement_header->set($settleNo,$settleYear);
		$status			= $finance_supplier_advance_balance_settlement_header->getSTATUS();
		
		$menupermision->set($programCode,$sessions->getUserId());		
		$permissionCArr	= $menupermision->checkMenuPermision('Cancel',$status,0);

		$cancelPerm		= $permissionCArr['type'];
		//validate duplicate entry
		$sys_no->validateDuplicateSerialNoWithSysNo($settleNo,'SUPPLIER_ADVANCE_BANK_SETTLE_NO',$sessions->getLocationId());
		
		$db->commit();
		$response['type'] 			= "pass";
		$response['msg'] 			= "Record has been saved successfully.";
			
		$response['settleNo']		= $settleNo;
		$response['settleYear']		= $settleYear;
		$response['cancelPerm']		= $cancelPerm;
	}
	else if($requestType=='cancel')
	{
		$settleNo		= $_REQUEST['settleNo'];
		$settleYear		= $_REQUEST['settleYear'];
		
		$db->connect();$db->begin();//open connection.
		
		$finance_supplier_advance_balance_settlement_header->set($settleNo,$settleYear);
		$status			= $finance_supplier_advance_balance_settlement_header->getSTATUS();
		$settleDate		= $finance_supplier_advance_balance_settlement_header->getSETTLE_DATE();
		
		//check month end process locked
		$mep_status		= $finance_month_end_process->getIsMonthEndProcessLocked($settleDate,$sessions->getCompanyId());
		
		if($mep_status)
				throw new Exception("Month end Process locked for the date '".$settleDate."'.");
		
		#########################################################
		#	1. check permissions.								#
		#########################################################
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Cancel',$status,0);
		
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
		
		#########################################################
		#	2. check user saved location when cancel.			#
		#########################################################		
		if($finance_supplier_advance_balance_settlement_header->getLOCATION_ID()!=$sessions->getLocationId())
			throw new Exception("This is not saved location");
		
		#########################################################
		#	3. update header status as '-2'.					#
		#########################################################
		$finance_supplier_advance_balance_settlement_header->setSTATUS(-2);
		$result_arr	= $finance_supplier_advance_balance_settlement_header->commit();
		
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
		
		// update approve by table
		$result_arr	= $finance_supplier_advance_balance_settlement_approve_by->insertRec($settleNo,$settleYear,-2,$sessions->getUserId(),$dateTimes->getCurruntDateTime(),0);
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
		
		#########################################################
		#	4. rollback 'finance_supplier_transaction'.			#
		#########################################################
		
		$where 	= " SERIAL_NO IN (SELECT 
					SUPPLIER_TRANSACTION_ID
					FROM 
					finance_supplier_advance_balance_settlement_details
					WHERE 
					SETTLE_NO = '$settleNo' AND
					SETTLE_YEAR = '$settleYear') ";
									
		$delResultSTArr = $finance_supplier_transaction->delete($where);
		if(!$delResultSTArr['status'])
			throw new Exception($delResultSTArr['msg']);
		
		#########################################################
		#	5. rollback 'finance_transaction'.					#
		#########################################################
		$where		= " DOCUMENT_NO = '".$settleNo."' AND
						DOCUMENT_YEAR = '".$settleYear."' AND
						DOCUMENT_TYPE = 'ADVANCE' AND
						TRANSACTION_CATEGORY = 'SU' AND
						ADVANCE_BALANCE_DEPOSIT_STATUS = 1 ";
						
		$delResultFTArr = $finance_transaction->delete($where);
		if(!$delResultFTArr['status'])
			throw new Exception($delResultFTArr['msg']);
		
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Record has been canceled successfully.";
	}
}
catch(Exception $e)
{
	$db->rollback();
	$response['msg'] 	=  $e->getMessage();
	$response['error'] 	=  $error_handler->jTraceEx($e);
	$response['type'] 	=  'fail';
	$response['sql']	=  $db->getSql();
	$db->disconnect();
}
$db->disconnect();
echo json_encode($response);

function loadDetailData($supplierId,$currencyId)
{
	global $finance_supplier_transaction;
	global $obj_get_GLCombo;
	global $sessions;
	global $tabIndex;
	
	$html		= "";
	$result		= $finance_supplier_transaction->getAdvancedDetails($supplierId,$currencyId,$sessions->getCompanyId());
	while($row 	= mysqli_fetch_array($result))
	{
		$remarks	= "";
		$title		= "";
		
		if(strlen($row['REMARKS'])>35)
		{
			$remarks	= substr($row['REMARKS'],0,35)."..." ;
			$title		= $row['REMARKS'];
		}
		else
		{
			$remarks	= ($row['REMARKS']==''?'&nbsp;':$row['REMARKS']);
			
		}
		
		$html  .= "<tr class=\"normalfnt\" id=\"".$row['DOCUMENT_NO']."/".$row['DOCUMENT_YEAR']."\">";
		$html  .= "<td style=\"text-align:center\"><input name=\"chkAdvance\" id=\"chkAdvance\" class=\"clsChkAdvance validate[minCheckbox[1]]\" type=\"checkbox\" tabindex=\"".++$tabIndex."\"></td>";
		$html  .= "<td style=\"text-align:left\" class=\"clsAdvanceNo\">".$row['DOCUMENT_NO']."/".$row['DOCUMENT_YEAR']."</td>";
		$html  .= "<td style=\"text-align:left\" class=\"clsPoNo\">".$row['PO_NO']."/".$row['PO_YEAR']."</td>";
		$html  .= "<td style=\"text-align:left\" title=\"".$title."\">".$remarks."</td>";
		$html  .= "<td style=\"text-align:right\" class=\"clsBalToDeposit\">".$row['balAmount']."</td>";
		$html  .= "<td style=\"text-align:center\"><select name=\"cboBank\" id=\"cboBank\" style=\"width:275px\" class=\"clsBankGL\" tabindex=\"".++$tabIndex."\">".
				   $obj_get_GLCombo->getBankGLCombo()."</select></td>";
		$html  .= "<td style=\"text-align:center\"><input name=\"txtSettleAmount\" id=\"txtSettleAmount\" type=\"text\" value=\"\" style=\"width:100px;text-align:right\" class=\"clsSettleAmount cls_input_number_validation\" tabindex=\"".++$tabIndex."\"/></td>";
		$html  .= "</tr>";
		
	}
	return $html;
}
?>
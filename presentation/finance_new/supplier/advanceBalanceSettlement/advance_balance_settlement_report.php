<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once "class/tables/mst_financecurrency.php";						$mst_financecurrency						= new mst_financecurrency($db);
require_once "class/tables/menupermision.php";								$menupermision								= new menupermision($db);
require_once "class/tables/sys_users.php";									$sys_users									= new sys_users($db);
require_once "class/tables/finance_mst_chartofaccount.php";					$finance_mst_chartofaccount											= new finance_mst_chartofaccount($db);
include_once "class/finance/cls_convert_amount_to_word.php";							$obj_AmtName	   										= new Cls_Convert_Amount_To_Word($db);
include_once "class/tables/finance_supplier_advance_balance_settlement_header.php";		$finance_supplier_advance_balance_settlement_header		= new finance_supplier_advance_balance_settlement_header($db);
include_once "class/tables/finance_supplier_advance_balance_settlement_details.php";	$finance_supplier_advance_balance_settlement_details	= new finance_supplier_advance_balance_settlement_details($db);
include_once "class/tables/finance_supplier_advance_balance_settlement_approve_by.php";	$finance_supplier_advance_balance_settlement_approve_by	= new finance_supplier_advance_balance_settlement_approve_by($db);

$programCode			= 'P1101'; 

$db->connect();

$settleNo				= $_REQUEST['settleNo'];
$settleYear				= $_REQUEST['settleYear'];
$mode 					= $_REQUEST['mode'];

$finance_supplier_advance_balance_settlement_header->set($settleNo,$settleYear);
$menupermision->set($programCode,$sessions->getUserId());

$locationId				= $finance_supplier_advance_balance_settlement_header->getLOCATION_ID();
$status					= $finance_supplier_advance_balance_settlement_header->getSTATUS();
$supplierId				= $finance_supplier_advance_balance_settlement_header->getSUPPLIER_ID();
$currencyId				= $finance_supplier_advance_balance_settlement_header->getCURRENCY_ID();
$settleDate				= $finance_supplier_advance_balance_settlement_header->getSETTLE_DATE();
$remarks				= $finance_supplier_advance_balance_settlement_header->getREMARKS();

$detailData				= $finance_supplier_advance_balance_settlement_details->getDetailData($settleNo,$settleYear);

$mst_supplier->set($supplierId);
$mst_financecurrency->set($currencyId);

$supplier				= $mst_supplier->getstrName();
$currency				= $mst_financecurrency->getstrCode();

$header_array["STATUS"]	= $status;
$header_array['LEVELS'] = 0;

$perCancelArr			= $menupermision->checkMenuPermision('Cancel',$status,0);
$permision_cancel		= ($perCancelArr['type']?1:0);

$db->disconnect();
?>
<title>Advance Balance Bank Settlement</title>
<script type="text/javascript" src="presentation/finance_new/supplier/advanceBalanceSettlement/advance_balance_settlement_js.js"></script>

</head>
<body id="rpt_body">
<div id="rpt_div_main">
<?php include 'presentation/report_ribbon_watermark.php'?>
<div id="rpt_div_table">
<form id="frmAdvBalanceSettleRpt" name="frmAdvBalanceSettleRpt">
    <table width="900" align="center">
      <tr>
        <td><?php 	$db->connect();
						include 'report_header_latest.php';
					$db->disconnect();	
					
					$locationId 	= $sessions->getLocationId();		
					?></td>
      </tr>
        <tr>
        <td style="text-align:center">&nbsp;</td>
        </tr>
        <tr>
        <td style="text-align:center"><strong>ADVANCE BALANCE BANK SETTLEMENT</strong></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:left">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
           		<?php include "presentation/report_approve_status_and_buttons_latest.php" ?>
            </table>
            </td>
        </tr>
      <tr>
        <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td>Settle No</td>
            <td>:</td>
            <td><?php echo $settleNo.'/'.$settleYear; ?></td>
            <td>Settle Date</td>
            <td>:</td>
            <td><?php echo $settleDate; ?></td>
          </tr>
          <tr>
            <td>Supplier</td>
            <td>:</td>
            <td><?php echo $supplier; ?></td>
            <td>Remarks</td>
            <td>:</td>
            <td width="29%" rowspan="2" valign="top"><?php echo $remarks; ?></td>
          </tr>
          <tr>
            <td width="16%">Currency</td>
            <td width="1%">:</td>
            <td width="37%"><?php echo $currency; ?></td>
            <td width="16%">&nbsp;</td>
            <td width="1%"></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
            <td valign="top">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" class="rptBordered" id="tblMain">
        <thead>
          <tr>
            <th width="18%">Advance No</th>
            <th width="18%">PO No</th>
            <th width="49%">Bank</th>
            <th width="15%">Amount</th>
            </tr>
        </thead>        
        <tbody>
        <?php
		$db->connect();
		
		while($row = mysqli_fetch_array($detailData))
		{
			$bankGLName		= $finance_mst_chartofaccount->getFullGLAccount($row['BANK_CHART_OF_ACCOUNT_ID']);
		?>
        	<tr class="normalfnt">
                <td style="text-align:left"><?php echo $row['ADVANCE_NO']."/".$row['ADVANCE_YEAR']; ?></td>
                <td style="text-align:left"><?php echo $row['PO_NO']."/".$row['PO_YEAR']; ?></td>
                <td style="text-align:left"><?php echo $bankGLName; ?></td>
                <td style="text-align:right"><?php echo number_format($row['AMOUNT'], 2, '.', ''); ?></td>
        	</tr>
        <?php
		}
		$db->disconnect();	
		?>
        </tbody>
        </table></td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
        </tr>
        <tr>
            <td><?php
            $db->connect();
            $sys_users->set($finance_supplier_advance_balance_settlement_header->getCREATED_BY());
            
            $creator		= $sys_users->getstrUserName();
            $createdDate	= $finance_supplier_advance_balance_settlement_header->getCREATED_DATE();
                                                
            $resultA		= $finance_supplier_advance_balance_settlement_approve_by->getApproveByData($settleNo,$settleYear);
            include "presentation/report_approvedBy_details.php";
            $db->disconnect();
            ?></td>
        </tr>
        <tr height="40">
        <td align="center" class="normalfntMid"><strong>Printed Date: <?php echo date('Y-m-d'); ?></strong></td>
        </tr>
    </table>
</form>
</div>
</div>
</body>
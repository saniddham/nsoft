// JavaScript Document
$(document).ready(function(){

	$("#frmAdvBalanceSettle #cboSupplier").die('change').live('change',loadCurrency);
	$("#frmAdvBalanceSettle #cboCurrency").die('change').live('change',loadDetails);
	$("#frmAdvBalanceSettle .clsChkAdvance").die('click').live('click',setAmount);
	$("#frmAdvBalanceSettle .clsSettleAmount").die('keyup').live('keyup',setMaxAmount);
	$("#frmAdvBalanceSettle #chkAll").die('click').live('click',checkAll);
	$("#frmAdvBalanceSettle #butSave").die('click').live('click',saveData);
	
	$("#frmAdvBalanceSettle #butReport").die('click').live('click',report);
	$("#frmAdvBalanceSettle #butCancel").die('click').live('click',cancelReport);
	
	$('#frmAdvBalanceSettleRpt #butRptCancel').die('click').live('click',cancel);
	
	$("#frmAdvBalanceSettle #cboSupplier").focus();
});
function loadCurrency()
{
	clearGridData();
	
	if($('#frmAdvBalanceSettle #cboSupplier').val()=='')
	{
		$('#frmAdvBalanceSettle #cboCurrency').html('');
		return;
	}
	showWaiting();
	var url 	= "controller.php?q=1101&requestType=loadCurrency";
	var data 	= "supplierId="+$('#frmAdvBalanceSettle #cboSupplier').val()+"&date="+$('#frmAdvBalanceSettle #txtDate').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmAdvBalanceSettle #cboCurrency').html(json.currency);
				$('#frmAdvBalanceSettle #txtCurrencyRate').val(json.exchgRate);
				$('#frmAdvBalanceSettle #tblMain tbody').html(json.detailData);
				hideWaiting();
			}
	});
	hideWaiting();
}
function loadDetails()
{
	clearGridData();
	if($('#frmAdvBalanceSettle #cboCurrency').val()=='')
	{
		return;
	}
	showWaiting();
	var url 	= "controller.php?q=1101&requestType=loadDetails";
	var data 	= "supplierId="+$('#frmAdvBalanceSettle #cboSupplier').val()+"&currencyId="+$('#frmAdvBalanceSettle #cboCurrency').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmAdvBalanceSettle #tblMain tbody').html(json.detailData);
				hideWaiting();
			}
	});
	hideWaiting();
}
function clearGridData()
{
	$("#frmAdvBalanceSettle #tblMain tbody tr").remove();
}
function setAmount()
{
	if($(this).is(':checked'))
	{
		$(this).parent().parent().find('.clsBankGL').addClass('validate[required]');
		$(this).parent().parent().find('.clsSettleAmount').addClass('validate[required,decimal[2],custom[number]]');
		$(this).parent().parent().find('.clsSettleAmount').val($(this).parent().parent().find('.clsBalToDeposit').html());
	}
	else
	{
		$(this).parent().parent().find('.clsBankGL').removeClass('validate[required]');
		$(this).parent().parent().find('.clsSettleAmount').removeClass('validate[required,decimal[2],custom[number]]');
		$(this).parent().parent().find('.clsBankGL').val('');
		$(this).parent().parent().find('.clsSettleAmount').val('');
	}
}
function setMaxAmount()
{
	var maxAmount	= parseFloat($(this).parent().parent().find('.clsBalToDeposit').html());
	
	if($(this).parent().parent().find('.clsChkAdvance').is(':checked'))
	{
		if(parseFloat($(this).val())>maxAmount)
			$(this).val(maxAmount);
	}
}
function checkAll()
{
	if($(this).is(':checked'))
	{
		$('#frmAdvBalanceSettle #tblMain .clsChkAdvance').each(function(index, element) {
            
			$(this).prop('checked',true);
			$(this).parent().parent().find('.clsBankGL').addClass('validate[required]');
			$(this).parent().parent().find('.clsSettleAmount').addClass('validate[required,decimal[2],custom[number]]');
			$(this).parent().parent().find('.clsSettleAmount').val($(this).parent().parent().find('.clsBalToDeposit').html());
			
        });
	}
	else
	{
		$('#frmAdvBalanceSettle #tblMain .clsChkAdvance').each(function(index, element) {
            
			$(this).prop('checked',false);
			$(this).parent().parent().find('.clsBankGL').removeClass('validate[required]');
			$(this).parent().parent().find('.clsSettleAmount').removeClass('validate[required,decimal[2],custom[number]]');
			$(this).parent().parent().find('.clsBankGL').val('');
			$(this).parent().parent().find('.clsSettleAmount').val('');
			
        });
	}
}
function saveData()
{
	if(!IsProcessMonthLocked_date($('#frmAdvBalanceSettle #txtDate').val()))
		return;
	
	if($('#frmAdvBalanceSettle').validationEngine('validate'))
	{
		showWaiting();
		var settleNo 		= $('#frmAdvBalanceSettle #txtSettleNo').val();
		var settleYear 		= $('#frmAdvBalanceSettle #txtSettleYear').val();
		var settleDate 		= $('#frmAdvBalanceSettle #txtDate').val();
		var supplierId 		= $('#frmAdvBalanceSettle #cboSupplier').val();
		var currency 		= $('#frmAdvBalanceSettle #cboCurrency').val();
		var Remarks 		= $('#frmAdvBalanceSettle #txtRemarks').val();
		
		var data 		= "requestType=saveData";
		var arrHeader 	= "{";
							arrHeader += '"settleNo":"'+settleNo+'",' ;
							arrHeader += '"settleYear":"'+settleYear+'",' ;
							arrHeader += '"settleDate":"'+settleDate+'",' ;
							arrHeader += '"supplierId":"'+supplierId+'",' ;
							arrHeader += '"currency":"'+currency+'",' ;
							arrHeader += '"Remarks":'+URLEncode_json(Remarks)+'';
		arrHeader 	   += "}";
		var arrHeader	= arrHeader;
		
		var arrDetails		= "";
		var status			= false;
		var qtyChkStatus 	= false;
		$('#frmAdvBalanceSettle #tblMain .clsChkAdvance').each(function(){
			
			if($(this).is(':checked'))
			{
				status 				= true;
				var advanceNoArr 	= $(this).parent().parent().attr('id');
				var PONoArr 		= $(this).parent().parent().find('.clsPoNo').html();
				var bankGLId 		= $(this).parent().parent().find('.clsBankGL').val();
				var amonut 			= $(this).parent().parent().find('.clsSettleAmount').val();
				
				if(amonut<=0 || amonut=='')
				{
					$(this).parent().parent().find('.clsSettleAmount').validationEngine('showPrompt','Amount must greater than 0.','fail');
					qtyChkStatus = true;
					hideWaiting();
					return false;
				}
				
				arrDetails += "{";
				arrDetails += '"advanceNoArr":"'+ advanceNoArr +'",' ;
				arrDetails += '"PONoArr":"'+ PONoArr +'",' ;
				arrDetails += '"bankGLId":"'+ bankGLId +'",' ;
				arrDetails += '"amonut":"'+ amonut +'"' ;
				arrDetails +=  '},';
			}
		});
		if(qtyChkStatus)
		{
			return;
		}
		if(!status)
		{
			$(this).validationEngine('showPrompt','No records to save.','fail');
			hideWaiting();	
			return;
		}
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		var arrDetails	= '['+arrDetails+']';
		data	   	   += "&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		var url = "controller.php?q=1101";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmAdvBalanceSettle #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertxAll()",3000);
						$('#frmAdvBalanceSettle #txtSettleNo').val(json.settleNo);
						$('#frmAdvBalanceSettle #txtSettleYear').val(json.settleYear);
						$('#frmAdvBalanceSettle #butCancel').show();
						$('#frmAdvBalanceSettle #butReport').show();
						$('#frmAdvBalanceSettle #butSave').hide();
						
						if(!json.cancelPerm)
							$('#frmAdvBalanceSettle #butCancel').hide();

						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
					$('#frmAdvBalanceSettle #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					hideWaiting();
					return;
				}		
		});
	}
	else
	{
		var t = setTimeout("alertxAll()",3000);
	}
}
function report()
{
	if($('#frmAdvBalanceSettle #txtSettleNo').val()=="") 
		return;
		
	var url = "?q=1104&settleNo="+$('#frmAdvBalanceSettle #txtSettleNo').val()+"&settleYear="+$('#frmAdvBalanceSettle #txtSettleYear').val();
	window.open(url,'advance_balance_settlement_report.php');
}
function cancelReport()
{
	if($('#frmAdvBalanceSettle #txtSettleNo').val()=="") 
		return;
		
	var url = "?q=1104&settleNo="+$('#frmAdvBalanceSettle #txtSettleNo').val()+"&settleYear="+$('#frmAdvBalanceSettle #txtSettleYear').val()+"&mode=Cancel";
	window.open(url,'advance_balance_settlement_report.php');
}
function cancel()
{
	var val = $.prompt('Are you sure you want to cancel this bank settlement?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
			showWaiting();
			var url = "controller.php"+window.location.search+'&requestType=cancel';
			var obj = $.ajax({
				url:url,
				type:'post',
				dataType: "json",  
				data:'',
				async:false,						
				success:function(json){
					$('#frmAdvBalanceSettleRpt #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx2()",1000);
						window.location.href = window.location.href;
						window.opener.location.reload();
						return;
					}
					 hideWaiting();
				},
				error:function(xhr,status){						
						$('#frmAdvBalanceSettleRpt #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx2()",3000);
						 hideWaiting();
				}		
			});	
		}			  
	}});
}
function alertxAll()
{
	$('#frmAdvBalanceSettle').validationEngine('hide')	;
}
function alertx()
{
	$('#frmAdvBalanceSettle #butSave').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmAdvBalanceSettleRpt #butRptCancel').validationEngine('hide')	;
}

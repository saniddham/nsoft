<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/tables/menupermision.php";									$menupermision 				= new menupermision($db);
include_once "class/tables/menus_special.php";									$menus_special 				= new menus_special($db);
include_once "class/tables/mst_financeexchangerate.php";						$mst_financeexchangerate	= new mst_financeexchangerate($db);
include_once "class/finance/cls_get_gldetails.php";								$obj_get_GLCombo			= new Cls_Get_GLDetails($db);
include_once "class/tables/finance_supplier_transaction.php";						$finance_supplier_transaction							= new finance_supplier_transaction($db);
include_once "class/tables/finance_supplier_advancepayment_header.php";				$finance_supplier_advancepayment_header					= new finance_supplier_advancepayment_header($db);
include_once "class/tables/finance_supplier_advance_balance_settlement_header.php";	$finance_supplier_advance_balance_settlement_header		= new finance_supplier_advance_balance_settlement_header($db);
include_once "class/tables/finance_supplier_advance_balance_settlement_details.php";$finance_supplier_advance_balance_settlement_details	= new finance_supplier_advance_balance_settlement_details($db);

$spMenuId		= 68;
$programCode	= 'P1101';
$settleNo		= (!isset($_REQUEST['settleNo'])?'':$_REQUEST['settleNo']);
$settleYear		= (!isset($_REQUEST['settleYear'])?'':$_REQUEST['settleYear']);
$status			= 'null';

$db->connect();
$menupermision->set($programCode,$sessions->getUserId());
if($settleNo!='' && $settleYear!='')
{
	$finance_supplier_advance_balance_settlement_header->set($settleNo,$settleYear);
	
	$status					= $finance_supplier_advance_balance_settlement_header->getSTATUS();
	$supplierId				= $finance_supplier_advance_balance_settlement_header->getSUPPLIER_ID();
	$currencyId				= $finance_supplier_advance_balance_settlement_header->getCURRENCY_ID();
	$settleDate				= $finance_supplier_advance_balance_settlement_header->getSETTLE_DATE();
	$remarks				= $finance_supplier_advance_balance_settlement_header->getREMARKS();
	
	if($mst_financeexchangerate->checkCurrencyAvailableForSelectedDate($settleDate,$sessions->getCompanyId()))
	{
		$mst_financeexchangerate->set($currencyId,$settleDate,$sessions->getCompanyId());
		$exchgRate			= $mst_financeexchangerate->getdblExcAvgRate();
	}
	
	$detailData				= $finance_supplier_advance_balance_settlement_details->getDetailData($settleNo,$settleYear);
	
	$perCancelArr			= $menupermision->checkMenuPermision('Cancel',$status,0);
}
else
{
	$perSaveArr				= $menupermision->checkMenuPermision('Edit','null','null');
	$perCancelArr			= $menupermision->checkMenuPermision('Cancel','null','null');
}
$backDate_perm				= $menus_special->loadSpecialMenuPermission($spMenuId,$sessions->getUserId());
?>
<title>Advance Balance Bank Settlement</title>
<link rel="stylesheet" type="text/css" href="../../../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../../../css/button.css"/>
<link rel="stylesheet" type="text/css" href="../../../../css/promt.css"/>

<form id="frmAdvBalanceSettle" name="frmAdvBalanceSettle" method="post">
<div align="center">
    <div class="trans_layoutL" style="width:1000px">
    	<div class="trans_text">Advance Balance Bank Settlement</div>
    	<table width="100%" border="0" align="center">
        	<tr>
            	<td>
                	<table width="100%" border="0" align="center">
                    	<tr class="normalfnt">
                    	  <td>Settlement No</td>
                    	  <td><input name="txtSettleNo" type="text" disabled="disabled" id="txtSettleNo" style="width:60px" value="<?php echo $settleNo; ?>" />&nbsp;<input name="txtSettleYear" type="text" disabled="disabled" id="txtSettleYear" style="width:35px" value="<?php echo $settleYear; ?>" /></td>
                    	  <td>Date</td>
                    	  <td><input name="txtDate" type="text" value="<?php echo($settleDate==''?date("Y-m-d"):$settleDate); ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtDate" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($backDate_perm!=1?'disabled="disabled"':''); ?> tabindex="1"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                  	  </tr>
                    	<tr class="normalfnt">
                        	<td width="10%">Supplier <span class="compulsoryRed">*</span></td>
                            <td width="66%"><select name="cboSupplier" id="cboSupplier"  style="width:252px" class="validate[required]" tabindex="2">
                            <option value="">&nbsp;</option>
							<?php
                            $result	= $finance_supplier_advancepayment_header->getAdvancedSupplier($sessions->getCompanyId());
                            while($row = mysqli_fetch_array($result))
                            {
                                if($supplierId==$row['SUPPLIER_ID'])
                                    echo '<option selected="selected" value="'.$row['SUPPLIER_ID'].'">'.$row['SUPPLIER_NAME'].'</option>';	
                                else
                                    echo '<option value="'.$row['SUPPLIER_ID'].'">'.$row['SUPPLIER_NAME'].'</option>';	
                            }
                            ?>
                            </select></td>
                            <td width="10%">Currency <span class="compulsoryRed">*</span></td>
                            <td width="14%"><select name="cboCurrency" id="cboCurrency"  style="width:102px" class="validate[required] cls_cbo_CurrencyCalExchangeRate" tabindex="3">
                            <option value="">&nbsp;</option>
                            <?php
                            $result	= $finance_supplier_advancepayment_header->getSupplierWiseCurrency($supplierId,$sessions->getCompanyId());
                            while($row=mysqli_fetch_array($result))
                            {
								if($currencyId==$row['CURRENCY_ID'])
									echo "<option value=\"".$row['CURRENCY_ID']."\" selected=\"selected\">".$row['CURRENCY_CODE']."</option>";
								else
									echo "<option value=\"".$row['CURRENCY_ID']."\">".$row['CURRENCY_CODE']."</option>";
                            }
                            ?>
                            </select></td>
                        </tr>
                        <tr class="normalfnt">
                        	<td width="10%" valign="top">Remarks</td>
                            <td width="66%" rowspan="2" valign="top"><textarea name="txtRemarks" id="txtRemarks" style="width:248px" rows="3" tabindex="4"><?php echo $remarks; ?></textarea></td>
                            <td width="10%" valign="top">Rate</td>
                            <td width="14%" valign="top"><input name="txtCurrencyRate" id="txtCurrencyRate" type="text" value="<?php echo number_format($exchgRate, 4, '.', ''); ?>" style="width:98px;text-align:right" disabled="disabled" class="cls_txt_exchangeRate"/></td>
                        </tr>
                        <tr class="normalfnt">
                        	<td width="10%"  valign="top">&nbsp;</td>
                        	<td width="10%">&nbsp;</td>
                            <td width="14%">&nbsp;</td>
                        </tr>
                        <tr>
                        	<td></td>
                        </tr>
                        <tr>
                        	<td colspan="4">
                        	<div style="overflow:scroll;overflow-x: hidden;width:100%;height:290px" class="cls_div_bgstyle">
                            <table width="100%" class="bordered" id="tblMain" bgcolor="#FFFFFF">
                            	<thead>
                                	<tr>
                                    	<th width="3%"><input name="chkAll" id="chkAll" type="checkbox" tabindex="5"></th>
                                        <th width="11%">Advance No</th>
                                        <th width="11%">PO No</th>
                                        <th width="26%">Remarks</th>
                                        <th width="10%">Balance To Deposit</th>
                                        <th width="28%">Bank</th>
                                        <th width="11%">Deposit Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
								$tabIndex  = 5;
								while($row = mysqli_fetch_array($detailData))
								{
									$finance_supplier_advancepayment_header->set($row['ADVANCE_NO'],$row['ADVANCE_YEAR']);
									
									$remarks	= "";
									$title		= "";
									
									if(strlen($finance_supplier_advancepayment_header->getREMARKS())>35)
									{
										$remarks	= substr($finance_supplier_advancepayment_header->getREMARKS(),0,35)."..." ;
										$title		= $finance_supplier_advancepayment_header->getREMARKS();
									}
									else
									{
										$remarks	= ($finance_supplier_advancepayment_header->getREMARKS()==''?'&nbsp;':$finance_supplier_advancepayment_header->getREMARKS());
									}
									$balToDeposit	= $finance_supplier_transaction->getAdvanceBalance($row['ADVANCE_NO'],$row['ADVANCE_YEAR']);
								?>
									<tr class="normalfnt" id=<?php echo $row['ADVANCE_NO']."/".$row['ADVANCE_YEAR'] ?>>
                                        <td style="text-align:center"><input name="chkAdvance" id="chkAdvance" class="clsChkAdvance validate[minCheckbox[1]]" type="checkbox" tabindex="<?php echo ++$tabIndex; ?>"></td>
                                        <td style="text-align:left" class="clsAdvanceNo"><?php echo $row['ADVANCE_NO']."/".$row['ADVANCE_YEAR'] ?></td>
                                        <td style="text-align:left" class="clsPoNo"><?php echo $row['PO_NO']."/".$row['PO_YEAR'] ?></td>
                                        <td style="text-align:left" title="<?php echo $title; ?>"><?php echo $remarks; ?></td>
                                        <td style="text-align:right" class="clsBalToDeposit"><?php echo $balToDeposit; ?></td>
                                        <td style="text-align:center"><select name="cboBank" id="cboBank"  style="width:275px" class="clsBankGL" tabindex="<?php echo ++$tabIndex; ?>" >
                                        <?php echo $obj_get_GLCombo->getBankGLCombo($row['BANK_CHART_OF_ACCOUNT_ID']); ?>
                                        </select></td>
                                        <td style="text-align:center"><input name="txtSettleAmount" id="txtSettleAmount" type="text" value="<?php echo $row['AMOUNT'] ?>" style="width:100px;text-align:right" class="clsSettleAmount cls_input_number_validation" tabindex="<?php echo ++$tabIndex; ?>"/></td>
                                    </tr>
                                <?php
								}
								?> 
                                </tbody>
                            </table>
                            </div>
                            </td>
                        </tr>
                    </table>	
                </td>
            </tr>
            <tr>
                <td height="32" >
                    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                    <tr>
                    <td align="center" ><a class="button white medium" id="butNew" name="butNew" href="?q=1101" tabindex="203">New</a><a class="button white medium" id="butSave" name="butSave" <?php echo(!$perSaveArr['type']?'style="display:none"':''); ?> tabindex="200">Save</a><a class="button white medium" id="butCancel" name="butCancel" <?php echo(!$perCancelArr['type']?'style="display:none"':''); ?> tabindex="201">Cancel</a><a class="button white medium" id="butReport" name="butReport" tabindex="202">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose" tabindex="204">Close</a></td>
                    </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>
</form>
<?php
$db->disconnect();
?>
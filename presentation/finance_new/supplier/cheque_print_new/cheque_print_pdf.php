<?php
session_start();
$userId				= $_SESSION["userId"];
$backwardseperator 	= "../../../../";
include_once "../../../../dataAccess/DBManager2.php";							$db									= new DBManager2();
require_once "../../../../libraries/fpdf/fpdf.php";
include_once "../../../../class/sessions.php";									$sessions							= new sessions();
require_once "../../../../class/tables/menus_special.php";						$menus_special						= new menus_special($db);
require_once "../../../../class/tables/mst_bank.php";							$mst_bank							= new mst_bank($db);
include_once "../../../../class/dateTime.php";									$dateTimes 							= new dateTimes($db);
require_once "../../../../class/tables/finance_check_print_config.php";			$finance_check_print_config			= new finance_check_print_config($db);
include_once "../../../../class/finance/cls_convert_amount_to_word.php";		$obj_convert_amount_to_word			= new Cls_Convert_Amount_To_Word($db);
require_once "../../../../class/tables/finance_supplier_payment_header.php";					$finance_supplier_payment_header		= new finance_supplier_payment_header($db);

$paymentNoArr		= explode('/',$_REQUEST["paymentNo"]);
$paymentNo			= $paymentNoArr[0];
$paymentYear		= $paymentNoArr[1];
$issueDate			= $_REQUEST["issueDate"];
$date_array			= str_split($_REQUEST["issueDate"]);
$spMenuId			= 69;
$savedStatus		= true;

class PDF extends FPDF
{
	function body($rowCheck,$rowConfig)
	{
		global $obj_convert_amount_to_word;
		global $date_array;
		
		$this->AddPage();
		$this->SetFont('courier','',12);
		//$this->SetXY(0,0);$this->Cell(0,0,'HI',0,0,'L');
		//$this->SetXY(1,0.6);$this->Cell(0.5,1,'IH',0,0,'L');
		$this->SetXY($rowConfig["DAY1_X"],$rowConfig["DAY1_Y"]);$this->Cell(0,0,$date_array[8],0,0,'L');
		
		$this->SetXY($rowConfig["DAY2_X"],$rowConfig["DAY2_Y"]);$this->Cell(0,0,$date_array[9],0,0,'L');
		$this->SetXY($rowConfig["MONTH1_X"],$rowConfig["MONTH1_Y"]);$this->Cell(0,0,$date_array[5],0,0,'L');
		$this->SetXY($rowConfig["MONTH2_X"],$rowConfig["MONTH2_Y"]);$this->Cell(0,0,$date_array[6],0,0,'L');
		$this->SetXY($rowConfig["YEAR1_X"],$rowConfig["YEAR1_Y"]);$this->Cell(0,0,$date_array[2],0,0,'L');
		$this->SetXY($rowConfig["YEAR2_X"],$rowConfig["YEAR2_Y"]);$this->Cell(0,0,$date_array[3],0,0,'L');
		$this->SetFont('courier','',12);
		$this->SetXY($rowConfig["PAY_NAME_X"],$rowConfig["PAY_NAME_Y"]);$this->Cell(15,1,$rowCheck["SUPPLIER_NAME"],0,0,'L');
		$this->SetXY($rowConfig["AMOUNT_IN_WORDS_X"],$rowConfig["AMOUNT_IN_WORDS_Y"]);$this->MultiCell(9,0.8,$obj_convert_amount_to_word->Convert_Amount($rowCheck["AMOUNT"],$rowCheck["CURRENCY_CODE"]),0,'L');
		$this->SetXY($rowConfig["AMOUNT_X"],$rowConfig["AMOUNT_Y"]);$this->Cell(5.1,0,number_format($rowCheck["AMOUNT"],2).'/=',0,0,'R');
		
	}
}
$db->connect();$db->begin();

$checkPrintPerm			= $menus_special->loadSpecialMenuPermission($spMenuId,$sessions->getUserId());
if($checkPrintPerm == 0 && $savedStatus)
{
	$savedStatus		= false;
	$errorMsg			= 'No permission to print cheque.';
}
$rowCheck				= $finance_supplier_payment_header->getChequeDetails($paymentNo,$paymentYear);
$rowConfig				= $finance_check_print_config->getBankChequeConfigaration($rowCheck['BANK_ID']);

if(!$rowConfig && $savedStatus)
{
	$mst_bank->set($rowCheck['BANK_ID']);
	
	$savedStatus		= false;
	$errorMsg			= "Please save cheque printing configaration for the bank '".$mst_bank->getBANK_NAME()."' before proceed.";
}

$pdf = new PDF('L','cm',array(8,17.8));
$pdf->body($rowCheck,$rowConfig);

/*$finance_supplier_payment_header->set($paymentNo,$paymentYear);
if($finance_supplier_payment_header->getSTATUS()!=1 && $savedStatus)
{
	$savedStatus		= false;
	$errorMsg			= "Payment No : ".$paymentNo."/".$paymentYear." is not approved.";
}
else if($finance_supplier_payment_header->getCHEQUE_PRINT_STATUS()==1 && $savedStatus)
{
	$savedStatus		= false;
	$errorMsg			= "This cheque is already printed.";
}
else
{
	$finance_supplier_payment_header->setCHEQUE_PRINT_STATUS(1);
	$finance_supplier_payment_header->setCHEQUE_ISSUE_DATE($issueDate);
	$finance_supplier_payment_header->setCHEQUE_PRINT_BY($sessions->getUserId());
	$finance_supplier_payment_header->setCHEQUE_PRINT_DATE($dateTimes->getCurruntDateTime());
	
	$result_arr	= $finance_supplier_payment_header->commit();	
	if(!$result_arr['status'] && $savedStatus)
	{
		$savedStatus	= false;
		$errorMsg		= $result_arr['msg'];
	}	
}*/
if($savedStatus)
{
	$db->commit();
	$pdf->Output('cheque_print_pdf.pdf','I');
}
else
{
	$db->rollback();
	echo $errorMsg;
}
$db->disconnect();	
?>
// JavaScript Document
$(document).ready(function(){
	
	$("#frmChequePrint #butSearch").die('click').live('click',pageSubmit);
	$("#frmChequePrint .clsPrint").die('click').live('click',chequePrint);
	$("#frmChequePrint .clsCancel").die('click').live('click',cancelCheque);
});
function pageSubmit()
{
	 $("#frmChequePrint").submit();
}
function chequePrint()
{
	var paymentNo	= $(this).parent().parent().attr('id');
	var issueDate	= $(this).parent().parent().find('.clsIssueDate').val();
	var obj			= this;
	
	if(issueDate=='')
	{
		$(this).validationEngine('showPrompt','Please enter a issue date.','fail');
		return;
	}
	
	var url = "presentation/finance_new/supplier/cheque_print_new/cheque_print_pdf.php?paymentNo="+paymentNo+"&issueDate="+issueDate;
	window.open(url,'cheque_print_pdf.php');
	
	var t = setTimeout(""+$("#frmChequePrint").submit()+"",3000);
	
}
function cancelCheque()
{
	var paymentNo	= $(this).parent().parent().attr('id');
	var obj			= $(this).parent();
	
	var val = $.prompt('Are you sure you want to cancel this printed cheque?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				var url 	= "controller.php?q=1113&requestType=cancelCheque";
				var data 	= "&paymentNo="+paymentNo;
				$.ajax({
					url:url,
					data:data,
					dataType:'json',
					type:"POST",
					async:false,
					success:function(json)
					{
						if(json.type=='pass')
						{
							obj.html('<span style="color:#F00">Canceled</span>');
							if(json.printPerm==1)
								obj.parent().find('.clsTdPrint').html('<a class="button green small clsPrint" id="butPrint" name="butPrint" tabindex="2">Print</a>');
							var t=setTimeout("alertxAll()",1000);
						}
						else
						{
							obj.validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						}
					}
				});
			}			  
	}});
	
	
}
function alertxAll()
{
	$('#frmChequePrint').validationEngine('hide')	;
}
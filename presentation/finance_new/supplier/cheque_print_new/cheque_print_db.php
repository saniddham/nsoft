<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/tables/menus_special.php";						$menus_special						= new menus_special($db);
include_once "class/dateTime.php";									$dateTimes 							= new dateTimes($db);
include_once "class/tables/finance_supplier_payment_header.php";	$finance_supplier_payment_header	= new finance_supplier_payment_header($db);
try
{
	$requestType 			= $_REQUEST['requestType']; // request parameter
	
	if($requestType=='getChequePrintStatus')
	{
		$paymentNoArr		= explode('/',$_REQUEST['paymentNo']);
		$paymentNo			= $paymentNoArr[0];
		$paymentYear		= $paymentNoArr[1];
		
		$finance_supplier_payment_header->set($paymentNo,$paymentYear);
		
		$printPerm			= $menus_special->loadSpecialMenuPermission(69,$sessions->getUserId());
		$cancelPerm			= $menus_special->loadSpecialMenuPermission(70,$sessions->getUserId());
		
		$response['status']		= $finance_supplier_payment_header->getCHEQUE_PRINT_STATUS();
		$response['printPerm']	= $printPerm;
		$response['cancelPerm']	= $cancelPerm;
	}
	else if($requestType=='cancelCheque')
	{
		$paymentNoArr		= explode('/',$_REQUEST['paymentNo']);
		$paymentNo			= $paymentNoArr[0];
		$paymentYear		= $paymentNoArr[1];
		
		$db->connect();$db->begin();
		
		$cancelPerm			= $menus_special->loadSpecialMenuPermission(70,$sessions->getUserId());
		$printPerm			= $menus_special->loadSpecialMenuPermission(69,$sessions->getUserId());
		
		if($cancelPerm==0)
			throw new Exception("No permission to cancel printed cheque.");	
		
		$finance_supplier_payment_header->set($paymentNo,$paymentYear);
		
		if($finance_supplier_payment_header->getSTATUS()!=1)
			throw new Exception("Payment No : ".$paymentNo."/".$paymentYear." is not approved.");	
		
		if($finance_supplier_payment_header->getCHEQUE_PRINT_STATUS()==-2)
			throw new Exception("This cheque is already printed.");
		
		$finance_supplier_payment_header->setCHEQUE_PRINT_STATUS(-2);
		$finance_supplier_payment_header->setCHEQUE_ISSUE_DATE('null');
		$finance_supplier_payment_header->setCHEQUE_PRINT_BY('null');
		$finance_supplier_payment_header->setCHEQUE_PRINT_DATE('null');
		
		$finance_supplier_payment_header->setCHEQUE_CANCEL_DATE($dateTimes->getCurruntDateTime());
		$finance_supplier_payment_header->setCHEQUE_CANCEL_BY($sessions->getUserId());
		
		$result_arr	= $finance_supplier_payment_header->commit();	
		
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
		
		$db->commit();
		$response['type'] 			= "pass";
		$response['printPerm']		= $printPerm;		
	}
}
catch(Exception $e)
{
	$db->rollback();
	$response['msg'] 		=  $e->getMessage();
	$response['error'] 		=  $error_handler->jTraceEx($e);
	$response['type'] 		=  'fail';
	$response['sql']		=  $db->getSql();
	$db->disconnect();
}
$db->disconnect();
echo json_encode($response);

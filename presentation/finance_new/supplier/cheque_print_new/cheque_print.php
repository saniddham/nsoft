<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/tables/menus_special.php";									$menus_special 						= new menus_special($db);
include_once "class/tables/finance_supplier_payment_header.php";				$finance_supplier_payment_header					= new finance_supplier_payment_header($db);

$printSpMenuId		= 69;
$cancelSpMenuId		= 70;
$supplierId			= (!isset($_REQUEST['cboSupplier'])?'':$_REQUEST['cboSupplier']);

$db->connect();
$result_detail		= $finance_supplier_payment_header->getChequePrintListingData($supplierId,$sessions->getCompanyId());
$printPerm			= $menus_special->loadSpecialMenuPermission($printSpMenuId,$sessions->getUserId());
$cancelPerm			= $menus_special->loadSpecialMenuPermission($cancelSpMenuId,$sessions->getUserId());
?>
<title>Supplier Cheque Printing</title>
<link rel="stylesheet" type="text/css" href="../../../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../../../css/button.css"/>
<link rel="stylesheet" type="text/css" href="../../../../css/promt.css"/>

<form id="frmChequePrint" name="frmChequePrint" method="post">
<div align="center">
    <div class="trans_layoutXL">
    	<div class="trans_text">Supplier Cheque Printing</div>
        <table width="100%" border="0" align="center">
        	<tr>
            	<td>
                	<table width="100%" border="0" align="center">
                    	<tr class="normalfnt">
                        	<td width="14%">Supplier</td>
                            <td width="74%"><select name="cboSupplier" id="cboSupplier"  style="width:300px" tabindex="1">
                            <option value="">&nbsp;</option>
							<?php
                            $result	= $finance_supplier_payment_header->getChequePrintingSuppliers($sessions->getCompanyId());
                            while($row = mysqli_fetch_array($result))
                            {
								if($row['SUPPLIER_ID']==$supplierId) 
                             		echo '<option selected=\"selected\" value="'.$row['SUPPLIER_ID'].'">'.$row['strName'].'</option>';	
								else
									echo '<option value="'.$row['SUPPLIER_ID'].'">'.$row['strName'].'</option>';	
                            }
                            ?>
                            </select></td>
                            <td width="12%" style="text-align:right"><a class="button white medium" id="butSearch" name="butSearch" tabindex="2">Search</a></td>
                        </tr>
                    	<tr class="normalfnt">
                    	  <td></td>
                    	  <td></td>
                    	  <td></td>
                  	  </tr>
                    </table>
                </td>
            </tr>
            <tr>
            	<td colspan="3">
                <div style="overflow:scroll;overflow-x: hidden;width:100%;height:350px" class="cls_div_bgstyle">
                <table width="100%" class="bordered" id="tblMain" bgcolor="#FFFFFF">
                	<thead>
                    	<tr class="normalfnt">
                        	<th width="8%">Payment No</th>
                            <th width="19%">Bank</th>
                            <th width="11%">Voucher No</th>
                            <th width="22%">Supplier</th>
                            <th width="7%">Amount</th>
                            <th width="7%">Date</th>
                            <th width="10%">Issue Date</th>
                            <th width="8%">Print</th>
                            <?php
							echo($cancelPerm==1?"<th width=\"8%\">Cancel</th>":"");
							?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
					$i	= 0;
					
					while($row = mysqli_fetch_array($result_detail))
					{
						
					?>
                    	<tr class="normalfnt" id="<?php echo $row['PAYMENT_NO'].'/'.$row['PAYMENT_YEAR'];?>">
                        	<td style="text-align:left" class="clsPaymentNo"><?php echo $row['PAYMENT_NO'].'/'.$row['PAYMENT_YEAR']; ?></td>
                            <td style="text-align:left" id="<?php echo $row['BANK_ID']; ?>" class="clsBank"><?php echo $row['BANK_NAME']; ?></td>
                            <td style="text-align:left" class="clsVoucher"><?php echo $row['VOUCHER_NO']; ?></td>
                            <td style="text-align:left" id="<?php echo $row['SUPPLIER_ID']; ?>" class="clsSupplier"><?php echo $row['SUPPLIER_NAME']; ?></td>
                            <td style="text-align:right" class="clsAmount"><?php echo $row['AMOUNT']; ?></td>
                            <td style="text-align:center" class="clsPayDate"><?php echo $row['PAY_DATE']; ?></td>
                            <td style="text-align:center"><input name="txtIssueDate" type="text" value="<?php echo date('Y-m-d') ?>" class="clsIssueDate" id="txtIssueDate<?php echo ++$i; ?>" style="width:90px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                            <td style="text-align:center" class="clsTdPrint"><?php echo($printPerm?($row['CHEQUE_PRINT_STATUS']==1?'<span style="color:#090">Printed</span>':'<a class="button green small clsPrint" id="butPrint" name="butPrint" tabindex="2">Print</a>'):'&nbsp;');?></td>
                            
                            <?php
							if($cancelPerm==1)
							{
							?>
                            	<td style="text-align:center" class="clsTdCancel"><?php echo($cancelPerm?($row['CHEQUE_PRINT_STATUS']==-2?'<span style="color:#F00">Canceled</span>':($row['CHEQUE_PRINT_STATUS']==1?'<a class="button red small clsCancel" id="butCancel" name="butCancel" tabindex="2">Cancel</a>':'')):'&nbsp;');?></td>
							<?php
							}
							?>
                        </tr>
                    <?php
					}
					?>	
                    </tbody>
                </table>
                </div>
                </td>
            </tr>
            <tr>
                <td height="32" >
                    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                    <tr>
                    <td align="center"><a href="main.php" class="button white medium" id="butClose" name="butClose" tabindex="204">Close</a></td>
                    </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div> 
</form>
<?php
$db->disconnect();
?>
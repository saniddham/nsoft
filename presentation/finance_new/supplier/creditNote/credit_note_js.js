$(document).ready(function(e) {
    $('#frmCreditNote #cboSupplier').die('change').live('change',loadCurrency);
	$('#frmCreditNote #cboCurrency').die('change').live('change',loadInvoiceNo);
	$('#frmCreditNote #cboInvoiceNo').die('change').live('change',loadMainGrid);
	
	$('#frmCreditNote #butSave').die('click').live('click',save);
	$('#frmCreditNote #butReport').die('click').live('click',viewReport);
	$('#frmCreditNote #butApprove').die('click').live('click',approveReport);
	$('#frmCreditNote #butCancel').die('click').live('click',cancelReport);	
	$('#frmCreditNote #butNew').die('click').live('click',clearForm);	
	$('#frmCreditNote #butAddNewGL').die('click').live('click',addNewGL);
	
	$('#frmCreditNote #tblMain tbody .cls_calculateAmount').die('blur').live('blur',calculateRowEvent);
	$('#frmCreditNote #tblMain tbody .cls_check').die('click').live('click',calculateRowEventCheck);
	$('#frmCreditNote #tblMain thead .cls_checkAll').die('click').live('click',checkAll);
	$('#frmCreditNote #tblMain tbody .cls_tax').die('change').live('change',calculateTax);
	$("#frmCreditNote #tblMain tbody :text").die('blur').live('blur',validateTextbox);
	
	$('#frmCreditNote #tblGL tbody #butDelete').die('click').live('click',deleteGLRow);
	$('#frmCreditNote #tblGL tbody .cls_amount').die('keyup').live('keyup',calculateGLTotal);
	$("#frmCreditNote #tblGL tbody :text").die('blur').live('blur',validateTextbox);
	
	$('#frmCreditNoteReport #butRptConfirm').die('click').live('click',approve);
	$('#frmCreditNoteReport #butRptReject').die('click').live('click',reject);
	$('#frmCreditNoteReport #butRptCancel').die('click').live('click',cancel);
	

});

function validateTextbox()
{
	if($(this).val()=='')
		$(this).val('0.00');
}

function clearForm()
{
	window.location = '?q=1084';
}

function checkAll()
{
	var checked		= $(this).is(':checked');
	$('#frmCreditNote #tblMain tbody tr .cls_td_checkbox').children().each(function(index, element) {
    	$(this).prop('checked',checked);
		
		var invoiced		= ($(this).parent().parent().find('.cls_td_invoiced').html()==''?0:parseFloat($(this).parent().parent().find('.cls_td_invoiced').html()));
		var credited		= ($(this).parent().parent().find('.cls_td_credited').html()==''?0:parseFloat($(this).parent().parent().find('.cls_td_credited').html()));
		var invoicing		= ($(this).parent().parent().find('.cls_td_amount').children().val()==''?0:parseFloat($(this).parent().parent().find('.cls_td_amount').children().val()));

		calculateRowAmount($(this));
    });
}

function calculateRowEventCheck()
{
	if($(this).is(':checked'))
	{	
		$($(this)).parent().parent().find('.cls_td_amount').children().attr('disabled',false);
		$($(this)).parent().parent().find('.cls_td_amount').children().select();
	}
	else
		$($(this)).parent().parent().find('.cls_td_amount').children().attr('disabled',true);

	calculateRowAmount($(this));
}

function calculateRowEvent()
{		
	calculateRowAmount($(this));
}

function calculateTax()
{
	showWaiting();
	calculateRowAmount($(this));
	hideWaiting();
}

function calculateRowAmount(obj)
{
	if($(obj).parent().parent().find('.cls_td_checkbox').children().is(':checked')){
				
		$(obj).parent().parent().find('.cls_td_amount').children().attr('disabled',false);
		
		var taxId			= $(obj).parent().parent().find('.cls_td_tax').children().val();
		var amount			= ($(obj).parent().parent().find('.cls_td_amount').children().val()==''?0:parseFloat($(obj).parent().parent().find('.cls_td_amount').children().val()));
		var rowAmount		= RoundNumber(amount,2);
		var taxAmount		= getTaxAmount(rowAmount,taxId);
		
		$(obj).parent().parent().find('.cls_td_tax').attr('id',taxAmount.toFixed(2));
	}
	else{
		$(obj).parent().parent().find('.cls_td_amount').children().attr('disabled',true);
		$(obj).parent().parent().find('.cls_td_tax').attr('id','0.00');
	}
		
	calculateTotal();
}

function calculateTotal()
{
	$('#frmCreditNote #tblGL tbody .cls_td_amount').children().val('0.00');
	
	var amount	 		= 0;
	var taxAmount	 	= 0;
	$('#frmCreditNote #tblMain tbody tr').each(function(index, element) {
		var rowAmount	= 0;
		var obj1		= $(this);
       
	    if(!$(obj1).find('.cls_td_checkbox').children().is(':checked'))
			return;
		
		amount 			+= ($(obj1).find('.cls_td_amount').children().val()==''?0:parseFloat($(obj1).find('.cls_td_amount').children().val()));
		taxAmount		+= ($(obj1).find('.cls_td_tax').attr('id')==''?0:parseFloat($(obj1).find('.cls_td_tax').attr('id')));
		rowAmount		 = ($(obj1).find('.cls_td_amount').children().val()==''?0:parseFloat($(obj1).find('.cls_td_amount').children().val()));
		taxRowAmount	 = ($(obj1).find('.cls_td_tax').attr('id')==''?0:parseFloat($(obj1).find('.cls_td_tax').attr('id')));
		
		$('#frmCreditNote #tblGL tbody tr').each(function() {
			var obj2	 = $(this);
			glrowAmount	 = ($(obj2).find('.cls_td_amount').children().val()==''?0:parseFloat($(obj2).find('.cls_td_amount').children().val()));
			
			if($(obj2).find('.cls_td_gl').attr('id')==$(obj1).attr('id'))
				$(obj2).find('.cls_td_amount').children().val(RoundNumber(glrowAmount+rowAmount,2).toFixed(2));
				
			if($(obj1).find('.cls_td_tax').children().val()!='' && ($(obj2).find('.cls_td_gl').attr('id')==$(obj1).find('.cls_td_tax').attr('title')))
				$(obj2).find('.cls_td_amount').children().val(RoundNumber(glrowAmount+taxRowAmount,2).toFixed(2));
		});		
    });
	
	calculateGLTotal();		//CALCULATE GL ALLOCATION GRID TOTALS.
	
	var subTotal	= RoundNumber(amount,2);
	var taxAmount	= RoundNumber(taxAmount,2);
	var grandTotal	= subTotal+taxAmount;

	$('#frmCreditNote #txtSubTotal').val(subTotal.toFixed(2));
	$('#frmCreditNote #txtTaxTotal').val(taxAmount.toFixed(2));
	$('#frmCreditNote #txtTotal').val(grandTotal.toFixed(2));
}

function calculateGLTotal()
{
	var totGLRowAmount = 0;
	$('#frmCreditNote #tblGL tbody tr').each(function() {
			totGLRowAmount += ($(this).find('.cls_td_amount').children().val()==''?0:parseFloat($(this).find('.cls_td_amount').children().val()));
		});
	$('#frmCreditNote #tblGL tfoot tr').find('.cls_td_total').html(totGLRowAmount.toFixed(2));
}

function loadCurrency()
{
	showWaiting();
	clearInput();
	$('#frmCreditNote #tblMain tbody').html('');
	$('#frmCreditNote #tblGL tbody').html('');
	$('#frmCreditNote #tblAmount tbody').html('');
	
	var url 	= "controller.php";
	var data	= "q=1084";
		data   += "&RequestType=URLLoadCurrency";
		data   += "&SupplierId="+$('#frmCreditNote #cboSupplier').val();
	
	$.ajax({
		url:url,
		data:data,
		dataType: 'json', 
		type:'POST', 
		async:false,
		success:function(json){
			$('#frmCreditNote #cboCurrency').html(json.html);
			$('#frmCreditNote #cboCurrency').change();
		}		
	});
	hideWaiting();
}

function loadInvoiceNo()
{
	showWaiting();
	clearInput();
	$('#frmCreditNote #tblMain tbody').html('');
	$('#frmCreditNote #tblGL tbody').html('');
	$('#frmCreditNote #tblAmount tbody').html('');
	
	var url 	= "controller.php";
	var data	= "q=1084";
		data   += "&RequestType=URLLoadInvoiceNo";
		data   += "&SupplierId="+$('#frmCreditNote #cboSupplier').val();
		data   += "&CurrencyId="+$('#frmCreditNote #cboCurrency').val();
		data   += "&Date="+$('#frmCreditNote #txtDate').val();
	
	$.ajax({
		url:url,
		data:data,
		dataType: 'json', 
		type:'POST', 
		async:false,
		success:function(json){
			$('#frmCreditNote #cboInvoiceNo').html(json.html);
			$('#frmCreditNote #txtExRate').val(json.ExchangeRate);
		}		
	});	
	hideWaiting();
}

function loadMainGrid()
{
	showWaiting();
	clearInput();
	$('#frmCreditNote #tblMain tbody').html('');
	$('#frmCreditNote #tblGL tbody').html('');
	$('#frmCreditNote #tblAmount tbody').html('');
	
	var url 	= "controller.php";
	var data	= "q=1084";
		data   += "&RequestType=URLLoadMainGrid";
		data   += "&InvoiceNo="+$('#frmCreditNote #cboInvoiceNo').val();
	
	var obj = $.ajax({
		url:url,
		data:data,
		dataType: 'json', 
		type:'POST', 
		async:false,
		success:function(json){
            console.log("this is json response");
			console.log(json);
			$('#frmCreditNote #tblMain tbody').html(json.html);
			$('#frmCreditNote #tblAmount tbody').html(json.html1);
			$('#frmCreditNote #tblGL tbody').html(json.html2);
		}		
	});
    var json = JSON.parse(obj.responseText);
    $('#frmCreditNote #tblMain tbody').html(json["html"]);
    $('#frmCreditNote #tblAmount tbody').html(json["html1"]);
    $('#frmCreditNote #tblGL tbody').html(json["html2"]);
	hideWaiting();
}

function save()
{
	showWaiting();

	if(!saveValidaton()){
		hideWaiting();
		return;
	}
	
	var headerArray = "{";
			headerArray += '"CreditNo":"'+$('#frmCreditNote #txtCreditNo').val()+'",';
			headerArray += '"CreditYear":"'+$('#frmCreditNote #txtCreditYear').val()+'",';
			headerArray += '"SupplierId":"'+$('#frmCreditNote #cboSupplier').val()+'",';
			headerArray += '"CurrencyId":"'+$('#frmCreditNote #cboCurrency').val()+'",';
			headerArray += '"InvoiceNo":"'+$('#frmCreditNote #cboInvoiceNo').val()+'",';
			headerArray += '"CreditDate":"'+$('#frmCreditNote #txtDate').val()+'",';
			headerArray += '"Remarks":'+URLEncode_json($('#frmCreditNote #txtRemarks').val())+'';
		headerArray += "}";
	
	var detailArray  = "[";
	$('#frmCreditNote #tblMain tbody tr').each(function(index, element) {
		 if(!$(this).find('.cls_td_checkbox').children().is(':checked'))
		 	return;
		detailArray += "{";
		detailArray += '"ITEM_ID":"'+$(this).find('.cls_td_item').attr('id')+'",';
		detailArray += '"AMOUNT":"'+$(this).find('.cls_td_amount').children().val()+'",';
		detailArray += '"TAX_CODE_ID":"'+$(this).find('.cls_td_tax').children().val()+'",';
		detailArray += '"TAX_AMOUNT":"'+$(this).find('.cls_td_tax').attr('id')+'",';
		detailArray += '"COST_CENTER_ID":"'+$(this).find('.cls_td_costCenter').children().val()+'"';
		detailArray += "},";	
	});
	detailArray 		= detailArray.substr(0,detailArray.length-1);detailArray += "]";
	
	var glArray  = "[";
	$('#frmCreditNote #tblGL tbody tr').each(function(index, element) {
		 if(parseFloat($(this).find('.cls_td_amount').children().val())<='0')
		 	return;
		glArray += "{";
		glArray += '"GL_ID":"'+$(this).find('.cls_td_gl').children().val()+'",';
		glArray += '"AMOUNT":"'+$(this).find('.cls_td_amount').children().val()+'"';
		glArray += "},";	
	});
	glArray 		= glArray.substr(0,glArray.length-1);glArray += "]";
	
	if($('#frmCreditNote #txtCreditNo').val().trim()=='')
		var requestType	= "URLSave";
	else
		var requestType	= "URLUpdate";
			
	var url 	= "controller.php";
	var data	= "q=1084";
		data   += "&RequestType="+requestType;
		data   += "&HeaderArray="+headerArray;
		data   += "&DetailArray="+detailArray;
		data   += "&GLArray="+glArray;
	
	$.ajax({
		url:url,
		data:data,
		dataType: 'json', 
		type:'POST', 
		async:false,
		success:function(json){
			if(json.type=='pass')
			{
				$('#frmCreditNote #butSave').validationEngine('showPrompt',json.msg,json.type);
				setTimeout("AutoHideFormValidate('#frmCreditNote #butSave')",2000);
				$('#frmCreditNote #txtCreditNo').val(json.creditNo);
				$('#frmCreditNote #txtCreditYear').val(json.creditYear);
				haddleInterface('AFTER_SAVE',json.butSave,json.butApprove,json.butCancel);				
				hideWaiting();
			}
			else
			{
				$('#frmCreditNote #butSave').validationEngine('showPrompt',json.msg,json.type);
				setTimeout("AutoHideFormValidate('#frmCreditNote #butSave')",5000);
				haddleInterface('AFTER_SAVE',true,false,false);
				hideWaiting();
			}
		},
		error:function(xhr,status){
			$('#frmCreditNote #butSave').validationEngine('showPrompt',errormsg(xhr.status),'fail');
			setTimeout("AutoHideFormValidate('#frmCreditNote #butSave')",5000);
			hideWaiting();	
		}		
	});	
}

function saveValidaton()
{
	if(!$('#frmCreditNote').validationEngine('validate')){
		setTimeout("AutoHideFormValidate('#frmCreditNote')",4000);
		return false;
	}

	if(!IsProcessMonthLocked_date($('#frmCreditNote #txtDate').val()))
		return false;
		
	return true;
}

function haddleInterface(type,butSave,butApprove,butCancel)
{
	switch (type){
		case 'AFTER_SAVE':
			if(butSave)		
				$('#frmCreditNote #butSave').show();
			else
				$('#frmCreditNote #butSave').hide();	
				
			if(butApprove){		
				$('#frmCreditNote #butApprove').show();
				$('#frmCreditNote #butReject').show();
			}else{
				$('#frmCreditNote #butApprove').hide();
				$('#frmCreditNote #butReject').hide();
			}
			break;
	}
}

function viewReport()
{
	if($('#frmCreditNote #txtCreditNo').val().trim()==''){
		$('#frmCreditNote #butReport').validationEngine('showPrompt','No valid credit note details appear to view','fail');
		setTimeout("AutoHideFormValidate('#frmCreditNote')",4000);	
		return;	
	}
	
	window.open('?q=1086&CreditNo='+$('#frmCreditNote #txtCreditNo').val()+'&CreditYear='+$('#frmCreditNote #txtCreditYear').val(),'credit_note_report.php');
}

function approveReport(obj)
{
	if($('#frmCreditNote #txtCreditNo').val().trim()==''){
		$('#frmCreditNote #butReport').validationEngine('showPrompt','No valid credit note details appear to view','fail');
		setTimeout("AutoHideFormValidate('#frmCreditNote')",4000);		
		return;	
	}
	
	window.open('?q=1086&CreditNo='+$('#frmCreditNote #txtCreditNo').val()+'&CreditYear='+$('#frmCreditNote #txtCreditYear').val()+'&mode=Confirm','credit_note_report.php');
}

function cancelReport(obj)
{
	if($('#frmCreditNote #txtCreditNo').val().trim()==''){
		$('#frmCreditNote #butReport').validationEngine('showPrompt','No valid credit note details appear to view','fail');
		setTimeout("AutoHideFormValidate('#frmCreditNote')",4000);
		return;	
	}
	
	window.open('?q=1086&CreditNo='+$('#frmCreditNote #txtCreditNo').val()+'&CreditYear='+$('#frmCreditNote #txtCreditYear').val()+'&mode=Cancel','credit_note_report.php');
}

function AutoHideFormValidate(obj)
{
	$(obj).validationEngine('hide');
}

function approve()
{	
	var val = $.prompt("Are you sure you want to 'Approve' this 'Credit Note' ?",{
	buttons: { Ok: true, Cancel: false },
	callback: function(v,m,f){
		if(v)
		{
			showWaiting();
			var url = "controller.php"+window.location.search+'&RequestType=URLApprove';
			var obj = $.ajax({
				url:url,
				type:'post',
				dataType: "json",  
				async:false,						
				success:function(json){					
					if(json.type=='pass')
					{	
						$('#frmCreditNoteReport #butRptConfirm').validationEngine('showPrompt',json.msg,json.type);
						var t = setTimeout("AutoHideFormValidate('#frmCreditNoteReport #butRptConfirm')",2000);
						window.location.href = window.location.href;
						window.opener.location.reload();
					}
					else
					{
						$('#frmCreditNoteReport #butRptConfirm').validationEngine('showPrompt',json.msg,json.type);
						var t = setTimeout("AutoHideFormValidate('#frmCreditNoteReport #butRptConfirm')",4000);
						hideWaiting();
					}
					
				},
				error:function(xhr,status){						
					$('#frmCreditNoteReport #butRptConfirm').validationEngine('showPrompt',errormsg(xhr.status),'fail');
					var t = setTimeout("AutoHideFormValidate('#frmCreditNoteReport #butRptConfirm')",4000);
					 hideWaiting();
				}		
			});	
		}			  
	}});
}

function reject()
{	
	var val = $.prompt("Are you sure you want to 'Reject' this 'Credit Note' ?",{
	buttons: { Ok: true, Cancel: false },
	callback: function(v,m,f){
		if(v)
		{
			showWaiting();
			var url = "controller.php"+window.location.search+'&RequestType=URLReject';
			var obj = $.ajax({
				url:url,
				type:'post',
				dataType: "json",  
				async:false,						
				success:function(json){					
					if(json.type=='pass')
					{	
						$('#frmCreditNoteReport #butRptConfirm').validationEngine('showPrompt',json.msg,json.type);
						var t = setTimeout("AutoHideFormValidate('#frmCreditNoteReport #butRptConfirm')",2000);
						window.location.href = window.location.href;
						window.opener.location.reload();
					}
					else
					{
						$('#frmCreditNoteReport #butRptConfirm').validationEngine('showPrompt',json.msg,json.type);
						var t = setTimeout("AutoHideFormValidate('#frmCreditNoteReport #butRptConfirm')",4000);
						hideWaiting();
					}
					
				},
				error:function(xhr,status){						
					$('#frmCreditNoteReport #butRptConfirm').validationEngine('showPrompt',errormsg(xhr.status),'fail');
					var t = setTimeout("AutoHideFormValidate('#frmCreditNoteReport #butRptConfirm')",4000);
					 hideWaiting();
				}		
			});	
		}			  
	}});
}

function cancel()
{	
	var val = $.prompt("Are you sure you want to 'Cancel' this 'Credit Note' ?",{
	buttons: { Ok: true, Cancel: false },
	callback: function(v,m,f){
		if(v)
		{
			showWaiting();
			var url = "controller.php"+window.location.search+'&RequestType=URLCancel';
			var obj = $.ajax({
				url:url,
				type:'post',
				dataType: "json",  
				async:false,						
				success:function(json){					
					if(json.type=='pass')
					{	
						$('#frmCreditNoteReport #butRptCancel').validationEngine('showPrompt',json.msg,json.type);
						var t = setTimeout("AutoHideFormValidate('#frmCreditNoteReport #butRptCancel')",2000);
						window.location.href = window.location.href;
						window.opener.location.reload();
					}
					else
					{
						$('#frmCreditNoteReport #butRptCancel').validationEngine('showPrompt',json.msg,json.type);
						var t = setTimeout("AutoHideFormValidate('#frmCreditNoteReport #butRptCancel')",4000);
						hideWaiting();
					}
					
				},
				error:function(xhr,status){						
					$('#frmCreditNoteReport #butRptCancel').validationEngine('showPrompt',errormsg(xhr.status),'fail');
					var t = setTimeout("AutoHideFormValidate('#frmCreditNoteReport #butRptCancel')",4000);
					 hideWaiting();
				}		
			});	
		}			  
	}});
}

function getTaxAmount(rowAmount,taxId)
{
	if(taxId=='')
		return 0;
		
	var taxAmount	=  0;	
	var url 	 	= "controller.php?q=709&requestType=calculateTaxAmount";
	var data 	 	= "taxId="+taxId+"&amount="+rowAmount;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				taxAmount = RoundNumber(json.TaxValue,2);
			}
	});
	return taxAmount;
}

function addNewGL()
{
	$('#frmCreditNote #tblGL tbody tr:last').after("<tr>"+$('#frmCreditNote #tblGL tbody tr:first').html()+"</tr>");
	$('#frmCreditNote #tblGL tbody tr:last').find('.cls_td_gl').attr('id','');
	$('#frmCreditNote #tblGL tbody tr:last').find('.cls_td_gl').children().val('');
	$('#frmCreditNote #tblGL tbody tr:last').find('.cls_td_amount').children().val('0.00');
}

function deleteGLRow()
{
	if($('#frmCreditNote #tblGL tbody tr').length>1)
		$(this).parent().parent().remove();
	calculateGLTotal();
}

function clearInput()
{
	$('#frmCreditNote #tblMain thead .cls_checkAll').prop('checked',false);
	$('#frmCreditNote #tblGL tfoot tr').find('.cls_td_total').html('0.00');
	$('#frmCreditNote #txtSubTotal').val('0.00');
	$('#frmCreditNote #txtTaxTotal').val('0.00');
	$('#frmCreditNote #txtTotal').val('0.00');
}
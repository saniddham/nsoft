<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('display_errors',1);
#BEGIN  - INCLUDE FILES & CREATE CLASS OBJECTS { 
require_once "class/tables/finance_supplier_creditnote_header.php";			$finance_supplier_creditnote_header			= new finance_supplier_creditnote_header($db);
require_once "class/tables/finance_supplier_creditnote_details.php";		$finance_supplier_creditnote_details		= new finance_supplier_creditnote_details($db);
require_once "class/tables/finance_supplier_creditnote_gl.php";				$finance_supplier_creditnote_gl				= new finance_supplier_creditnote_gl($db);
require_once "class/tables/mst_supplier.php";								$mst_supplier								= new mst_supplier($db);
require_once "class/tables/mst_financecurrency.php";						$mst_financecurrency						= new mst_financecurrency($db);
require_once "class/tables/finance_supplier_purchaseinvoice_header.php";	$finance_supplier_purchaseinvoice_header	= new finance_supplier_purchaseinvoice_header($db);
require_once "class/tables/mst_item.php";									$mst_item									= new mst_item($db);
require_once "class/tables/mst_units.php";									$mst_units									= new mst_units($db);
require_once "class/tables/finance_mst_chartofaccount.php";					$finance_mst_chartofaccount					= new finance_mst_chartofaccount($db);
require_once "class/tables/menupermision.php";								$menupermision 								= new menupermision($db);
require_once "class/finance/cls_convert_amount_to_word.php";				$obj_AmtName	   							= new Cls_Convert_Amount_To_Word($db);
require_once "class/tables/finance_supplier_creditnote_approve_by.php";		$finance_supplier_creditnote_approve_by	   	= new finance_supplier_creditnote_approve_by($db);
require_once "class/tables/sys_users.php";									$sys_users									= new sys_users($db);
require_once "class/tables/mst_financetaxgroup.php";						$mst_financetaxgroup						= new mst_financetaxgroup($db);
require_once "class/tables/mst_maincategory.php";								$mst_maincategory								= new mst_maincategory($db);
require_once "class/tables/mst_subcategory.php";								$mst_subcategory								= new mst_subcategory($db);

#END	- INCLUDE FILES & CREATE CLASS OBJECTS }

#BEGIN  - ASSIGN PARAMETERS {
$programCode		= 'P1084'; 
$creditNo			= $_REQUEST['CreditNo'];
$creditYear			= $_REQUEST['CreditYear'];
$mode 				= $_REQUEST['mode'];
#END 	}

#BEGIN	- GET REPORT BODY DETAILS {
$menupermision->set($programCode,$sessions->getUserId());
$finance_supplier_creditnote_header->set($creditNo,$creditYear);
$mst_supplier->set($finance_supplier_creditnote_header->getSUPPLIER_ID());
$mst_financecurrency->set($finance_supplier_creditnote_header->getCURRENCY_ID());
$finance_supplier_purchaseinvoice_header->set($finance_supplier_creditnote_header->getPURCHASE_INVOICE_NO(),$finance_supplier_creditnote_header->getPURCHASE_INVOICE_YEAR());

$locationId				= $finance_supplier_creditnote_header->getLOCATION_ID();
$status					= $finance_supplier_creditnote_header->getSTATUS();
$levels					= $finance_supplier_creditnote_header->getAPPROVE_LEVELS();	

$header_array["STATUS"]	= $status;
$header_array['LEVELS'] = $levels;
$perRejArr				= $menupermision->checkMenuPermision('Reject',$status,$levels);
$perApproArr			= $menupermision->checkMenuPermision('Approve',$status,$levels);
$perCancelArr			= $menupermision->checkMenuPermision('Cancel',$status,$levels);

$permision_reject		= ($perRejArr['type']?1:0);
$permision_confirm		= ($perApproArr['type']?1:0);
$permision_cancel		= ($perCancelArr['type']?1:0);

#END 	}
?>

<head>
<title>Supplier Credit Note Report</title>
<script type="text/javascript" src="presentation/finance_new/supplier/creditNote/credit_note_js.js"></script>

</head>
<body id="rpt_body">
<div id="rpt_div_main">
<?php include 'presentation/report_ribbon_watermark.php'?>
<div id="rpt_div_table">
<form id="frmCreditNoteReport" name="frmCreditNoteReport" method="post" >
  <table width="1000" align="center">
    <tr>
      <td colspan="2"><?php include 'report_header_latest.php'?></td>
    </tr>
    <tr>
      <td colspan="2" style="text-align:center">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="text-align:center"><strong>SUPPLIER CREDIT NOTE REPORT</strong></td>
    </tr>
    <tr>
      <td colspan="2" style="text-align:left"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <?php include "presentation/report_approve_status_and_buttons_latest.php" ?>
      </table></td>
    </tr>

    <tr>
      <td colspan="2"><table width="100%" border="0" class="normalfnt">
          <tr>
            <td width="14%">Credit Note No</td>
            <td width="1%">:</td>
            <td width="44%"><?php echo $creditNo.'/'.$creditYear?></td>
            <td width="14%">Date</td>
            <td width="1%">:</td>
            <td width="26%"><?php echo $finance_supplier_creditnote_header->getCREDIT_DATE()?></td>
          </tr>
          <tr>
            <td>Supplier</td>
            <td>:</td>
            <td><?php echo $mst_supplier->getstrName()?></td>
            <td>Currency</td>
            <td>:</td>
            <td><?php echo $mst_financecurrency->getstrCode()?></td>
          </tr>
          <tr>
            <td>Invoice No</td>
            <td>:</td>
            <td><a target="rptPurchaseInvoice.php" href="?q=893&purInvoiceNo=<?php echo $finance_supplier_creditnote_header->getPURCHASE_INVOICE_NO()?>&purInvoiceYear=<?php echo $finance_supplier_creditnote_header->getPURCHASE_INVOICE_YEAR()?>"><?php echo $finance_supplier_creditnote_header->getPURCHASE_INVOICE_NO().'/'.$finance_supplier_creditnote_header->getPURCHASE_INVOICE_YEAR()?></a></td>
            <td>Supplier Invoice No</td>
            <td>:</td>
            <td><b><?php echo $finance_supplier_purchaseinvoice_header->getINVOICE_NO()?></b></td>
          </tr>
          <tr>
            <td>Remarks</td>
            <td>:</td>
            <td rowspan="2" valign="top"><?php echo $finance_supplier_creditnote_header->getREMARKS()?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="right"><?php if($menupermision->getintExportToPDF()){?><a id="rpt_export_pdf" class="button green small no-print" target="crebit_note_report_pdf.php" href="<?php echo "presentation/finance_new/supplier/creditNote/credit_note_report_pdf.php?CreditNo=$creditNo&CreditYear=$creditYear"?>">Click here to print Credit Note</a><?php } ?></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="2" ><table width="100%" border="0" class="rptBordered" id="tblMain">
          <thead>
            <tr>
              <th width="3%">&nbsp;</th>
              <th width="13%">Main Category</th>
              <th width="12%">Sub Category</th>
              <th width="30%">Item</th>
              <th width="7%">UOM</th>
              <th width="8%">Amount</th>
              <th width="8%">Tax Code</th>
              <th width="9%">Tax Amount</th>
              <th width="10%">Total Amount</th>
            </tr>
          </thead>
          <tbody>
            <?php
			$subTotal			= 0;
			$taxTotal			= 0;
			$grandTotal			= 0;
			$result				= getDetails($creditNo,$creditYear);
		  	while($row=mysqli_fetch_array($result))
		  	{
				$mst_item->set($row['ITEM_ID']);
				$mst_units->set($mst_item->getintUOM());
				$mst_maincategory->set($mst_item->getintMainCategory());
				$mst_subcategory->set($mst_item->getintSubCategory());
				if($row['TAX_GROUP_ID']=='')
					$taxName	= '&nbsp;';
				else{
					$mst_financetaxgroup->set($row['TAX_GROUP_ID']);		
					$taxName	= $mst_financetaxgroup->getstrCode();							
				}
		  ?>
            <tr>
              <td style="text-align:center"><?php echo ++$i;?>.</td>
              <td width="13%"><?php echo $mst_maincategory->getstrName();?></td>
              <td width="12%"><?php echo $mst_subcategory->getstrName();?></td>
              <td width="30%"><?php echo $mst_item->getstrName();?></td>
              <td width="7%"><?php echo $mst_units->getstrName();?></td>
              <td style="text-align:right"><?php echo number_format($row['AMOUNT'],2)?></td>
              <td style="text-align:left"><?php echo $taxName?></td>
              <td style="text-align:right"><?php echo number_format($row['TAX_AMOUNT'],2)?></td>
              <td style="text-align:right"><?php echo number_format(round($row['AMOUNT'],2)+round($row['TAX_AMOUNT'],2),2)?></td>
            </tr>
            <?php
			$subTotal			+= round($row['AMOUNT'],2);
			$taxTotal			+= round($row['TAX_AMOUNT'],2);;
		  }
		  	$grandTotal			 = round($subTotal+$taxTotal,2);
			?>
          </tbody>
          <tfoot>
          	 <tr style="font-weight:bold">
              <td style="text-align:center" colspan="5">TOTAL</td>
              <td style="text-align:right"><?php echo number_format($subTotal,2)?></td>
              <td style="text-align:left">&nbsp;</td>
              <td style="text-align:right"><?php echo number_format($taxTotal,2)?></td>
              <td style="text-align:right"><?php echo number_format($grandTotal,2)?></td>
            </tr>
          </tfoot>
        </table></td>
    </tr>
    <tr>
      <td colspan="2" class="normalfnt"><b><?php echo $obj_AmtName->Convert_Amount($grandTotal,$mst_financecurrency->getstrCode()); ?></b></td>
    </tr>
    <tr>
      <td width="416" valign="top"><table width="300" class="rptBordered" id="tblLedger" >
          <tr>
            <th width="69%" >Ledger Account</th>
            <th width="31%" >Amount</th>
          </tr>
          <?php
		  $glTotal	= 0;
		  $result = $finance_supplier_creditnote_gl->select("CHART_OF_ACCOUNT_ID,AMOUNT",null,"CREDIT_NO = $creditNo AND CREDIT_YEAR = $creditYear");
		  while($row=mysqli_fetch_array($result))
		  {
		  ?>
          <tr>
            <td width="69%" ><?php echo $finance_mst_chartofaccount->getFullGLAccount($row['CHART_OF_ACCOUNT_ID']);?></td>
            <td width="31%" align="right"><?php echo number_format($row['AMOUNT'],2)?></td>
          </tr>
          <?php
		  	 $glTotal	+= round($row['AMOUNT'],2);
		  }
		  ?>
          <tr class="dataRow">
            <td style="text-align:center"><b>TOTAL</b></td>
            <td style="text-align:right"><b><?php echo number_format($glTotal,2); ?></b></td>
          </tr>
        </table></td>
      <td width="472" valign="top" ><table width="230" border="0" align="right" class="normalfnt">
          <tr>
            <td width="100">Sub Total</td>
            <td width="10">:</td>
            <td width="106" align="right"><?php echo number_format($subTotal,2)?></td>
          </tr>
          <tr>
            <td>Tax Total</td>
            <td>:</td>
            <td align="right"><?php echo number_format($taxTotal,2);?></td>
          </tr>
          <tr>
            <td>Grand Total</td>
            <td>:</td>
            <td align="right"><?php echo number_format($grandTotal,2);?></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="normalfnt" colspan="2"></td>
    </tr>
	<tr>
            <td><?php
			$db->connect();
			$sys_users->set($finance_supplier_creditnote_header->getCREATED_BY());
			
			$creator		= $sys_users->getstrUserName();
			$createdDate	= $finance_supplier_creditnote_header->getCREATED_DATE();
												
			$resultA		= $finance_supplier_creditnote_approve_by->getApproveByData($creditNo,$creditYear);
			include "presentation/report_approvedBy_details.php";
			$db->disconnect();
			?></td>
			</tr>
            <?php
    if($mode=='Confirm')
    {
    ?>
    <tr height="30">
    <?php
    $url  = "presentation/send_to_approval_latest.php";
    $url .= "?status=$status";										// * set recent status
    $url .= "&approveLevels=$levels";								// * set approve levels in order header
    $url .= "&programCode=$programCode";								// * program code (ex:P10001)
    $url .= "&program=Supplier Credit Note Report";									// * program name (ex:Purchase Order)
    $url .= "&companyId=".$finance_supplier_creditnote_header->getCOMPANY_ID();									// * created company id
    $url .= "&createUserId=".$finance_supplier_creditnote_header->getCREATED_BY();
    
    $url .= "&subject=Supplier Credit Note for Approval (".$creditNo."/".$creditYear.")";	
    
    $url .= "&statement1=Please Approve this";	
    $url .= "&statement2=to Approve this";	
    $url .= "&link=".base64_encode(MAIN_URL."?q=1086&CreditNo=".$creditNo."&CreditYear=".$creditYear."&mode=Confirm");
    ?>
    <td align="center" class="normalfntMid" colspan="2"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"></iframe></td>
    </tr>
    <?php
    }
    ?>
     <tr height="30">
    	<td align="center" class="normalfntMid" colspan="2"><strong>Printed Date: <?php echo date('Y-m-d'); ?></strong></td>
    </tr>
  </table>
</form>
</div>
</div>
</body>
<?php
function getDetails($creditNo,$creditYear)
{
	global $db;
	global $finance_supplier_creditnote_details;
	
	$cols	= "ITEM_ID,
			   AMOUNT,
			   TAX_GROUP_ID,
			   TAX_AMOUNT ";

	$where	= "CREDIT_NO = $creditNo
    			AND CREDIT_YEAR = $creditYear";
	
	return $finance_supplier_creditnote_details->select($cols,$join=null,$where);
}
?>

<?php
#BEGIN  - INCLUDE FILES & CREATE CLASS OBJECTS {
include_once "class/tables/finance_supplier_debitnote_header.php";				$finance_supplier_debitnote_header 			= new finance_supplier_debitnote_header($db);
require_once "libraries/jqgrid2/inc/jqgrid_dist.php";
#END	}

$programCode		= 'P1084';
$userId				= $sessions->getUserId();
$arr 				=  json_decode($_REQUEST['filters'],true);
$arr 				= $arr['rules'];

$select				= "MAX(APPROVE_LEVELS) AS MAX_LEVEL";
$header_result		= $finance_supplier_debitnote_header->select($select,NULL,NULL,NULL,NULL);
$header_array		= mysqli_fetch_array($header_result);
$approveLevel 		= $header_array['MAX_LEVEL'];

$where_string 		= '';
$where_array 		= array(
						'STATUS'=>'CH.STATUS',
						'CONCAT_CREDIT_NO'=>"CONCAT(CH.CREDIT_NO,'/',CH.CREDIT_YEAR)",
						'INVOICE_NO'=>'PH.INVOICE_NO',
						'SUPPLIER_NAME'=>'SU.strName',
						'CURRENCY_CODE'=>'FC.strCode',
						'REMARKS'=>'CH.REMARKS',
						'CREDIT_DATE'=>'CH.CREDIT_DATE',
						'CREATE_BY'=>'U.strUserName',
						'CREATE_DATE'=>'DATE(CH.CREATED_DATE)'
						);
				
$arr_status 	= array('Approved'=>'1','Rejected'=>'0','Canceled'=>'-2','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='STATUS')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND DATE(CH.CREATED_DATE) = '".date('Y-m-d')."'";

$sql = "SELECT
  		SUB_1.*
		FROM (SELECT 
			    IF(CH.STATUS=1,'Approved',IF(CH.STATUS=0,'Rejected',IF(CH.STATUS='-2','Canceled','Pending'))) 	AS STATUS,
			    CH.CREDIT_NO																					AS CREDIT_NO,
			    CH.CREDIT_YEAR																					AS CREDIT_YEAR,
			    CONCAT(CH.CREDIT_NO,'/',CH.CREDIT_YEAR)															AS CONCAT_CREDIT_NO,
				PH.INVOICE_NO																					AS INVOICE_NO,
				SU.strName																						AS SUPPLIER_NAME,
				FC.strCode																						AS CURRENCY_CODE,
				CH.REMARKS																						AS REMARKS,
				CH.CREDIT_DATE																					AS CREDIT_DATE,				
				DATE(CH.CREATED_DATE)																			AS CREATE_DATE,
				CH.CREATED_DATE																					AS CREATED_DATE1,	
				U.strUserName 																					AS CREATE_BY,
				(SELECT ROUND(SUM(AMOUNT + TAX_AMOUNT),2)
				 FROM finance_supplier_creditnote_details CD
				 WHERE CD.CREDIT_NO = CH.CREDIT_NO
				   AND CD.CREDIT_YEAR = CH.CREDIT_YEAR)															AS AMOUNT,
				   
				IFNULL((
				SELECT
				  CONCAT(U.strUserName,'(',max(AB.APPROVE_DATE),')' )
				FROM
				  finance_supplier_creditnote_approve_by AB
				INNER JOIN sys_users U 
				  ON AB.APPROVE_BY = U.intUserId
				WHERE
				  AB.CREDIT_NO  = CH.CREDIT_NO AND
				  AB.CREDIT_YEAR =  CH.CREDIT_YEAR AND
				  AB.APPROVE_LEVEL = '1' AND
				  AB.STATUS =  '0'
				),IF(((SELECT
					menupermision.int1Approval 
					FROM menupermision 
					INNER JOIN menus ON menupermision.intMenuId = menus.intId
					WHERE
					menus.strCode = '$programCode' AND
					menupermision.intUserId = '$userId')=1 AND CH.STATUS>1),'Approve','')) 					AS `1st_Approval`, ";
					
					for($i=2; $i<=$approveLevel; $i++){							
							if($i==2)
								$approval	= "2nd_Approval";
							else if($i==3)
								$approval	= "3rd_Approval";
							else
								$approval	= $i."th_Approval";
							
						$sql .= "IFNULL((
								SELECT
								concat(U.strUserName,'(',max(AB.APPROVE_DATE),')' )
								FROM
								finance_supplier_creditnote_approve_by AB
								Inner Join sys_users U ON AB.APPROVE_BY = U.intUserId
								WHERE
								AB.CREDIT_NO  = CH.CREDIT_NO AND
								AB.CREDIT_YEAR =  CH.CREDIT_YEAR AND
								AB.APPROVE_LEVEL = '$i' AND
								AB.STATUS =  '0'
							),
							IF(
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
									menus.strCode = '$programCode' AND
									menupermision.intUserId =  '$userId')=1 AND (CH.STATUS>1) AND (CH.STATUS<=CH.APPROVE_LEVELS) AND ((SELECT
									concat(sys_users.strUserName )
								FROM
									finance_supplier_creditnote_approve_by AB
								Inner Join sys_users ON AB.APPROVE_BY = sys_users.intUserId
								WHERE
									AB.CREDIT_NO  = CH.CREDIT_NO AND
									AB.CREDIT_YEAR =  CH.CREDIT_YEAR AND
									AB.APPROVE_LEVEL = ($i-1) AND 
									AB.STATUS='0' )<>'')),								
									'Approve',
								 if($i>CH.APPROVE_LEVELS,'-----',''))
								
								) as `".$approval."`, "; 
									
								} 
				   
				$sql .= "IFNULL((
								SELECT
								concat(sys_users.strUserName,'(',max(AB.APPROVE_DATE),')' )
								FROM
								finance_supplier_creditnote_approve_by AB
								Inner Join sys_users ON AB.APPROVE_BY = sys_users.intUserId
								WHERE
								AB.CREDIT_NO = CH.CREDIT_NO AND
								AB.CREDIT_YEAR = CH.CREDIT_YEAR AND
								AB.APPROVE_LEVEL = '-2' AND
								AB.STATUS =  '0'
							),IF(((SELECT
								menupermision.intCancel 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$userId')=1 AND
								CH.STATUS=1),'Cancel', '')) as `Cancel`,
						'View'																							AS VIEW
			  FROM 
			    finance_supplier_creditnote_header CH
			  INNER JOIN finance_supplier_purchaseinvoice_header PH
			    ON PH.PURCHASE_INVOICE_NO = CH.PURCHASE_INVOICE_NO
				AND PH.PURCHASE_INVOICE_YEAR = CH.PURCHASE_INVOICE_YEAR
			  INNER JOIN mst_supplier SU
			    ON SU.intId = CH.SUPPLIER_ID
			  INNER JOIN mst_financecurrency FC
			    ON FC.intId = CH.CURRENCY_ID
			  INNER JOIN sys_users U
			    ON U.intUserId = CH.CREATED_BY
			  WHERE 1=1
			  	$where_string
		) AS SUB_1
		WHERE 1 = 1";

$jq 	= new jqgrid('',$db);
$col 	= array();
$cols 	= array();

$col["title"] 			= "Credit No";
$col["name"] 			= "CREDIT_NO";
$col["width"] 			= "1";
$col["align"] 			= "left";
$col["hidden"] 			= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Credit Year";
$col["name"] 			= "CREDIT_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "left";
$col["hidden"] 			= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Status"; 
$col["name"] 			= "STATUS";
$col["width"] 			= "2";
$col["stype"] 			= "select";
$str 					= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Canceled:Canceled" ;
$col["editoptions"] 	=  array("value"=> $str);
$col["align"] 			= "center";
$cols[] 				= $col;
$col					= NULL;

$col["title"] 			= "Credit No";
$col["name"] 			= "CONCAT_CREDIT_NO";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 
$col['link']			= '?q=1084&CreditNo={CREDIT_NO}&CreditYear={CREDIT_YEAR}';
$col["linkoptions"] 	= "target=credit_note.php";					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Invoice No";
$col["name"] 			= "INVOICE_NO";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Supplier";
$col["name"] 			= "SUPPLIER_NAME";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "CURRENCY_CODE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Remarks";
$col["name"] 			= "REMARKS";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Amount";
$col["name"] 			= "AMOUNT";
$col["width"] 			= "2";
$col["align"] 			= "right";
$col["sortable"] 		= true; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Credit Date";
$col["name"] 			= "CREDIT_DATE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Create By";
$col["name"] 			= "CREATE_BY";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Create Date";
$col["name"] 			= "CREATE_DATE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 				= "1st Approval";
$col["name"] 				= "1st_Approval";
$col["width"] 				= "3";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q=1086&CreditNo={CREDIT_NO}&CreditYear={CREDIT_YEAR}&mode=Confirm';
$col["linkoptions"] 		= "target='credit_note_report.php'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;

for($i=2; $i<=$approveLevel; $i++)
{
	if($i==2){
		$ap		= "2nd Approval";
		$ap1	= "2nd_Approval";
	}
	else if($i==3){
		$ap		= "3rd Approval";
		$ap1	= "3rd_Approval";
	}
	else {
		$ap		= $i."th Approval";
		$ap1	= $i."th_Approval";
	}

$col["title"] 				= $ap; 
$col["name"] 				= $ap1; 
$col["width"] 				= "3";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q=1086&CreditNo={CREDIT_NO}&CreditYear={CREDIT_YEAR}&mode=Confirm';
$col["linkoptions"] 		= "target='credit_note_report.php'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;
}

$col["title"] 				= "Cancel"; 
$col["name"] 				= "Cancel"; 
$col["width"] 				= "3";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q=1086&CreditNo={CREDIT_NO}&CreditYear={CREDIT_YEAR}&mode=Cancel';
$col["linkoptions"] 		= "target='credit_note_report.php'";
$col['linkName']			= 'Cancel';
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 			= "View";
$col["name"] 			= "VIEW";
$col["width"] 			= "1";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= '?q=1086&CreditNo={CREDIT_NO}&CreditYear={CREDIT_YEAR}';
$col["linkoptions"] 	= "target=credit_note_report.php";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Supplier Credit Note Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
//$grid["sortname"] 		= 'CREATED_DATE1'; // by default sort grid by this field
//$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Supplier Credit Note Listing</title>
<?php
echo $out;

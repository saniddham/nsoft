<?php
try{

#BEGIN  - ASSIGN PARAMETERS {
$programCode		= 'P1084'; 
$requestType		= $_REQUEST['RequestType'];
#END 	}

#BEGIN  - INCLUDE FILES & CREATE CLASS OBJECTS { 
require_once "class/tables/finance_supplier_purchaseinvoice_header.php";		$finance_supplier_purchaseinvoice_header		= new finance_supplier_purchaseinvoice_header($db);
require_once "class/tables/finance_supplier_purchaseinvoice_details.php";		$finance_supplier_purchaseinvoice_details		= new finance_supplier_purchaseinvoice_details($db);
require_once "class/tables/mst_financeexchangerate.php";						$mst_financeexchangerate						= new mst_financeexchangerate($db);
require_once "class/tables/mst_maincategory.php";								$mst_maincategory								= new mst_maincategory($db);
require_once "class/tables/mst_subcategory.php";								$mst_subcategory								= new mst_subcategory($db);
require_once "class/tables/mst_units.php";										$mst_units										= new mst_units($db);
require_once "class/tables/mst_financetaxgroup.php";							$mst_financetaxgroup							= new mst_financetaxgroup($db);
require_once "class/tables/mst_financedimension.php";							$mst_financedimension							= new mst_financedimension($db);
require_once "class/tables/finance_supplier_transaction.php";					$finance_supplier_transaction					= new finance_supplier_transaction($db);
require_once "class/tables/finance_transaction.php";							$finance_transaction							= new finance_transaction($db);
require_once "class/tables/finance_supplier_purchaseinvoice_gl.php";			$finance_supplier_purchaseinvoice_gl			= new finance_supplier_purchaseinvoice_gl($db);
require_once "class/tables/finance_mst_chartofaccount.php";						$finance_mst_chartofaccount						= new finance_mst_chartofaccount($db);
require_once "class/tables/finance_supplier_creditnote_header.php";				$finance_supplier_creditnote_header				= new finance_supplier_creditnote_header($db);
require_once "class/tables/finance_supplier_creditnote_details.php";			$finance_supplier_creditnote_details			= new finance_supplier_creditnote_details($db);
require_once "class/tables/finance_supplier_creditnote_gl.php";					$finance_supplier_creditnote_gl					= new finance_supplier_creditnote_gl($db);
require_once "class/tables/finance_supplier_creditnote_approve_by.php";			$finance_supplier_creditnote_approve_by			= new finance_supplier_creditnote_approve_by($db);
require_once "class/tables/menupermision.php";									$menupermision 									= new menupermision($db);
require_once "class/tables/sys_no.php";											$sys_no 										= new sys_no($db);
require_once "class/tables/sys_approvelevels.php";								$sys_approvelevels								= new sys_approvelevels($db);
require_once "class/dateTime.php";												$dateTimes										= new dateTimes($db);
require_once "class/tables/finance_month_end_process.php";						$finance_month_end_process						= new finance_month_end_process($db);
#END	}

#BEGIN 	- USER PRE-DEFINED LOG {
/*
SAVE
	1. CHECK SAVE PERMISSION.
	2. CHECK IS EXCHANGE RATE AVAILABLE FOR SELECTED DATE AND CURRENCY.
	3. CHECK PURCHASE INVOICE STATUS [ITS SHOULD BE APPROVED INVOICE].
	4. CHECK MONTH END PROCESS LOCKED STATUS.
	5. GENERATE NEW SERIAL NO.
	6. SAVE `finance_supplier_creditnote_header` TABLE.
	7. SAVE `finance_supplier_creditnote_details` TABLE.
	8. SAVE `finance_supplier_creditnote_gl` TABLE.
	9. GL AMOUNT & GRAND TOTAL AMOUNT VALIDATION.

UPDATE
	1. CHECK EDIT PERMISSION.
	2. CHECK IS EXCHANGE RATE AVAILABLE FOR SELECTED DATE AND CURRENCY.
	3. CHECK USER SAVED LOCATION WHEN APPROVE.
	4. CHECK PURCHASE INVOICE STATUS [ITS SHOULD BE APPROVED INVOICE].
	5. CHECK MONTH END PROCESS LOCKED STATUS.
	6. UPDATE `finance_supplier_creditnote_header` TABLE.
	7. DELETE `finance_supplier_creditnote_details` TABLE.
	8. SAVE `finance_supplier_creditnote_details` TABLE.
	9. DELETE `finance_supplier_creditnote_gl` TABLE.
	1. SAVE `finance_supplier_creditnote_gl` TABLE.
   11. GL AMOUNT & GRAND TOTAL AMOUNT VALIDATION.
   12. UPDATE MAX STATUS IN `finance_supplier_creditnote_approve_by` TABLE.
	
APPROVE
	1. CHECK APPROVE PERMISSION. 
	2. CHECK IS EXCHANGE RATE AVAILABLE FOR SELECTED DATE AND CURRENCY.
	3. CHECK USER SAVED LOCATION WHEN APPROVE. 
	4. CHECK PURCHASE INVOICE STATUS [ITS SHOULD BE APPROVED INVOICE].
	5. CHECK MONTH END PROCESS LOCKED STATUS.
	6. UPDATE APPROVE HEADER STATUS.
	7. INSERT APPROVED BY TABLE.
	8. FINAL APPROVAL
			insert into `finance_supplier_transaction` [Document Type = 'CREDIT' , Amount '+'].
			insert into `finance_transaction` [Document Type = 'CREDIT',TRANSACTION_CATEGORY 'SU',TRANSACTION_CATEGORY_ID 'GL_ID'] [SUPPLIER 'C' , TAX & OTHER GL 'D']
	
REJECT
	1. CHECK REJECT PERMISSION. 
	2. CHECK USER SAVED LOCATION WHEN REJECT.
	3. UPDATE REJECT HEADER STATUS.
	4. INSERT APPROVED BY TABLE. 
	
CANCEL
	1. CHECK CANCEL PERMISSION.
	2. CHECK MONTH END PROCESS LOCKED STATUS.
	3. CHECK USER SAVED LOCATION WHEN CANCEL.
	4. CHECK IS INVOICE BALANCE AMOUNT AVAILABLE.
	5. UPDATE CANCEL HEADER STATUS.
	6. INSERT APPROVED BY TABLE.
	7. ROLLBACK RECORDS FROM `finance_supplier_transaction` TABLE.
	8. ROLLBACK RECORDS FROM `finance_transaction` TABLE.
*/
#END	}
	
if($requestType=='URLSave')
{
	$headerArray		= json_decode($_REQUEST['HeaderArray'],true);
	$detailArray		= json_decode($_REQUEST['DetailArray'],true);
	$glArray			= json_decode($_REQUEST['GLArray'],true);
	$invoiceArray		= explode('/',$headerArray['InvoiceNo']);
	
	$db->connect();$db->begin();

##########################################################################################################
## 1. CHECK SAVE PERMISSION.          																	##
##########################################################################################################
	$menupermision->set($programCode,$sessions->getUserId());	
	$result	= $menupermision->checkMenuPermision('Edit','','');
	if(!$result['type'])
		throw new Exception($result['msg']);
		
##########################################################################################################
## 2. CHECK IS EXCHANGE RATE AVAILABLE FOR SELECTED DATE AND CURRENCY.								    ##
##########################################################################################################
	$result					= $mst_financeexchangerate->select("dblExcAvgRate",NULL,"intCurrencyId = ".$headerArray['CurrencyId']." AND dtmDate = '".$headerArray['CreditDate']."' AND intCompanyId = ".$sessions->getCompanyId(),NULL,NULL);
	$row					= mysqli_fetch_array($result);
	$exchangeRate			= $row['dblExcAvgRate'];
	if($exchangeRate == '')
		throw new Exception("Please enter exchange rates for ".$headerArray['CreditDate']);

##########################################################################################################
## 3. CHECK PURCHASE INVOICE STATUS [ITS SHOULD BE APPROVED INVOICE]								    ##
##########################################################################################################
	$finance_supplier_purchaseinvoice_header->set($invoiceArray[0],$invoiceArray[1]);
	if($finance_supplier_purchaseinvoice_header->getSTATUS()!='1')
		throw new Exception("Selected 'Purchase Invoice No' not a valid invoice.You must select comfirm invoice.");

##########################################################################################################
## 4. CHECK MONTH END PROCESS LOCKED STATUS.								    						##
##########################################################################################################
	if($finance_month_end_process->getIsMonthEndProcessLocked($headerArray['CreditDate'],$sessions->getCompanyId()))
				throw new Exception("Month end Process locked for the date '".$headerArray['CreditDate']."' .");
		
##########################################################################################################
## 5. GENERATE NEW SERIAL NO.  																		    ##
##########################################################################################################
	$creditNo		= $sys_no->getSerialNoAndUpdateSysNo('SUPPLIER_CREDIT_NOTE_NO',$sessions->getLocationId());
	$creditYear		= $dateTimes->getCurruntYear();
	
##########################################################################################################
## 6. SAVE `finance_supplier_creditnote_header` TABLE.													##
##########################################################################################################
	$sys_approvelevels->set($programCode);
	$approveLevel	= $sys_approvelevels->getintApprovalLevel();
	$status			= $approveLevel+1;

	$result = $finance_supplier_creditnote_header->insertRec($creditNo,$creditYear,$invoiceArray[0],$invoiceArray[1],$headerArray['SupplierId'],$headerArray['CurrencyId'],$headerArray['CreditDate'],$headerArray['Remarks'],$status,$approveLevel,$sessions->getCompanyId(),$sessions->getLocationId(),$sessions->getUserId(),$dateTimes->getCurruntDateTime(),'null','null');
	if(!$result['status'])
		throw new Exception($result['msg']);

##########################################################################################################
## 7. SAVE `finance_supplier_creditnote_details` TABLE.													##
##########################################################################################################
	$detailAmount	= 0;
	$booAvailable	= false;
	foreach($detailArray as $row)
	{
		$booAvailable	= true;
		$detailAmount  += round($row['AMOUNT'],2) + round($row['TAX_AMOUNT'],2);
		$taxId	= $row['TAX_CODE_ID']==''?'null':$row['TAX_CODE_ID'];
		$result = $finance_supplier_creditnote_details->insertRec($creditNo,$creditYear,$row['ITEM_ID'],$row['AMOUNT'],$row['TAX_AMOUNT'],$taxId,$row['COST_CENTER_ID']);
		if(!$result['status'])
			throw new Exception($result['msg']);
	}
	
	if(!$booAvailable)
		throw new Exception('No invoice details available to proceed.');			

##########################################################################################################
## 8. SAVE `finance_supplier_creditnote_gl` TABLE.													    ##
##########################################################################################################
	$glAmount		= 0;
	$booAvailable	= false;
	foreach($glArray as $row)
	{
		$booAvailable	= true;
		$glAmount      += round($row['AMOUNT'],2);
		$result = $finance_supplier_creditnote_gl->insertRec($creditNo,$creditYear,$row['GL_ID'],$row['AMOUNT']);
		if(!$result['status'])
			throw new Exception($result['msg']);
	}
	
	if(!$booAvailable)
		throw new Exception('No general ledger details available to proceed.');

##########################################################################################################
## 9. GL AMOUNT & GRAND TOTAL AMOUNT VALIDATION.												    	##
##########################################################################################################
	if($detailAmount!=$glAmount)			
		throw new Exception('Grand Total and GL total amount should be equal.');
				
##########################################################################################################
## VALIDATE APPROVE BUTTON ONCE UPDATE 																	##
##########################################################################################################
	$finance_supplier_creditnote_header->set($creditNo,$creditYear);
	$menupermision->set($programCode,$sessions->getUserId());
	
	$status						= $finance_supplier_creditnote_header->getSTATUS();
	$approveLevel				= $finance_supplier_creditnote_header->getAPPROVE_LEVELS();	
	$butSave					= $menupermision->checkMenuPermision('Edit',$status,$approveLevel);
	$butApprove					= $menupermision->checkMenuPermision('Approve',$status,$approveLevel);
		
	$response['type'] 			= 'pass';
	$response['msg']			= "Credit Note No : $creditNo/$creditYear  saved successfully.";
	$response['creditNo']		= $creditNo;
	$response['creditYear']		= $creditYear;
	$response['butSave']		= $butSave['type'];
	$response['butApprove']		= $butApprove['type'];
	
	$db->commit();$db->disconnect();
	echo json_encode($response);
##########################################################################################################
}
elseif($requestType=='URLUpdate')
{
	$headerArray		= json_decode($_REQUEST['HeaderArray'],true);
	$detailArray		= json_decode($_REQUEST['DetailArray'],true);
	$glArray			= json_decode($_REQUEST['GLArray'],true);
	$invoiceArray		= explode('/',$headerArray['InvoiceNo']);
	$creditNo			= $headerArray['CreditNo'];
	$creditYear			= $headerArray['CreditYear'];
	$db->connect();$db->begin();

##########################################################################################################
## 1. CHECK EDIT PERMISSION.          																	##
##########################################################################################################
	$finance_supplier_creditnote_header->set($creditNo,$creditYear);
	$menupermision->set($programCode,$sessions->getUserId());	
	$status						= $finance_supplier_creditnote_header->getSTATUS();
	$approveLevel				= $finance_supplier_creditnote_header->getAPPROVE_LEVELS();	
	$result	= $menupermision->checkMenuPermision('Edit',$status	,$approveLevel);
	if(!$result['type'])
		throw new Exception($result['msg']);

##########################################################################################################
## 2. CHECK IS EXCHANGE RATE AVAILABLE FOR SELECTED DATE AND CURRENCY.								    ##
##########################################################################################################
	$result					= $mst_financeexchangerate->select("dblExcAvgRate",NULL,"intCurrencyId = ".$headerArray['CurrencyId']." AND dtmDate = '".$headerArray['CreditDate']."' AND intCompanyId = ".$sessions->getCompanyId(),NULL,NULL);
	$row					= mysqli_fetch_array($result);
	$exchangeRate			= $row['dblExcAvgRate'];
	if($exchangeRate == '')
		throw new Exception("Please enter exchange rates for ".$headerArray['CreditDate']);

##########################################################################################################
## 3. CHECK USER SAVED LOCATION WHEN APPROVE. 														    ##
##########################################################################################################
	if($finance_supplier_creditnote_header->getLOCATION_ID()!=$sessions->getLocationId())
		throw new Exception("Saved location and current location should be same.");
		
##########################################################################################################
## 4. CHECK PURCHASE INVOICE STATUS [ITS SHOULD BE APPROVED INVOICE].								    ##
##########################################################################################################
	$finance_supplier_purchaseinvoice_header->set($invoiceArray[0],$invoiceArray[1]);
	if($finance_supplier_purchaseinvoice_header->getSTATUS()!='1')
		throw new Exception("Selected 'Purchase Invoice No' not a valid invoice.You must select comfirm invoice.");

##########################################################################################################
## 5. CHECK MONTH END PROCESS LOCKED STATUS.								    						##
##########################################################################################################
	if($finance_month_end_process->getIsMonthEndProcessLocked($headerArray['CreditDate'],$sessions->getCompanyId()))
				throw new Exception("Month end Process locked for the date '".$headerArray['CreditDate']."' .");
						
##########################################################################################################
## 6. UPDATE `finance_supplier_creditnote_header` TABLE													##
##########################################################################################################
	$sys_approvelevels->set($programCode);
	$approveLevel	= $sys_approvelevels->getintApprovalLevel();
	$status			= $approveLevel+1;
	$finance_supplier_creditnote_header->set($creditNo,$creditYear);
	$finance_supplier_creditnote_header->setSTATUS($status);
	$finance_supplier_creditnote_header->setREMARKS($headerArray['Remarks']);
	$finance_supplier_creditnote_header->setAPPROVE_LEVELS($approveLevel);
	$finance_supplier_creditnote_header->setLAST_MODIFY_BY($sessions->getUserId());
	$finance_supplier_creditnote_header->setLAST_MODIFY_DATE($dateTimes->getCurruntDateTime());
	$finance_supplier_creditnote_header->commit();
	
##########################################################################################################
## 7. DELETE `finance_supplier_creditnote_details` TABLE.												##
##########################################################################################################
	$where		= 'CREDIT_NO = "'.$creditNo.'" AND CREDIT_YEAR = "'.$creditYear.'"';	
	$result = $finance_supplier_creditnote_details->delete($where);
	if(!$result['status'])
		throw new Exception($result['msg']);

##########################################################################################################
## 8. SAVE `finance_supplier_creditnote_details` TABLE.													##
##########################################################################################################
	$detailAmount		= 0;
	$booAvailable		= false;
	foreach($detailArray as $row)
	{
		$detailAmount  += round($row['AMOUNT'],2) + round($row['TAX_AMOUNT'],2);
		$booAvailable	= true;
		$taxId	= $row['TAX_CODE_ID']==''?'null':$row['TAX_CODE_ID'];
		$result = $finance_supplier_creditnote_details->insertRec($creditNo,$creditYear,$row['ITEM_ID'],$row['AMOUNT'],$row['TAX_AMOUNT'],$taxId,$row['COST_CENTER_ID']);
		if(!$result['status'])
			throw new Exception($result['msg']);
	}
	
	if(!$booAvailable)
		throw new Exception('No invoice details available to proceed.');

##########################################################################################################
## 9. DELETE `finance_supplier_creditnote_gl` TABLE.													##
##########################################################################################################
	$where		= 'CREDIT_NO = "'.$creditNo.'" AND CREDIT_YEAR = "'.$creditYear.'"';	
	$result = $finance_supplier_creditnote_gl->delete($where);
	if(!$result['status'])
		throw new Exception($result['msg']);

##########################################################################################################
## 10. SAVE `finance_supplier_creditnote_gl` TABLE.													    ##
##########################################################################################################
	$glAmount			= 0;
	$booAvailable		= false;
	foreach($glArray as $row)
	{
		$glAmount      += round($row['AMOUNT'],2);
		$booAvailable	= true;
		$result 		= $finance_supplier_creditnote_gl->insertRec($creditNo,$creditYear,$row['GL_ID'],$row['AMOUNT']);
		if(!$result['status'])
			throw new Exception($result['msg']);
	}
	
	if(!$booAvailable)
		throw new Exception('No general ledger details available to proceed.');

##########################################################################################################
## 11. GL AMOUNT & GRAND TOTAL AMOUNT VALIDATION.												    	##
##########################################################################################################
	if($detailAmount!=$glAmount)			
		throw new Exception('Grand Total and GL total amount should be equal.');	

##########################################################################################################
## 12. UPDATE MAX STATUS IN `finance_supplier_creditnote_approve_by` TABLE						    	##
##########################################################################################################
	$finance_supplier_creditnote_approve_by->updateMaxStatus($creditNo,$creditYear);
							
##########################################################################################################
## VALIDATE APPROVE BUTTON ONCE UPDATE 																	##
##########################################################################################################
	$finance_supplier_creditnote_header->set($creditNo,$creditYear);
	$menupermision->set($programCode,$sessions->getUserId());
	
	$status						= $finance_supplier_creditnote_header->getSTATUS();
	$approveLevel				= $finance_supplier_creditnote_header->getAPPROVE_LEVELS();	
	$butSave					= $menupermision->checkMenuPermision('Edit',$status,$approveLevel);
	$butApprove					= $menupermision->checkMenuPermision('Approve',$status,$approveLevel);
		
	$response['type'] 			= 'pass';
	$response['msg']			= "Credit Note No : $creditNo/$creditYear  updated successfully.";
	$response['creditNo']		= $creditNo;
	$response['creditYear']		= $creditYear;
	$response['butSave']		= $butSave['type'];
	$response['butApprove']		= $butApprove['type'];
	
	$db->commit();$db->disconnect();
	echo json_encode($response);
##########################################################################################################
}
elseif($requestType=='URLApprove')
{
	$creditNo		= $_REQUEST['CreditNo'];
	$creditYear		= $_REQUEST['CreditYear'];	

	$db->connect();$db->begin();	
	
##########################################################################################################
## 1. CHECK APPROVE PERMISSION.											   								##
##########################################################################################################
	$finance_supplier_creditnote_header->set($creditNo,$creditYear);
	$menupermision->set($programCode,$sessions->getUserId());
	$status				= $finance_supplier_creditnote_header->getSTATUS();
	$approveLevel		= $finance_supplier_creditnote_header->getAPPROVE_LEVELS();	
	$result	= $menupermision->checkMenuPermision('Approve',$status,$approveLevel);
	if(!$result['type'])
		throw new Exception($result['msg']);

##########################################################################################################
## 2. CHECK IS EXCHANGE RATE AVAILABLE FOR SELECTED DATE AND CURRENCY.								    ##
##########################################################################################################
	$result					= $mst_financeexchangerate->select("dblExcAvgRate",NULL,"intCurrencyId = ".$finance_supplier_creditnote_header->getCURRENCY_ID()." AND dtmDate = '".$dateTimes->getCurruntDate()."' AND intCompanyId = ".$sessions->getCompanyId(),NULL,NULL);
	$row					= mysqli_fetch_array($result);
	$exchangeRate			= $row['dblExcAvgRate'];
	if($exchangeRate == '')
		throw new Exception("Please enter exchange rates for ".$headerArray['CreditDate']);

##########################################################################################################
## 3. CHECK USER SAVED LOCATION WHEN APPROVE. 														    ##
##########################################################################################################
	if($finance_supplier_creditnote_header->getLOCATION_ID()!=$sessions->getLocationId())
		throw new Exception("Saved location and current location should be same.");

##########################################################################################################
## 4. CHECK PURCHASE INVOICE STATUS [ITS SHOULD BE APPROVED INVOICE].								    ##
##########################################################################################################
	$finance_supplier_purchaseinvoice_header->set($finance_supplier_creditnote_header->getPURCHASE_INVOICE_NO(),$finance_supplier_creditnote_header->getPURCHASE_INVOICE_YEAR());
	if($finance_supplier_purchaseinvoice_header->getSTATUS()!='1')
		throw new Exception("Selected 'Purchase Invoice No' not a valid invoice.You must select comfirm invoice.");

##########################################################################################################
## 5. CHECK MONTH END PROCESS LOCKED STATUS.								    						##
##########################################################################################################
	if($finance_month_end_process->getIsMonthEndProcessLocked($finance_supplier_creditnote_header->getCREDIT_DATE(),$sessions->getCompanyId()))
				throw new Exception("Month end Process locked for the date '".$finance_supplier_creditnote_header->getCREDIT_DATE()."' .");
				
##########################################################################################################
## 6. UPDATE APPROVE HEADER STATUS.																		##
##########################################################################################################
	$finance_supplier_creditnote_header->setSTATUS($finance_supplier_creditnote_header->getSTATUS()-1);
	$finance_supplier_creditnote_header->commit();

##########################################################################################################
## 7. INSERT APPROVED BY TABLE 											   								##
##########################################################################################################
	$finance_supplier_creditnote_header->set($creditNo,$creditYear);
	$status		= $finance_supplier_creditnote_header->getSTATUS();
	$level		= $finance_supplier_creditnote_header->getAPPROVE_LEVELS();	
	$approval	= $level+1-$status;
	
	$result_arr	= $finance_supplier_creditnote_approve_by->insertRec($creditNo,$creditYear,$approval,$sessions->getUserId(),$dateTimes->getCurruntDateTime(),0);
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
						
##########################################################################################################
## 8. FINAL APPROVAL                   																    ##
##########################################################################################################
	if($finance_supplier_creditnote_header->getSTATUS()=='1')
		updateTransactionTable($creditNo,$creditYear);
			
##########################################################################################################
	$response['type'] 			= 'pass';
	$response['msg']			= "Credit Note No : $creditNo/$creditYear  approved successfully.";

	$db->commit();$db->disconnect();
	echo json_encode($response);
##########################################################################################################
}
elseif($requestType=='URLReject')
{
	$creditNo		= $_REQUEST['CreditNo'];
	$creditYear		= $_REQUEST['CreditYear'];	

	$db->connect();$db->begin();	
	
##########################################################################################################
## 1. CHECK REJECT PERMISSION. 											   								##
##########################################################################################################
	$finance_supplier_creditnote_header->set($creditNo,$creditYear);
	$menupermision->set($programCode,$sessions->getUserId());
	$status				= $finance_supplier_creditnote_header->getSTATUS();
	$approveLevel		= $finance_supplier_creditnote_header->getAPPROVE_LEVELS();	
	$result	= $menupermision->checkMenuPermision('Reject',$status,$approveLevel);
	if(!$result['type'])
		throw new Exception($result['msg']);
		
##########################################################################################################
## 2. CHECK USER SAVED LOCATION WHEN REJECT. 														    ##
##########################################################################################################
	if($finance_supplier_creditnote_header->getLOCATION_ID()!=$sessions->getLocationId())
		throw new Exception("Saved location and current location should be same.");

##########################################################################################################
## 3. UPDATE REJECT HEADER STATUS.																		##
##########################################################################################################
	$finance_supplier_creditnote_header->setSTATUS(0);
	$finance_supplier_creditnote_header->commit();
	
##########################################################################################################
## 4. INSERT APPROVED BY TABLE 											   								##
##########################################################################################################
	$result_arr	= $finance_supplier_creditnote_approve_by->insertRec($creditNo,$creditYear,0,$sessions->getUserId(),$dateTimes->getCurruntDateTime(),0);
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
					
##########################################################################################################
	$response['type'] 			= 'pass';
	$response['msg']			= "Credit Note No : $creditNo/$creditYear  reject successfully.";

	$db->commit();$db->disconnect();
	echo json_encode($response);
##########################################################################################################
}
elseif($requestType=='URLCancel')
{
	$creditNo		= $_REQUEST['CreditNo'];
	$creditYear		= $_REQUEST['CreditYear'];	

	$db->connect();$db->begin();

##########################################################################################################
## 1. CHECK CANCEL PERMISSION. 											   								##
##########################################################################################################	
	$finance_supplier_creditnote_header->set($creditNo,$creditYear);
	$menupermision->set($programCode,$sessions->getUserId());
	$status				= $finance_supplier_creditnote_header->getSTATUS();
	$approveLevel		= $finance_supplier_creditnote_header->getAPPROVE_LEVELS();	
	$permissionArr		= $menupermision->checkMenuPermision('Cancel',$status,$approveLevels);
	
	if(!$permissionArr['type'])
		throw new Exception($permissionArr['msg']);

##########################################################################################################
## 2. CHECK MONTH END PROCESS LOCKED STATUS.								    						##
##########################################################################################################
	if($finance_month_end_process->getIsMonthEndProcessLocked($finance_supplier_creditnote_header->getCREDIT_DATE(),$sessions->getCompanyId()))
				throw new Exception("Month end Process locked for the date '".$finance_supplier_creditnote_header->getCREDIT_DATE()."' .");
						
##########################################################################################################
## 3. CHECK USER SAVED LOCATION WHEN CANCEL. 														    ##
##########################################################################################################
	if($finance_supplier_creditnote_header->getLOCATION_ID()!=$sessions->getLocationId())
		throw new Exception("Saved location and current location should be same.");

##########################################################################################################
## 4. CHECK IS INVOICE BALANCE AMOUNT AVAILABLE.
##########################################################################################################
	$result  = $finance_supplier_transaction->getInvoiceTransactionDetails($finance_supplier_creditnote_header->getPURCHASE_INVOICE_NO(),$finance_supplier_creditnote_header->getPURCHASE_INVOICE_YEAR());
	$result  = mysqli_fetch_array($result);
	$result1 = $finance_supplier_creditnote_details->getTotalAmount($creditNo,$creditYear);
	$result1 = mysqli_fetch_array($result1);
	
	if($result['toBeDebited'] < $result1['TOTAL_AMOUNT'])
		throw new Exception("No enough invoice balance available to cancel.");
		
##########################################################################################################
## 5. UPDATE CANCEL HEADER STATUS.																		##
##########################################################################################################
	$finance_supplier_creditnote_header->setSTATUS('-2');
	$finance_supplier_creditnote_header->commit();

##########################################################################################################
## 6. INSERT APPROVED BY TABLE.											   								##
##########################################################################################################
	$result_arr	= $finance_supplier_creditnote_approve_by->insertRec($creditNo,$creditYear,'-2',$sessions->getUserId(),$dateTimes->getCurruntDateTime(),0);
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);

##########################################################################################################
## 7. ROLLBACK RECORDS FROM `finance_supplier_transaction` TABLE.				   						##
##########################################################################################################
	$where	= "DOCUMENT_NO = '".$creditNo."' AND
		       DOCUMENT_YEAR = '".$creditYear."' AND
	           DOCUMENT_TYPE = 'CREDIT' ";						
	$result_arr = $finance_supplier_transaction->delete($where);
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
		
##########################################################################################################
## 8. ROLLBACK RECORDS FROM `finance_transaction` TABLE.			   									##
##########################################################################################################
	$where	= "DOCUMENT_NO = '".$creditNo."' AND
		       DOCUMENT_YEAR = '".$creditYear."' AND
	           DOCUMENT_TYPE = 'CREDIT' AND
			   TRANSACTION_CATEGORY = 'SU'";						
	$result_arr = $finance_transaction->delete($where);
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
				
##########################################################################################################
	$response['type'] 			= 'pass';
	$response['msg']			= "Credit Note No : $creditNo/$creditYear  canceled successfully.";

	$db->commit();$db->disconnect();
	echo json_encode($response);
##########################################################################################################				
}
elseif($requestType=='URLLoadCurrency')
{
	$response['html']	 = "";
	$result = $finance_supplier_purchaseinvoice_header->getSupplierWiseCurrency($_REQUEST['SupplierId'],$sessions->getCompanyId());
	while($row=mysqli_fetch_array($result))
	{
		$response['html'] .= "<option value=\"".$row['CURRENCY_ID']."\">".$row['CURRENCY_CODE']."</option>";
	}
	echo json_encode($response);
}
elseif($requestType=='URLLoadInvoiceNo')
{
	$response['html'] .= "<option value=\"".''."\">&nbsp;</option>";
	$result = $finance_supplier_purchaseinvoice_header->getSupplierCurrencyWiseInvoiceNo($_REQUEST['SupplierId'],$_REQUEST['CurrencyId'],$sessions->getCompanyId());
	while($row=mysqli_fetch_array($result))
	{
		$response['html'] .= "<option value=\"".$row['PURCHASE_INVOICE_NO'].'/'.$row['PURCHASE_INVOICE_YEAR']."\">".$row['PURCHASE_INVOICE_NO'].'/'.$row['PURCHASE_INVOICE_YEAR'].' - '.$row['INVOICE_NO']."</option>";
	}
	
	if($mst_financeexchangerate->set($_REQUEST['CurrencyId'],$_REQUEST['Date'],$sessions->getCompanyId()))
		$response['ExchangeRate']	= $mst_financeexchangerate->getdblExcAvgRate();
	else
		$response['ExchangeRate']	= 0.00;
	echo json_encode($response);
}
elseif($requestType=='URLLoadMainGrid')
{	
	$invoiceArray	= explode('/',$_REQUEST['InvoiceNo']);
	

##########################################################################################################	
## LOAD AMOUNTS GRID DETAILS																			##
##########################################################################################################	
	$result = $finance_supplier_transaction->getInvoiceTransactionDetails($invoiceArray[0],$invoiceArray[1]);
    $html1	= '';
	while($row=mysqli_fetch_array($result))
	{
        $html1 .= "<tr align=\"center\">".
								"<td class=\"cls_td_date\">".$row['INVOICE_DATE']."</td>".
								"<td class=\"cls_td_invoiced\"><a target=\"rptPurchaseInvoice.php\" href=\"?q=893&purInvoiceNo=".$invoiceArray[0]."&purInvoiceYear=".$invoiceArray[1]."\">".$row['invoiceAmount']."</a></td>".
								"<td class=\"cls_td_advanced\">".$row['advancedAmount']."</td>".
								"<td class=\"cls_td_credit\">".$row['creditAmount']."</td>".
								"<td class=\"cls_td_debit\">".$row['debitAmount']."</td>".
								"<td class=\"cls_td_payment\">".$row['paidAmount']."</td>".
							  "</tr>";
	}
	
##########################################################################################################	
## LOAD MAIN GRID DETAILS																				##
##########################################################################################################	
	$result = $finance_supplier_purchaseinvoice_details->getInvoiceDetails($invoiceArray[0],$invoiceArray[1]);
    $html	= '';
	$i=0;
	while($row=mysqli_fetch_array($result))
	{
		$i++;
		$mst_maincategory->set($row['MAIN_CAT_ID']);
		$mst_subcategory->set($row['SUB_CAT_ID']);
		$mst_units->set($row['UNIT_ID']);
		$creditedAmount	= $finance_supplier_creditnote_details->getApprovedInvoiceAmount($invoiceArray[0],$invoiceArray[1]);

        $html .= "<tr id=\"".$row['GLACCOUNT_ID']."\">".
							  "<td class=\"cls_td_checkbox\" style=\"text-align:center\"><input type=\"checkbox\" id=\"chkCheck\" name=\"chkCheck\" class=\"validate[minCheckbox[1]] cls_check\" ></td>".
							  "<td class=\"cls_td_mainCat\" style=\"text-align:left\">".$mst_maincategory->getstrName()."</td>".
							  "<td class=\"cls_td_subCat\" style=\"text-align:left\">".$mst_subcategory->getstrName()."</td>".
							  "<td class=\"cls_td_item\" style=\"text-align:left\" id=\"".$row['ITEM_ID']."\">".$row['ITEM_NAME']."</td>".
							  "<td class=\"cls_td_unit\" style=\"text-align:left\">".$mst_units->getstrName()."</td>".
							  "<td class=\"cls_td_invoiced\" style=\"text-align:right\">".$row['AMOUNT']."</td>".
							  "<td class=\"cls_td_credited\" style=\"text-align:right\">".$creditedAmount."</td>".
							  "<td class=\"cls_td_amount\" style=\"text-align:right\"><input type=\"text\" id=\"txtAmount".$i."\" name=\"txtAmount".$i."\" class=\"cls_calculateAmount validate[required,decimal[2],custom[number]] cls_input_number_validation\" style=\"width:85px;text-align:right\" value=\"0.00\" disabled=\"disabled\"></td>".
							  "<td class=\"cls_td_tax\" style=\"text-align:center\" id=\"0.00\" title=\"".$row['TAX_GL_ID']."\"><select class=\"cls_tax\" style=\"width:100%\" >".$mst_financetaxgroup->getCombo($row['TAX_CODE'])."</select></td>".
							  "<td class=\"cls_td_costCenter\" style=\"text-align:center\"><select style=\"width:100%\" disabled=\"disabled\">".$mst_financedimension->getCombo($row['COST_CENTER'])."</select></td>".
							"</tr>";
	}
	
##########################################################################################################	
## LOAD MAIN GRID DETAILS																				##
##########################################################################################################	
	$result = $finance_supplier_purchaseinvoice_gl->getInvoiceWiseGLDetails($invoiceArray[0],$invoiceArray[1]);
	$html2	= '';

	while($row=mysqli_fetch_array($result))
	{
        $html2 .= "<tr>".
								"<td class=\"cls_td_delete\" width=\"9%\" style=\"text-align:center\"><img id=\"butDelete\" name=\"butDelete\" border=\"0\" src=\"images/del.png\" alt=\"delete\"/></td>".
								"<td class=\"cls_td_gl\" width=\"60%\" id=\"".$row['GLACCOUNT_ID']."\"><select class=\"validate[required]\" style=\"width:100%\" >".$finance_mst_chartofaccount->getCombo($row['GLACCOUNT_ID'])."</select></td>".
								"<td class=\"cls_td_amount\" width=\"31%\" style=\"text-align:center\"><input type=\"text\"class=\"validate[required,decimal[2],custom[number]] cls_amount cls_input_number_validation\" style=\"width:100%;text-align:right\" value=\"0.00\"/></td>".
							  "</tr>";
	}


    $response = array('html' =>$html, 'html1' => $html1, 'html2' => $html2);
	echo json_encode($response);
}
}catch(Exception $e){	
	switch ($e->getCode()){
		case 0:
			$db->rollback();		
			$response['msg'] 	=  $e->getMessage();
			$response['error'] 	=  $error_handler->jTraceEx($e);
			$response['type'] 	=  'fail';
			$response['sql']	=  $db->getSql();
			break;		
	}
	echo json_encode($response);
}

function updateTransactionTable($creditNo,$creditYear)
{
	global $finance_supplier_transaction;
	global $finance_supplier_creditnote_header;
	global $sessions;
	global $dateTimes;
	global $finance_transaction;
	global $finance_mst_chartofaccount;
	
	$amount	= 0;
	
	$cols	= " trn_poheader.intPOYear                                   AS PO_YEAR,
				trn_poheader.intPONo                                     AS PO_NO,
				finance_supplier_creditnote_header.SUPPLIER_ID           AS SUPPLIER_ID,
				finance_supplier_creditnote_header.CURRENCY_ID           AS CURRENCY_ID,
				finance_supplier_creditnote_header.CREDIT_YEAR           AS DOCUMENT_YEAR,
				finance_supplier_creditnote_header.CREDIT_NO             as DOCUMENT_NO,
				'CREDIT'                                                 AS DOCUMENT_TYPE,
				finance_supplier_creditnote_header.PURCHASE_INVOICE_YEAR AS INVOICE_YEAR,
				finance_supplier_creditnote_header.PURCHASE_INVOICE_NO   AS INVOICE_NO,
				finance_supplier_creditnote_gl.CHART_OF_ACCOUNT_ID       AS GL_ID,
				finance_supplier_creditnote_gl.AMOUNT                    AS AMOUNT,
				finance_supplier_creditnote_header.COMPANY_ID            AS COMPANY_ID,
				finance_supplier_creditnote_header.LOCATION_ID           AS LOCATION_ID,
				finance_supplier_creditnote_header.CREDIT_DATE           AS CREDIT_DATE";
	
	$join 	= "INNER JOIN finance_supplier_creditnote_gl
				 ON finance_supplier_creditnote_header.CREDIT_NO = finance_supplier_creditnote_gl.CREDIT_NO
				   AND finance_supplier_creditnote_header.CREDIT_YEAR = finance_supplier_creditnote_gl.CREDIT_YEAR
			   INNER JOIN finance_supplier_purchaseinvoice_header
				 ON finance_supplier_purchaseinvoice_header.PURCHASE_INVOICE_NO = finance_supplier_creditnote_header.PURCHASE_INVOICE_NO
				   AND finance_supplier_purchaseinvoice_header.PURCHASE_INVOICE_YEAR = finance_supplier_creditnote_header.PURCHASE_INVOICE_YEAR
			   INNER JOIN ware_grnheader
				 ON ware_grnheader.strInvoiceNo = finance_supplier_purchaseinvoice_header.INVOICE_NO
			   INNER JOIN trn_poheader
				 ON trn_poheader.intPONo = ware_grnheader.intPoNo
				   AND trn_poheader.intPOYear = ware_grnheader.intPOYear
				   AND finance_supplier_creditnote_header.SUPPLIER_ID = trn_poheader.intSupplier
				";
	
	$where 	= "finance_supplier_creditnote_header.CREDIT_NO = '$creditNo'
    			AND finance_supplier_creditnote_header.CREDIT_YEAR = '$creditYear'";
	
	$result = $finance_supplier_creditnote_header->select($cols,$join,$where);
	while($row=mysqli_fetch_array($result))
	{
		$result1 = $finance_supplier_transaction->insertRec($row['PO_YEAR'],$row['PO_NO'],$row['SUPPLIER_ID'],$row['CURRENCY_ID'],$row['DOCUMENT_YEAR'],$row['DOCUMENT_NO'],$row['DOCUMENT_TYPE'],$row['INVOICE_YEAR'],$row['INVOICE_NO'],$row['GL_ID'],$row['AMOUNT'],$row['COMPANY_ID'],$row['LOCATION_ID'],$sessions->getUserId(),$row['CREDIT_DATE']);
		if(!$result1['status'])
			throw new Exception($result1['msg']);
			
		$result2	= $finance_transaction->insertRec($row['GL_ID'],$row['AMOUNT'],$row['DOCUMENT_NO'],$row['DOCUMENT_YEAR'],$row['DOCUMENT_TYPE'],$row['INVOICE_NO'],$row['INVOICE_YEAR'],'D','SU',$row['SUPPLIER_ID'],$row['CURRENCY_ID'],'null','null',0,$row['LOCATION_ID'],$row['COMPANY_ID'],$sessions->getUserId(),$row['CREDIT_DATE'],$dateTimes->getCurruntDateTime());
		if(!$result2['status'])
			throw new Exception($result2['msg']);
			
		$amount	       += round($row['AMOUNT'],2);
		$suppId			= $row['SUPPLIER_ID'];
		$currId			= $row['CURRENCY_ID'];
		$invoNo			= $row['INVOICE_NO'];
		$invoYear		= $row['INVOICE_YEAR'];
		$locId			= $row['LOCATION_ID'];
		$compId			= $row['COMPANY_ID'];
		$creditDate		= $row['CREDIT_DATE'];
	}
	
	$glId	= $finance_mst_chartofaccount->getCatogoryTypeWiseGL('S',$suppId);
	$result	= $finance_transaction->insertRec($glId,$amount,$creditNo,$creditYear,'CREDIT',$invoNo,$invoYear,'C','SU',$suppId,$currId,'null','null',0,$locId,$compId,$sessions->getUserId(),$creditDate,$dateTimes->getCurruntDateTime());
		if(!$result['status'])
			throw new Exception($result['msg']);
	
}
?>
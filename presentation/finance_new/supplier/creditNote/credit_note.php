<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
#BEGIN  - INCLUDE FILES & CREATE CLASS OBJECTS { 
require_once "class/tables/finance_supplier_purchaseinvoice_header.php";		$finance_supplier_purchaseinvoice_header		= new finance_supplier_purchaseinvoice_header($db);
require_once "class/tables/finance_supplier_purchaseinvoice_details.php";		$finance_supplier_purchaseinvoice_details		= new finance_supplier_purchaseinvoice_details($db);
include_once "class/tables/menupermision.php";									$menupermision 									= new menupermision($db);
require_once "class/tables/finance_supplier_creditnote_header.php";				$finance_supplier_creditnote_header				= new finance_supplier_creditnote_header($db);
require_once "class/tables/finance_supplier_creditnote_details.php";			$finance_supplier_creditnote_details			= new finance_supplier_creditnote_details($db);
require_once "class/tables/finance_supplier_creditnote_gl.php";					$finance_supplier_creditnote_gl					= new finance_supplier_creditnote_gl($db);
require_once "class/tables/finance_supplier_transaction.php";					$finance_supplier_transaction					= new finance_supplier_transaction($db);
require_once "class/tables/mst_maincategory.php";								$mst_maincategory								= new mst_maincategory($db);
require_once "class/tables/mst_subcategory.php";								$mst_subcategory								= new mst_subcategory($db);
require_once "class/tables/mst_units.php";										$mst_units										= new mst_units($db);
require_once "class/tables/mst_financetaxgroup.php";							$mst_financetaxgroup							= new mst_financetaxgroup($db);
require_once "class/tables/mst_financedimension.php";							$mst_financedimension							= new mst_financedimension($db);
require_once "class/tables/finance_mst_chartofaccount.php";						$finance_mst_chartofaccount						= new finance_mst_chartofaccount($db);
require_once "class/tables/menus_special.php";									$menus_special									= new menus_special($db);
#END 	}

#BEGIN  - ASSIGN PARAMETERS {
$programCode		= 'P1084'; 
$creditNo			= $_REQUEST['CreditNo'];
$creditYear			= $_REQUEST['CreditYear'];
#END 	}

#BEGIN 	- {
$menupermision->set($programCode,$sessions->getUserId());
$dateSP		= $menus_special->loadSpecialMenuPermission(65,$sessions->getUserId());
#END	}

#BEGIN	- BUTTON PERMISSION VALIDATION {
if(isset($_REQUEST['CreditNo']))
{	
	$finance_supplier_creditnote_header->set($creditNo,$creditYear);
	$status				= $finance_supplier_creditnote_header->getSTATUS();
	$levels				= $finance_supplier_creditnote_header->getAPPROVE_LEVELS();	
	$butSave			= $menupermision->checkMenuPermision('Edit',$status,$levels);
	$butApprove			= $menupermision->checkMenuPermision('Approve',$status,$levels);
	$butReject			= $menupermision->checkMenuPermision('Approve',$status,$levels);
	$butCancel			= $menupermision->checkMenuPermision('Cancel',$status,$levels);
	
	$invoiceNo			= $finance_supplier_creditnote_header->getPURCHASE_INVOICE_NO();
	$invoiceYear		= $finance_supplier_creditnote_header->getPURCHASE_INVOICE_YEAR();
	$concatInvoiceNo	= $invoiceNo.'/'.$invoiceYear;
	$supplierId			= $finance_supplier_creditnote_header->getSUPPLIER_ID();
	$currencyId			= $finance_supplier_creditnote_header->getCURRENCY_ID();
	$creditDate			= $finance_supplier_creditnote_header->getCREDIT_DATE();
	$remarks			= $finance_supplier_creditnote_header->getREMARKS();
}
else
{
	$invoiceArray		= explode('-',$_REQUEST['InvoiceNo']);
	$concatInvoiceNo	= $invoiceArray[1].'/'.$invoiceArray[0];
	$currencyId			= $_REQUEST['CurrencyId'];
	$supplierId			= $_REQUEST['SupplierId'];
	$butSave			= $menupermision->checkMenuPermision('Edit','null','null');
	$butApprove			= $menupermision->checkMenuPermision('Approve','null','null');
	$butReject			= $menupermision->checkMenuPermision('Approve','null','null');
	$butCancel			= $menupermision->checkMenuPermision('Cancel','null','null');
}
#END 	}
?>
<script type="text/javascript" src="presentation/finance_new/supplier/creditNote/credit_note_js.js"></script>
<?php if(isset($_REQUEST['InvoiceNo'])){?>
<body onLoad="loadMainGrid()">
<?php } ?>
<title>Supplier Credit Note</title>
<form id="frmCreditNote" name="frmCreditNote"  method="post">
  <div align="center">
    <div class="trans_layoutL" style="width:1100px">
      <div class="trans_text">Supplier Credit Note</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" align="center" bgcolor="#FFFFFF">
              <tr>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" cellspacing="2" cellpadding="1">
                  <tr>
                    <td width="18%">Credit Note No</td>
                    <td width="48%"><input name="txtCreditNo" type="text" disabled="disabled" id="txtCreditNo" style="width:60px" value="<?php echo $creditNo; ?>" />
                      <input name="txtCreditYear" type="text" disabled="disabled" id="txtCreditYear" style="width:35px" value="<?php echo $creditYear; ?>" /></td>
                    <td width="22%">Date</td>
                    <td width="12%"><input name="txtDate" type="text" value="<?php echo($creditDate==''?date("Y-m-d"):$creditDate); ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtDate" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($dateSP!=1?'disabled="disabled"':''); ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;width:2px"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                  </tr>
                  <tr>
                    <td>Supplier <span class="compulsoryRed">*</span></td>
                    <td><select name="cboSupplier" id="cboSupplier"  style="width:240px" class="validate[required]" >
                      <option value="">&nbsp;</option>
                      <?php
					$supplier_result 		= $finance_supplier_purchaseinvoice_header->getConfirmSupplier($sessions->getCompanyId());
					while($row=mysqli_fetch_array($supplier_result))
					{
						if($row['SUPPLIER_ID']==$supplierId)						
							echo "<option value=\"".$row['SUPPLIER_ID']."\" selected=\"selected\">".$row['SUPPLIER_NAME']."</option>";
						else
							echo "<option value=\"".$row['SUPPLIER_ID']."\">".$row['SUPPLIER_NAME']."</option>";
					}
					?>
                    </select></td>
                    <td>Currency <span class="compulsoryRed">*</span></td>
                    <td><select name="cboCurrency" id="cboCurrency"  style="width:104px" class="validate[required] cls_cbo_CurrencyCalExchangeRate" >
                      <option value="">&nbsp;</option>
                      <?php 
					$result = $finance_supplier_purchaseinvoice_header->getSupplierWiseCurrency($supplierId,$sessions->getCompanyId());
					while($row=mysqli_fetch_array($result))
					{
						if($row['CURRENCY_ID']==$currencyId)		
							echo "<option value=\"".$row['CURRENCY_ID']."\" selected=\"selected\">".$row['CURRENCY_CODE']."</option>";
						else
							echo "<option value=\"".$row['CURRENCY_ID']."\">".$row['CURRENCY_CODE']."</option>";
					}
					echo json_encode($response);
					?>
                    </select></td>
                  </tr>
                  <tr>
                    <td>Invoice No <span class="compulsoryRed">*</span></td>
                    <td><select name="cboInvoiceNo" id="cboInvoiceNo"  style="width:240px" class="validate[required]" >
                      <option value="">&nbsp;</option>
                      <?php
					$result = $finance_supplier_purchaseinvoice_header->getSupplierCurrencyWiseInvoiceNo($supplierId,$currencyId,$sessions->getCompanyId());
					while($row=mysqli_fetch_array($result))
					{
						if($row['CONCAT_INVOICE_NO']==$concatInvoiceNo)
							echo "<option value=\"".$row['PURCHASE_INVOICE_NO'].'/'.$row['PURCHASE_INVOICE_YEAR']."\" selected=\"selected\">".$row['PURCHASE_INVOICE_NO'].'/'.$row['PURCHASE_INVOICE_YEAR'].' - '.$row['INVOICE_NO']."</option>";
						else
							echo "<option value=\"".$row['PURCHASE_INVOICE_NO'].'/'.$row['PURCHASE_INVOICE_YEAR']."\">".$row['PURCHASE_INVOICE_NO'].'/'.$row['PURCHASE_INVOICE_YEAR'].' - '.$row['INVOICE_NO']."</option>";
					}
					?>
                    </select></td>
                    <td>Exchange Rate</td>
                    <td><input type="text" name="txtExRate" id="txtExRate" style="width:100px;text-align:right" disabled="disabled" value="0.00" class="cls_txt_exchangeRate"/></td>
                  </tr>
                  <tr>
                    <td valign="top">Remark</td>
                    <td><textarea name="txtRemarks" id="txtRemarks" style="width:240px;height:40px"><?php echo (!isset($remarks)?'':$remarks); ?></textarea></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="bordered" id="tblAmount">
                    <thead>
                      <tr>
                        <th width="200" class="normalfnt"><b>Invoice Date</b></th>
                        <th width="200" class="normalfnt"><b>Invoiced Amount</b></th>
                        <th width="200" class="normalfnt"><b>Advanced Amount</b></th>
                        <th width="200" class="normalfnt"><b>Credit Note Amount</b></th>
                        <th width="200" class="normalfnt"><b>Debit Note Amount</b></th>
                        <th width="200" class="normalfnt"><b>Payment Amount</b></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
					$result = $finance_supplier_transaction->getInvoiceTransactionDetails($invoiceNo,$invoiceYear);
					$row 	= mysqli_fetch_array($result);
					?>
                      <tr align="center">
                        <td class="cls_td_date"><?php echo ($row['INVOICE_DATE']==''?'-':$row['INVOICE_DATE'])?></td>
                        <td class="cls_td_invoiced"><a target="rptPurchaseInvoice.php" href="?q=893&purInvoiceNo=<?php echo $invoiceNo?>&purInvoiceYear=<?php echo $invoiceYear?>"><?php echo ($row['invoiceAmount']==''?'0.00':$row['invoiceAmount'])?></a></td>
                        <td class="cls_td_advanced"><?php echo ($row['advancedAmount']==''?'0.00':$row['advancedAmount'])?></td>
                        <td class="cls_td_credit"><?php echo ($row['creditAmount']==''?'0.00':$row['creditAmount'])?></td>
                        <td class="cls_td_debit"><?php echo ($row['debitAmount']==''?'0.00':$row['debitAmount'])?></td>
                        <td class="cls_td_payment"><?php echo ($row['paidAmount']==''?'0.00':$row['paidAmount'])?></td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <td colspan="4" class="normalfnt">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="4" class="normalfnt"><div style="overflow:scroll;overflow-x:hidden;width:1100px;height:250px;" id="divGrid">
                    <table width="100%" class="bordered" id="tblMain" >
                      <thead>
                        <tr>
                          <th width="2%" height="30" ><input class="cls_checkAll" type="checkbox" id="chkAll" name="chkAll" /></th>
                          <th width="13%" >Main Category</th>
                          <th width="12%" >Sub Category</th>
                          <th width="20%" >Item</th>
                          <th width="5%" >UOM</th>
                          <th width="9%" >Invoice Amount</th>
                          <th width="9%" >Credited Amount</th>
                          <th width="9%" >Credit Amount</th>
                          <th width="10%" >Tax</th>
                          <th width="11%" >Cost Center</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
					  	$i	= 0;
						$subTotal	= 0;
						$taxTotal	= 0;
						$grandTotal	= 0;
						$result = $finance_supplier_creditnote_details->getSavedDetails($creditNo,$creditYear);
						while($row = mysqli_fetch_array($result))
						{
							$i++;
							$mst_maincategory->set($row['MAIN_CAT_ID']);
							$mst_subcategory->set($row['SUB_CAT_ID']);
							$mst_units->set($row['UNIT_ID']);
							
							$invoiced		= $finance_supplier_purchaseinvoice_details->getItemWiseApprovedInvoiceValur($invoiceNo,$invoiceYear,$row['ITEM_ID']);
							$creditedAmount	= $finance_supplier_creditnote_details->getApprovedInvoiceAmount($invoiceNo,$invoiceYear);
							
							echo  "<tr id=\"".$row['GLACCOUNT_ID']."\">".
									"<td class=\"cls_td_checkbox\" style=\"text-align:center\"><input type=\"checkbox\" id=\"chkCheck\" name=\"chkCheck\" class=\"validate[minCheckbox[1]] cls_check\" checked=\"checked\"></td>".
									"<td class=\"cls_td_mainCat\" style=\"text-align:left\">".$mst_maincategory->getstrName()."</td>".
									"<td class=\"cls_td_subCat\" style=\"text-align:left\">".$mst_subcategory->getstrName()."</td>".
									"<td class=\"cls_td_item\" style=\"text-align:left\" id=\"".$row['ITEM_ID']."\">".$row['ITEM_NAME']."</td>".
									"<td class=\"cls_td_unit\" style=\"text-align:left\">".$mst_units->getstrName()."</td>".
									"<td class=\"cls_td_invoiced\" style=\"text-align:right\">".$invoiced."</td>".
							  		"<td class=\"cls_td_credited\" style=\"text-align:right\">".$creditedAmount."</td>".
							  		"<td class=\"cls_td_amount\" style=\"text-align:center\"><input type=\"text\" id=\"txtAmount".$i."\" name=\"txtAmount".$i."\" class=\"cls_calculateAmount validate[required,decimal[2],custom[number]] cls_input_number_validation\" style=\"width:85px;text-align:right\" value=\"".$row['AMOUNT']."\" ></td>".
									"<td class=\"cls_td_tax\" style=\"text-align:center\" id=\"".$row['TAX_AMOUNT']."\" title=\"".$row['TAX_GL_ID']."\"><select class=\"cls_tax\" style=\"width:100%\">".$mst_financetaxgroup->getCombo($row['TAX_GROUP_ID'])."</select></td>".
									"<td class=\"cls_td_costCenter\" style=\"text-align:center\"><select style=\"width:100%\" disabled=\"disabled\">".$mst_financedimension->getCombo($row['COST_CENTER_ID'])."</select></td>".
								  "</tr>";
							$subTotal	+= $row['AMOUNT'];
							$taxTotal	+= $row['TAX_AMOUNT'];
							
						}
							$grandTotal	= $subTotal + $taxTotal;
						?>
                      </tbody>
                    </table>
                </div></td>
              </tr>
              <tr>
                <td width="64%" colspan="2" valign="top" class="normalfnt"><table style="width:450px" class="bordered" id="tblGL" >
                    <thead>
                      <tr>
                        <th colspan="3" >GL Allocation <div style="float:right" class="white button small" id="butAddNewGL">Add New GL</div></th>
                      </tr>
                      <tr>
                        <th width="6%" >Del</th>
                        <th width="69%" >GL Account <span class="compulsoryRed">*</span></th>
                        <th width="25%" >Amount <span class="compulsoryRed">*</span></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
					$glTotal	= 0;
					$result = $finance_supplier_creditnote_gl->getSavedDetails($creditNo,$creditYear);
					$response['html2']	= '';
					while($row=mysqli_fetch_array($result))
					{
						echo "<tr>".
								"<td class=\"cls_td_delete\" width=\"9%\" style=\"text-align:center\"><img id=\"butDelete\" name=\"butDelete\" border=\"0\" src=\"images/del.png\" alt=\"delete\"/></td>".
								"<td class=\"cls_td_gl\" width=\"60%\" id=\"".$row['CHART_OF_ACCOUNT_ID']."\"><select class=\"validate[required]\" style=\"width:100%\">".$finance_mst_chartofaccount->getCombo($row['CHART_OF_ACCOUNT_ID'])."</select></td>".
								"<td class=\"cls_td_amount\" width=\"31%\" style=\"text-align:center\"><input type=\"text\" class=\"validate[required,decimal[2],custom[number]] cls_amount cls_input_number_validation\" style=\"width:100%;text-align:right\" value=\"".$row['AMOUNT']."\"/></td>".
							 "</tr>";
							 $glTotal	+= $row['AMOUNT'];
					}
					?>
                    </tbody>
                    <tfoot>
                    	<tr style="font-weight:bold">
                    	<td colspan="2" style="text-align:right">TOTAL</td>
                      	<td style="text-align:right;" class="cls_td_total"><?php echo number_format($glTotal,2)?></td>
                        </tr>
                   	</tfoot>
                  </table></td>
                <td width="16%" class="normalfnt" style="text-align:right">&nbsp;</td>
                <td width="20%" class="normalfnt" ><table width="100%" border="0">
                    <tr>
                      <td width="50%" class="normalfnt" style="text-align:right">Sub Total</td>
                      <td width="14%" class="normalfnt" style="text-align:right">&nbsp;</td>
                      <td width="36%" class="normalfnt"><span class="normalfnt" style="text-align:right">
                        <input name="txtSubTotal" type="text" disabled="disabled" id="txtSubTotal" style="width:100px;text-align:right" value="<?php echo number_format($subTotal, 2, '.', ''); ?>" />
                        </span></td>
                    </tr>
                    <tr>
                      <td class="normalfnt" style="text-align:right">Tax Total</td>
                      <td class="normalfnt" style="text-align:right">&nbsp;</td>
                      <td class="normalfnt"><span class="normalfnt" style="text-align:right">
                        <input name="txtTaxTotal" type="text" disabled="disabled" id="txtTaxTotal" style="width:100px;text-align:right" value="<?php echo number_format($taxTotal, 2, '.', ''); ?>" />
                        </span></td>
                    </tr>
                    <tr>
                      <td class="normalfnt" style="text-align:right">Grand Total</td>
                      <td class="normalfnt" style="text-align:right">&nbsp;</td>
                      <td class="normalfnt"><span class="normalfnt" style="text-align:right">
                        <input name="txtTotal" type="text" disabled="disabled" id="txtTotal" style="width:100px;text-align:right" value="<?php echo number_format(($grandTotal), 2, '.', ''); ?>" />
                        </span></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="32" ><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td align="center"><a class="button white medium" id="butNew">New</a> <a class="button white medium" id="butSave" style="display:<?php echo ($butSave['type']?'inline':'none')?>">Save</a> <a class="button white medium" id="butApprove" style="display:<?php echo ($butApprove['type']?'inline':'none')?>">Approve</a> <a class="button white medium" id="butCancel" style="display:<?php echo ($butCancel['type']?'inline':'none')?>">Cancel</a> <a class="button white medium" id="butReport" style="display:inline">Report</a> <a href="main.php" class="button white medium" id="butClose">Close</a></td>
              </tr>
            </table></td>
        </tr>
      </table>
    </div>
  </div>
</form>
</body>
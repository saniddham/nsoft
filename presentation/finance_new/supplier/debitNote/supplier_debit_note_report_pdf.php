<?php
session_start();
//ini_set('display_errors',1);

//BEGIN - INCLUDE FILES {
$backwardseperator 	= "../../../../";
include_once "../../../../dataAccess/DBManager2.php";									$db							= new DBManager2();
require_once "../../../../libraries/fpdf/fpdf.php";
include_once "../../../../class/sessions.php";											$sessions					= new sessions();
include_once "../../../../class/tables/mst_locations.php";								$mst_locations				= new mst_locations($db);
include_once "../../../../class/tables/mst_companies.php";								$mst_companies				= new mst_companies($db);
include_once "../../../../class/tables/mst_supplier.php";								$mst_supplier				= new mst_supplier($db);
include_once "../../../../class/tables/mst_country.php";								$mst_country				= new mst_country($db);
require_once "../../../../class/tables/menupermision.php";								$menupermision				= new menupermision($db);
include_once "../../../../class/tables/mst_units.php";									$mst_units					= new mst_units($db);
include_once "../../../../class/tables/sys_users.php";									$sys_users					= new sys_users($db);
include_once "../../../../class/tables/mst_financecurrency.php";						$mst_financecurrency		= new mst_financecurrency($db);
include_once "../../../../class/finance/cls_convert_amount_to_word.php";				$obj_AmtName	   			= new Cls_Convert_Amount_To_Word($db);
include_once "../../../../class/tables/mst_financeexchangerate.php";					$mst_financeexchangerate	= new mst_financeexchangerate($db);
include_once "../../../../class/tables/finance_supplier_debitnote_header.php";			$finance_supplier_debitnote_header			= new finance_supplier_debitnote_header($db);
include_once "../../../../class/tables/finance_supplier_debitnote_details.php";			$finance_supplier_debitnote_details			= new finance_supplier_debitnote_details($db);
include_once "../../../../class/tables/finance_supplier_purchaseinvoice_header.php";	$finance_supplier_purchaseinvoice_header	= new finance_supplier_purchaseinvoice_header($db);

//END	- INCLUDE FILES }

//BEGIN - PARAMETERS {
//$periodArray = array();
$programCode		= 'P0709';
$debitNo			= $_REQUEST["debitNo"];
$debitYear			= $_REQUEST["debitYear"];
$title				= "Suspended Debit Note - Supplier";
$font				= 'Times';
$totValue			= 0;
$savedStatus		= true;
$errorMsg			= '';			

//END	- PARAMETERS }
class PDF extends FPDF
{
	function Header1()
	{
		global $result_reportHeader;
		global $title;
		global $font;
		$h = 5;
		
		$this->Image('../../../../images/screenline.jpg',8,9);
		$this->SetFont($font,'B',14);$this->Cell(0,$h,$result_reportHeader["COMPANY_NAME"],'0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,$result_reportHeader["LOCATION_ADDRESS"].' '.$result_reportHeader["LOCATION_STREET"].' '.$result_reportHeader["LOCATION_CITY"].' '.$result_reportHeader["COMPANY_COUNTRY"].'.','0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,"Tel : ".$result_reportHeader["LOCATION_PHONE"].'  '."Fax : ".$result_reportHeader["LOCATION_FAX"],'0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,"E-mail : ".$result_reportHeader["LOCATION_EMAIL"].'  '."Web : ".$result_reportHeader["LOCATION_WEB"],'0',1,'C');
		$this->SetFont($font,'B',12);$this->Cell(0,10,$title,'0',1,'C');		
	}
	
	function SetPageWaterMark($status,$printStatus)
	{
		switch($status)
		{
			case 1:
				$msg	= ($printStatus==1?"DUPLICATE DEBIT NOTE":"");
				break;
			case '-2':
				$msg	= "CANCELED DEBIT NOTE";
				break;
			default:
				$msg	= "NOT A VALID DEBIT NOTE";
				break;
		}
		$this->SetFont('Times','B',50);
		$this->SetTextColor(255,192,203);
		$this->RotatedText(35,190,$msg,45);
		$this->SetTextColor(0,0,0);
	}
	
	function RotatedText($x, $y, $txt, $angle)
	{
		//Text rotated around its origin
		$this->Rotate($angle,$x,$y);
		$this->Text($x,$y,$txt);
		$this->Rotate(0);
	}
	
	function ReportHeader($debitNo,$debitYear,$result_reportHeader)
	{
		global $finance_supplier_debitnote_header;
		global $finance_supplier_purchaseinvoice_header;
		global $mst_supplier;
		global $mst_country;
		
		$invoiceNo		= $finance_supplier_debitnote_header->getPURCHASE_INVOICE_NO();
		$invoiceYear	= $finance_supplier_debitnote_header->getPURCHASE_INVOICE_YEAR();

		$finance_supplier_debitnote_header->set($debitNo,$debitYear);
		$finance_supplier_purchaseinvoice_header->set($invoiceNo,$invoiceYear);	
		$mst_supplier->set($finance_supplier_debitnote_header->getSUPPLIER_ID());
		$mst_country->set($mst_supplier->getintCountryId());
		
		$h = 5;
		$this->SetFont($font,'',11);$this->Cell(34,$h,'To','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(85,$h,$mst_supplier->getstrName(),'0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Debit No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$debitNo.' - '.$debitYear,'0',1,'L');
		
		$this->SetX(46);
		$this->SetFont($font,'',11);$this->MultiCell(85,$h,$mst_supplier->getstrAddress(),'0','L',0);	
		$this->SetX(46);
		$this->SetFont($font,'',11);$this->Cell(85,$h,$mst_supplier->getstrCity(),'0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Debit Note Date','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$finance_supplier_debitnote_header->getDEBIT_DATE(),'0',1,'L');
		$this->SetX(46);
		$this->SetFont($font,'',11);$this->Cell(85,$h,$mst_country->getstrCountryName().'.','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Invoice No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$finance_supplier_purchaseinvoice_header->getINVOICE_NO(),'0',1,'L');
		
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Attention','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(85,$h,$mst_supplier->getstrContactPerson(),'0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Invoice Date','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$finance_supplier_purchaseinvoice_header->getPURCHASE_DATE(),'0',1,'L');
		
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Supplier VAT No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(85,$h,$mst_supplier->getstrVatNo(),'0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Company VAT No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$result_reportHeader["COMPANY_VAT"],'0',1,'L');
		
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Supplier SVAT No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(85,$h,$mst_supplier->getstrSVatNo(),'0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Company SVAT No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$result_reportHeader["COMPANY_SVAT"],'0',1,'L');
		
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Narration','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->MultiCell(85,$h,$finance_supplier_debitnote_header->getREMARKS(),'0','L',0);	

	}
	
	function CreateTable($result_details,$currCode,$supplierId,$obj_convert_amount_to_word)
	{
		global $totValue;
		global $mst_supplier;
		global $mst_units;
		
		$mst_supplier->set($supplierId);
		$count 			= 0;
		
		$this->Table_header($currCode);
		while($row = mysqli_fetch_array($result_details))
		{
			$mst_units->set($row['UNIT_ID']);
			
			$totValue	+= $row['AMOUNT']+$row["TAX_AMOUNT"];	
			$this->Table_Body($row,$mst_supplier->getstrName(),$mst_units->getstrName());	
			if($count == 10){
				$this->AddPage();
				$this->Table_header($currCode);
				$count = 0;	
			}			
			$count++;			
		}
		$this->Table_Body_Validate($count);	
		$this->Table_footer($totValue,$obj_convert_amount_to_word);
	}
	
	function Table_header($currCode)
	{
		$this->SetXY($this->GetX(),$this->GetY()+2);
		$h = 10;
		$this->SetFont('Times','B',10);
		$this->Cell(69,$h,'Name','1',0,'C');
		$this->Cell(91,$h,'Item Description','1',0,'C');
		$this->Cell(15,$h,"Unit",'1',0,'C');
		$this->Cell(25,$h,'Total Value','1',0,'R');
		$this->Ln();		
	}
		
	function Table_Body($row,$supplierName,$unit)
	{	
		$h	= 8;
		$this->SetFont('Times','',8);
		$this->Cell(69,$h,$supplierName,'LRB',0,'L');
		$this->Cell(91,$h,$row["ITEM_NAME"],'LRB',0,'L');
		$this->Cell(15,$h,$unit,'LRB',0,'L');
		$this->Cell(25,$h,number_format($row["AMOUNT"]+$row["TAX_AMOUNT"],2),'LRB',0,'R');
		$this->Ln();
	}
	
	function Table_Body_Validate($count)
	{
		$h1	= 0;
		for($i=$count;$i<=9;$i++)
		{ 
			//echo "$i";
			//echo "<br/>";
			$h1 += 8;
		}//echo $count.'/'.$h1;
		$this->SetFont('Times','',7);
		$this->Cell(175,$h1,"",'1',0,'C');
		$this->Cell(25,$h1,"",'1',0,'C');
		$this->Ln();
	}
	
	function Table_footer($totValue,$obj_convert_amount_to_word)
	{
		$this->SetY($this->GetY());
		$this->SetFont('Times','B',10);$this->Cell(175,10,"Total Debit Note value",'1',0,'R');
		$this->Cell(25,10,number_format($totValue,2),'1',0,'R');
		$this->Ln();
		
		$this->SetFont('Times','',8);$this->Cell(175,10,$obj_convert_amount_to_word->Convert_Amount($totValue,$baseCurr_array["CODE"]),'0',0,'L');
		$this->Cell(25,10,"",'0',0,'C');
		$this->Ln();
	}
	
	function ReportFooter($exchgRate,$baseCurrencyId)
	{
		global $totValue;
		global $mst_financecurrency;

		$mst_financecurrency->set($baseCurrencyId);
		$this->SetXY(10,$this->GetY()+1);
		$h = 5;		
		$this->SetFont('Times','B',10);$this->Cell(180,$h,"For VAT Purpose Only",'0',0,'L');
		$this->Cell(20,$h,"",'0',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','',10);$this->Cell(180,$h,"Suspended Value Added Tax @ 12 % = ".number_format(($totValue*12)/100,4),'0',0,'L');
		$this->Cell(20,$h,"",'0',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','',10);$this->Cell(180,$h,"Exchange Rate : ".$mst_financecurrency->getstrSymbol().' '.$exchgRate.' / '.$mst_financecurrency->getstrCode().' '.number_format($totValue*$exchgRate,2),'0',0,'L');
		$this->Cell(20,$h,"",'0',0,'C');
		$this->Ln();
		
	}
	
	function Footer1($creator)
	{		
		$this->SetFont('Times','',8);
		
		$this->SetXY(20,$this->GetY()+7);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetX(140);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetXY(20,$this->GetY()+3);
		$this->Cell(50,5,'Authorized By',0,0,'C');
		
		$this->SetX(140);		
		$this->Cell(50,5,'Checked By',0,0,'C');	
		
		$this->SetXY(20,$this->GetY()+10);
		$this->Cell(50,5,$creator,0,0,'C');
		
		$this->SetXY(20,$this->GetY()+2);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetX(140);
		$this->Cell(50,5,'..........................................',0,0,'C');

		$this->SetXY(140,$this->GetY()+3);
		$this->Cell(50,5,'Received By',0,0,'C');

		$this->SetX(20);
		$this->Cell(50,5,'Prepared By',0,0,'C');	
	}
	
	function Footer()
	{
		$this->SetY(-5);
		$this->SetFont('Times','I',8);
		$this->Cell(0,2,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf 					= new PDF('P','mm','Letter');

$db->connect();$db->begin();

$menupermision->set($programCode,$sessions->getUserId());
if($menupermision->getintExportToPDF()==0 && $savedStatus)
{
	$savedStatus	= false;
	$errorMsg		= 'No permission to view PDF.';
}
$finance_supplier_debitnote_header->set($debitNo,$debitYear);
$mst_financecurrency->set($finance_supplier_debitnote_header->getCURRENCY_ID());	
$mst_companies->set($finance_supplier_debitnote_header->getCOMPANY_ID());
$sys_users->set($finance_supplier_debitnote_header->getCREATED_BY());

$result_details			= $finance_supplier_debitnote_details->getDebitNoteDetailData($debitNo,$debitYear);
$result_reportHeader	= $mst_locations->getCompanyReportHeaderData($finance_supplier_debitnote_header->getLOCATION_ID());
$exchgRate				= 0;

if($mst_financeexchangerate->checkCurrencyAvailableForSelectedDate($finance_supplier_debitnote_header->getDEBIT_DATE(),$finance_supplier_debitnote_header->getCOMPANY_ID()) && $finance_supplier_debitnote_header->getCURRENCY_ID()!='')
{
	$mst_financeexchangerate->set($finance_supplier_debitnote_header->getCURRENCY_ID(),$finance_supplier_debitnote_header->getDEBIT_DATE(),$finance_supplier_debitnote_header->getCOMPANY_ID());
	$exchgRate			= $mst_financeexchangerate->getdblExcAvgRate();
}
$baseCurrencyId			= $mst_companies->getintBaseCurrencyId();

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Header1();
$pdf->SetPageWaterMark($finance_supplier_debitnote_header->getSTATUS(),$finance_supplier_debitnote_header->getPRINT_STATUS());
$pdf->ReportHeader($debitNo,$debitYear,$result_reportHeader);
$pdf->CreateTable($result_details,$mst_financecurrency->getstrCode(),$finance_supplier_debitnote_header->getSUPPLIER_ID(),$obj_AmtName);
$pdf->ReportFooter($exchgRate,$baseCurrencyId);
$pdf->Footer1($sys_users->getstrUserName());

if($finance_supplier_debitnote_header->getSTATUS()==1)
{
	$finance_supplier_debitnote_header->set($debitNo,$debitYear);
	$finance_supplier_debitnote_header->setPRINT_STATUS(1);
	
	$result_arr	= $finance_supplier_debitnote_header->commit();	
	if(!$result_arr['status'] && $savedStatus)
	{
		$savedStatus	= false;
		$errorMsg		= $result_arr['msg'];
	}
}
if($savedStatus)
{
	$db->commit();
	$pdf->Output('rpt_supplier_debit_note_pdf.pdf','I');
}
else
{
	$db->rollback();
	echo $errorMsg;
}
$db->disconnect();				
?>
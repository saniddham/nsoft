<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once "class/tables/mst_supplier.php";								$mst_supplier								= new mst_supplier($db);
require_once "class/tables/mst_financecurrency.php";						$mst_financecurrency						= new mst_financecurrency($db);
require_once "class/tables/mst_item.php";									$mst_item									= new mst_item($db);
require_once "class/tables/mst_units.php";									$mst_units									= new mst_units($db);
require_once "class/tables/sys_users.php";									$sys_users									= new sys_users($db);
require_once "class/tables/menupermision.php";								$menupermision								= new menupermision($db);
include_once "class/tables/mst_financetaxgroup.php";						$mst_financetaxgroup						= new mst_financetaxgroup($db);
include_once "class/finance/cls_convert_amount_to_word.php";				$obj_AmtName	   									= new Cls_Convert_Amount_To_Word($db);
require_once "class/tables/finance_mst_chartofaccount.php";					$finance_mst_chartofaccount							= new finance_mst_chartofaccount($db);
require_once "class/tables/finance_supplier_debitnote_header.php";			$finance_supplier_debitnote_header					= new finance_supplier_debitnote_header($db);
require_once "class/tables/finance_supplier_debitnote_details.php";			$finance_supplier_debitnote_details					= new finance_supplier_debitnote_details($db);
require_once "class/tables/finance_supplier_debitnote_approve_by.php";		$finance_supplier_debitnote_approve_by				= new finance_supplier_debitnote_approve_by($db);
require_once "class/tables/finance_supplier_debitnote_gl.php";				$finance_supplier_debitnote_gl						= new finance_supplier_debitnote_gl($db);
include_once "class/tables/finance_supplier_purchaseinvoice_header.php";	$finance_supplier_purchaseinvoice_header			= new finance_supplier_purchaseinvoice_header($db);

$programCode	= 'P0709';

$db->connect();

$debitNo				= $_REQUEST['debitNo'];
$debitYear				= $_REQUEST['debitYear'];
$mode 					= $_REQUEST['mode'];

$totalAmount			= 0;
$totalTaxAmount			= 0;
$totalGLAmonut			= 0;

$finance_supplier_debitnote_header->set($debitNo,$debitYear);
$menupermision->set($programCode,$sessions->getUserId());

$locationId				= $finance_supplier_debitnote_header->getLOCATION_ID();
$status					= $finance_supplier_debitnote_header->getSTATUS();
$levels					= $finance_supplier_debitnote_header->getAPPROVE_LEVELS();
$supplierId				= $finance_supplier_debitnote_header->getSUPPLIER_ID();
$currencyId				= $finance_supplier_debitnote_header->getCURRENCY_ID();
$debitDate				= $finance_supplier_debitnote_header->getDEBIT_DATE();
$invoiceNo				= $finance_supplier_debitnote_header->getPURCHASE_INVOICE_NO();
$invoiceYear			= $finance_supplier_debitnote_header->getPURCHASE_INVOICE_YEAR();
$remarks				= $finance_supplier_debitnote_header->getREMARKS();

$detailData				= $finance_supplier_debitnote_details->getDebitNoteDetailData($debitNo,$debitYear);
$GLDetailData			= $finance_supplier_debitnote_gl->getGLDetailData($debitNo,$debitYear);

$finance_supplier_purchaseinvoice_header->set($invoiceNo,$invoiceYear);
$mst_supplier->set($supplierId);
$mst_financecurrency->set($currencyId);

$supplierInvNo			= $finance_supplier_purchaseinvoice_header->getINVOICE_NO();
$supplier				= $mst_supplier->getstrName();
$currency				= $mst_financecurrency->getstrCode();

$header_array["STATUS"]	= $status;
$header_array['LEVELS'] = $levels;

$perRejArr				= $menupermision->checkMenuPermision('Reject',$status,$levels);
$perApproArr			= $menupermision->checkMenuPermision('Approve',$status,$levels);
$perCancelArr			= $menupermision->checkMenuPermision('Cancel',$status,$levels);
$pdfViewPerm			= $menupermision->getintExportToPDF();

$permision_reject		= ($perRejArr['type']?1:0);
$permision_confirm		= ($perApproArr['type']?1:0);
$permision_cancel		= ($perCancelArr['type']?1:0);

$db->disconnect();
?>

<title>Supplier Debit Note Report</title>
<script type="text/javascript" src="presentation/finance_new/supplier/debitNote/debit_note_js.js"></script>

</head>
<body id="rpt_body">
<div id="rpt_div_main">
<?php include 'presentation/report_ribbon_watermark.php'?>
<div id="rpt_div_table">
<form id="frmDebitNoteRpt" name="frmDebitNoteRpt">
    <table width="1000" align="center">
      <tr>
        <td><?php 	$db->connect();
						include 'report_header_latest.php';
					$db->disconnect();	
					
					$locationId 	= $sessions->getLocationId();		
					?></td>
      </tr>
        <tr>
        <td style="text-align:center">&nbsp;</td>
        </tr>
        <tr>
        <td style="text-align:center"><strong>SUPPLIER DEBIT NOTE REPORT</strong></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:left">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
           		<?php include "presentation/report_approve_status_and_buttons_latest.php" ?>
            </table>
            </td>
        </tr>
      <tr>
        <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td>Debit No</td>
            <td>:</td>
            <td><?php echo $debitNo.'/'.$debitYear; ?></td>
            <td>Invoice No</td>
            <td>:</td>
            <td><?php echo $invoiceNo.'/'.$invoiceYear.' - '.$supplierInvNo; ?></td>
          </tr>
          <tr>
            <td>Supplier</td>
            <td>:</td>
            <td><?php echo $supplier; ?></td>
            <td>Debited Date</td>
            <td>:</td>
            <td><?php echo $debitDate; ?></td>
          </tr>
          <tr>
            <td width="16%">Currency</td>
            <td width="1%">:</td>
            <td width="37%"><?php echo $currency; ?></td>
            <td width="16%">Remarks</td>
            <td width="1%">:</td>
            <td width="29%" rowspan="2" valign="top"><?php echo $remarks;?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td></td>
            <td valign="top">&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="right"><a class="button green small no-print" <?php echo($pdfViewPerm==0?'style="display:none"':''); ?> target="rpt_supplier_debit_note_pdf.php" id="rpt_export_pdf" href="<?php echo "presentation/finance_new/supplier/debitNote/supplier_debit_note_report_pdf.php?debitNo=$debitNo&debitYear=$debitYear"?>">Click here to print Debit Note</a></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" class="rptBordered" id="tblMain">
        <thead>
          <tr>
            <th width="31%">Item</th>
            <th width="10%">UOM</th>
            <th width="11%">Debit Amount</th>
            <th width="10%">Tax</th>
            <th width="10%">Tax Amount</th>
            <th width="12%">Value</th>
            </tr>
        </thead>        
        <tbody>
        <?php
		$db->connect();
		
		while($row = mysqli_fetch_array($detailData))
		{
			
			$mst_units->set($row['UNIT_ID']);
			
			if($row['TAX_CODE']!='')
				$mst_financetaxgroup->set($row['TAX_CODE']);
			
			$totalAmount	+= $row['AMOUNT'];
			$totalTaxAmount	+= $row['TAX_AMOUNT'];
		?>
        	<tr class="normalfnt">
                <td style="text-align:left"><?php echo $row['ITEM_NAME']; ?></td>
                <td style="text-align:left"><?php echo $mst_units->getstrName(); ?></td>
                <td style="text-align:right"><?php echo number_format($row['AMOUNT'], 2, '.', ''); ?></td>
                <td style="text-align:right"><?php echo ($row['TAX_CODE']==''?'&nbsp;':$mst_financetaxgroup->getstrCode()); ?></td>
                <td style="text-align:right"><?php echo number_format(($row['TAX_AMOUNT']==''?0:$row['TAX_AMOUNT']), 2, '.', ''); ?></td>
                <td style="text-align:right"><?php echo number_format(($row['AMOUNT']+$row['TAX_AMOUNT']), 2, '.', ''); ?></td>
        	</tr>
        <?php
		}
		$db->disconnect();	
		?>
        </tbody>
        </table></td>
      </tr>
      <tr>
        <td class="normalfnt">&nbsp;</td>
      </tr>
      <tr>
      	<td valign="top">
        <table width="100%">
        	<tr>
            	<td valign="top">
                	<table class="rptBordered" id="tblLedger" >
                    <thead>
                    <tr>
                        <th width="69%" >Ledger Account</th>
                        <th width="31%" >Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
					$db->connect();
					while($rowGL = mysqli_fetch_array($GLDetailData))
					{
						$GLAccName		 = $finance_mst_chartofaccount->getFullGLAccount($rowGL['CHART_OF_ACCOUNT_ID']);
						$totalGLAmonut	+= $rowGL['AMOUNT'];
					?>
                    	<tr>
                        	<td style="text-align:left" ><?php echo $GLAccName; ?></td>
                        	<td style="text-align:right" ><?php echo number_format($rowGL['AMOUNT'], 2, '.', ''); ?></td>
                    	</tr>
                    <?php
					}
					$db->disconnect();
					?>
                    <tr class="dataRow">
                        <td style="text-align:right"><b>Total</b></td>
                        <td style="text-align:right"><b><?php echo number_format($totalGLAmonut, 2, '.', ''); ?></b></td>
                    </tr>
                    </tbody>                     
        			</table>
                </td>
                <td valign="top">
                	<table border="0" align="right" class="normalfnt">
                      <tr>
                        <td width="100">Sub Total</td>
                        <td width="10">:</td>
                        <td width="106" align="right"><b><?php echo number_format($totalAmount, 2, '.', ''); ?></b></td>
                      </tr>
                      <tr>
                        <td>Tax Total</td>
                        <td>:</td>
                        <td align="right"><b><?php echo number_format($totalTaxAmount, 2, '.', ''); ?></b></td>
                      </tr>
                      <tr>
                        <td>Grand Total</td>
                        <td>:</td>
                        <td align="right"><b><?php echo number_format($totalAmount+$totalTaxAmount, 2, '.', ''); ?></b></td>
                      </tr>
                    </table>
                </td>
            </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td valign="top">
        </td>
        <td valign="top" ></td>
      </tr>
      <tr>
        <td class="normalfnt"><b><?php echo $obj_AmtName->Convert_Amount($totalAmount+$totalTaxAmount,$currency); ?></b></td>
      </tr>
        <tr>
            <td><?php
			$db->connect();
			$sys_users->set($finance_supplier_debitnote_header->getCREATED_BY());
			
			$creator		= $sys_users->getstrUserName();
			$createdDate	= $finance_supplier_debitnote_header->getCREATED_DATE();
												
			$resultA		= $finance_supplier_debitnote_approve_by->getApproveByData($debitNo,$debitYear);
			include "presentation/report_approvedBy_details.php";
			$db->disconnect();
			?></td>
			</tr>
		<tr>
        
<?php
    if($mode=='Confirm' && $status!=1)
    {
    ?>
    <tr height="40">
    <?php
    $url  = "presentation/send_to_approval_latest.php";
    $url .= "?status=$status";										// * set recent status
    $url .= "&approveLevels=$levels";								// * set approve levels in order header
    $url .= "&programCode=$programCode";								// * program code (ex:P10001)
    $url .= "&program=Supplier Debit Note Report";									// * program name (ex:Purchase Order)
    $url .= "&companyId=".$finance_supplier_debitnote_header->getCOMPANY_ID();									// * created company id
    $url .= "&createUserId=".$finance_supplier_debitnote_header->getCREATED_BY();;
    
    $url .= "&subject=Supplier Debit Note for Approval (".$debitNo."/".$debitYear.")";	
    
    $url .= "&statement1=Please Approve this";	
    $url .= "&statement2=to Approve this";	
    $url .= "&link=".base64_encode(MAIN_URL."?q=1082&debitNo=".$debitNo."&debitYear=".$debitYear."&mode=Confirm");
    ?>
    <td align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"></iframe></td>
    </tr>
    <?php
    }
    ?>
    </tr>
        <tr height="30">
    	<td align="center" class="normalfntMid"><strong>Printed Date: <?php echo date('Y-m-d'); ?></strong></td>
    </tr>
      </table>
</form>
</div>
</div>
</body>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
/*
SAVE
	1. check permissions.
	2. check exchange rate.
	3. check user saved location when edit.
	4. check invoice status (its should be approved).
	5. gl amount & grand total amount should be tally.
	6. check with selected invoice balace.

APPROVE
	1. check permissions.
	2. check exchange rate.
	3. check user saved location when approve.
	4. check invoice status (its should be approved).
	5. check with selected invoice balace.
	6. insert into `finance_supplier_transaction` [Document Type = 'DEBIT' , Amount '-'].
	7. insert into `finance_transaction` [Document Type = 'DEBIT',TRANSACTION_CATEGORY 'SU',TRANSACTION_CATEGORY_ID 'GL_ID'] [SUPPLIER 'D' , TAX & OTHER GL 'C']

REJECT
	1. check permissions.
	2. check user saved location when reject.
	3. update header status as '0'.

CANCEL
	1. check permissions.
	2. check saved location when cancel.
	3. update header status as '-2'.
	4. rollback 'finance_supplier_transaction'
	5. rollback 'finance_transaction'
*/
try
{
	$requestType 		= $_REQUEST['requestType']; // request parameter
	$programCode		= 'P0709'; // program code
	$spMenuId			= 64; // back date permission menu id
	
	include_once "class/tables/menupermision.php";								$menupermision 				= new menupermision($db);
	include_once "class/tables/menus_special.php";								$menus_special 				= new menus_special($db);
	include_once "class/tables/sys_no.php";										$sys_no 					= new sys_no($db);
	include_once "class/dateTime.php";											$dateTimes 					= new dateTimes($db);
	include_once "class/tables/sys_approvelevels.php";							$sys_approvelevels 			= new sys_approvelevels($db);
	include_once "class/tables/mst_supplier.php";								$mst_supplier				= new mst_supplier($db);
	include_once "class/tables/mst_financeexchangerate.php";					$mst_financeexchangerate	= new mst_financeexchangerate($db);
	include_once "class/tables/mst_maincategory.php";							$mst_maincategory			= new mst_maincategory($db);
	include_once "class/tables/mst_subcategory.php";							$mst_subcategory			= new mst_subcategory($db);
	include_once "class/tables/mst_units.php";									$mst_units					= new mst_units($db);
	include_once "class/tables/mst_financetaxgroup.php";						$mst_financetaxgroup		= new mst_financetaxgroup($db);
	include_once "class/tables/mst_financedimension.php";						$mst_financedimension		= new mst_financedimension($db);
	include_once "class/tables/finance_transaction.php";						$finance_transaction		= new finance_transaction($db);
	include_once "class/tables/finance_mst_chartofaccount.php";					$finance_mst_chartofaccount	= new finance_mst_chartofaccount($db);
	include_once "class/tables/finance_month_end_process.php";					$finance_month_end_process	= new finance_month_end_process($db);
	include_once "class/tables/finance_supplier_transaction.php";				$finance_supplier_transaction						= new finance_supplier_transaction($db);
	include_once "class/tables/finance_supplier_purchaseinvoice_header.php";	$finance_supplier_purchaseinvoice_header			= new finance_supplier_purchaseinvoice_header($db);
	include_once "class/tables/finance_supplier_purchaseinvoice_details.php";	$finance_supplier_purchaseinvoice_details			= new finance_supplier_purchaseinvoice_details($db);
	include_once "class/tables/finance_supplier_purchaseinvoice_gl.php";		$finance_supplier_purchaseinvoice_gl				= new finance_supplier_purchaseinvoice_gl($db);
	include_once "class/tables/finance_supplier_debitnote_header.php";			$finance_supplier_debitnote_header					= new finance_supplier_debitnote_header($db);
	include_once "class/tables/finance_supplier_debitnote_approve_by.php";		$finance_supplier_debitnote_approve_by				= new finance_supplier_debitnote_approve_by($db);
	include_once "class/tables/finance_supplier_debitnote_details.php";			$finance_supplier_debitnote_details					= new finance_supplier_debitnote_details($db);
	include_once "class/tables/finance_supplier_debitnote_gl.php";				$finance_supplier_debitnote_gl						= new finance_supplier_debitnote_gl($db);
	
	
	include_once "class/finance/cls_get_gldetails.php";							$obj_get_GLCombo			= new Cls_Get_GLDetails($db);
	include_once "class/finance/cls_calculate_tax.php";							$obj_calculate_tax			= new Cls_Calculate_Tax($db);
	
	if($requestType=='loadCurrency')
	{
		$db->connect();
		
		$supplierId		= $_REQUEST['supplierId'];
		$date			= $_REQUEST['date'];
		
		$currencyId		= '';
		$exchgRate		= '0.0000';
		
		$result			= $finance_supplier_purchaseinvoice_header->getSupplierWiseCurrency($supplierId,$sessions->getCompanyId());
		$count			= mysqli_num_rows($result);
		$html 			= "<option value=\"\"></option>";	
		while($row = mysqli_fetch_array($result))
		{
			if($count==1)
			{
				$html .= "<option selected=\"selected\" value=\"".$row['CURRENCY_ID']."\">".$row['CURRENCY_CODE']."</option>";
				$currencyId	= $row['CURRENCY_ID'];
			}
			else
				$html .= "<option value=\"".$row['CURRENCY_ID']."\">".$row['CURRENCY_CODE']."</option>";
		}
		if($mst_financeexchangerate->checkCurrencyAvailableForSelectedDate($date,$sessions->getCompanyId()) && $currencyId!='')
		{
			$mst_financeexchangerate->set($currencyId,$date,$sessions->getCompanyId());
			$exchgRate	= $mst_financeexchangerate->getdblExcAvgRate();
		}
		
		$response['currency'] 		= $html;
		$response['exchgRate'] 		= $exchgRate;
		$response['invoiceNo']		= loadInvoiceNo($supplierId,$currencyId);	
	}
	else if($requestType=='loadExchngRate')
	{
		$db->connect();
		
		$currencyId		= $_REQUEST['currencyId'];
		$supplierId		= $_REQUEST['supplierId'];
		$date			= $_REQUEST['date'];
	
		$exchgRate		= '0.0000';
		
		if($mst_financeexchangerate->checkCurrencyAvailableForSelectedDate($date,$sessions->getCompanyId()))
		{
			$mst_financeexchangerate->set($currencyId,$date,$sessions->getCompanyId());
			$exchgRate	= $mst_financeexchangerate->getdblExcAvgRate();
		}
		
		$response['exchgRate'] 		= $exchgRate;	
		$response['invoiceNo']		= loadInvoiceNo($supplierId,$currencyId);	
	}
	else if($requestType=='loadDetails')
	{
		$invoiceNoArr	= explode('/',$_REQUEST['invoiceNo']);
		$invoiceYear	= $invoiceNoArr[0];
		$invoiceNo		= $invoiceNoArr[1];
		$htmlT			= "";
		$htmlDt			= "";
		$htmlGL			= "";
		$debitedAmount	= 0;
		$balToDebit		= 0;
		
		$result			= $finance_supplier_transaction->getInvoiceTransactionDetails($invoiceNo,$invoiceYear);
		while($row = mysqli_fetch_array($result))
		{
			$htmlT	.= "<tr>";
            $htmlT	.= "<td style=\"text-align:center\" >".$row['INVOICE_DATE']."</td>";
			$htmlT	.= "<td style=\"text-align:right\" >".$row['invoiceAmount']."</td>";
			$htmlT	.= "<td style=\"text-align:right\" >".$row['advancedAmount']."</td>";
			$htmlT	.= "<td style=\"text-align:right\" >".$row['paidAmount']."</td>";
			$htmlT	.= "<td style=\"text-align:right\" >".$row['creditAmount']."</td>";
			$htmlT	.= "<td style=\"text-align:right\" >".$row['debitAmount']."</td>";
			$htmlT	.= "<td style=\"text-align:right\" class=\"clsBalToDebit\" >".$row['toBeDebited']."</td>";
            $htmlT	.= "</tr>";   
			
			$balToDebit	=  $row['toBeDebited'];    		
		}
		
		$resultDet		= $finance_supplier_purchaseinvoice_details->getInvoiceDetails($invoiceNo,$invoiceYear);
		while($rowDt = mysqli_fetch_array($resultDet))
		{
			$mst_maincategory->set($rowDt['MAIN_CAT_ID']);
			$mst_subcategory->set($rowDt['SUB_CAT_ID']);
			$mst_units->set($rowDt['UNIT_ID']);
			
			$debitedAmount	 = $finance_supplier_debitnote_details->getItemWiseDebitedAmount($invoiceNo,$invoiceYear,$rowDt['ITEM_ID']);
			
			$htmlDt	.= '<tr class="normalfnt" id="'.$rowDt['GLACCOUNT_ID'].'">';
            $htmlDt	.= '<td style="text-align:center"><input type="checkbox" id="chkItem" name="chkItem" class="clsChkItem validate[minCheckbox[1]]" ></td>';
			$htmlDt	.= '<td class="clsMainCat" id="'.$rowDt['MAIN_CAT_ID'].'" style="text-align:left">'.$mst_maincategory->getstrName().'</td>';
			$htmlDt	.= '<td class="clsSubCat" id="'.$rowDt['SUB_CAT_ID'].'" style="text-align:left">'.$mst_subcategory->getstrName().'</td>';
			$htmlDt	.= '<td class="clsItem" id="'.$rowDt['ITEM_ID'].'" style="text-align:left">'.$rowDt['ITEM_NAME'].'</td>';
			$htmlDt	.= '<td class="clsUOM" id="'.$rowDt['UNIT_ID'].'" style="text-align:left">'.$mst_units->getstrName().'</td>';
    		$htmlDt	.= '<td style="text-align:right">'.number_format($rowDt['AMOUNT']+$rowDt['TAX_AMOUNT'], 2, '.', '').'</td>';
			$htmlDt	.= '<td style="text-align:right">'.number_format($debitedAmount, 2, '.', '').'</td>';
			$htmlDt	.= '<td style="text-align:center" id="'.(($rowDt['AMOUNT']+$rowDt['TAX_AMOUNT'])-$debitedAmount).'"><input type="text" id="txtAmount" class="validate[required,decimal[2],custom[number]] cls_input_number_validation clsAmount" style="width:78px;text-align:right" disabled="disabled" value="0.00"></td>';
			$htmlDt .= '<td style="text-align:center" id="0"><select name="cboTax" id="cboTax" class="clsTax" style="width:92px">
                    	'.$mst_financetaxgroup->getCombo($rowDt['TAX_CODE'],'intStatus=1').'</select></td>';
			$htmlDt .= '<td style="text-align:center"><select name="cboCostCenter" id="cboCostCenter" class="clsCostCener" style="width:112px" disabled="disabled">
                    	'.$mst_financedimension->getCombo($rowDt['COST_CENTER'],'intStatus=1').'</select></td>';
			$htmlDt .= '</tr>';
		}
		
		$resultGL	= $finance_supplier_purchaseinvoice_gl->getInvoiceWiseGLDetails($invoiceNo,$invoiceYear);
		while($rowGL = mysqli_fetch_array($resultGL))
		{
			if($rowGL['TAX_ID']=='')
				$glType	= 'subCat';
			else
				$glType	= 'tax';
				
			$htmlGL	.= '<tr id="'.$glType.'">';
			$htmlGL	.= '<td style="text-align:center" ><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>';
			$htmlGL	.= '<td style="text-align:center" id="'.$rowGL['TAX_ID'].'"><select name="cboLedgerAc" id="cboLedgerAc" class="clsLedgerAc" style="width:100%">
					   '.$obj_get_GLCombo->getGLCombo('SUPPLIER_DEBITNOTE',$rowGL['GLACCOUNT_ID']).'</select></td>';
			$htmlGL	.= '<td style="text-align:center"><input name="txtLedgerAmonut" id="txtLedgerAmonut" class="validate[required,decimal[2],custom[number]] cls_input_number_validation clsLedgerAmount" type="text" style="width:98%;text-align:right" value="0.00" /></td>';
			$htmlGL	.= '</tr>';
		}

		$response['invoicedDetails']	= $htmlT;
		$response['detailData']			= $htmlDt;
		$response['detailGLData']		= $htmlGL;
		$response['balToDebit']			= $balToDebit;
	}
	else if($requestType=='calculateTaxAmount')
	{
		$taxId		= $_REQUEST['taxId'];
		$amount		= ($_REQUEST['amount']==''?0:$_REQUEST['amount']);
	
		$taxAmount	= $obj_calculate_tax->CalculateTax($taxId,$amount);
		$response   = json_decode($taxAmount);	
	}
	else if($requestType=='saveData')
	{
		$arrHeader 		= json_decode($_REQUEST['arrHeader'],true);
		$arrDetails 	= json_decode($_REQUEST['arrDetails'],true);
		$arrGLDetails 	= json_decode($_REQUEST['arrGLDetails'],true);
		
		$debitNo		= $arrHeader['debitNo'];
		$debitYear		= $arrHeader['debitYear'];
		$invoiceNoArr	= explode('/',$arrHeader['invoiceNo']);
		$invoiceNo		= $invoiceNoArr[1];
		$invoiceYear	= $invoiceNoArr[0];
		$supplierId		= $arrHeader['supplierId'];
		$currency		= $arrHeader['currency'];
		$debitDate		= $arrHeader['debitDate'];
		$Remarks		= $arrHeader['Remarks'];
		$editMode		= false;
		$detailStatus	= false;
		$glDetailStatus	= false;
		
		$db->connect();$db->begin();//open connection.
		
		//check back date permission
		if($debitDate<$dateTimes->getCurruntDate())
		{
			$backDate_perm	= $menus_special->loadSpecialMenuPermission($spMenuId,$sessions->getUserId());
			
			if($backDate_perm!=1)
				throw new Exception('No permission to backdate Supplier Debit Note.');
		}
		
		//check month end process locked
		$mep_status		= $finance_month_end_process->getIsMonthEndProcessLocked($debitDate,$sessions->getCompanyId());
		
		if($mep_status)
				throw new Exception("Month end Process locked for the date '".$debitDate."' .");
		
		//check save or update
		if($debitNo=='' && $debitYear=='')
		{	
			#########################################################
			#	1. check permissions.								#
			#########################################################
			$menupermision->set($programCode,$sessions->getUserId());
			$permissionArr	= $menupermision->checkMenuPermision('Edit','','');
			
			if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);			
			
			#########################################################
			#	2. check exchange rate.								#
			#########################################################
			if(!$mst_financeexchangerate->checkCurrencyAvailableForSelectedDate($debitDate,$sessions->getCompanyId()))
				throw new Exception("Please enter exchange rates for '".$debitDate."' before save.");
			
			#########################################################
			#	4. check invoice status (its should be approved).	#
			#########################################################
			$finance_supplier_purchaseinvoice_header->set($invoiceNo,$invoiceYear);
			
			if($finance_supplier_purchaseinvoice_header->getSTATUS()!=1)
				throw new Exception("Selected Invoice is not a Approved Invoice.");
			
			//get sys max no
			$debitNo		= $sys_no->getSerialNoAndUpdateSysNo('intSupplierDebitNo',$sessions->getLocationId());
			$debitYear		= $dateTimes->getCurruntYear();
			// set sys_approvelevels values
			$sys_approvelevels->set($programCode);
			//get approve levels
			$approveLevels	= $sys_approvelevels->getintApprovalLevel();
			//set status status = approve level + 1
			$status			= $approveLevels+1; 
			// save header data
			$result_arr		= $finance_supplier_debitnote_header->insertRec($debitNo,$debitYear,$invoiceNo,$invoiceYear,$supplierId,$currency,$debitDate,$Remarks,$status,$approveLevels,$sessions->getCompanyId(),$sessions->getLocationId(),$sessions->getUserId(),$dateTimes->getCurruntDateTime(),'NULL',$dateTimes->getCurruntDateTime());
			
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
		}
		else
		{
			#########################################################
			#	3. check user saved location when edit.				#
			#########################################################
			$finance_supplier_debitnote_header->set($debitNo,$debitYear);
			
			$status			= $finance_supplier_debitnote_header->getSTATUS();
			$approveLevels	= $finance_supplier_debitnote_header->getAPPROVE_LEVELS();
			
			if($finance_supplier_debitnote_header->getLOCATION_ID()!=$sessions->getLocationId())
				throw new Exception("This is not saved location");
			
			//check edit permission
			$menupermision->set($programCode,$sessions->getUserId());
			$permissionArr	= $menupermision->checkMenuPermision('Edit',$status,$approveLevels);
			
			if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);

			// set sys_approvelevels values
			$sys_approvelevels->set($programCode);
			
			$approveLevels		= $sys_approvelevels->getintApprovalLevel();
			$status				= $approveLevels+1;
			//update header data
			$finance_supplier_debitnote_header->setPURCHASE_INVOICE_NO($invoiceNo);
			$finance_supplier_debitnote_header->setPURCHASE_INVOICE_YEAR($invoiceYear);
			$finance_supplier_debitnote_header->setSUPPLIER_ID($supplierId);
			$finance_supplier_debitnote_header->setCURRENCY_ID($currency);
			$finance_supplier_debitnote_header->setDEBIT_DATE($debitDate);
			$finance_supplier_debitnote_header->setREMARKS($Remarks);
			$finance_supplier_debitnote_header->setSTATUS($status);
			$finance_supplier_debitnote_header->setAPPROVE_LEVELS($approveLevels);
			$finance_supplier_debitnote_header->setCOMPANY_ID($sessions->getCompanyId());
			$finance_supplier_debitnote_header->setLOCATION_ID($sessions->getLocationId());
			$finance_supplier_debitnote_header->setLAST_MODIFY_BY($sessions->getUserId());
			$finance_supplier_debitnote_header->setLAST_MODIFY_DATE($dateTimes->getCurruntDateTime());
			
			$result_arr	= $finance_supplier_debitnote_header->commit();
			
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
			
			//update max status in approve by table
			$finance_supplier_debitnote_approve_by->updateMaxStatus($debitNo,$debitYear);
			
			//delete finance debot note detail data
			$result_arr = $finance_supplier_debitnote_details->delete(" DEBIT_NO = $debitNo AND DEBIT_YEAR = $debitYear ");
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
			
			//delete finance debot note GL detail data
			$result_arr = $finance_supplier_debitnote_gl->delete(" DEBIT_NO = $debitNo AND DEBIT_YEAR = $debitYear ");
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
			
			$editMode	= true;			
		}
		$totalAmount	= 0;
		$totalGLAmount	= 0;
		foreach($arrDetails as $array_loop)
		{
			$detailStatus	= true;
			$itemId			= $array_loop['itemId'];
			$amount			= $array_loop['amount'];
			$taxAmount		= $array_loop['taxAmount'];
			$taxCode		= ($array_loop['taxCode']==''?'null':$array_loop['taxCode']);
			$costCenter		= $array_loop['costCenter'];
			
			$totalAmount   += $amount+$taxAmount;
			
			//save detail data
			$result_arr = $finance_supplier_debitnote_details->insertRec($debitNo,$debitYear,$itemId,$amount,$taxAmount,$taxCode,$costCenter);
			
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);	
		}
		if(!$detailStatus)
			throw new Exception('No detail data to save.');
			
		foreach($arrGLDetails as $array_loop_gl)
		{
			$glDetailStatus	= true;
			$ledgerAccount	= $array_loop_gl['ledgerAccount'];
			$glamount		= $array_loop_gl['amount'];
			
			$totalGLAmount += $glamount;
			
			//save GL detail data
			$result_arr = $finance_supplier_debitnote_gl->insertRec($debitNo,$debitYear,$ledgerAccount,$glamount);
			
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);	
		}
		if(!$glDetailStatus)
			throw new Exception('No GL detail data to save.');
		
		#########################################################
		#	5. gl amount & grand total amount should be tally.	#
		#########################################################
		if(round($totalAmount,2)!=round($totalGLAmount,2))
			throw new Exception('Total Ledger amount must equal to Grand Total.');
		
		#########################################################
		#	6. check with selected invoice balace.				#
		#########################################################
		$balResult	= $finance_supplier_transaction->getInvoiceTransactionDetails($invoiceNo,$invoiceYear);
		$balRow		= mysqli_fetch_array($balResult);
		
		$balToDebit	= $balRow['toBeDebited'];
		
		if(round($totalAmount,2)>$balToDebit)
			throw new Exception('Grand total greater than bal to Debit Amount.');
		
		//if status = 1 then save to transactions
		$finance_supplier_debitnote_header->set($debitNo,$debitYear);
		$status			= $finance_supplier_debitnote_header->getSTATUS();
		$approveLevels	= $finance_supplier_debitnote_header->getAPPROVE_LEVELS();
		
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Approve',$status,$approveLevels);
		$approvePerm	= $permissionArr['type'];
		
		$permissionCArr	= $menupermision->checkMenuPermision('Cancel',$status,$approveLevels);
		$cancelPerm		= $permissionCArr['type'];
		
		$permissionSArr	= $menupermision->checkMenuPermision('Edit',$status,$approveLevels);
		$editPerm		= $permissionSArr['type'];
		
		if($status==1)
			saveTransactions($debitNo,$debitYear,$supplierId,$currencyId,$invoiceYear,$invoiceNo,$debitDate);
			
		//validate duplicate entry
		$sys_no->validateDuplicateSerialNoWithSysNo($debitNo,'intSupplierDebitNo',$sessions->getLocationId());
		
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode)
			$response['msg'] 		= "Updated Successfully.";
		else
			$response['msg'] 		= "Saved Successfully.";
			
		$response['debitNo']		= $debitNo;
		$response['debitYear']		= $debitYear;
		$response['approvePerm']	= $approvePerm;
		$response['cancelPerm']		= $cancelPerm;
		$response['editPerm']		= $editPerm;
	}
	else if($requestType=='approve')
	{
		$debitNo			= $_REQUEST['debitNo'];
		$debitYear			= $_REQUEST['debitYear'];
		
		$db->connect();$db->begin();//open connection.
		
		$finance_supplier_debitnote_header->set($debitNo,$debitYear);
		$status				= $finance_supplier_debitnote_header->getSTATUS();
		$approveLevels		= $finance_supplier_debitnote_header->getAPPROVE_LEVELS();
		$debitDate			= $finance_supplier_debitnote_header->getDEBIT_DATE();
		$invoiceNo			= $finance_supplier_debitnote_header->getPURCHASE_INVOICE_NO();
		$invoiceYear		= $finance_supplier_debitnote_header->getPURCHASE_INVOICE_YEAR();
		$supplierId			= $finance_supplier_debitnote_header->getSUPPLIER_ID();
		$currencyId			= $finance_supplier_debitnote_header->getCURRENCY_ID();
		
		//check back date permission
		if($debitDate<$dateTimes->getCurruntDate())
		{
			$backDate_perm	= $menus_special->loadSpecialMenuPermission($spMenuId,$sessions->getUserId());
			
			if($backDate_perm!=1)
				throw new Exception('No permission to backdate Supplier Debit Note.');
		}
		
		#########################################################
		#	1. check permissions.								#
		#########################################################
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Approve',$status,$approveLevels);
		
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
		
		#########################################################
		#	2. check exchange rate.								#
		#########################################################
		if(!$mst_financeexchangerate->checkCurrencyAvailableForSelectedDate($debitDate,$sessions->getCompanyId()))
			throw new Exception("Please enter exchange rates for '".$debitDate."' before save.");
		
		#########################################################
		#	3. check user saved location when approve.				#
		#########################################################		
		if($finance_supplier_debitnote_header->getLOCATION_ID()!=$sessions->getLocationId())
			throw new Exception("This is not saved location");
		
		#########################################################
		#	4. check invoice status (its should be approved).	#
		#########################################################
		$finance_supplier_purchaseinvoice_header->set($invoiceNo,$invoiceYear);
		
		if($finance_supplier_purchaseinvoice_header->getSTATUS()!=1)
			throw new Exception("Selected Invoice is not a Approved Invoice.");
		
		#########################################################
		#	5. check with selected invoice balace.				#
		#########################################################
		$cols	= " ROUND(SUM(AMOUNT),2) totAmonut ";
		
		$where	= " DEBIT_NO = '".$debitNo."' AND
					DEBIT_YEAR = '".$debitYear."' ";
		
		$result			= $finance_supplier_debitnote_gl->select($cols,$join=null,$where);
		$row			= mysqli_fetch_array($result);
		$grandTotal		= $row['totAmonut'];
		
		$balResult		= $finance_supplier_transaction->getInvoiceTransactionDetails($invoiceNo,$invoiceYear);
		$balRow			= mysqli_fetch_array($balResult);
		$balToDebit		= $balRow['toBeDebited'];
		
		if(round($grandTotal,2)>$balToDebit)
			throw new Exception('Grand total greater than bal to Debit Amount.');
		
		// update header status function
		$where		= 'DEBIT_NO = "'.$debitNo.'" AND DEBIT_YEAR = "'.$debitYear.'" ' ;
		$data  		= array('STATUS' => '-1');
		
		$resultArr	= $finance_supplier_debitnote_header->upgrade($data,$where); // upgrade header status
		if(!$resultArr['status'])
			throw new Exception($resultArr['msg']);
		
		 // update approve by table
		$finance_supplier_debitnote_header->set($debitNo,$debitYear);
		$status		= $finance_supplier_debitnote_header->getSTATUS();
		$level		= $finance_supplier_debitnote_header->getAPPROVE_LEVELS();
		$approval	= $level+1-$status;
		
		$result_arr	= $finance_supplier_debitnote_approve_by->insertRec($debitNo,$debitYear,$approval,$sessions->getUserId(),$dateTimes->getCurruntDateTime(),0);
		
		if($status==1)
			saveTransactions($debitNo,$debitYear,$supplierId,$currencyId,$invoiceYear,$invoiceNo,$debitDate);
		
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Approved Successfully.";			
	}
	else if($requestType=='reject')
	{
		$debitNo			= $_REQUEST['debitNo'];
		$debitYear			= $_REQUEST['debitYear'];
		
		$db->connect();$db->begin();//open connection.
		
		$finance_supplier_debitnote_header->set($debitNo,$debitYear);
		$status				= $finance_supplier_debitnote_header->getSTATUS();
		$approveLevels		= $finance_supplier_debitnote_header->getAPPROVE_LEVELS();
		
		#########################################################
		#	1. check permissions.								#
		#########################################################
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Reject',$status,$approveLevels);
		
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
		
		#########################################################
		#	2. check user saved location when reject.			#
		#########################################################		
		if($finance_supplier_debitnote_header->getLOCATION_ID()!=$sessions->getLocationId())
			throw new Exception("This is not saved location");
		
		#########################################################
		#	3. update header status as '0'.						#
		#########################################################
		$finance_supplier_debitnote_header->setSTATUS(0);
		$result_arr	= $finance_supplier_debitnote_header->commit();
		
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
		
		// update approve by table
		$result_arr	= $finance_supplier_debitnote_approve_by->insertRec($debitNo,$debitYear,0,$sessions->getUserId(),$dateTimes->getCurruntDateTime(),0);
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
		
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Rejected Successfully.";
	}
	else if($requestType=='cancel')
	{
		$debitNo			= $_REQUEST['debitNo'];
		$debitYear			= $_REQUEST['debitYear'];
		
		$db->connect();$db->begin();//open connection.
		
		$finance_supplier_debitnote_header->set($debitNo,$debitYear);
		$status				= $finance_supplier_debitnote_header->getSTATUS();
		$approveLevels		= $finance_supplier_debitnote_header->getAPPROVE_LEVELS();
		$debitDate			= $finance_supplier_debitnote_header->getDEBIT_DATE();
		
		//check month end process locked
		$mep_status		= $finance_month_end_process->getIsMonthEndProcessLocked($debitDate,$sessions->getCompanyId());
		
		if($mep_status)
				throw new Exception("Month end Process locked for the date '".$debitDate."' .");
				
		#########################################################
		#	1. check permissions.								#
		#########################################################
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Cancel',$status,$approveLevels);
		
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
		
		#########################################################
		#	2. check user saved location when cancel.			#
		#########################################################		
		if($finance_supplier_debitnote_header->getLOCATION_ID()!=$sessions->getLocationId())
			throw new Exception("This is not saved location");
		
		#########################################################
		#	3. update header status as '-2'.					#
		#########################################################
		$finance_supplier_debitnote_header->setSTATUS(-2);
		$result_arr	= $finance_supplier_debitnote_header->commit();
		
		// update approve by table
		$result_arr	= $finance_supplier_debitnote_approve_by->insertRec($debitNo,$debitYear,-2,$sessions->getUserId(),$dateTimes->getCurruntDateTime(),0);
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
		
		#########################################################
		#	4. rollback 'finance_supplier_transaction'.			#
		#########################################################
		$where		= " DOCUMENT_NO = '".$debitNo."' AND
						DOCUMENT_YEAR = '".$debitYear."' AND
						DOCUMENT_TYPE = 'DEBIT' ";
						
		$delResultSTArr = $finance_supplier_transaction->delete($where);
		if(!$delResultSTArr['status'])
			throw new Exception($delResultSTArr['msg']);
		
		#########################################################
		#	5. rollback 'finance_transaction'.					#
		#########################################################
		$where		= " DOCUMENT_NO = '".$debitNo."' AND
						DOCUMENT_YEAR = '".$debitYear."' AND
						DOCUMENT_TYPE = 'DEBIT' AND
						TRANSACTION_CATEGORY = 'SU' ";
						
		$delResultFTArr = $finance_transaction->delete($where);
		if(!$delResultFTArr['status'])
			throw new Exception($delResultFTArr['msg']);
		
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Cancelled Successfully.";	
	}
}
catch(Exception $e)
{
	$db->rollback();
	$response['msg'] 	=  $e->getMessage();
	$response['error'] 	=  $error_handler->jTraceEx($e);
	$response['type'] 	=  'fail';
	$response['sql']	=  $db->getSql();
	$db->disconnect();
}
$db->disconnect();
echo json_encode($response);

function loadInvoiceNo($supplierId,$currencyId)
{
	global $finance_supplier_purchaseinvoice_header;
	global $sessions;
	
	$html 			= "<option value=\"\"></option>";	
	$result = $finance_supplier_purchaseinvoice_header->getSupplierCurrencyWiseInvoiceNo($supplierId,$currencyId,$sessions->getCompanyId());
	while($row = mysqli_fetch_array($result))
	{
		$html .= "<option value=\"".$row['PURCHASE_INVOICE_YEAR']."/".$row['PURCHASE_INVOICE_NO']."\">".$row['PURCHASE_INVOICE_NO']."/".$row['PURCHASE_INVOICE_YEAR']." - ".$row['INVOICE_NO']."</option>";	
	}
	return $html;
}
function saveTransactions($debitNo,$debitYear,$supplierId,$currencyId,$invoiceYear,$invoiceNo,$debitDate)
{
	global $finance_supplier_transaction;
	global $finance_transaction;
	global $finance_mst_chartofaccount;
	global $sessions;
	global $dateTimes;
	
	$totGLAmount	= 0;
	$result			= getGLDetails($debitNo,$debitYear);
	$chkStatus		= false;
	
	while($row = mysqli_fetch_array($result))
	{
		#########################################################
		#	6. insert into finance_supplier_transaction			#
		#########################################################
		$chkStatus		= true;
		$totGLAmount	= $totGLAmount+$row['AMOUNT'];
		
		$resultSTArr	= $finance_supplier_transaction->insertRec($row['PO_YEAR'],$row['PO_NO'],$supplierId,$currencyId,$debitYear,$debitNo,'DEBIT',$invoiceYear,$invoiceNo,$row['GL_ID'],($row['AMOUNT']*-1),$sessions->getCompanyId(),$sessions->getLocationId(),$sessions->getUserId(),$debitDate);
		
		if(!$resultSTArr['status'])
			throw new Exception($resultSTArr['msg']);
		
		#########################################################
		#	7.	insert into finance_transaction					#
		#########################################################
		
		//finance credit transaction TAX & OTHER GL 'C'
		$resultFTArr	= $finance_transaction->insertRec($row['GL_ID'],$row['AMOUNT'],$debitNo,$debitYear,'DEBIT',$invoiceNo,$invoiceYear,'C','SU',$supplierId,$currencyId,'NULL','NULL',0,$sessions->getLocationId(),$sessions->getCompanyId(),$sessions->getUserId(),$debitDate,$dateTimes->getCurruntDateTime());
		
		if(!$resultFTArr['status'])
			throw new Exception($resultFTArr['msg']);	
	}
	if(!$chkStatus)
		throw new Exception('Error in Final Approval.');		
	
	//finance debit transaction SUPPLIER 'D'
	$supplierGLId	= $finance_mst_chartofaccount->getCatogoryTypeWiseGL('S',$supplierId);
	$resultSFTArr	= $finance_transaction->insertRec($supplierGLId,$totGLAmount,$debitNo,$debitYear,'DEBIT',$invoiceNo,$invoiceYear,'D','SU',$supplierId,$currencyId,'NULL','NULL',0,$sessions->getLocationId(),$sessions->getCompanyId(),$sessions->getUserId(),$debitDate,$dateTimes->getCurruntDateTime());
	
	if(!$resultSFTArr['status'])
		throw new Exception($resultSFTArr['msg']);	
	
}
function getGLDetails($debitNo,$debitYear)
{
	global $finance_supplier_debitnote_gl;
	
	$cols	= " trn_poheader.intPOYear                                  AS PO_YEAR,
				trn_poheader.intPONo                                    AS PO_NO,
				finance_supplier_debitnote_gl.CHART_OF_ACCOUNT_ID       AS GL_ID,
				finance_supplier_debitnote_gl.AMOUNT                    AS AMOUNT ";
	
	$join	= " INNER JOIN finance_supplier_debitnote_header
				ON finance_supplier_debitnote_header.DEBIT_NO = finance_supplier_debitnote_gl.DEBIT_NO
				AND finance_supplier_debitnote_header.DEBIT_YEAR = finance_supplier_debitnote_gl.DEBIT_YEAR
				INNER JOIN finance_supplier_purchaseinvoice_header
				ON finance_supplier_purchaseinvoice_header.PURCHASE_INVOICE_NO = finance_supplier_debitnote_header.PURCHASE_INVOICE_NO
				AND finance_supplier_purchaseinvoice_header.PURCHASE_INVOICE_YEAR = finance_supplier_debitnote_header.PURCHASE_INVOICE_YEAR
				INNER JOIN ware_grnheader
				ON ware_grnheader.strInvoiceNo = finance_supplier_purchaseinvoice_header.INVOICE_NO
				INNER JOIN trn_poheader
				ON trn_poheader.intPONo = ware_grnheader.intPoNo
				AND trn_poheader.intPOYear = ware_grnheader.intPOYear
				AND finance_supplier_debitnote_header.SUPPLIER_ID = trn_poheader.intSupplier ";
	
	$where	= " finance_supplier_debitnote_header.DEBIT_NO = '".$debitNo."' AND 
   				finance_supplier_debitnote_header.DEBIT_YEAR = '".$debitYear."' ";
	
	return $finance_supplier_debitnote_gl->select($cols,$join,$where);
}
?>
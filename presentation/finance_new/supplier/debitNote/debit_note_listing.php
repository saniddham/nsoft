<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$session_locationId = $sessions->getLocationId();
$session_companyId 	= $sessions->getCompanyId();
$intUser  			= $sessions->getUserId();

$programCode		= 'P0709';

include_once 		"class/tables/finance_supplier_debitnote_header.php";				$finance_supplier_debitnote_header 			= new finance_supplier_debitnote_header($db);
include_once 		"libraries/jqgrid2/inc/jqgrid_dist.php";

$select				= "MAX(APPROVE_LEVELS) AS MAX_LEVEL";
$header_result		= $finance_supplier_debitnote_header->select($select,NULL,NULL,NULL,NULL);
$header_array		= mysqli_fetch_array($header_result);
$approveLevel 		= $header_array['MAX_LEVEL'];

//BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
$arr =  json_decode($_REQUEST['filters'],true);

$arr = $arr['rules'];

$where_string = '';
$where_array = array(
					'STATUS'=>'SDH.STATUS',
					'CONCAT_DEBIT_NO'=>"CONCAT(SDH.DEBIT_NO,' / ',SDH.DEBIT_YEAR)",
					'PUR_INVOICE_NO'=>"CONCAT(SDH.PURCHASE_INVOICE_NO,' / ',SDH.PURCHASE_INVOICE_YEAR)",
					'INVOICE_NO'=>"PIH.INVOICE_NO",
					'SUPPLIER'=>"SDH.SUPPLIER_ID",
					'CURRENCY'=>"SDH.CURRENCY_ID",
					'RAISED_BY'=>"sys_users.strUserName",
					'CREATED_DATE'=>"DATE(SDH.DEBIT_DATE)"
					);
$arr_status 	= array('Approved'=>'1','Rejected'=>'0','Revised'=>'-1','Cancelled'=>'-2','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='STATUS')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0)					 
		$where_string .= "AND DATE(SDH.DEBIT_DATE) = '".date('Y-m-d')."'";
//END }

$sql = "SELECT SUB_1.* FROM
			(SELECT
				if(SDH.STATUS=1,'Approved',if(SDH.STATUS=-2,'Cancelled',if(SDH.STATUS=0,'Rejected','Pending'))) as STATUS,
				CONCAT(SDH.DEBIT_NO,' / ',SDH.DEBIT_YEAR) AS CONCAT_DEBIT_NO,
				CONCAT(SDH.PURCHASE_INVOICE_NO,' / ',SDH.PURCHASE_INVOICE_YEAR) AS PUR_INVOICE_NO,
				SDH.PURCHASE_INVOICE_NO,
				SDH.PURCHASE_INVOICE_YEAR,
				PIH.INVOICE_NO AS INVOICE_NO,
				SDH.DEBIT_NO,
				SDH.DEBIT_YEAR,
				SDH.REMARKS,
				SDH.SUPPLIER_ID,
				SDH.CURRENCY_ID,
				DATE(SDH.DEBIT_DATE) AS DEBIT_DATE,
				MS.strName AS SUPPLIER,
				FC.strCode AS CURRENCY,
				SDH.APPROVE_LEVELS,
				SDH.CREATED_BY,
				sys_users.strUserName AS RAISED_BY,
				(
				SELECT round(SUM(AMOUNT+TAX_AMOUNT),2) AS totAmount
				FROM finance_supplier_debitnote_details
				WHERE DEBIT_NO = SDH.DEBIT_NO AND
				DEBIT_YEAR = SDH.DEBIT_YEAR
				) AS AMOUNT,
				
				IFNULL((
					SELECT
					concat(sys_users.strUserName,'(',max(finance_supplier_debitnote_approve_by.APPROVE_DATE),')' )
					FROM
					finance_supplier_debitnote_approve_by
					Inner Join sys_users ON finance_supplier_debitnote_approve_by.APPROVE_BY = sys_users.intUserId
					WHERE
					finance_supplier_debitnote_approve_by.DEBIT_NO  = SDH.DEBIT_NO AND
					finance_supplier_debitnote_approve_by.DEBIT_YEAR =  SDH.DEBIT_YEAR AND
					finance_supplier_debitnote_approve_by.APPROVE_LEVEL = '1' AND
					finance_supplier_debitnote_approve_by.STATUS =  '0'
				),IF(((SELECT
					menupermision.int1Approval 
					FROM menupermision 
					Inner Join menus ON menupermision.intMenuId = menus.intId
					WHERE
					menus.strCode = '$programCode' AND
					menupermision.intUserId =  '$intUser')=1 AND SDH.STATUS>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
								$approval	= "2nd_Approval";
							}

							else if($i==3){
								$approval	= "3rd_Approval";
							}
							else {
								$approval	= $i."th_Approval";
							}
							
							
						$sql .= "IFNULL((
								SELECT
								concat(sys_users.strUserName,'(',max(finance_supplier_debitnote_approve_by.APPROVE_DATE),')' )
								FROM
								finance_supplier_debitnote_approve_by
								Inner Join sys_users ON finance_supplier_debitnote_approve_by.APPROVE_BY = sys_users.intUserId
								WHERE
								finance_supplier_debitnote_approve_by.DEBIT_NO  = SDH.DEBIT_NO AND
								finance_supplier_debitnote_approve_by.DEBIT_YEAR =  SDH.DEBIT_YEAR AND
								finance_supplier_debitnote_approve_by.APPROVE_LEVEL = '$i' AND
								finance_supplier_debitnote_approve_by.STATUS =  '0'
							),
							IF(
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (SDH.STATUS>1) AND (SDH.STATUS<=SDH.APPROVE_LEVELS) AND ((SELECT
								concat(sys_users.strUserName )
								FROM
								finance_supplier_debitnote_approve_by
								Inner Join sys_users ON finance_supplier_debitnote_approve_by.APPROVE_BY = sys_users.intUserId
								WHERE
								finance_supplier_debitnote_approve_by.DEBIT_NO  = SDH.DEBIT_NO AND
								finance_supplier_debitnote_approve_by.DEBIT_YEAR =  SDH.DEBIT_YEAR AND
								finance_supplier_debitnote_approve_by.APPROVE_LEVEL =  ($i-1) AND 
								finance_supplier_debitnote_approve_by.STATUS='0' )<>'')),
								
								'Approve',
								 if($i>SDH.APPROVE_LEVELS,'-----',''))
								
								) as `".$approval."`, "; 
									
								}
								
							/*$sql .= "IFNULL((
								SELECT
								concat(sys_users.strUserName,'(',max(finance_supplier_debitnote_approve_by.APPROVE_DATE),')' )
								FROM
								finance_supplier_debitnote_approve_by
								Inner Join sys_users ON finance_supplier_debitnote_approve_by.APPROVE_BY = sys_users.intUserId
								WHERE
								finance_supplier_debitnote_approve_by.DEBIT_NO  = SDH.DEBIT_NO AND
								finance_supplier_debitnote_approve_by.DEBIT_YEAR =  SDH.DEBIT_YEAR AND
								finance_supplier_debitnote_approve_by.APPROVE_LEVEL = '0' AND
								finance_supplier_debitnote_approve_by.STATUS =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND SDH.STATUS<=SDH.APPROVE_LEVELS AND 
								SDH.STATUS>1),'Reject', '')) as `Reject`,";*/
							
							$sql .= "IFNULL((
								SELECT
								concat(sys_users.strUserName,'(',max(finance_supplier_debitnote_approve_by.APPROVE_DATE),')' )
								FROM
								finance_supplier_debitnote_approve_by
								Inner Join sys_users ON finance_supplier_debitnote_approve_by.APPROVE_BY = sys_users.intUserId
								WHERE
								finance_supplier_debitnote_approve_by.DEBIT_NO  = SDH.DEBIT_NO AND
								finance_supplier_debitnote_approve_by.DEBIT_YEAR =  SDH.DEBIT_YEAR AND
								finance_supplier_debitnote_approve_by.APPROVE_LEVEL = '-2' AND
								finance_supplier_debitnote_approve_by.STATUS =  '0'
							),IF(((SELECT
								menupermision.intCancel 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND
								SDH.STATUS=1),'Cancel', '')) as `Cancel`,";
								
							$sql .= "'View' as `View`   
				
				FROM finance_supplier_debitnote_header as SDH 
				INNER JOIN sys_users ON sys_users.intUserId = SDH.CREATED_BY
				INNER JOIN finance_supplier_purchaseinvoice_header PIH ON PIH.PURCHASE_INVOICE_NO = SDH.PURCHASE_INVOICE_NO AND
				PIH.PURCHASE_INVOICE_YEAR = SDH.PURCHASE_INVOICE_YEAR
				INNER JOIN mst_supplier MS ON MS.intId=SDH.SUPPLIER_ID
				LEFT JOIN mst_financecurrency FC ON FC.intId=SDH.CURRENCY_ID
				WHERE SDH.COMPANY_ID='$session_companyId'
				$where_string
		)  
		AS SUB_1 WHERE 1=1";
//echo $sql.'<br>';
$jq = new jqgrid('',$db);	

$cols	= array();
$col	= array();

$col["title"] 				= "Status";
$col["name"] 				= "STATUS";
$col["width"] 				= "2"; 						
$col["align"] 				= "center";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["stype"] 				= "select";
$str 						= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Cancelled:Cancelled" ;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "debitNo";
$col["name"] 				= "DEBIT_NO";	
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;				
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "debitYear";
$col["name"] 				= "DEBIT_YEAR";
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;					
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "purInvNo";
$col["name"] 				= "PURCHASE_INVOICE_NO";	
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;				
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "purInvYear";
$col["name"] 				= "PURCHASE_INVOICE_YEAR";
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;					
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Debit No";
$col["name"] 				= "CONCAT_DEBIT_NO";
$col["width"] 				= "3";
$col["align"] 				= "center";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col['link']				= '?q=709&debitNo={DEBIT_NO}&debitYear={DEBIT_YEAR}';
$col["linkoptions"] 		= "target='debit_note.php'";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Inv Serial No";
$col["name"] 				= "PUR_INVOICE_NO";
$col["width"] 				= "3";
$col["align"] 				= "center";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col['link']				= '?q=893&purInvoiceNo={PURCHASE_INVOICE_NO}&purInvoiceYear={PURCHASE_INVOICE_YEAR}';
$col["linkoptions"] 		= "target='rptPurchaseInvoice.php'";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Invoice No";
$col["name"] 				= "INVOICE_NO";
$col["width"] 				= "2";
$col["left"] 				= "center";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Supplier";
$col["name"] 				= "SUPPLIER";
$col["width"] 				= "4"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "SUPPLIER_ID";
$client_lookup 				= $jq->get_dropdown_values("SELECT
														DISTINCT
														MS.intId   	AS k,
														MS.strName 	AS v
														FROM finance_supplier_purchaseinvoice_header SPH
														INNER JOIN  mst_supplier MS ON MS.intId = SPH.SUPPLIER_ID
														WHERE
														SPH.STATUS = 1
														AND SPH.COMPANY_ID = '$session_companyId'
														ORDER BY MS.strName");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;

$col["title"] 				= "Currency";
$col["name"] 				= "CURRENCY";
$col["width"] 				= "2"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "CURRENCY_ID";
$client_lookup 				= $jq->get_dropdown_values("SELECT
														DISTINCT
														FC.intId   	AS k,
														FC.strCode 	AS v 
														FROM mst_financecurrency FC
														WHERE
														FC.intStatus = 1
														ORDER BY 
														FC.strCode");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;

$col["title"] 				= "Date";
$col["name"] 				= "DEBIT_DATE";
$col["width"] 				= "3";
$col["align"] 				= "center";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Raised By";
$col["name"] 				= "RAISED_BY";
$col["width"] 				= "2";
$col["align"] 				= "center";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Amount";
$col["name"] 				= "AMOUNT";
$col["width"] 				= "2";
$col["align"] 				= "right";
$col["sortable"] 			= true; 					
$col["search"] 				= false; 					
$col["editable"] 			= false;
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "1st Approval";
$col["name"] 				= "1st_Approval";
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q=1082&debitNo={DEBIT_NO}&debitYear={DEBIT_YEAR}&mode=Confirm';
$col["linkoptions"] 		= "target='debit_note_report.php'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;

for($i=2; $i<=$approveLevel; $i++)
{
	if($i==2){
		$ap		= "2nd Approval";
		$ap1	= "2nd_Approval";
	}
	else if($i==3){
		$ap		= "3rd Approval";
		$ap1	= "3rd_Approval";
	}
	else {
		$ap		= $i."th Approval";
		$ap1	= $i."th_Approval";
	}

$col["title"] 				= $ap; 
$col["name"] 				= $ap1; 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q=1082&debitNo={DEBIT_NO}&debitYear={DEBIT_YEAR}&mode=Confirm';
$col["linkoptions"] 		= "target='debit_note_report.php'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;
}

$col["title"] 				= "Cancel"; 
$col["name"] 				= "Cancel"; 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q=1082&debitNo={DEBIT_NO}&debitYear={DEBIT_YEAR}&mode=Cancel';
$col["linkoptions"] 		= "target='debit_note_report.php'";
$col['linkName']			= 'Cancel';
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Report";
$col["name"] 				= "View";
$col["width"] 				= "2";
$col["align"] 				= "center"; 
$col["sortable"]			= false;
$col["editable"] 			= false; 	
$col["search"] 				= false; 
$col['link']				= '?q=1082&debitNo={DEBIT_NO}&debitYear={DEBIT_YEAR}';
$col["linkoptions"] 		= "target='debit_note_report.php'";
$col['linkName']			= 'View';
$cols[] 					= $col;	
$col						= NULL;

$grid["caption"] 			= "Supplier Debit Note Listing";
$grid["multiselect"] 		= false;
$grid["rowNum"] 			= 20; 
$grid["sortname"] 			= 'DEBIT_YEAR,DEBIT_NO'; 
$grid["sortorder"] 			= "DESC"; 
$grid["autowidth"] 			= true; 
$grid["multiselect"] 		= false; 
$grid["search"] 			= true; 
$grid["postData"] 			= array("filters" => $sarr ); 
$grid["export"] 			= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Supplier Debit Note Listing</title>
<?php
echo $out;

?>
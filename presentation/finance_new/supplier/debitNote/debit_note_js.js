// JavaScript Document
$(document).ready(function(){
	
	$("#frmDebitNote").validationEngine();
	$('#frmDebitNoteRpt').validationEngine();
	
	$('#frmDebitNote #cboSupplier').die('change').live('change',loadCurrency);
	$('#frmDebitNote #cboCurrency').die('change').live('change',loadExchngRate);
	$('#frmDebitNote #cboInvoiceNo').die('change').live('change',loadDetails);
	$('#frmDebitNote .clsAmount').die('keyup').live('keyup',setMaxQty);
	$('#frmDebitNote .clsAmount').die('blur').live('blur',calTaxAndTotals);
	$('#frmDebitNote .clsChkItem').die('click').live('click',setTotalAmount);
	$('#frmDebitNote .clsChkAll').die('click').live('click',checkAll);
	$('#frmDebitNote #butSave').die('click').live('click',saveData);
	$('#frmDebitNote #tblLedger .clsDel').die('click').live('click',removeRow);
	$('#frmDebitNote #tblLedger #butInsertRow').die('click').live('click',butInsertRow);
	$('#frmDebitNote .clsTax').die('change').live('change',calTaxAndTotals);
	$('#frmDebitNote #tblLedger .clsLedgerAmount').die('keyup').live('keyup',calTotLeadgerAmount);
	$('#frmDebitNote #tblLedger .clsLedgerAc').die('change').live('change',calTotLeadgerAmount);
	
	$('#frmDebitNote #butApprove').die('click').live('click',approveReport);
	$('#frmDebitNote #butCancle').die('click').live('click',cancelReport);
	$('#frmDebitNote #butReport').die('click').live('click',report);
	
	$('#frmDebitNoteRpt #butRptConfirm').die('click').live('click',approve);
	$('#frmDebitNoteRpt #butRptReject').die('click').live('click',reject);
	$('#frmDebitNoteRpt #butRptCancel').die('click').live('click',cancel);
		
});
function loadCurrency()
{
	$('#frmDebitNote #cboInvoiceNo').html('');
	clearGridsandAmounts();
	
	if($('#frmDebitNote #cboSupplier').val()=='')
	{
		$('#frmDebitNote #cboCurrency').html('');
		$('#frmDebitNote #txtCurrencyRate').val('0.0000');
		return;
	}
	showWaiting();
	var url 	= "controller.php?q=709&requestType=loadCurrency";
	var data 	= "supplierId="+$('#frmDebitNote #cboSupplier').val()+"&date="+$('#frmDebitNote #txtDate').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmDebitNote #cboCurrency').html(json.currency);
				$('#frmDebitNote #txtCurrencyRate').val(json.exchgRate);
				$('#frmDebitNote #cboInvoiceNo').html(json.invoiceNo);
				hideWaiting();
			}
	});
	hideWaiting();
}
function loadExchngRate()
{
	$('#frmDebitNote #cboInvoiceNo').html('');
	clearGridsandAmounts()
	
	if($('#frmDebitNote #cboCurrency').val()=='')
	{
		$('#frmDebitNote #txtCurrencyRate').val('0.0000');
		return;
	}
	showWaiting();
	var url 	 = "controller.php?q=709&requestType=loadExchngRate";
	var data 	 = "currencyId="+$('#frmDebitNote #cboCurrency').val();
		data	+= "&date="+$('#frmDebitNote #txtDate').val();
		data	+= "&supplierId="+$('#frmDebitNote #cboSupplier').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmDebitNote #txtCurrencyRate').val(json.exchgRate);
				$('#frmDebitNote #cboInvoiceNo').html(json.invoiceNo);
				hideWaiting();
			}
	});
	hideWaiting();
}
function loadDetails()
{
	clearGridsandAmounts()
	
	if($('#frmDebitNote #cboInvoiceNo').val()=='')
		return;
	
	showWaiting();
	var url 	 = "controller.php?q=709&requestType=loadDetails";
	var data 	 = "invoiceNo="+$('#frmDebitNote #cboInvoiceNo').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmDebitNote #tblInvDetails tbody').html(json.invoicedDetails);
				$('#frmDebitNote #tblMain tbody').html(json.detailData);
				$('#frmDebitNote #tblLedger tbody').html(json.detailGLData);
				hideWaiting();
			}
	});
	hideWaiting();
}
function clearGridsandAmounts()
{
	$("#frmDebitNote #tblInvDetails tr:gt(0)").remove();
	$("#frmDebitNote #tblMain tr:gt(0)").remove();
	$("#frmDebitNote #tblMain .clsChkAll").prop('checked',false);
	$("#frmDebitNote #tblLedger tbody").html('');
	
	$("#frmDebitNote #txtSubTotal").val('0.00');
	$("#frmDebitNote #txtTaxTotal").val('0.00');
	$("#frmDebitNote #txtGrandTotal").val('0.00');
	
	$("#frmDebitNote #txtLedgerTotAmonut").html('0.00');
}
function setMaxQty()
{
	var balToDebit		= parseFloat($(this).parent().attr('id'));

	if(parseFloat($(this).val())>=balToDebit)
	{
		$(this).val(RoundNumber(balToDebit,2));
	}

	$('#frmDebitNote #tblLedger .clsLedgerAmount').val('0.00');
	$('#frmDebitNote #tblLedger #txtLedgerTotAmonut').val('0.00');
}
function calTaxAndTotals()
{
	calculateTax(this);
	caculateTotalValues();
}
function calculateTax(obj)
{
	var taxId	= $(obj).parent().parent().find('.clsTax').val();
	var amount	= parseFloat($(obj).parent().parent().find('.clsAmount').val()==''?0:$(obj).parent().parent().find('.clsAmount').val());
	
	if(taxId=='' || amount<=0)
	{
		$(obj).parent().parent().find('.clsTax').parent().attr('id',0);
		return;
	}
		
	var url 	 = "controller.php?q=709&requestType=calculateTaxAmount";
	var data 	 = "taxId="+taxId+"&amount="+amount;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$(obj).parent().parent().find('.clsTax').parent().attr('id',RoundNumber(json.TaxValue,2));
			}
	});
}
function caculateTotalValues()
{
	var subTotal		= 0;
	var grandTotal		= 0;
	var taxTotal		= 0;
	var totLedAmount	= 0;
	
	$('#frmDebitNote #tblMain .clsAmount').each(function(){
		if($(this).parent().parent().find('.clsChkItem').is(':checked'))
		{
			subTotal 	+= parseFloat($(this).val()==''?0:$(this).val());
			taxTotal   	+= parseFloat($(this).parent().parent().find('.clsTax').parent().attr('id'));
		}	
	});
	
	$('#frmDebitNote #tblLedger .clsLedgerAc').each(function(){
				
		if($(this).parent().parent().attr('id')=='subCat')
		{
			var totSubCatVal    = 0;
			var GLId 			= $(this).val();
			
			$('#frmDebitNote #tblMain .clsAmount').each(function(){
				if($(this).parent().parent().find('.clsChkItem').is(':checked') && GLId==$(this).parent().parent().attr('id'))
				{
					totSubCatVal += parseFloat($(this).val()==''?0:$(this).val());
				}	
			});
			totLedAmount += parseFloat(totSubCatVal);
			$(this).parent().parent().find('.clsLedgerAmount').val(RoundNumber(totSubCatVal,2).toFixed(2));
		}
		if($(this).parent().parent().attr('id')=='tax')
		{
			var totTaxVal    	= 0;
			var taxIdArr		= $(this).parent().attr('id').split(',');
			
			for(var i=0;i<taxIdArr.length;i++)
			{
				
				$('#frmDebitNote #tblMain .clsAmount').each(function(){
					
					if($(this).parent().parent().find('.clsChkItem').is(':checked') && taxIdArr[i]==$(this).parent().parent().find('.clsTax').val())
					{
						totTaxVal += parseFloat($(this).parent().parent().find('.clsTax').parent().attr('id'));
					}	
				});
			}
			totLedAmount += parseFloat(totTaxVal);
			$(this).parent().parent().find('.clsLedgerAmount').val(RoundNumber(totTaxVal,2).toFixed(2));
		}
		
	});	
	
	taxTotal	 = RoundNumber(taxTotal,2);
	subTotal	 = RoundNumber(subTotal,2);
	grandTotal	 = parseFloat(subTotal)+parseFloat(taxTotal);
	
	$('#frmDebitNote #txtTaxTotal').val(taxTotal.toFixed(2));	
	$('#frmDebitNote #txtSubTotal').val(subTotal.toFixed(2));	
	$('#frmDebitNote #txtGrandTotal').val(grandTotal.toFixed(2));
	$('#frmDebitNote #tblLedger #txtLedgerTotAmonut').html(RoundNumber(totLedAmount,2).toFixed(2));
}
function setTotalAmount()
{
	if($(this).is(':checked'))
	{
		$(this).parent().parent().find('.clsAmount').removeAttr("disabled");
		$(this).parent().parent().find('.clsAmount').val('').focus();
		
	}
	else
	{
		$(this).parent().parent().find('.clsAmount').val('0.00');	
		$(this).parent().parent().find('.clsAmount').attr("disabled", "disabled");
	}
	
	calculateTax(this);
	caculateTotalValues();
}
function checkAll()
{
	if($(this).is(':checked'))
	{
		$('#frmDebitNote .clsChkItem').attr('checked',true);
		$('#frmDebitNote .clsAmount').removeAttr("disabled");
		$('#frmDebitNote .clsAmount').val('');
	}
	else
	{
		$('#frmDebitNote .clsChkItem').attr('checked',false);
		$('#frmDebitNote .clsAmount').val('0.00');
		$('#frmDebitNote .clsAmount').attr("disabled", "disabled");
	}
	
	$('#frmDebitNote #tblMain .clsChkItem').each(function(){
		calculateTax(this);	
	});
	caculateTotalValues();
}
function removeRow()
{
	var delRowCount = $('#frmDebitNote #tblLedger tbody tr').length;
	
	if(delRowCount>1)
		$(this).parent().parent().remove();
		
	calTotLeadgerAmount();
}
function butInsertRow()
{
	$('#frmDebitNote #tblLedger tbody tr:last').after("<tr>"+$('#frmDebitNote #tblLedger tbody tr:first').html()+"</tr>");
	$('#frmDebitNote #tblLedger tbody tr:last').find('.clsLedgerAc').val('');
	$('#frmDebitNote #tblLedger tbody tr:last').find('.clsLedgerAc').parent().attr('id','');
	$('#frmDebitNote #tblLedger tbody tr:last').find('.clsLedgerAmount').val('0.00');
	
}
function calTotLeadgerAmount()
{
	var totLedAmount	= 0;
	$('#frmDebitNote #tblLedger .clsLedgerAmount').each(function(){
		
		if($(this).parent().parent().find('.clsLedgerAc').val()=='')
			$(this).val('0.00');
				
		totLedAmount	+= parseFloat(($(this).val()==''?0:$(this).val()));
	});
	
	$('#frmDebitNote #tblLedger #txtLedgerTotAmonut').html(RoundNumber(totLedAmount,2).toFixed(2));
}
function saveData()
{
	if(!IsProcessMonthLocked_date($('#frmDebitNote #txtDate').val()))
		return;
	
	showWaiting();
	var debitNo 		= $('#frmDebitNote #txtDebitNo').val();
	var debitYear 		= $('#frmDebitNote #txtDebitYear').val();
	var invoiceNo 		= $('#frmDebitNote #cboInvoiceNo').val();
	var supplierId 		= $('#frmDebitNote #cboSupplier').val();
	var currency 		= $('#frmDebitNote #cboCurrency').val();
	var debitDate 		= $('#frmDebitNote #txtDate').val();
	var grandTotal 		= $('#frmDebitNote #txtGrandTotal').val();
	var Remarks 		= $('#frmDebitNote #txtRemarks').val();
	
	if($('#frmDebitNote').validationEngine('validate'))
	{
		var totLedgerAmount	= parseFloat($('#frmDebitNote #tblLedger #txtLedgerTotAmonut').html());
		if(totLedgerAmount!=grandTotal)
		{
			$('#frmDebitNote #tblLedger #txtLedgerTotAmonut').validationEngine('showPrompt','Total Ledger amount must equal to Grand Total.','fail');
			hideWaiting();
			return;
		}
		var balToDebited 	= parseFloat($('#frmDebitNote #tblInvDetails .clsBalToDebit').html());
		if(grandTotal>balToDebited)
		{
			$('#frmDebitNote #butSave').validationEngine('showPrompt','Grand total greater than bal to Debit Amount.','fail');
			hideWaiting();
			return;
		}
		
		var data 		= "requestType=saveData";
		var arrHeader 	= "{";
							arrHeader += '"debitNo":"'+debitNo+'",' ;
							arrHeader += '"debitYear":"'+debitYear+'",' ;
							arrHeader += '"invoiceNo":"'+invoiceNo+'",' ;
							arrHeader += '"supplierId":"'+supplierId+'",' ;
							arrHeader += '"currency":"'+currency+'",' ;
							arrHeader += '"debitDate":"'+debitDate+'",' ;
							arrHeader += '"Remarks":'+URLEncode_json(Remarks)+'';
		arrHeader 	   += "}";
		var arrHeader	= arrHeader;
		
		var arrDetails		= "";
		var status			= false;
		var qtyChkStatus 	= false;
		$('#frmDebitNote #tblMain .clsChkItem').each(function(){
			
			if($(this).is(':checked'))
			{
				status 			= true;
				var itemId 		= $(this).parent().parent().find('.clsItem').attr('id');
				var amount 		= $(this).parent().parent().find('.clsAmount').val();
				var taxAmount 	= $(this).parent().parent().find('.clsTax').parent().attr('id');
				var taxCode 	= $(this).parent().parent().find('.clsTax').val();
				var costCenter 	= $(this).parent().parent().find('.clsCostCener').val();
			
				if(amount<=0 || amount=='')
				{
					$(this).parent().parent().find('.clsAmount').validationEngine('showPrompt','Amount must greater than 0.','fail');
					qtyChkStatus = true;
					hideWaiting();
					return false;
				}
				
				arrDetails += "{";
				arrDetails += '"itemId":"'+ itemId +'",' ;
				arrDetails += '"amount":"'+ amount +'",' ;
				arrDetails += '"taxAmount":"'+ taxAmount +'",' ;
				arrDetails += '"taxCode":"'+ taxCode +'",' ;
				arrDetails += '"costCenter":"'+ costCenter +'"' ;
				arrDetails +=  '},';
			}
			
		});
		
		if(qtyChkStatus)
		{
			return;
		}
		if(!status)
		{
			$(this).validationEngine('showPrompt','No records to save.','fail');
			hideWaiting();	
			return;
		}
		
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		var arrDetails	= '['+arrDetails+']';
		
		var arrGLDetails	= "";
		var GLstatus		= false;
		$('#frmDebitNote #tblLedger .clsLedgerAmount').each(function(){
			
			var ledgerAccount 	= $(this).parent().parent().find('.clsLedgerAc').val();
			var amount 			= $(this).val();
			
			if(amount>0 && amount!='')
			{
				GLstatus	= true;
				arrGLDetails += "{";
				arrGLDetails += '"ledgerAccount":"'+ ledgerAccount +'",' ;
				arrGLDetails += '"amount":"'+ amount +'"' ;
				arrGLDetails +=  '},';
			}
			
		});
		if(!GLstatus)
		{
			$(this).validationEngine('showPrompt','No Ledger records to save.','fail');
			hideWaiting();	
			return;
		}
		
		arrGLDetails 		= arrGLDetails.substr(0,arrGLDetails.length-1);
		var arrGLDetails	= '['+arrGLDetails+']';
		data	   	   	   += "&arrHeader="+arrHeader+"&arrDetails="+arrDetails+"&arrGLDetails="+arrGLDetails;
		
		var url = "controller.php?q=709";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmDebitNote #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						$('#frmDebitNote #txtDebitNo').val(json.debitNo);
						$('#frmDebitNote #txtDebitYear').val(json.debitYear);
						$('#frmDebitNote #butApprove').show();
						$('#frmDebitNote #butCancle').show();
						$('#frmDebitNote #butReport').show();
						$('#frmDebitNote #butSave').show();
						
						if(!json.approvePerm)
							$('#frmDebitNote #butApprove').hide();
						if(!json.cancelPerm)
							$('#frmDebitNote #butCancle').hide();
						if(!json.editPerm)
							$('#frmDebitNote #butSave').hide();
						
						
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
					$('#frmDebitNote #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					hideWaiting();
					return;
				}		
		});
	}
	else
	{
		hideWaiting();
	}
}
function approve()
{
	var val = $.prompt('Are you sure you want to approve this Debit Note ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
			showWaiting();
			var url = "controller.php"+window.location.search+'&requestType=approve';
			var obj = $.ajax({
				url:url,
				type:'post',
				dataType: "json",  
				data:'',
				async:false,						
				success:function(json){
					$('#frmDebitNoteRpt #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx1()",1000);
						window.location.href = window.location.href;
						window.opener.location.reload();
						return;
					}
					 hideWaiting();
				},
				error:function(xhr,status){						
						$('#frmDebitNoteRpt #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx1()",3000);
						 hideWaiting();
				}		
			});	
		}			  
	}});
}
function cancel()
{
	var val = $.prompt('Are you sure you want to cancel this Debit Note ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
			showWaiting();
			var url = "controller.php"+window.location.search+'&requestType=cancel';
			var obj = $.ajax({
				url:url,
				type:'post',
				dataType: "json",  
				data:'',
				async:false,						
				success:function(json){
					$('#frmDebitNoteRpt #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx2()",1000);
						window.location.href = window.location.href;
						window.opener.location.reload();
						return;
					}
					 hideWaiting();
				},
				error:function(xhr,status){						
						$('#frmDebitNoteRpt #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx2()",3000);
						 hideWaiting();
				}		
			});	
		}			  
	}});
}
function reject()
{
	var val = $.prompt('Are you sure you want to reject this Debit Note ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
			showWaiting();
			var url = "controller.php"+window.location.search+'&requestType=reject';
			var obj = $.ajax({
				url:url,
				type:'post',
				dataType: "json",  
				data:'',
				async:false,						
				success:function(json){
					$('#frmDebitNoteRpt #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx3()",1000);
						window.location.href = window.location.href;
						window.opener.location.reload();
						return;
					}
					 hideWaiting();
				},
				error:function(xhr,status){						
						$('#frmDebitNoteRpt #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx3()",3000);
						 hideWaiting();
				}		
			});	
		}			  
	}});
}
function approveReport()
{
	if($('#frmDebitNote #txtDebitNo').val()=="") 
		return;
		
	var url = "?q=1082&debitNo="+$('#frmDebitNote #txtDebitNo').val()+"&debitYear="+$('#frmDebitNote #txtDebitYear').val()+"&mode=Confirm";
	window.open(url,'debit_note_report.php');
}
function cancelReport()
{
	if($('#frmDebitNote #txtDebitNo').val()=="") 
		return;
		
	var url = "?q=1082&debitNo="+$('#frmDebitNote #txtDebitNo').val()+"&debitYear="+$('#frmDebitNote #txtDebitYear').val()+"&mode=Cancel";
	window.open(url,'debit_note_report.php');
}
function report()
{
	if($('#frmDebitNote #txtDebitNo').val()=="") 
		return;
		
	var url = "?q=1082&debitNo="+$('#frmDebitNote #txtDebitNo').val()+"&debitYear="+$('#frmDebitNote #txtDebitYear').val();
	window.open(url,'debit_note_report.php');
}
function alertx()
{
	$('#frmDebitNote #butSave').validationEngine('hide')	;
}
function alertx1()
{
	$('#frmDebitNoteRpt #butRptConfirm').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmDebitNoteRpt #butRptCancel').validationEngine('hide')	;
}
function alertx3()
{
	$('#frmDebitNoteRpt #butRptReject').validationEngine('hide')	;
}
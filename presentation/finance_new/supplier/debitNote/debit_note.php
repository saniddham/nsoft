<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/tables/mst_supplier.php";								$mst_supplier				= new mst_supplier($db);
include_once "class/tables/mst_financecurrency.php";						$mst_financecurrency		= new mst_financecurrency($db);
include_once "class/tables/menupermision.php";								$menupermision 				= new menupermision($db);
include_once "class/tables/menus_special.php";								$menus_special 				= new menus_special($db);
include_once "class/tables/mst_financeexchangerate.php";					$mst_financeexchangerate 	= new mst_financeexchangerate($db);
include_once "class/tables/mst_maincategory.php";							$mst_maincategory			= new mst_maincategory($db);
include_once "class/tables/mst_subcategory.php";							$mst_subcategory			= new mst_subcategory($db);
include_once "class/tables/mst_units.php";									$mst_units					= new mst_units($db);
include_once "class/tables/mst_financetaxgroup.php";						$mst_financetaxgroup		= new mst_financetaxgroup($db);
include_once "class/tables/mst_financedimension.php";						$mst_financedimension		= new mst_financedimension($db);
include_once "class/tables/finance_supplier_transaction.php";				$finance_supplier_transaction 						= new finance_supplier_transaction($db);
include_once "class/tables/finance_supplier_purchaseinvoice_header.php";	$finance_supplier_purchaseinvoice_header			= new finance_supplier_purchaseinvoice_header($db);
include_once "class/tables/finance_supplier_debitnote_header.php";			$finance_supplier_debitnote_header					= new finance_supplier_debitnote_header($db);
include_once "class/tables/finance_supplier_debitnote_details.php";			$finance_supplier_debitnote_details					= new finance_supplier_debitnote_details($db);
include_once "class/tables/finance_supplier_debitnote_gl.php";				$finance_supplier_debitnote_gl						= new finance_supplier_debitnote_gl($db);

include_once "class/finance/cls_get_gldetails.php";							$obj_get_GLCombo			= new Cls_Get_GLDetails($db);

/*$locationId 	= $sessions->getLocationId();
$companyId 		= $sessions->getCompanyId();
$userId  		= $sessions->getUserId();*/

$spMenuId		= 64;
$programCode	= 'P0709';
$debitNo		= (!isset($_REQUEST['debitNo'])?'':$_REQUEST['debitNo']);
$debitYear		= (!isset($_REQUEST['debitYear'])?'':$_REQUEST['debitYear']);
$status			= 'null';
$levels			= 'null';

$totalAmount	= 0;
$totalTaxAmount	= 0;
$totalGLAmount	= 0;
$exchgRate		= 0;
$balToDebit		= 0;

$db->connect();
$menupermision->set($programCode,$sessions->getUserId());
if($debitNo!='' && $debitYear!='')
{
	$finance_supplier_debitnote_header->set($debitNo,$debitYear);
	
	$status					= $finance_supplier_debitnote_header->getSTATUS();
	$levels					= $finance_supplier_debitnote_header->getAPPROVE_LEVELS();
	$supplierId				= $finance_supplier_debitnote_header->getSUPPLIER_ID();
	$currencyId				= $finance_supplier_debitnote_header->getCURRENCY_ID();
	$debitDate				= $finance_supplier_debitnote_header->getDEBIT_DATE();
	$invoiceNo				= $finance_supplier_debitnote_header->getPURCHASE_INVOICE_NO();
	$invoiceYear			= $finance_supplier_debitnote_header->getPURCHASE_INVOICE_YEAR();
	$remarks				= $finance_supplier_debitnote_header->getREMARKS();
	
	if($mst_financeexchangerate->checkCurrencyAvailableForSelectedDate($debitDate,$sessions->getCompanyId()))
	{
		$mst_financeexchangerate->set($currencyId,$debitDate,$sessions->getCompanyId());
		$exchgRate			= $mst_financeexchangerate->getdblExcAvgRate();
	}
	
	$invoiceDetailData		= $finance_supplier_transaction->getInvoiceTransactionDetails($invoiceNo,$invoiceYear);
	$detailData				= $finance_supplier_debitnote_details->getDebitNoteDetailData($debitNo,$debitYear);
	$GLDetailData			= $finance_supplier_debitnote_gl->getGLDetailData($debitNo,$debitYear);
	
	
	$perSaveArr				= $menupermision->checkMenuPermision('Edit',$status,$levels);
	$perConfirmArr			= $menupermision->checkMenuPermision('Approve',$status,$levels);
	$perCancelArr			= $menupermision->checkMenuPermision('Cancel',$status,$levels);
}
else
{
	$supplierId				= (!isset($_REQUEST['SupplierId'])?'':$_REQUEST['SupplierId']);
	$currencyId				= (!isset($_REQUEST['CurrencyId'])?'':$_REQUEST['CurrencyId']);
	$invoiceNoArr			= (!isset($_REQUEST['InvoiceNo'])?'':$_REQUEST['InvoiceNo']);
	$invoiceNoArr			= explode('-',$invoiceNoArr);
	$invoiceNo				= $invoiceNoArr[1];
	$invoiceYear			= $invoiceNoArr[0];
	
	$perSaveArr				= $menupermision->checkMenuPermision('Edit','null','null');
	$perConfirmArr			= $menupermision->checkMenuPermision('Approve','null','null');
	$perCancelArr			= $menupermision->checkMenuPermision('Cancel','null','null');
	$invoiceDetailData		= '';
	$detailData				= '';
	$GLDetailData			= '';
}
$backDate_perm				= $menus_special->loadSpecialMenuPermission($spMenuId,$sessions->getUserId());
$db->disconnect();
?>
<title>Supplier Debit Note</title>

<script type="text/jscript" src="presentation/finance_new/supplier/debitNote/debit_note_js.js"></script>

<style>
#apDiv1 {
	position: absolute;
	left: 106px;
	top: 120px;
	width: auto;
	height: auto;
	z-index: 1;
}
</style>

<?php
if($status==-2)//pending
{
?>
	<div id="apDiv1" style="display:"><img src="images/cancelled.png" /></div>
<?php
}
if(isset($_REQUEST['InvoiceNo']))
	echo '<body onload="loadDetails();GetCommonExchangeRate()">';
else
	echo '<body>';
?>
<form id="frmDebitNote" name="frmDebitNote" autocomplete="off" action="debitNote.php" method="post">
<div align="center">
<div class="trans_layoutL" style="width:1100px">
<div class="trans_text">Debit Note</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
              <td class="normalfnt">Debit No</td>
              <td class="normalfnt"><input name="txtDebitNo" type="text" disabled="disabled" id="txtDebitNo" style="width:60px" value="<?php echo $debitNo; ?>" />&nbsp;<input name="txtDebitYear" type="text" disabled="disabled" id="txtDebitYear" style="width:35px" value="<?php echo $debitYear; ?>" /></td>
              <td class="normalfnt">Date</td>
              <td class="normalfnt"><input name="txtDate" type="text" value="<?php echo($debitDate==''?date("Y-m-d"):$debitDate); ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtDate" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($backDate_perm!=1?'disabled="disabled"':''); ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
            </tr>
            <tr>
              <td width="15%" class="normalfnt">Supplier <span class="compulsoryRed">*</span></td>
              <td width="49%" class="normalfnt"><select name="cboSupplier" id="cboSupplier"  style="width:240px" class="validate[required]" >
              <option value=""></option>
              <?php
			  	$db->connect();
			  	$result	= $finance_supplier_purchaseinvoice_header->getConfirmSupplier($sessions->getCompanyId());
				while($row = mysqli_fetch_array($result))
				{
					if($supplierId==$row['SUPPLIER_ID'])
						echo '<option selected="selected" value="'.$row['SUPPLIER_ID'].'">'.$row['SUPPLIER_NAME'].'</option>';	
					else
						echo '<option value="'.$row['SUPPLIER_ID'].'">'.$row['SUPPLIER_NAME'].'</option>';	
				}
				$db->disconnect();
			  ?>
              </select></td>
              <td width="16%" class="normalfnt">Currency <span class="compulsoryRed">*</span></td>
              <td width="20%" class="normalfnt"><select name="cboCurrency" id="cboCurrency"  style="width:102px" class="validate[required] cls_cbo_CurrencyCalExchangeRate" >
				<?php
				$db->connect();
					$result	= $finance_supplier_purchaseinvoice_header->getSupplierWiseCurrency($supplierId,$sessions->getCompanyId());
					while($row=mysqli_fetch_array($result))
					{
						if($currencyId==$row['CURRENCY_ID'])
							echo "<option value=\"".$row['CURRENCY_ID']."\" selected=\"selected\">".$row['CURRENCY_CODE']."</option>";
						else
							echo "<option value=\"".$row['CURRENCY_ID']."\">".$row['CURRENCY_CODE']."</option>";
					}
				$db->disconnect();
                ?>
              </select></td>
              </tr>
            <tr>
              <td class="normalfnt">Invoice No <span class="compulsoryRed">*</span></td>
              <td class="normalfnt"><select name="cboInvoiceNo" id="cboInvoiceNo" style="width:240px" class="validate[required]">
				<?php
				$db->connect();
                	$result = $finance_supplier_purchaseinvoice_header->getSupplierCurrencyWiseInvoiceNo($supplierId,$currencyId,$sessions->getCompanyId());
					while($row = mysqli_fetch_array($result))
					{
						if($invoiceNo==$row['PURCHASE_INVOICE_NO'] && $invoiceYear==$row['PURCHASE_INVOICE_YEAR'])
							echo "<option value=\"".$row['PURCHASE_INVOICE_YEAR']."/".$row['PURCHASE_INVOICE_NO']."\" selected=\"selected\">".$row['PURCHASE_INVOICE_NO']."/".$row['PURCHASE_INVOICE_YEAR']." - ".$row['INVOICE_NO']."</option>";
						else
							echo "<option value=\"".$row['PURCHASE_INVOICE_YEAR']."/".$row['PURCHASE_INVOICE_NO']."\">".$row['PURCHASE_INVOICE_NO']."/".$row['PURCHASE_INVOICE_YEAR']." - ".$row['INVOICE_NO']."</option>";
					}
				$db->disconnect();
                ?>
              </select></td>
              <td class="normalfnt">Rate</td>
              <td class="normalfnt"><input name="txtCurrencyRate" id="txtCurrencyRate" type="text" value="<?php echo number_format($exchgRate, 4, '.', ''); ?>" style="width:98px;text-align:right" disabled="disabled" class="cls_txt_exchangeRate"/></td>
            </tr>
            <tr>
              <td class="normalfnt" valign="top">Remark</td>
              <td rowspan="2" valign="top" class="normalfnt"><textarea name="txtRemarks" id="txtRemarks" style="width:236px" rows="3"><?php echo $remarks; ?></textarea></td>
              <td class="normalfnt">&nbsp;</td>
              <td rowspan="2" valign="top" class="normalfnt">&nbsp;</td>
              </tr>
            <tr>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
              </tr>
            <tr>
            	<td></td>
            </tr>
             <tr>
              <td colspan="4" class="normalfnt">
              <div style="overflow:auto;width:1100px;" id="divInvDetails">
              <table width="100%" class="bordered" id="tblInvDetails" >
              	<thead>
                	<tr>
                    	<th width="12%" >Invoice Date</th>
                    	<th width="12%" >Invoice Amount</th>
                        <th width="12%" >Advance Amount</th>
                        <th width="12%" >Payment Amount</th>
                        <th width="12%" >Credit Note</th>
                        <th width="12%" >Debit Note</th>
                        <th width="12%" >Bal to Debit Note</th>
                    </tr>
                </thead>
                <tbody>
   				<?php
					$htmlT		 = '';
					while($row = mysqli_fetch_array($invoiceDetailData))
					{
						$htmlT	.= "<tr>";
						$htmlT	.= "<td style=\"text-align:center\" >".$row['INVOICE_DATE']."</td>";
						$htmlT	.= "<td style=\"text-align:right\" >".$row['invoiceAmount']."</td>";
						$htmlT	.= "<td style=\"text-align:right\" >".$row['advancedAmount']."</td>";
						$htmlT	.= "<td style=\"text-align:right\" >".$row['paidAmount']."</td>";
						$htmlT	.= "<td style=\"text-align:right\" >".$row['creditAmount']."</td>";
						$htmlT	.= "<td style=\"text-align:right\" >".$row['debitAmount']."</td>";
						$htmlT	.= "<td style=\"text-align:right\" class=\"clsBalToDebit\" >".$row['toBeDebited']."</td>";
						$htmlT	.= "</tr>";
						
						$balToDebit	= $row['toBeDebited'];
					}
					echo $htmlT;
				?>
                </tbody>
              </table>
              </div>
              </td>
              </tr>
             <tr>
            	<td></td>
            </tr>
            <tr>
              <td colspan="4" class="normalfnt">
              <div style="overflow:auto;width:1100px;height:250px;" id="divGrid">
              <table width="100%" class="bordered" id="tblMain" >
                <thead>
                    <tr>
                        <th width="3%" height="30" ><input class="clsChkAll" type="checkbox" id="chkAll"/></th>
                        <th width="14%" >Main Category</th>
                        <th width="15%" >Sub Category</th>
                        <th width="17%" >Item</th>
                        <th width="8%" >UOM</th>
                         <th width="9%" >Invoiced Amount</th>
                          <th width="9%" >Debited Amount</th>
                        <th width="7%" >Amount</th>
                        <th width="9%" >Tax</th>
                        <th width="9%" >Cost Center </th>
                    </tr>
                </thead>
                <tbody>
                	<?php
					$db->connect();
					$htmlDt		 = '';
					while($rowDt = mysqli_fetch_array($detailData))
					{
						$mst_maincategory->set($rowDt['MAIN_CAT_ID']);
						$mst_subcategory->set($rowDt['SUB_CAT_ID']);
						$mst_units->set($rowDt['UNIT_ID']);
						
						$debitedAmount	 = $finance_supplier_debitnote_details->getItemWiseDebitedAmount($invoiceNo,$invoiceYear,$rowDt['ITEM_ID']);
						$totalAmount	+= $rowDt['AMOUNT'];
						$totalTaxAmount	+= $rowDt['TAX_AMOUNT'];
						
						$htmlDt	.= '<tr class="normalfnt" id="'.$rowDt['GLACCOUNT_ID'].'">';
						$htmlDt	.= '<td style="text-align:center"><input type="checkbox" id="chkItem" name="chkItem" class="clsChkItem validate[minCheckbox[1]]" checked="checked"></td>';
						$htmlDt	.= '<td class="clsMainCat" id="'.$rowDt['MAIN_CAT_ID'].'" style="text-align:left">'.$mst_maincategory->getstrName().'</td>';
						$htmlDt	.= '<td class="clsSubCat" id="'.$rowDt['SUB_CAT_ID'].'" style="text-align:left">'.$mst_subcategory->getstrName().'</td>';
						$htmlDt	.= '<td class="clsItem" id="'.$rowDt['ITEM_ID'].'" style="text-align:left">'.$rowDt['ITEM_NAME'].'</td>';
						$htmlDt	.= '<td class="clsUOM" id="'.$rowDt['UNIT_ID'].'" style="text-align:left">'.$mst_units->getstrName().'</td>';
						$htmlDt	.= '<td style="text-align:right">'.number_format($rowDt['INVOICE_AMOUNT'], 2, '.', '').'</td>';
						$htmlDt	.= '<td style="text-align:right">'.number_format($debitedAmount, 2, '.', '').'</td>';
						$htmlDt	.= '<td style="text-align:center" id="'.($rowDt['INVOICE_AMOUNT']-$debitedAmount).'"><input type="text" id="txtAmount" class="validate[required,decimal[2],custom[number]] cls_input_number_validation clsAmount" style="width:78px;text-align:right" value="'.number_format($rowDt['AMOUNT'], 2, '.', '').'"></td>';
						$htmlDt .= '<td style="text-align:center" id="'.$rowDt['TAX_AMOUNT'].'"><select name="cboTax" id="cboTax" class="clsTax" style="width:92px">
									'.$mst_financetaxgroup->getCombo($rowDt['TAX_CODE'],'intStatus=1').'</select></td>';
						$htmlDt .= '<td style="text-align:center"><select name="cboCostCenter" id="cboCostCenter" class="clsCostCener" style="width:112px" disabled="disabled">
									'.$mst_financedimension->getCombo($rowDt['COST_CENTER'],'intStatus=1').'</select></td>';
						$htmlDt .= '</tr>';
					}
					echo $htmlDt;
					$db->disconnect();
					?>
                </tbody>
              </table>
                </div></td>
            </tr>
            <tr>
              <td colspan="2" rowspan="7" class="normalfnt" valign="top">
              <div style="overflow:auto;overflow-x:hidden;width:450px;height:150px;" id="divGrid">
              <table width="400" class="bordered" id="tblLedger" >
                <thead>
                    <tr>
                      <th colspan="3" >GL Allocation
                        <div style="float:right"><a class="button white small" id="butInsertRow">Add New GL</a></div></th>
                      </tr>
                    <tr>
                    <tr>
                        <th width="9%" >Del</th>
                        <th width="67%" >Ledger Account <span class="compulsoryRed">*</span></th>
                        <th width="24%" >Amount <span class="compulsoryRed">*</span></th>
                    </tr>
                </thead>
                <tbody>
				<?php
				$db->connect();
				$htmlGL		 = ''; 
				if($debitNo!='')
				{ 
					while($rowGL = mysqli_fetch_array($GLDetailData))
					{
						if($rowGL['TAX_ID']=='')
							$glType	= 'subCat';
						else
							$glType	= 'tax';
						
						$totalGLAmount	+= $rowGL['AMOUNT'] ;
						
						$htmlGL	.= '<tr id="'.$glType.'">';
						$htmlGL	.= '<td style="text-align:center" ><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>';
						$htmlGL	.= '<td style="text-align:center" id="'.$rowGL['TAX_ID'].'"><select name="cboLedgerAc" id="cboLedgerAc" class="clsLedgerAc" style="width:100%">
						   '.$obj_get_GLCombo->getGLCombo('SUPPLIER_DEBITNOTE',$rowGL['CHART_OF_ACCOUNT_ID']).'</select></td>';
						$htmlGL	.= '<td style="text-align:center"><input name="txtLedgerAmonut" id="txtLedgerAmonut" class="validate[required,decimal[2],custom[number]] cls_input_number_validation clsLedgerAmount" type="text" style="width:98%;text-align:right" value="'.number_format($rowGL['AMOUNT'], 2, '.', '').'"/></td>';
						$htmlGL	.= '</tr>';
					}
					echo $htmlGL;
				}
				$db->disconnect();
				?>
                </tbody>
                <tfoot>
                	<tr>
                    	<td colspan="2" align="right">Total :</td>
                        <td style="text-align:right" id="txtLedgerTotAmonut" class="clsLedgerTotAmount"><?php echo number_format($totalGLAmount, 2, '.', ''); ?></td>
                    </tr>
                </tfoot>
                </table>
                </div></td>
              <td class="normalfnt" style="text-align:right">Sub Total</td>
              <td class="normalfnt" style="text-align:right"><input name="txtSubTotal" type="text" disabled="disabled" id="txtSubTotal" style="width:120px;text-align:right" value="<?php echo number_format($totalAmount, 2, '.', ''); ?>" /></td>
            </tr>
            <tr>
              <td class="normalfnt" style="text-align:right">Tax Total</td>
              <td class="normalfnt" style="text-align:right"><span class="normalfnt" style="text-align:right">
                <input name="txtTaxTotal" type="text" disabled="disabled" id="txtTaxTotal" style="width:120px;text-align:right" value="<?php echo number_format($totalTaxAmount, 2, '.', ''); ?>" />
              </span></td>
            </tr>
            <tr>
              <td class="normalfnt" style="text-align:right">Grand Total</td>
              <td class="normalfnt" style="text-align:right"><span class="normalfnt" style="text-align:right">
                <input name="txtGrandTotal" type="text" disabled="disabled" id="txtGrandTotal" style="width:120px;text-align:right" value="<?php echo number_format($totalAmount+$totalTaxAmount, 2, '.', ''); ?>" />
              </span></td>
            </tr>
            <tr>
            	<td class="normalfnt">&nbsp;</td>
                <td class="normalfnt" style="text-align:right"></td>
           	  </tr>
            <tr>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
            </tr>
            <tr>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
            </tr>
            
            </table>
        </td>
    </tr>
    
    <tr>
    	<td height="32" >
    		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td align="center" ><a class="button white medium" id="butNew" name="butNew" href="?q=709">New</a><a class="button white medium" id="butSave" name="butSave" <?php echo(!$perSaveArr['type']?'style="display:none"':''); ?>>Save</a><a class="button white medium" id="butApprove" name="butApprove" <?php echo(!$perConfirmArr['type']?'style="display:none"':''); ?>>Approve</a><a class="button white medium" id="butCancle" name="butCancle" <?php echo(!$perCancelArr['type']?'style="display:none"':''); ?>>Cancel</a><a class="button white medium" id="butReport" name="butReport" <?php echo($debitNo==''?'style="display:none"':''); ?>>Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
              </tr>
            </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>
</body>
</html>
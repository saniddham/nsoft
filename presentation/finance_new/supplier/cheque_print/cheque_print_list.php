<?php
ini_set('display_errors',0);
session_start();
$companyId				= $_SESSION["headCompanyId"];
$backwardseperator 		= "../../../../";
require_once "../../../../dataAccess/Connector.php";
require_once("../../../../libraries/jqgrid2/inc/jqgrid_dist.php");

$g 						= new jqgrid();
$col 					= array();
$col["title"] 			= "Payment No";
$col["name"] 			= "CONCAT_PAYMENT_NO";
$col["width"] 			= "15";
$col["hidden"] 			= false; // hide column by default
$cols[] 				= $col;	

$col 					= array();
$col["title"] 			= "Cheque Issue Date";
$col["name"] 			= "PAY_DATE"; 
$col["width"] 			= "25";
$col["editable"] 		= true; // this column is editable
$col["editoptions"] 	= array("size"=>10); // with default display of textbox with size 20
$col["editrules"]		= array("required"=>true); // required:true(false), number:true(false), minValue:val, maxValue:val
$col["formatter"] 		= "date"; // format as date
$col["search"] 			= false;
$cols[] 				= $col;


$col 					= array();
$col["title"] 			= "Bank";
$col["name"] 			= "BANK_NAME";
$col["width"] 			= "50";
$col["hidden"] 			= false; // hide column by default
$col["dbname"] 			= "BANK_ID";
$client_lookup 			= $g->get_dropdown_values("SELECT DISTINCT
													  FCOA.CHART_OF_ACCOUNT_ID 		AS k,
													  FCOA.CHART_OF_ACCOUNT_NAME	AS v
													FROM finance_supplier_payment_header FSPH
													  INNER JOIN finance_supplier_payment_gl FSPGL
														ON FSPGL.PAYMENT_NO = FSPH.PAYMENT_NO
														  AND FSPGL.PAYMENT_YEAR = FSPH.PAYMENT_YEAR
													  INNER JOIN finance_mst_chartofaccount FCOA
														ON FCOA.CHART_OF_ACCOUNT_ID = FSPGL.ACCOUNT_ID
													WHERE FSPH.COMPANY_ID = '$companyId'");
$col["stype"] 			= "select";
$str 					= ":;".$client_lookup;  // all row, blank row, then all db values. ; is separator
$col["searchoptions"] = array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	

$col 					= array();
$col["title"] 			= "Voucher No"; // caption of column
$col["name"] 			= "VOUCHER_NO"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] 			= "40";
$col["hidden"] 			= false; // hide column by default
$cols[] 				= $col;

$col 					= array();
$col["title"] 			= "Payment Date"; // caption of column
$col["name"] 			= "PAY_DATE"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] 			= "40";
$col["hidden"] 			= false; // hide column by default
$cols[] 				= $col;	

$col 					= array();
$col["title"] 			= "Supplier Name";
$col["name"] 			= "SUPPLIER_NAME";
$col["width"] 			= "50";
$col["hidden"] 			= false; // hide column by default
$col["dbname"] 			= "SUPPLIER_ID";
$client_lookup 			= $g->get_dropdown_values("SELECT DISTINCT
													  S.intId 	AS k,
													  S.strName	AS v
													FROM finance_supplier_payment_header FSPH
													  INNER JOIN mst_supplier S
														ON S.intId = FSPH.SUPPLIER_ID
													WHERE FSPH.COMPANY_ID = '$companyId'
													ORDER BY S.strName");
$col["stype"] 			= "select";
$str 					= ":;".$client_lookup;  // all row, blank row, then all db values. ; is separator
$col["searchoptions"] = array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	

$col 					= array();
$col["title"] 			= "Total";
$col["name"] 			= "AMOUNT";
$col["width"] 			= "10";
$col["align"] 			= "right";
//$col["editable"] 		= true;
//$col["editoptions"] 	= array("value"=>'10');
// can be switched to select (dropdown)
# $col["edittype"] 		= "select"; // render as select
# $col["editoptions"] 	= array("value"=>'10:$10;20:$20;30:$30;40:$40;50:$50'); // with these values "key:value;key:value;key:value"
$cols[] 				= $col;

$col 					= array();
$col["title"] 			= "Voucher";
$col["name"] 			= "VOUCHER";
$col["width"] 			= "10";
$col["align"] 			= "center";
$col["search"] 			= false;
$col["sortable"] 		= false;
$col['link']			= '../../reports/rpt_payment_voucher_pdf.php?SerialNo={PAYMENT_NO}&SerialYear={PAYMENT_YEAR}&Type=PAYMENT';
$col["linkoptions"] 	= "target='rptPayment_voucher.php'";
$col["default"] 		= "Voucher"; // default link text
$cols[] 				= $col;

# Custom made column to show link, must have default value as it's not db driven
$col 					= array();
$col["title"] 			= "View";
$col["name"] 			= "REPORT";
$col["width"] 			= "10";
$col["align"] 			= "center";
$col["search"] 			= false;
$col["sortable"] 		= false;
$col['link']			= '../supplierPayment/rptSupplierPayment.php?paymentNo={PAYMENT_NO}&paymentYear={PAYMENT_YEAR}';
$col["linkoptions"] 	= "target='rptSupplierPayment.php'"; // extra params with <a> tag
$col["default"] 		= "Report"; // default link text
$cols[] 				= $col;

# Customization of Action column width and other properties
$col 					= array();
$col["title"] 			= "Action";
$col["name"] 			= "act";
$col["width"] 			= "50";
$col["hidden"] 			= true;
$cols[] 				= $col;

$col 					= array();
$col["title"] 			= "PAYMENT_NO";
$col["name"] 			= "PAYMENT_NO";
$col["width"] 			= "40";
$col["hidden"] 			= true; // hide column by default
$cols[] 				= $col;	

$col 					= array();
$col["title"] 			= "PAYMENT_YEAR";
$col["name"] 			= "PAYMENT_YEAR";
$col["width"] 			= "40";
$col["hidden"] 			= true; // hide column by default
$cols[] 				= $col;	


// $grid["url"] 		= ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 500; // by default 20
$grid["sortname"] 		= 'VOUCHER_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "desc"; // ASC or DESC
$grid["caption"] 		= "Supplier Cheque Printing"; // caption of grid
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= true; // allow you to multi-select through checkboxes

$grid["rowactions"] 	= true; // allow you to multi-select through checkboxes

// export XLS file
// export to excel parameters
$grid["export"] 		= array("format"=>"pdf", "filename"=>"my-file", "sheetname"=>"test");

// RTL support
// $grid["direction"] = "rtl";

// $grid["cellEdit"] = true; // inline cell editing, like spreadsheet

$g->set_options($grid);

$g->set_actions(array(	
						"add"=>false, // allow/disallow add
						"edit"=>true, // allow/disallow edit
						"delete"=>false, // allow/disallow delete
						"rowactions"=>false, // show/hide row wise edit/del/save option
						"export"=>true, // show/hide export to excel option
						"autofilter" => true, // show/hide autofilter for search
						"search" => "advance" // show single/multi field search condition (e.g. simple or advance)
					) 
				);

// you can provide custom SQL query to display data
$sql = "SELECT SUB_1.* FROM
			(SELECT
				FSPH.PAYMENT_NO										AS PAYMENT_NO,
				FSPH.PAYMENT_YEAR									AS PAYMENT_YEAR,
				CONCAT(FSPH.PAYMENT_NO,'/',FSPH.PAYMENT_YEAR) 		AS CONCAT_PAYMENT_NO,
			   	FSPH.VOUCHER_NO										AS VOUCHER_NO,
			   	FSPH.PAY_DATE										AS PAY_DATE,
				S.intId												AS SUPPLIER_ID,
			   	S.strName											AS SUPPLIER_NAME,
				FCOA.CHART_OF_ACCOUNT_ID								AS BANK_ID,
				FCOA.CHART_OF_ACCOUNT_NAME							AS BANK_NAME,
				(SELECT ROUND(SUM(FSPD.AMOUNT),2) 
					FROM finance_supplier_payment_details FSPD
				WHERE FSPD.PAYMENT_NO = FSPH.PAYMENT_NO 
					AND FSPD.PAYMENT_YEAR = FSPH.PAYMENT_YEAR) 		AS AMOUNT,
				
			   	'Report'											AS REPORT,
				'Voucher'											AS VOUCHER
			 FROM finance_supplier_payment_header FSPH
			 INNER JOIN finance_supplier_payment_gl FSPGL 
			 	ON FSPGL.PAYMENT_NO = FSPH.PAYMENT_NO
				AND FSPGL.PAYMENT_YEAR = FSPH.PAYMENT_YEAR
			INNER JOIN finance_mst_chartofaccount FCOA
				ON FCOA.CHART_OF_ACCOUNT_ID = FSPGL.ACCOUNT_ID
			 INNER JOIN mst_supplier S ON S.intId = FSPH.SUPPLIER_ID
			 WHERE FSPH.COMPANY_ID = '$companyId'
			 AND CHEQUE_PRINT_STATUS = 0)  
		AS SUB_1 WHERE 1=1";
$g->select_command = $sql;

// this db table will be used for add,edit,delete
$g->table = "finance_supplier_payment_header";

// pass the cooked columns to grid
$g->set_columns($cols);

// generate grid output, with unique grid name as 'list1'
$out = $g->render("list1");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<title>Supplier Cheque Printing</title>
<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqgrid2/js/themes/smoothness/jquery-ui.custom.css">
</link>
<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqgrid2/js/jqgrid/css/ui.jqgrid.css">
</link>
<table width="100%" border="0" align="center">
  <tr>
    <td height="6" colspan="2" ><?php include $backwardseperator.'Header.php'; ?></td>
  </tr>
</table>
<script src="../../../../libraries/jqgrid2/js/jquery.min.js" type="text/javascript"></script>
<script src="../../../../libraries/jqgrid2/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="../../../../libraries/jqgrid2/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script src="../../../../libraries/jqgrid2/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
<script src="<?php echo $backwardseperator?>libraries/javascript/script.js" type="text/javascript"></script>
<script src="cheque_print.js" type="text/javascript"></script>
</head>
<body>
<table width="100%" border="0" align="center" >
  <tr>
    <td align="center"><?php echo $out?></td>
  </tr>
  <tr>
    <td align="center"><a class="button green" id="butPrint">Print Selected Voucher/s</a></td>
  </tr>
</table>
</body>
</html>
<?php
session_start();
$userId		= $_SESSION["userId"];
$backwardseperator 	= "../../../../";
include_once ("{$backwardseperator}dataAccess/Connector.php");
require_once ("../../../../libraries/fpdf/fpdf.php");
include_once ("../../../../class/finance/cls_convert_amount_to_word.php");
$obj_convert_amount_to_word	= new Cls_Convert_Amount_To_Word($db);

$data	= json_decode($_REQUEST["Data"],true);

class PDF extends FPDF
{
}

$pdf = new FPDF('L','cm',array(8.9,26.7));
//$pdf = new FPDF('L','cm',array(24.3,8.9));
foreach($data as $data)
{
	$payment_array	= explode('/',$data["PNo"]);
	$date_array		= str_split($data["PDate"]);
	$header_array	= GetDetails($payment_array[1],$payment_array[0]);
	
	if($header_array["CHEQUE_PRINT_STATUS"]==1)
		continue;
		
	UpdateDetails($payment_array[1],$payment_array[0],$data["PDate"]);	
	$pdf->AddPage();
	$pdf->SetFont('courier','',12);
	$pdf->SetXY(0,0.6);$pdf->Cell(0.5,1,'HI',0,0,'L');
	$pdf->SetXY(1,0.6);$pdf->Cell(0.5,1,'IH',0,0,'L');
	$pdf->SetXY(21,0.6);$pdf->Cell(0.5,1,$date_array[8],0,0,'L');
	$pdf->SetXY(21.6,0.6);$pdf->Cell(0.5,1,$date_array[9],0,0,'L');
	$pdf->SetXY(22.3,0.6);$pdf->Cell(0.5,1,$date_array[5],0,0,'L');
	$pdf->SetXY(22.9,0.6);$pdf->Cell(0.5,1,$date_array[6],0,0,'L');
	$pdf->SetXY(25.5,0.6);$pdf->Cell(0.5,1,$date_array[2],0,0,'L');
	$pdf->SetXY(26,0.6);$pdf->Cell(0.5,1,$date_array[3],0,0,'L');
	$pdf->SetFont('courier','',12);
	$pdf->SetXY(8,2.2);$pdf->Cell(10,1,$header_array["SUPPLIER_NAME"],0,0,'L');
	$pdf->SetXY(8.5,3.7);$pdf->MultiCell(10,1,$obj_convert_amount_to_word->Convert_Amount($header_array["AMOUNT"],$header_array["CURRENCY_CODE"]),0,'L');
	$pdf->SetXY(22,4.5);$pdf->Cell(10,1,$header_array["AMOUNT"].'/=',0,0,'L');
}

$pdf->Output();

function GetDetails($paymentYear,$paymentNo)
{
	global $db;
	$sql = "SELECT
			  S.strName  AS SUPPLIER_NAME,
			  CU.strCode AS CURRENCY_CODE,
			  FSPH.CHEQUE_PRINT_STATUS	AS CHEQUE_PRINT_STATUS,
			  SUM(FSPD.AMOUNT) AS AMOUNT
			FROM finance_supplier_payment_header FSPH
			  INNER JOIN finance_supplier_payment_details FSPD
				ON FSPD.PAYMENT_NO = FSPH.PAYMENT_NO
				  AND FSPD.PAYMENT_YEAR = FSPH.PAYMENT_YEAR
			  INNER JOIN mst_supplier S
				ON S.intId = FSPH.SUPPLIER_ID
			  INNER JOIN mst_financecurrency CU
				ON CU.intId = FSPH.CURRENCY_ID
			WHERE FSPH.PAYMENT_NO = '$paymentNo' 
				AND FSPH.PAYMENT_YEAR = '$paymentYear'";
	$result = $db->RunQuery($sql);
	return mysqli_fetch_array($result);
	
}

function UpdateDetails($paymentYear,$paymentNo,$issue_date)
{
	global $db;
	global $userId;
	
	$sql = "UPDATE finance_supplier_payment_header
			SET CHEQUE_PRINT_STATUS = 0,
			  CHEQUE_ISSUE_DATE = '$issue_date',
			  CHEQUE_PRINT_DATE = NOW(),
			  CHEQUE_PRINT_BY = $userId
			WHERE PAYMENT_NO = '$paymentNo'
				AND PAYMENT_YEAR = '$paymentYear'";
	$db->RunQuery($sql);
}
?>
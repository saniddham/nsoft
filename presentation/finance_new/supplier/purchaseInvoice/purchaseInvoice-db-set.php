<?php 
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$requestType 		= $_REQUEST['requestType'];
$programCode		= 'P0676';

include "{$backwardseperator}dataAccess/Connector.php";
require_once "../../../../class/cls_commonFunctions_get.php";
require_once "../../../../class/masterData/exchange_rate/cls_exchange_rate_get.php";
require_once  "../../../../class/finance/cash_flow/cls_cf_common.php";
require_once  "../../../../class/finance/cash_flow/cls_cf_common_set.php";
	
$obj_maxNo	   		= new cls_commonFunctions_get($db);
$obj_exchngRate_get = new cls_exchange_rate_get($db);
$obj_cf_common		= new cls_cf_common($db);
$obj_cf_common_set	= new cls_cf_common_set($db);

$arr 				= json_decode($_REQUEST['itemDetails'], true);
$arrLedger 			= json_decode($_REQUEST['ledgerDetails'], true);
$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);

if($requestType=='saveData')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
		
	$PurInvoiceNo  	 = $arrHeader['PurInvoiceNo'];
	$PurInvoiceYear  = $arrHeader['PurInvoiceYear'];
	$invoiceNo  	 = $obj_maxNo->replace($arrHeader["invoiceNo"]);
	$supplierId  	 = $arrHeader['supplierId'];
	//$ledgerAccountId = ($_REQUEST['ledgerAccountId']==''?'null':$_REQUEST['ledgerAccountId']);
	$invoiceType  	 = ($arrHeader['invoiceType']==''?'null':$arrHeader['invoiceType']);
	$currency  		 = ($arrHeader['currency']==''?'null':$arrHeader['currency']);
	$PurDate  		 = $arrHeader['PurDate'];
	$Remarks  		 = $obj_maxNo->replace($arrHeader["Remarks"]);
	$GRNArr			 = explode('/',$arrHeader['GRNArr']);
	$POArr			 = explode('/',$arrHeader['POArr']);
	$GRNNo			 = $GRNArr[0];
	$GRNYear		 = $GRNArr[1];
	$PONo			 = $POArr[0];
	$POYear		 	 = $POArr[1];
	$grandTotal		 = round($arrHeader['grandTotal'],2);
	$rollBackFlag 	 = 0;
	$svatAmount  	 = $arrHeader['svatAmount'];
	$saveMode		 = loadSaveMode($programCode,$userId);
	$svatValue		 = getSvatValue($svatAmount,$PONo,$POYear);
	
	$exchngRateArr	 = $obj_exchngRate_get->GetAllValues($currency,2,$PurDate,$companyId,'RunQuery2');
	if($exchngRateArr['AVERAGE_RATE']=='' && $rollBackFlag==0)
	{
		$rollBackFlag	= 1;
		$rollBackMsg	= 'Please enter exchange rates for '.$PurDate.' before save.';
	}
	$chkAmountArr		 = $obj_maxNo->checkAmountWithRS($grandTotal,$PONo,$POYear,$supplierId,$invoiceNo,'RunQuery2');
	if($chkAmountArr['chkStatus']=='fail' && $rollBackFlag==0)
	{
		$rollBackFlag	= 1;
		$rollBackMsg	= "Can not raise Invoice due to Return to Supplier.<br>PO Amount = ".$chkAmountArr['POValue']."<br>Return Amount = ".$chkAmountArr['RSValue']."<br>Balance to Invoice = ".$chkAmountArr['balToInvoice']." ";
	}
	if(!ValidateWithSessionCompany($PONo,$POYear) && $rollBackFlag==0)
	{
		$rollBackFlag	= 1;
		$rollBackMsg	= 'Please logging with correct company location.';
	}
	
	if($saveMode==1)
	{
		if($PurInvoiceNo=='' && $PurInvoiceYear=='')
		{
			$data['arrData'] = $obj_maxNo->GetSystemMaxNo('intPurchaseInvoiceNo',$locationId);
			if($data['arrData']['rollBackFlag']==1 && $rollBackFlag==0)
			{
				$rollBackFlag		= 1;
				$rollBackMsg		= $data['arrData']['msg'];
				$q					= $data['arrData']['q'];
			}
			else
			{
				$PurInvoiceNo		= $data['arrData']['max_no'];
				$PurInvoiceYear		= date('Y');
				
				$sql = "INSERT INTO finance_supplier_purchaseinvoice_header 
						(
						PURCHASE_INVOICE_NO, PURCHASE_INVOICE_YEAR, INVOICE_NO, 
						PURCHASE_DATE, SUPPLIER_ID, COMPANY_ID, 
						INVOICE_TYPE, REMARKS, 
						CURRENCY_ID, STATUS, CREATED_BY,
						CREATED_DATE
						)
						VALUES
						(
						'$PurInvoiceNo', '$PurInvoiceYear', '$invoiceNo', 
						'$PurDate', $supplierId, '$companyId', 
						$invoiceType, '$Remarks', 
						$currency, '1', '$userId', 
						NOW() 
						);";
				$result = $db->RunQuery2($sql);
				if(!$result)
				{
					$rollBackFlag	= 1 ;
					$rollBackMsg 	= $db->errormsg;
					$q 				= $sql;
				}
				else
				{
					$headerSave			= true;
					$saved				= 0;
					$toSave				= 0;
					$status     		= 'saved';
					
					foreach($arr as $arrVal)
					{
						$itemId 	 	= $arrVal['itemId'];
						$qty 		 	= $arrVal['qty'];
						$unitPrice 		= $arrVal['unitPrice'];
						$discount 		= $arrVal['discount'];
						$taxAmount 		= round($arrVal['taxAmount'],2);
						$taxCode 		= ($arrVal['taxCode']==''?'null':$arrVal['taxCode']);
						$costCenter 	= $arrVal['costCenter'];
						$validateQty	= getQtyValidation($invoiceNo,$itemId,$supplierId);
						$validateQty	= round($validateQty,4);
						$qty			= round($qty,4);
						
						if($qty>$validateQty)
						{
							$db->RunQuery2('Rollback');
							$response['type'] 		= 'fail';
							$response['msg'] 		= 'Some Qty exceed GRN Qty.'/*."/inv:".$invoiceNo."/item:".$itemId."/sup:".$supplierId."/vali:".$validateQty."/qty:".$qty*/;
							$q 						= $sql;
							
							$db->CloseConnection();
							echo json_encode($response);
							return;
						}
						
						$sql = "INSERT INTO finance_supplier_purchaseinvoice_details 
								(
								PURCHASE_INVOICE_NO, PURCHASE_INVOICE_YEAR, ITEM_ID, 
								QTY, UNIT_PRICE, DISCOUNT, 
								TAX_AMOUNT, TAX_CODE, COST_CENTER
								)
								VALUES
								(
								'$PurInvoiceNo', '$PurInvoiceYear', '$itemId', 
								'$qty', '$unitPrice', '$discount', 
								'$taxAmount', $taxCode, '$costCenter'
								);";
						$result = $db->RunQuery2($sql);
						if($result)
						{
							$sqlGRN = "UPDATE ware_grndetails 
										SET
										dblInvoiceBalanceQty = dblInvoiceBalanceQty-$qty
										WHERE
										intGrnNo = '$GRNNo' AND 
										intGrnYear = '$GRNYear' AND 
										intItemId = '$itemId' ";
							$resultGRN = $db->RunQuery2($sqlGRN);
							if($resultGRN)
							{
								$saved++;
								$status = 'saved';
							}
							else
							{
								$rollBackFlag	= 1;
								$rollBackMsg	= "GRN Qty update error.";
								$q 				= $sqlGRN;
							}	
						}
						else
						{
							$rollBackFlag	= 1;
							$rollBackMsg	= "detail saving error.";
							$q 				= $sql;
						}
						$toSave++;
					}
					foreach($arrLedger as $arrLVal)
					{
						$ledgerAccId 	= $arrLVal['ledgerAccount'];
						$amount 		= $arrLVal['amount'];
						
						$sqlGL = " INSERT INTO finance_supplier_purchaseinvoice_gl 
									(
									PURCHASE_INVOICE_NO, 
									PURCHASE_INVOICE_YEAR, 
									GLACCOUNT_ID, 
									AMOUNT
									)
									VALUES
									('$PurInvoiceNo', 
									'$PurInvoiceYear', 
									'$ledgerAccId', 
									'$amount'
									);";
						$resultGL = $db->RunQuery2($sqlGL);
						if($resultGL)
						{
							$status = 'saved';
							
							$sql = "INSERT INTO finance_supplier_transaction 
									(
									PO_YEAR, PO_NO, SUPPLIER_ID, 
									CURRENCY_ID, DOCUMENT_YEAR, DOCUMENT_NO, 
									DOCUMENT_TYPE, PURCHASE_INVOICE_YEAR, PURCHASE_INVOICE_NO, 
									LEDGER_ID, VALUE, COMPANY_ID, 
									LOCATION_ID, USER_ID, TRANSACTION_DATE_TIME
									)
									VALUES
									( 
									'$POYear', '$PONo', '$supplierId', 
									'$currency', '$PurInvoiceYear', '$PurInvoiceNo', 
									'INVOICE', '$PurInvoiceYear', '$PurInvoiceNo', 
									'$ledgerAccId', '$amount', '$companyId', 
									'$locationId', '$userId', '$PurDate'
									);";
							$result = $db->RunQuery2($sql);
							if(!$result)
							{
								$rollBackFlag	= 1;
								$rollBackMsg	= $db->errormsg;
								$q 				= $sql;
							}
							else
							{
								$sql = "INSERT INTO finance_transaction 
										(
										CHART_OF_ACCOUNT_ID, AMOUNT, DOCUMENT_NO, 
										DOCUMENT_YEAR, DOCUMENT_TYPE, INVOICE_NO, 
										INVOICE_YEAR, TRANSACTION_TYPE, TRANSACTION_CATEGORY, TRANSACTION_CATEGORY_ID,
										CURRENCY_ID, LOCATION_ID, COMPANY_ID, LAST_MODIFIED_BY ,LAST_MODIFIED_DATE
										)
										VALUES
										(
										'$ledgerAccId', '$amount', '$PurInvoiceNo', 
										'$PurInvoiceYear', 'INVOICE', '$PurInvoiceNo', 
										'$PurInvoiceYear', 'D', 'SU', '$supplierId',
										'$currency', '$locationId', '$companyId', '$userId', '$PurDate'
										);";
								$result = $db->RunQuery2($sql);
								if(!$result)
								{
									$rollBackFlag	= 1;
									$rollBackMsg	= $db->errormsg;
									$q 				= $sql;
								}
								else
								{
									$saved++;
								}
								
							}
						}
						else
						{
							$rollBackFlag	= 1;
							$rollBackMsg	= $db->errormsg;
							$q 				= $sqlGL;
						}
						$toSave++;
					}
					// ----- save SVAT && Supplier Ledger transaction -----------
					
					$sqlChk = "SELECT IFNULL(intInvoiceType,0) AS invoiceType ,
								(SELECT CHART_OF_ACCOUNT_ID FROM finance_mst_chartofaccount
								WHERE CATEGORY_TYPE='S' AND
								CATEGORY_ID='$supplierId' AND
								STATUS=1) chartOfAccId
								FROM mst_supplier 
								WHERE intId='$supplierId' ";
					
					$resultChk = $db->RunQuery2($sqlChk);
					$rowChk = mysqli_fetch_array($resultChk);
					if($rowChk['invoiceType']==3 && $svatValue>0)
					{
						$data['finalResult'] = insertSVAT('D',$svatValue,$PurInvoiceNo,$PurInvoiceYear,$currency,$supplierId,$PurDate);
						if($data['finalResult']['rollBackFlag']==0)
						{
							$data['finalResult'] = insertSVAT('C',$svatValue,$PurInvoiceNo,$PurInvoiceYear,$currency,$supplierId,$PurDate);
							if($data['finalResult']['rollBackFlag']==1)
							{
								$rollBackFlag	= 1;
								$rollBackMsg	= $data['finalResult']['msg'];
								$q				= $data['finalResult']['q'];
							}		
						}
						else
						{
							$rollBackFlag	= 1;
							$rollBackMsg	= $data['finalResult']['msg'];
							$q				= $data['finalResult']['q'];
						}
					}
					if($rowChk['chartOfAccId']=='')
					{
						$rollBackFlag	= 1;
						$rollBackMsg	= "Not assign GL Account for the selected Supplier.";
						$q				= $sqlChk;
					}
					else
					{
						$data['finalResult'] = insertSupplierLedger('C',$rowChk['chartOfAccId'],$grandTotal,$PurInvoiceNo,$PurInvoiceYear,$currency,$supplierId,$PurDate);
						if($data['finalResult']['rollBackFlag']==1)
						{
							$rollBackFlag	= 1;
							$rollBackMsg	= $data['finalResult']['msg'];
							$q				= $data['finalResult']['q'];
						}	
					}							
					
					//----------------------------------------
				}
			}
		}
		else
		{
			$sql = "UPDATE finance_supplier_purchaseinvoice_header 
					SET
					INVOICE_NO = '$invoiceNo' , 
					PURCHASE_DATE = '$PurDate' , 
					SUPPLIER_ID = '$supplierId' , 
					COMPANY_ID = '$companyId' , 
					INVOICE_TYPE = $invoiceType , 
					REMARKS = '$Remarks' , 
					CURRENCY_ID = $currency , 
					LAST_MODIFY_BY = '$userId' , 
					LAST_MODIFY_DATE = NOW()
					WHERE
					PURCHASE_INVOICE_NO = '$PurInvoiceNo' AND 
					PURCHASE_INVOICE_YEAR = '$PurInvoiceYear' ";
			$result = $db->RunQuery2($sql);
			if(!$result)
			{
				$rollBackFlag	= 1 ;
				$rollBackMsg 	= $db->errormsg;
			}
			else
			{
				$headerSave			= true;
				$saved				= 0;
				$toSave				= 0;
				$status				= 'updated';
				
				$sqlDel = " DELETE FROM finance_supplier_purchaseinvoice_details 
							WHERE
							PURCHASE_INVOICE_NO = '$PurInvoiceNo' AND 
							PURCHASE_INVOICE_YEAR = '$PurInvoiceYear' ;";
				$resultDel = $db->RunQuery2($sqlDel);
				
				$sqlDel = " DELETE FROM finance_supplier_purchaseinvoice_gl 
							WHERE
							PURCHASE_INVOICE_NO = '$PurInvoiceNo' AND 
							PURCHASE_INVOICE_YEAR = '$PurInvoiceYear' ;";
				$resultDel = $db->RunQuery2($sqlDel);
				
				if(!$resultDel)
				{
					$rollBackFlag	= 1 ;
					$rollBackMsg 	= $db->errormsg;
				}
				else
				{
					foreach($arr as $arrVal)
					{
						$itemId 	 	= $arrVal['itemId'];
						$qty 		 	= $arrVal['qty'];
						$unitPrice 		= $arrVal['unitPrice'];
						$discount 		= $arrVal['discount'];
						$taxAmount 		= $arrVal['taxAmount'];
						$taxCode 		= $arrVal['taxCode'];
						$costCenter 	= $arrVal['costCenter'];
						
						$sql = "INSERT INTO finance_supplier_purchaseinvoice_details 
								(
								PURCHASE_INVOICE_NO, PURCHASE_INVOICE_YEAR, ITEM_ID, 
								QTY, UNIT_PRICE, DISCOUNT, 
								TAX_AMOUNT, TAX_CODE, COST_CENTER
								)
								VALUES
								(
								'$PurInvoiceNo', '$PurInvoiceYear', '$itemId', 
								'$qty', '$unitPrice', '$discount', 
								'$taxAmount', '$taxCode', '$costCenter'
								);";
						$result = $db->RunQuery2($sql);
						if($result)
						{
							$sqlGRN = "UPDATE ware_grndetails 
										SET
										dblInvoiceBalanceQty = dblInvoiceBalanceQty-$qty
										WHERE
										intGrnNo = '$GRNNo' AND 
										intGrnYear = '$GRNYear' AND 
										intItemId = '$itemId' ";
							$resultGRN = $db->RunQuery2($sqlGRN);
							if($resultGRN)
							{
								$saved++;
								$status = 'updated';
							}
							else
							{
								$rollBackFlag	= 1;
								$rollBackMsg	= "GRN Qty update error.";
							}
						}
						else
						{
							$rollBackFlag	= 1;
							$rollBackMsg	= "detail saving error.";
						}
						$toSave++;
					}
					foreach($arrLedger as $arrLVal)
					{
						$ledgerAccId 	= $arrLVal['ledgerAccount'];
						$amount 		= $arrLVal['amount'];
						
						$sqlGL = " INSERT INTO finance_supplier_purchaseinvoice_gl 
									(
									PURCHASE_INVOICE_NO, 
									PURCHASE_INVOICE_YEAR, 
									GLACCOUNT_ID, 
									AMOUNT
									)
									VALUES
									('$PurInvoiceNo', 
									'$PurInvoiceYear', 
									'$ledgerAccId', 
									'$amount'
									);";
						$resultGL = $db->RunQuery2($sqlGL);
						if($resultGL)
						{
							$saved++;
							$status = 'saved';
						}
						else
						{
							$rollBackFlag	= 1;
							$rollBackMsg	= $db->errormsg;
						}
						$toSave++;
					}
				}
			}
		}
	}
	else
	{
		$rollBackFlag	= 1;
		$rollBackMsg	= 'You do not have permission to Save Purchase Invoice.';
	}
	//-------------------------- cash flow update 27/05/2014 by lahiru --------------------------------
	
	$CFUpdResult	= getDetailData($PurInvoiceNo,$PurInvoiceYear);
	while($row = mysqli_fetch_array($CFUpdResult))
	{
		$amount	   		= $row['QTY']*$row['UNIT_PRICE'];	
		$taxAmount		= round($row['TAX_AMOUNT'],2);
		$disamonut 		= round((($amount*(100-$row['DISCOUNT']))/100),2)+$taxAmount;
		$currencyId		= $row['CURRENCY_ID'];
		$entryDate		= $row['PURCHASE_DATE'];
		$company		= $row['COMPANY_ID'];
		$rpt_field		= $row['REPORT_FIELD_ID'];
		$creditTerm		= $row['CREDIT_TERM'];
		$creditDate		= date('Y-m-d', strtotime($entryDate. ' + '.$creditTerm.' days'));
		
		$tot_to_save	= $disamonut;	
		
		$cfAmountResult	= $obj_cf_common->get_cf_log_bal_to_invoice_result($locationId,$rpt_field,$currencyId,$entryDate,$company,'RunQuery2');
		while($rowAmt = mysqli_fetch_array($cfAmountResult))
		{
			$balAmount	= round($rowAmt['BAL'],2);
			if(($tot_to_save>0) && ($balAmount>0))
			{
				if($tot_to_save<=$balAmount)
				{
					$saveAmnt 	 	= $tot_to_save;
					$tot_to_save 	= 0;
				}
				else if($tot_to_save>$balAmount)
				{
					$saveAmnt 		= $balAmount;
					$tot_to_save	= $tot_to_save-$saveAmnt;
				}
				$rcv_date	= $rowAmt['RECEIVE_DATE'];
				$currency	= $rowAmt['CURRENCY_ID'];
				
				$resultArr	= $obj_cf_common_set->insert_to_log($locationId,$rpt_field,$rcv_date,$entryDate,$creditDate,$PurInvoiceNo,$PurInvoiceYear,'INVOICE',$saveAmnt,$currency,'RunQuery2');
				if($resultArr['type']=='fail' && $rollBackFlag==0)
				{
					$rollBackFlag	= 1;
					$rollBackMsg	= $resultArr['msg'];
					$q				= $resultArr['sql'];
				}
			}
		}
		if($tot_to_save>0)
		{
			if($rcv_date == '')
					$rcv_date = $entryDate;	
			
			$resultArr	= $obj_cf_common_set->insert_to_log($locationId,$rpt_field,$rcv_date,$entryDate,$creditDate,$PurInvoiceNo,$PurInvoiceYear,'INVOICE',$tot_to_save,$currencyId,'RunQuery2');
			if($resultArr['type']=='fail' && $rollBackFlag==0)
			{
				$rollBackFlag	= 1;
				$rollBackMsg	= $resultArr['msg'];
				$q				= $resultArr['sql'];
			}
		}
	}
	//-------------------------------------------------------------------------------------------------
	$resultArr	= $obj_maxNo->validateDuplicateSerialNoWithSysNo($PurInvoiceNo,'intPurchaseInvoiceNo',$locationId);
	if($resultArr['type']=='fail' && $rollBackFlag==0)
	{
		$rollBackFlag	= 1;
		$rollBackMsg	= $resultArr['msg'];
	}
	
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
		$response['q'] 			= $q;
	}
	else if(($headerSave) && ($saved==$toSave))
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		
		if($status=='saved')
			$response['msg'] 	= 'Saved successfully.';
		else
			$response['msg'] 	= 'Updated successfully.';
		
		$response['purInvoiceNo']	= $PurInvoiceNo;
		$response['purInvoiceYear']	= $PurInvoiceYear;
	}
	else
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
		$response['q'] 			= $q;
	}
	$db->CloseConnection();
	echo json_encode($response);
}
else if($requestType=='cancleInvoice')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
		
	$PurInvoiceNo  	 = $_REQUEST['purchaseInvNo'];
	$PurInvoiceYear  = $_REQUEST['purchaseInvYear'];
    $supplier  = $_REQUEST['supplier'];
	$rollBackFlag	 = 0;
	
	$cancleMode 	 = loadCancleMode($programCode,$userId);
	if($cancleMode==1)
	{
		$data['arrData'] = CancleValidation($PurInvoiceNo,$PurInvoiceYear);
		if($data['arrData']['rollBackFlag']==1)
		{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $data['arrData']['rollBackMsg'];
			
			$db->CloseConnection();
			echo json_encode($response);
			return;
		}
		
				
		$sql = "SELECT intGrnNo,intGrnYear
				FROM finance_supplier_purchaseinvoice_header
				INNER JOIN ware_grnheader ON finance_supplier_purchaseinvoice_header.INVOICE_NO=ware_grnheader.strInvoiceNo
				INNER JOIN trn_poheader ON trn_poheader.intPONo = ware_grnheader.intPoNo AND trn_poheader.intPOYear = ware_grnheader.intPoYear AND trn_poheader.intSupplier = '$supplier'
				WHERE PURCHASE_INVOICE_NO='$PurInvoiceNo' AND
				PURCHASE_INVOICE_YEAR='$PurInvoiceYear' ";
		
		$result = $db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		$GRNNo 		= $row['intGrnNo'];
		$GRNYear 	= $row['intGrnYear'];
		
		$sqlGet = " SELECT ITEM_ID,QTY
					FROM finance_supplier_purchaseinvoice_details
					WHERE PURCHASE_INVOICE_NO='$PurInvoiceNo' AND
					PURCHASE_INVOICE_YEAR='$PurInvoiceYear' ";
		$resultGet = $db->RunQuery2($sqlGet);
	
		while($rowGet = mysqli_fetch_array($resultGet))
		{
			$qty  = $rowGet['QTY'];
			$item = $rowGet['ITEM_ID'];
			
			$sqlUpdGrn = "UPDATE ware_grndetails 
							SET
							dblInvoiceBalanceQty = dblInvoiceBalanceQty+$qty
							WHERE
							intGrnNo = '$GRNNo' AND 
							intGrnYear = '$GRNYear' AND 
							intItemId = '$item' ";
			$finalResult = $db->RunQuery2($sqlUpdGrn);
			if(!$finalResult)
			{
				$rollBackFlag	= 1;
				$rollBackMsg	= "GRN Qty update error.";
			}
		}
		$sqlUpdH = "UPDATE finance_supplier_purchaseinvoice_header 
					SET
					STATUS = '-2'	
					WHERE
					PURCHASE_INVOICE_NO = '$PurInvoiceNo' AND 
					PURCHASE_INVOICE_YEAR = '$PurInvoiceYear' ";
		$resultUpdH = $db->RunQuery2($sqlUpdH);
		if(!$resultUpdH)
		{
			$rollBackFlag	= 1;
			$rollBackMsg	= $db->errormsg;
		}
		else
		{
			$sql = "DELETE FROM finance_supplier_transaction 
					WHERE DOCUMENT_NO='$PurInvoiceNo' AND 
					DOCUMENT_YEAR='$PurInvoiceYear' AND 
					DOCUMENT_TYPE='INVOICE' AND
					COMPANY_ID='$companyId' ";
			$result = $db->RunQuery2($sql);
			
			if(!$result)
			{
				$rollBackFlag	= 1;
				$rollBackMsg	= $db->errormsg;
			}
			
			$sql = "DELETE FROM finance_transaction 
					WHERE DOCUMENT_NO ='$PurInvoiceNo' AND
					DOCUMENT_YEAR='$PurInvoiceYear' AND
					DOCUMENT_TYPE='INVOICE' AND
					TRANSACTION_CATEGORY = 'SU' AND 
					COMPANY_ID='$companyId' ";
			$result = $db->RunQuery2($sql);
			
			if(!$result)
			{
				$rollBackFlag	= 1;
				$rollBackMsg	= $db->errormsg;
			}
			
		}
		$resultDCFArr	= $obj_cf_common_set->delete_from_log($PurInvoiceNo,$PurInvoiceYear,'INVOICE','RunQuery2');
		if($resultDCFArr['type'] == 'fail' && $rollBackFlag==0)
		{
			$rollBackFlag	= 1;
			$rollBackMsg	= $resultDCFArr['msg']; 
			$error_sql		= $q; 
		}
	}
	else
	{
		$rollBackFlag	 = 1;
		$rollBackMsg	 = "You do not have permission to Cancle Purchase Invoice.";
	}	
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	else if($rollBackFlag==0)
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Cancelled successfully.';
	}
	else
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	$db->CloseConnection();
	echo json_encode($response);
}
function loadCancleMode($programCode,$userId)
{
	global $db;
	$Mode	= 0;
	$sql = "SELECT
			menupermision.intCancel 
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId = '$userId' ";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	if($row['intCancel']==1)
	{
		$Mode = 1;
	}
	return $Mode;
}
function loadSaveMode($programCode,$userId)
{
	global $db;
	$Mode	= 0;
	$sql = "SELECT
			menupermision.intAdd 
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId = '$userId' ";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	if($row['intAdd']==1)
	{
		$Mode = 1;
	}
	return $Mode;
}
function getQtyValidation($invoiceNo,$itemId,$supplierId)
{
	global $db;
	
	$sql = "SELECT ROUND(GD.dblInvoiceBalanceQty,2) AS InvoiceBalanceQty
			FROM ware_grndetails GD 
			INNER JOIN ware_grnheader GH ON GH.intGrnNo=GD.intGrnNo AND GH.intGrnYear=GD.intGrnYear
			INNER JOIN trn_poheader POH ON POH.intPONo=GH.intPoNo AND POH.intPOYear=GH.intPoYear
			WHERE GH.strInvoiceNo='$invoiceNo' AND
			GD.intItemId='$itemId' AND
			POH.intSupplier='$supplierId' AND 
			GH.intStatus = 1 ";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['InvoiceBalanceQty'];
}
function CancleValidation($PurInvoiceNo,$PurInvoiceYear)
{
	global $db;
	
	$data['rollBackFlag'] 	= 0 ;
	
	$sql = "SELECT STATUS
			FROM finance_supplier_purchaseinvoice_header
			WHERE PURCHASE_INVOICE_NO='$PurInvoiceNo' AND
			PURCHASE_INVOICE_YEAR='$PurInvoiceYear' ";
	
	$result = $db->RunQuery2($sql);
	$row 	= mysqli_fetch_array($result);
	
	if($row['STATUS']=='-2')
	{
		$data['rollBackFlag'] 	= 1 ;
		$data['rollBackMsg'] 	= 'Already Cancelled.';
	}
	
	$sql1 = "SELECT IFNULL((SUM(ST.VALUE)*-1),0) totQty,ST.DOCUMENT_TYPE
			FROM finance_supplier_transaction ST
			WHERE ST.PURCHASE_INVOICE_NO='$PurInvoiceNo' AND
			ST.PURCHASE_INVOICE_YEAR='$PurInvoiceYear' AND
			ST.DOCUMENT_TYPE<>'INVOICE' ";
			
	$result1 = $db->RunQuery2($sql1);
	$row1 	 = mysqli_fetch_array($result1);
	
	if($row1['totQty']>0)
	{	
		$data['rollBackFlag'] 	= 1 ;
		
		if($row1['DOCUMENT_TYPE']=='ADVANCE')
			$data['rollBackMsg'] 	= 'Cannot cancel this Invoice.Advance settlement raised.';	
		else
			$data['rollBackMsg'] 	= 'Cannot cancel this Invoice.Supplier payment raised.';
	}
	
	return $data;
}
function getSvatValue($grandTotal,$PONo,$POYear)
{
	global $db;
	
	//$sql = "SELECT dblRate FROM mst_financetaxisolated WHERE intId='3' ";
	$sql 	= "SELECT IFNULL(SVAT_RATE,0) AS svat_rate FROM trn_poheader WHERE intPONo = '$PONo' AND intPOYear = '$POYear' ";
	$result = $db->RunQuery2($sql);
	$row 	= mysqli_fetch_array($result);
	
	$svatValue = $grandTotal*($row['svat_rate']/100);
	return round($svatValue,2);
}
function insertSVAT($transactionType,$svatValue,$PurInvoiceNo,$PurInvoiceYear,$currency,$supplierId,$PurDate)
{
	global $db;
	global $locationId;
	global $companyId;
	global $userId;

	$sql = "INSERT INTO finance_transaction 
			(
			CHART_OF_ACCOUNT_ID, 
			AMOUNT, DOCUMENT_NO, DOCUMENT_YEAR, 
			DOCUMENT_TYPE, INVOICE_NO, INVOICE_YEAR, 
			TRANSACTION_TYPE, TRANSACTION_CATEGORY, TRANSACTION_CATEGORY_ID, 
			CURRENCY_ID, LOCATION_ID, COMPANY_ID, LAST_MODIFIED_BY, LAST_MODIFIED_DATE
			)
			VALUES
			(
			(SELECT CHART_OF_ACCOUNT_ID 
			FROM finance_mst_chartofaccount_invoice_type 
			WHERE INVOICE_TYPE_ID='3' AND 
			TRANSACTION_CATEGORY='SU' AND 
			TRANSACTION_TYPE='$transactionType'), 
			'$svatValue', '$PurInvoiceNo', '$PurInvoiceYear', 
			'INVOICE', '$PurInvoiceNo', '$PurInvoiceYear', 
			'$transactionType', 'SU', '$supplierId', 
			'$currency', '$locationId', '$companyId', '$userId', '$PurDate'
			);";
	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$data['rollBackFlag'] 	= 1 ;
		$data['msg'] 			= $db->errormsg;
		$data['q'] 				= $sql ;
	}
	else
	{
		$data['rollBackFlag'] 	= 0 ;
	}
	return $data;
}
function insertSupplierLedger($transactionType,$chartOfAccId,$grandTotal,$PurInvoiceNo,$PurInvoiceYear,$currency,$supplierId,$PurDate)
{
	global $db;
	global $locationId;
	global $companyId;
	global $userId;

	$sql = "INSERT INTO finance_transaction 
			(
			CHART_OF_ACCOUNT_ID, 
			AMOUNT, DOCUMENT_NO, DOCUMENT_YEAR, 
			DOCUMENT_TYPE, INVOICE_NO, INVOICE_YEAR, 
			TRANSACTION_TYPE, TRANSACTION_CATEGORY, TRANSACTION_CATEGORY_ID, 
			CURRENCY_ID, LOCATION_ID, COMPANY_ID, LAST_MODIFIED_BY, LAST_MODIFIED_DATE
			)
			VALUES
			(
			'$chartOfAccId', 
			'$grandTotal', '$PurInvoiceNo', '$PurInvoiceYear', 
			'INVOICE', '$PurInvoiceNo', '$PurInvoiceYear', 
			'$transactionType', 'SU', '$supplierId', 
			'$currency', '$locationId', '$companyId', '$userId' ,'$PurDate'
			);";
	
	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$data['rollBackFlag'] 	= 1 ;
		$data['msg'] 			= $db->errormsg;
		$data['q'] 				= $sql ;
	}
	else
	{
		$data['rollBackFlag'] 	= 0 ;
	}
	return $data;
}

function ValidateWithSessionCompany($PONo,$POYear)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT 
			  LO.intCompanyId AS COMPANY_ID
			FROM trn_poheader PH
			  INNER JOIN mst_locations LO
				ON LO.intId = PH.intCompany
			WHERE PH.intPONo = '$PONo'
				AND PH.intPOYear = '$POYear'";
	$result = $db->RunQuery2($sql);
	$row 	= mysqli_fetch_array($result);
	if($companyId != $row["COMPANY_ID"])
		return false;
	else
		return true;
}
function getDetailData($PurInvoiceNo,$PurInvoiceYear)
{
	global $db;
	
	$sql = "SELECT
			SPIH.COMPANY_ID,
			CFCP.REPORT_FIELD_ID, 
			SPIH.CURRENCY_ID, 
			SPID.ITEM_ID ,  
			SPID.QTY, 
			SPID.UNIT_PRICE,
			SPID.TAX_AMOUNT, 
			IFNULL(SPID.DISCOUNT,0) AS DISCOUNT, 
			SPIH.PURCHASE_DATE,
			FPT.strName AS CREDIT_TERM
			
			FROM
			finance_supplier_purchaseinvoice_header SPIH
			INNER JOIN finance_supplier_purchaseinvoice_details SPID ON SPIH.PURCHASE_INVOICE_NO = SPID.PURCHASE_INVOICE_NO AND 
			SPIH.PURCHASE_INVOICE_YEAR = SPID.PURCHASE_INVOICE_YEAR
			INNER JOIN finance_cashflow_cpanal CFCP ON FIND_IN_SET(SPID.ITEM_ID,CFCP.PO_ITEM_ID)
			INNER JOIN ware_grnheader GH ON GH.strInvoiceNo=SPIH.INVOICE_NO
			INNER JOIN trn_poheader PO ON PO.intPONo=GH.intPoNo AND PO.intPOYear=GH.intPoYear
			INNER JOIN mst_financepaymentsterms FPT ON FPT.intId=PO.intPaymentTerm
			WHERE
			CFCP.BOO_PO_RAISED = 1 AND 
			SPID.PURCHASE_INVOICE_NO = '$PurInvoiceNo' AND
			SPID.PURCHASE_INVOICE_YEAR = '$PurInvoiceYear' ";
	
	$result = $db->RunQuery2($sql);
	return $result;
}
?>
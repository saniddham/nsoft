<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/finance/cls_convert_amount_to_word.php";
include_once "class/finance/cls_common_get.php";

$obj_fin_com		= new Cls_Common_Get($db); 
$obj_AmtName	   	= new Cls_Convert_Amount_To_Word($db);

$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];

$purInvoiceNo		= $_REQUEST["purInvoiceNo"];
$purInvoiceYear		= $_REQUEST["purInvoiceYear"];

$header_array 		= GetHeaderDetails($purInvoiceNo,$purInvoiceYear,$companyId);
$detail_result 		= GetGridDetails($purInvoiceNo,$purInvoiceYear);

$company_Id			= $header_array["COMPANY_ID"];
?>
<head>
<title>Supplier Purchase Invoice</title>

<style>
#apDiv1 {
	position: absolute;
	left: -3px;
	top: 120px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
</style>

</head>
<body>
<?php
if($header_array['STATUS']==-2)//pending
{
?>
	<div id="apDiv1"><img src="images/cancelled.png" style="opacity:0.3" /></div>
<?php
}
?>
<form id="frmSalesInvoice" name="frmSalesInvoice" method="post">
    <table width="900" align="center">
      <tr>
        <td colspan="2"><?php include 'reportHeader.php'?></td>
      </tr>
      <tr>
        <td colspan="2" style="text-align:center">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" style="text-align:center"><strong>SUPPLIER PURCHASE INVOICE REPORT</strong></td>
      </tr>
      <tr>
        <td colspan="2"><table width="100%" border="0" class="normalfnt">
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>Invoice Serial No</td>
            <td>:</td>
            <td><?php echo $obj_fin_com->getSerialNo($header_array["SERIAL_NO"],$header_array["SERIAL_DATE"],$company_Id,'RunQuery')//echo $header_array["ADVANCE_NO"]; ?></td>
            <td>Invoice No</td>
            <td>:</td>
            <td><?php echo $header_array["INVOICE_NO"]?></td>
          </tr>
          <tr>
            <td>Supplier</td>
            <td>:</td>
            <td><?php echo $header_array["supplier"]; ?></td>
            <td>Invoiced Date</td>
            <td>:</td>
            <td><?php echo $header_array["PURCHASE_DATE"]; ?></td>
          </tr>
          <tr>
            <td width="14%">PO No</td>
            <td width="1%">:</td>
            <td width="44%"><?php echo $header_array["PONo"]; ?></td>
            <td width="14%">GRN No</td>
            <td width="1%">:</td>
            <td width="26%"><?php echo $header_array["GRNNo"]; ?></td>
          </tr>
          <tr>
            <td>Payment Term</td>
            <td>:</td>
            <td><?php echo $header_array["permantTerm"]; ?></td>
            <td>Currency</td>
            <td>:</td>
            <td><?php echo $header_array["currency"]; ?></td>
          </tr>
          <tr>
            <td>Remarks</td>
            <td>:</td>
            <td rowspan="2" valign="top"><?php echo $header_array["REMARKS"]; ?></td>
            <td>Invoice Type</td>
            <td>:</td>
            <td><?php echo $header_array["strInvoiceType"]; ?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Created By</td>
            <td>:</td>
            <td><?php echo $header_array["strFullName"]; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" ><table width="100%" border="0" class="rptBordered" id="tblMain">
        <thead>
          <tr>
            <th width="4%">&nbsp;</th>
            <th width="12%">Item Code</th>
            <th width="31%">Item</th>
            <th width="10%">UOM</th>
            <th width="11%">Unit Price</th>
            <th width="10%">Qty</th>
            <th width="10%">Discount(%)</th>
            <th width="12%">Value</th>
            </tr>
        </thead>        
        <tbody>
<?php
$totAmount 		= 0;
$totTaxAmount 	= 0;
$i				= 0;
while($row = mysqli_fetch_array($detail_result))
{ 
	$amount 		= $row['QTY']*$row['UNIT_PRICE'];
	$finalAmount 	= $amount;
?>
		<tr>
            <td class="cls_td_check" align="center"><?php echo ++$i;?>.</td>
            <td class="cls_td_salesOrderNo"><?php echo $row["strCode"]?></td>
            <td width="31%"><?php echo $row["itemName"]?></td>
            <td width="10%"><?php echo $row["unit"]?></td>
            <td width="11%" style="text-align:right"><?php echo $row["UNIT_PRICE"]?></td>
            <td class="cls_td_invoice cls_Subtract" style="text-align:right"><?php echo $row["QTY"]?></td>
            <td style="text-align:right"><?php echo $row["DISCOUNT"]?></td>
            <td class="cls_td_value" style="text-align:right"><?php echo number_format($finalAmount,2);?></td>
            </tr>
<?php
	$totAmount 		= $totAmount+$finalAmount;
	$totTaxAmount 	= $totTaxAmount+$row['TAX_AMOUNT'];
}
	$grandTotal = round($totAmount,2) + round($totTaxAmount,2)
?>
        </tbody>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" class="normalfnt">&nbsp;</td>
      </tr>
      <tr>
        <td width="416" valign="top"><table width="300" class="rptBordered" id="tblLedger" >
                  <tr>
                    <th width="69%" >Ledger Account</th>
                    <th width="31%" >Amount</th>
                    </tr>
                    <?php
					$totLedAccAmount = 0;
					
					$sqlM = "SELECT SPGL.GLACCOUNT_ID,
							 (SELECT CONCAT(CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE),' - ',
							 FCOA.CHART_OF_ACCOUNT_NAME)
							 FROM finance_mst_chartofaccount FCOA
							 INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
							 INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
							 INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
							 WHERE FCOA.CHART_OF_ACCOUNT_ID=SPGL.GLACCOUNT_ID) AS ledgerAccount ,
							 ROUND(SPGL.AMOUNT,2) AS AMOUNT
							 FROM  
							 finance_supplier_purchaseinvoice_gl SPGL
							 WHERE PURCHASE_INVOICE_NO='$purInvoiceNo' AND
							 PURCHASE_INVOICE_YEAR='$purInvoiceYear' ";

						$resultM = $db->RunQuery($sqlM);
						while($rowM=mysqli_fetch_array($resultM))
						{
							$totLedAccAmount+=$rowM['AMOUNT'];
						?>
                            <tr>
                            <td width="69%" style="text-align:left" ><?php echo $rowM['ledgerAccount']; ?></td>
                            <td width="31%" style="text-align:right" ><?php echo $rowM['AMOUNT']; ?></td>
                           </tr>
                        <?php	
						}
					?>
                   <tr class="dataRow">
                    <td style="text-align:right"><b>Total</b></td>
                    <td style="text-align:right"><b><?php echo number_format($totLedAccAmount,2); ?></b></td>
                    </tr>
                   </table></td>
        <td width="472" valign="top" ><table width="230" border="0" align="right" class="normalfnt">
          <tr>
            <td width="100">Sub Total</td>
            <td width="10">:</td>
            <td width="106" align="right"><?php echo number_format($totAmount,2)?></td>
          </tr>
          <tr>
            <td>Tax Total</td>
            <td>:</td>
            <td align="right"><?php echo number_format($totTaxAmount,2);?></td>
          </tr>
          <tr>
            <td>Grand Total</td>
            <td>:</td>
            <td align="right"><?php echo number_format($grandTotal,2);?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" class="normalfnt"><b><?php echo $obj_AmtName->Convert_Amount($grandTotal,$header_array["currency"]); ?></b></td>
      </tr>
      <tr>
        <td colspan="2">&nbsp;</td>
      </tr>
      </table>
</form>
</body>
<?php
function GetHeaderDetails($purInvoiceNo,$purInvoiceYear,$companyId)
{
	global $db;
	
	$sql = "SELECT 
			SPIH.PURCHASE_INVOICE_NO				AS SERIAL_NO,
			SPIH.COMPANY_ID							AS COMPANY_ID,
			SPIH.PURCHASE_DATE						AS SERIAL_DATE,
			SPIH.INVOICE_NO,
			MS.strName AS supplier,
			SPIH.PURCHASE_DATE,
			CONCAT(PO.intPONo,' / ',PO.intPOYear) AS PONo,
			CONCAT(GH.intGrnNo,' / ',GH.intGrnYear) AS GRNNo,
			FPT.strDescription AS permantTerm,
			FC.strDescription AS currency,
			IT.strInvoiceType,
			SPIH.REMARKS,
			SPIH.STATUS,
			sys_users.strFullName
			FROM finance_supplier_purchaseinvoice_header SPIH
			INNER JOIN mst_supplier MS ON MS.intId=SPIH.SUPPLIER_ID
			INNER JOIN ware_grnheader GH ON GH.strInvoiceNo=SPIH.INVOICE_NO
			INNER JOIN mst_locations ML ON ML.intId=GH.intCompanyId
			INNER JOIN trn_poheader PO ON GH.intPoNo=PO.intPONo AND GH.intPoYear=PO.intPOYear AND PO.intSupplier=SPIH.SUPPLIER_ID
			INNER JOIN mst_financepaymentsterms FPT ON FPT.intId=PO.intPaymentTerm
			LEFT JOIN mst_financecurrency FC ON FC.intId=SPIH.CURRENCY_ID
			LEFT JOIN mst_invoicetype IT ON IT.intId=SPIH.INVOICE_TYPE
			INNER JOIN sys_users on SPIH.CREATED_BY = sys_users.intUserId
			WHERE SPIH.COMPANY_ID='$companyId' AND
			SPIH.PURCHASE_INVOICE_NO='$purInvoiceNo' AND
			SPIH.PURCHASE_INVOICE_YEAR='$purInvoiceYear'";
				
	$result = $db->RunQuery($sql);
	$header_array = mysqli_fetch_array($result);
	return $header_array;
}

function GetGridDetails($purInvoiceNo,$purInvoiceYear)
{
	global $db;
	
	$sql = "SELECT SPID.ITEM_ID,
			MI.strName AS itemName,
			MI.strCode,
			MU.strCode AS unit,
			ROUND(SPID.UNIT_PRICE,4) AS UNIT_PRICE,
			ROUND(SPID.QTY,2) AS QTY,
			SPID.DISCOUNT,
			SPID.TAX_AMOUNT
			FROM finance_supplier_purchaseinvoice_details SPID
			INNER JOIN mst_item MI ON SPID.ITEM_ID = MI.intId 
			INNER JOIN mst_units MU ON MU.intId = MI.intUOM
			WHERE SPID.PURCHASE_INVOICE_NO='$purInvoiceNo' AND
			SPID.PURCHASE_INVOICE_YEAR='$purInvoiceYear' ";
	return $db->RunQuery($sql);
}
?>
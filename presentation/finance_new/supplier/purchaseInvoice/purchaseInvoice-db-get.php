<?php 
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$requestType 		= $_REQUEST['requestType'];

include 		"../../../../dataAccess/Connector.php";
include  		"../../../../class/finance/cls_calculate_tax.php";
include  		"../../../../class/finance/cls_get_gldetails.php";
include_once 	"../../../../class/masterData/exchange_rate/cls_exchange_rate_get.php";
include_once  	"../../../../class/cls_commonFunctions_get.php";

$obj_get_GLData		= new Cls_Get_GLDetails($db);
$obj_exchgRate_get	= new cls_exchange_rate_get($db);
$obj_calculate_tax	= new Cls_Calculate_Tax($db);
$obj_common			= new cls_commonFunctions_get($db);

if($requestType=='loadData')
{
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	
	$invoiceNo			= $obj_common->replace($arrHeader["invoiceNo"]);
	$date				= $arrHeader['date'];
	
	$sql = "SELECT distinct PO.intSupplier,MS.strName
			FROM ware_grnheader GH
			INNER JOIN mst_locations ML ON ML.intId=GH.intCompanyId
			INNER JOIN trn_poheader PO ON GH.intPoNo=PO.intPONo AND GH.intPoYear=PO.intPOYear
			INNER JOIN mst_supplier MS ON PO.intSupplier=MS.intId
			WHERE GH.intStatus=1 AND
			ML.intCompanyId='$companyId' AND
			GH.strInvoiceNo='$invoiceNo' ";
	
	$result = $db->RunQuery($sql);
	$html 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$html.="<option value=\"".$row['intSupplier']."\">".$row['strName']."</option>";
		$supplierId = $row['intSupplier'];
	}
	if(mysqli_num_rows($result)>1)
	{
		$response['type']			= 'pass';
		$response['supplierCombo']	= $html;
		$response['supplierId']		= '';
		echo json_encode($response);
	}
	else if(mysqli_num_rows($result)==1)
	{
		loadDetalis($invoiceNo,$supplierId,$companyId,$html,$date);
	}
	else
	{
		$response['type']			= 'fail';
		$response['msg']			= 'Invalid Invoice No.';
		echo json_encode($response);
	}
}
else if($requestType=='loadSupData')
{
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	
	$invoiceNo			= $obj_common->replace($arrHeader["invoiceNo"]);
	$supplierId			= trim($arrHeader['supplierId']);
	$date				= $arrHeader['date'];
	
	loadDetalis($invoiceNo,$supplierId,$companyId,'',$date);
}
else if($requestType=='URLCalculateTax')
{
	$PONoArr	= explode('/',$_REQUEST["PONoArr"]);
	$itemId		= $_REQUEST['itemId'];
	$PONo		= $PONoArr[0];
	$POYear		= $PONoArr[1];
	//echo $obj_calculate_tax->CalculateTax($taxId,$value);
	$sql = "SELECT SUM(ROUND(((dblQty*dblUnitPrice)*((100-dblDiscount)/100)),2)) AS POAmount,
			ROUND(SUM(dblTaxAmmount),2) totTaxAmount FROM trn_podetails 
			WHERE intPONo='$PONo' AND 
			intPOYear='$POYear' and
			intItem='$itemId' ";

	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	$taxPerQty = $row['totTaxAmount']/$row['POAmount'];
	
	$response["TaxValue"]	= $taxPerQty;
	echo json_encode($response);
}
else if($requestType=='loadExchangeRate')
{
	$currency				= $_REQUEST['currencyId'];
	$date	  				= $_REQUEST['date'];
	
	$row					= $obj_exchgRate_get->GetAllValues($currency,4,$date,$companyId,'RunQuery');
	$response['exchgRate']	= $row['AVERAGE_RATE'];
	
	echo json_encode($response);
}
function getLedgerAccount($spplierId,$companyId)
{
	global $db;
	
	$sql = "SELECT DISTINCT intChartOfAccountId
			FROM mst_financesupplieractivate SA
			WHERE intSupplierId='$spplierId' AND
			intCompanyId='$companyId' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['intChartOfAccountId'];
	
}
function getInoviceType($spplierId)
{
	global $db;
	
	$sql = "SELECT intInvoiceType FROM mst_supplier WHERE intId='$spplierId' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['intInvoiceType'];
}
function getCurrency($PONo,$POYear)
{
	global $db;
	
	$sql = "SELECT intCurrency FROM trn_poheader WHERE intPONo='$PONo' AND intPOYear='$POYear' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['intCurrency'];
}
function getPODetails($invoiceNo,$supplierId,$companyId)
{
	global $db;
	$sql = "SELECT DISTINCT PO.intPONo,PO.intPOYear,GH.intGrnNo,GH.intGrnYear,FPT.strDescription
			FROM ware_grnheader GH
			INNER JOIN mst_locations ML ON ML.intId=GH.intCompanyId
			INNER JOIN trn_poheader PO ON GH.intPoNo=PO.intPONo AND GH.intPoYear=PO.intPOYear
			INNER JOIN mst_financepaymentsterms FPT ON FPT.intId=PO.intPaymentTerm
			WHERE GH.intStatus=1 AND
			ML.intCompanyId='$companyId' AND
			GH.strInvoiceNo='$invoiceNo' AND
			PO.intSupplier='$supplierId' AND
			PO.PAYMENT_COMPLETED_FLAG = 0 ";
	
	return $db->RunQuery($sql);
}
function getDetailData($invoiceNo,$companyId,$supplierId)
{
	global $db;
	
	$sql = "SELECT
			ware_grndetails.dblInvoiceBalanceQty AS dblQty,
			IFNULL((SELECT ABS(SUM(dblQty))
			FROM ware_stocktransactions_bulk STB
			WHERE STB.intGRNNo=ware_grnheader.intGrnNo AND
			STB.intGRNYear=ware_grnheader.intGrnYear AND
			STB.intItemId=mst_item.intId AND
			STB.intLocationId=ware_grnheader.intCompanyId AND
			STB.strType='RTSUP'),0) AS RTSUPQty1,
			ware_grndetails.dblRetunSupplierQty	AS RTSUPQty,
			ROUND(SUM((ware_grndetails.dblInvoiceBalanceQty-ware_grndetails.dblRetunSupplierQty)*ware_grndetails.dblGrnRate),2) AS amount,
			ware_grndetails.dblGrnRate,
			ware_grndetails.dblInvoiceRate,
			trn_podetails.intTaxCode,
			trn_podetails.dblDiscount,
			mst_item.intId AS itemId,
			mst_item.strName,
			mst_item.intUOM, 
			mst_item.strCode,
			mst_units.strCode AS unit, 
			mst_maincategory.strName AS mainCategory,
			mst_subcategory.strName AS subCategory,
			mst_subcategory.intId AS subCatId,
			ware_grnheader.intCompanyId,
			trn_podetails.SVAT_ITEM,
			FCOAS.CHART_OF_ACCOUNT_ID
			FROM ware_grndetails 
			INNER JOIN ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
			INNER JOIN mst_item ON ware_grndetails.intItemId = mst_item.intId
			INNER JOIN (SELECT * FROM trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem ) AS trn_podetails ON ware_grnheader.intPoNo = trn_podetails.intPONo AND 
			ware_grnheader.intPoYear = trn_podetails.intPOYear AND ware_grndetails.intItemId = trn_podetails.intItem
			INNER JOIN trn_poheader ON trn_poheader.intPONo=trn_podetails.intPONo AND trn_poheader.intPOYear=trn_podetails.intPOYear
			INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
			INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
			INNER JOIN mst_units ON mst_units.intId = mst_item.intUOM
			INNER JOIN mst_locations ON mst_locations.intId=ware_grnheader.intCompanyId
			LEFT JOIN finance_mst_chartofaccount_subcategory FCOAS ON FCOAS.SUBCATEGORY_ID=mst_item.intSubCategory
			WHERE
			ware_grnheader.strInvoiceNo = '$invoiceNo' AND
			mst_locations.intCompanyId = '$companyId' AND
			trn_poheader.intSupplier = '$supplierId' AND ware_grnheader.intStatus=1 and trn_poheader.intStatus=1
			GROUP BY
			ware_grndetails.intItemId     
			ORDER BY mst_maincategory.strName ASC, mst_subcategory.strName ASC, mst_item.strName asc";
	
	$result = $db->RunQuery($sql);
	
	while($row=mysqli_fetch_array($result))
	{
		$data['itemId'] 	 	= $row['itemId'];
		$data['mainCategory'] 	= $row['mainCategory'];
		$data['subCategory'] 	= $row['subCategory'];
		$data['subCatId'] 		= $row['subCatId'];
		$data['itemCode'] 	 	= $row['strCode'];
		$data['itemName'] 		= $row['strName'];
		$data['UOM']   			= $row['unit'];
		$data['unitPrice'] 	 	= $row['dblGrnRate'];
		$data['SVAT_ITEM'] 	 	= $row['SVAT_ITEM'];
		$data['Qty'] 			= ($row['dblQty']-$row['RTSUPQty']);
		$data['Discount'] 		= $row['dblDiscount'];
		//$amount					= ($row['dblQty']-$row['RTSUPQty'])*$row['dblGrnRate'];
		//$data['amount']   		= (($amount*(100-$row['dblDiscount']))/100);
		$data['amount']   		= $row['amount'];
		$data['taxCode'] 		= $row['intTaxCode'];
		$data['COAId'] 			= $row['CHART_OF_ACCOUNT_ID'];
		$htmlTax				= getTaxCombo($row['intTaxCode']);
		$htmlCostCenter			= getCostCenter($row['intCompanyId']);
		$data['taxHtml'] 		= $htmlTax;
		$data['costCenterHtml'] = $htmlCostCenter;
		
		//$dataGL['GLAccountId']	= $obj_get_GLData->getGLData('SUPPLIER_INVOICE',$row['subCategory'],$row['intTaxCode']);
		$arrDetailData[] 		= $data;		
	}
	return $arrDetailData;
}
function getSubCatGLData($invoiceNo,$companyId,$supplierId)
{
	global $db;
	global $obj_get_GLData;
	
	$sql = "SELECT 
			FCOAS.CHART_OF_ACCOUNT_ID,
			GROUP_CONCAT(tbl1.intSubCategory) AS subCategory
			FROM
			(
			SELECT DISTINCT
						mst_item.intSubCategory
						FROM ware_grndetails 
						INNER JOIN ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
						INNER JOIN mst_item ON ware_grndetails.intItemId = mst_item.intId
						INNER JOIN (SELECT * FROM trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem ) AS trn_podetails ON ware_grnheader.intPoNo = trn_podetails.intPONo AND 
						ware_grnheader.intPoYear = trn_podetails.intPOYear AND ware_grndetails.intItemId = trn_podetails.intItem
						INNER JOIN trn_poheader ON trn_poheader.intPONo=trn_podetails.intPONo AND trn_poheader.intPOYear=trn_podetails.intPOYear
						INNER JOIN mst_locations ON mst_locations.intId=ware_grnheader.intCompanyId
						WHERE
						ware_grnheader.strInvoiceNo = '$invoiceNo' AND
						mst_locations.intCompanyId = '$companyId' AND
						trn_poheader.intSupplier = '$supplierId'
						GROUP BY
						mst_item.intId) AS tbl1
			INNER JOIN finance_mst_chartofaccount_subcategory FCOAS ON FCOAS.SUBCATEGORY_ID=tbl1.intSubCategory
			GROUP BY FCOAS.CHART_OF_ACCOUNT_ID ";
	$result = $db->RunQuery($sql);
	$html 	= '';
	while($row=mysqli_fetch_array($result))
	{
		$html	.= "<tr id=\"SubCat\">";
		$html	.= "<td style=\"text-align:center\"><img border=\"0\" src=\"images/del.png\" alt=\"Save\" name=\"butDel\" class=\"mouseover clsDel\" id=\"butDel\" tabindex=\"24\"/></td>";
		$html	.= "<td style=\"text-align:center\" id=\"\"><select name=\"cboLedgerAc\" id=\"cboLedgerAc\" class=\"clsLedgerAc validate[required]\" style=\"width:100%\">".

                    $obj_get_GLData->getGLCombo('SUPPLIER_INVOICE',$row['CHART_OF_ACCOUNT_ID'])

                    ."</select></td>";
		$html	.= "<td style=\"text-align:center\"><input name=\"txtLedgerAmonut\" id=\"txtLedgerAmonut\" class=\"clsLedgerAmount validate[required]\" type=\"text\" style=\"width:100%;text-align:right\" value=\"0.00\"/></td>";
		$html	.= "<\tr>";

	}
	return $html;
}
function getTaxGLData($html,$invoiceNo,$companyId,$supplierId)
{
	global $db;
	global $obj_get_GLData;
	
	$sql = "SELECT 
			FCOAT.CHART_OF_ACCOUNT_ID,
			GROUP_CONCAT(tbl1.intTaxCode) AS taxId
			FROM
			(
			SELECT DISTINCT
						trn_podetails.intTaxCode
						FROM ware_grndetails 
						INNER JOIN ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
						INNER JOIN mst_item ON ware_grndetails.intItemId = mst_item.intId
						INNER JOIN (SELECT * FROM trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem ) AS trn_podetails ON ware_grnheader.intPoNo = trn_podetails.intPONo AND 
						ware_grnheader.intPoYear = trn_podetails.intPOYear AND ware_grndetails.intItemId = trn_podetails.intItem
						INNER JOIN trn_poheader ON trn_poheader.intPONo=trn_podetails.intPONo AND trn_poheader.intPOYear=trn_podetails.intPOYear
						INNER JOIN mst_locations ON mst_locations.intId=ware_grnheader.intCompanyId
						WHERE
						ware_grnheader.strInvoiceNo = '$invoiceNo' AND
						mst_locations.intCompanyId = '$companyId' AND
						trn_poheader.intSupplier = '$supplierId'
						GROUP BY
						mst_item.intId) AS tbl1
			INNER JOIN finance_mst_chartofaccount_tax FCOAT ON FCOAT.TAX_ID=tbl1.intTaxCode
			GROUP BY FCOAT.CHART_OF_ACCOUNT_ID";
	//echo $sql;
	$result = $db->RunQuery($sql);
	
	while($row=mysqli_fetch_array($result))
	{
		$html	.= "<tr id=\"Tax\">";
		$html	.= "<td style=\"text-align:center\"><img border=\"0\" src=\"images/del.png\" alt=\"Save\" name=\"butDel\" class=\"mouseover clsDel\" id=\"butDel\" tabindex=\"24\"/></td>";
		$html	.= "<td style=\"text-align:center\" id=\"".$row['taxId']."\"><select name=\"cboLedgerAc\" id=\"cboLedgerAc\" class=\"clsLedgerAc validate[required]\"  style=\"width:100%\">".

                    $obj_get_GLData->getGLCombo('SUPPLIER_INVOICE',$row['CHART_OF_ACCOUNT_ID'])

                    ."</select></td>";
		$html	.= "<td style=\"text-align:center\"><input name=\"txtLedgerAmonut\" id=\"txtLedgerAmonut\" class=\"clsLedgerAmount validate[required]\" type=\"text\" style=\"width:100%;text-align:right\" value=\"0.00\"/></td>";
		$html	.= "<\tr>";
	}
	return $html;
}
function getTaxCombo($taxId)
{
	global $db;
	
	$sql = "SELECT intId,strCode
			FROM mst_financetaxgroup
			WHERE intStatus=1 ";
	
	$result = $db->RunQuery($sql);
	$html = "<select name=\"cboTax\" id=\"cboTax\" class=\"clsTax\" style=\"width:100%\" disabled=\"disabled\">";
	$html.= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		if($taxId==$row['intId'])
			$html.="<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";
		else
			$html.="<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
	}
	$html.= "</select>";
	
	return $html;
}
function getCostCenter($location)
{
	global $db;
	
	$sql = "SELECT intId,strName,intLocation
			FROM mst_financedimension
			WHERE intStatus=1 ";
			
	$result = $db->RunQuery($sql);
	$html = "<select name=\"cboCostCenter\" id=\"cboCostCenter\" class=\"clsCostCener\" style=\"width:100%\">";
	$html.= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		if($row['intLocation']==$location)
			$html.="<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
		else
			$html.="<option value=\"".$row['intId']."\" >".$row['strName']."</option>";
	}
	$html.= "</select>";
	
	return $html;
}
function loadDetalis($invoiceNo,$supplierId,$companyId,$html,$date)
{
	global $obj_exchgRate_get;
	global $obj_get_GLData;
	//$ledgerAccount				= getLedgerAccount($supplierId,$companyId);
	$GLHtml						= '';
	$invoiceType				= getInoviceType($supplierId);
	$resultPO					= getPODetails($invoiceNo,$supplierId,$companyId);
	$rowPO						= mysqli_fetch_array($resultPO);
	if(mysqli_num_rows($resultPO)==0)
	{
		$response['type']			= 'fail';
		$response['msg']			= 'This invoice marked as payment completed.';
		echo json_encode($response);
		return;
	}
	$response['PONo'] 			= $rowPO['intPONo'];
	$response['POYear'] 		= $rowPO['intPOYear'];
	$response['term'] 			= $rowPO['strDescription'];
	$response['GRNNo'] 			= $rowPO['intGrnNo'];
	$response['GRNYear'] 		= $rowPO['intGrnYear'];	
	
	$currency					= getCurrency($rowPO['intPONo'],$rowPO['intPOYear']);
	$row						= $obj_exchgRate_get->GetAllValues($currency,4,$date,$companyId,'RunQuery');
	$response['exchgRate']		= $row['AVERAGE_RATE'];
	
	$response['arrDetailData']	= getDetailData($invoiceNo,$companyId,$supplierId);
	//$response['arrSubCatGLData']	= getSubCatGLData($invoiceNo,$companyId,$supplierId);
	//$response['arrTaxGLData']	= getTaxGLData($invoiceNo,$companyId,$supplierId);
	$GLHtml						= getSubCatGLData($invoiceNo,$companyId,$supplierId);
	$GLHtml						= getTaxGLData($GLHtml,$invoiceNo,$companyId,$supplierId);
	
	if($GLHtml=='')
	{
		$GLHtml	.= "<tr>";
		$GLHtml	.= "<td style=\"text-align:center\"><img border=\"0\" src=\"images/del.png\" alt=\"Save\" name=\"butDel\" class=\"mouseover clsDel\" id=\"butDel\" tabindex=\"24\"/></td>";
		$GLHtml	.= "<td style=\"text-align:center\"><select name=\"cboLedgerAc\" id=\"cboLedgerAc\" class=\"clsLedgerAc validate[required]\" style=\"width:100%\">".

                    $obj_get_GLData->getGLCombo('SUPPLIER_INVOICE','')

                    ."</select></td>";
		$GLHtml	.= "<td style=\"text-align:center\"><input name=\"txtLedgerAmonut\" id=\"txtLedgerAmonut\" class=\"clsLedgerAmount validate[required]\" type=\"text\" style=\"width:100%;text-align:right\" value=\"0.00\"/></td>";
		$GLHtml	.= "<\tr>";
	}
	
	$response['GLHTML']			= $GLHtml;
	$response['type']			= 'pass';
	if($html!='')
	{
		$response['supplierCombo']	= $html;
		$response['supplierId']		= $supplierId;
	}
	//$response['ledgerAccount']	= $ledgerAccount;
	$response['invoiceType']	= $invoiceType;
	$response['currency']		= $currency;
	echo json_encode($response);
}
?>
// JavaScript Document
var basePath	= "presentation/finance_new/supplier/purchaseInvoice/";
var menuId		= 676;
var POReportId	= 934;
var GRNReportId	= 895;
$(document).ready(function(){
	
	$("#frmPurchaseInvoice").validationEngine();
	
	$('#frmPurchaseInvoice #txtInvoiceNo').die('keypress').live('keypress',function(e){loadData('enter',e,this)});
	$('#frmPurchaseInvoice #btnSearch').die('click').live('click',function(e){loadData('click',e,this)});
	$('#frmPurchaseInvoice #cboSupplier').die('change').live('change',loadSupData);
	$('#frmPurchaseInvoice #chkAll').die('click').live('click',checkAll);
	$('#frmPurchaseInvoice #butSave').die('click').live('click',saveData);
	$('#frmPurchaseInvoice .clsQty').die('keyup').live('keyup',setMaxQty);
	$('#frmPurchaseInvoice .clsQty').die('blur').live('blur',calTaxAndTotals);
	$('#frmPurchaseInvoice #butInsertRow').die('click').live('click',addNewRow);
	$('#frmPurchaseInvoice .clsDel').die('click').live('click',deleteRow);
	$('#frmPurchaseInvoice .clsLedgerAmount').die('keyup').live('keyup',setAmount);
	$('#frmPurchaseInvoice .clsChkItem').die('click').live('click',setTotalAmount);
	//$('.clsDiscount').live('keyup',setDiscountedAmount);
	$('#frmPurchaseInvoice .clsTax').die('change').live('change',CalculateTax);
	$('#frmPurchaseInvoice #butNew').die('click').live('click',reloadPage);
	$('#frmPurchaseInvoice #butCancel').die('click').live('click',cancleInvoice);
	$('#frmPurchaseInvoice #txtDate').die('blur').live('blur',loadExchangeRate);
	
	if(invoiceNoAL!='')
		LoadAutoData(invoiceNoAL)
});
function loadData(type,evnt,obj)
{
	clearAll('Invoice');
	if(type=='enter')
	{
		if(evnt.keyCode!=13)
			return
	}
	showWaiting();
	$("#frmPurchaseInvoice #tblMain tr:gt(0)").remove();
	
	$('#frmPurchaseInvoice #txtPurchaseInvNo').val('');
	$('#frmPurchaseInvoice #txtPurchaseInvYear').val('');
	
	var invoiceNo = $('#frmPurchaseInvoice #txtInvoiceNo').val();
	var date	  = $('#frmPurchaseInvoice #txtDate').val();
	
	var arrHeader = "{";
						arrHeader += '"invoiceNo":'+URLEncode_json(invoiceNo)+',';
						arrHeader += '"date":"'+date+'"' ;
		arrHeader += "}";
	
	var arrHeader	= arrHeader;
	
	var url = basePath+"purchaseInvoice-db-get.php?requestType=loadData";
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',  
		data:"&arrHeader="+arrHeader,
		async:false,
		success:function(json){
			if(json.type=='pass')
			{
				$('#frmPurchaseInvoice #cboSupplier').html(json.supplierCombo);
				$('#frmPurchaseInvoice #cboSupplier').val(json.supplierId);
				$('#frmPurchaseInvoice #txtCurrencyRate').val(json.exchgRate);
				$('#frmPurchaseInvoice #cboInvoiceType').val(json.invoiceType);
				$('#frmPurchaseInvoice #cboCurrency').val(json.currency);
				
				if(json.supplierId=='')
				{
					$('#frmPurchaseInvoice #cboSupplier').validationEngine('showPrompt','Please select a Supplier.','fail');
					hideWaiting();
					return;
				}
				
				$('.clsPONo').html('<a href="?q='+POReportId+'&poNo='+json.PONo+'&year='+json.POYear+'" target="_blank">'+json.PONo+'/'+json.POYear+'</a>');
				$('.clsTerms').html(json.term);
				$('.clsGRNNo').html('<a href="?q='+GRNReportId+'&grnNo='+json.GRNNo+'&year='+json.GRNYear+'" target="_blank">'+json.GRNNo+'/'+json.GRNYear+'</a>');
				
				if(json.arrDetailData==null)
				{
					$('#frmPurchaseInvoice #txtInvoiceNo').validationEngine('showPrompt','Detail data loading Error.','fail');
					hideWaiting();
					return;
				}
				var lengthDetail   = json.arrDetailData.length;
				var arrDetailData  = json.arrDetailData;	
				for(var i=0;i<lengthDetail;i++)
				{
					var itemId			= arrDetailData[i]['itemId'];
					var mainCategory	= arrDetailData[i]['mainCategory'];	
					var subCategory		= arrDetailData[i]['subCategory'];
					var subCatId		= arrDetailData[i]['subCatId'];
					var itemCode		= arrDetailData[i]['itemCode'];
					var itemName		= arrDetailData[i]['itemName'];
					var UOM				= arrDetailData[i]['UOM'];
					var Discount		= arrDetailData[i]['Discount'];
					var unitPrice		= RoundNumber(arrDetailData[i]['unitPrice'],4);
					var Qty				= RoundNumber(arrDetailData[i]['Qty'],2);
					var amount			= arrDetailData[i]['amount'];
					var taxHtml			= arrDetailData[i]['taxHtml'];
					var costCenterHtml	= arrDetailData[i]['costCenterHtml'];
					var svatItem		= arrDetailData[i]['SVAT_ITEM'];
					var COAId			= arrDetailData[i]['COAId'];
					
					createGrid(itemId,mainCategory,subCategory,subCatId,itemCode,itemName,UOM,Discount,unitPrice,Qty,amount,taxHtml,costCenterHtml,svatItem,COAId);
				}
				
				$('#frmPurchaseInvoice #tblLedger tbody').html(json.GLHTML);
				hideWaiting();
			}
			else
			{
				$('#frmPurchaseInvoice #txtInvoiceNo').validationEngine('showPrompt', json.msg,'fail');
				hideWaiting();
				return;
			}
			
			
		},
		error:function(xhr,status){
				
				$('#frmPurchaseInvoice #txtInvoiceNo').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				hideWaiting();
				return;
			}		
	});
}
function loadSupData()
{
	clearAll('Supplier');
	if($(this).val()=='')
	{
		return;
	}
	showWaiting();
	$("#frmPurchaseInvoice #tblMain tr:gt(0)").remove();
	
	var invoiceNo 	= $('#frmPurchaseInvoice #txtInvoiceNo').val();
	var supplierId 	= $(this).val();
	var date	  	= $('#frmPurchaseInvoice #txtDate').val();
	
	var arrHeader = "{";
						arrHeader += '"invoiceNo":'+URLEncode_json(invoiceNo)+',';
						arrHeader += '"supplierId":"'+supplierId+'",' ;
						arrHeader += '"date":"'+date+'"' ;
		arrHeader += "}";
	
	var arrHeader	= arrHeader;
	
	var url = basePath+"purchaseInvoice-db-get.php?requestType=loadSupData";
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',  
		data:"&arrHeader="+arrHeader,
		async:false,
		success:function(json){
			if(json.type=='pass')
			{
				$('#frmPurchaseInvoice #txtCurrencyRate').val(json.exchgRate);
				$('#frmPurchaseInvoice #cboInvoiceType').val(json.invoiceType);
				$('#frmPurchaseInvoice #cboCurrency').val(json.currency);
				
				$('#frmPurchaseInvoice .clsPONo').html('<a href="presentation/procurement/purchaseOrder/listing/rptPurchaseOrder.php?poNo='+json.PONo+'&year='+json.POYear+'" target="new">'+json.PONo+'/'+json.POYear+'</a>');
				$('#frmPurchaseInvoice .clsTerms').html(json.term);
				$('#frmPurchaseInvoice .clsGRNNo').html('<a href="presentation/warehouse/grn/listing/rptGrn.php?grnNo='+json.GRNNo+'&year='+json.GRNYear+'" target="new">'+json.GRNNo+'/'+json.GRNYear+'</a>');
				
				var lengthDetail   = json.arrDetailData.length;
				var arrDetailData  = json.arrDetailData;	
				for(var i=0;i<lengthDetail;i++)
				{
					var itemId			= arrDetailData[i]['itemId'];
					var mainCategory	= arrDetailData[i]['mainCategory'];	
					var subCategory		= arrDetailData[i]['subCategory'];
					var subCatId		= arrDetailData[i]['subCatId'];
					var itemCode		= arrDetailData[i]['itemCode'];
					var itemName		= arrDetailData[i]['itemName'];
					var UOM				= arrDetailData[i]['UOM'];
					var Discount		= arrDetailData[i]['Discount'];
					var unitPrice		= RoundNumber(arrDetailData[i]['unitPrice'],4);
					var Qty				= RoundNumber(arrDetailData[i]['Qty'],2);
					var amount			= arrDetailData[i]['amount'];
					var taxHtml			= arrDetailData[i]['taxHtml'];
					var costCenterHtml	= arrDetailData[i]['costCenterHtml'];
					var svatItem		= arrDetailData[i]['SVAT_ITEM'];
					var COAId			= arrDetailData[i]['COAId'];
					
					createGrid(itemId,mainCategory,subCategory,subCatId,itemCode,itemName,UOM,Discount,unitPrice,Qty,amount,taxHtml,costCenterHtml,svatItem,COAId);
				}
				
				$('#frmPurchaseInvoice #tblLedger tbody').html(json.GLHTML);
				hideWaiting();
			}
			else
			{
				$('#frmPurchaseInvoice #cboSupplier').validationEngine('showPrompt', json.msg,'fail');
				hideWaiting();
				return;
			}	
			
		},
		error:function(xhr,status){
				
				$('#frmPurchaseInvoice #cboSupplier').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				hideWaiting();
				return;
			}		
	});
}
function createGrid(itemId,mainCategory,subCategory,subCatId,itemCode,itemName,UOM,Discount,unitPrice,Qty,amount,taxHtml,costCenterHtml,svatItem,COAId)
{
	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);
	row.id			= svatItem;
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
	cell.id			= itemId;
	if(Qty==0)
		cell.innerHTML  = "&nbsp;";
	else
		cell.innerHTML  = "<input type=\"checkbox\" id=\"chkItem\" name=\"chkItem\" class=\"clsChkItem validate[minCheckbox[1]]\">";
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsMainCat';
	cell.innerHTML 	= mainCategory;
	
	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsSubCat';
	cell.id			= subCatId;
	cell.innerHTML 	= subCategory;
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsItemCode';
	cell.innerHTML 	= itemCode;
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsItem';
	cell.id			= COAId;
	cell.innerHTML 	= itemName;
	
	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsUOM';
	cell.innerHTML 	= UOM;
	
	var cell 		= row.insertCell(6);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsUnitPrice';
	cell.innerHTML 	= unitPrice;
	
	var cell 		= row.insertCell(7);
	cell.setAttribute("style",'text-align:center');
	cell.id			= Qty;
	cell.innerHTML 	= "<input type=\"textbox\" id=\"txtQty\" class=\"clsQty validate[custom[number]]\" style=\"width:100%;text-align:right\" value=\""+Qty+"\" "+(Qty==0?'disabled="disabled"':'')+">";
	
	var cell 		= row.insertCell(8);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<input type=\"textbox\" id=\"txtDiscount\" class=\"clsDiscount validate[custom[number]]\" style=\"width:100%;text-align:right\" value=\""+Discount+"\" disabled=\"disabled\">";
	
	var cell 		= row.insertCell(9);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<input type=\"textbox\" id=\"txtAmount\" class=\"clsAmount\" style=\"width:100%;text-align:right\" disabled=\"disabled\" value=\""+amount+"\"><input type=\"hidden\" id=\"txtTaxAmount\" class=\"clsTaxAmount\" value=\"0\" >";
	
	var cell 		= row.insertCell(10);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= taxHtml;
	
	var cell 		= row.insertCell(11);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= costCenterHtml;
}
function saveData()
{
	if(!IsProcessMonthLocked_date($('#frmPurchaseInvoice #txtDate').val()))
		return;
		
	showWaiting();
	var PurInvoiceNo 	= $('#frmPurchaseInvoice #txtPurchaseInvNo').val();
	var PurInvoiceYear 	= $('#frmPurchaseInvoice #txtPurchaseInvYear').val();
	var invoiceNo 		= $('#frmPurchaseInvoice #txtInvoiceNo').val();
	var supplierId 		= $('#frmPurchaseInvoice #cboSupplier').val();
	//var ledgerAccountId = $('#cboLedgerAccount').val();
	var invoiceType 	= $('#frmPurchaseInvoice #cboInvoiceType').val();
	var currency 		= $('#frmPurchaseInvoice #cboCurrency').val();
	var PurDate 		= $('#frmPurchaseInvoice #txtDate').val();
	var Remarks 		= $('#frmPurchaseInvoice #txtRemarks').val();
	var svatAmount	 	= $('#frmPurchaseInvoice #txtSvatTotal').val();
	var grandTotal 		= parseFloat($('#frmPurchaseInvoice #txtTotal').val());
	var GRNArr          = $('#frmPurchaseInvoice .clsGRNNo').children().html();
	var POArr           = $('#frmPurchaseInvoice .clsPONo').children().html();
	
	if($('#frmPurchaseInvoice').validationEngine('validate'))
	{
		var totLedgerAmount = parseFloat($('#frmPurchaseInvoice #txtLedgerTotAmonut').val());
		
		if(totLedgerAmount!=grandTotal)
		{
			$('#frmPurchaseInvoice #txtLedgerTotAmonut').validationEngine('showPrompt','Total Ledger amount must equal to Total.','fail');
			hideWaiting();
			return;
		}
		if(PurInvoiceNo!='' && PurInvoiceYear!='')
		{
			$('#frmPurchaseInvoice #butSave').validationEngine('showPrompt','You can not update this Invoice.','fail');
			hideWaiting();
			return;
		}
		
		var arrHeader = "{";
						arrHeader += '"PurInvoiceNo":"'+PurInvoiceNo+'",' ;
						arrHeader += '"PurInvoiceYear":"'+PurInvoiceYear+'",' ;
						arrHeader += '"invoiceNo":'+URLEncode_json(invoiceNo)+',';
						arrHeader += '"supplierId":"'+supplierId+'",' ;
						arrHeader += '"invoiceType":"'+invoiceType+'",' ;
						arrHeader += '"currency":"'+currency+'",' ;
						arrHeader += '"PurDate":"'+PurDate+'",' ;
						arrHeader += '"Remarks":'+URLEncode_json(Remarks)+',';
						arrHeader += '"svatAmount":"'+svatAmount+'",' ;
						arrHeader += '"grandTotal":"'+grandTotal+'",' ;
						arrHeader += '"GRNArr":"'+GRNArr+'",' ;
						arrHeader += '"POArr":"'+POArr+'"' ;

		arrHeader += "}";
		
		var arrHeader	= arrHeader;
		
		value = "[ ";
		var chkStatus  = false;
		var qtyStatus  = false;
		$('#frmPurchaseInvoice #tblMain .clsChkItem').each(function(){
			
			chkStatus = true;
			
			if($(this).attr('checked'))
			{
				
				var itemId 		= $(this).parent().attr('id');
				var qty 		= $(this).parent().parent().find('.clsQty').val();
				var unitPrice 	= $(this).parent().parent().find('.clsUnitPrice').html();
				var discount 	= $(this).parent().parent().find('.clsDiscount').val();
				var taxAmount 	= $(this).parent().parent().find('.clsTaxAmount').val();
				var taxCode 	= $(this).parent().parent().find('.clsTax').val();
				var costCenter 	= $(this).parent().parent().find('.clsCostCener').val();
				
				if(qty<=0)
				{
					$(this).parent().parent().find('.clsQty').validationEngine('showPrompt','Qty must greater than 0.','fail');
					qtyStatus = true;
					hideWaiting();
					return false;
				}
			
				value += '{"itemId": "'+itemId+'", "qty": "'+qty+'", "unitPrice": "'+unitPrice+'", "discount": "'+discount+'", "taxAmount": "'+taxAmount+'", "taxCode": "'+taxCode+'", "costCenter": "'+costCenter+'" },';
			}
		});
			
			value = value.substr(0,value.length-1);
			value += " ]";
			
			if(qtyStatus)
			{
				return;
				
			}
			if(!chkStatus)
			{
				$(this).validationEngine('showPrompt','No records to save.','fail');
				hideWaiting();	
				return;
			}
		
		ledgerValue = "[ ";
		$('#frmPurchaseInvoice #tblLedger .clsLedgerAmount').each(function(){
			
				var ledgerAccount 	= $(this).parent().parent().find('.clsLedgerAc').val();
				var amount 			= $(this).val();
				if(amount!=0)
					ledgerValue += '{"ledgerAccount": "'+ledgerAccount+'", "amount": "'+amount+'" },';	
		});	
		ledgerValue = ledgerValue.substr(0,ledgerValue.length-1);
		ledgerValue += " ]";
		
			var url = basePath+"purchaseInvoice-db-set.php?requestType=saveData";
			var obj = $.ajax({
				url:url,
				dataType: "json",  
				type:'POST',
				data:'&arrHeader='+arrHeader+'&itemDetails='+value+'&ledgerDetails='+ledgerValue,
				async:false,
				success:function(json){
						$('#frmPurchaseInvoice #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							var t		= setTimeout("alertx3()",3000);
							$('#frmPurchaseInvoice #txtPurchaseInvNo').val(json.purInvoiceNo);
							$('#frmPurchaseInvoice #txtPurchaseInvYear').val(json.purInvoiceYear);
							$('#frmPurchaseInvoice #butSave').hide();
							hideWaiting();
							return;
						}
						else
						{
							hideWaiting();
						}
					},
				error:function(xhr,status){
						
						$('#frmPurchaseInvoice #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
					}		
				});
		
	}
	else
	{
		hideWaiting();
	}
}
function setMaxQty()
{
	var maxVal = parseFloat($(this).parent().attr('id'));
	if(parseFloat($(this).val())>maxVal)
	{
		$(this).val(RoundNumber(maxVal,2));
	}
	var qty		  	= parseFloat($(this).val()==''?0:$(this).val());
	var unitPrice 	= parseFloat($(this).parent().parent().find('.clsUnitPrice').html());
	var amount		= RoundNumber((unitPrice*qty),2);
	
	$(this).parent().parent().find('.clsAmount').val(amount);
	$('#frmPurchaseInvoice .clsLedgerAmount').val('0.00');
	$('#frmPurchaseInvoice #txtLedgerTotAmonut').val('0.00');
	
	CaculateTotalValue();
		
}
function calTaxAndTotals()
{
	CalculateTax(this);
	CaculateTotalValue();
}
function setTotalAmount()
{
	var total = 0;
	$('#frmPurchaseInvoice .clsLedgerAmount').val('');
	$('#frmPurchaseInvoice #txtLedgerTotAmonut').val('');
	$('#frmPurchaseInvoice .clsChkItem').each(function(){
        
		if($(this).attr('checked'))
		{
			$(this).parent().parent().find('.clsCostCener').addClass('validate[required]');
		}
		else
		{
			$(this).parent().parent().find('.clsCostCener').removeClass('validate[required]');
		}
    });
	//setDiscountedAmount(this);
	CalculateTax(this);
	CaculateTotalValue();
}
function setAmount()
{
	var obj			 = this;
	var totLedAmount = 0;
	var totAmount 	 = parseFloat($('#frmPurchaseInvoice #txtTotalAmount').val());
	$('#frmPurchaseInvoice .clsLedgerAmount').each(function(){

		if($(this).val()!="")
			totLedAmount += parseFloat($(this).val());
	});
	if(totLedAmount>totAmount)
	{
		obj.value = RoundNumber((parseFloat(obj.value)-parseFloat(parseFloat(totLedAmount)-parseFloat(totAmount))),2).toFixed(2);
		totLedAmount = totAmount;
	}
	$('#frmPurchaseInvoice #txtLedgerTotAmonut').val(RoundNumber(totLedAmount,2).toFixed(2))	;
}
function setDiscountedAmount(obj)
{
		var discount 	= parseFloat($(obj).parent().parent().find('.clsDiscount').val());
		var qty			= parseFloat($(obj).parent().parent().find('.clsQty').val());
		var unitPrice	= parseFloat($(obj).parent().parent().find('.clsUnitPrice').html());
		var amount		= (qty*unitPrice);
		
		var newAmount	= ((amount*(100-discount))/100);
		$(obj).parent().parent().find('.clsAmount').val(RoundNumber(newAmount,2));
}
function checkAll()
{
	if($(this).attr('checked'))
	{
		$('#frmPurchaseInvoice .clsChkItem').attr("checked",true);
		$('#frmPurchaseInvoice .clsCostCener').addClass('validate[required]');
	}
	else
	{
		$('#frmPurchaseInvoice .clsChkItem').attr("checked",false);
		$('#frmPurchaseInvoice .clsCostCener').removeClass('validate[required]');
	}
	$('#frmPurchaseInvoice #tblMain .clsChkItem').each(function(){
		
		//setDiscountedAmount(this);
		CalculateTax(this);
		
	});
	CaculateTotalValue();
}
function addNewRow()
{
	if($('#frmPurchaseInvoice').validationEngine('validate'))
	{
		$('#frmPurchaseInvoice #tblLedger tbody tr:last').after("<tr>"+$('#frmPurchaseInvoice #tblLedger tbody tr:first').html()+"</tr>");
		$('#frmPurchaseInvoice #tblLedger tbody tr:last').find('.clsLedgerAc').val('');
		$('#frmPurchaseInvoice #tblLedger tbody tr:last').find('.clsLedgerAc').parent().attr('id','');
		$('#frmPurchaseInvoice #tblLedger tbody tr:last').find('.clsLedgerAmount').val('0.00');		
	}
}
function deleteRow()
{
	var delRowCount = $('#frmPurchaseInvoice #tblLedger tbody tr').length;
	
	if(delRowCount>1)
		$(this).parent().parent().remove();
		
	var totLedAmount = 0;
	$('#frmPurchaseInvoice .clsLedgerAmount').each(function(){

			totLedAmount += parseFloat(($(this).val()==''?0:$(this).val()));
	});
	$('#frmPurchaseInvoice #txtLedgerTotAmonut').val(RoundNumber(totLedAmount,2).toFixed(2));
}
function CalculateTax(obj)
{
	var url 	= basePath+"purchaseInvoice-db-get.php?requestType=URLCalculateTax";
	var data 	= "PONoArr="+$('.clsPONo').children().html()+"&itemId="+$(obj).parent().parent().find('.clsChkItem').parent().attr('id');
		
		var httpobj = $.ajax({
		url:url,
		data:data,
		dataType:'json',
		type:'POST',
		async:false,
		success:function(json)
		{
			var amount = parseFloat($(obj).parent().parent().find('.clsAmount').val());
			var taxAmount = amount*parseFloat(json.TaxValue);
			
			$(obj).parent().parent().find('.clsTaxAmount').val(RoundNumber(taxAmount,2));
			
		}
	});		
}
function CaculateTotalValue()
{
	var totalValue		= 0;
	var totalSvat		= 0;
	var grandTotal		= 0;
	var taxValue		= 0;
	var totLedAmount	= 0;
	
	$('#frmPurchaseInvoice #tblMain .clsAmount').each(function(){
		if($(this).parent().parent().find('.clsChkItem').attr('checked'))
		{
			totalValue += parseFloat($(this).val());
			taxValue   += parseFloat($(this).parent().find('.clsTaxAmount').val());
			
			if($(this).parent().parent().attr('id')==1)
			{
				totalSvat += (parseFloat($(this).val())+parseFloat($(this).parent().find('.clsTaxAmount').val()));
			}
		}	
	});
	
	$('#frmPurchaseInvoice #tblLedger .clsLedgerAc').each(function(){
				
		if($(this).parent().parent().attr('id')=='SubCat')
		{
			var totSubCatVal    = 0;
			var GLId 			= $(this).val();
			
			$('#frmPurchaseInvoice #tblMain .clsAmount').each(function(){
				if($(this).parent().parent().find('.clsChkItem').is(':checked') && GLId==$(this).parent().parent().find('.clsItem').attr('id'))
				{
					totSubCatVal += parseFloat($(this).val());
				}	
			});
			totLedAmount += parseFloat(totSubCatVal);
			$(this).parent().parent().find('.clsLedgerAmount').val(RoundNumber(totSubCatVal,2).toFixed(2));
		}
		if($(this).parent().parent().attr('id')=='Tax')
		{
			var totTaxVal    	= 0;
			var taxIdArr		= $(this).parent().attr('id').split(',');
			
			for(var i=0;i<taxIdArr.length;i++)
			{
				$('#frmPurchaseInvoice #tblMain .clsAmount').each(function(){
					if($(this).parent().parent().find('.clsChkItem').is(':checked') && taxIdArr[i]==$(this).parent().parent().find('.clsTax').val())
					{
						totTaxVal += parseFloat($(this).parent().find('.clsTaxAmount').val());
					}	
				});
			}
			
			totLedAmount += parseFloat(totTaxVal);
			$(this).parent().parent().find('.clsLedgerAmount').val(RoundNumber(totTaxVal,2).toFixed(2));
		}
		
	});	
	
	taxValue	 = RoundNumber(taxValue,2);
	totalValue	 = RoundNumber(totalValue,2);
	totalSvat	 = RoundNumber(totalSvat,2);
	grandTotal	 = parseFloat(totalValue)+parseFloat(taxValue);
	
	$('#frmPurchaseInvoice #txtTaxTotal').val(taxValue.toFixed(2));	
	$('#frmPurchaseInvoice #txtSubTotal').val(totalValue.toFixed(2));	
	$('#frmPurchaseInvoice #txtTotal').val(RoundNumber(grandTotal,2).toFixed(2));
	$('#frmPurchaseInvoice #txtTotalAmount').val(grandTotal.toFixed(2));
	$('#frmPurchaseInvoice #txtSvatTotal').val(totalSvat.toFixed(2));
	$('#frmPurchaseInvoice #txtLedgerTotAmonut').val(RoundNumber(totLedAmount,2).toFixed(2));
}
function clearAll(type)
{
	$("#frmPurchaseInvoice #tblMain tr:gt(0)").remove();
	//$('#cboLedgerAccount').val('');
	$('#frmPurchaseInvoice #cboInvoiceType').val('');
	$('#frmPurchaseInvoice #txtRemarks').html('');
	$('#frmPurchaseInvoice #cboCurrency').val('');
	$('#frmPurchaseInvoice .clsPONo').html('&nbsp;');
	$('#frmPurchaseInvoice .clsTerms').html('&nbsp;');
	$('#frmPurchaseInvoice .clsGRNNo').html('&nbsp;');
	$('#frmPurchaseInvoice #txtRemarks').val('');
	$('#frmPurchaseInvoice #txtSubTotal').val(0.00);
	$('#frmPurchaseInvoice #txtTaxTotal').val(0.00);
	$('#frmPurchaseInvoice #txtTotal').val(0.00);
	$('#frmPurchaseInvoice #txtTotalAmount').val(0.00);
	$('#frmPurchaseInvoice .clsChkAll').prop('checked',false);
	
	$('#frmPurchaseInvoice #txtLedgerTotAmonut').val(0.00);
	$('#frmPurchaseInvoice #tblLedger .clsDel').each(function(){
		
		var delRowCount = parseInt(document.getElementById('tblLedger').rows.length);
		if(delRowCount>4)
			$(this).parent().parent().remove();
		else
		{
			$('#frmPurchaseInvoice #tblLedger tbody tr:last').addClass('cls_tr_firstRow');
			$('#frmPurchaseInvoice #tblLedger tbody tr:last').removeAttr('id');
			$('#frmPurchaseInvoice #tblLedger tbody tr:last').find('.clsLedgerAc').parent().removeAttr('id');
			$(this).parent().parent().find('.clsLedgerAc').val('');
			$(this).parent().parent().find('.clsLedgerAmount').val('');
		}
	});
	
	if(type=='Invoice')
	{
		$('#frmPurchaseInvoice #cboSupplier').html('');		
	}
}
function cancleInvoice()
{
	var purInvNo 	= $('#frmPurchaseInvoice #txtPurchaseInvNo').val();
	var purInvYear 	= $('#frmPurchaseInvoice #txtPurchaseInvYear').val();
    var supplier 	= $('#frmPurchaseInvoice #cboSupplier').val();
	
	if(purInvNo=='')
	{
		$('#frmPurchaseInvoice #butCancle').validationEngine('showPrompt','No purchase invoice No to Cancel.','fail');
		return;
	}
	var val = $.prompt('Are you sure you want to Cancle "'+purInvNo+'/'+purInvYear+'" ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = basePath+"purchaseInvoice-db-set.php?requestType=cancleInvoice&purchaseInvNo="+purInvNo+"&purchaseInvYear="+purInvYear+"&supplier="+supplier;
					var obj = $.ajax({
						url:url,
						dataType: 'json',
						type:'POST',  
						data:'',
						async:false,
						success:function(json){
								$('#frmPurchaseInvoice #butCancle').validationEngine('showPrompt', json.msg,json.type );
								if(json.type=='pass')
								{
									hideWaiting();
									var t=setTimeout("alertx4()",2000);
									var t=setTimeout("window.location.reload();",2000);
									return;
								}
								else
								{
									hideWaiting();
								}
							},
						error:function(xhr,status){
								
								$('#frmPurchaseInvoice #butCancle').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								hideWaiting();
								return;
							}		
						});
					
					}
				
			}});
}
function loadExchangeRate()
{
	if($('#frmPurchaseInvoice #cboCurrency').val()=='')
	{
		return;
	}
	var url 	= basePath+"purchaseInvoice-db-get.php?requestType=loadExchangeRate";
	var data 	= "currencyId="+$('#cboCurrency').val()+"&date="+$(this).val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmPurchaseInvoice #txtCurrencyRate').val(json.exchgRate);
			}
	});	
}
function LoadAutoData(invoiceNo)
{
	$('#frmPurchaseInvoice #txtInvoiceNo').val(invoiceNo);
	$('#frmPurchaseInvoice #btnSearch').click();
}
function reloadPage()
{
	document.location.href = "?q="+menuId;
}
function alertx()
{
	$('#frmPurchaseInvoice #txtInvoiceNo').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmPurchaseInvoice #cboSupplier').validationEngine('hide')	;
}
function alertx3()
{
	$('#frmPurchaseInvoice #butSave').validationEngine('hide')	;
}
function alertx4()
{
	$('#frmPurchaseInvoice #butCancle').validationEngine('hide')	;
}


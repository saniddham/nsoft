<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$userId 			= $_SESSION['userId'];

$POReportId			= 934;
$GRNReportId		= 895;

//include  		"dataAccess/Connector.php";

include  		"class/finance/cls_get_gldetails.php";
include_once 	"class/masterData/exchange_rate/cls_exchange_rate_get.php";
include_once  	"class/cls_commonFunctions_get.php";
include_once  	"class/cls_commonErrorHandeling_get.php";

$obj_get_GLCombo	= new Cls_Get_GLDetails($db);
$obj_exchgRate_get	= new cls_exchange_rate_get($db);
$obj_common			= new cls_commonFunctions_get($db);
$obj_commonErr		= new cls_commonErrorHandeling_get($db);

$invoiceNoAL        = (!isset($_REQUEST['invoiceNoAL'])?'':$_REQUEST['invoiceNoAL']);

$PurInvoiceNo 		= (!isset($_REQUEST['purInvoiceNo'])?'':$_REQUEST['purInvoiceNo']);
$PurInvoiceYear 	= (!isset($_REQUEST['purInvoiceYear'])?'':$_REQUEST['purInvoiceYear']);
$programCode		= 'P0676';

if($PurInvoiceNo!='' && $PurInvoiceYear!='')
{
	$sql = "SELECT 	INVOICE_NO, 
			PURCHASE_DATE, 
			SUPPLIER_ID,  
			INVOICE_TYPE, 
			REMARKS, 
			CURRENCY_ID, 
			STATUS
			FROM 
			finance_supplier_purchaseinvoice_header 
			WHERE PURCHASE_INVOICE_NO= '$PurInvoiceNo' AND
			PURCHASE_INVOICE_YEAR = '$PurInvoiceYear' ";
	
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$invoiceNo 		= $row['INVOICE_NO'];
		$purDate 		= $row['PURCHASE_DATE'];
		$supplierId 	= $row['SUPPLIER_ID'];
		$invoiceType 	= $row['INVOICE_TYPE'];
		$remarks 		= $row['REMARKS'];
		$currencyId 	= $row['CURRENCY_ID'];
		$status 		= $row['STATUS'];	
		
		$row			= $obj_exchgRate_get->GetAllValues($currencyId,4,$purDate,$companyId,'RunQuery');
		$exchgRate		= $row['AVERAGE_RATE'];
		
		$resultPO		= getPODetails($invoiceNo,$supplierId,$companyId);
		$rowPO			= mysqli_fetch_array($resultPO);
		$PONo 			= $rowPO['intPONo'];
		$POYear 		= $rowPO['intPOYear'];
		$term			= $rowPO['strDescription'];
		$GRNNo			= $rowPO['intGrnNo'];
		$GRNYear 		= $rowPO['intGrnYear'];	
	}
}
$status					= (!isset($status)?'':$status);
$purDate				= (!isset($purDate)?'':$purDate);

$dateChangeMode 		= $obj_common->ValidateSpecialPermission('10',$userId,'RunQuery');

$permition_arr			= $obj_commonErr->get_permision_withApproval_save($status,0,$userId,$programCode,'RunQuery');
$permision_save			= $permition_arr['permision']; // check approve permission

$permition_arr			= $obj_commonErr->get_permision_withApproval_cancel($status,0,$userId,$programCode,'RunQuery');
$permision_cancel		= $permition_arr['permision']; // check cancel permission
			
?>
<title>Purchase Invoice</title>
<?php
//include 		"include/javascript.html";
?>
<!--<script type="text/javascript" src="presentation/finance_new/supplier/purchaseInvoice/purchaseInvoice-js.js"></script>-->

<style>
#apDiv1 {
	position: absolute;
	left: 100px;
	top: 117px;
	width: auto;
	height: auto;
	z-index: 1;
}
</style>

<script>
	var invoiceNoAL		='<?php echo $invoiceNoAL; ?>';
</script>

<?php
if($status==-2)//pending
{
?>
	<div id="apDiv1" style="display:none"><img src="images/cancelled.png" /></div>
<?php
}

?>
<form id="frmPurchaseInvoice" name="frmPurchaseInvoice"  method="post">
<div align="center">
<div class="trans_layoutL" style="width:1100px">
<div class="trans_text">Purchase Invoice/ Bill</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
              <td class="normalfnt">Purchase Invoice No</td>
              <td class="normalfnt"><input name="txtPurchaseInvNo" type="text" disabled="disabled" id="txtPurchaseInvNo" style="width:60px" value="<?php echo $PurInvoiceNo; ?>" />&nbsp;<input name="txtPurchaseInvYear" type="text" disabled="disabled" id="txtPurchaseInvYear" style="width:35px" value="<?php echo $PurInvoiceYear; ?>" /></td>
              <td class="normalfnt">Date</td>
              <td class="normalfnt"><input name="txtDate" type="text" value="<?php echo($purDate==''?date("Y-m-d"):$purDate); ?>" class="validate[required]" id="txtDate" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($dateChangeMode!=1?'disabled="disabled"':''); ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
            </tr>
            <tr>
              <td width="18%" class="normalfnt">Invoice No <span class="compulsoryRed">*</span></td>
              <td width="38%" class="normalfnt"><div style="float:left"><input name="txtInvoiceNo"  type="text" class="validate[required]" id="txtInvoiceNo" style="width:180px;" value="<?php echo (!isset($invoiceNo)?'':$invoiceNo); ?>" /></div><div style="float:left"><a class="button green small" id="btnSearch">Search</a></div></td>
              <td width="15%" class="normalfnt">Supplier <span class="compulsoryRed">*</span></td>
              <td width="29%" class="normalfnt"><select name="cboSupplier" id="cboSupplier"  style="width:220px" class="validate[required]" >
              <?php
				if($PurInvoiceNo!='' && $PurInvoiceYear!='')
				{
					$sql = "SELECT distinct PO.intSupplier,MS.strName
							FROM ware_grnheader GH
							INNER JOIN mst_locations ML ON ML.intId=GH.intCompanyId
							INNER JOIN trn_poheader PO ON GH.intPoNo=PO.intPONo AND GH.intPoYear=PO.intPOYear
							INNER JOIN mst_supplier MS ON PO.intSupplier=MS.intId
							WHERE GH.intStatus=1 AND
							ML.intCompanyId='$companyId' AND
							GH.strInvoiceNo='$invoiceNo'
							ORDER BY MS.strName ";
					$result = $db->RunQuery($sql);
					echo "<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						if($row['intSupplier']==$supplierId)
							echo "<option value=\"".$row['intSupplier']."\" selected=\"selected\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intSupplier']."\" >".$row['strName']."</option>"; 
					}
				}
			  ?>          
              </select></td>
              </tr>
            <tr>
              <td class="normalfnt" valign="top">Remark</td>
              <td rowspan="3" class="normalfnt" valign="top"><textarea name="txtRemarks" id="txtRemarks" style="width:240px" rows="3"><?php echo (!isset($remarks)?'':$remarks); ?></textarea></td>
              <td class="normalfnt">Invoice Type</td>
              <td class="normalfnt"><select name="cboInvoiceType" id="cboInvoiceType"  style="width:220px" disabled="disabled" >
               <option value=""></option>
              <?php
				$sql = " SELECT intId,strInvoiceType FROM mst_invoicetype WHERE intStatus=1 ";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($row['intId']==$invoiceType)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strInvoiceType']."</option>";
					else
						echo "<option value=\"".$row['intId']."\">".$row['strInvoiceType']."</option>";
				}
			  ?>
              </select></td>
            </tr>
            <tr>
              <td class="normalfnt" valign="top">&nbsp;</td>
              <td class="normalfnt" valign="top">Currency <span class="compulsoryRed">*</span></td>
              <td class="normalfnt" valign="top"><select name="cboCurrency" id="cboCurrency"  style="width:100px" disabled="disabled" class="validate[required]" >
              <option value=""></option>
              <?php
				$sql = "SELECT intId,strCode FROM mst_financecurrency WHERE intStatus=1 ";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($row['intId']==$currencyId)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";
					else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
				}
			  ?>
              </select></td>
            </tr>
            <tr>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt" valign="top">Rate</td>
              <td class="normalfnt" valign="top"><input type="text" name="txtCurrencyRate" id="txtCurrencyRate" style="width:100px" disabled="disabled" value="<?php echo($PurInvoiceNo==''?'0.00':$exchgRate); ?>" /></td>
              
            </tr>
            <tr>
              <td colspan="4" class="normalfnt"><table width="100%" border="0" cellpadding="0" cellspacing="2" class="tableBorder_allRound" >
                <tr>
                  <td height="25"><table width="100%" border="0" >
                    <tr>
                      <td width="100" class="normalfnt">PO No</td>
                      <td width="10" class="normalfnt" style="text-align:left">:</td>
                      <td width="160" class="normalfnt clsPONo"><?php echo ($PurInvoiceNo!=''?'<a href="?q='.$POReportId.'&poNo='.$PONo.'&year='.$POYear.'" target="_blank">'.$PONo.'/'.$POYear.'</a>':'&nbsp;'); ?></td>
                      <td width="100" class="normalfnt">Terms</td>
                      <td width="10" class="normalfnt" style="text-align:left">:</td>
                      <td width="160" class="normalfnt clsTerms"><?php echo ($PurInvoiceNo!=''?$term:'&nbsp;'); ?></td>
                      <td width="100" class="normalfnt">GRN No</td>
                      <td width="10" class="normalfnt" style="text-align:left">:</td>
                      <td width="160" class="normalfnt clsGRNNo"><?php echo ($PurInvoiceNo!=''?'<a href="?q='.$GRNReportId.'&grnNo='.$GRNNo.'&year='.$GRNYear.'" target="_blank">'.$GRNNo.'/'.$GRNYear.'</a>':'&nbsp;'); ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
            </tr>
            <tr>
              <td colspan="4" class="normalfnt"><div style="overflow:scroll;overflow-x:hidden;width:1100px;height:250px;" id="divGrid">
                <table width="100%" class="bordered" id="tblMain" >
                  <tr>
                    <th width="3%" height="30" ><input class="clsChkAll" type="checkbox" id="chkAll" <?php echo($PurInvoiceNo!=''?'checked="checked"':''); ?>/></th>
                    <th width="10%" >Main Category</th>
                    <th width="10%" >Sub Category</th>
                    <th width="8%" >Item Code </th>
                    <th width="16%" >Item</th>
                    <th width="4%" >UOM</th>
                    <th width="7%">Unit Price</th>
                    <th width="6%">Qty <span class="compulsoryRed">*</span></th>
                    <th width="8%" >Discount %</th>
                    <th width="10%" >Amount</th>
                    <th width="9%" >Tax</th>
                    <th width="9%" >Cost Center <span class="compulsoryRed">*</span></th>
                    </tr>
                   <?php
				   $totAmount 		= 0;
				   $totTaxAmount 	= 0;
				   if($PurInvoiceNo!='' && $PurInvoiceYear!='')
				   {   
						$sql = "SELECT SPID.ITEM_ID,
								MI.strName AS itemName,
								MI.strCode, 
								MC.strName AS mainCategory,
								SC.strName AS subCategory,
								ROUND(SPID.QTY,2) as QTY, 
								ROUND(SPID.UNIT_PRICE,4) as UNIT_PRICE,
								ROUND(QTY*UNIT_PRICE,2) as TOT_AMOUNT,
								MU.strCode AS unit, 
								SPID.DISCOUNT, 
								SPID.TAX_AMOUNT, 
								SPID.TAX_CODE, 
								SPID.COST_CENTER,
								FCOAS.CHART_OF_ACCOUNT_ID
								FROM finance_supplier_purchaseinvoice_details SPID
								INNER JOIN mst_item MI ON SPID.ITEM_ID = MI.intId
								INNER JOIN mst_maincategory MC ON MI.intMainCategory = MC.intId
								INNER JOIN mst_subcategory SC ON MI.intSubCategory = SC.intId
								INNER JOIN mst_units MU ON MU.intId = MI.intUOM 
								INNER JOIN finance_mst_chartofaccount_subcategory FCOAS ON FCOAS.SUBCATEGORY_ID=MI.intSubCategory
								WHERE SPID.PURCHASE_INVOICE_NO='$PurInvoiceNo' AND
								SPID.PURCHASE_INVOICE_YEAR='$PurInvoiceYear' ";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							$amount 		= $row['TOT_AMOUNT'];
							$totAmount 		= $totAmount+$amount;
							$totTaxAmount 	= $totTaxAmount+$row['TAX_AMOUNT'];
						?>
                        <tr class="normalfnt">
                        <td id="<?php echo $row['ITEM_ID']; ?>" style="text-align:center"><input type="checkbox" id="chkItem" name="chkItem" class="clsChkItem validate[minCheckbox[1]]" checked="checked"></td>
                        <td class="clsMainCat" style="text-align:left"><?php echo $row['mainCategory']; ?></td>
                        <td class="clsSubCat" style="text-align:left"><?php echo $row['subCategory']; ?></td>
                        <td class="clsItemCode" style="text-align:left"><?php echo $row['strCode']; ?></td>
                        <td id="<?php echo $row['CHART_OF_ACCOUNT_ID']; ?>" class="clsItem" style="text-align:left"><?php echo $row['itemName']; ?></td>
                        <td class="clsUOM" style="text-align:left"><?php echo $row['unit']; ?></td>
                        <td class="clsUnitPrice" style="text-align:right"><?php echo $row['UNIT_PRICE']; ?></td>
                        <td id="<?php echo $row['QTY']; ?>" style="text-align:center"><input type="textbox" id="txtQty" class="clsQty validate[custom[number]]" style="width:100%;text-align:right" value="<?php echo $row['QTY']; ?>"></td>
                        <td style="text-align:center"><input type="textbox" id="txtDiscount" class="clsDiscount validate[custom[number]]" style="width:100%;text-align:right" value="<?php echo $row['DISCOUNT']; ?>" disabled="disabled"></td>
                        <td style="text-align:center"><input type="textbox" id="txtAmount" class="clsAmount" style="width:100%;text-align:right" disabled="disabled" value="<?php echo number_format($amount, 2, '.', ''); ?>"><input type="hidden" id="txtTaxAmount" class="clsTaxAmount" value="<?php echo $row['TAX_AMOUNT']; ?>" ></td>
                        <td style="text-align:center"><select name="cboTax" id="cboTax" class="clsTax" style="width:100%" disabled="disabled">
                        <option value=""></option>
                        <?php
						$sqlTax = "SELECT intId,strCode
									FROM mst_financetaxgroup
									WHERE intStatus=1";
						$resultTax = $db->RunQuery($sqlTax);
						while($rowTax=mysqli_fetch_array($resultTax))
						{
							if($rowTax['intId']==$row['TAX_CODE'])
								echo "<option value=\"".$rowTax['intId']."\" selected=\"selected\">".$rowTax['strCode']."</option>";
							else
								echo "<option value=\"".$rowTax['intId']."\">".$rowTax['strCode']."</option>";
						}
						?>
                        </select></td>
                        <td style="text-align:center"><select name="cboCostCenter" id="cboCostCenter" class="clsCostCener" style="width:100%">
                        <option value=""></option>
                        <?php
						$sqlCC = "SELECT intId,strName
									FROM mst_financedimension
									WHERE intStatus=1";
						$resultCC = $db->RunQuery($sqlCC);
						while($rowCC=mysqli_fetch_array($resultCC))
						{
							if($rowCC['intId']==$row['COST_CENTER'])
								echo "<option value=\"".$rowCC['intId']."\" selected=\"selected\">".$rowCC['strName']."</option>";
							else
								echo "<option value=\"".$rowCC['intId']."\">".$rowCC['strName']."</option>";
						}
						?>
                        </select></td>
                        <?php	
						}
				   }
				   ?>
                  </table>
                </div></td>
            </tr>
            <tr>
              <td colspan="2" rowspan="5" class="normalfnt" valign="top">
                <table style="width:450px" class="bordered" id="tblLedger" >
                  <thead>
                    <tr>
                      <th colspan="3" >GL Allocation
                        <div style="float:right"><a class="button white small" id="butInsertRow">Add New GL</a></div></th>
                      </tr>
                    <tr>
                      <th width="6%" >Del</th>
                      <th width="69%" >GL Account <span class="compulsoryRed">*</span></th>
                      <th width="25%" >Amount <span class="compulsoryRed">*</span></th>
                      </tr>
                    </thead>
                  <tbody>
                    <?php
					$totLedAccAmount = 0;
					if($PurInvoiceNo!='' && $PurInvoiceYear!='')
					{
						$sqlM = "SELECT GLACCOUNT_ID, 
								ROUND(AMOUNT,2) as AMOUNT,
								(
								SELECT GROUP_CONCAT(COAT.TAX_ID)
								FROM finance_mst_chartofaccount_tax AS COAT
								WHERE COAT.CHART_OF_ACCOUNT_ID=finance_supplier_purchaseinvoice_gl.GLACCOUNT_ID
								) AS TAX_ID
								FROM 
								finance_supplier_purchaseinvoice_gl 
								WHERE PURCHASE_INVOICE_NO='$PurInvoiceNo' AND
								PURCHASE_INVOICE_YEAR='$PurInvoiceYear' ";
						
						$resultM = $db->RunQuery($sqlM);
						while($rowM=mysqli_fetch_array($resultM))
						{
							if($rowM['TAX_ID']=='')
								$glType	= 'subCat';
							else
								$glType	= 'tax';
								
							$totLedAccAmount+=$rowM['AMOUNT'];
						?>
                    <tr id="<?php echo $glType; ?>">
                      <td style="text-align:center" ><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>
                      <td style="text-align:center" id="<?php echo $rowM['TAX_ID']; ?>" ><select name="cboLedgerAc" id="cboLedgerAc" class="clsLedgerAc validate[required]"  style="width:100%" >
                        <?php
                      		echo $obj_get_GLCombo->getGLCombo('SUPPLIER_INVOICE',$rowM['GLACCOUNT_ID']);
                      ?>
                        </select></td>
                      <td style="text-align:center"><input name="txtLedgerAmonut" id="txtLedgerAmonut" class="clsLedgerAmount validate[required]" type="text" style="width:100%;text-align:right" value="<?php echo $rowM['AMOUNT']; ?>"/></td>
                      </tr>
                    <?php	
						}
					}
					?>
                    <?php
					if($PurInvoiceNo=='' && $PurInvoiceYear=='')
					{
					?>
                    <tr>
                      <td style="text-align:center" ><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>
                      <td style="text-align:center"><select name="cboLedgerAc" id="cboLedgerAc" class="clsLedgerAc validate[required]"  style="width:100%" >
                        <?php
                    	echo $obj_get_GLCombo->getGLCombo('SUPPLIER_INVOICE','');
                    ?>
                        </select></td>
                      <td style="text-align:center" ><input name="txtLedgerAmonut" id="txtLedgerAmonut" class="clsLedgerAmount validate[required]" type="text" style="width:100%;text-align:right"/></td>
                      </tr>
                    <?php
					}
					?>
                    </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="2" align="left">&nbsp;</td>
                      <td style="text-align:center">
                        <input name="txtLedgerTotAmonut" id="txtLedgerTotAmonut" class="clsLedgerTotAmount" type="text" style="width:100%;text-align:right" disabled="disabled" value="<?php echo number_format($totLedAccAmount, 2, '.', ''); ?>"/></td>
                      </tr>
                    </tfoot>
                  </table>
                </td>
              <td class="normalfnt" style="text-align:right">&nbsp;</td>
              <td class="normalfnt" ><table width="100%" border="0">
                <tr>
                  <td width="39%" class="normalfnt" style="text-align:right">Sub Total</td>
                  <td width="8%" class="normalfnt" style="text-align:right">:</td>
                  <td width="20%" class="normalfnt" style="text-align:right">&nbsp;</td>
                  <td width="33%" class="normalfnt"><span class="normalfnt" style="text-align:right">
                    <input name="txtSubTotal" type="text" disabled="disabled" id="txtSubTotal" style="width:100px;text-align:right" value="<?php echo number_format($totAmount, 2, '.', ''); ?>" />
                    </span></td>
                  </tr>
                <tr>
                  <td class="normalfnt" style="text-align:right">Tax</td>
                  <td class="normalfnt" style="text-align:right">:</td>
                  <td class="normalfnt" style="text-align:right">&nbsp;</td>
                  <td class="normalfnt"><span class="normalfnt" style="text-align:right">
                    <input name="txtTaxTotal" type="text" disabled="disabled" id="txtTaxTotal" style="width:100px;text-align:right" value="<?php echo number_format($totTaxAmount, 2, '.', ''); ?>" />
                  </span></td>
                  </tr>
                <tr>
                  <td class="normalfnt" style="text-align:right">Total</td>
                  <td class="normalfnt" style="text-align:right">:</td>
                  <td class="normalfnt" style="text-align:right">&nbsp;</td>
                  <td class="normalfnt"><span class="normalfnt" style="text-align:right">
                    <input name="txtTotal" type="text" disabled="disabled" id="txtTotal" style="width:100px;text-align:right" value="<?php echo number_format(($totAmount+$totTaxAmount), 2, '.', ''); ?>" />
                  </span></td>
                  </tr>
                </table></td>
            </tr>
            <tr>
            	<td class="normalfnt">&nbsp;</td>
                <td class="normalfnt" style="text-align:right"><input type="hidden" id="txtTotalAmount" value="<?php echo $totAmount+$totTaxAmount; ?>" /> </td>
           	  </tr>
            <tr>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt" style="text-align:right"><input type="hidden" id="txtSvatTotal" value="" /></td>
            </tr>
            <tr>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
            </tr>
            <tr>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
            </tr>
            </table>
        </td>
    </tr>
    
    <tr>
    	<td height="32" >
    		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td align="center"><a class="button white medium" id="butNew">New</a><a class="button white medium" id="butSave" <?php if($permision_save!=1 || ($PurInvoiceNo!='' && $PurInvoiceYear!='')){ ?>style="display:none"<?php } ?>>Save</a><a class="button white medium" id="butCancel" <?php if($permision_cancel!=1){ ?>  style="display:none"<?php } ?>>Cancel</a><a href="main.php" class="button white medium" id="butClose">Close</a></td><br />          
              </tr>
            </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>
<?php
function getPODetails($invoiceNo,$supplierId,$companyId)
{
	global $db;
	$sql = "SELECT DISTINCT PO.intPONo,PO.intPOYear,GH.intGrnNo,GH.intGrnYear,FPT.strDescription
			FROM ware_grnheader GH
			INNER JOIN mst_locations ML ON ML.intId=GH.intCompanyId
			INNER JOIN trn_poheader PO ON GH.intPoNo=PO.intPONo AND GH.intPoYear=PO.intPOYear
			INNER JOIN mst_financepaymentsterms FPT ON FPT.intId=PO.intPaymentTerm
			WHERE GH.intStatus=1 AND
			ML.intCompanyId='$companyId' AND
			GH.strInvoiceNo='$invoiceNo' AND
			PO.intSupplier='$supplierId' ";
	
	return $db->RunQuery($sql);
}
function loadDateChangeMode($programCode,$userId)
{
	global $db;
	
	$Mode	= 0;
	$sql = "SELECT MS.intStatus
			FROM menus M
			INNER JOIN menus_special MS ON M.intId=MS.intMenuId
			INNER JOIN menus_special_permision SP ON MS.intId=SP.intSpMenuId
			WHERE M.strCode='$programCode' AND
			SP.intUser='$userId' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	if($row['intStatus']==1)
	{
		$Mode = 1;
	}
	return $Mode;
}
function loadCancleMode($programCode,$userId)
{
	global $db;
	$Mode	= 0;
	$sql = "SELECT
			menupermision.intCancel 
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId = '$userId' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	if($row['intCancel']==1)
	{
		$Mode = 1;
	}
	return $Mode;
}
function loadSaveeMode($programCode,$userId)
{
	global $db;
	$Mode	= 0;
	$sql = "SELECT
			menupermision.intAdd 
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId = '$userId' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	if($row['intAdd']==1)
	{
		$Mode = 1;
	}
	return $Mode;
}
?>
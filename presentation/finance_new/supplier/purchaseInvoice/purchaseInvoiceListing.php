<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId			= $_SESSION["headCompanyId"];
$MenuId				= 676;
$reportId			= 893;

require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'SPIH.STATUS',
				'purInvoiceNo'=>"CONCAT(SPIH.PURCHASE_INVOICE_NO,' / ',SPIH.PURCHASE_INVOICE_YEAR)",
				'INVOICE_NO'=>'SPIH.INVOICE_NO',
				'SUPPLIER_INVOICE_DATE'=>'GH.SUPPLIER_INVOICE_DATE',
				'supplier'=>'MS.strName',
				'intGrnYear'=>'GH.intGrnYear',
				'PURCHASE_DATE'=>'SPIH.PURCHASE_DATE',
				'strCode'=>'FC.strCode',
				'REMARKS'=>'SPIH.REMARKS'
				);
				
$arr_status = array('Saved'=>'1','Cancelled'=>'-2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND SPIH.PURCHASE_DATE = '".date('Y-m-d')."'";
	
// }

$sql = "SELECT SUB_1.* FROM
		(
		SELECT if(SPIH.STATUS=-2,'Cancelled','Saved') as Status,
		SPIH.PURCHASE_INVOICE_NO,
		SPIH.PURCHASE_INVOICE_YEAR,
		CONCAT(SPIH.PURCHASE_INVOICE_NO,' / ',SPIH.PURCHASE_INVOICE_YEAR) AS purInvoiceNo,
		SPIH.INVOICE_NO,
		GH.SUPPLIER_INVOICE_DATE,
		SPIH.PURCHASE_DATE,
		SPIH.REMARKS,
		GH.intGrnNo,
		GH.intGrnYear,
		CONCAT(GH.intGrnNo,' / ',GH.intGrnYear) AS conCatGRNNo,
		PH.intPONo,
		PH.intPOYear,
		CONCAT(GH.intPONo,' / ',GH.intPOYear) AS conCatPONo,
		FC.strCode,
		MS.strName AS supplier,
		'View' AS VIEW,
		(SELECT ROUND(SUM((QTY*UNIT_PRICE)+TAX_AMOUNT),2) FROM finance_supplier_purchaseinvoice_details SPID
		WHERE SPID.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
		SPID.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR) AS totAmount
		FROM finance_supplier_purchaseinvoice_header SPIH
		INNER JOIN ware_grnheader GH ON GH.strInvoiceNo=SPIH.INVOICE_NO
		INNER JOIN trn_poheader PH ON GH.intPoNo=PH.intPONo AND GH.intPoYear=PH.intPOYear AND SPIH.SUPPLIER_ID=PH.intSupplier
		INNER JOIN mst_supplier MS ON MS.intId=SPIH.SUPPLIER_ID
		LEFT JOIN mst_financecurrency FC ON FC.intId=SPIH.CURRENCY_ID
		WHERE GH.intStatus = 1 AND 
		SPIH.COMPANY_ID='$companyId'
		$where_string
		)  
		AS SUB_1 WHERE 1=1";

$sql2 = "SELECT SUB_1.* FROM
		(
		SELECT if(SPIH.STATUS=-2,'Cancelled','Saved') as Status,
		SPIH.PURCHASE_INVOICE_NO,
		SPIH.PURCHASE_INVOICE_YEAR,
		CONCAT(SPIH.PURCHASE_INVOICE_NO,' / ',SPIH.PURCHASE_INVOICE_YEAR) AS purInvoiceNo,
		SPIH.INVOICE_NO,
		GH.SUPPLIER_INVOICE_DATE,
		SPIH.PURCHASE_DATE,
		SPIH.REMARKS,
		GH.intGrnNo,
		GH.intGrnYear,
		CONCAT(GH.intGrnNo,' / ',GH.intGrnYear) AS conCatGRNNo,
		PH.intPONo,
		PH.intPOYear,
		CONCAT(GH.intPONo,' / ',GH.intPOYear) AS conCatPONo,
		FC.strCode,
		MS.strName AS supplier,
		'View' AS VIEW,
		(SELECT ROUND(SUM((QTY*UNIT_PRICE)+TAX_AMOUNT),2) FROM finance_supplier_purchaseinvoice_details SPID
		WHERE SPID.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
		SPID.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR) AS totAmount
		FROM finance_supplier_purchaseinvoice_header SPIH
		INNER JOIN ware_grnheader GH ON GH.strInvoiceNo=SPIH.INVOICE_NO
		INNER JOIN trn_poheader PH ON GH.intPoNo=PH.intPONo AND GH.intPoYear=PH.intPOYear AND SPIH.SUPPLIER_ID=PH.intSupplier
		INNER JOIN mst_supplier MS ON MS.intId=SPIH.SUPPLIER_ID
		LEFT JOIN mst_financecurrency FC ON FC.intId=SPIH.CURRENCY_ID
		WHERE GH.intStatus = 1 AND 
		SPIH.COMPANY_ID='$companyId'
		)  
		AS SUB_1 WHERE 1=1";

$jq 	= new jqgrid('',$db);	
$col 	= array();
$cols 	= array();

$col["title"] 			= "PurchaseInvoice No";
$col["name"] 			= "PURCHASE_INVOICE_NO";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "PurchaseInvoice Year";
$col["name"] 			= "PURCHASE_INVOICE_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Status"; 
$col["name"] 			= "Status";
$col["width"] 			= "3";
$col["stype"] 			= "select";
$str 					= ":All;Saved:Saved;Cancelled:Cancelled" ;
$col["editoptions"] 	=  array("value"=> $str);
$col["align"] 			= "center";
$cols[] 				= $col;
$col					= NULL;

$col["title"] 			= "Inv Serial No";
$col["name"] 			= "purInvoiceNo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 
$col['link']			= '?q='.$MenuId.'&purInvoiceNo={PURCHASE_INVOICE_NO}&purInvoiceYear={PURCHASE_INVOICE_YEAR}';
$col["linkoptions"] 	= "target=new";					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Invoice No";
$col["name"] 			= "INVOICE_NO";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Invoice Date";
$col["name"] 			= "SUPPLIER_INVOICE_DATE";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;
$col["title"] 			= "Supplier";
$col["name"] 			= "supplier";
$col["width"] 			= "6";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "PO No";
$col["name"] 			= "intPONo";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "PO Year";
$col["name"] 			= "intPOYear";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "GRN No";
$col["name"] 			= "intGrnNo";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "GRN Year";
$col["name"] 			= "intGrnYear";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Date";
$col["name"] 			= "PURCHASE_DATE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "strCode";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Remarks";
$col["name"] 			= "REMARKS";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Amount";
$col["name"] 			= "totAmount";
$col["width"] 			= "3";
$col["align"] 			= "right";
$col["sortable"] 		= true; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "View";
$col["name"] 			= "VIEW";
$col["width"] 			= "2";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= '?q='.$reportId.'&purInvoiceNo={PURCHASE_INVOICE_NO}&purInvoiceYear={PURCHASE_INVOICE_YEAR}';
$col["linkoptions"] 	= "target=new";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Purchase Invoice Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'PURCHASE_INVOICE_YEAR,PURCHASE_INVOICE_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test", "range"=>"filtered");

$jq->set_options($grid);
$jq->select_command 	= $sql;
$jq->select_command2 	= $sql2;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Purchase Invoice Listing</title>
<?php
echo $out;

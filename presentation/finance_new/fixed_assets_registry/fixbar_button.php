<?php
session_start();
$mainPath = $_SESSION['mainPath'];
?>
<style type="text/css">
  #site-bottom-bar {
	  background-color: #F0F0F0 ;
	  border-top: 1px solid #CCCCCC ;
	  bottom: 0px ;
	  font-family: verdana, arial ;
	  font-size: 11px ;
	  height: 40px ;
	  position: fixed ;
	  width: 100% ;
	  z-index: 1 ;
	}

  #site-bottom-bar-frame {
	  height: 30px ;
	  margin: 0px 10px 0px 10px ;
	  position: relative ;
	  }

  #site-bottom-bar-content {
	  padding: 3px 0px 0px 0px ;
	  }

  #menu-root {
	  background-color: #E8E8E8 ;
	  border: 1px solid #D0D0D0 ;
	  color: #000000 ;
	  display: block ;
	  height: 22px ;
	  line-height: 22px ;
	  text-align: center ;
	  text-decoration: none ;
	  width: 105px ;
	  }

  #menu-root:hover {
	  background-color: #666666 ;
	  border-color: #000000 ;
	  color: #FFFFFF ;
	  }

  #menu {
	  background-color: #E8E8E8 ;
	  border: 1px solid #666666 ;
	  bottom: 32px ;
	  display: none ;
	  left: 0px ;
	  padding: 5px 5px 1px 5px ;
	  position: absolute ;
	  width: 200px ;
	  }

  #menu a {
	  background-color: #E8E8E8 ;
	  border: 1px solid #FFFFFF ;
	  color: #000000 ;
	  display: block ;
	  margin-bottom: 4px ;
	  padding: 5px 0px 5px 5px ;
	  text-decoration: none ;
	  }

  #menu a:hover {
	  background-color: #666666 ;
	  border-color: #000000 ;
	  color: #FFFFFF ;
	  }
</style>
<script type="text/javascript">

	jQuery(function( $ ){
	  var menuRoot = $( "#menu-root" );
	  var menu = $( "#menu" );

	  // Hook up menu root click event.
	  menuRoot
		  .attr( "href", "javascript:void( 0 )" )
		  .click(
			  function(){
				  // Toggle the menu display.
				  menu.toggle();

				  // Blur the link to remove focus.
				  menuRoot.blur();

				  // Cancel event (and its bubbling).
				  return( false );
			  }
		  )
	  ;

	  // Hook up a click handler on the document so that
	  // we can hide the menu if it is not the target of
	  // the mouse click.
	  $( document ).click(
		  function( event ){
			  // Check to see if this came from the menu.
			  if (
				  menu.is( ":visible" ) &&
				  !$( event.target ).closest( "#menu" ).size()
				  ){

				  // The click came outside the menu, so
				  // close the menu.
				  menu.hide();

			  }
		  }
	  );
  });
</script>
   
  <div id="site-bottom-bar" class="fixed-position">
  <div id="site-bottom-bar-frame">
  <div id="site-bottom-bar-content">    
	<tr>
    <td height="32" >
        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            <tr>
                <td align="center" class="tableBorder_allRound"><a class="button white medium" id="butNew">New</a><a class="button white medium" id="butSave" <?php if($saveMode!=1){ ?>style="display:none"<?php } ?>>Save</a><a style="display:none" class="button white medium" id="butReport">Report</a><a href="main.php" class="button white medium" id="butClose">Close</a>
                </td>
            </tr>
        </table>
     </td>
     </tr>
  </div>
  </div>
  </div>

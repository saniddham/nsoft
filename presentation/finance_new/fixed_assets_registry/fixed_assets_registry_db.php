<?php
session_start();
$backwardseperator 		= "../../../";
$locationId				= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$session_userId 		= $_SESSION['userId'];

$requestType 			= $_REQUEST['requestType'];
$programCode			= 'P0771';

include_once  "../../../dataAccess/Connector.php";
include_once  "../../../class/finance/fixedAssets/fixedAssetsRegistry/cls_registry_set.php";
include_once  "../../../class/finance/fixedAssets/fixedAssetsRegistry/cls_registry_get.php";
include_once  "../../../class/cls_commonFunctions_get.php";

$obj_registry_set		= new Cls_Registry_Set($db);
$obj_registry_get		= new Cls_Registry_Get($db);
$obj_common				= new cls_commonFunctions_get($db);

$savedStatus			= true;
$savedMasseged			= '';
$error_sql				= '';

if($requestType=='deleteItem')
{
	$rowId			= $_REQUEST['rowId'];
	
	$db->begin();
	
	validateBeforDelete($programCode,$session_userId);
	
	$dataArr		= $obj_registry_set->deleteItem($rowId,'RunQuery2');
	if($dataArr['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus		= false;
		$savedMasseged 		= $dataArr['savedMasseged'];
		$error_sql 			= $dataArr['error_sql'];
	}

	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='loadSubDepartment')
{
	$department			= $_REQUEST['department'];
	$result				= $obj_registry_get->loadSubDepartment($department);
	$html				= "<option value=\"\"></option>";
	
	while($row=mysqli_fetch_array($result))
	{
		$html.= "<option value=\"".$row['SUB_DEPT_ID']."\" >".$row['NAME']."</option>";
	}
	$response['html']	= $html;
	echo json_encode($response);
	
}
else if($requestType=='saveData')
{
	$arrDetails = json_decode($_REQUEST['arrDetails'],true);
	
	$db->begin();
	
	foreach($arrDetails as $array_loop)
	{
		$rowId				= $array_loop["rowId"];
		$supplier			= ($array_loop["supplier"]==''?'null':$array_loop["supplier"]);
		$itemId				= ($array_loop["itemId"]==''?0:$array_loop["itemId"]);
		$serialNo			= $obj_common->replace($array_loop["serialNo"]);
		$assetsCode			= $obj_common->replace($array_loop["assetsCode"]);
		$warrantyMonth		= ($array_loop["warrantyMonth"]==''?'null':$array_loop["warrantyMonth"]);
		$warrantyEndDate	= ($array_loop["warrantyEndDate"]==''?'null':"'".$array_loop["warrantyEndDate"]."'");
		$location			= ($array_loop["location"]==''?'null':$array_loop["location"]);
		$department			= ($array_loop["department"]==''?'null':$array_loop["department"]);
		$subDepartment		= ($array_loop["subDepartment"]==''?'null':$array_loop["subDepartment"]);
		$itemPrice			= ($array_loop["itemPrice"]==''?0:$array_loop["itemPrice"]);
		$depreciateRate		= ($array_loop["depreciateRate"]==''?0:$array_loop["depreciateRate"]);
		$grnDate			= "'".date('Y-m-d')."'";

		if($rowId==0)
		{
			$dataArr	= $obj_registry_set->SaveDetails(0,0,$supplier,$itemId,$serialNo,'null',$grnDate,$itemPrice,$manualFlag=1,'RunQuery2',$assetsCode,$warrantyMonth,$warrantyEndDate,$location,$department,$subDepartment,$depreciateRate,$status=1);
			
			if($dataArr['savedStatus']=='fail' && ($savedStatus))
			{
				$savedStatus	= false;
				$savedMasseged 	= $dataArr['savedMassege'];
				$error_sql 		= $dataArr['error_sql'];
			}
		}
		else
		{
			$dataArr	= $obj_registry_set->updateRegistryDetails($rowId,$supplier,$itemId,$serialNo,$grnDate,$assetsCode,$warrantyMonth,$warrantyEndDate,$location,$department,$subDepartment,$itemPrice,$depreciateRate,'RunQuery2');
			if($dataArr['savedStatus']=='fail' && ($savedStatus))
			{
				$savedStatus	= false;
				$savedMasseged 	= $dataArr['savedMassege'];
				$error_sql 		= $dataArr['error_sql'];
			}
		}		
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Saved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
}
function validateBeforDelete($programCode,$session_userId)
{
	global $savedStatus;
	global $savedMasseged;
	global $obj_common;
	
	$deleteMode	= $obj_common->Load_menupermision2($programCode,$session_userId,'intDelete');
	if($deleteMode==0 && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged 	= 'No permission to Delete Data.';
	}	
}
?>
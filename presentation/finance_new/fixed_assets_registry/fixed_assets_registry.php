<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId				= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$session_userId 		= $_SESSION['userId'];

include_once  "class/finance/fixedAssets/fixedAssetsRegistry/cls_registry_get.php";
include_once  "class/cls_commonFunctions_get.php";
//include 	  "include/javascript.html";

$obj_registry_get		= new Cls_Registry_Get($db);
$obj_common				= new cls_commonFunctions_get($db);

$programCode			= 'P0771';

$GRNNoArr				= explode('/',$_REQUEST['cboGRNNo']);
$GRNNo					= $GRNNoArr[0];
$GRNYear				= $GRNNoArr[1];
$supplier				= (!isset($_REQUEST['cboSearchSupplier'])?'':$_REQUEST['cboSearchSupplier']);
$item					= (!isset($_REQUEST['cboSearchItem'])?'':$_REQUEST['cboSearchItem']);
$serialNo				= (!isset($_REQUEST['txtSearchSerialNo'])?'':$_REQUEST['txtSearchSerialNo']);
$assetCode				= (!isset($_REQUEST['txtSearchAssetCode'])?'':$_REQUEST['txtSearchAssetCode']);
$location				= (!isset($_REQUEST['cboSearchLocation'])?'':$_REQUEST['cboSearchLocation']);
$search_status			= (!isset($_REQUEST['cboSearchStatus'])?'':$_REQUEST['cboSearchStatus']);

if(!isset($_REQUEST["cboSearchStatus"]))
	$search_status		= 1;

$detail_result			= $obj_registry_get->loadDetailData($GRNNo,$GRNYear,$supplier,$item,$serialNo,$assetCode,$location,$department='',$subDepartment='',$search_status,'RunQuery');
$saveMode 				= $obj_common->Load_menupermision($programCode,$session_userId,'intAdd');
$deleteMode 			= $obj_common->Load_menupermision($programCode,$session_userId,'intDelete');

?>
<title>Fixed Assets Registry</title>

<!--<script type="text/javascript" src="presentation/finance_new/fixed_assets_registry/fixed_assets_registry.js"></script>-->

<style>
.clsChangeRow {
	background-color:#C1FFC1;
}
.clsNotChangeRow {
	background-color:#FFFFFF;
}
</style>

<form id="frmFixedAssetsRegistry" name="frmFixedAssetsRegistry" method="post">
  <div align="center">
    <div class="trans_layoutXL" >
      <div class="trans_text">Fixed Assets Registry</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" align="center" bgcolor="#FFFFFF">
              <tr>
                <td width="8%" class="normalfnt">GRN No</td>
                <td width="13%" valign="top" class="normalfnt"><select name="cboGRNNo" id="cboGRNNo"  style="width:120px">
                  <option value=""></option>
                  <?php
								$GRNNo_result = $obj_registry_get->loadGRNNo();
                            	while($row=mysqli_fetch_array($GRNNo_result))
								{
									if($row['GRN_NO']==$GRNNo && $row['GRN_YEAR']==$GRNYear )
										echo "<option value=\"".$row['GRN_NO'].'/'.$row['GRN_YEAR']."\" selected=\"selected\">".$row['GRN_NO'].'/'.$row['GRN_YEAR']."</option>";
									else
										echo "<option value=\"".$row['GRN_NO'].'/'.$row['GRN_YEAR']."\">".$row['GRN_NO'].'/'.$row['GRN_YEAR']."</option>";
								}
                            ?>
                </select></td>
                <td width="7%" valign="top" class="normalfnt">Supplier</td>
                <td width="23%" class="normalfnt"><select name="cboSearchSupplier" id="cboSearchSupplier"  style="width:250px">
                            <option value="" ></option>
							<?php
								$search_supplier_result = $obj_registry_get->loadSupplier();
                            	while($row = mysqli_fetch_array($search_supplier_result))
								{
									if($row['intId']==$supplier)
										echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
									else
										echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                            ?>
                            </select></td>
                <td width="6%" class="normalfnt">Item</td>
                <td width="23%" class="normalfnt"><select name="cboSearchItem" id="cboSearchItem"  style="width:250px">
                            <option value=""></option>
							<?php
								$search_item_result = $obj_registry_get->loadItem();
                            	while($row = mysqli_fetch_array($search_item_result))
								{
									if($row['intId']==$item)
										echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
									else
										echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                            ?>
                            </select></td>
                <td width="7%" class="normalfnt">Serial No</td>
                <td width="13%" class="normalfnt"><input type="text" name="txtSearchSerialNo" id="txtSearchSerialNo" style="width:100%" value="<?php echo $serialNo; ?>" /></td>
              </tr>
              <tr>
                <td class="normalfnt">Asset Code</td>
                <td valign="top" class="normalfnt"><input type="text" name="txtSearchAssetCode" id="txtSearchAssetCode" style="width:120px" value="<?php echo $assetCode; ?>" /></td>
                <td valign="top" class="normalfnt">Location</td>
                <td class="normalfnt"><select name="cboSearchLocation" id="cboSearchLocation"  style="width:250px">
                            <option value=""></option>
							<?php
								$search_location_result = $obj_common->loadLocation($companyId,'RunQuery');
                            	while($row = mysqli_fetch_array($search_location_result))
								{
									if($row['intId']==$location)
										echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
									else
										echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                            ?>
                            </select></td>
                <td class="normalfnt">Status</td>
                <td class="normalfnt"><select name="cboSearchStatus" id="cboSearchStatus"  style="width:120px">
                  <option value="0" <?php echo($search_status==0?'selected="selected"':'') ?>>Pending</option>
                  <option value="1" <?php echo($search_status==1?'selected="selected"':'') ?>>Confirmed</option>
                  <option value="-2" <?php echo($search_status==-2?'selected="selected"':'') ?>>Deleted</option>
                </select></td>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt" style="text-align:right"><a class="button green small" id="butSearch">Search</a></td>
              </tr>
              <tr>
                <td colspan="8" class="normalfnt"><table width="100%" class="bordered" id="tblMain" >
                    <thead>
                        <tr>
                        	<th colspan="15">Fixed Assets Registry
                        	<div style="float:right"><a class="button white small" id="butInsertRow">Add New Row</a></div></th>
                        </tr>
                        <tr>
                            <th width="1%" >Del</th>
                            <th width="11%" >GRN No</th>
                            <th width="9%"  >Supplier</th>
                            <th width="9%"  >Item Description</th>
                            <th width="9%"  >Serial No</th>
                            <th width="9%"  >Asset Code</th>
                            <th width="9%"  >Warranty Month</th>
                            <th width="11%"  >Warranty End Date</th>
                            <th width="9%"  >Location</th>
                            <th width="9%"  >Department</th>
                            <th width="9%"  >Sub Department</th>
                            <th width="11%"  >Item Price</th>
                            <th width="7%"  >Dep. Rate</th>
                            <th width="9%"  >Accumulate Depreciate Amount</th>
                            <th width="9%"  >Net Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
					$count	= 0;
					
					while($row=mysqli_fetch_array($detail_result))
					{
						$disable		= '';
						$disableMode	= 0;
						
						if($row['ACCUMILATE_DEPRECIATE_AMOUNT']>0 || $search_status==-2 || $search_status==0)
						{
							$disable	 	= 'disabled="disabled"';
							$disableMode	= 1;
						}
					?>
                    	<tr class="<?php echo($count==0?'cls_tr_firstRow clsNotChangeRow':'clsNotChangeRow'); ?>" id="<?php echo $row['ID']; ?>">
                            <td width="1%" style="text-align:center" title="Delete" class="clsDelete"><?php echo(($deleteMode==1 && $disableMode==0)?'<img src="images/del.png" alt="delete" class="removeRow mouseover" />':'&nbsp;'); ?></td>
                            <td width="11%" class="clsGRNNo" style="text-align:center" title="GRN No"><?php echo $row['GRN_NO'].'/'.$row['GRN_YEAR']; ?></td>
                            <td width="9%" title="Supplier"><select name="cboSupplier" id="cboSupplier"  style="width:100%" class="clsSupplier" <?php echo $disable; ?> >
                            <option value="" ></option>
							<?php
								$supplier_result = $obj_registry_get->loadSupplier();
                            	while($rowS=mysqli_fetch_array($supplier_result))
								{
									if($rowS['intId']==$row['SUPPLIER_ID'])
										echo "<option value=\"".$rowS['intId']."\" selected=\"selected\">".$rowS['strName']."</option>";
									else
										echo "<option value=\"".$rowS['intId']."\">".$rowS['strName']."</option>";
								}
                            ?>
                            </select></td>
                            <td width="9%" title="Item"><select name="cboItem" id="cboItem"  style="width:100%" class="clsItem" <?php echo $disable; ?> >
                            <option value=""></option>
							<?php
								$item_result = $obj_registry_get->loadItem();
                            	while($rowI=mysqli_fetch_array($item_result))
								{
									if($rowI['intId']==$row['ITEM_ID'])
										echo "<option value=\"".$rowI['intId']."\" selected=\"selected\">".$rowI['strName']."</option>";
									else
										echo "<option value=\"".$rowI['intId']."\">".$rowI['strName']."</option>";
								}
                            ?>
                            </select></td>
                            <td width="9%" style="text-align:center" title="Serial No"><input type="text" id="txtSerialNo" name="txtSerialNo" style="width:100%" class="clsSerialNo" value="<?php echo $row['SERIAL_NO']; ?>" <?php echo $disable; ?> /></td>
                            <td width="9%" style="text-align:right" title="Asset Code"><input type="text" id="txtAssetCode" name="txtAssetCode" style="width:100%" class="clsAssetCode" value="<?php echo $row['ASSET_CODE']; ?>" <?php echo $disable; ?> /></td>
                            <td width="9%" style="text-align:right" title="Warranty Month"><input type="text" id="txtWarrantyMonth" name="txtWarrantyMonth" style="width:100%" class="clsWarrantyMonth" value="<?php echo $row['WARRANTY_MONTHS']; ?>" <?php echo $disable; ?> /></td>
                            <td width="9%" style="text-align:left" title="Warranty End Date"><input name="txtEndDate" type="text" value="<?php echo $row['WARRANTY_END_DATE']; ?>" class="clsWarrantyEndDate" id="txtEndDate~<?php echo $count; ?>" style="width:90px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo $disable; ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                            <td width="9%" style="text-align:right" title="Location"><select name="cboLocation" id="cboLocation"  style="width:100%" class="clsLocation" <?php echo $disable; ?> >
                            <option value=""></option>
							<?php
								$location_result = $obj_common->loadLocation($companyId,'RunQuery');
                            	while($rowL=mysqli_fetch_array($location_result))
								{
									if($rowL['intId']==$row['LOCATION_ID'])
										echo "<option value=\"".$rowL['intId']."\" selected=\"selected\">".$rowL['strName']."</option>";
									else
										echo "<option value=\"".$rowL['intId']."\">".$rowL['strName']."</option>";
								}
                            ?>
                            </select></td>
                            <td width="9%" style="text-align:center" title="Department"><select name="cboDepartment" id="cboDepartment"  style="width:100%" class="clsDepartment" <?php echo $disable; ?> >
                            <option value=""></option>
							<?php
								$department_result = $obj_common->loadDepartment('RunQuery');
                            	while($rowD=mysqli_fetch_array($department_result))
								{
									if($rowD['intId']==$row['DEPARTMENT_ID'])
										echo "<option value=\"".$rowD['intId']."\" selected=\"selected\">".$rowD['strName']."</option>";
									else
										echo "<option value=\"".$rowD['intId']."\">".$rowD['strName']."</option>";
								}
                            ?>
                            </select></td>
                            <td width="9%" style="text-align:center" title="Sub Department"><select name="cboSubDepartment" id="cboSubDepartment"  style="width:100%" class="clsSubDepartment" <?php echo $disable; ?> >
                            <option value=""></option>
							<?php
								$subDepartment_result	= $obj_registry_get->loadSubDepartment($row['DEPARTMENT_ID']);
                            	while($rowSub=mysqli_fetch_array($subDepartment_result))
								{
									if($rowSub['SUB_DEPT_ID']==$row['SUB_DEPARTMENT_ID'])
										echo "<option value=\"".$rowSub['SUB_DEPT_ID']."\" selected=\"selected\">".$rowSub['NAME']."</option>";
									else
										echo "<option value=\"".$rowSub['SUB_DEPT_ID']."\">".$rowSub['NAME']."</option>";
								}
                            ?>
                            </select></td>
                            <td width="11%" style="text-align:center" title="Item Price"><input type="text" id="txtItemPrice" name="txtItemPrice" style="width:70px;text-align:right" class="clsItemPrice validate[custom[number]]" value="<?php echo round($row['ITEM_PRICE'],2); ?>" <?php echo $disable; ?> /></td>
                            <td width="7%" style="text-align:center" title="Depreciate Rate"><input type="text" id="txtDepreciateRate" name="txtDepreciateRate" style="width:100%;text-align:right" class="clsDepreciateRate validate[custom[number]]" value="<?php echo $row['DEPRECIATE_RATE']; ?>" <?php echo $disable; ?>/></td>
                            <td width="9%" class="clsADAmount" style="text-align:right" title="Accumulate Depreciate Amount"><?php echo($row['ACCUMILATE_DEPRECIATE_AMOUNT']==''?'&nbsp;':number_format($row['ACCUMILATE_DEPRECIATE_AMOUNT'],2)); ?></td>
                            <td width="9%" style="text-align:right" class="clsNetAmount" title="Net Amount"><?php echo($row['NET_AMOUNT']==''?'&nbsp;':number_format($row['NET_AMOUNT'],2)); ?></td>
                      </tr>
                    <?php
						$count++;	
					}
					if($count==0)
					{
					?>
                    	<tr class="cls_tr_firstRow clsNotChangeRow" id="0">
                            <td style="text-align:center" title="Delete" class="clsDelete"><?php echo($deleteMode==1?'<img src="images/del.png" alt="delete" class="removeRow mouseover" />':'&nbsp;'); ?></td>
                            <td class="clsGRNNo" style="text-align:center" title="GRN No">&nbsp;</td>
                            <td title="Supplier"><select name="cboSupplier" id="cboSupplier"  style="width:100%" class="clsSupplier" >
                            <option value=""></option>
							<?php
								$supplier_result = $obj_registry_get->loadSupplier();
                            	while($rowS=mysqli_fetch_array($supplier_result))
								{
									echo "<option value=\"".$rowS['intId']."\">".$rowS['strName']."</option>";
								}
                            ?>
                            </select></td>
                            <td title="Item"><select name="cboItem" id="cboItem"  style="width:100%" class="clsItem" >
                            <option value=""></option>
							<?php
								$item_result = $obj_registry_get->loadItem();
                            	while($rowI=mysqli_fetch_array($item_result))
								{
									echo "<option value=\"".$rowI['intId']."\">".$rowI['strName']."</option>";
								}
                            ?>
                            </select></td>
                            <td title="Serial No"><input type="text" id="txtSerialNo" name="txtSerialNo" style="width:100%" class="clsSerialNo"/></td>
                            <td title="Asset Code"><input type="text" id="txtAssetCode" name="txtAssetCode" style="width:100%" class="clsAssetCode"/></td>
                            <td title="Warranty Month"><input type="text" id="txtWarrantyMonth" name="txtWarrantyMonth" style="width:100%" class="clsWarrantyMonth"/></td>
                            <td title="Warranty End Date"><input name="txtEndDate" type="text" value="<?php echo $row['WARRANTY_END_DATE']; ?>" class="clsWarrantyEndDate" id="txtEndDate~<?php echo $count; ?>" style="width:90px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                            <td title="Location"><select name="cboLocation" id="cboLocation"  style="width:100%" class="clsLocation" >
                            <option value=""></option>
							<?php
								$location_result = $obj_common->loadLocation($companyId,'RunQuery');
                            	while($rowL=mysqli_fetch_array($location_result))
								{
									echo "<option value=\"".$rowL['intId']."\">".$rowL['strName']."</option>";
								}
                            ?>
                            </select></td>
                            <td title="Department"><select name="cboDepartment" id="cboDepartment"  style="width:100%" class="clsDepartment" >
                            <option value=""></option>
							<?php
								$department_result = $obj_common->loadDepartment('RunQuery');
                            	while($rowD=mysqli_fetch_array($department_result))
								{
									echo "<option value=\"".$rowD['intId']."\">".$rowD['strName']."</option>";
								}
                            ?>
                            </select></td>
                            <td style="text-align:center" title="Sub Department"><select name="cboSubDepartment" id="cboSubDepartment"  style="width:100%" class="clsSubDepartment" >
                            <option value=""></option>
                            </select></td>
                            <td title="Item Price"><input type="text" id="txtItemPrice" name="txtItemPrice" style="width:70px" class="clsItemPrice validate[custom[number]]" /></td>
                            <td title="Depreciate Rate"><input type="text" id="txtDepreciateRate" name="txtDepreciateRate" style="width:100%" class="clsDepreciateRate validate[custom[number]]"/></td>
                            <td style="text-align:center" class="clsADAmount" title="Accumulate Depreciate Amount">&nbsp;</td>
                            <td style="text-align:center" class="clsNetAmount" title="Net Amount">&nbsp;</td>
                      </tr>
                    <?php
					}
					?>
                    </tbody>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </div>
  </div>
</form>
<?php
include "fixbar_button.php";
?>
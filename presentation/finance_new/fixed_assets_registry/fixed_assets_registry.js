// JavaScript Document
var basePath	= "presentation/finance_new/fixed_assets_registry/";
$(document).ready(function(){
	
	$("#frmFixedAssetsRegistry").validationEngine();
	
	/*if(intAddx)
	{
		$('#frmFixedAssetsRegistry #butNew').show();
		$('#frmFixedAssetsRegistry #butSave').show();
	}
	//permision for edit 
	if(intEditx)
	{
		$('#frmFixedAssetsRegistry #butSave').hide();
		$('#frmFixedAssetsRegistry #butSave').show();
	}*/
	
	$('#frmFixedAssetsRegistry #butInsertRow').die('click').live('click',addNewRow);
	$('#frmFixedAssetsRegistry .removeRow').die('click').live('click',deleteRow);
	$('#frmFixedAssetsRegistry .clsDepartment').die('change').live('change',loadSubDepartment);
	$('#frmFixedAssetsRegistry .clsItemPrice').die('keyup').live('keyup',changeNetAmount);
	$('#frmFixedAssetsRegistry #butSave').die('click').live('click',saveData);
	
	$('#frmFixedAssetsRegistry .clsSupplier').die('focus').live('focus',changeTRClass);
	$('#frmFixedAssetsRegistry .clsItem').die('focus').live('focus',changeTRClass);
	$('#frmFixedAssetsRegistry .clsSerialNo').die('focus').live('focus',changeTRClass);
	$('#frmFixedAssetsRegistry .clsAssetCode').die('focus').live('focus',changeTRClass);
	$('#frmFixedAssetsRegistry .clsWarrantyMonth').die('focus').live('focus',changeTRClass);
	$('#frmFixedAssetsRegistry .clsWarrantyEndDate').die('focus').live('focus',changeTRClass);
	$('#frmFixedAssetsRegistry .clsLocation').die('focus').live('focus',changeTRClass);
	$('#frmFixedAssetsRegistry .clsDepartment').die('focus').live('focus',changeTRClass);
	$('#frmFixedAssetsRegistry .clsSubDepartment').die('focus').live('focus',changeTRClass);
	$('#frmFixedAssetsRegistry .clsItemPrice').die('focus').live('focus',changeTRClass);
	$('#frmFixedAssetsRegistry .clsDepreciateRate').die('focus').live('focus',changeTRClass);
	$('#frmFixedAssetsRegistry #butSearch').die('click').live('click',pageSubmit);

});
function addNewRow()
{
	var lastDateIdArr 	= $('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsWarrantyEndDate').attr('id').split('~');
	var lastDateNo		= parseFloat(lastDateIdArr[1]);
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').after("<tr>"+$('#frmFixedAssetsRegistry #tblMain .cls_tr_firstRow').html()+"</tr>");
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').attr('id',0);
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').addClass('clsNotChangeRow');
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsDelete').html('<img src="images/del.png" alt="delete" class="removeRow mouseover" />');
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsGRNNo').html('&nbsp;');
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsSupplier').val('');
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsItem').val(' ');
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsSerialNo').val('');
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsAssetCode').val('');
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsWarrantyMonth').val('');
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsWarrantyEndDate').val('');
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsWarrantyEndDate').attr('id','txtEndDate~'+(lastDateNo+1));
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsLocation').val('');
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsDepartment').val('');
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsSubDepartment').html('<option value=""></option>');
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsItemPrice').val('');
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsDepreciateRate').val('');
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsADAmount').html('&nbsp;');
	$('#frmFixedAssetsRegistry #tblMain tbody tr:last').find('.clsDepreciateRate').html('&nbsp;');
}
function deleteRow()
{
	var rowId		= $(this).parent().parent().attr('id');
	var obj		 	= this;
	if(rowId!=0)
	{
		var val = $.prompt('Are you sure you want to Delete this Item ?',{
					buttons: { Ok: true, Cancel: false },
					callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"fixed_assets_registry_db.php";
					var objHttp = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:"requestType=deleteItem&rowId="+rowId,
						async:false,
						success:function(json){
								if(json.type=='fail')
								{
									$(obj).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
									return;
								}
								else
								{
									var delRowCount = parseInt(document.getElementById('tblMain').rows.length);
									if(delRowCount>3)
										$(obj).parent().parent().remove();
									
									$('#frmFixedAssetsRegistry #tblMain tbody tr').removeClass('cls_tr_firstRow');
									$('#frmFixedAssetsRegistry #tblMain tbody tr:first').addClass('cls_tr_firstRow');
								}
							},
						error:function(xhr,status){
								
								$(obj).validationEngine('showPrompt', errormsg(xhr.status),'fail');
								return;
							}		
						});
					}
					hideWaiting();
				}});
	}
	else
	{
		var delRowCount = parseInt(document.getElementById('tblMain').rows.length);
		if(delRowCount>3)
			$(obj).parent().parent().remove();
		
		$('#frmFixedAssetsRegistry #tblMain tbody tr').removeClass('cls_tr_firstRow');
		$('#frmFixedAssetsRegistry #tblMain tbody tr:first').addClass('cls_tr_firstRow');
	}
}
function loadSubDepartment()
{
	var obj		= this;
	if($(this).val()=='')
	{
		$(this).parent().parent().find('.clsSubDepartment').html('');
		return;
	}
	var url 	= basePath+"fixed_assets_registry_db.php";
	var data 	= "requestType=loadSubDepartment&department="+$(obj).parent().parent().find('.clsDepartment').val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	async:false,
	success:function(json)
			{
				$(obj).parent().parent().find('.clsSubDepartment').html(json.html);
			}
	});	
}
function changeNetAmount()
{
	var itemPrice = ($(this).val()==''?0:$(this).val());
	$(this).parent().parent().find('.clsNetAmount').html(itemPrice);
}
function saveData()
{
	showWaiting();
	if($('#frmFixedAssetsRegistry').validationEngine('validate'))
	{
		var data = "requestType=saveData";
		var chkStatus	= false;
		var arrDetails	= "";
		
		$('#frmFixedAssetsRegistry .clsChangeRow').each(function(){
			
				var rowId			= $(this).attr('id');
				var supplier		= $(this).find('.clsSupplier').val();
				var itemId			= $(this).find('.clsItem').val();
				var serialNo		= $(this).find('.clsSerialNo').val();
				var assetsCode		= $(this).find('.clsAssetCode').val();
				var warrantyMonth	= $(this).find('.clsWarrantyMonth').val();
				var warrantyEndDate	= $(this).find('.clsWarrantyEndDate').val();
				var location		= $(this).find('.clsLocation').val();
				var department		= $(this).find('.clsDepartment').val();
				var subDepartment	= $(this).find('.clsSubDepartment').val();
				var itemPrice		= $(this).find('.clsItemPrice').val();
				var depreciateRate	= $(this).find('.clsDepreciateRate').val();
				
				chkStatus	= true;
				arrDetails += "{";
				arrDetails += '"rowId":"'+ rowId +'",' ;
				arrDetails += '"supplier":"'+ supplier +'",' ;
				arrDetails += '"itemId":"'+ itemId +'",' ;
				arrDetails += '"serialNo":'+URLEncode_json(serialNo)+',';
				arrDetails += '"assetsCode":'+URLEncode_json(assetsCode)+',';
				arrDetails += '"warrantyMonth":"'+warrantyMonth+'",' ;
				arrDetails += '"warrantyEndDate":"'+warrantyEndDate+'",' ;
				arrDetails += '"location":"'+location+'",' ;
				arrDetails += '"department":"'+department+'",' ;
				arrDetails += '"subDepartment":"'+subDepartment+'",' ;
				arrDetails += '"itemPrice":"'+itemPrice+'",' ;
				arrDetails += '"depreciateRate":"'+depreciateRate+'"' ;
				arrDetails += "},";
		});
		if(!chkStatus)
		{
			$(this).validationEngine('showPrompt','No records to save.','fail');
			hideWaiting();	
			return;
		}
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		var arrDetails	= '['+arrDetails+']';
		
		data+="&arrDetails="+arrDetails;
		
		var url = basePath+"fixed_assets_registry_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmFixedAssetsRegistry #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						hideWaiting();
						window.location.reload();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
						$('#frmFixedAssetsRegistry #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
	}
	else
	{
		hideWaiting();
		return;
	}
}
function changeTRClass()
{
	var accDepAmunt = parseFloat($(this).parent().parent().find('.clsADAmount').html());
	if(accDepAmunt>0)
	{
		return;
	}
	$(this).parent().parent().removeClass('clsNotChangeRow');
	$(this).parent().parent().addClass('clsChangeRow');
}
function pageSubmit()
{
	document.getElementById('frmFixedAssetsRegistry').submit();
}
<?php
session_start();
$backwardseperator 		= "../../../";
$session_userId			= $_SESSION["userId"];
$session_companyId		= $_SESSION["headCompanyId"];

include_once "{$backwardseperator}dataAccess/Connector.php";
include_once "../../../class/finance/month_end/cls_month_end_set.php";

$requestType	= $_REQUEST["RequestType"];

$obj_month_end_set	= new cls_month_end_set($db);

if($requestType=="URLProcess")
{
	$month		= $_REQUEST["Month"];
	$year		= $_REQUEST["Year"];
	$accPeriod	= $_REQUEST["APeriod"];
	
	$db->begin();

	$result1 = $obj_month_end_set->Process($month,$year,$accPeriod,$session_companyId);
	if($result1['BooSaved'])
		$result1 = $obj_month_end_set->ApprovedBy($month,$year,$accPeriod,$session_companyId,$session_userId,1,0);

	if($result1['BooSaved'])
	{
		$db->commit();
		$responce['Flag']	= 'pass';
		$responce['Msg']	= 'Proceed successfully.';
	}
	else
	{
		$db->rollback();
		$responce['Flag']	= 'fail';
		$responce['Msg']	= $result1['ErrorMsg'];
		$responce['Sql']	= $result1['ErrorSql'];
	}
	echo json_encode($responce);
}

if($requestType=="URLRevise")
{
	$month		= $_REQUEST["Month"];
	$year		= $_REQUEST["Year"];
	$accPeriod	= $_REQUEST["APeriod"];
	
	$db->begin();

	$result1 = $obj_month_end_set->Revise($month,$year,$accPeriod,$session_userId);
	if($result1['BooSaved'])
		$result1 = $obj_month_end_set->ApprovedBy($month,$year,$accPeriod,$session_companyId,$session_userId,0,1);

	if($result1['BooSaved'])
	{
		$db->commit();
		$responce['Flag']	= 'pass';
		$responce['Msg']	= 'Proceed successfully.';
	}
	else
	{
		$db->rollback();
		$responce['Flag']	= 'fail';
		$responce['Msg']	= $result1['ErrorMsg'];
		$responce['Sql']	= $result1['ErrorSql'];
	}
	echo json_encode($responce);
}
?>
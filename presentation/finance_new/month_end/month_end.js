var basePath	= "presentation/finance_new/month_end/";
$(document).ready(function(e) {
    $('#frmMonthEnd .cls_submit').die('change').live('change',PageSubmit);
	$('#frmMonthEnd .cls_a_process').die('click').live('click',Process);
	$('#frmMonthEnd .cls_a_revise').die('click').live('click',Revise);
});

function PageSubmit()
{
	$('#frmMonthEnd').submit();
}

function Process()
{
	var obj 			= this;
	
	if(!confirm("Are you sure you want to Process and Lock the selected Year / Month : "+$(obj).parent().parent().find('.cls_td_month').html()+"."))
		return;		
	
	var month_array 	= $(this).parent().parent().find('.cls_td_month').attr('id').split('-');
	var accPeriod 		= $('#frmMonthEnd #cboAccountPeriod').val().split('|');
	
	var url 	= basePath+"month_end_db.php?RequestType=URLProcess";
	var data 	= "Month="+month_array[1];
		data   += "&Year="+month_array[0];
		data   += "&APeriod="+accPeriod[0];
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		if(json.Flag=='pass')
		{
			PageSubmit();
		}
		else
		{
			$(obj).validationEngine('showPrompt',json.Msg,json.Flag);
		}
	}
	});		 
}

function Revise()
{
	var obj 			= this;
	
	if(!confirm("Are you sure you want to Revise and Unlock the selected Year / Month : "+$(obj).parent().parent().find('.cls_td_month').html()+"."))
		return;	
		
	var month_array 	= $(this).parent().parent().find('.cls_td_month').attr('id').split('-');
	var accPeriod 		= $('#frmMonthEnd #cboAccountPeriod').val().split('|');
	
	var url 	= basePath+"month_end_db.php?RequestType=URLRevise";
	var data 	= "Month="+month_array[1];
		data   += "&Year="+month_array[0];
		data   += "&APeriod="+accPeriod[0];
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		if(json.Flag=='pass')
		{
			PageSubmit();
		}
		else
		{
			$(obj).validationEngine('showPrompt',json.Flag,json.Flag);
		}
	}
	});		 
}
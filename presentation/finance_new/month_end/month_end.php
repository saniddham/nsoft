<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$cboAccountPeriod	= $_REQUEST["cboAccountPeriod"];
$ex_array			= explode('|',$cboAccountPeriod);

$startDate = strtotime($ex_array[1]);
$endDate   = strtotime($ex_array[2]);
//echo($ex_array[0]);
$currentDate = $endDate;
$i	= 0;
$array	= array();
if($cboAccountPeriod!=""){
while ($currentDate >= $startDate) {
	$array[$i][0]	= date('Y | F',$currentDate);
	$array[$i][1]	= date('Y-m',$currentDate);
    $currentDate = strtotime( date('Y/m/01/',$currentDate).' -1 day');
	$i++;
	
}
krsort($array);
}
?>
<title>Finance Month End Process</title>

<!--<script type="text/javascript" src="presentation/finance_new/month_end/month_end.js"></script>--> 
 
<form id="frmMonthEnd" name="frmMonthEnd" method="post">
  <div align="center">
    <div class="trans_layoutS" >
      <div class="trans_text">Finance Month End Process</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" align="center" bgcolor="#FFFFFF">
              <tr class="normalfnt">
                <td width="32%" class="normalfnt">Accounting Period</td>
                <td width="46%" class="normalfnt"><select id="cboAccountPeriod" name="cboAccountPeriod" style="width:220px" class="cls_submit">
                  <option value=""></option>
                  <?php
				$sql = "SELECT 
							CONCAT(intId,'|',dtmStartingDate,'|',dtmClosingDate) 	AS ID,
							CONCAT(dtmStartingDate,' | ',dtmClosingDate) 			AS VALUE
						FROM mst_financeaccountingperiod
						ORDER BY intId desc";
				$result = $db->RunQuery($sql);
				while($row = mysqli_fetch_array($result))
				{
					if($cboAccountPeriod == $row["ID"])
						echo "<option value=\"".$row["ID"]."\" selected=\"selected\">".$row["VALUE"]."</option>";
					else
						echo "<option value=\"".$row["ID"]."\">".$row["VALUE"]."</option>";
				}
				
				?>
                </select></td>
                <td width="22%" class="normalfnt">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="3"><table width="100%" border="0" class="bordered" id="tblMain">
                    <thead>
                      <tr>
                        <th colspan="4">Month End Process</th>
                      </tr>
                      <tr>
                        <th width="32%">Month</th>
                        <th width="46%">Process</th>
                        <th width="22%">Revise</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
					foreach ($array as $key => $val) 
					{
						$row = GetProceed($val[1],$ex_array[0]);
						
						if(IsLastProcess($val[1],$ex_array[0]))
							$revise_html	= "<a class=\"button red small cls_a_revise\">Revise</a>";
						else
							$revise_html	= "&nbsp;";
						
						if($row['COUNT']>0)
						{
							$html = $row['PROCESS_BY'].' | '.$row['PROCESS_DATE'];
						}
						elseif(!$booFlag)
						{
							$booFlag	= true;
							$html = "<a class=\"button green small cls_a_process\">Process</a>";
						}
						else
							$html = "&nbsp;";
					?>
                      <tr>
                        <td class="cls_td_month" nowrap="nowrap" style="text-align:left" id="<?php echo $val[1];?>"><?php echo $val[0];?></td>
                        <td class="cls_td_prcess" style="text-align:center"><?php echo $html?></td>
                        <td class="cls_td_revise" style="text-align:center" ><?php echo $revise_html?></td>
                      </tr>
                      <?php
					   $currentDate = strtotime( date('Y/m/01/',$currentDate).' -1 month');
					}
			 ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="32" >&nbsp;</td>
        </tr>
      </table>
    </div>
  </div>
</form>
<?php
function GetProceed($val,$accountPeriod)
{
	global $db;
	$val_array	= explode('-',$val);
	$sql = "SELECT 
				COUNT(*)				AS COUNT,
				U.strFullName 			AS PROCESS_BY,
				FMEPA.PROCESS_DATE		AS PROCESS_DATE
			FROM finance_month_end_process_approveby FMEPA
			INNER JOIN finance_month_end_process FMEP
				ON FMEP.PERIOD_ID = FMEPA.PERIOD_ID
				AND FMEP.YEAR = FMEPA.YEAR
				AND FMEP.MONTH = FMEPA.MONTH
			INNER JOIN sys_users U ON U.intUserId = FMEPA.PROCESS_BY
			WHERE 
				FMEP.PERIOD_ID = $accountPeriod
				AND FMEPA.YEAR = ".$val_array[0]." 
				AND FMEPA.MONTH = ".$val_array[1]."";
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	return $row;
}

function IsLastProcess($val,$accountPeriod)
{
	global $db;
	$val_array	= explode('-',$val);
	
	$sql 	= "SELECT * FROM finance_month_end_process WHERE PERIOD_ID = $accountPeriod ORDER BY YEAR DESC,MONTH DESC LIMIT 1  ";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	
	if($row["YEAR"]==$val_array[0] && $row["MONTH"]==$val_array[1])	
		return true;
	else
		return false;
	
}
?>
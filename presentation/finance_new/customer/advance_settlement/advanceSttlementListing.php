<?php
date_default_timezone_set('Asia/Kolkata');
session_start();

$backwardseperator 	= "../../../../";
$companyId			= $_SESSION["headCompanyId"];

require_once "../../../../dataAccess/Connector.php";
require_once("../../../../libraries/jqgrid2/inc/jqgrid_dist.php");

$sql = "SELECT SUB_1.* FROM
		(
			SELECT 	SSH.SETTLE_NO,
			SSH.SETTLE_YEAR,
			CONCAT(SSH.SETTLE_NO,' / ',SSH.SETTLE_YEAR) AS advanceSettleNo,
			CONCAT(SSH.INVOICE_NO,' / ',SSH.INVOICE_YEAR) AS invoiceNo,
			CONCAT(SSH.ORDER_NO,' / ',SSH.ORDER_YEAR) AS orderNo,
			CONCAT(SSH.ADVANCE_NO,' / ',SSH.ADVANCE_YEAR) AS avanceNo,
			ROUND(SSH.SETTLE_AMOUNT,2) AS Amonut, 
			SSH.SETTLE_DATE,
			SSH.CUSTOMER_ID,
			SSH.CURRENCY_ID,
			FC.strCode AS currency,
			SSH.CHAT_OF_ACCOUNT_ID,
			(
				SELECT CONCAT(FCOA.CHART_OF_ACCOUNT_NAME,' | ',FMT.FINANCE_TYPE_CODE,FMMT.MAIN_TYPE_CODE,FMST.SUB_TYPE_CODE,FCOA.CHART_OF_ACCOUNT_CODE)
				FROM finance_mst_chartofaccount FCOA
				INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
				INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
				INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
				INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
				WHERE FCOA.CHART_OF_ACCOUNT_ID = SSH.CHAT_OF_ACCOUNT_ID
			) AS chartOfAccount,
			MC.strName AS customer,
			'View' AS VIEW
			FROM 
			finance_customer_advance_settle_header AS SSH
			INNER JOIN mst_financecurrency FC ON FC.intId=SSH.CURRENCY_ID
			INNER JOIN mst_customer MC ON MC.intId=SSH.CUSTOMER_ID
			INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=SSH.CHAT_OF_ACCOUNT_ID
			WHERE SSH.COMPANY_ID = '$companyId'
		)  
		AS SUB_1 WHERE 1=1";

$jq = new jqgrid('',$db);	
$col = array();

$col["title"] 			= "AdvanceSettle No";
$col["name"] 			= "SETTLE_NO";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "AdvanceSettle Year";
$col["name"] 			= "SETTLE_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "AdvanceSettle No";
$col["name"] 			= "advanceSettleNo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Invoice No";
$col["name"] 			= "invoiceNo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Order No";
$col["name"] 			= "orderNo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Advance No";
$col["name"] 			= "avanceNo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Customer";
$col["name"] 			= "customer";
$col["width"] 			= "7";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["dbname"] 			= "CUSTOMER_ID";
$supplier_lookup 		= $jq->get_dropdown_values("SELECT DISTINCT 
													intId AS k,
													strName AS v
													FROM mst_customer
													WHERE intStatus = 1
													ORDER BY strName");
$col["stype"] 			= "select";
$str 					= ":;".$supplier_lookup;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "currency";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["dbname"] 			= "CURRENCY_ID";
$currency_lookup 		= $jq->get_dropdown_values("SELECT DISTINCT 
													intId AS k,
													strCode AS v
													FROM mst_financecurrency
													WHERE intStatus = 1
													ORDER BY intId");
$col["stype"] 			= "select";
$str 					= ":;".$currency_lookup;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;
$col					= NULL;

$col["title"] 				= "GL Account";
$col["name"] 				= "chartOfAccount";
$col["width"] 				= "7"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "CHAT_OF_ACCOUNT_ID";
$client_lookup 				= $jq->get_dropdown_values("SELECT DISTINCT 
														FCOA.CHART_OF_ACCOUNT_ID AS k,
														CONCAT(FCOA.CHART_OF_ACCOUNT_NAME,' | ',FMT.FINANCE_TYPE_CODE,FMMT.MAIN_TYPE_CODE,FMST.SUB_TYPE_CODE,FCOA.CHART_OF_ACCOUNT_CODE) AS v
														
														FROM finance_mst_chartofaccount FCOA
														INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
														INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
														INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
														INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
														WHERE FCOAC.COMPANY_ID = '$companyId' AND FCOA.BANK = '1'
														ORDER BY CHART_OF_ACCOUNT_NAME ");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;

$col["title"] 			= "Settle Date";
$col["name"] 			= "SETTLE_DATE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Amount";
$col["name"] 			= "Amonut";
$col["width"] 			= "3";
$col["align"] 			= "right"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "View";
$col["name"] 			= "VIEW";
$col["width"] 			= "1";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= 'rptAddvanceSettlement.php?advSettleNo={SETTLE_NO}&advSettleYear={SETTLE_YEAR}';
$col["linkoptions"] 	= "target='rptAddvanceSettlement.php'";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Advance Settlement Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'SETTLE_YEAR,SETTLE_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Advance Settlement Listing</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>
<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">
<table width="100%" border="0" align="center">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $backwardseperator?>libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $backwardseperator?>libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	

<script src="<?php echo $backwardseperator?>libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $backwardseperator?>libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="<?php echo $backwardseperator?>libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
<script src="<?php echo $backwardseperator?>libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
<script src="<?php echo $backwardseperator?>libraries/javascript/script.js" type="text/javascript"></script>
   <td><table width="100%" border="0">
      <tr>
        <td>
        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
        </td>
      </tr>
    </table></td>
    </tr>
</form>
</body>
</html>
<?php 
session_start();
//BEGIN - INCLUDE FILES {
include  "../../../../dataAccess/Connector.php";
//include  "../../../../class/finance/customer/advance/cls_advance_get.php";
include  "../../../../class/finance/customer/advance/cls_advance_set.php";
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {
//$obj_advance_get	= new Cls_Advance_Get($db);
$obj_advance_set	= new Cls_Advance_Set($db);
//END 	- CREATE OBJECTS }
$requestType		= $_REQUEST["RequestType"];

if($requestType == "URLLoadAdvanceDetails")
{
	$advanceNo_array		= explode('/',$_REQUEST["AdvanceNo"]);
	$advanceNo				= $advanceNo_array[0];
	$advanceYear			= $advanceNo_array[1];
	
	$data["ADVANCE_GL_HTML"]	= GetAdvanceGLHTML($advanceNo,$advanceYear);
	
	
	$data["ADVANCE_AMOUNT"]	= '0';
	$data["SETTLE_BALANCE"]	= '0';
		
	$sql = "SELECT
	          COALESCE(SUM(AMOUNT),0) 				AS ADVANCE_AMOUNT,
			  COALESCE(SUM(SETTLE_BALANCE),0) 		AS SETTLE_BALANCE
			FROM finance_customer_advance_header FCAH
			INNER JOIN finance_customer_advance_details FCAD ON FCAD.ADVANCE_NO = FCAH.ADVANCE_NO AND FCAD.ADVANCE_YEAR = FCAH.ADVANCE_YEAR
			WHERE FCAD.ADVANCE_NO = '$advanceNo'
				AND FCAD.ADVANCE_YEAR = '$advanceYear'";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$data["ADVANCE_AMOUNT"]	= $row["ADVANCE_AMOUNT"];
		$data["SETTLE_BALANCE"]	= $row["SETTLE_BALANCE"];
	}
	echo json_encode($data);
}
elseif($requestType == "URLLoadAdvanceDetails1")
{
	$advanceNo_array		= explode('/',$_REQUEST["AdvanceNo"]);
	$advanceNo				= $advanceNo_array[0];
	$advanceYear			= $advanceNo_array[1];
	$gl_id					= $_REQUEST["GL_ID"];
	
	$data["ADVANCE_AMOUNT"]	= '0';
	$data["SETTLE_BALANCE"]	= '0';
		
	$sql = "SELECT
	          ROUND(COALESCE(SUM(AMOUNT),0),2) 				AS ADVANCE_AMOUNT,
			  ROUND(COALESCE(SUM(SETTLE_BALANCE),0),2) 		AS SETTLE_BALANCE
			FROM finance_customer_advance_header FCAH
			INNER JOIN finance_customer_advance_details FCAD ON FCAD.ADVANCE_NO = FCAH.ADVANCE_NO AND FCAD.ADVANCE_YEAR = FCAH.ADVANCE_YEAR
			WHERE FCAD.ADVANCE_NO = '$advanceNo'
				AND FCAD.ADVANCE_YEAR = '$advanceYear'
				AND FCAD.ACCOOUNT_NO = $gl_id";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$data["ADVANCE_AMOUNT"]	= $row["ADVANCE_AMOUNT"];
		$data["SETTLE_BALANCE"]	= $row["SETTLE_BALANCE"];
	}
	echo json_encode($data);
}
elseif($requestType == "URLSettle")
{
	$advanceNo_array	= explode('/',$_REQUEST["AdvanceNo"]);
	$invoiceNo_array	= explode('/',$_REQUEST["InvoiceNo"]);
	$orderNo_array		= explode('/',$_REQUEST["OrderNo"]);
	$gl_id				= $_REQUEST["GL_ID"];
	$currencyId			= $_REQUEST["CurrencyId"];
	$customerId			= $_REQUEST["CustomerId"];
	$settleAmount		= $_REQUEST["SettleAmount"];
	
	echo $obj_advance_set->Settle((int)$advanceNo_array[0],(int)$advanceNo_array[1],$invoiceNo_array[0],$invoiceNo_array[1],$orderNo_array[0],$orderNo_array[1],$gl_id,$currencyId,$customerId,$settleAmount);
	
}

function GetAdvanceGLHTML($advanceNo,$advanceYear)
{
	global $db;
	
		$sql = "SELECT
					FCAD.ACCOOUNT_NO 						AS GL_ID,
				CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE,' - ',FCOA.CHART_OF_ACCOUNT_NAME)		AS GL_NAME
			FROM finance_customer_advance_header AS FCAH
			  INNER JOIN finance_customer_advance_details AS FCAD ON FCAD.ADVANCE_NO = FCAH.ADVANCE_NO AND FCAH.ADVANCE_YEAR = FCAD.ADVANCE_YEAR
			  INNER JOIN finance_mst_chartofaccount FCOA ON FCOA.CHART_OF_ACCOUNT_ID = FCAD.ACCOOUNT_NO
			  INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
			  INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
			  INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
			WHERE FCAD.ADVANCE_NO = $advanceNo
			AND FCAD.ADVANCE_YEAR = $advanceYear
			AND SETTLE_BALANCE >0";
	$result = $db->RunQuery($sql);
	$string = "<select style=\"width:100%\" class=\"cls_cbo_advanceGL\">";
	$string .= "<option value=\"".""."\">&nbsp;</option>";
	while($row = mysqli_fetch_array($result))
	{
		$string .= "<option value=\"".$row["GL_ID"]."\">".$row["GL_NAME"]."</option>";
	}
	$string .= "</select>";
	$response["HTML"]	= $string;
	return $response["HTML"];
}
?>
var basePath	= "presentation/finance_new/customer/advance_settlement/";
var menuId		= 722;
$(document).ready(function(e) {
	
    $('#frmAdvanceSettlement .cls_submit').die('change').live('change',SubmitForm);
	$('#frmAdvanceSettlement #butNew').die('click').live('click',New);
	$('#frmAdvanceSettlement .cls_cbo_advanceNo').die('change').live('change',LoadAdvanceDetails);
	$('#frmAdvanceSettlement .cls_cbo_advanceGL').die('change').live('change',LoadAdvanceDetails1);
	$('#frmAdvanceSettlement #tblMain .cls_settle').die('click').live('click',Settle);
	
	$('#frmAdvanceSettlement #tblMain input').die('keyup').live('keyup',function(e){
			if($(this).val()=="" || isNaN($(this).val())){
				$(this).val(0);
				$(this).select();
			}
	});
	
	$('#frmAdvanceSettlement #tblMain .cls_txt_settleAmount').die('keyup').live('keyup',function(){
		var invoiceAmount 	= parseFloat($(this).parent().parent().find('.cls_td_invoiceValue').html());
		var settleAmount 	= parseFloat($(this).val());
		var balanceAmount 	= parseFloat($(this).parent().parent().find('.cls_td_balanceSettleAmount').html());
		
		if(balanceAmount<settleAmount)
			$(this).val(balanceAmount);
			
		if(invoiceAmount<settleAmount)
			$(this).val(invoiceAmount);
	});
});

function SubmitForm()
{
	window.frmAdvanceSettlement.submit();
}

function New()
{
	window.location.href ='?q='+menuId;
}

function LoadAdvanceDetails()
{
	showWaiting();
	var obj 	= $(this);
	var url 	= basePath+"advance_settlement_db.php?RequestType=URLLoadAdvanceDetails";
	var data 	= "AdvanceNo="+$(this).val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	async:false,
	success:function(json)
	{
		obj.parent().parent().find('.cls_td_advanceGL').html(json.ADVANCE_GL_HTML);
		//obj.parent().parent().find('.cls_td_advanceAmount').html(json.ADVANCE_AMOUNT);
		//obj.parent().parent().find('.cls_td_balanceSettleAmount').html(json.SETTLE_BALANCE);
		//obj.parent().parent().find('.cls_td_settleAmount').children().val(json.SETTLE_BALANCE);
		hideWaiting();
	}
	});
}

function LoadAdvanceDetails1()
{
	showWaiting();
	var obj 	= $(this);
	var url 	= basePath+"advance_settlement_db.php?RequestType=URLLoadAdvanceDetails1";
	var data 	= "AdvanceNo="+$(this).parent().parent().find('.cls_td_advanceNo').children().val();
		data   += "&GL_ID="+$(this).val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	async:false,
	success:function(json)
	{
		obj.parent().parent().find('.cls_td_advanceAmount').html(json.ADVANCE_AMOUNT);
		obj.parent().parent().find('.cls_td_balanceSettleAmount').html(json.SETTLE_BALANCE);
		obj.parent().parent().find('.cls_td_settleAmount').children().val(json.SETTLE_BALANCE);
		hideWaiting();
	}
	});
}

function Settle()
{
	showWaiting();
	if(!Save_Validation(this)){
		hideWaiting();
		return;
	}
		
	var obj		= $(this);
	var url 	= basePath+"advance_settlement_db.php?RequestType=URLSettle";
	var data 	= "InvoiceNo="+$(this).parent().parent().find('.cls_td_invoiceNo').attr('id');
		data   += "&OrderNo="+$(this).parent().parent().find('.cls_td_orderNo').html();
		data   += "&AdvanceNo="+$(this).parent().parent().find('.cls_td_advanceNo').children().val();
		data   += "&GL_ID="+$(this).parent().parent().find('.cls_td_advanceGL').children().val();
		data   += "&CustomerId="+$('#frmAdvanceSettlement #cboCustomer').val();
		data   += "&CurrencyId="+$('#frmAdvanceSettlement #cboCurrency').val();
		data   += "&SettleAmount="+$(this).parent().parent().find('.cls_td_settleAmount').children().val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{		
		obj.validationEngine('showPrompt', json.msg,json.type);
		var t = setTimeout("alertx()",2000);
		hideWaiting();
		SubmitForm();
	}
	});
	hideWaiting();
}

function alertx()
{
	$('#frmAdvanceSettlement a').validationEngine('hide')	;
}

function LoadAutomate(customerId,currencyId)
{
	$('#frmAdvanceSettlement #cboCustomer').val(customerId);
	$('#frmAdvanceSettlement #cboCurrency').val(currencyId);
	SubmitForm();
}

function Save_Validation(obj)
{
	var settleAmount	= parseFloat($(obj).parent().parent().find('.cls_td_settleAmount').children().val());
	var invoiceBal		= parseFloat($(obj).parent().parent().find('.cls_td_invoiceBal').html());
	
	if(settleAmount=="" || settleAmount == 0 || isNaN(settleAmount))
	{
		$(obj).validationEngine('showPrompt','Invalid settle amount','fail');
		setTimeout("alertx()",2000);
		return false;
	}
	
	if(invoiceBal<settleAmount)
	{
		$(obj).validationEngine('showPrompt','Settle amount should not be greater than Invoice Balance Amount.','fail');
		setTimeout("alertx()",3000);
		return false;
	}
	return true;
}
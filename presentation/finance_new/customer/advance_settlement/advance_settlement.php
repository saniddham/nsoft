<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$userId 			= $_SESSION['userId'];

$cboCustomer		= (!isset($_REQUEST["cboCustomer"])?'':$_REQUEST["cboCustomer"]);
$cboCurrency		= (!isset($_REQUEST["cboCurrency"])?'':$_REQUEST["cboCurrency"]);

?>
<title>Customer Advance Settlement</title>

<!--<script type="text/javascript" src="presentation/finance_new/customer/advance_settlement/advance_settlement.js"></script>-->

<form id="frmAdvanceSettlement" name="frmAdvanceSettlement" method="post">
  <div align="center">
    <div class="trans_layoutL" style="width:1100px">
      <div class="trans_text">Customer Advance Settlement</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" align="center" bgcolor="#FFFFFF">
              <tr>
                <td width="17%" class="normalfnt">Customer <span class="compulsoryRed">*</span></td>
                <td width="36%" valign="top" class="normalfnt"><select name="cboCustomer" id="cboCustomer"  style="width:220px" class="validate[required] cls_submit" >
                    <?php
				
					$sql = "SELECT DISTINCT
								CU.intId 		AS CUSTOMER_ID,
								CU.strName 		AS CUSTOMER_NAME								
								FROM
								finance_customer_advance_header AS FCAH
								INNER JOIN finance_customer_advance_details AS FCAD ON FCAD.ADVANCE_NO = FCAH.ADVANCE_NO AND FCAH.ADVANCE_YEAR = FCAD.ADVANCE_YEAR
								INNER JOIN mst_customer AS CU ON FCAH.CUSTOMER_ID = CU.intId
								WHERE FCAD.SETTLE_BALANCE > 0
								ORDER BY CU.strName ";
					$result = $db->RunQuery($sql);
					echo "<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						if($row['CUSTOMER_ID']==$cboCustomer)
							echo "<option value=\"".$row['CUSTOMER_ID']."\" selected=\"selected\">".$row['CUSTOMER_NAME']."</option>";
						else
							echo "<option value=\"".$row['CUSTOMER_ID']."\" >".$row['CUSTOMER_NAME']."</option>"; 
					}
			  ?>
                  </select></td>
                <td width="21%" class="normalfnt">Currency <span class="compulsoryRed">*</span></td>
                <td width="26%" class="normalfnt"><select name="cboCurrency" id="cboCurrency"  style="width:100px" class="validate[required] cls_submit" >
                    <option value=""></option>
                    <?php
				$sql = "SELECT DISTINCT
							CU.intId 		AS CURRENCY_ID,
							CU.strCode		AS CURRENCY_NAME							
							FROM
							finance_customer_advance_header AS FCAH
							INNER JOIN finance_customer_advance_details AS FCAD ON FCAD.ADVANCE_NO = FCAH.ADVANCE_NO AND FCAH.ADVANCE_YEAR = FCAD.ADVANCE_YEAR
							INNER JOIN mst_financecurrency AS CU ON FCAH.CURRENCY_ID = CU.intId
							WHERE FCAD.SETTLE_BALANCE > 0
								AND FCAH.CUSTOMER_ID = '$cboCustomer'
							ORDER BY CU.strCode  ";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($row['CURRENCY_ID']==$cboCurrency)
						echo "<option value=\"".$row['CURRENCY_ID']."\" selected=\"selected\">".$row['CURRENCY_NAME']."</option>";
					else
						echo "<option value=\"".$row['CURRENCY_ID']."\">".$row['CURRENCY_NAME']."</option>";
				}
			  ?>
                  </select></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt" valign="top">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="4" class="normalfnt"><table width="100%" class="bordered" id="tblMain" >
                    <thead>
                      <tr>
                        <th width="8%" >Invoice No</th>
                        <th width="13%" >Invoice Remarks</th>
                        <th width="8%"  >Order No</th>
                        <th width="8%"  >Invoice Amount</th>
                        <th width="8%"  >Invoice Balance</th>
                        <th width="10%"  >Advance No</th>
                        <th width="13%"  >Advance GL</th>
                        <th width="9%"  >Advance Amount</th>
                        <th width="9%"  >Balance To Settle</th>
                        <th width="8%"  >Settle Amount</th>
                        <th width="6%"  >Save</th>
                      </tr>
                    </thead>
                     <tbody>
  <?php
  $result = GetMainDetails($cboCustomer,$cboCurrency);
  while($row = mysqli_fetch_array($result))
  {
  ?>                   
                      <tr>
                        <td width="8%" class="cls_td_invoiceNo" id="<?php echo $row["INVOICE_SERIAL_NO"].'/'.$row["INVOICE_SERIAL_YEAR"]?>"><?php echo $row["INVOICE_NO"]?></td>
                        <td width="13%" class="cls_td_invoiceRemarks"><?php echo ($row['INVOICE_REMARKS']==''?'&nbsp;':$row['INVOICE_REMARKS']); ?></td>
                        <td width="8%" class="cls_td_orderNo"><?php echo $row["ORDER_NO"].'/'.$row["ORDER_YEAR"]?></td>
                        <td width="8%" style="text-align:right" class="cls_td_invoiceValue"><?php echo number_format($row["INVOICE_VALUE"],2)?></td>
                        <td width="8%" style="text-align:right" class="cls_td_invoiceBal"><?php echo number_format($row["INVOICE_BAL"],2)?></td>
                        <td width="10%" style="text-align:center" class="cls_td_advanceNo"><?php echo CreateAdvanceNo_HTML($cboCustomer,$cboCurrency)?></td>
                        <td width="13%" style="text-align:right" class="cls_td_advanceGL" >&nbsp;</td>
                        <td width="9%" style="text-align:right" class="cls_td_advanceAmount" >&nbsp;</td>
                        <td width="9%" style="text-align:right" class="cls_td_balanceSettleAmount">&nbsp;</td>
                        <td width="8%" style="text-align:center" class="cls_td_settleAmount"><input type="text" style="width:80px;text-align:right" class="cls_txt_settleAmount"/></td>
                        <td width="6%" style="text-align:center"><a class="button green small cls_settle" >Settle</a></td>
                       </tr>
 <?php
  }
 ?>
                    </tbody>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="32" ><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td align="center" class="tableBorder_allRound"><a class="button white medium" id="butNew" name="butNew">&nbsp;New&nbsp;</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
              </tr>
            </table></td>
        </tr>
      </table>
    </div>
  </div>
</form>
<?php
function GetMainDetails($cboCustomer,$cboCurrency)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT 
				FCIH.SERIAL_NO		AS INVOICE_SERIAL_NO,
				FCIH.SERIAL_YEAR	AS INVOICE_SERIAL_YEAR,
				FCIH.INVOICE_NO		AS INVOICE_NO,
				FCIH.ORDER_NO		AS ORDER_NO,
				FCIH.ORDER_YEAR		AS ORDER_YEAR,
				FCIH.REMARKS		AS INVOICE_REMARKS,
				FCAD.ACCOOUNT_NO	AS GL_ID,
				CONCAT(FCOA.CHART_OF_ACCOUNT_CODE,'-',FCOA.CHART_OF_ACCOUNT_NAME) AS GL_NAME,
				(SELECT SUM(VALUE) FROM finance_customer_invoice_details SUB_CIV 
				WHERE SUB_CIV.SERIAL_NO = FCIH.SERIAL_NO 
					AND SUB_CIV.SERIAL_YEAR = FCIH.SERIAL_YEAR) AS INVOICE_VALUE,
				-- SUM(FT.VALUE)   AS INVOICE_BAL
				(SELECT SUM(CT.VALUE) FROM finance_customer_transaction CT 
				WHERE CT.INVOICE_NO = FCIH.SERIAL_NO
					AND CT.INVOICE_YEAR = FCIH.SERIAL_YEAR)		AS INVOICE_BAL
			FROM finance_customer_advance_header FCAH
			INNER JOIN finance_customer_advance_details FCAD ON FCAD.ADVANCE_NO = FCAH.ADVANCE_NO AND FCAD.ADVANCE_YEAR = FCAH.ADVANCE_YEAR
			INNER JOIN finance_customer_invoice_header FCIH ON FCIH.CUSTOMER_ID = FCAH.CUSTOMER_ID AND FCIH.CURRENCY_ID = FCAH.CURRENCY_ID
			INNER JOIN finance_mst_chartofaccount FCOA ON FCOA.CHART_OF_ACCOUNT_ID = FCAD.ACCOOUNT_NO
			WHERE FCAH.CUSTOMER_ID = $cboCustomer
				AND FCAH.CURRENCY_ID = $cboCurrency
				-- AND FCAH.COMPANY_ID = $companyId
				AND FCIH.COMPANY_ID = $companyId
				AND FCIH.STATUS = 1
				AND FCAD.SETTLE_BALANCE > 0
			GROUP BY FCIH.SERIAL_NO,FCIH.SERIAL_YEAR
			HAVING INVOICE_BAL > 0";
	return $db->RunQuery($sql);
}

function CreateAdvanceNo_HTML($cboCustomer,$cboCurrency)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT DISTINCT
	          CONCAT(FCAH.ADVANCE_NO,'/',FCAH.ADVANCE_YEAR) AS VALUE,
			  CONCAT(FCAH.ADVANCE_NO,'/',FCAH.ADVANCE_YEAR) AS TEXT
			FROM finance_customer_advance_header FCAH
			INNER JOIN finance_customer_advance_details FCAD ON FCAH.ADVANCE_NO = FCAD.ADVANCE_NO AND FCAH.ADVANCE_YEAR = FCAD.ADVANCE_YEAR
			WHERE FCAH.CUSTOMER_ID = '$cboCustomer'
				AND FCAH.CURRENCY_ID = '$cboCurrency'
				AND FCAD.SETTLE_BALANCE > 0
				AND FCAH.COMPANY_ID = $companyId
			ORDER BY FCAH.ADVANCE_YEAR,FCAH.ADVANCE_NO";
	$result = $db->RunQuery($sql);
	$string = "<select style=\"width:100px\" class=\"cls_cbo_advanceNo\">";
	$string .= "<option value=\"".""."\">&nbsp;</option>";
	while($row = mysqli_fetch_array($result))
	{
		$string .= "<option value=\"".$row["VALUE"]."\">".$row["TEXT"]."</option>";
	}
	$string .= "</select>";
	return $string;
}
?>
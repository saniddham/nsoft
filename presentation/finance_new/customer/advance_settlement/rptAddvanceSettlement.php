<?php
session_start();
$backwardseperator 	= "../../../../";
include_once  "{$backwardseperator}dataAccess/Connector.php";

$mainPath 			= $_SESSION['mainPath'];
$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$thisFilePath 		= $_SERVER['PHP_SELF'];

$advSettleNo 		= $_REQUEST['advSettleNo'];
$advSettleYear 		= $_REQUEST['advSettleYear'];

$detailArr 			= GetDetails($advSettleNo,$advSettleYear);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Advance Settlement</title>
<link rel="stylesheet" type="text/css" href="../../../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../../../css/button.css"/>
<link rel="stylesheet" type="text/css" href="../../../../css/promt.css"/>

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

<form id="frmAdvanceSettlement" name="frmAdvanceSettlement" method="post" autocomplete="off">
    <table width="900" align="center">
      <tr>
        <td width="888"><?php include '../../../../reportHeader.php'?></td>
      </tr>
      <tr>
        <td style="text-align:center">&nbsp;</td>
      </tr>
      <tr>
        <td style="text-align:center"><strong>CUSTOMER ADVANCE SETTLEMENT</strong></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td width="15%">&nbsp;</td>
            <td width="1%">&nbsp;</td>
            <td width="40%">&nbsp;</td>
            <td width="17%">&nbsp;</td>
            <td width="1%">&nbsp;</td>
            <td width="26%">&nbsp;</td>
          </tr>
          <tr>
            <td>Advance Settle No</td>
            <td>:</td>
            <td><?php echo $advSettleNo.' / '.$advSettleYear; ?></td>
            <td>Settle Date</td>
            <td>:</td>
            <td><?php echo $detailArr["SETTLE_DATE"]?></td>
          </tr>
          <tr>
            <td>Customer</td>
            <td>:</td>
            <td><?php echo $detailArr["customer"]; ?></td>
            <td>Currency</td>
            <td>:</td>
            <td><?php echo $detailArr["currency"]; ?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td ><table width="100%" border="0" class="rptBordered" id="tblMain">
        <thead>
            <tr>
                <th width="16%">Invoice No</th>
                <th width="16%">Order No</th>
                <th width="16%">Advance No</th>
                <th width="39%">GL Account</th>
                <th width="13%">Amount</th>
            </tr>
        </thead>        
        <tbody>
            <tr>
                <td style="text-align:center"><?php echo $detailArr['INVOICE_NO'].' - '.$detailArr['INVOICE_YEAR']; ?></td>
                <td style="text-align:center"><?php echo $detailArr['ORDER_NO'].' - '.$detailArr['ORDER_YEAR']; ?></td>
                <td style="text-align:center"><?php echo $detailArr['ADVANCE_NO'].' - '.$detailArr['ADVANCE_YEAR']; ?></td>
                <td style="text-align:left"><?php echo $detailArr['chartOfAccount']; ?></td>
                <td style="text-align:right"><?php echo number_format($detailArr['SETTLE_AMOUNT'],2); ?></td>
            </tr>
        </tbody>
        </table></td>
      </tr>
      <tr>
        <td class="normalfnt">&nbsp;</td>
      </tr>
      </table>
</form>
</body>
</html>
<?php
function GetDetails($advSettleNo,$advSettleYear)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT  SSH.INVOICE_NO, 
			SSH.INVOICE_YEAR, 
			SSH.ORDER_NO, 
			SSH.ORDER_YEAR, 
			SSH.ADVANCE_NO, 
			SSH.ADVANCE_YEAR, 
			SSH.SETTLE_AMOUNT, 
			SSH.SETTLE_DATE,
			FC.strCode AS currency,
			(
			SELECT CONCAT(FCOA.CHART_OF_ACCOUNT_NAME,' | ',FMT.FINANCE_TYPE_CODE,FMMT.MAIN_TYPE_CODE,FMST.SUB_TYPE_CODE,FCOA.CHART_OF_ACCOUNT_CODE)
			FROM finance_mst_chartofaccount FCOA
			INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
			INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
			WHERE FCOA.CHART_OF_ACCOUNT_ID = SSH.CHAT_OF_ACCOUNT_ID
			) AS chartOfAccount,
			MC.strName AS customer
			FROM 
			finance_customer_advance_settle_header AS SSH
			INNER JOIN mst_financecurrency FC ON FC.intId=SSH.CURRENCY_ID
			INNER JOIN mst_customer MC ON MC.intId=SSH.CUSTOMER_ID
			WHERE SSH.SETTLE_NO = '$advSettleNo' AND
			SSH.SETTLE_YEAR = '$advSettleYear' AND
			SSH.COMPANY_ID = '$companyId' ";
	
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	
	return $row;
}
?>
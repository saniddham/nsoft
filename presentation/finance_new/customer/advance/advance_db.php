<?php 
session_start();
//BEGIN - INCLUDE FILES {
include  "../../../../dataAccess/Connector.php";
include  "../../../../class/finance/customer/advance/cls_advance_get.php";
include  "../../../../class/finance/customer/advance/cls_advance_set.php";
include  "../../../../class/finance/cls_calculate_tax.php";
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {
$obj_advance_get	= new Cls_Advance_Get($db);
$obj_advance_set	= new Cls_Advance_Set($db);
$obj_calculate_tax	= new Cls_Calculate_Tax($db);
//END 	- CREATE OBJECTS }
$requestType		= $_REQUEST["RequestType"];

if($requestType == "URLLoadCustomer")
{
	$ledgerId		= $_REQUEST["LedgerId"];
	echo $obj_advance_get->LoadCustomer($ledgerId);
}
elseif($requestType == "URLLoadWhenChangeCustomer")
{
	$customerId		= $_REQUEST["CustomerId"];
	echo $obj_advance_get->LoadWhenChangeCustomer($customerId);
}
else if($requestType == "URLLoadWhenChangeOrderNo")
{
	$customerId		= $_REQUEST["CustomerId"];
	$currencyId		= $_REQUEST["CurrencyId"];
	echo $obj_advance_get->LoadWhenChangeOrderNo($customerId,$currencyId);
}
elseif($requestType == "URLSave")
{
	$customerId		= $_REQUEST["CustomerId"];
	$orderNo		= 'null';
	$orderYear		= 'null';
	$currencyId		= $_REQUEST["CurrencyId"];
	$payMethod		= $_REQUEST["PayMethod"];
	$ledgerAccount	= $_REQUEST["LedgerAccount"];
	$remarks		= $_REQUEST["Remarks"];
	$invoiceDate	= $_REQUEST["InvoiceDate"];
	$bankReferenceNo= $_REQUEST["BankReferenceNo"];
	$receivingAmount= $_REQUEST["ReceivingAmount"];
	$advanceAmount	= $_REQUEST["AdvanceAmount"];
	$invoiceType	= $_REQUEST["InvoiceType"];
	$detail_array	= json_decode($_REQUEST["Detail_Array"],true);
	
/*	$response		= $obj_advance_get->ValidateBeforeSave($orderNo,$orderYear,$receivingAmount);
	
	if($response["type"]=='false')
	{
		echo json_encode($response);
		return;
	}*/

	echo $obj_advance_set->Save($customerId,$orderNo,$orderYear,$currencyId,$payMethod,$ledgerAccount,$remarks,$invoiceDate,$detail_array,$bankReferenceNo,$invoiceType,$advanceAmount);
}
elseif($requestType == "URLCancel")
{
	$advanceNo		= $_REQUEST["AdvanceNo"];
	$advanceYear	= $_REQUEST["AdvanceYear"];
	
	$response		= $obj_advance_get->ValidateBeforeCancel($advanceNo,$advanceYear);
	if($response["type"]=='false')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_advance_set->Cancel($advanceNo,$advanceYear);
}
?>
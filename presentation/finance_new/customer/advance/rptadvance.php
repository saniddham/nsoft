<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once "class/finance/cls_common_get.php";
require_once "class/finance/cls_convert_amount_to_word.php";

$obj_fin_com		= new Cls_Common_Get($db); 
$obj_AmtName	   	= new Cls_Convert_Amount_To_Word($db);

$advanceNo 			= $_REQUEST['AdvanceNo'];
$advanceYear 		= $_REQUEST['AdvanceYear'];

$header_array 		= GetHeaderDetails($advanceNo,$advanceYear);
$detail_result 		= GetGridDetails($advanceNo,$advanceYear);
$locationId			= $header_array["LOCATION_ID"];
$companyId			= $header_array["COMPANY_ID"];
?>
<head>
<title>Customer Advance Receiving</title>

<style type="text/css">
.apDiv1 {
	position: absolute;
	left: 380px;
	top: 100px;
	width: auto;
	height: auto;
	z-index: 0;
	opacity: 0.1;
}
</style>
</head>
<body>
<div id="partPay" class="apDiv1 <?php echo $header_array["STATUS"]=='10'?'maskShow':'maskHide'?>"><img src="images/cancelled.png"  /></div>
<form id="frmSalesInvoice" name="frmSalesInvoice" method="post">
  <table width="900" align="center">
    <tr>
      <td width="888"><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
      <td style="text-align:center">&nbsp;</td>
    </tr>
    <tr>
      <td style="text-align:center" class="reportHeader">Customer Advance Receiving</td>
    </tr>
    <tr>
      <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td>Advance Payment No</td>
            <td>:</td>
            <td><?php echo $obj_fin_com->getSerialNo($header_array["SERIAL_NO"],$header_array["ADVANCE_DATE"],$companyId,'RunQuery')//echo $header_array["ADVANCE_NO"]; ?></td>
            <td>Date</td>
            <td>:</td>
            <td><?php echo $header_array["ADVANCE_DATE"]?></td>
          </tr>
          <tr>
            <td>Customer</td>
            <td>:</td>
            <td><?php echo $header_array["CUSTOMER_NAME"]; ?></td>
            <td>Order No</td>
            <td>:</td>
            <td><?php echo $header_array["ORDER_NO"]; ?></td>
          </tr>
          <tr>
            <td width="15%">Advance Amount</td>
            <td width="1%">:</td>
            <td width="43%"><?php echo number_format($header_array["AMOUNT"],2,'.',''); ?></td>
            <td width="15%">Currency</td>
            <td width="1%">:</td>
            <td width="25%"><?php echo $header_array["CURRENCY_NAME"]; ?></td>
          </tr>
          <tr>
            <td>Bank Reference No</td>
            <td>:</td>
            <td><?php echo $header_array["BANK_REFERENCE_NO"]; ?></td>
            <td>Payment Method</td>
            <td>:</td>
            <td><?php echo $header_array["PAYMENT_METHOD"]; ?></td>
          </tr>
          <tr>
            <td>Remarks</td>
            <td>:</td>
            <td rowspan="2" valign="top"><?php echo $header_array["REMARKS"]; ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td align="right"><a class="button green small no-print" target="rpt_advance_pdf.php" href="presentation/finance_new/customer/advance/rpt_advance_pdf.php?AdvanceNo=<?php echo $advanceNo; ?>&AdvanceYear=<?php echo $advanceYear ; ?>">Click here to print receipt</a></td>
    </tr>
    <tr>
      <td ><table width="100%" border="0" class="rptBordered" id="tblMain">
          <thead>
            <tr>
              <th width="4%">&nbsp;</th>
              <th width="28%">Account</th>
              <th width="41%">Remarks</th>
              <th width="19%">Cost Center</th>
              <th width="8%">Amount</th>
            </tr>
          </thead>
          <tbody>
            <?php
$totAmount 		= 0;
$i				= 0;
while($row = mysqli_fetch_array($detail_result))
{ 
?>
            <tr>
              <td class="cls_td_check" align="center"><?php echo ++$i;?>.</td>
              <td class="cls_td_salesOrderNo"><?php echo $row["ACCOUNT_NAME"]?></td>
              <td width="41%"><?php echo $row["REMARKS"]?></td>
              <td width="19%"><?php echo $row["COST_CENTER_NAME"]?></td>
              <td width="8%" style="text-align:right"><?php echo number_format($row["AMOUNT"],2);?></td>
            </tr>
            <?php
	$totAmount	+= round($row["AMOUNT"],2);
}
?>
            <tr>
              <td colspan="4" align="right"><b>TOTAL </b></td>
              <td width="8%" style="text-align:right"><b><?php echo number_format($totAmount,2);?></b></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td class="normalfnt"><b><?php echo $obj_AmtName->Convert_Amount($totAmount,$header_array["CURRENCY_NAME"]); ?></b></td>
    </tr>
    <tr>
      <td class="normalfnt">&nbsp;</td>
    </tr>
  </table>
</form>
</body>
<?php
function GetHeaderDetails($advanceNo,$advanceYear)
{
	global $db;
	
	$sql = "SELECT 
				CAH.ADVANCE_NO								AS SERIAL_NO,
				CONCAT(CAH.ADVANCE_NO,'/',CAH.ADVANCE_YEAR)	AS ADVANCE_NO,
				CONCAT(CAH.ORDER_NO,'/',CAH.ORDER_YEAR)		AS ORDER_NO,	
				CU.strName									AS CUSTOMER_NAME,
				CAH.ADVANCE_DATE							AS ADVANCE_DATE,
				C.strDescription							AS CURRENCY_NAME,
				CONCAT(COA.CHART_OF_ACCOUNT_CODE,' - ',COA.CHART_OF_ACCOUNT_NAME)		AS LEDGER_NAME,
				PM.strName									AS PAYMENT_METHOD,
				CAH.BANK_REFERENCE_NO						AS BANK_REFERENCE_NO,
				CAH.REMARKS									AS REMARKS,
				CAH.COMPANY_ID								AS COMPANY_ID,
				CAH.LOCATION_ID								AS LOCATION_ID,
				CAH.STATUS									AS STATUS,
				ROUND(COALESCE((SELECT SUM(AMOUNT) 
				FROM finance_customer_advance_details SUB_CAD 
				WHERE SUB_CAD.ADVANCE_NO = CAH.ADVANCE_NO 
				AND SUB_CAD.ADVANCE_YEAR = CAH.ADVANCE_YEAR),0),2) AS AMOUNT
			FROM finance_customer_advance_header CAH
			INNER JOIN mst_customer CU ON CU.intId = CAH.CUSTOMER_ID
			INNER JOIN mst_financecurrency C ON C.intId = CAH.CURRENCY_ID
			LEFT JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID = CAH.LEDGER_ID
			INNER JOIN mst_financepaymentsmethods PM ON PM.intId = CAH.PAYMENT_METHOD
			WHERE CAH.ADVANCE_NO = $advanceNo AND CAH.ADVANCE_YEAR = $advanceYear ";
				
	$result = $db->RunQuery($sql);
	return mysqli_fetch_array($result);
}

function GetGridDetails($advanceNo,$advanceYear)
{
	global $db;
	
	$sql = "SELECT 
				CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE,' - ',FCOA.CHART_OF_ACCOUNT_NAME) AS ACCOUNT_NAME,
				CAD.AMOUNT								AS AMOUNT,
				CAD.REMARKS								AS REMARKS,
				FD.strName								AS COST_CENTER_NAME				
			FROM finance_customer_advance_header CAH
			INNER JOIN finance_customer_advance_details CAD 
			  ON CAD.ADVANCE_NO = CAH.ADVANCE_NO 
			    AND CAD.ADVANCE_YEAR = CAH.ADVANCE_YEAR
			INNER JOIN finance_mst_chartofaccount FCOA
			  ON FCOA.CHART_OF_ACCOUNT_ID = CAD.ACCOOUNT_NO
			INNER JOIN finance_mst_account_sub_type FMST 
			  ON FMST.SUB_TYPE_ID = FCOA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type FMMT 
			  ON FMMT.MAIN_TYPE_ID = FMST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_type FMT 
			  ON FMT.FINANCE_TYPE_ID = FMMT.FINANCE_TYPE_ID
			INNER JOIN mst_financedimension FD 
			  ON FD.intId = CAD.COST_CENTER_ID
			WHERE CAH.ADVANCE_NO = $advanceNo AND CAH.ADVANCE_YEAR = $advanceYear";
	return $db->RunQuery($sql);
}
?>
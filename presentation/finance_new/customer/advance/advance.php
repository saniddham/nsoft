<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once ("class/finance/customer/cls_common_get.php");
include_once ("class/finance/cls_get_gldetails.php");
include_once ("class/cls_commonFunctions_get.php");
//include 	  "include/javascript.html";

$obj_common_get				= new Cls_Common_Get($db);
$obj_GLDetails_get			= new Cls_Get_GLDetails($db);
$obj_common_function_get	= new cls_commonFunctions_get($db);

$session_locationId			= $_SESSION["CompanyID"];
$session_companyId			= $_SESSION["headCompanyId"];
$session_userId 			= $_SESSION['userId'];

$advanceNo 					= (!isset($_REQUEST['AdvanceNo'])?'':$_REQUEST['AdvanceNo']);
$advanceYear 				= (!isset($_REQUEST['AdvanceYear'])?'':$_REQUEST['AdvanceYear']);
$customerId					= (!isset($_REQUEST['CustomerId'])?'':$_REQUEST['CustomerId']);
$currencyId					= (!isset($_REQUEST['CurrencyId'])?'':$_REQUEST['CurrencyId']);
$programCode				= 'P0702';

if($obj_common_function_get->ValidateSpecialPermission('23',$_SESSION["userId"],'RunQuery'))
	$dateDisabled		= "";
else
	$dateDisabled		= "disabled='disabled'";	

if($advanceNo!='' && $advanceYear!='')
{	
	$header_array 		= GetHeaderDetails($advanceNo,$advanceYear);
	$detail_result 		= GetGridDetails($advanceNo,$advanceYear);
}
if(!isset($_REQUEST["AdvanceNo"]))
	$payDate		= date("Y-m-d");
else
	$payDate		= $header_array["ADVANCE_DATE"];
?>
<title>Customer Advance Receiving</title>

<!--<script type="text/javascript" src="presentation/finance_new/customer/advance/advance.js"></script>-->
<script>
	var customerIdAL	='<?php echo $customerId; ?>';
	var currencyIdAL	='<?php echo $currencyId; ?>';
</script>

<form id="frmAdvancePayment" name="frmAdvancePayment" method="post">
  <div align="center">
    <div class="trans_layoutL" style="width:1100px">
      <div class="trans_text">Customer Advance Receiving</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" align="center" bgcolor="#FFFFFF">
              <tr>
                <td width="17%" class="normalfnt">Advance Payment No</td>
                <td width="36%" class="normalfnt"><input name="txtAdvanceNo" type="text" disabled="disabled" id="txtAdvanceNo" style="width:100px" value="<?php echo (!isset($_REQUEST["AdvanceNo"])?'':$header_array["ADVANCE_NO"]); ?>" />
                  <input name="txtAdvanceYear" type="text" disabled="disabled" id="txtAdvanceYear" style="width:45px" value="<?php echo (!isset($_REQUEST["AdvanceNo"])?'':$header_array["ADVANCE_YEAR"]); ?>"/></td>
                <td width="21%" class="normalfnt">Date</td>
                <td width="26%" class="normalfnt"><input name="txtInvoiceDate" type="text" value="<?php echo $payDate; ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtInvoiceDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo $dateDisabled?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
              <tr>
                <td class="normalfnt">Customer <span class="compulsoryRed">*</span></td>
                <td class="normalfnt" valign="top"><select name="cboCustomer" id="cboCustomer" style="width:280px;height:20px" class="validate[required]" >
                    <option value="">&nbsp;</option>
                    <?php 
		$sql = "SELECT DISTINCT 
				C.intId AS ID, C.strName AS NAME
				FROM trn_orderheader OH
				INNER JOIN mst_customer C ON C.intId = OH.intCustomer
				WHERE OH.intStatus = 1
				ORDER BY C.strName";
		$result = $db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			if($header_array["CUSTOMER_ID"]==$row["ID"])
				echo "<option value=\"".$row["ID"]."\" selected=\"selected\">".$row["NAME"]."</option>";
			else
				echo "<option value=\"".$row["ID"]."\">".$row["NAME"]."</option>";
		}
?>
                  </select></td>
                <td class="normalfnt">Payment Method <span class="compulsoryRed">*</span></td>
                <td class="normalfnt"><select name="cboPaymentMethod" id="cboPaymentMethod" style="width:280px" class="validate[required]">
                    <option value=""></option>
                    <?php
					$sql = "SELECT
							mst_financepaymentsmethods.intId,
							mst_financepaymentsmethods.strName
							FROM mst_financepaymentsmethods
							WHERE
							mst_financepaymentsmethods.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$header_array["PAYMENT_METHOD_ID"])
							echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
					?>
                  </select></td>
              </tr>
              <tr>
                <td class="normalfnt">Currency <span class="compulsoryRed">*</span></td>
                <td class="normalfnt" valign="top"><select name="cboCurrency" id="cboCurrency"  style="width:280px" class="validate[required] cls_load_details cls_cbo_CurrencyCalExchangeRate" >
                    <option value="">&nbsp;</option>
                    <?php
				$sql = "SELECT intId,strCode FROM mst_financecurrency WHERE intStatus=1 ";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($row['intId'] == $header_array["CURRENCY_ID"])
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";
					else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
				}
			  ?>
                  </select>
                  <input type="text" name="txtExchangeRate" id="txtExchangeRate" style="width:60px;text-align:right" disabled="disabled" value="" title="Exchange Rate" class="cls_txt_exchangeRate validate[required]"/></td>
                <td class="normalfnt">Bank Reference Number</td>
                <td class="normalfnt"><input type="text" name="txtBankRefNo" id="txtBankRefNo" style="width:280px" maxlength="20" value="<?php echo (!isset($_REQUEST["AdvanceNo"])?'':$header_array["BANK_REFERENCE_NO"]); ?>" /></td>
              </tr>
              <tr>
                <td class="normalfnt">Remarks</td>
                <td valign="top" class="normalfnt"><textarea name="txtRemarks" id="txtRemarks" style="width:280px;height:50px" ><?php echo (!isset($_REQUEST["AdvanceNo"])?'':$header_array["REMARKS"]); ?></textarea></td>
                <td class="normalfnt" valign="top">Invoice Type</td>
                <td class="normalfnt" valign="top"><select name="cboInvoiceType" id="cboInvoiceType" style="width:280px" disabled="disabled">
                    <option value=""></option>
                    <?php
$sql = "SELECT intId, strInvoiceType FROM mst_invoicetype ORDER BY strInvoiceType ";
$result = $db->RunQuery($sql);
while($row = mysqli_fetch_array($result))
{
	if($row["intId"]==$header_array["INVOICE_TYPE"])
		echo "<option value=\"".$row["intId"]."\" selected=\"selected\">".$row["strInvoiceType"]."</option>";
	else
		echo "<option value=\"".$row["intId"]."\">".$row["strInvoiceType"]."</option>";
}
?>
                  </select></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><table width="100%" border="0" class="bordered" id="tblPODetails">
              <thead>
                <tr>
                  <th width="14%">Order Amount</th>
                  <th width="14%">Advanced Amount</th>
                  <th width="14%">Credit Notes</th>
                  <th width="14%">Debit Notes</th>
                  <th width="13%">Payment Received</th>
                  <th width="14%">Receiving Balance</th>
                  <th width="17%"> Receiving Amount <span class="compulsoryRed">*</span></th>
                </tr>
              </thead>
              <tr>
                <td class="cls_td_orderAmount" style="text-align:right"><?php echo (!isset($_REQUEST["AdvanceNo"])?'':round($header_array["ORDER_AMOUNT"],2));?></td>
                <td class="cls_td_advancedAmount" style="text-align:right"><?php echo (!isset($_REQUEST["AdvanceNo"])?'':round($header_array["ADVANCED_AMOUNT"],2));?></td>
                <td class="cls_td_creditNotes" style="text-align:right"><?php echo (!isset($_REQUEST["AdvanceNo"])?'':round($header_array["CREDIT_AMOUNT"],2));?></td>
                <td class="cls_td_debitNotes" style="text-align:right"><?php echo (!isset($_REQUEST["AdvanceNo"])?'':round($header_array["DEBIT_AMOUNT"],2));?></td>
                <td class="cls_td_paymentReceived" style="text-align:right"><?php echo (!isset($_REQUEST["AdvanceNo"])?'':round($header_array["PAID_AMOUNT"],2));?></td>
                <td class="cls_td_balanceToReceive" style="text-align:right"><?php echo (!isset($_REQUEST["AdvanceNo"])?'':round($header_array["BALANCE_TO_RECEIVE"],2));?></td>
                <td class="cls_td_receiving" style="text-align:center"><input class="cls_receiving" type="text" style="text-align:right;width:100%" value="<?php echo (!isset($_REQUEST["AdvanceNo"])?'':round($header_array["AMOUNT"],2));?>"/></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><table width="100%" class="bordered" id="tblMain" >
              <tr>
                <th height="30" colspan="5" >GL Allocation
                  <div style="float:right"><a class="button white small" id="cls_addNewGL">Add New GL</a></div></th>
              </tr>
              <tr>
                <th width="3%" height="30" >Del</th>
                <th width="28%" >GL Account <span class="compulsoryRed">*</span></th>
                <th width="15%" >Amount <span class="compulsoryRed">*</span></th>
                <th width="38%" >Remarks </th>
                <th width="16%" >Cost Center <span class="compulsoryRed">*</span></th>
              </tr>
              <?php
$booAvailable	= "";
$total			= 0;
if(isset($_REQUEST["AdvanceNo"]))
{
	while($row = mysqli_fetch_array($detail_result))
	{
		$booAvailable	= true;
	?>
				  <tr class="firstRow">
					<td class="cls_td_Del" align="center"><img src="images/del.png" alt="delete" name="butDel" class="mouseover removeRow" id="butDel" /></td>
					<td class="cls_td_GL"><select name="cboAccounts" id="cboAccounts" style="width:100%" class="validate[required]">
						<?php echo $obj_GLDetails_get->getGLCombo('CUSTOMER_ADVANCE',$row["ACCOOUNT_NO"]);?>
					  </select></td>
					<td class="cls_td_GLAmount"><input name="txtAmmount" type="text" id="txtAmmount" style="width:100%;text-align:right" class="validate[required,custom[number]] cls_GLAmount" value="<?php echo ($row['AMOUNT']==''?0:round($row['AMOUNT'],2))?>" /></td>
					<td class="cls_td_remarks"><input type="text" name="txtMemo" id="txtMemo" style="width:100%" class="clsMemo" value="<?php echo $row['REMARKS']; ?>" /></td>
					<td class="cls_td_costCenter"><select id="cboCostCenter" style="width:100%;"  class="validate[required]" >
						<?php echo GetCostCenter($row["COST_CENTER_ID"]);?>
					  </select></td>
				  </tr>
				  <?php
		$total += round($row['AMOUNT'],2);
	}
}
?>
              <?php
if(!$booAvailable){
?>
              <tr class="firstRow">
                <td class="cls_td_Del" align="center"><img src="images/del.png" alt="delete" name="butDel" class="mouseover removeRow" id="butDel" /></td>
                <td class="cls_td_GL"><select name="cboAccounts" id="cboAccounts" style="width:100%" class="validate[required]">
                    <?php echo $obj_GLDetails_get->getGLCombo('CUSTOMER_ADVANCE',$row["ACCOOUNT_NO"]);?>
                  </select></td>
                <td class="cls_td_GLAmount"><input name="txtAmmount" type="text" id="txtAmmount" style="width:100%;text-align:right" class="validate[required,custom[number]] cls_GLAmount" value="<?php echo ($row['AMOUNT']==""?0:round($row['AMOUNT'],2))?>" /></td>
                <td class="cls_td_remarks"><input type="text" name="txtMemo" id="txtMemo" style="width:100%" class="clsMemo" value="<?php echo $row['REMARKS']; ?>" /></td>
                <td class="cls_td_costCenter"><select id="cboCostCenter" style="width:100%;"  class="validate[required]" >
                    <?php echo GetCostCenter($row["COST_CENTER_ID"]);?>
                  </select></td>
              </tr>
              <?php
}
?>
            </table></td>
        </tr>
        <tr>
          <td><table width="301" border="0" align="right" class="normalfnt" style="font-weight:bold">
              <tr>
                <td width="166">Advance Amount</td>
                <td width="11" style="text-align:center"><b>:</b></td>
                <td width="91" style="text-align:right;font-weight:bold" id="td_totalAdvanceAmount"><?php echo number_format($total,2)?></td>
              </tr>
              <tr>
                <td>GL Allocated Amount</td>
                <td style="text-align:center"><b>:</b></td>
                <td style="text-align:right;font-weight:bold" id="td_totalGLAlloAmount"><?php echo number_format($total,2)?></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="32" ><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td align="center" ><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($form_permision['edit']==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');?><a class="button white medium" id="butCancel" name="butCancel">&nbsp;Cancel&nbsp;</a><a class="button white medium" id="butReport" name="butReport">&nbsp;Report&nbsp;</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
              </tr>
            </table></td>
        </tr>
      </table>
    </div>
  </div>
</form>
<?php
function GetHeaderDetails($advanceNo,$advanceYear)
{
	global $db;
	
	$sql = "SELECT 
				CAH.ADVANCE_NO 												AS ADVANCE_NO,
				CAH.ADVANCE_YEAR											AS ADVANCE_YEAR,
				CONCAT(CAH.ORDER_NO,'/',CAH.ORDER_YEAR)						AS ORDER_NO,	
				CAH.CUSTOMER_ID												AS CUSTOMER_ID,
				CAH.ADVANCE_DATE											AS ADVANCE_DATE,
				CAH.CURRENCY_ID												AS CURRENCY_ID,
				C.strDescription											AS CURRENCY_NAME,
				CAH.LEDGER_ID												AS LEDGER_ID,
				CONCAT(COA.strCode,' - ',COA.strName)						AS LEDGER_NAME,
				CAH.PAYMENT_METHOD											AS PAYMENT_METHOD_ID,
				PM.strName													AS PAYMENT_METHOD,
				CAH.BANK_REFERENCE_NO										AS BANK_REFERENCE_NO,
				CAH.REMARKS													AS REMARKS,
				CAH.STATUS													AS STATUS,
				ROUND(COALESCE((SELECT SUM(AMOUNT) 
				FROM finance_customer_advance_details SUB_CAD 
				WHERE SUB_CAD.ADVANCE_NO = CAH.ADVANCE_NO 
				AND SUB_CAD.ADVANCE_YEAR = CAH.ADVANCE_YEAR),0),2) 			AS AMOUNT,
				
				ROUND(COALESCE((SELECT SUM((intQty * dblPrice))
				FROM trn_orderdetails SUB_OD
				WHERE SUB_OD.intOrderNo = CAH.ORDER_NO
				AND SUB_OD.intOrderYear = CAH.ORDER_YEAR),0),2)				AS ORDER_AMOUNT,
				
				ROUND(COALESCE((SELECT
				SUM(SUB_CT.VALUE)
				FROM finance_customer_transaction SUB_CT 
				WHERE SUB_CT.ORDER_YEAR = CAH.ORDER_YEAR
				AND SUB_CT.ORDER_NO = CAH.ORDER_NO
				AND SUB_CT.DOCUMENT_NO != CAH.ADVANCE_NO
				AND SUB_CT.DOCUMENT_YEAR != CAH.ADVANCE_YEAR
				AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'),0),2) 				AS ADVANCED_AMOUNT,
				
				COALESCE((SELECT
				SUM(SUB_CT.VALUE)
				FROM finance_customer_transaction SUB_CT 
				WHERE SUB_CT.ORDER_YEAR = CAH.ORDER_YEAR
				AND SUB_CT.ORDER_NO = CAH.ORDER_NO
				AND SUB_CT.DOCUMENT_TYPE = 'CREDIT'),0) 					AS CREDIT_AMOUNT,
				
				COALESCE((SELECT
				SUM(SUB_CT.VALUE)
				FROM finance_customer_transaction SUB_CT 
				WHERE SUB_CT.ORDER_YEAR = CAH.ORDER_YEAR
				AND SUB_CT.ORDER_NO = CAH.ORDER_NO
				AND SUB_CT.DOCUMENT_TYPE = 'DEBIT'),0) 						AS DEBIT_AMOUNT,
				
				ROUND(COALESCE((SELECT
				SUM(SUB_CT.VALUE)
				FROM finance_customer_transaction SUB_CT 
				WHERE SUB_CT.ORDER_YEAR = CAH.ORDER_YEAR
				AND SUB_CT.ORDER_NO = CAH.ORDER_NO
				AND SUB_CT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2) 				AS PAID_AMOUNT
					
			FROM finance_customer_advance_header CAH
			INNER JOIN mst_financecurrency C ON C.intId = CAH.CURRENCY_ID
			LEFT JOIN mst_financechartofaccounts COA ON COA.intId = CAH.LEDGER_ID
			INNER JOIN mst_financepaymentsmethods PM ON PM.intId = CAH.PAYMENT_METHOD
			WHERE CAH.ADVANCE_NO = $advanceNo AND CAH.ADVANCE_YEAR = $advanceYear ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	$header_array["ADVANCE_NO"]				= $row["ADVANCE_NO"];
	$header_array["ADVANCE_YEAR"]			= $row["ADVANCE_YEAR"];
	$header_array["ADVANCE_DATE"]			= $row["ADVANCE_DATE"];
	$header_array["CUSTOMER_ID"]			= $row["CUSTOMER_ID"];
	$header_array["ORDER_HTML"]				= "<option value=\"".$row["ORDER_NO"]."\">".$row["ORDER_NO"]."</option>";
	$header_array["LEDGER_ID"]				= $row["LEDGER_ID"];
	$header_array["LEDGER_HTML"]			= "<option value=\"".$row["LEDGER_ID"]."\">".$row["LEDGER_NAME"]."</option>";
	$header_array["CURRENCY_ID"]			= $row["CURRENCY_ID"];
	$header_array["REMARKS"]				= $row["REMARKS"];
	$header_array["PAYMENT_METHOD_ID"]		= $row["PAYMENT_METHOD_ID"];
	$header_array["BANK_REFERENCE_NO"]		= $row["BANK_REFERENCE_NO"];
	$header_array["AMOUNT"]					= $row["AMOUNT"];
	$header_array["STATUS"]					= $row["STATUS"];
	$header_array["ORDER_AMOUNT"]			= $row["ORDER_AMOUNT"];
	$header_array["ADVANCED_AMOUNT"]		= abs($row["ADVANCED_AMOUNT"]);
	$header_array["CREDIT_AMOUNT"]			= abs($row["CREDIT_AMOUNT"]);
	$header_array["DEBIT_AMOUNT"]			= abs($row["DEBIT_AMOUNT"]);
	$header_array["PAID_AMOUNT"]			= abs($row["PAID_AMOUNT"]);
	$header_array["BALANCE_TO_RECEIVE"]		= $row["ORDER_AMOUNT"] - (abs($row["ADVANCED_AMOUNT"])  + abs($row["CREDIT_AMOUNT"]) + abs($row["PAID_AMOUNT"]));
	
	return $header_array;
}

function GetGridDetails($advanceNo,$advanceYear)
{
	global $db;
	
	$sql = "SELECT
				CAD.ACCOOUNT_NO							AS ACCOOUNT_NO,
				CONCAT(COA.strCode,' - ',COA.strName)	AS ACCOUNT_NAME,
				CAD.AMOUNT								AS AMOUNT,
				CAD.REMARKS								AS REMARKS,
				CAD.COST_CENTER_ID						AS COST_CENTER_ID,
				FD.strName								AS COST_CENTER_NAME	
			FROM finance_customer_advance_header CAH
			INNER JOIN finance_customer_advance_details CAD ON CAD.ADVANCE_NO = CAH.ADVANCE_NO AND CAD.ADVANCE_YEAR = CAH.ADVANCE_YEAR
			INNER JOIN mst_financechartofaccounts COA ON COA.intId = CAD.ACCOOUNT_NO
			INNER JOIN mst_financedimension FD ON FD.intId = CAD.COST_CENTER_ID
			WHERE CAH.ADVANCE_NO = $advanceNo AND CAH.ADVANCE_YEAR = $advanceYear";
	return $db->RunQuery($sql);
}
function GetGL($id)
{
	global $db;
	global $session_companyId;
	
	$sql = "SELECT
			mst_financechartofaccounts.intId,
			mst_financechartofaccounts.strCode,
			mst_financechartofaccounts.strName
			FROM
			mst_financechartofaccounts
			INNER JOIN mst_financechartofaccounts_companies ON mst_financechartofaccounts.intId = mst_financechartofaccounts_companies.intChartOfAccountId
			WHERE
			mst_financechartofaccounts.intStatus = '1' AND
			mst_financechartofaccounts.intFinancialTypeId = '24' AND
			mst_financechartofaccounts_companies.intCompanyId = '$session_companyId' AND
			mst_financechartofaccounts.strType =  'Posting'
			ORDER BY strCode";
	$result = $db->RunQuery($sql);
		$string = "<option value=\"".""."\"></option>";
	while($row = mysqli_fetch_array($result))
	{
		if($row['intId'] == $id)
			$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']." - ".$row['strName']."</option>";
		else
			$string .= "<option value=\"".$row['intId']."\">".$row['strCode']." - ".$row['strName']."</option>";
	}
	return $string;
}

function GetCostCenter($id)
{
	global $db;
	
	$sql = "SELECT intId,strName FROM mst_financedimension WHERE intStatus = 1 order by strName";
	$result = $db->RunQuery($sql);
		$string = "<option value=\"".""."\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		if($row['intId'] == $id)
			$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
		else
			$string .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
	}
	return $string;
}
?>

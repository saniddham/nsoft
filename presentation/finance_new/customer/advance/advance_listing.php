<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$MenuId				= 702;
$reportId			= 899;
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'STATUS'=>'CAH.STATUS',
				'CONCAT_ADVANCE_NO'=>"CONCAT(CAH.ADVANCE_NO,'/',CAH.ADVANCE_YEAR)",
				'ADVANCE_DATE'=>'CAH.ADVANCE_DATE',
				'CUSTOMER_NAME'=>'CU.strName',
				'BANK_REFERENCE_NO'=>'CAH.BANK_REFERENCE_NO',
				'PAYMENT_METHOD'=>'PM.strName',
				'REMARKS'=>'CAH.REMARKS',
				'CURRENCY_NAME'=>'C.strDescription'
				);
				
$arr_status = array('Confirmed'=>'1','Canceled'=>'10');
foreach($arr as $k=>$v)
{
	if($v['field']=='STATUS')
	{
		$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND CAH.ADVANCE_DATE = '".date('Y-m-d')."'";
	
// }

$sql = "SELECT SUB_1.* FROM
			(SELECT
				CAH.ADVANCE_NO										AS ADVANCE_NO,
				CAH.ADVANCE_YEAR									AS ADVANCE_YEAR,
				CONCAT(CAH.ADVANCE_NO,'/',CAH.ADVANCE_YEAR)			AS CONCAT_ADVANCE_NO,
				CU.strName											AS CUSTOMER_NAME,
				CAH.ADVANCE_DATE									AS ADVANCE_DATE,
				C.strDescription									AS CURRENCY_NAME,
				PM.strName											AS PAYMENT_METHOD,
				CAH.BANK_REFERENCE_NO								AS BANK_REFERENCE_NO,
				CAH.REMARKS											AS REMARKS,
				ROUND(COALESCE((SELECT SUM(AMOUNT) 
				FROM finance_customer_advance_details SUB_CAD 
				WHERE SUB_CAD.ADVANCE_NO = CAH.ADVANCE_NO 
				AND SUB_CAD.ADVANCE_YEAR = CAH.ADVANCE_YEAR),0),2) 	AS AMOUNT,
				'View'												AS View,
				IF(CAH.STATUS=1,'Confirmed','Canceled') 			AS STATUS
			FROM finance_customer_advance_header CAH
			INNER JOIN mst_customer CU ON CU.intId = CAH.CUSTOMER_ID
			INNER JOIN mst_financecurrency C ON C.intId = CAH.CURRENCY_ID
			INNER JOIN mst_financepaymentsmethods PM ON PM.intId = CAH.PAYMENT_METHOD
			WHERE CAH.COMPANY_ID = $session_companyId
			$where_string
		)  
		AS SUB_1 WHERE 1=1";

$jq	 	= new jqgrid('',$db);	
$col	= array();
$cols 	= array();

$col["title"] 			= "Status";
$col["name"] 			= "STATUS";
$col["width"] 			= "2"; 						// not specifying width will expand to fill space
$col["align"] 			= "left";
$col["sortable"] 		= true; 						// this column is not sortable
$col["search"] 			= true; 						// this column is not searchable
$col["editable"] 		= false;
$col["stype"] 			= "select";
$str 					= ":All;Confirmed:Confirmed;Canceled:Canceled";  // all row, blank row, then all db values. ; is separator
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Serial No";
$col["name"] 			= "ADVANCE_NO";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Serial Year";
$col["name"] 			= "ADVANCE_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Advance No";
$col["name"] 			= "CONCAT_ADVANCE_NO";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col['link']			= '?q='.$MenuId.'&AdvanceNo={ADVANCE_NO}&AdvanceYear={ADVANCE_YEAR}';
$col["linkoptions"] 	= "target=new";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Date";
$col["name"] 			= "ADVANCE_DATE";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Customer Name";
$col["name"] 			= "CUSTOMER_NAME";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Bank RefNo";
$col["name"] 			= "BANK_REFERENCE_NO";
$col["width"] 			= "4";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;



$col["title"] 			= "Payment Method";
$col["name"] 			= "PAYMENT_METHOD";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Remarks";
$col["name"] 			= "REMARKS";
$col["width"] 			= "4";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "CURRENCY_NAME";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Value";
$col["name"] 			= "AMOUNT";
$col["width"] 			= "2";
$col["align"] 			= "right";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Report";
$col["name"] 			= "View";
$col["width"] 			= "1";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= '?q='.$reportId.'&AdvanceNo={ADVANCE_NO}&AdvanceYear={ADVANCE_YEAR}';
$col["linkoptions"] 	= "target=new";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Customer Advance Receiving Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'ADVANCE_YEAR,ADVANCE_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "ASC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Confirmed"}
     ]
}
SEARCH_JSON;
//$grid["postData"] = array("filters" => $sarr ); 

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Customer Advance Receiving Listing</title>
<?php
echo $out;
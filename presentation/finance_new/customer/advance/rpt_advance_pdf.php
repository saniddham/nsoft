<?php
session_start();
ini_set('display_errors',0);
//BEGIN - SESSION {
$session_companyId	= $_SESSION["CompanyID"];
//END	- SESSION }

//BEGIN - INCLUDE FILES {
$backwardseperator 	= "../../../../";
include_once ("{$backwardseperator}dataAccess/Connector.php");
include_once ("../../../../libraries/fpdf/fpdf.php");
include_once ("../../../../class/masterData/companies/cls_companies_get.php");
include_once ("../../../../class/finance/customer/advance/cls_advance_get.php");
include_once ("../../../../class/finance/cls_convert_amount_to_word.php");
include_once ("../../../../class/finance/cls_common_get.php");
//END	- INCLUDE FILES }

//BEGIN	- CREATE OBJECTS {
$obj_company_get			= new cls_companies_get($db);
$obj_advance_get			= new Cls_Advance_Get($db);
$obj_convert_amount_to_word	= new Cls_Convert_Amount_To_Word($db);
$obj_fin_com				= new Cls_Common_Get($db);
//END	- CREATE OBJECTS }

//BEGIN - PARAMETERS {

$serialNo 			= $_REQUEST['AdvanceNo'];
$serialYear 		= $_REQUEST['AdvanceYear'];

$title			= "ADVANCE PAYMENT RECEIPT";
$font			= 'Times';

//END	- PARAMETERS }
class PDF extends FPDF
{
	function Header1()
	{
		global $result_reportHeader;
		global $title;
		global $session_companyId;
		global $font;
		$h = 5;
		
		$this->Image('../../../../images/screenline.jpg',8,9);
		$this->SetFont($font,'B',14);$this->Cell(0,$h,$result_reportHeader["COMPANY_NAME"],'0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,$result_reportHeader["LOCATION_ADDRESS"].' '.$result_reportHeader["LOCATION_STREET"].' '.$result_reportHeader["LOCATION_CITY"].' '.$result_reportHeader["COMPANY_COUNTRY"].'.','0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,"Tel : ".$result_reportHeader["LOCATION_PHONE"].'  '."Fax : ".$result_reportHeader["LOCATION_FAX"],'0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,"E-mail : ".$result_reportHeader["LOCATION_EMAIL"].'  '."Web : ".$result_reportHeader["LOCATION_WEB"],'0',1,'C');
		$this->SetFont($font,'B',12);$this->Cell(0,10,$title,'0',1,'C');		
	}
	
	function SetPageWaterMark($status,$printStatus)
	{
		//Put the watermark
		switch($status)
		{
			case 1:
				$msg	= ($printStatus==1?"DUPLICATE":"");
				break;
		}
		$this->SetFont('Times','B',50);
		$this->SetTextColor(255,192,203);
		$this->RotatedText(35,190,$msg,45);
		$this->SetTextColor(0,0,0);
	}
	
	function RotatedText($x, $y, $txt, $angle)
	{
		//Text rotated around its origin
		$this->Rotate($angle,$x,$y);
		$this->Text($x,$y,$txt);
		$this->Rotate(0);
	}
		
	function ReportHeader($result_header,$result_reportHeader,$receiptNo)
	{
		$h = 5;
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Receipt No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$receiptNo,'0',1,'L');
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Receipt Date','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["ADVANCE_DATE"],'0',1,'L');
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Payment Method','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["PAYMENT_METHOD"],'0',1,'L');
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Currency','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["CURRENCY_NAME"],'0',1,'L');
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Reference No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["BANK_REFERENCE_NO"],'0',1,'L');
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Customer Information','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["CUSTOMER_NAME"],'0',1,'L');
		$this->SetX(52);
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["ADDRESS"],'0',1,'L');
		$this->SetX(52);
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["CITY"],'0',1,'L');
		$this->SetX(52);
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["COUNTRY_NAME"].'.','0',1,'L');		
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Narration','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->MultiCell(0,5,$result_header["REMARKS"],'0','',0);
	}
	
	function CreateTable($result_header,$result_details,$obj_convert_amount_to_word)
	{
		$this->Table_footer($result_header,$result_details,$obj_convert_amount_to_word);
	}	
	function Table_footer($result_header,$result_details,$obj_convert_amount_to_word)
	{
		$this->SetY($this->GetY());
		
		$this->SetFont('Times','B',10);$this->Cell(26,10,"Through",'LBT',0,'L');
		$this->Cell(4,10,":",'TB',0,'L');
		$this->SetFont('Times','',10);$this->Cell(140,10,"",'BTR',0,'C');
		$this->Cell(30,10,"",'1',0,'C');
		$this->Ln();
		
		$this->Cell(4,10,"",'LBT',0,'L');
		$this->Cell(166,10,$result_details["ACCOUNT_NAME"],'TB',0,'L');
		$this->Cell(30,10,"",'1',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','B',10);$this->Cell(26,10,"Amount(In Words)",'LBT',0,'L');
		$this->Cell(4,10,":",'TB',0,'L');
		$this->SetFont('Times','',10);$this->Cell(140,10,"",'BTR',0,'C');
		$this->Cell(30,10,"",'1',0,'C');
		$this->Ln();

		$this->Cell(4,10,"",'LBT',0,'L');
		$this->SetFont('Times','',8);
		$this->Cell(166,10,$obj_convert_amount_to_word->Convert_Amount($result_details["AMOUNT"],$result_header["CURRENCY_NAME"]),'0',0,'L');
		$this->Cell(30,10,"",'1',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','B',10);
		$this->Cell(170,10,"Total",'1',0,'R');
		$this->Cell(30,10,number_format($result_details["AMOUNT"],2),'1',0,'R');
		$this->Ln();
		
	}	
	function Footer1()
	{
		global $session_companyId;
		global $result_header;	
			
		$this->SetFont('Times','',8);
		
		$this->SetXY(20,$this->GetY()+10);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetX(140);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetXY(20,$this->GetY()+3);
		$this->Cell(50,5,'Authorized By',0,0,'C');
		
		$this->SetX(140);		
		$this->Cell(50,5,'Checked By',0,0,'C');	
		
		$this->SetXY(20,$this->GetY()+10);
		$this->Cell(50,5,$result_header["CREATED_BY_NAME"],0,0,'C');
		
		$this->SetXY(20,$this->GetY()+2);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetX(140);
		$this->Cell(50,5,'..........................................',0,0,'C');

		$this->SetXY(140,$this->GetY()+3);
		$this->Cell(50,5,'Received By',0,0,'C');

		$this->SetX(20);
		$this->Cell(50,5,'Prepared By',0,0,'C');	
	}
	
	function Footer()
	{
		$this->SetY(-5);
		$this->SetFont('Times','I',8);
		$this->Cell(0,2,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf 					= new PDF('P','mm','Letter');

$result_header 			= $obj_advance_get->getAdvanceReportHeader($serialNo,$serialYear);
$receiptNo				= $obj_fin_com->getSerialNo($result_header["ADVANCE_NO"],$result_header["ADVANCE_DATE"],$session_companyId,'RunQuery');
$result_details			= $obj_advance_get->getAdvanceReportDetails($serialNo,$serialYear);
$result_reportHeader	= $obj_company_get->GetCompanyReportHeader($result_header["LOCATION_ID"]);

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetPageWaterMark($result_header["STATUS"],$result_header["PRINT_STATUS"]);
$pdf->Header1();
$pdf->ReportHeader($result_header,$result_reportHeader,$receiptNo);
$pdf->CreateTable($result_header,$result_details,$obj_convert_amount_to_word);
$pdf->Footer1();
$obj_advance_get->UpdateStatus($serialNo,$serialYear);
$pdf->Output('rpt_advance_pdf.pdf','I');

?>
var basePath	= "presentation/finance_new/customer/advance/";
var reportId	= 899;
var menuId		= 702;
$(document).ready(function(index, element) {
	
	$('#frmAdvancePayment #cboCustomer').focus();
	$('#frmAdvancePayment #cboCustomer').die('change').live('change',LoadWhenChangeCustomer);	
	$('#frmAdvancePayment .cls_load_details').die('change').live('change',LoadWhenChangeOrderNo);	
	$('#frmAdvancePayment #tblPODetails input').die('keyup').live('keyup',ValidateTextBox);
	$('#frmAdvancePayment #tblMain .cls_GLAmount').die('keyup').live('keyup',ValidateTextBox);
	$('#frmAdvancePayment #tblPODetails .cls_receiving').die('keyup').live('keyup',CalculateTotals);
	$('#frmAdvancePayment #tblMain .cls_GLAmount').die('keyup').live('keyup',CalculateGLTotals);
	$('#frmAdvancePayment #butSave').die('click').live('click',Save);
	$('#frmAdvancePayment #cls_addNewGL').die('click').live('click',AddNewGL);
	$('#frmAdvancePayment #butReport').die('click').live('click',Report);
	$('#frmAdvancePayment #butNew').die('click').live('click',New);
	$('#frmAdvancePayment #butCancel').die('click').live('click',Cancel);
	
	$('#frmAdvancePayment .butSaveformError').die('click').live('click',function(){
		$(this).validationEngine('hide');
	});
	
	$('#frmAdvancePayment #tblMain .removeRow').die('click').live('click',function(){
		if($(this).parent().parent().attr('class')=="")
		 	$(this).parent().parent().remove();
		CalculateGLTotals();
	});	
	
	if(customerIdAL!='')
		LoadAutoData(customerIdAL,currencyIdAL);
});

function LoadWhenChangeCustomer()
{
	var url 	= basePath+"advance_db.php?RequestType=URLLoadWhenChangeCustomer";
	var data 	= "CustomerId="+$('#frmAdvancePayment #cboCustomer').val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		ClearObjects();
		$('#frmAdvancePayment #cboCurrency').html(json.Details);
		$('#frmAdvancePayment #cboInvoiceType').val(json.INVOICE_TYPE);
		$('#frmAdvancePayment #cboCustomer').focus();	
		LoadWhenChangeOrderNo();
		GetCommonExchangeRate();	
	}
	});		
}

function LoadWhenChangeOrderNo()
{
	showWaiting();
	
	var url 	= basePath+"advance_db.php?RequestType=URLLoadWhenChangeOrderNo";
	var data 	= "CustomerId="+$('#frmAdvancePayment #cboCustomer').val();
		data   += "&CurrencyId="+$('#frmAdvancePayment #cboCurrency').val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		if(json==null)
			hideWaiting();
					
		$('#frmAdvancePayment #cboPaymentMethod').val(json.PAY_METHOD);

		$('#frmAdvancePayment #tblPODetails .cls_td_orderAmount').html(RoundNumber(json.ORDER_AMOUNT,2));
		$('#frmAdvancePayment #tblPODetails .cls_td_advancedAmount').html(RoundNumber(json.ADVANCED_AMOUNT,2));
		$('#frmAdvancePayment #tblPODetails .cls_td_creditNotes').html(RoundNumber(json.CREDIT_AMOUNT,2));
		$('#frmAdvancePayment #tblPODetails .cls_td_debitNotes').html(RoundNumber(json.DEBIT_AMOUNT,2));
		$('#frmAdvancePayment #tblPODetails .cls_td_paymentReceived').html(RoundNumber(json.PAID_AMOUNT,2));
		$('#frmAdvancePayment #tblPODetails .cls_td_balanceToReceive').html(RoundNumber(json.BALANCE_TO_RECEIVE,2));
		$('#frmAdvancePayment #tblPODetails .cls_td_receiving').children().val(RoundNumber(json.BALANCE_TO_RECEIVE,2));
		
		$('#frmAdvancePayment #tblPODetails .cls_receiving').keyup();
	},
	error:function(xhr,status)
	{
		hideWaiting();
	}
	});	
	hideWaiting();
}

function ValidateTextBox()
{
	if($(this).val()=="" || isNaN($(this).val())){
		$(this).val(0);
		$(this).select();
	}
}

function CalculateTotals()
{
	var totals = 0.00;
	var receivingAmount 	= parseFloat($('#frmAdvancePayment #tblPODetails .cls_td_receiving').children().val());
	var receivingBalance 	= parseFloat($('#frmAdvancePayment #tblPODetails .cls_td_balanceToReceive').html());
	totals					= receivingAmount;
	
	if(receivingBalance<receivingAmount){
		totals	= receivingBalance;
		$(this).val(receivingBalance);
		$(this).focus();
	}
		
	$('#frmAdvancePayment #td_totalAdvanceAmount').html(RoundNumber(totals,2));
}

function CalculateGLTotals()
{
	var total	= 0;
	$('#frmAdvancePayment #tblMain .cls_GLAmount').each(function() {
    	total  += parseFloat($(this).val());
    });
	total	= RoundNumber(total,2,true);
	$('#frmAdvancePayment #td_totalGLAlloAmount').html(total);
}

function Save()
{
	if(!IsProcessMonthLocked_date($('#frmAdvancePayment #txtInvoiceDate').val()))
		return;
		
	if(!Validation_Save()){		
		$('frmAdvancePayment').validationEngine('hide');
		return;
	}
	
	showWaiting();
	var detail_array		= "[";
	$('#frmAdvancePayment #tblMain .cls_td_Del').each(function(){
			detail_array += "{";
			detail_array += '"GLAccountNo":"'+$(this).parent().find('.cls_td_GL').children().val()+'",';
			detail_array += '"Amount":"'+$(this).parent().find('.cls_td_GLAmount').children().val()+'",';
			detail_array += '"Remarks":"'+$(this).parent().find('.cls_td_remarks').children().val()+'",';
			detail_array += '"CostCenter":"'+$(this).parent().find('.cls_td_costCenter').children().val()+'"';
			detail_array +=  '},';
	});
	detail_array 		 = detail_array.substr(0,detail_array.length-1);
	detail_array 		+= " ]";

	var url 	= basePath+"advance_db.php?RequestType=URLSave";
	var data 	= "CustomerId="+$('#frmAdvancePayment #cboCustomer').val();
		data   += "&CurrencyId="+$('#frmAdvancePayment #cboCurrency').val();
		data   += "&PayMethod="+$('#frmAdvancePayment #cboPaymentMethod').val();
		data   += "&LedgerAccount=null";
		data   += "&Remarks="+$('#frmAdvancePayment #txtRemarks').val();
		data   += "&InvoiceDate="+$('#frmAdvancePayment #txtInvoiceDate').val();
		data   += "&BankReferenceNo="+$('#frmAdvancePayment #txtBankRefNo').val();
		data   += "&ReceivingAmount="+$('#frmAdvancePayment #tblPODetails .cls_receiving').val();
		data   += "&AdvanceAmount="+$('#frmAdvancePayment #td_totalAdvanceAmount').html();
		data   += "&InvoiceType="+$('#frmAdvancePayment #cboInvoiceType').val();
		data   += "&Detail_Array="+detail_array;
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
		{
			$('#frmAdvancePayment #butSave').validationEngine('showPrompt', json.msg,json.type );
			if(json.type == 'pass')
			{				
				$('#frmAdvancePayment #txtAdvanceNo').val(json.Advance_No);
				$('#frmAdvancePayment #txtAdvanceYear').val(json.Advance_Year);
				$('#frmAdvancePayment #butSave').hide('slow');
				hideWaiting();
			}
			else
			{
				hideWaiting();
			}
		},
	error:function(xhr,status)
		{
			$('#frmAdvancePayment #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			hideWaiting();
		}
	});
	hideWaiting();
}

function Validation_Save()
{
	var totalAdvance	= parseFloat($('#frmAdvancePayment #td_totalAdvanceAmount').html());
	var totalGL			=  parseFloat($('#frmAdvancePayment #td_totalGLAlloAmount').html());
	
	if(!$('#frmAdvancePayment').validationEngine('validate')){
		return false;
	}
	
	if(totalAdvance<=0)
	{
		alert('Advance amount should be greater than zero.');
		return false;
	}
	
	if(totalAdvance != totalGL)
	{
		alert("'Total Advance Amount' and 'Total GL Allocated Amount' should be equal.");
		return false;
	}
	return true;
}

function AddNewGL()
{
	if(!$('#frmAdvancePayment #tblMain').validationEngine('validate')){
		setTimeout("alertx('')",2000);	
		return false;
	}
	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);	
	row.innerHTML	= $('#frmAdvancePayment #tblMain .firstRow').html();
}

function Report()
{
	if($('#frmAdvancePayment #txtAdvanceNo').val()=="")
	{
		alert("No details available to view report.");
		return;
	}
	var url  = "?q="+reportId+"&AdvanceNo="+$('#frmAdvancePayment #txtAdvanceNo').val()
	    url += "&AdvanceYear="+$('#frmAdvancePayment #txtAdvanceYear').val();
	window.open(url,'rptadvance.php');
}

function New()
{
	window.location.href = '?q='+menuId;
}

function ClearObjects()
{
	$("#frmAdvancePayment #cboCurrency").html('');
	$("#frmAdvancePayment #txtExchangeRate").val('');
	$("#frmAdvancePayment #cboPaymentMethod").val('');
	$("#frmAdvancePayment #tblPODetails .cls_td_orderAmount").html(0);
	$("#frmAdvancePayment #tblPODetails .cls_td_advancedAmount").html(0);
	$("#frmAdvancePayment #tblPODetails .cls_td_creditNotes").html(0);
	$("#frmAdvancePayment #tblPODetails .cls_td_debitNotes").html(0);
	$("#frmAdvancePayment #tblPODetails .cls_td_paymentReceived").html(0);
	$("#frmAdvancePayment #tblPODetails .cls_td_balanceToReceive").html(0);
	$("#frmAdvancePayment #tblPODetails .cls_td_receiving").children().val(0);
	
	$("#frmAdvancePayment #tblMain .cls_td_GL").children().val('');
	$("#frmAdvancePayment #tblMain .cls_td_GLAmount").children().val(0);
	$("#frmAdvancePayment #tblMain .cls_td_remarks").children().val('');
	$("#frmAdvancePayment #tblMain .cls_td_costCenter").children().val('');
	
	$('#frmAdvancePayment #tblMain .removeRow').each(function(){
		if($(this).parent().parent().attr('class')=="")
		 	$(this).parent().parent().remove();
		CalculateGLTotals();
	});	
	
	CalculateTotals();
	CalculateGLTotals();
}

function Cancel()
{
	if($('#frmAdvancePayment #txtAdvanceNo').val()=="")
	{
		var msg = "No details appear to 'Cancel'.";
		$('#frmAdvancePayment #butCancel').validationEngine('showPrompt',msg,'fail');
		setTimeout("alertx('#butCancel')",2000);	
		return;
	}
	
	if(!confirm("Are you sure you want to cancel selected Advance Payment No :"+$('#frmAdvancePayment #txtAdvanceNo').val()+"/"+$('#frmAdvancePayment #txtAdvanceYear').val()+"."))
		return;
		
	var url 	= basePath+"advance_db.php?RequestType=URLCancel";
	var data 	= "AdvanceNo="+$('#frmAdvancePayment #txtAdvanceNo').val();
	    data   += "&AdvanceYear="+$('#frmAdvancePayment #txtAdvanceYear').val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		$('#frmAdvancePayment #butCancel').validationEngine('showPrompt',json.msg,json.type );
	}
	});
	var t = setTimeout("alertx('#butCancel')",2000);	
}

function alertx(but)
{
	$('#frmAdvancePayment '+but).validationEngine('hide')	;
}

function LoadAutoData(customerId,currencyId)
{
	$('#frmAdvancePayment #cboCustomer').val(customerId);
	$('#frmAdvancePayment #cboCustomer').change();
	$('#frmAdvancePayment #cboCurrency').val(currencyId);
	$('#frmAdvancePayment #cboCurrency').change();
}
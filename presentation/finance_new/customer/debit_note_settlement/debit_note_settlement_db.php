<?php
session_start();
ini_set('max_execution_time',600000);
//ini_set('display_errors',1);
//BEGIN - INCLUDE FILES {
include  "../../../../dataAccess/Connector.php";
include  "../../../../class/finance/customer/debit_receive/cls_debit_receive_get.php";
include  "../../../../class/finance/customer/debit_receive/cls_debit_receive_set.php";
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {
$obj_debit_receive_get		= new Cls_Debit_Receive_Get($db);
$obj_debit_receive_set		= new Cls_Debit_Receive_Set($db);
//END 	- CREATE OBJECTS }

$requestType		= $_REQUEST["RequestType"];

if($requestType=="URLLoadCustomerChange")
{
	$customerId		= $_REQUEST["CustomerId"];
	echo $obj_debit_receive_get->LoadCustomerChange($customerId);
}

if($requestType=="URLLoadMainDetails")
{
	$customerId		= $_REQUEST["CustomerId"];
	$currencyId  	= $_REQUEST["CurrencyId"];
	
	echo $obj_debit_receive_get->LoadMainDetails($customerId,$currencyId);
}

if($requestType=="URLSave")
{
	$customerId		= $_REQUEST["CustomerId"];
	$currencyId  	= $_REQUEST["CurrencyId"];
	$remarks		= $_REQUEST["Remarks"];
	$receiptDate	= $_REQUEST["ReceiptDate"];
	$paymentMode	= $_REQUEST["PaymentMode"];
	$detailArray	= json_decode($_REQUEST["DetailArray"],true);
	$glArray		= json_decode($_REQUEST["GLArray"],true);
	
	echo $obj_debit_receive_set->Save($customerId,$currencyId,$remarks,$detailArray,$glArray,$receiptDate,$paymentMode);
}

if($requestType=="URLWhenChangeGL")
{
	$GLID			= $_REQUEST["GLID"];
	echo $obj_debit_receive_get->WhenChangeGL($GLID);
}

if($requestType=="URLCancel")
{
	$receiptNo			= $_REQUEST["ReceiptNo"];
	$receiptYear		= $_REQUEST["ReceiptYear"];
	
	$response		= $obj_debit_receive_get->ValidateBeforeCancel($receiptNo,$receiptYear);
	
	if($response["type"]=='false')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_debit_receive_set->Cancel($receiptNo,$receiptYear);
}

if($requestType=="confirmPayment")
{
	$savedStatus	= true;
	$savedMsg		= '';
	$errorSql		= '';
	
	$arr = json_decode($_REQUEST['confirmDetails'], true);
	
	$db->begin();
	
	foreach($arr as $arrVal)
	{
		$receiptNo 	   = $arrVal['receiptNo'];
		$receiptYear   = $arrVal['receiptYear'];
		
		$dataArr	= $obj_payment_receive_set->confirmPaymnet($receiptNo,$receiptYear);	
		if($dataArr['savedStatus']=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMsg 		= $dataArr['savedMassege'];
			$errorSql 		= $dataArr['error_sql'];
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$invReportId	= 894;
$debReportId	= 928;

include_once "class/finance/cls_convert_amount_to_word.php";
require_once "class/finance/cls_common_get.php";

$mainPath 			= $_SESSION['mainPath'];
$locationId 		= $_SESSION['CompanyID'];

//BEGIN - 
$obj_fin_com		= new Cls_Common_Get($db); 
$obj_convert_amount_to_word	= new Cls_Convert_Amount_To_Word($db);
//END	- 
$serialNo			= $_REQUEST["SerialNo"];
$serialYear			= $_REQUEST["SerialYear"];

$header_array 		= GetHeaderDetails($serialYear,$serialNo);
$detail_result 		= GetGridDetails($serialYear,$serialNo);
$gl_result 			= GetGLGridDetails($serialYear,$serialNo);

if(!isset($_REQUEST["SerialNo"]))
	$invoiceDate	= date("Y-m-d");
else
	$invoiceDate	= $header_array["RECEIPT_DATE"];
	
$company_Id			= $header_array["COMPANY_ID"];
?>
<head>

<title>Debit Receipt Report</title>

<style type="text/css">
.apDiv1 {position:absolute;left:380px;top:100px;width:auto;height:auto;z-index:0;opacity:0.1;}
</style>
</head>
<body>
<div id="partPay" class="apDiv1 <?php echo $header_array["STATUS"]=='10'?'maskShow':'maskHide'?>"><img src="images/cancelled.png"  /></div>
<form id="frmSalesInvoice" name="frmSalesInvoice" method="post">
  <table width="900" align="center">
    <tr>
      <td><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
      <td style="text-align:center" class="reportHeader">Debit Receipt</td>
    </tr>
    <tr>
      <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td width="14%">Receipt No</td>
            <td width="1%">:</td>
            <td><?php echo $header_array["RECEIPT_NO"]."/".$header_array["RECEIPT_YEAR"]; ?></td>
            <td width="11%">Receipt Date</td>
            <td width="1%">:</td>
            <td width="34%"><?php echo $header_array["RECEIPT_DATE"]?></td>
          </tr>
          <tr>
            <td>Payment Method</td>
            <td>:</td>
            <td><?php echo $header_array["PAYMENT_MODE"]?></td>
            <td>Currency</td>
            <td>:</td>
            <td><?php echo $header_array["CURRENCY_CODE"]?></td>
          </tr>
          <tr>
            <td>Received From</td>
            <td>:</td>
            <td><?php echo $header_array["CUSTOMER_NAME"]?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>Remarks</td>
            <td>:</td>
            <td><?php echo $header_array["REMARKS"]?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td ><table width="100%%" border="0" class="rptBordered" id="tblMain">
          <thead>
            <tr>
              <th width="3%">&nbsp;</th>
              <th width="77%">Debit No</th>
              <th width="20%">Amount</th>
            </tr>
          </thead>
          <tbody>
            <?php
$total_value	= 0;
$i				= 0;
while($row = mysqli_fetch_array($detail_result))
{ 
	$debit_url	= "<a target=\"rptdebit_note.php\" href=\"?q=".$debReportId."&debitNoteNo=".$row["DEBIT_NO"]."&debitNoteYear=".$row["DEBIT_YEAR"]."\">".$row["DEBIT_NO"].'/'.$row["DEBIT_YEAR"]."</a>";

?>
            <tr>
              <td class="cls_td_check" align="center"><?php echo ++$i.'.';?></td>
              <td class="cls_td_salesOrderNo"><?php echo $debit_url?></a></td>
              <td width="20%" style="text-align:right"><?php echo number_format($row["PAY_AMOUNT"],2)?></td>
            </tr>
            <?php
	$total_value 	+= round($row["PAY_AMOUNT"],2);
}
?>
            <tr style="font-weight:bold">
              <td colspan="2" align="center" class="cls_td_check">TOTAL</td>
              <td width="20%" style="text-align:right;border-top-style:groove;border-bottom-style:double"><?php echo number_format($total_value,2)?></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td class="normalfnt"><b><?php echo $obj_convert_amount_to_word->Convert_Amount($total_value,$header_array["CURRENCY_CODE"])?></b></td>
    </tr>
    <tr>
      <td class="normalfnt"><table width="100%%" border="0" class="rptBordered" id="tblMain2">
          <thead>
            <tr>
              <th width="26%">Account Name</th>
              <th width="22%">Cost Center</th>
              <th width="32%">Remarks</th>
              <th width="10%">Debit</th>
              <th width="10%">Credit</th>
            </tr>
          </thead>
          <tbody>
            <?php
$total_value	= 0;
$totDrGlAmount	= 0;
$totCrGlAmount	= 0;
while($row = mysqli_fetch_array($gl_result))
{ 
?>
            <tr>
              <td style="text-align:left" nowrap="nowrap"><?php echo $row["GL_NAME"]?></td>
              <td style="text-align:left"><?php echo $row["COST_CENTER"]?></td>
              <td style="text-align:left"><?php echo $row["REMARKS"]?></td>
              <td style="text-align:right"><?php echo ($row["DEBIT_AMOUNT"]=='0'?'&nbsp;':number_format($row["DEBIT_AMOUNT"],2))?></td>
              <td style="text-align:right"><?php echo ($row["CREDIT_AMOUNT"]=='0'?'&nbsp;':number_format($row["CREDIT_AMOUNT"],2))?></td>
            </tr>
            <?php
			$totDrGlAmount	+= round($row["DEBIT_AMOUNT"],2);
			$totCrGlAmount	+= round($row["CREDIT_AMOUNT"],2);
}
?>
            <tr style="font-weight:bold">
              <td colspan="3" style="text-align:center">TOTAL</td>
              <td style="text-align:right;border-top-style:groove;border-bottom-style:double"><?php echo number_format($totDrGlAmount,2)?></td>
              <td style="text-align:right;border-top-style:groove;border-bottom-style:double"><?php echo number_format($totCrGlAmount,2)?></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td class="normalfnt">&nbsp;</td>
    </tr>
    <tr>
      <td >&nbsp;</td>
    </tr>
  </table>
</form>
</body>
<?php
function GetHeaderDetails($serialYear,$serialNo)
{
	global $db;
	
	$sql = "SELECT 
				CDRH.RECEIPT_NO 		AS RECEIPT_NO,
				CDRH.RECEIPT_YEAR 		AS RECEIPT_YEAR,
				CU.intId 				AS CUSTOMER_ID,
				CU.strName 				AS CUSTOMER_NAME,
				CURR.intId				AS CURRENCY_ID,
				CURR.strCode			AS CURRENCY_CODE,
				CDRH.REMARKS			AS REMARKS,
				PM.strName				AS PAYMENT_MODE,
				CDRH.RECEIPT_DATE		AS RECEIPT_DATE,
				CDRH.STATUS				AS STATUS
			FROM finance_customer_debit_receive_header CDRH
			INNER JOIN mst_financepaymentsmethods PM ON PM.intId = CDRH.PAYMENT_MODE
			INNER JOIN mst_customer CU ON CU.intId = CDRH.CUSTOMER_ID
			INNER JOIN mst_financecurrency CURR ON CURR.intId = CDRH.CURRENCY_ID
			WHERE CDRH.RECEIPT_NO = '$serialNo' AND 
			CDRH.RECEIPT_YEAR = '$serialYear' ";
			
			
				
	$result = $db->RunQuery($sql);
	$header_array = mysqli_fetch_array($result);
	return $header_array;
}

function GetGridDetails($serialYear,$serialNo)
{
	global $db;
	
	$sql = "SELECT DEBIT_NO,
			DEBIT_YEAR,
			PAY_AMOUNT
			FROM
			finance_customer_debit_receive_details
			WHERE
			RECEIPT_NO = '$serialNo' AND
			RECEIPT_YEAR = '$serialYear' ";
	return $db->RunQuery($sql);
}

function GetGLGridDetails($serialYear,$serialNo)
{
	global $db;
	
	$sql = "SELECT
			  CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE,' - ',FCOA.CHART_OF_ACCOUNT_NAME) AS GL_NAME,
			  GL.REMARKS							AS REMARKS,
			  D.strName								AS COST_CENTER,
			  IF(GL.TRANSACTION_TYPE='D',GL.PAY_AMOUNT,0) 						AS DEBIT_AMOUNT,
			  IF(GL.TRANSACTION_TYPE='C',GL.PAY_AMOUNT,0) 						AS CREDIT_AMOUNT
			FROM finance_customer_debit_receive_gl GL
			INNER JOIN finance_mst_chartofaccount FCOA
			  ON FCOA.CHART_OF_ACCOUNT_ID = GL.LEDGER_ID
			INNER JOIN finance_mst_account_sub_type FMST 
			  ON FMST.SUB_TYPE_ID = FCOA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type FMMT 
			  ON FMMT.MAIN_TYPE_ID = FMST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_type FMT 
			  ON FMT.FINANCE_TYPE_ID = FMMT.FINANCE_TYPE_ID
			INNER JOIN mst_financedimension D
			  ON D.intId = GL.COST_CENTER_ID
			WHERE RECEIPT_NO = '$serialNo'
				AND RECEIPT_YEAR = '$serialYear'";
	return $db->RunQuery($sql);
}
?>
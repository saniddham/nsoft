var basePath		= "presentation/finance_new/customer/debit_note_settlement/";
var settleMenuId	= 722;
var reportId		= 1174;
var menuId			= 1163;
$(document).each(function(index, element){
	
	$('#frmDebitReceive #cboCustomer').focus();
	$('#frmDebitReceive #cboCustomer').die('change').live('change',LoadCustomerChange);
	$('#frmDebitReceive .cls_load_details').die('change').live('change',LoadMainDetails);
	$('#frmDebitReceive #tblMain .cls_th_checkAll').die('click').live('click',CheckAll);
	$('#frmDebitReceive #cls_addNewGL').die('click').live('click',AddNewGL);
	$('#frmDebitReceive #butSave').die('click').live('click',Save);
	$('#frmDebitReceive #butNew').die('click').live('click',New);
	$('#frmDebitReceive #butCancel').die('click').live('click',Cancel);
	$('#frmDebitReceive #butReport').die('click').live('click',Report);
	
	//$('#frmPaymentReceive #hyper_settle').die('click').live('click',SettleReport);
	$('#frmDebitReceive #tblGL #cboGL').die('change').live('change',WhenChangeGL);
	//$('#butRConfirm').die('click').live('click',confirmPayment);
	
	$('#frmDebitReceive #tblMain .cls_check').die('click').live('click',function(){
		ValidateRowWhenCheck($(this));
	});
	
	$('#frmDebitReceive #tblMain .cls_td_payingAmount').die('keyup').live('keyup',function(){ CalculateTotals(); });
	
	$('#frmDebitReceive #tblGL .cls_GLAmount').die('keyup').live('keyup',function(){ CalculateGLTotals(); });

	$('#frmDebitReceive #tblGL .removeRow').die('click').live('click',function(){
		
		if($(this).parent().parent().prop('class')=="")
			$(this).parent().parent().remove(); 	
		CalculateGLTotals();
	});
	
	$('#frmDebitReceive #tblMain input').die('keyup').live('keyup',function(){
		 if($(this).val()=="" || isNaN($(this).val())){
				$(this).val(0);
				$(this).select();
			}
	});
	
	$('#frmDebitReceive #tblGL .cls_cbo_numberOnly').die('keyup').live('keyup',function(){
		 if($(this).val()=="" || isNaN($(this).val())){
				$(this).val(0);
				$(this).select();
			}
	});	
	
	/*if(customerIdAL!='')
		LoadAutoData(customerIdAL,currencyIdAL);*/
});

function New()
{
	window.location.href = '?q='+menuId;
}

function LoadCustomerChange()
{	
	var url 	= basePath+"debit_note_settlement_db.php?RequestType=URLLoadCustomerChange";
	var data 	= "CustomerId="+$('#frmDebitReceive #cboCustomer').val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		$('#frmDebitReceive #cboPaymentsMethods').val(json.PayMethod);
	}
	});
}

function LoadMainDetails()
{
	var customerId 	= $('#frmDebitReceive #cboCustomer').val();
	var currencyId	= $('#frmDebitReceive #cboCurrency').val();
	
	if(customerId=='' || currencyId=='')
	{
		ClearObjects();
		return;
	}
		
	ClearObjects();
	showWaiting();
	var url 	= basePath+"debit_note_settlement_db.php?RequestType=URLLoadMainDetails";
	var data 	= "CustomerId="+customerId;
		 data  += "&CurrencyId="+currencyId;
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		if(json.GRID == null)
		{
			hideWaiting();
		}		
		else
		{
			$('#frmDebitReceive #tblMain tbody').html(json.GRID);
			//CreateGridLastRow();
			
			var gl_data		= json.GL_GRID;
			
			for(var i=0;i<json.GL_GRID.length;i++)
			{
				$('#frmDebitReceive #tblGL tbody tr:last').after("<tr class=\"cls_tr_autoAdd\">"+$('#frmDebitReceive #tblGL .cls_tr_firstRow').html()+"</tr>");
				$('#frmDebitReceive #tblGL tr:eq(3)').find('.cls_td_GL').children().html(gl_data[i]['GL_HTML']);
				$('#frmDebitReceive #tblGL tr:eq(3)').find('.cls_td_GL').addClass('cls_td_autoAdd');
				$('#frmDebitReceive #tblGL tr:eq(3)').find('.cls_td_GL').attr('title','C');
				$('#frmDebitReceive #tblGL tr:eq(3)').find('.cls_td_glAmount_cr').children().addClass('cls_txt_autoAdd_cr');
				$('#frmDebitReceive #tblGL tr:eq(3)').find('.cls_td_glAmount_cr').children().attr('disabled',false);
				$('#frmDebitReceive #tblGL tr:eq(3)').find('.cls_td_glAmount_dr').children().attr('disabled',true);			
			}
		}
		hideWaiting();
	}
	});
	
	hideWaiting();
}

function ValidateRowWhenCheck(obj)
{
	if(obj.is(':checked'))
	{
		var toBePaidValue = parseFloat(obj.parent().parent().find('.cls_td_toBePaid').html());
		obj.parent().parent().find('.cls_td_payingAmount').children().val(toBePaidValue);
		obj.parent().parent().find('.cls_td_payingAmount').children().removeAttr('disabled');;
	}
	else
	{
		obj.parent().parent().find('.cls_td_payingAmount').children().val(0);
		obj.parent().parent().find('.cls_td_payingAmount').children().attr('disabled','disabled');;
	}
	CalculateTotals();
}

function CalculateTotals()
{
	var totalPaying	= 0;
	$('#frmDebitReceive #tblMain .cls_td_payingAmount').each(function(index, element) {
    	if($(this).parent().find('.cls_td_check').children().is(':checked')){
			var toBePaid		= parseFloat($(this).parent().find('.cls_td_toBePaid').html());
			var payingAmount	= parseFloat($(this).children().val());
			
			/*BEGIN - COMMENTER THIS LINE BECUASE USER MUST ABLE TO ENTER MORE THAN INVOICE VALUE & SYSTEM WILL RAISE PAYMENT ADVANCE FOR EXCEEDED VALUE AUTOMATICALLY {
				if(toBePaid<payingAmount)
				$(this).children().val(toBePaid);
			  END   - }*/
			
			totalPaying 	+= parseFloat($(this).children().val());
		}			
    });
	totalPaying	= RoundNumber(totalPaying,2,true);
	$('#frmDebitReceive #tblMain .cls_td_total_toBePaid').html(totalPaying);
	$('#frmDebitReceive #td_totalPayingAmount').html(totalPaying);
}

function CalculateGLTotals()
{
	var total	= 0;
	$('#frmDebitReceive #tblGL tbody .cls_td_glAmount_dr').each(function(){
    		total	+= parseFloat($(this).children().val());
    });
	total	= RoundNumber(total,2,true);
	$('#frmDebitReceive #tblGL tfoot .cls_td_totAmount_dr').html(total);
	
	var total	= 0;
	$('#frmDebitReceive #tblGL tbody .cls_td_glAmount_cr').each(function(){
    		total	+= parseFloat($(this).children().val());
    });
	total	= RoundNumber(total,2,true);
	$('#frmDebitReceive #tblGL tfoot .cls_td_totAmount_cr').html(total);
	
	var total	= 0;
	$('#frmDebitReceive #tblGL .cls_td_GLAmount').each(function(){
		if($(this).parent().find('.cls_td_GL').attr('title')=='D')
    		total	+= parseFloat($(this).children().val());
    });
	total	= RoundNumber(total,2,true);
	$('#frmDebitReceive #td_totalGLAlloAmount').html(total);
}

function AddNewGL()
{
	if(!$('#frmDebitReceive').validationEngine('validate')){
		setTimeout("HideValidation('#frmDebitReceive','')",2000);	
		return false;
	}
	$('#frmDebitReceive #tblGL tbody tr:last').after("<tr>"+$('#frmDebitReceive #tblGL .cls_tr_firstRow').html()+"</tr>");
}

function Save()
{
	if(!IsProcessMonthLocked_date($('#frmDebitReceive #txtReceiptDate').val()))
		return;
		
	if(!Validation_Save()){
		setTimeout("HideValidation('#frmDebitReceive','')",4000);	
		return;
	}
	
	showWaiting();
	var detail_array		= "[";
	$('#frmDebitReceive #tblMain .cls_td_check').children(':checked').each(function(){
			detail_array += "{";
			detail_array += '"debitNo":"'+$(this).parent().parent().find('.cls_td_debitNo').children().attr('id')+'",';
			detail_array += '"ToBeReceive":"'+$(this).parent().parent().find('.cls_td_toBePaid').html()+'",';
			detail_array += '"PayAmount":"'+$(this).parent().parent().find('.cls_td_payingAmount').children().val()+'"';
			detail_array +=  '},';
	});

	detail_array 		 = detail_array.substr(0,detail_array.length-1);
	detail_array 		+= " ]";
	
	var gl_Array		= "[";
	$('#frmDebitReceive #tblGL .cls_td_GL').each(function(){
			gl_Array += "{";
			gl_Array += '"GLAccount":"'+$(this).parent().find('.cls_td_GL').children().val()+'",';
			gl_Array += '"GLValue_DR":"'+$(this).parent().find('.cls_td_glAmount_dr').children().val()+'",';
			gl_Array += '"GLValue_CR":"'+$(this).parent().find('.cls_td_glAmount_cr').children().val()+'",';
			gl_Array += '"GLRemarks":"'+$(this).parent().find('.cls_td_remarks').children().val()+'",';
			gl_Array += '"GLCostCenter":"'+$(this).parent().find('.cls_td_costCenter').children().val()+'",';
			gl_Array += '"TransType":"'+$(this).parent().find('.cls_td_GL').attr('title')+'"';
			gl_Array +=  '},';
	});

	gl_Array 		 = gl_Array.substr(0,gl_Array.length-1);
	gl_Array 		+= " ]";

	var url 	= basePath+"debit_note_settlement_db.php?RequestType=URLSave";
	var data 	= "CustomerId="+$('#frmDebitReceive #cboCustomer').val();
		data   += "&CurrencyId="+$('#frmDebitReceive #cboCurrency').val();
		data   += "&Remarks="+$('#frmDebitReceive #txtRemarks').val();
		data   += "&ReceiptDate="+$('#frmDebitReceive #txtReceiptDate').val();
		data   += "&PaymentMode="+$('#frmDebitReceive #cboPaymentsMethods').val();
		data   += "&DetailArray="+detail_array;
		data   += "&GLArray="+gl_Array;
	
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type: "POST",
	async:false,
	success:function(json)
		{
			$('#frmDebitReceive #butSave').validationEngine('showPrompt', json.msg,json.type);
			if(json.type == 'pass')
			{
				$('#frmDebitReceive #txtReceiptNo').val(json.ReceiptNo);
				$('#frmDebitReceive #txtReceiptYear').val(json.ReceiptYear);
				$('#frmDebitReceive #butSave').hide('slow');
				$('#frmDebitReceive #butCancel').hide('slow');
				hideWaiting();
			}
			else
			{
				hideWaiting();
			}
		},
	error:function(xhr,status)
		{
			$('#frmDebitReceive #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			hideWaiting();
		}
	});
	hideWaiting();
}

function Validation_Save()
{
	var totalPaying 		= parseFloat($('#frmDebitReceive #td_totalPayingAmount').html());
	var totalGL				= parseFloat($('#frmDebitReceive #td_totalGLAlloAmount').html());
	var tot_debitAmount		= parseFloat($('#frmDebitReceive #tblGL tfoot .cls_td_totAmount_dr').html())
	var tot_creditAmount	= parseFloat($('#frmDebitReceive #tblGL tfoot .cls_td_totAmount_cr').html())
	var customerGLAmount	= parseFloat($('#frmDebitReceive #tblGL tbody .cls_txt_autoAdd_cr').val());
		
	if(!$('#frmDebitReceive').validationEngine('validate'))
		return false;
	
	if($('#frmDebitReceive #txtReceiptNo').val()!="")
	{
		alert("Receipt No already saved.");
		return false;
	}
	
	if(totalPaying<=0)
	{
		alert('Total paying amount shold be more than zero.');
		return false;
	}
	
	if(customerGLAmount!=totalPaying)
	{
		alert("'Total Paying Amount' and 'Debtor Credit Amount' should be equal.");
		return false;
	}
	
	/*BEGIN - This if condition commented and add another if condition to validate credit debit balance only {.
	if(totalPaying != totalGL)
	{
		alert("'Total Receiving Amount' and 'Total GL Allocated Amount' should be equal.")
		return false;
	}
	END - This if condition commented and add another if condition to validate credit debit balance only.  } */
	if(tot_debitAmount != tot_creditAmount)
	{
		alert("'Credit Amount' and 'Debit Amount' should be equal.")
		return false;
	}
	
return true;
}

function Report()
{
	if($('#frmDebitReceive #txtReceiptNo').val()=="")
	{
		alert("No details available to view report.");
		return;
	}
	var url  = "?q="+reportId+"&SerialNo="+$('#frmDebitReceive #txtReceiptNo').val()
	    url += "&SerialYear="+$('#frmDebitReceive #txtReceiptYear').val();
	window.open(url,'rptdebit_note_settlement.php');
}

function CheckAll()
{
	if($(this).is(':checked'))
		$('#frmDebitReceive #tblMain .cls_check').attr('checked','checked');
	else
		$('#frmDebitReceive #tblMain .cls_check').removeAttr('checked');
		
	$('#frmDebitReceive #tblMain .cls_check').each(function(){
		ValidateRowWhenCheck($(this));
	});
}

function ClearObjects()
{
	$("#frmDebitReceive #tblMain tr:gt(0)").remove();
	$("#frmDebitReceive #tblGL tbody tr:gt(0)").remove();
	$('#frmDebitReceive #tblMain .cls_th_checkAll').removeAttr('checked');
	
	$("#frmDebitReceive #tblGL .cls_td_GL").children().val('');
	$("#frmDebitReceive #tblGL .cls_td_GLAmount").children().val(0);
	$("#frmDebitReceive #tblGL .cls_td_remarks").children().val('');
	$("#frmDebitReceive #tblGL .cls_td_costCenter").children().val('');
	
	$('#frmDebitReceive #tblGL .removeRow').each(function(){
		if($(this).parent().parent().attr('class')=="")
		 	$(this).parent().parent().remove();		
	});
	
	CalculateTotals();
	CalculateGLTotals();
}

/*function SettleReport()
{
	var url  = "?q="+settleMenuId;
		url += "&cboCustomer="+$('#frmPaymentReceive #cboCustomer').val(); 
		url += "&cboCurrency="+$('#frmPaymentReceive #cboCurrency').val();
	window.open(url,'advance_settlement.php');
}*/

/*function LoadAutoData(customerId,currencyId)
{
	$('#frmPaymentReceive #cboCustomer').val(customerId);
	LoadCustomerChange();
	$('#frmPaymentReceive #cboCurrency').val(currencyId);
	//LoadMainDetails();
	$('#frmPaymentReceive #cboCurrency').change();
}*/

function WhenChangeGL()
{
	var obj		= $(this);
	var url 	= basePath+"debit_note_settlement_db.php?RequestType=URLWhenChangeGL";
	var data 	= "GLID="+$(this).val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		obj.parent().parent().find('.cls_td_GL').attr('title',json.ADD);
		if(json.ADD=='D'){
			obj.parent().parent().find('.cls_td_glAmount_dr').children().val('0');
			obj.parent().parent().find('.cls_td_glAmount_cr').children().val('0');
			obj.parent().parent().find('.cls_td_glAmount_cr').children().attr('disabled',true);
			obj.parent().parent().find('.cls_td_glAmount_dr').children().attr('disabled',false);			
			obj.parent().parent().find('.cls_td_glAmount_dr').children().select();
		}else if(json.ADD=='C'){
			obj.parent().parent().find('.cls_td_glAmount_cr').children().val('0');
			obj.parent().parent().find('.cls_td_glAmount_dr').children().val('0');
			obj.parent().parent().find('.cls_td_glAmount_dr').children().attr('disabled',true);
			obj.parent().parent().find('.cls_td_glAmount_cr').children().attr('disabled',false);
     		obj.parent().parent().find('.cls_td_glAmount_cr').children().select();
			
		}
	}
	});
}

function Cancel()
{
	if($('#frmDebitReceive #txtReceiptNo').val()=="")
	{
		return;
	}
	var x = confirm("Are you sure you want to cancel Debit Received No : "+$('#frmDebitReceive #txtReceiptNo').val()+"/"+$('#frmDebitReceive #txtReceiptYear').val()+" .");
		if(!x)return;
	
	var url 	= basePath+"debit_note_settlement_db.php?RequestType=URLCancel";
	var data 	= "ReceiptNo="+$('#frmDebitReceive #txtReceiptNo').val();
	 	data   += "&ReceiptYear="+$('#frmDebitReceive #txtReceiptYear').val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		$('#frmDebitReceive #butCancel').validationEngine('showPrompt',json.msg,json.type );
	}
	});
	var t = setTimeout("HideValidation('#butCancel')",2000);
}

function HideValidation(form,button)
{
	$(form+' '+button).validationEngine('hide');
}
/*function confirmPayment()
{
	var x = confirm("Are you sure you want to Confirm this Payment/s.");
	if(!x)
		return;
					
	var data = "RequestType=confirmPayment";
	value = "[";
	$('.cls_chkConfirm').children().each(function(){
		
		if($(this).attr('checked'))
		{
			var receiptNo 	= $(this).parent().parent().find('.cls_receiptNo').html();
			var receiptYear	= $(this).parent().parent().find('.cls_receiptYear').html();
			
			value += "{";
			value += '"receiptNo":"'+ receiptNo +'",' ;
			value += '"receiptYear":"'+ receiptYear +'"' ;
			value += "},";
		}
	});
	value = value.substr(0,value.length-1);
	value += "]";
	
	data += "&confirmDetails="+value;
	
	var url = basePath+"payment_receive_db.php";
	var obj = $.ajax({
		url:url,
		type:'post',
		dataType:'json',  
		data:data,
		async:false,
		success:function(json){
				if(json.type=='pass')
				{
					alert("Confirmed Sucessfully");
					window.location.href = 'receipt_confermation.php';
				}
				else
				{
				alert(json.msg);
				}
			}	
		});
}*/
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$session_locationId	 	= $_SESSION["CompanyID"];
$session_companyId 		= $_SESSION["headCompanyId"];

include_once ("class/finance/customer/cls_common_get.php");
include_once ("class/finance/cls_get_gldetails.php");
include_once ("class/cls_commonFunctions_get.php");
//include 	  "include/javascript.html";

$obj_common_get			= new Cls_Common_Get($db);
$obj_GLDetails_get		= new Cls_Get_GLDetails($db);
$obj_common_function_get= new cls_commonFunctions_get($db);

$serialNo				= (!isset($_REQUEST["SerialNo"])?'':$_REQUEST["SerialNo"]);
$serialYear				= (!isset($_REQUEST["SerialYear"])?'':$_REQUEST["SerialYear"]);
$customerId				= (!isset($_REQUEST["CustomerId"])?'':$_REQUEST["CustomerId"]);
$currencyId				= (!isset($_REQUEST["CurrencyId"])?'':$_REQUEST["CurrencyId"]);
$programCode			= 'P1163';

$header_array 			= GetHeaderDetails($serialYear,$serialNo);
$detail_result 			= GetGridDetails($serialYear,$serialNo);
$gl_result 				= GetGLGridDetails($serialYear,$serialNo);

if($obj_common_function_get->ValidateSpecialPermission('72',$_SESSION["userId"],'RunQuery'))
	$dateDisabled		= "";
else
	$dateDisabled		= "disabled='disabled'";	
	
if(!isset($_REQUEST["SerialNo"]))
	$invoiceDate		= date("Y-m-d");
else
	$invoiceDate		= $header_array["RECEIPT_DATE"];
	
$cls_display_butCancel	= 'maskHide'; //maskShow/maskHide
?>
<title>Debit Note Receive</title>

<!--<script type="text/javascript" src="presentation/finance_new/customer/payment_receive/payment_receive.js"></script>-->
<script>
	var customerIdAL	='<?php echo $customerId; ?>';
	var currencyIdAL	='<?php echo $currencyId; ?>';
</script>

<form id="frmDebitReceive" name="frmDebitReceive" method="post">
  <div align="center">
    <div class="trans_layoutS" style="width:900px">
      <div class="trans_text">Debit Note Receive</div>
      <table width="900">
        <tr>
          <td><table width="100%" border="0" class="normalfnt">
              <tr>
                <td width="14%"> Receipt Number</td>
                <td width="38%"><input type="text" name="txtReceiptNo" id="txtReceiptNo" style="width:100px" disabled="disabled" value="<?php echo $header_array["RECEIPT_NO"]?>"/>
                  <input type="text" name="txtReceiptYear" id="txtReceiptYear" style="width:50px" disabled="disabled" value="<?php echo $header_array["RECEIPT_YEAR"]?>"/></td>
                <td width="16%">&nbsp;</td>
                <td width="32%">&nbsp;</td>
              </tr>
              <tr>
                <td>Customer <span class="compulsoryRed">*</span></td>
                <td><select name="cboCustomer" id="cboCustomer" style="width:280px;height:20px" class="cls_load_details validate[required]">
                  <?php 		
		if(!isset($_REQUEST["SerialNo"]))
		{			
			$sql = "SELECT DISTINCT
					  CU.intId   AS CUSTOMER_ID,
					  CU.strName AS CUSTOMER_NAME
					FROM mst_customer CU
					  INNER JOIN finance_customer_transaction FCT
						ON FCT.CUSTOMER_ID = CU.intId
					WHERE FCT.COMPANY_ID = '$session_companyId'
					GROUP BY FCT.DEBIT_NO,FCT.DEBIT_YEAR,FCT.CUSTOMER_ID,FCT.CURRENCY_ID
					HAVING ROUND(SUM(FCT.VALUE),2)>0
					ORDER BY strName";
			$result = $db->RunQuery($sql);
				echo "<option value=\"".""."\">&nbsp;</option>";
			while($row = mysqli_fetch_array($result))
			{
				echo "<option value=\"".$row["CUSTOMER_ID"]."\">".$row["CUSTOMER_NAME"]."</option>";
			}
		}
		else
		{
			echo $header_array["CUSTOMER_HTML"];
		}
?>
                </select></td>
                <td>Date</td>
                <td><input name="txtReceiptDate" type="text" value="<?php echo $invoiceDate ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtReceiptDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo $dateDisabled;?> /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
              <tr>
                <td>Currency <span class="compulsoryRed">*</span></td>
                <td><select name="cboCurrency" id="cboCurrency" style="width:215px" class="cls_load_details validate[required] cls_cbo_CurrencyCalExchangeRate">
                  <?php
	if(!isset($_REQUEST["SerialNo"]))
	{
		echo "<option value=\"".""."\">&nbsp;</option>";
		$sql = "SELECT DISTINCT
				  CU.intId   AS CURRENCY_ID,
				  CU.strCode AS CURRENCY_NAME
				FROM mst_financecurrency CU
				  INNER JOIN finance_customer_transaction FCT
					ON FCT.CURRENCY_ID = CU.intId
				WHERE FCT.COMPANY_ID = '$session_companyId'
				GROUP BY FCT.DEBIT_NO,FCT.DEBIT_YEAR
				HAVING ROUND(SUM(FCT.VALUE),2) > 0
				ORDER BY CU.strCode";
		$result = $db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			if($row["CURRENCY_ID"]==$header_array["CURRENCY_ID"])
				echo "<option value=\"".$row["CURRENCY_ID"]."\" selected=\"selected\">".$row["CURRENCY_NAME"]."</option>";
			else
				echo "<option value=\"".$row["CURRENCY_ID"]."\">".$row["CURRENCY_NAME"]."</option>";
		}
	}
	else
	{
		echo $header_array["CURRENCY_HTML"];
	}
?>
                </select>
                <input type="text" name="txtExchangeRate" id="txtExchangeRate" style="width:60px;text-align:right" disabled="disabled" value="0.00" title="Exchange Rate" class="cls_txt_exchangeRate validate[required]"/></td>
                <td>Payment Method <span class="compulsoryRed">*</span></td>
                <td><select name="cboPaymentsMethods" id="cboPaymentsMethods" style="width:280px" class="validate[required]">
                    <option value=""></option>
                    <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_financepaymentsmethods
						WHERE
							intStatus = 1
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($header_array["PAYMENT_MODE"]==$row['intId'])
								echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
							else
								echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
                  </select></td>
              </tr>
              <tr>
                <td>Remarks</td>
                <td><textarea name="txtRemarks" id="txtRemarks2" style="width:280px;height:50px" ><?php echo $header_array["REMARKS"]?></textarea></td>
                <td valign="top">&nbsp;</td>
                <td class="normalfnt" valign="top">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td ><div style="overflow:scroll;width:900px;height:250px;">
              <table width="100%%" border="0" class="bordered" id="tblMain">
                <thead>
                  <tr>
                    <th width="3%"><input type="checkbox" class="cls_th_checkAll"></th>
                    <th width="18%">Debit No</th>
                    <th width="27%">Debit Remarks</th>
                    <th width="14%">Debit Note Date</th>
                    <th width="12%">Debit Note </th>
                   
                    <th width="13%">To Be Receive</th>
                    <th width="13%">Receiving Amount <span class="compulsoryRed">*</span></th>
                  </tr>
                </thead>
                <tbody>
                  <?php
$totRecieveAmount	= 0;
while($row = mysqli_fetch_array($detail_result))
{ 
	$toBeReceived		= round((abs($row["DEBIT_VALUE"]) - abs($row["RECEIVED_VALUE"])),2);
	$totRecieveAmount+=$row["RECEIVED_AMOUNT"] ;
?>
                  <tr>
                    <td class="cls_td_check" style="text-align:center"><input type="checkbox" checked="checked" disabled="disabled" class="cls_check"></td>
                    <td class="cls_td_debitNo"><?php echo $row["DEBIT_NO"]?></td>
                    <td class="cls_td_debitRemarks"><?php echo ($row["DEBIT_REMARKS"]==''?'&nbsp;':$row["DEBIT_REMARKS"])?></td>
                    <td class="cls_td_debitDate"><?php echo $row["DEBIT_DATE"]?></td>
                    <td class="cls_td_debitAmount" style="text-align:right"><?php echo $row["DEBIT_VALUE"]?></td>
                  
                    <td class="cls_td_toBePaid" style="text-align:right"><?php echo $toBeReceived?></td>
                    <td class="cls_td_payingAmount" style="text-align:right"><?php echo abs($row["RECEIVED_AMOUNT"]); ?></td>
                  </tr>
                  <?php
}
?>
                </tbody>
              </table>
          </div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td ><table width="100%%" border="0" class="bordered" id="tblGL">
              <thead>
                <tr>
                  <th colspan="6">GL Allocation
                    <div style="float:right"><a class="button white small" id="cls_addNewGL">Add New GL</a></div></th>
                </tr>
                <tr>
                  <th width="2%">&nbsp;</th>
                  <th width="28%">GL Account <span class="compulsoryRed">*</span></th>
                  <th width="10%">Debit <span class="compulsoryRed">*</span></th>
                  <th width="10%">Credit<span class="compulsoryRed"> *</span></th>
                  <th width="34%">Remarks</th>
                  <th width="16%">Cost Center <span class="compulsoryRed">*</span></th>
                </tr>
              </thead>
              <tbody>
                <?php 
 $totDebitAmount	= 0;
 $totCeditAmount	= 0;
 while($row = mysqli_fetch_array($gl_result))
 {
	 $booAvailable	 = true;
	 $totDebitAmount+= $row["DEBIT_AMOUNT"];
	 $totCeditAmount+= $row["CREDIT_AMOUNT"];
 ?>
                <tr class="cls_tr_firstRow">
                  <td class="cls_td_check" style="text-align:center"><img src="images/del.png" class="removeRow mouseover"/></td>
                  <td class="cls_td_GL" title="D"><select id="cboGL" style="width:100%" class="validate[required]" >
                      <?php echo $obj_GLDetails_get->getGLCombo('CUSTOMER_PAYMENT_RECEIVE',$row["GL_ID"]);?>
                    </select></td>
                  <td class="cls_td_GLAmount cls_td_glAmount_dr"><input id="txtGLAmount2" type="text" style="width:100%;text-align:right" class="validate[required,custom[number]] cls_GLAmount cls_cbo_numberOnly" value="<?php echo number_format($row["DEBIT_AMOUNT"],2,'.',''); ?>"/></td>
                  <td class="cls_td_GLAmount cls_td_glAmount_cr"><input id="txtGLAmount" type="text" style="width:100%;text-align:right" class="validate[required,custom[number]] cls_GLAmount cls_cbo_numberOnly" value="<?php echo number_format($row["CREDIT_AMOUNT"],2,'.',''); ?>"/></td>
                  <td class="cls_td_remarks"><textarea id="txtRemarks" style="width:100%;height:20px" ><?php echo $row['REMARKS']; ?></textarea></td>
                  <td class="cls_td_costCenter"><select id="cboCostCenter" style="width:100%;"  class="validate[required]" >
                      <?php echo GetCostCenter($row["COST_CENTER_ID"]);?>
                    </select></td>
                </tr>
                <?php 
 }
?>
                <?php
if(!$booAvailable){
?>
                <tr class="cls_tr_firstRow">
                  <td class="cls_td_check" style="text-align:center"><img src="images/del.png" class="removeRow mouseover"/></td>
                  <td class="cls_td_GL" title="D"><select id="cboGL" style="width:100%" class="validate[required]" >
                      <?php echo $obj_GLDetails_get->getGLCombo('CUSTOMER_PAYMENT_RECEIVE',$row["GL_ID"]);?>
                    </select></td>
                  <td class="cls_td_GLAmount cls_td_glAmount_dr"><input id="txtGLAmount3" type="text" style="width:100%;text-align:right" class="validate[required,custom[number]] cls_GLAmount cls_cbo_numberOnly" value="<?php echo ($row['GL_AMOUNT']==''?0:round($row['GL_AMOUNT'],2))?>"/></td>
                  <td class="cls_td_GLAmount cls_td_glAmount_cr"><input id="txtGLAmount" type="text" style="width:100%;text-align:right" class="validate[required,custom[number]] cls_GLAmount cls_cbo_numberOnly" value="<?php echo ($row['GL_AMOUNT']==''?0:round($row['GL_AMOUNT'],2))?>"/></td>
                  <td class="cls_td_remarks"><textarea id="txtRemarks" style="width:100%;height:20px" ><?php echo $row['REMARKS']; ?></textarea></td>
                  <td class="cls_td_costCenter"><select id="cboCostCenter" style="width:100%;"  class="validate[required]" >
                      <?php echo GetCostCenter($row["COST_CENTER_ID"]);?>
                    </select></td>
                </tr>
                <?php
}
?>
              </tbody>
              <tfoot>
              <tr style="font-weight:bold">
                  <td>&nbsp;</td>
                  <td style="text-align:center">TOTAL</td>
                  <td class="cls_td_totAmount_dr" style="text-align:right"><?php echo number_format($totDebitAmount,2,'.',''); ?></td>
                  <td class="cls_td_totAmount_cr" style="text-align:right"><?php echo number_format($totCeditAmount,2,'.',''); ?></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </tfoot>
            </table></td>
        </tr>
        <tr>
          <td><table width="301" border="0" align="right" class="normalfnt" style="font-weight:bold">
              <tr>
                <td width="166">Total Paying Amount</td>
                <td width="11" style="text-align:center"><b>:</b></td>
                <td width="91" style="text-align:right;font-weight:bold" id="td_totalPayingAmount"><?php echo number_format($totRecieveAmount,2,'.','');?></td>
              </tr>
              <tr>
                <td>Total GL Allocated Amount</td>
                <td style="text-align:center"><b>:</b></td>
                <td style="text-align:right;font-weight:bold" id="td_totalGLAlloAmount"><?php echo number_format($totDebitAmount,2,'.',''); ?></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="center"><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($form_permision['edit']==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');?><a class="button white medium <?php echo $cls_display_butCancel?>" style="display:none" id="butCancel">Cancel</a><a class="button white medium" id="butReport" name="butReport">&nbsp;Report&nbsp;</a><a href="main.php" class="button white medium" id="butClose">Close</a></td>
        </tr>
      </table>
    </div>
  </div>
</form>
<?php
function GetHeaderDetails($serialYear,$serialNo)
{
	global $db;
				
	$sql = "SELECT 
				CDRH.RECEIPT_NO 		AS RECEIPT_NO,
				CDRH.RECEIPT_YEAR 		AS RECEIPT_YEAR,
				CU.intId 				AS CUSTOMER_ID,
				CU.strName 				AS CUSTOMER_NAME,
				CURR.intId				AS CURRENCY_ID,
				CURR.strCode			AS CURRENCY_CODE,
				CDRH.REMARKS			AS REMARKS,
				CDRH.PAYMENT_MODE		AS PAYMENT_MODE,
				CDRH.RECEIPT_DATE		AS RECEIPT_DATE
			FROM finance_customer_debit_receive_header CDRH
			INNER JOIN mst_customer CU ON CU.intId = CDRH.CUSTOMER_ID
			INNER JOIN mst_financecurrency CURR ON CURR.intId = CDRH.CURRENCY_ID
			WHERE CDRH.RECEIPT_NO = '$serialNo' AND CDRH.RECEIPT_YEAR = '$serialYear'";
				
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	$header_array["RECEIPT_NO"]			= $row["RECEIPT_NO"];
	$header_array["RECEIPT_YEAR"]		= $row["RECEIPT_YEAR"];
	$header_array["CUSTOMER_HTML"]		= "<option value=\"".$row["CUSTOMER_ID"]."\">".$row["CUSTOMER_NAME"]."</option>";
	$header_array["CURRENCY_HTML"]		= "<option value=\"".$row["CURRENCY_ID"]."\">".$row["CURRENCY_CODE"]."</option>";
	$header_array["REMARKS"]			= $row["REMARKS"];
	$header_array["PAYMENT_MODE"]		= $row["PAYMENT_MODE"];
	$header_array["RECEIPT_DATE"]		= $row["RECEIPT_DATE"];		
	return $header_array;
}

function GetGridDetails($serialYear,$serialNo)
{
	global $db;
	
	$sql = "SELECT 
				CONCAT(CDNH.DEBIT_NO,'-',CDNH.DEBIT_YEAR)						AS DEBIT_NO,
				CDNH.DEBIT_DATE 									AS DEBIT_DATE,
				CDNH.REMARKS										AS DEBIT_REMARKS,
				
				ROUND(COALESCE((SELECT SUM(SUB_CDRD.PAY_AMOUNT) FROM 
				finance_customer_debit_receive_details SUB_CDRD
				WHERE SUB_CDRD.RECEIPT_NO = CDRD.RECEIPT_NO 
					AND SUB_CDRD.RECEIPT_YEAR = CDRD.RECEIPT_YEAR
					AND SUB_CDRD.DEBIT_NO = CDNH.DEBIT_NO
					AND SUB_CDRD.DEBIT_YEAR = CDNH.DEBIT_YEAR
				GROUP BY SUB_CDRD.RECEIPT_NO,
					SUB_CDRD.RECEIPT_YEAR,
					SUB_CDRD.DEBIT_NO,
					SUB_CDRD.DEBIT_YEAR),0),2) 							AS RECEIVED_AMOUNT,
				
				ROUND(COALESCE((SELECT SUM(SUB_CDND.QTY*SUB_CDND.UNIT_PRICE) 
				FROM finance_customer_debit_note_header SUB_CDNH 
				INNER JOIN finance_customer_debit_note_detail SUB_CDND 
				ON SUB_CDNH.DEBIT_NO = SUB_CDND.DEBIT_NO 
					AND SUB_CDNH.DEBIT_YEAR = SUB_CDND.DEBIT_YEAR
				WHERE SUB_CDNH.DEBIT_NO = CDNH.DEBIT_NO
					AND SUB_CDNH.DEBIT_YEAR = CDNH.DEBIT_YEAR
				GROUP BY SUB_CDNH.DEBIT_NO,
					SUB_CDNH.DEBIT_YEAR),0),2)							AS DEBIT_VALUE,
				  
				
				ROUND(COALESCE((SELECT
				SUM(SUB_CT.VALUE)
				FROM finance_customer_transaction SUB_CT 
				WHERE SUB_CT.CUSTOMER_ID = CDNH.CUSTOMER_ID
				AND SUB_CT.CURRENCY_ID = CDNH.CURRENCY_ID
				AND SUB_CT.DEBIT_NO = CDNH.DEBIT_NO
				AND SUB_CT.DEBIT_YEAR = CDNH.DEBIT_YEAR
				AND SUB_CT.DOCUMENT_TYPE = 'DEBIT_RECEIVE'),0),2) 		AS RECEIVED_VALUE
				 
			FROM finance_customer_debit_receive_details CDRD
			INNER JOIN finance_customer_debit_note_header CDNH ON CDNH.DEBIT_NO = CDRD.DEBIT_NO AND CDNH.DEBIT_YEAR = CDRD.DEBIT_YEAR
			WHERE CDRD.RECEIPT_NO = '$serialNo' AND 
			CDRD.RECEIPT_YEAR = '$serialYear'
			GROUP BY CDRD.RECEIPT_NO,CDRD.RECEIPT_YEAR,CDRD.DEBIT_NO,CDRD.DEBIT_YEAR";
			//die($sql);
	return $db->RunQuery($sql);
}

function GetGLGridDetails($serialYear,$serialNo)
{
	global $db;
	
	$sql = "SELECT 
				CDRGL.LEDGER_ID					AS GL_ID,
				CDRGL.PAY_AMOUNT				AS GL_AMOUNT,
				CDRGL.REMARKS					AS REMARKS,
				IF(CDRGL.TRANSACTION_TYPE='D',CDRGL.PAY_AMOUNT,0) AS DEBIT_AMOUNT,
				IF(CDRGL.TRANSACTION_TYPE='C',CDRGL.PAY_AMOUNT,0) AS CREDIT_AMOUNT,
				CDRGL.COST_CENTER_ID			AS COST_CENTER_ID
			FROM finance_customer_debit_receive_gl CDRGL
			WHERE CDRGL.RECEIPT_NO = '$serialNo' 
				AND CDRGL.RECEIPT_YEAR = '$serialYear'";
	return $db->RunQuery($sql);
}

function GetGL($id)
{
	global $db;
	global $session_companyId;
	
	$sql = "SELECT
			mst_financechartofaccounts.intId,
			mst_financechartofaccounts.strCode,
			mst_financechartofaccounts.strName,
			mst_financechartofaccounts_companies.intCompanyId,
			mst_financechartofaccounts_companies.intChartOfAccountId
			FROM
			mst_financechartofaccounts
			Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
			WHERE
			intStatus = '1' AND  (intFinancialTypeId = '24'  OR  intFinancialTypeId = '23' OR  intFinancialTypeId = '11' OR  intFinancialTypeId = '6') AND strType = 'Posting' AND intCompanyId = '$session_companyId'
			order by strCode";
	$result = $db->RunQuery($sql);
		$string = "<option value=\"".""."\"></option>";
	while($row = mysqli_fetch_array($result))
	{
		if($id == $row['intId'])
			$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."-".$row['strName']."</option>";
		else
			$string .= "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";
	}
	return $string;
}

function GetCostCenter($id)
{
	global $db;
	
	$sql = "SELECT intId,strName FROM mst_financedimension WHERE intStatus = 1 order by strName";
	$result = $db->RunQuery($sql);
		$string = "<option value=\"".""."\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		if($id == $row['intId'])
			$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
		else
			$string .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
	}
	return $string;
}
?>
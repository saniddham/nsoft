// JavaScript Document
var basePath	= "presentation/finance_new/customer/debit_note/";
var reportId	= 928;
var menuId		= 758;
$(document).ready(function(){
	
	$("#frmDebitNote").validationEngine();
	
	/*if(intAddx)
	{
		$('#frmDebitNote #butNew').show();
		
		if(status==-2)
			$('#frmDebitNote #butSave').hide();
		else
			$('#frmDebitNote #butSave').show();
	}
	//permision for edit 
	if(intEditx)
	{
		$('#frmDebitNote #butNew').show();
		
		if(status==-2)
			$('#frmDebitNote #butSave').hide();
		else
			$('#frmDebitNote #butSave').show();
	}
	
	//permision for delete
	if(intDeletex)
	{
		$('#frmDebitNote #butDelete').show();
	}
	
	//permision for view
	if(intViewx)
	{
		$('#frmDebitNote #cboSearch').removeAttr('disabled');
	}*/
	
	$('#frmDebitNote #butInsertRow').die('click').live('click',addNewRow);
	$('#frmDebitNote .clsDel').die('click').live('click',deleteRow);
	$('#frmDebitNote .clsQty').die('keyup').live('keyup',setAmount);
	$('#frmDebitNote .clsUnitPrice').die('keyup').live('keyup',setAmount);
	$('#frmDebitNote .clsTax').die('change').live('change',function(){calculateTax(this)});
	$('#frmDebitNote .clsQty').die('blur').live('blur',function(){calculateTax(this)});
	$('#frmDebitNote .clsUnitPrice').die('blur').live('blur',function(){calculateTax(this)});
	$('#frmDebitNote #cboCustomer').die('change').live('change',loadInvoiceType);
	$('#frmDebitNote #butSave').die('click').live('click',saveData);
	$('#frmDebitNote #butNew').die('click').live('click',clearAll);
	$('#frmDebitNote #butReport').die('click').live('click',viewReport);
});
function addNewRow()
{
	if($('#frmDebitNote').validationEngine('validate'))
	{
		$('#frmDebitNote #tblMain tbody tr:last').after("<tr>"+$('#frmDebitNote #tblMain .cls_tr_firstRow').html()+"</tr>");
		$('#frmDebitNote #tblMain tbody tr:last #txtItemDisc').val('');
		$('#frmDebitNote #tblMain tbody tr:last #cboUOM').val('');
		$('#frmDebitNote #tblMain tbody tr:last #txtQty').val('');
		$('#frmDebitNote #tblMain tbody tr:last #txtUnitPrice').val('');	
		$('#frmDebitNote #tblMain tbody tr:last #txtAmount').val(0);	
		$('#frmDebitNote #tblMain tbody tr:last #cboTax').val('');
		$('#frmDebitNote #tblMain tbody tr:last #hidTaxRate').val(0);		
	}
}
function deleteRow()
{
	var delRowCount = parseInt(document.getElementById('tblMain').rows.length);
	
	if(delRowCount>3)
		$(this).parent().parent().remove();
	
	if(parseInt(document.getElementById('tblMain').rows.length)==3)
		$('#frmDebitNote #tblMain tbody tr:last').addClass('cls_tr_firstRow');
	
	CaculateTotalValue();
}
function setAmount()
{
	var qty 		= parseFloat($(this).parent().parent().find('.clsQty').val()==''?0:$(this).parent().parent().find('.clsQty').val());
	var unitPrice 	= parseFloat($(this).parent().parent().find('.clsUnitPrice').val()==''?0:$(this).parent().parent().find('.clsUnitPrice').val());
	
	var amount		= (qty*unitPrice);
	
	$(this).parent().parent().find('.clsAmount').val(RoundNumber(amount,2));
	
	CaculateTotalValue();
}
function calculateTax(obj)
{
	var url 	= basePath+"debit_note_db.php?requestType=URLCalculateTax";
	var data 	= "taxId="+$(obj).parent().parent().find('.clsTax').val()+"&amount="+$(obj).parent().parent().find('.clsAmount').val();
	var httpobj = $.ajax({
							url:url,
							data:data,
							dataType:'json',
							type:'POST',
							async:false,
							success:function(json)
							{
								$(obj).parent().parent().find('.clsTaxAmount').val(RoundNumber(json.TaxValue,2));
							}
						});	
	CaculateTotalValue();	
}

function loadInvoiceType()
{
	if($(this).val()=='')
	{
		$('#frmDebitNote #cboInvoiceType').val('');
		return;
	}
	var url 	= basePath+"debit_note_db.php?requestType=loadInvoiceType";
	var data 	= "customerId="+$('#frmDebitNote #cboCustomer').val();
	var httpobj = $.ajax({
							url:url,
							data:data,
							dataType:'json',
							type:'POST',
							async:false,
							success:function(json)
							{
								$('#frmDebitNote #cboInvoiceType').val(json.invType);
							}
						});	
}
function CaculateTotalValue()
{
	var totalValue		= 0;
	var grandTotal		= 0;
	var taxValue		= 0;
	
	$('#frmDebitNote #tblMain .clsAmount').each(function(){
		
		totalValue += parseFloat($(this).val());
		taxValue   += parseFloat($(this).parent().parent().find('.clsTaxAmount').val());	
	});
	
	taxValue	 = RoundNumber(taxValue,2);
	totalValue	 = RoundNumber(totalValue,2);
	grandTotal	 = parseFloat(totalValue)+parseFloat(taxValue);
	
	$('#frmDebitNote #txtSubTotal').val(totalValue);
	$('#frmDebitNote #txtTaxTotal').val(taxValue);	
	$('#frmDebitNote #txtTotal').val(RoundNumber(grandTotal,2));
}
function saveData()
{
	if(!IsProcessMonthLocked_date($('#txtDate').val()))
		return;
	
	showWaiting();
	var debitNoteNo 	= $('#frmDebitNote #txtDebitNoteNo').val();
	var debitNoteYear 	= $('#frmDebitNote #txtDebitYear').val();
	var dtDate 			= $('#frmDebitNote #txtDate').val();
	var customerId 		= $('#frmDebitNote #cboCustomer').val();
	var invoiceType 	= $('#frmDebitNote #cboInvoiceType').val();
	var currency 		= $('#frmDebitNote #cboCurrency').val();
	var remarks 		= $('#frmDebitNote #txtRemarks').val();
	var subTotal 		= $('#frmDebitNote #txtSubTotal').val();
	var grandTotal 		= $('#frmDebitNote #txtTotal').val();
	
	if($('#frmDebitNote').validationEngine('validate'))
	{
		if(debitNoteNo!='' && debitNoteYear!='')
		{
			$('#frmDebitNote #butSave').validationEngine('showPrompt','You can not update this Debit Note.','fail');
			hideWaiting();
			return;
		}
		
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"debitNoteNo":"'+debitNoteNo+'",' ;
							arrHeader += '"debitNoteYear":"'+debitNoteYear+'",' ;
							arrHeader += '"dtDate":"'+dtDate+'",' ;
							arrHeader += '"customerId":"'+customerId+'",' ;
							arrHeader += '"invoiceType":"'+invoiceType+'",' ;
							arrHeader += '"currency":"'+currency+'",' ;
							arrHeader += '"remarks":'+URLEncode_json(remarks)+',' ;
							arrHeader += '"subTotal":"'+subTotal+'",' ;
							arrHeader += '"grandTotal":"'+grandTotal+'"' ;
			arrHeader += ' }';
			
		var chkStatus	= false;
		var arrDetails	= "";
		$('#tblMain .clsAmount').each(function(){
			
			var itemId			= $(this).parent().parent().find('.clsLedgerAc').val();
			var itemDisc		= $(this).parent().parent().find('.clsItemDesc').val();
 			var uom			 	= $(this).parent().parent().find('.clsUOM').val();
			var qty		 		= $(this).parent().parent().find('.clsQty').val();
			var amonut			= $(this).val();
			var unitPrice 		= $(this).parent().parent().find('.clsUnitPrice').val();
			var taxId 			= $(this).parent().parent().find('.clsTax').val();
			var costCenter 		= $(this).parent().parent().find('.clsCostCener').val();
			var taxAmount 		= $(this).parent().parent().find('.clsTaxAmount').val();
			
			if(parseFloat(amonut)>0)
			{
				chkStatus   = true;
				arrDetails += "{";
				arrDetails += '"itemId":"'+ itemId +'",' ;
				arrDetails += '"itemDisc":'+ URLEncode_json(itemDisc) +',' ;
				arrDetails += '"uom":"'+ uom +'",' ;
				arrDetails += '"qty":"'+ qty +'",' ;
				arrDetails += '"unitPrice":"'+ unitPrice +'",' ;
				arrDetails += '"taxId":"'+ taxId +'",' ;
				arrDetails += '"costCenter":"'+ costCenter +'",' ;
				arrDetails += '"taxAmount":"'+ taxAmount +'"' ;
				arrDetails +=  '},';
			}
			
		});
		
		if(!chkStatus)
		{
			$('#frmDebitNote #butSave').validationEngine('showPrompt','No records to save.','fail');
			hideWaiting();
			return;
		}
		
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		
		var arrHeader	= arrHeader;
		var arrDetails	= '['+arrDetails+']';
		data+="&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		var url = basePath+"debit_note_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmDebitNote #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						$('#frmDebitNote #txtDebitNoteNo').val(json.debiNoteNo);
						$('#frmDebitNote #txtDebitYear').val(json.debitNoteYear);
						$('#frmDebitNote #frmDebitNote #butSave').hide();
						$('#frmDebitNote #butCancle').show();
						$('#frmDebitNote #butReport').show();
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
						$('#frmDebitNote #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
	}
	else
	{
		hideWaiting();
	}	
}
function clearAll()
{
	window.location.href = '?q='+menuId;
}
function viewReport()
{
	if($('#frmDebitNote #txtDebitNoteNo').val()=='')
	{
		$('#frmDebitNote #butReport').validationEngine('showPrompt','No Debit Note No to view report.','fail');
		return;
	}
	var url  = "?q="+reportId+"&debitNoteNo="+$('#frmDebitNote #txtDebitNoteNo').val();
		url += "&debitNoteYear="+$('#frmDebitNote #txtDebitYear').val();
	
	window.open(url,'rptdebit_note.php');
	
}
function alertx()
{
	$('#frmDebitNote #butSave').validationEngine('hide')	;
}
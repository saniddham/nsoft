<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$menuId				= 758;
$reportId			= 928;

$companyId			= $_SESSION["headCompanyId"];
require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'STATUS'=>'CDNH.STATUS',
				'debitNo'=>"CONCAT(CDNH.DEBIT_NO,' / ',CDNH.DEBIT_YEAR)",
				'Customer'=>'MC.strName',
				'DEBIT_DATE'=>'CDNH.DEBIT_DATE',
				'strCode'=>'FC.strCode',
				'REMARKS'=>'CDNH.REMARKS'
				);
$arr_status = array('Saved'=>'1','Cancelled'=>'-2');				
foreach($arr as $k=>$v)
{
	if($v['field']=='STATUS')
	{
		$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND CDNH.DEBIT_DATE = '".date('Y-m-d')."'";
	
// }

$sql = "SELECT SUB_1.* FROM
		(
		SELECT IF(CDNH.STATUS=-2,'Cancelled','Saved') AS STATUS,
		CONCAT(CDNH.DEBIT_NO,' / ',CDNH.DEBIT_YEAR) AS debitNo,
		CDNH.DEBIT_NO,
		CDNH.DEBIT_YEAR,
		CDNH.DEBIT_DATE,
		CDNH.REMARKS,
		FC.strCode,
		MC.strName AS Customer,
		'View' AS VIEW,
		(SELECT ROUND(SUM((CDND.QTY*CDND.UNIT_PRICE)+CDND.TAX_VALUE),2) 
		FROM finance_customer_debit_note_detail CDND
		WHERE CDND.DEBIT_NO=CDNH.DEBIT_NO AND
		CDND.DEBIT_YEAR=CDNH.DEBIT_YEAR) AS totAmount
		FROM finance_customer_debit_note_header CDNH
		INNER JOIN mst_customer MC ON MC.intId=CDNH.CUSTOMER_ID
		LEFT JOIN mst_financecurrency FC ON FC.intId=CDNH.CURRENCY_ID
		WHERE CDNH.COMPANY_ID='$companyId'
		$where_string
		)  
		AS SUB_1 WHERE 1=1";

$jq 	= new jqgrid('',$db);	
$col 	= array();
$cols 	= array();

$col["title"] 			= "DebitNote No";
$col["name"] 			= "DEBIT_NO";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "DebitNote Year";
$col["name"] 			= "DEBIT_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Status"; 
$col["name"] 			= "STATUS";
$col["width"] 			= "3";
$col["stype"] 			= "select";
$str 					= ":All;Saved:Saved;Cancelled:Cancelled" ;
$col["editoptions"] 	=  array("value"=> $str);
$col["align"] 			= "center";
$cols[] 				= $col;
$col					= NULL;

$col["title"] 			= "Serial No";
$col["name"] 			= "debitNo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 
$col['link']			= '?q='.$menuId.'&debitNoteNo={DEBIT_NO}&debitNoteYear={DEBIT_YEAR}';
$col["linkoptions"] 	= "target='debit_note.php'";					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Customer";
$col["name"] 			= "Customer";
$col["width"] 			= "6";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Date";
$col["name"] 			= "DEBIT_DATE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "strCode";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Remarks";
$col["name"] 			= "REMARKS";
$col["width"] 			= "7";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Amount";
$col["name"] 			= "totAmount";
$col["width"] 			= "3";
$col["align"] 			= "right";
$col["sortable"] 		= true; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "View";
$col["name"] 			= "VIEW";
$col["width"] 			= "2";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= '?q='.$reportId.'&debitNoteNo={DEBIT_NO}&debitNoteYear={DEBIT_YEAR}';
$col["linkoptions"] 	= "target='rptdebit_note.php'";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Customer Debit Note Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'DEBIT_YEAR,DEBIT_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Customer Debit Note Listing</title>
<?php
echo $out;
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$userId 			= $_SESSION['userId'];

include_once  "class/finance/cls_common_get.php";
//include_once  "class/masterData/exchange_rate/cls_exchange_rate_get.php";
include_once  "class/finance/customer/debit_note/cls_debit_note_get.php";
include_once  "class/cls_commonFunctions_get.php";
//include 	  "include/javascript.html";

$obj_common_get		= new Cls_Common_Get($db);
$obj_exchgRate_get	= new cls_exchange_rate_get($db);
$obj_debitnote_get	= new Cls_Debit_Note_Get($db,$companyId);
$obj_common			= new cls_commonFunctions_get($db);

$debitNoteNo 		= (!isset($_REQUEST['debitNoteNo'])?'':$_REQUEST['debitNoteNo']);
$debitNoteYear	 	= (!isset($_REQUEST['debitNoteYear'])?'':$_REQUEST['debitNoteYear']);
$programCode		= 'P0758';

$header_arr			= $obj_debitnote_get->getRptHeaderData($debitNoteNo,$debitNoteYear);
$exchngRate_array	= $obj_exchgRate_get->GetAllValues($header_arr['CURRENCY_ID'],2,$header_arr['DEBIT_DATE'],$companyId,'RunQuery');
$detail_result		= $obj_debitnote_get->getRptDetaiData($debitNoteNo,$debitNoteYear);
$dateChangeMode 	= $obj_common_get->getDateChangeMode($programCode,$userId);
$cancleMode 		= $obj_common->Load_menupermision($programCode,$userId,'intCancel');

if($obj_common->ValidateSpecialPermission('25',$_SESSION["userId"],'RunQuery'))
	$dateDisabled		= "";
else
	$dateDisabled		= "disabled='disabled'";
?>
<title>Customer - Debit Note</title>

<!--<script type="text/javascript" src="presentation/finance_new/customer/debit_note/debit_note.js"></script>-->

<style>
#apDiv1 {
	position: absolute;
	left: 27px;
	top: 134px;
	width: auto;
	height: auto;
	z-index: 1;
}
</style>

<?php
if($header_arr['STATUS']==-2)
{
?>
	<div id="apDiv1" style="display:none"><img src="images/cancelled.png" /></div>
<?php
}
?>
<form id="frmDebitNote" name="frmDebitNote" method="post">
<div align="center">
<div class="trans_layoutXL">
<div class="trans_text">Customer - Debit Note</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td class="normalfnt">Debit Note No</td>
                <td class="normalfnt"><input name="txtDebitNoteNo" type="text" disabled="disabled" id="txtDebitNoteNo" style="width:80px" value="<?php echo $debitNoteNo; ?>" />&nbsp;<input name="txtDebitYear" type="text" disabled="disabled" id="txtDebitYear" style="width:50px" value="<?php echo $debitNoteYear; ?>" /></td>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt"></td>
            </tr>
            <tr>
                <td width="16%" class="normalfnt">Customer <span class="compulsoryRed">*</span></td>
                <td width="43%" class="normalfnt">
                <select name="cboCustomer" id="cboCustomer"  style="width:270px" class="validate[required]" >
                <option value=""></option>
                <?php
                $result = $obj_debitnote_get->loadCustomer();
				while($row=mysqli_fetch_array($result))
				{
					if($row['intId']==$header_arr['CUSTOMER_ID'])
						echo "<option value=\"".$row["intId"]."\" selected=\"selected\">".$row["strName"]."</option>";
					else
						echo "<option value=\"".$row["intId"]."\">".$row["strName"]."</option>";
				}
                ?>
                </select></td>
                <td width="14%" class="normalfnt">Date</td>
                <td width="27%" class="normalfnt"><input name="txtDate" type="text" value="<?php echo($header_arr['DEBIT_DATE']==''?date("Y-m-d"):$header_arr['DEBIT_DATE']); ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtDate" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo $dateDisabled ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
            </tr>
            <tr>
              <td class="normalfnt">Currency <span class="compulsoryRed">*</span></td>
              <td class="normalfnt"><select name="cboCurrency" id="cboCurrency"  style="width:270px" class="validate[required] cls_cbo_CurrencyCalExchangeRate" >
                <?php
                echo $obj_common_get->getCurrencyCombo($header_arr['CURRENCY_ID']);
                ?>
              </select>
                <input type="text" name="txtExchangeRate" id="txtExchangeRate" style="width:60px;text-align:right" disabled="disabled" value="<?php echo $exchngRate_array["AVERAGE_RATE"]?>" title="Exchange Rate" class="cls_txt_exchangeRate validate[required]"/></td>
              <td class="normalfnt">Invoice Type</td>
              <td class="normalfnt"><select name="cboInvoiceType" id="cboInvoiceType" style="width:270px" disabled="disabled">
                <option value=""></option>
                <?php
					$result = $obj_debitnote_get->loadInvoiceType();
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$header_arr['INVOICE_TYPE'])
							echo "<option value=\"".$row["intId"]."\" selected=\"selected\">".$row["strInvoiceType"]."</option>";
						else
							echo "<option value=\"".$row["intId"]."\">".$row["strInvoiceType"]."</option>";
					}	
                ?>
              </select></td>
            </tr>
            <tr>
                <td class="normalfnt" valign="top">Remarks</td>
                <td rowspan="2" valign="top" class="normalfnt"><textarea name="txtRemarks" id="txtRemarks" style="width:270px" rows="3"><?php echo $header_arr['REMARKS']; ?></textarea></td>
                <td class="normalfnt" valign="top">&nbsp;</td>
                <td class="normalfnt" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td class="normalfnt" valign="top">&nbsp;</td>
                <td class="normalfnt" valign="top">&nbsp;</td>
                <td class="normalfnt" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4" class="normalfnt"><div style="overflow:scroll;overflow-x:hidden;width:auto;height:350px;" id="divGrid">
                <table width="100%" class="bordered" id="tblMain" >
                <thead>
                    <tr>
                    	<th colspan="9" >Items<div style="float:right"><a class="button white small" id="butInsertRow">Add New Item</a></div></th>
                    </tr>
                    <tr>
                        <th width="15" height="30" >Del</th>
                        <th width="220" >Item / Expenses <span class="compulsoryRed">*</span></th>
                        <th width="250"  >Item Description <span class="compulsoryRed">*</span></th>
                        <th width="80" >UOM <span class="compulsoryRed">*</span></th>
                        <th width="80" >Qty <span class="compulsoryRed">*</span></th>
                        <th width="80" >Unit Price <span class="compulsoryRed">*</span></th>
                        <th width="100" >Amount</th>
                        <th width="100" >Tax</th>
                        <th width="100" >Cost Center <span class="compulsoryRed">*</span></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $totAmount 		= 0;
                $totTaxAmount 	= 0;
                if($debitNoteNo!='' && $debitNoteYear!='')
                {
                while($row=mysqli_fetch_array($detail_result))
                {
                    $amount	   		 = $row['QTY']*$row['UNIT_PRICE'];	
                    
                    $totAmount 		+=$amount;
                    $totTaxAmount 	+=$row['taxAmount'];
                ?>
                    <tr class="normalfnt cls_tr_firstRow">
                        <td style="text-align:center"><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>
                        <td style="text-align:left">
                        <select name="cboLedgerAc" id="cboLedgerAc" class="clsLedgerAc validate[required]"  style="width:100%" >
                        <option value=""></option>
                        <?php
                        	$resultItem = $obj_debitnote_get->loadItem();
							while($rowItem=mysqli_fetch_array($resultItem))
							{
								if($rowItem['intId']==$row['ITEM_ID'])
									echo "<option value=\"".$rowItem["intId"]."\" selected=\"selected\">".$rowItem["strName"]."</option>";
								else
									echo "<option value=\"".$rowItem["intId"]."\">".$rowItem["strName"]."</option>";
							}
                        ?>
                        </select></td>
                        <td style="text-align:left"><textarea name="txtItemDisc" id="txtItemDisc" class="clsItemDesc validate[required]" style="width:100%;height:19px"><?php echo $row['ITEM_DESCRIPTION']; ?></textarea></td>
                        <td style="text-align:left">
                        <select name="cboUOM" id="cboUOM" class="clsUOM validate[required]" style="width:100%">
                        <?php
                        echo $obj_common_get->getUOMCombo($row['UNIT_OF_MEASURE']);
                        ?>
                        </select>
                        </td>
                        <td style="text-align:right">
                        <input type="textbox" id="txtQty" class="clsQty validate[required,custom[number]]" style="width:100%;text-align:right" value="<?php echo number_format($row['QTY'], 2, '.', ''); ?>" />
                        </td>
                        <td style="text-align:center"><input type="textbox" id="txtUnitPrice" class="clsUnitPrice validate[required,custom[number]]" style="width:100%;text-align:right" value="<?php echo number_format($row['UNIT_PRICE'], 2, '.', ''); ?>"></td>
                        <td style="text-align:center"><input type="textbox" id="txtAmount" class="clsAmount" style="width:100%;text-align:right" value="<?php echo number_format($amount, 2, '.', ''); ?>" disabled="disabled" /></td>
                        <td style="text-align:center">
                        <select name="cboTax" id="cboTax" class="clsTax" style="width:100%">
                        <?php
                        echo $obj_common_get->getTaxCombo($row['TAX_ID']);
                        ?>
                        </select><input type="hidden" id="hidTaxRate" class="clsTaxAmount" value="<?php echo number_format($row['taxAmount'], 2, '.', ''); ?>"/></td>
                        <td style="text-align:center">
                        <select name="cboCostCenter" id="cboCostCenter" class="clsCostCener validate[required]" style="width:100%">                
                        <?php
                        echo $obj_common_get->getCostCenterCombo($row['COST_CENTER_ID']);
                        ?>
                        </select></td>
                    </tr>
                <?php		
                }
                }
                if($debitNoteNo=='' && $debitNoteYear=='')
                {
                ?>
                    <tr class="normalfnt cls_tr_firstRow">
                        <td style="text-align:center"><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>
                        <td style="text-align:left"><select name="cboLedgerAc" id="cboLedgerAc" class="clsLedgerAc validate[required]"  style="width:100%" >
                        <option value=""></option>
                        <?php
                        	$resultItem = $obj_debitnote_get->loadItem();
							while($rowItem=mysqli_fetch_array($resultItem))
							{
								if($rowItem['intDefaultFocus']==1)
									echo "<option value=\"".$rowItem["intId"]."\" selected=\"selected\">".$rowItem["strName"]."</option>";
								else
									echo "<option value=\"".$rowItem["intId"]."\">".$rowItem["strName"]."</option>";
							}
                        ?>
                        </select></td>
                        <td style="text-align:left"><textarea name="txtItemDisc" id="txtItemDisc" class="clsItemDesc validate[required]" style="width:100%;height:19px"><?php echo $remarks; ?></textarea></td>
                        <td style="text-align:left">
                        <select name="cboUOM" id="cboUOM" class="clsUOM validate[required]" style="width:100%">
                        <?php
                        echo $obj_common_get->getUOMCombo("");
                        ?>
                        </select>
                        </td>
                        <td style="text-align:right">
                        <input type="textbox" id="txtQty" class="clsQty validate[required,custom[number]]" style="width:100%;text-align:right" />
                        </td>
                        <td style="text-align:center"><input type="textbox" id="txtUnitPrice" class="clsUnitPrice validate[required,custom[number]]" style="width:100%;text-align:right"></td>
                        <td style="text-align:center"><input type="textbox" id="txtAmount" class="clsAmount" style="width:100%;text-align:right" disabled="disabled" value="0" /></td>
                        <td style="text-align:center">
                        <select name="cboTax" id="cboTax" class="clsTax" style="width:100%">
                        <?php
                        echo $obj_common_get->getTaxCombo("");
                        ?>
                        </select><input type="hidden" id="hidTaxRate" class="clsTaxAmount" value="0"/></td>
                        <td style="text-align:center">
                        <select name="cboCostCenter" id="cboCostCenter" class="clsCostCener validate[required]" style="width:100%">
                        <?php
                        echo $obj_common_get->getCostCenterCombo("");
                        ?>
                        </select></td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
                </table>
                </div></td>
            </tr>
            <tr>
                <td colspan="2" class="normalfnt" valign="top">&nbsp;</td>
                <td class="normalfnt" style="text-align:right">&nbsp;</td>
                <td class="normalfnt" >
                <table width="100%" border="0">
                    <tr>
                        <td width="39%" class="normalfnt" style="text-align:right">Sub Total</td>
                        <td width="8%" class="normalfnt" style="text-align:right">:</td>
                        <td width="20%" class="normalfnt" style="text-align:right">&nbsp;</td>
                        <td width="33%" class="normalfnt"><span class="normalfnt" style="text-align:right">
                        <input name="txtSubTotal" type="text" disabled="disabled" id="txtSubTotal" style="width:100px;text-align:right" value="<?php echo number_format($totAmount, 2, '.', ''); ?>" />
                        </span></td>
                    </tr>
                    <tr>
                        <td class="normalfnt" style="text-align:right">Tax</td>
                        <td class="normalfnt" style="text-align:right">:</td>
                        <td class="normalfnt" style="text-align:right">&nbsp;</td>
                        <td class="normalfnt"><span class="normalfnt" style="text-align:right">
                        <input name="txtTaxTotal" type="text" disabled="disabled" id="txtTaxTotal" style="width:100px;text-align:right" value="<?php echo number_format($totTaxAmount, 2, '.', ''); ?>" />
                        </span></td>
                    </tr>
                    <tr>
                        <td class="normalfnt" style="text-align:right">Total</td>
                        <td class="normalfnt" style="text-align:right">:</td>
                        <td class="normalfnt" style="text-align:right">&nbsp;</td>
                        <td class="normalfnt"><span class="normalfnt" style="text-align:right">
                        <input name="txtTotal" type="text" disabled="disabled" id="txtTotal" style="width:100px;text-align:right" value="<?php echo number_format(($totAmount+$totTaxAmount), 2, '.', ''); ?>" />
                        </span></td>
                    </tr>
                </table>
                </td>
            </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="32" >
            <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            <tr>
            	<td align="center"><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($form_permision['edit']==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');?><a class="button white medium" id="butCancle" name="butCancle" <?php echo(($cancleMode!=1 || $header_arr['STATUS']==-2 || ($debitNoteNo=='' && $debitNoteYear==''))?'style="display:none"':''); ?> >Cancel</a><a class="button white medium" id="butReport" name="butReport" <?php echo(($debitNoteNo=='' && $debitNoteYear=='')?'style="display:none"':''); ?> >Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
            </tr>
            </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>

<?php 
session_start();
$backwardseperator 	= "../../../../";

$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];

$requestType 		= $_REQUEST['requestType'];
$programCode		= 'P0758';

include "../../../../dataAccess/Connector.php";
include_once "../../../../class/finance/cls_calculate_tax.php";
include_once "../../../../class/masterData/exchange_rate/cls_exchange_rate_get.php";
include_once "../../../../class/finance/customer/debit_note/cls_debit_note_set.php";
include_once "../../../../class/finance/customer/debit_note/cls_debit_note_get.php";

$obj_calculate_tax	= new Cls_Calculate_Tax($db);
$obj_exchgRate_get	= new cls_exchange_rate_get($db);
$obj_debitnote_set	= new Cls_Debit_Note_Set($db,$companyId,$locationId,$userId);
$obj_debitnote_get	= new Cls_Debit_Note_Get($db,$companyId);

if($requestType=='URLCalculateTax')
{
	$taxId		= $_REQUEST['taxId'];
	$amount		= ($_REQUEST['amount']==''?0:$_REQUEST['amount']);
	
	$taxAmount	= $obj_calculate_tax->CalculateTax($taxId,$amount);
	echo $taxAmount;
}
else if($requestType=='loadExchangeRate')
{
	$currencyId 			= $_REQUEST['currencyId'];
	$date 					= $_REQUEST['date'];
	
	$row					= $obj_exchgRate_get->GetAllValues($currencyId,4,$date,$companyId,'RunQuery');
	$response['exchgRate']	= $row['AVERAGE_RATE'];
	
	echo json_encode($response);
}
else if($requestType=='loadInvoiceType')
{
	$customerId 			= $_REQUEST['customerId'];
	
	$result					= $obj_debitnote_get->getInvoiceId($customerId);
	$row					= mysqli_fetch_array($result);
	$response['invType']	= $row['strInvoiceType'];
	
	echo json_encode($response);
}
else if($requestType=='saveData')
{	
	$savedStatus		= true;
	$savedMasseged		= '';	
	$error_sql			= '';
	
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	$arrDetails 		= json_decode($_REQUEST['arrDetails'],true);	
	
	echo $obj_debitnote_set->save($arrHeader,$arrDetails,$programCode);
}
/*else if($requestType=='cancleInvoice')
{	
	$billInvNo 		= $_REQUEST['billInvNo'];
	$billInvYear 	= $_REQUEST['billInvYear'];
	
	echo $obj_invoice_set->cancle($billInvNo,$billInvYear,$programCode);
}
else if($requestType=='loadCurrency')
{
	$supplierId 	= $_REQUEST['supplierId'];
	
	echo $obj_invoice_get->loadCurrency($supplierId);
}*/

?>
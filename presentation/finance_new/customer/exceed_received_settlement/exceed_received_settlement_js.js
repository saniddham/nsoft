// JavaScript Document
$(document).ready(function(){
	
	$("#frmExceedSettlement").validationEngine();
	
	$('#frmExceedSettlement #cboCustomer').die('change').live('change',loadData);
	$('#frmExceedSettlement #cboCurrency').die('change').live('change',loadData);
	$('#frmExceedSettlement .clsSettleAmt').die('keyup').live('keyup',checkMaxValue); 
	$('#frmExceedSettlement .clsSave').die('click').live('click',saveData);
	
});

function loadData()
{
	var customer	= $('#frmExceedSettlement #cboCustomer').val();
	var currency	= $('#frmExceedSettlement #cboCurrency').val();
	
	$("#frmExceedSettlement #tblInvDetails tr:gt(0)").remove();
	if(customer=='' || currency=='')
	{		
		return;
	}
	
	showWaiting();
	
	var url 	 = "controller.php?q=1124&requestType=loadData";
	var data 	 = "customer="+customer+"&currency="+currency;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				if(json.type=='pass')
				{
					$('#frmExceedSettlement #tblInvDetails tbody').html(json.gridDetail);
					hideWaiting();
				}
				else
				{
					$('#frmExceedSettlement #cboCustomer').validationEngine('showPrompt','No details to view','fail' /*'pass'*/);
					hideWaiting();
				}
				
			}
	});
	hideWaiting();
}
function checkMaxValue()
{
	var maxValue = parseFloat($(this).parent().parent().find('.clsToBeSettle').val());
	
	if(parseFloat($(this).val())>maxValue)
		$(this).val(maxValue);
}
function saveData()
{
	if(!IsProcessMonthLocked_date($('#frmExceedSettlement #txtDate').val()))
		return;
	
	var invoiceNo 		= $(this).parent().parent().attr('id');
	var settleAmount	= parseFloat($(this).parent().parent().find('.clsSettleAmt').val());
	var customer		= $('#frmExceedSettlement #cboCustomer').val();
	var currency		= $('#frmExceedSettlement #cboCurrency').val();
	var date			= $('#frmExceedSettlement #txtDate').val();
	var toBeSettle		= $(this).parent().parent().find('.clsToBeSettle').val();
	var obj				= this;
	
	if(!$('#frmExceedSettlement').validationEngine('validate'))
	{
		return;
	}
	
	if(settleAmount<=0)
	{
		$(this).parent().parent().find('.clsSettleAmt').validationEngine('showPrompt','Amount must greater than 0.','fail' /*'pass'*/);
		return;
	}
	showWaiting();
	
	var data 		= "requestType=saveData";
		var arrHeader 	= "{";
							arrHeader += '"invoiceNo":"'+invoiceNo+'",' ;
							arrHeader += '"settleAmount":"'+settleAmount+'",' ;
							arrHeader += '"customer":"'+customer+'",' ;
							arrHeader += '"date":"'+date+'",' ;
							arrHeader += '"currency":"'+currency+'"' ;
		arrHeader 	   += "}";
		var arrHeader	= arrHeader;
	
	data	   	    += "&arrHeader="+arrHeader;
	
	var url = "controller.php?q=1124";
	$.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:data,
			async:false,
			success:function(json){
				$(obj).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				if(json.type=='pass')
				{
					var t = setTimeout("alertx()",1000);
					$(obj).parent().parent().find('.exReceived').html(json.exceedReceive);
					$(obj).parent().parent().find('.tobeSettled').html(json.toBeSettle);
					$(obj).parent().parent().find('.clsToBeSettle').val(json.toBeSettle);
					$(obj).parent().parent().find('.clsSettleAmt').removeClass('validate[custom[number],max['+toBeSettle+']]');
					$(obj).parent().parent().find('.clsSettleAmt').addClass('validate[custom[number],max['+json.toBeSettle+']]');
					hideWaiting();
					return;
				}
				else
				{
					hideWaiting();
				}
			},
			error:function(xhr,status){
					
				$(obj).validationEngine('showPrompt', errormsg(xhr.status),'fail');
				hideWaiting();
				return;
			}		
	});	
}
function alertx()
{
	$('#frmExceedSettlement').validationEngine('hide')	;
}
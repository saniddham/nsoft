<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
try
{
	$requestType 		= $_REQUEST['requestType']; // request parameter
	$programCode		= 'P1124'; // program code
	$spMenuId			= 71; // back date permission menu id
	
	include_once "class/tables/menupermision.php";								$menupermision 					= new menupermision($db);
	include_once "class/tables/menus_special.php";								$menus_special 					= new menus_special($db);
	include_once "class/tables/mst_financeexchangerate.php";					$mst_financeexchangerate		= new mst_financeexchangerate($db);
	include_once "class/tables/sys_no.php";										$sys_no 						= new sys_no($db);
	include_once "class/dateTime.php";											$dateTimes 						= new dateTimes($db);
	include_once "class/tables/finance_customer_transaction.php";				$finance_customer_transaction 	= new finance_customer_transaction($db);
	include_once "class/tables/finance_transaction.php";						$finance_transaction 			= new finance_transaction($db);
	include_once "class/tables/finance_month_end_process.php";					$finance_month_end_process		= new finance_month_end_process($db);
	include_once "class/tables/finance_customer_exceed_settlement_header.php";	$finance_customer_exceed_settlement_header		= new finance_customer_exceed_settlement_header($db);
	
	
	if($requestType=='loadData')
	{
		$db->connect();
		
		$customer		= $_REQUEST['customer'];
		$currency		= $_REQUEST['currency'];
		$chkStatus		= false;
		
		$result			= getExceedReceivedDetail($customer,$currency);
		$html			= '';
		while($row = mysqli_fetch_array($result))
		{
			$invoiceNo			= $row['SERIAL_NO'];
			$invoiceYear		= $row['SERIAL_YEAR'];
			$concatInvNo		= $row['INVOICE_NO'];
			$invoiceDate		= $row['INVOICED_DATE'];
			$invRemarks			= $row['INVOICE_REMARKS'];
			$invValue			= $row['INVOICED_VALUE'];
			$creditValue		= $row['CREDIT_VALUE'];
			$advValue			= $row['ADVANCE_VALUE'];
			$receieveValue		= $row['RECEIVED_VALUE'];
			$exRecValue			= $row['EXCEED_RECEIVED'];
			$exToBeSettle		= $receieveValue-$row['BAL_VAL']-$exRecValue;
			
			$chkStatus			= true;
			
			$html .= "<tr id=\"".$concatInvNo."\">
						<td class=\"normalfnt\" style=\"text-align:center\" >".$concatInvNo."</td>
						<td class=\"normalfnt\" style=\"text-align:left\" >".$invRemarks."</td>
						<td class=\"normalfnt\" style=\"text-align:center\" >".$invoiceDate."</td>
						<td class=\"normalfnt\" style=\"text-align:right\" >".$invValue."</td>
						<td class=\"normalfnt\" style=\"text-align:right\" >".$creditValue."</td>
						<td class=\"normalfnt\" style=\"text-align:right\" >".$advValue."</td>
						<td class=\"normalfnt\" style=\"text-align:right\" >".$receieveValue."</td>
						<td class=\"normalfnt exReceived\" style=\"text-align:right\" >".$exRecValue."</td>
						<td class=\"normalfnt tobeSettled\" style=\"text-align:right\" >".$exToBeSettle."</td>
						<td class=\"normalfnt\" style=\"text-align:center\" ><input type=\"textbox\" id=\"txtSettleAmt\" class=\"clsSettleAmt validate[custom[number],max[".$exToBeSettle."]]\" style=\"width:90px;text-align:right\" value=\"0\"><input type=\"hidden\" id=\"hidSettleAmount\" class=\"clsToBeSettle\" value=\"".$exToBeSettle."\"></td>
						<td class=\"normalfnt\" style=\"text-align:center\" ><a class=\"button green small clsSave\" id=\"butSave\" name=\"butSave\">&nbsp;Save&nbsp;</a></td>
					</tr>";
				
		}
		if($chkStatus)
		{
			$response['type'] 		= 'pass';
			$response['gridDetail'] = $html;
		}
		else
		{
			$response['type'] 		= 'fail';
		}	
	}
	else if($requestType=='saveData')
	{
		$arrHeader 		= json_decode($_REQUEST['arrHeader'],true);
		
		$invoiceNoArr	= explode('-',$arrHeader['invoiceNo']);
		$invoiceNo		= $invoiceNoArr[0];
		$invoiceYear	= $invoiceNoArr[1];
		$settleAmount	= $arrHeader['settleAmount'];
		$customerId		= $arrHeader['customer'];
		$currency		= $arrHeader['currency'];
		$date			= $arrHeader['date'];
		
		$db->connect();$db->begin();//open connection.
		
		//check back date permission
		if($date < $dateTimes->getCurruntDate())
		{
			$backDate_perm	= $menus_special->loadSpecialMenuPermission($spMenuId,$sessions->getUserId());
			
			if($backDate_perm!=1)
				throw new Exception('No permission to backdate.');
		}
		
		//check month end process locked
		$mep_status		= $finance_month_end_process->getIsMonthEndProcessLocked($date,$sessions->getCompanyId());
		
		if($mep_status)
				throw new Exception("Month end Process locked for the date '".$date."'.");
		
		if(!$mst_financeexchangerate->checkCurrencyAvailableForSelectedDate($date,$sessions->getCompanyId()))
				throw new Exception("Please enter exchange rates for '".$date."' before save.");
		
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Edit','','');
		
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);	
		
		$row_bal	= getBalToSettleAmount($customerId,$currency,$invoiceNo,$invoiceYear);
		if($settleAmount > $row_bal['TO_BE_SETTLE'])
			throw new Exception("Settlement amount greater than to be settle.");
		
		$settleNo		= $sys_no->getSerialNoAndUpdateSysNo('EXCEED_SETTLE_NO',$sessions->getLocationId());
		$settleYear		= $dateTimes->getCurruntYear();
		
		$result_arr		= $finance_customer_exceed_settlement_header->insertRec($settleNo,$settleYear,$customerId,$currency,$invoiceNo,$invoiceYear,$settleAmount,1,$date,$sessions->getCompanyId(),$sessions->getLocationId(),$sessions->getUserId(),$dateTimes->getCurruntDateTime());
		
		if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
		
		
		$row_d			= $finance_customer_transaction->getExceedReceiveDetails($invoiceNo,$invoiceYear,$sessions->getCompanyId(),$currency,'PAYRECEIVE');
		
		$resultCTArr	= $finance_customer_transaction->insertRec($row_d['ORDER_YEAR'],$row_d['ORDER_NO'],$customerId,$currency,$settleYear,$settleNo,'EXRECEIVE',$invoiceYear,$invoiceNo,$row_d['LEDGER_ID'],$settleAmount,$sessions->getCompanyId(),$sessions->getLocationId(),$sessions->getUserId(),$date);
		
		if(!$resultCTArr['status'])
			throw new Exception($resultCTArr['msg']);
			
		
		$row_p			= $finance_customer_transaction->getLastPayReceiveNo($invoiceNo,$invoiceYear,$sessions->getCompanyId(),$currency);
		
		//get CREDIT transaction Data
		$row_tc			= $finance_transaction->getExceedTransactionDetails($row_p['DOCUMENT_NO'],$row_p['DOCUMENT_YEAR'],'PAYRECEIVE','C','CU',$customerId,$currency,$sessions->getCompanyId());
		
		$resultFTCArr	= $finance_transaction->insertRec($row_tc['CHART_OF_ACCOUNT_ID'],$settleAmount,$settleNo,$settleYear,'EXRECEIVE',$invoiceNo,$invoiceYear,'D','CU',$customerId,$currency,$row_tc['BANK_REFERENCE_NO'],$row_tc['REMARKS'],$row_tc['REC_STATUS'],$sessions->getLocationId(),$sessions->getCompanyId(),$sessions->getUserId(),$date,$dateTimes->getCurruntDateTime());
		
		if(!$resultFTCArr['status'])
			throw new Exception($resultFTCArr['msg']);
		
		//get DEBIT transaction Data
		$row_td			= $finance_transaction->getExceedTransactionDetails($row_p['DOCUMENT_NO'],$row_p['DOCUMENT_YEAR'],'PAYRECEIVE','D','CU',$customerId,$currency,$sessions->getCompanyId());
		
		$resultFTDArr	= $finance_transaction->insertRec($row_td['CHART_OF_ACCOUNT_ID'],$settleAmount,$settleNo,$settleYear,'EXRECEIVE',$invoiceNo,$invoiceYear,'C','CU',$customerId,$currency,$row_td['BANK_REFERENCE_NO'],$row_td['REMARKS'],$row_td['REC_STATUS'],$sessions->getLocationId(),$sessions->getCompanyId(),$sessions->getUserId(),$date,$dateTimes->getCurruntDateTime());
		
		if(!$resultFTDArr['status'])
			throw new Exception($resultFTDArr['msg']);
			
		//validate duplicate entry
		$sys_no->validateDuplicateSerialNoWithSysNo($debitNo,'EXCEED_SETTLE_NO',$sessions->getLocationId());
		
		$row_bal		= getBalToSettleAmount($customerId,$currency,$invoiceNo,$invoiceYear);
		
		$db->commit();
		$response['type'] 			= "pass";
		$response['msg'] 			= "Saved Successfully.";
		$response['exceedReceive']	= $row_bal['EXCEED_RECEIVED'];
		$response['toBeSettle']		= $row_bal['TO_BE_SETTLE'];		
	}
}
catch(Exception $e)
{
	$db->rollback();
	$response['msg'] 	=  $e->getMessage();
	$response['error'] 	=  $error_handler->jTraceEx($e);
	$response['type'] 	=  'fail';
	$response['sql']	=  $db->getSql();
	$db->disconnect();
}
$db->disconnect();
echo json_encode($response);

function getExceedReceivedDetail($customer,$currency)
{
	global $sessions;
	global $db;
	
	$sql = "SELECT 
			(INVOICED_VALUE-ADVANCE_VALUE-CREDIT_VALUE) AS BAL_VAL,
			tb1.*
			FROM
			(
				SELECT 
				CIH.SERIAL_NO										AS SERIAL_NO,
				CIH.SERIAL_YEAR										AS SERIAL_YEAR,
				CIH.INVOICE_NO										AS INVOICE_NO,
				CIH.INVOICED_DATE									AS INVOICED_DATE,
				CIH.REMARKS										AS INVOICE_REMARKS,
			
				ABS(ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'INVOICE'),0),2)) 		AS INVOICED_VALUE,
			
				ABS(ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'CREDIT'),0),2))			AS CREDIT_VALUE,
			
				ABS(ROUND(COALESCE((SELECT
								 SUM(SUB_CT.VALUE)
							   FROM finance_customer_transaction SUB_CT 
							   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
								AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
								AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
								AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
								AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'),0),2)) 		AS ADVANCE_VALUE,
			
				ABS(ROUND(COALESCE((SELECT
								 SUM(SUB_CT.VALUE)
							   FROM finance_customer_transaction SUB_CT 
							   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
								AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
								AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
								AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
								AND SUB_CT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2)) 	AS RECEIVED_VALUE,
			
				ABS(ROUND(COALESCE((SELECT
								 SUM(SUB_CT.VALUE)
							   FROM finance_customer_transaction SUB_CT 
							   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
								AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
								AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
								AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
								AND SUB_CT.DOCUMENT_TYPE = 'EXRECEIVE'),0),2)) 	AS EXCEED_RECEIVED
								
				FROM finance_customer_transaction CT
				INNER JOIN finance_customer_invoice_header CIH ON CIH.SERIAL_YEAR = CT.INVOICE_YEAR AND CIH.SERIAL_NO = CT.INVOICE_NO
				WHERE 
				CT.CUSTOMER_ID 	= $customer
				AND CT.CURRENCY_ID 	= $currency
				AND CT.COMPANY_ID 	= '".$sessions->getCompanyId()."' 
				GROUP BY
				CT.CUSTOMER_ID,
				CT.CURRENCY_ID,
				CIH.INVOICE_NO
			) AS tb1
			HAVING (RECEIVED_VALUE-EXCEED_RECEIVED) > BAL_VAL ";
	
	return $db->RunQuery($sql);
}
function getBalToSettleAmount($customerId,$currency,$invoiceNo,$invoiceYear)
{
	global $sessions;
	global $db;
	
	$sql = "SELECT 
			(RECEIVED_VALUE-(INVOICED_VALUE-ADVANCE_VALUE-CREDIT_VALUE)-EXCEED_RECEIVED) AS TO_BE_SETTLE,
			EXCEED_RECEIVED
			FROM
			(
				SELECT 
				ABS(ROUND(COALESCE((SELECT
				SUM(SUB_CT.VALUE)
				FROM finance_customer_transaction SUB_CT 
				WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
				AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
				AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
				AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
				AND SUB_CT.DOCUMENT_TYPE = 'INVOICE'),0),2)) 		AS INVOICED_VALUE,
				
				ABS(ROUND(COALESCE((SELECT
				SUM(SUB_CT.VALUE)
				FROM finance_customer_transaction SUB_CT 
				WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
				AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
				AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
				AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
				AND SUB_CT.DOCUMENT_TYPE = 'CREDIT'),0),2))			AS CREDIT_VALUE,
				
				ABS(ROUND(COALESCE((SELECT
				SUM(SUB_CT.VALUE)
				FROM finance_customer_transaction SUB_CT 
				WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
				AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
				AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
				AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
				AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'),0),2)) 		AS ADVANCE_VALUE,
				
				ABS(ROUND(COALESCE((SELECT
				SUM(SUB_CT.VALUE)
				FROM finance_customer_transaction SUB_CT 
				WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
				AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
				AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
				AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
				AND SUB_CT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2)) 	AS RECEIVED_VALUE,
				
				ABS(ROUND(COALESCE((SELECT
				SUM(SUB_CT.VALUE)
				FROM finance_customer_transaction SUB_CT 
				WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
				AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
				AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
				AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
				AND SUB_CT.DOCUMENT_TYPE = 'EXRECEIVE'),0),2)) 		AS EXCEED_RECEIVED
				
				FROM finance_customer_transaction CT
				INNER JOIN finance_customer_invoice_header CIH ON CIH.SERIAL_YEAR = CT.INVOICE_YEAR AND CIH.SERIAL_NO = CT.INVOICE_NO
				WHERE 
				CT.CUSTOMER_ID 	= $customerId
				AND CT.CURRENCY_ID 	= $currency
				AND CT.COMPANY_ID 	= '".$sessions->getCompanyId()."' 
				AND CIH.SERIAL_NO = $invoiceNo
				AND CIH.SERIAL_YEAR = $invoiceYear
				GROUP BY
				CT.CUSTOMER_ID,
				CT.CURRENCY_ID,
				CIH.INVOICE_NO
			) AS tb1";
	
	$result		= $db->RunQuery($sql);
	$row		= mysqli_fetch_array($result);
	return $row;
}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$session_locationId = $sessions->getLocationId();
$session_companyId 	= $sessions->getCompanyId();
$intUser  			= $sessions->getUserId();

$programCode		= 'P0709';
$reportId			= 1126;

include_once 		"class/tables/finance_customer_exceed_settlement_header.php";				$finance_customer_exceed_settlement_header 			= new finance_customer_exceed_settlement_header($db);
include_once 		"libraries/jqgrid2/inc/jqgrid_dist.php";

//BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
$arr =  json_decode($_REQUEST['filters'],true);

$arr = $arr['rules'];

$where_string = '';
$where_array = array(
				'exceedSettleNo'=>"CONCAT(CESH.SETTLE_NO,' / ',CESH.SETTLE_YEAR)",
				'invoiceNo'=>"CONCAT(CESH.INVOICE_NO,' / ',CESH.INVOICE_YEAR)",
				'CUSTOMER_ID'=>'CESH.CUSTOMER_ID',
				'CURRENCY_ID'=>'CESH.CURRENCY_ID'
				);
				
foreach($arr as $k=>$v)
{
	if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND CESH.SETTLE_DATE = '".date('Y-m-d')."'";
//END }

$sql = "SELECT SUB_1.* FROM
			(SELECT 	
			CESH.SETTLE_NO,
			CESH.SETTLE_YEAR,
			CONCAT(CESH.SETTLE_NO,' / ',CESH.SETTLE_YEAR) AS settleNo,
			CONCAT(CESH.INVOICE_NO,' / ',CESH.INVOICE_YEAR) AS invoiceNo,
			FCIH.REMARKS AS invoiceRemarks,
			FCIH.INVOICED_DATE AS invoiceDate,
			CESH.CUSTOMER_ID,
			CESH.CURRENCY_ID,
			MC.strName AS customer,
			FC.strCode AS currency,
			CESH.SETTLE_DATE AS settleDate,
			ROUND(CESH.SETTLE_AMOUNT,2) AS settleAmount,
			'View' AS VIEW
			FROM 
			finance_customer_exceed_settlement_header AS CESH
			INNER JOIN mst_financecurrency FC ON FC.intId=CESH.CURRENCY_ID
			INNER JOIN mst_customer MC ON MC.intId=CESH.CUSTOMER_ID
			INNER JOIN finance_customer_invoice_header FCIH ON FCIH.SERIAL_NO = CESH.INVOICE_NO AND FCIH.SERIAL_YEAR = CESH.INVOICE_YEAR
			WHERE CESH.COMPANY_ID = '$session_companyId'
			$where_string
		)  
		AS SUB_1 WHERE 1=1";
//echo $sql.'<br>';
$jq = new jqgrid('',$db);	

$cols	= array();
$col	= array();

$col["title"] 			= "ExceedSettle No";
$col["name"] 			= "SETTLE_NO";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "ExceedSettle Year";
$col["name"] 			= "SETTLE_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "ExceedSettle No";
$col["name"] 			= "settleNo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Invoice No";
$col["name"] 			= "invoiceNo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Invoice Remarks";
$col["name"] 			= "invoiceRemarks";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Invoiced Date";
$col["name"] 			= "invoiceDate";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Customer";
$col["name"] 			= "customer";
$col["width"] 			= "7";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["dbname"] 			= "CUSTOMER_ID";
$customer_lookup 		= $jq->get_dropdown_values("SELECT DISTINCT 
													intId AS k,
													strName AS v
													FROM mst_customer
													WHERE intStatus = 1
													ORDER BY strName");
$col["stype"] 			= "select";
$str 					= ":;".$customer_lookup;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "currency";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["dbname"] 			= "CURRENCY_ID";
$currency_lookup 		= $jq->get_dropdown_values("SELECT DISTINCT 
													intId AS k,
													strCode AS v
													FROM mst_financecurrency
													WHERE intStatus = 1
													ORDER BY intId");
$col["stype"] 			= "select";
$str 					= ":;".$currency_lookup;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;
$col					= NULL;

$col["title"] 			= "Settle Date";
$col["name"] 			= "settleDate";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Amount";
$col["name"] 			= "settleAmount";
$col["width"] 			= "3";
$col["align"] 			= "right"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "View";
$col["name"] 			= "VIEW";
$col["width"] 			= "1";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= '?q='.$reportId.'&settleNo={SETTLE_NO}&settleYear={SETTLE_YEAR}';
$col["linkoptions"] 	= "target='rpt_exceed_received_settlement.php'";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Exceed Settlement Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'SETTLE_YEAR,SETTLE_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Customer Exceed Settlement Listing</title>
<?php
echo $out;

?>
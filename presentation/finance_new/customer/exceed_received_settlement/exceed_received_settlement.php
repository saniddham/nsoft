<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/tables/mst_customer.php";								$mst_customer				= new mst_customer($db);
include_once "class/tables/mst_financecurrency.php";						$mst_financecurrency		= new mst_financecurrency($db);
include_once "class/tables/menus_special.php";								$menus_special 				= new menus_special($db);

$spMenuId		= 71;
$backDate_perm	= $menus_special->loadSpecialMenuPermission($spMenuId,$sessions->getUserId());
?>

<title>Exceed Received Settlement</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />

<form id="frmExceedSettlement" name="frmExceedSettlement" method="post">
<div align="center">
<div class="trans_layoutXL">
<div class="trans_text">Exceed Received Settlement</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td width="8%" class="normalfnt">Customer <span class="compulsoryRed">*</span></td>
                <td width="27%" class="normalfnt"><select name="cboCustomer" id="cboCustomer"  style="width:300px" class="validate[required]" >
                <?php
					echo $mst_customer->getCombo(NULL,'intStatus = 1 ORDER BY strName');
				?>
                </select></td>
                <td width="11%" class="normalfnt">Currency <span class="compulsoryRed">*</span></td>
                <td width="13%" class="normalfnt"><select name="cboCurrency" id="cboCurrency"  style="width:100px" class="validate[required] cls_cbo_CurrencyCalExchangeRate" >
                <?php
					echo $mst_financecurrency->getExceedReceiveCurrencyCombo(NULL,$sessions->getCompanyId());
				?>
                </select></td>
                <td width="9%" class="normalfnt">Rate</td>
                <td width="14%" class="normalfnt"><input name="txtCurrencyRate" id="txtCurrencyRate" type="text" value="<?php echo number_format($exchgRate, 4, '.', ''); ?>" style="width:98px;text-align:right" disabled="disabled" class="cls_txt_exchangeRate"/></td>
                <td width="7%" class="normalfnt">Date</td>
                <td width="11%" class="normalfnt"><input name="txtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtDate" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($backDate_perm!=1?'disabled="disabled"':''); ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>   
            </tr>
            <tr>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="8" class="normalfnt">
              	<table width="100%" class="bordered" id="tblInvDetails" >
                	<thead>
                        <tr>
                            <th width="10%" >Invoice No</th>
                            <th width="20%" >Invoice Remarks</th>
                            <th width="7%" >Invoice Date</th>
                            <th width="8%" >Invoiced</th>
                            <th width="8%" >Credit Note</th>
                            <th width="8%" >Advanced</th>
                            <th width="8%" >Recieved</th>
                            <th width="8%" >Exceed Settled</th>
                            <th width="8%" >Exceed to Be Settle</th>
                            <th width="8%" >Settle Amount</th>
                            <th width="7%" >&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    	
                    </tbody>
                </table>
              </td>
         	</tr>
        </table>
    </tr>
    <tr>
    <td height="32" >
        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            <tr>
                <td align="center" ><a class="button white medium" id="butNew" name="butNew" href="?q=1124">New</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
            </tr>
        </table>
    </td>
    </tr>
</table>	
</div>
</div>
</form>
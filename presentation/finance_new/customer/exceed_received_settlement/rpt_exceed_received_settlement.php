<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once "class/tables/mst_customer.php";								$mst_customer								= new mst_customer($db);
require_once "class/tables/mst_financedimension.php";						$mst_financedimension						= new mst_financedimension($db);
require_once "class/tables/mst_financecurrency.php";						$mst_financecurrency						= new mst_financecurrency($db);
require_once "class/tables/finance_customer_exceed_settlement_header.php";	$finance_customer_exceed_settlement_header	= new finance_customer_exceed_settlement_header($db);
require_once "class/tables/finance_customer_invoice_header.php";			$finance_customer_invoice_header			= new finance_customer_invoice_header($db);


$programCode		= 'P1124'; // program code

$db->connect();

$settleNo				= $_REQUEST['settleNo'];
$settleYear				= $_REQUEST['settleYear'];

$finance_customer_exceed_settlement_header->set($settleNo,$settleYear);
$locationId				= $finance_customer_exceed_settlement_header->getLOCATION_ID();
$invoiceNo				= $finance_customer_exceed_settlement_header->getINVOICE_NO();
$invoiceYear			= $finance_customer_exceed_settlement_header->getINVOICE_YEAR();
$settleAmount			= $finance_customer_exceed_settlement_header->getSETTLE_AMOUNT();
$settleDate				= $finance_customer_exceed_settlement_header->getSETTLE_DATE();
$customer				= $finance_customer_exceed_settlement_header->getCUSTOMER_ID();
$currencyId				= $finance_customer_exceed_settlement_header->getCURRENCY_ID();

$finance_customer_invoice_header->set($invoiceNo,$invoiceYear);
$invoiceRemarks			= $finance_customer_invoice_header->getREMARKS();
$invoiceDate			= $finance_customer_invoice_header->getINVOICED_DATE();

$mst_customer->set($customer);
$customerName			= $mst_customer->getstrName();

$mst_financecurrency->set($currencyId);
$currency				= $mst_financecurrency->getstrCode();


$db->disconnect();
?>
<title>Exceed Receive Settlement</title>

<form id="frmExceedReceiveSettlement" name="frmAdvanceSettlement" method="post">
    <table width="800" align="center">
      <tr>
        <td width="100%"><?php 	$db->connect();
						include 'reportHeader.php';
					$db->disconnect();	
					
					$locationId 	= $sessions->getLocationId();		
					?></td>
      </tr>
      <tr>
        <td style="text-align:center">&nbsp;</td>
      </tr>
      <tr>
        <td style="text-align:center"><strong>EXCEED RECEIVE SETTLEMENT</strong></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td width="17%">&nbsp;</td>
            <td width="1%">&nbsp;</td>
            <td width="46%">&nbsp;</td>
            <td width="14%">&nbsp;</td>
            <td width="1%">&nbsp;</td>
            <td width="21%">&nbsp;</td>
          </tr>
          <tr>
            <td>Exceed Settle No</td>
            <td>:</td>
            <td><?php echo $settleNo.' / '.$settleYear; ?></td>
            <td>Settle Date</td>
            <td>:</td>
            <td><?php echo $settleDate?></td>
          </tr>
          <tr>
            <td>Customer</td>
            <td>:</td>
            <td><?php echo $customerName; ?></td>
            <td>Currency</td>
            <td>:</td>
            <td><?php echo $currency; ?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td ><table width="100%" border="0" class="rptBordered" id="tblMain">
        <thead>
            <tr>
                <th width="19%">Invoice No</th>
                <th width="43%">Invoice Remarks</th>
                <th width="20%">Invoice Date</th>
                <th width="18%">Settled Amount</th>
            </tr>
        </thead>        
        <tbody>
            <tr>
                <td style="text-align:left"><?php echo $invoiceNo.' - '.$invoiceYear; ?></td>
                <td style="text-align:left"><?php echo $invoiceRemarks; ?></td>
                <td style="text-align:center"><?php echo $invoiceDate; ?></td>
                <td style="text-align:right"><?php echo number_format($settleAmount,2); ?></td>
            </tr>
        </tbody>
        </table></td>
      </tr>
      <tr>
        <td class="normalfnt">&nbsp;</td>
      </tr>
      </table>
</form>
</body>

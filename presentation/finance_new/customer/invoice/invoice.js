var basePath	= "presentation/finance_new/customer/invoice/";
var reportId	= 894;
var menuId		= 701;
var COMMERCIAL_INVOICE = 1;
var TAX_INVOICE = 2;
var SVAT_INVOICE = 3;
var INVOICE = 5;
$(document).ready(function() {
	
	$('#frmSalesInvoice #txtCustomerPONo').focus();
	$('#frmSalesInvoice').validationEngine();	
		
	$('#frmSalesInvoice #butSave').die('click').live('click',Save);
	$('#frmSalesInvoice #butNew').die('click').live('click',New);
	//$('#frmSalesInvoice .cls_cbo_tax').die('change').live('change',CalculateTax);
	$('#frmSalesInvoice .cls_cbo_tax').die('change').live('change',function(){
		CalculateTax($(this));
	});
	
	/*$('#frmSalesInvoice .cls_td_invoice').die('keyup').live('keyup',function(){
		alert('okkk');
		var obj = $(this);
		apply_percentages(obj);
		CalculateTax(obj);
		CaculateTotalValue();
		
	});*/
	
	$('#frmSalesInvoice #cboCustomer').die('change').live('change',LoadMultiData);
	$('#frmSalesInvoice #btnSearch').die('click').live('click',LoadData);
	
	$('#frmSalesInvoice #butCancel').die('click').live('click',Cancel);
	$('#frmSalesInvoice #butReport').die('click').live('click',Report);
	$('#frmSalesInvoice #tblGL #butAddNewGL').die('click').live('click',AddNewGL);
	$('#frmSalesInvoice #tblGL .cls_GLAmount').die('keyup').live('keyup',function(){ CalculateGLTotals(); });
	$('#frmSalesInvoice #tblMain .cls_check_all').die('click').live('click',CheckAll);
	
	$('#frmSalesInvoice #tblMain .cls_check').die('click').live('click',function(){
		ValidateRowWhenCheck($(this));
	});
	
    $('#frmSalesInvoice #txtCustomerPONo').die('keyup').live('keyup',function(e){
			if(e.which==13)
				LoadData();
	});

	$('#frmSalesInvoice #tblMain input').die('keyup').live('keyup',function(e){
			if($(this).val()=="" || isNaN($(this).val())){
				$(this).val(0);
				$(this).select();
				
			}
	});
	
	$('#frmSalesInvoice .cls_cbo_percentage').die('change').live('change',function()
	{
		var obj = $(this);
		apply_percentages(obj);
		CalculateTax(obj);
		CaculateTotalValue();
	
	
	});
	

	$('#frmSalesInvoice #tblGL input').die('keyup').live('keyup',function(e){			
/*	   if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
			this.value = this.value.replace(/[^0-9\.]/g, '');
		}
*/		if($(this).val()=="" || isNaN($(this).val())){
			$(this).val(0);
			$(this).select();
		}
	});	
	
	$('#frmSalesInvoice #tblGL .removeRow').die('click').live('click',function(){
		if($(this).parent().parent().attr('class')=="")
		 	$(this).parent().parent().remove();
		CalculateGLTotals();
	});
	
	$('#frmSalesInvoice #tblMain .cls_Subtract').die('keyup').live('keyup',function(){	
		var price			= parseFloat($(this).parent().find('.cls_td_price').html());
		var dispatchQty		= parseFloat($(this).parent().find('.cls_td_qty').html());
		var pdQty			= parseFloat($(this).parent().find('.cls_td_PD').children().val());
		var fdQty			= parseFloat($(this).parent().find('.cls_td_FD').children().val());
		var sampleQty		= parseFloat($(this).parent().find('.cls_td_sample').children().val());
		var missingPanal	= parseFloat($(this).parent().find('.cls_td_missingPanal').children().val());
		var other			= parseFloat($(this).parent().find('.cls_td_other').children().val());
		
		var invoieQty		= dispatchQty - (pdQty + fdQty + sampleQty + missingPanal + other);
		$(this).parent().find('.cls_td_invoice').children().val(invoieQty);
		$(this).parent().find('.cls_td_value').html(RoundNumber(invoieQty*price,4));			
		
		CaculateTotalValue();
	});
	
	$('#frmSalesInvoice #tblMain .cls_td_invoice').die('keyup').live('keyup',function(){
	
		var obj =$(this).children()
	
		var dispatchQty		= parseFloat($(this).parent().find('.cls_td_qty').html());
		var pdQty			= parseFloat($(this).parent().find('.cls_td_PD').children().val());
		var fdQty			= parseFloat($(this).parent().find('.cls_td_FD').children().val());
		var sampleQty		= parseFloat($(this).parent().find('.cls_td_sample').children().val());
		var missingPanal	= parseFloat($(this).parent().find('.cls_td_missingPanal').children().val());
		var other			= parseFloat($(this).parent().find('.cls_td_other').children().val());
		var invoieQty		= parseFloat($(this).parent().find('.cls_td_invoice').children().val());
		invoieQty 			= invoieQty + pdQty + fdQty + sampleQty + missingPanal + other;
		var balance			= dispatchQty - (pdQty + fdQty + sampleQty + missingPanal + other);
		
		/*if(dispatchQty < invoieQty){
			//$(this).parent().find('.cls_td_invoice').children().val(balance);
			$(this).parent().find('.cls_td_invoice').children().select();
		}*/
			
		var price			= parseFloat($(this).parent().find('.cls_td_price').html());
		var invoieQty		= parseFloat($(this).parent().find('.cls_td_invoice').children().val());
		$(this).parent().find('.cls_td_invoice').children().val(invoieQty);
		//$(this).parent().find('.cls_td_value').html(RoundNumber(invoieQty*price,4));			
		
		apply_percentages(obj);
		CalculateTax(obj);
		
		CaculateTotalValue();
	});
	
    if(customerPOAL!='') {
		LoadAutoData(customerPOAL);
    }


    $( "#btnModal" ).click(function() {


        var arr ="[";
        var i=0;
        var data= '';
        var x=0;
        var checked=0;

        //rowCount = document.getElementById('tbl_intervals_main').rows.length-2;
        $('#tbl_dispatch >tbody >tr').not("#first").each(function() {


            if ($(this).find('#modal_check').is(":checked"))
            {
                arr += "{";
                checked = checked +1 ;


                var fabricDetails =  $(this).find('#fabricDetails').val();
                var salesOrder =  $(this).find('#salesOrder').val();
                var part =  $(this).find('#part').val();
                var graphic_no =  $(this).find('#graphic_no').val();
                var groundColour =  $(this).find('#groundColour').val();
                var lineNo =  $(this).find('#lineNo').val();
                var  CutNo =  $(this).find('#CutNo').val();
                var Size =  $(this).find('#Size').val();
                var  DispatchGoodQty =  $(this).find('#DispatchGoodQty').val();
                var  ProductionDamage =  $(this).find('#ProductionDamage').val();
                var FabricDamge =  $(this).find('#FabricDamge').val();
                var  SampleNo =  $(this).find('#SampleNo').val();
                var missingPanel =  $(this).find('#missingPanel').val();
                var other =  $(this).find('#other').val();
                var  invoiceQty =  $(this).find('#invoiceQty').val();
                var   invoicePrice =  $(this).find('#invoicePrice').val();
                var   invoiceValue =  $(this).find('#invoiceValue').val();
                var   salesOrderId =  $(this).find('#salesOrderId').val();
                var GLAccount =  $(this).find('#GLAccount').val();
                var  TaxCode =  $(this).find('#TaxCode').val();
                var  CostCenter =  $(this).find('#CostCenter').val();
                var  SampleQty =  $(this).find('#SampleQty').val();
                var  SerialYear =  $(this).find('#SerialYear').val();
                var   SerialNo =  $(this).find('#SerialNo').val();
                var   TaxValue =  $(this).find('#TaxValue').val();


                if(fabricDetails == '/') {
                    fabricDetails ='';
                }

                arr += '"fabricDetails":"'+				fabricDetails +'",' 	;

                arr += '"salesOrder":"'+				salesOrder +'",' 	;
                arr += '"part":"'+				part +'",' 	;
                arr += '"graphic_no":"'+				graphic_no +'",' 	;
                arr += '"groundColour":"'+				groundColour +'",' 	;
                arr += '"lineNo":"'+				lineNo +'",' 	;
                arr += '"CutNo":"'+				CutNo +'",' 	;
                arr += '"Size":"'+				Size +'",' 	;
                arr += '"DispatchGoodQty":"'+				DispatchGoodQty +'",' 	;
                arr += '"ProductionDamage":"'+				ProductionDamage +'",' 	;
                arr += '"FabricDamge":"'+				FabricDamge +'",' 	;
                arr += '"SampleNo":"'+				SampleNo +'",' 	;
                arr += '"missingPanel":"'+				missingPanel +'",' 	;
                arr += '"other":"'+				other +'",' 	;
                arr += '"invoiceQty":"'+				invoiceQty +'",' 	;
                arr += '"invoicePrice":"'+				invoicePrice +'",' 	;
                arr += '"invoiceValue":"'+				invoiceValue +'",' 	;
                arr += '"salesOrderId":"'+				salesOrderId +'",' 	;
                arr += '"GLAccount":"'+				GLAccount +'",' 	;
                arr += '"TaxCode":"'+				TaxCode +'",' 	;
                arr += '"CostCenter":"'+				CostCenter +'",' 	;
                arr += '"SampleQty":"'+				SampleQty +'",' 	;
                arr += '"SerialYear":"'+				SerialYear +'",' 	;
                arr += '"SerialNo":"'+				SerialNo +'",' 	;
                arr += '"TaxValue":"'+				TaxValue +'"' 	;

                // arr += '"TaxValue":"'+ 				TaxValue
                arr += "},";
            }




            x++;
        });

        if(checked == 0){
            alert("SELECT EITHER ONE OPTION");
            return false;
        }





        arr = arr.substr(0,arr.length-1);
        arr += "]";
       var Sid= $('#Sid').val();
       var orderNo= $('#orderNohidden').val();
       var orderYear= $('#orderYearhidden').val();

        var SerialNo =$('#frmSalesInvoice #txtInvoiceNo').val();
        var Sno=SerialNo.split("-");

        data+="&arr="+arr;
        data+="&SalesorderId="+Sid ;
        data+="&orderNo="+orderNo ;
        data+="&orderYear="+orderYear ;
        data+="&SerialNo="+Sno[0];
        data+="&SerialYear="+Sno[1];

        var url 	= basePath+"invoice_db.php?RequestType=AddModalData";
        var httpobj = $.ajax({
            url:url,
            data:data,
            dataType:'json',
            type: "POST",
            async:false,
            success:function(json)
            {

                if(json.type=='pass')
                {
                    hideWaiting();
                    $('#btnModal').validationEngine('showPrompt',json.msg,json.type);

                    var t = setTimeout("alertx('butSave')",4000);
                }
                else
                {
                    hideWaiting();
                    $('#btnModal').validationEngine('showPrompt',json.msg,json.type);
                    var t = setTimeout("alertx('butSave')",4000);
                }
            },
            error:function(xhr,status)
            {
                hideWaiting();
                $('#frmSalesInvoice #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                var t = setTimeout("alertx('butSave')",4000);
            }
        });




    });




});



function Report()
{
	if($('#frmSalesInvoice #txtInvoiceNo').val()=="")
	{
		alert("No details available to view report.");
		return;
	}
	var serial = $('#frmSalesInvoice #txtSerialNo').val().split('-');
	var url  = "?q="+reportId+"&SerialNo="+serial[0];
	    url += "&SerialYear="+serial[1];
	window.open(url,'rptinvoice.php');
}

function CaculateTotalValue()
{
	var totalValue		= 0;
	var grandTotal		= 0;
	var taxValue		= 0;
	
	$('#frmSalesInvoice #tblMain .cls_td_value').each(function(){
		if($(this).parent().find('.cls_td_check').children().is(':checked'))
		{
			totalValue += parseFloat($(this).html());
			taxValue   += parseFloat($(this).parent().find('.cls_td_tax').attr('id'));
		}
	});
	taxValue	= parseFloat(RoundNumber(taxValue,4));
	totalValue	= parseFloat(RoundNumber(totalValue,4));
	grandTotal	= totalValue + taxValue;
	
	$('#frmSalesInvoice #txtTotTax').val(RoundNumber(taxValue,2));	
	$('#frmSalesInvoice #txtSubTotal').val(RoundNumber(totalValue,2));	
	$('#frmSalesInvoice #txtGrandTotal').val(RoundNumber(grandTotal,2));
}

function LoadData()
{
	$("#frmSalesInvoice #tblMain tr:gt(0)").remove();
	ClearObjects();
	var cusPONo	= $('#frmSalesInvoice #txtCustomerPONo').val();
    var companyInvoice = $('#companyInvoiceType').val();
	var arrHeader = "{";
						arrHeader += '"CustomerPoNo":'+URLEncode_json(cusPONo.trim())+'';
		arrHeader += "}";
	
	var arrHeader	= arrHeader;
	
	var url 	= basePath+"invoice_db.php?RequestType=URLLoadData";
	
	var httpobj = $.ajax({
	url:url,
	data:"&arrHeader="+arrHeader,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		if(json.Type=="Fail")
		{
			alert(json.MSG);
			$('#frmSalesInvoice #cboCustomer').html(json.CustomerHTML);	
			return;
		}
		//alert(json.CUSTOMER_INV_LOC_HTML);
		$('#frmSalesInvoice #cboCustomer').html(json.CustomerHTML);
		$('#frmSalesInvoice #cboCustomerInvLoc').html(json.CUSTOMER_INV_LOC_HTML);
		if (companyInvoice == SVAT_INVOICE && json.InvoiceType == SVAT_INVOICE){
            $('#frmSalesInvoice #cboInvoiceType').val(SVAT_INVOICE);
		} else if (companyInvoice == INVOICE) {
            $('#frmSalesInvoice #cboInvoiceType').val(INVOICE);
		} else {
            $('#frmSalesInvoice #cboInvoiceType').val(TAX_INVOICE);
		}
		$('#frmSalesInvoice #td_order').html(json.OrderNo_HTML);
		$('#frmSalesInvoice #txtMarketer').val(json.Marketer);
		$('#frmSalesInvoice #cboCurrency').val(json.CurrencyId);
            $('#frmSalesInvoice #orderNo').val(json.ORDER_NO);
            $('#frmSalesInvoice #orderYear').val(json.OrderYear);
            GetCommonExchangeRate();
            $('#frmSalesInvoice #tblGL .cls_td_GL').children().val(json.ChartOfAccountID);
            var length 		= json.GRID.length;
            var arrayGrid 	= json.GRID;
            for(var i=0;i<length;i++)
            {
                CreateGrid(arrayGrid[i]['SALES_ORDER_ID'],arrayGrid[i]['SALES_ORDER_NO'],arrayGrid[i]['GRAPHIC_NO'],arrayGrid[i]['STYLE_NO'],arrayGrid[i]['PART'],arrayGrid[i]['QTY'],arrayGrid[i]['PRICE'],arrayGrid[i]['PERCENTAGE_HTML'],arrayGrid[i]['VALUE'],arrayGrid[i]['TAX_HTML'],arrayGrid[i]['COSTCENTER'],arrayGrid[i]['GL_HTML']);
            }
            CaculateTotalValue();
        }
    });
}

function LoadMultiData()
{
	$("#tblMain tr:gt(0)").remove();
    var companyInvoice = $('#companyInvoiceType').val();
	
	var url 	= basePath+"invoice_db.php?RequestType=URLLoadMultiData";
	var data 	= "CustomerPoNo="+$('#frmSalesInvoice #txtCustomerPONo').val();
		data   += "&CustomerId="+$('#frmSalesInvoice #cboCustomer').val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		if(json.Type=="Fail")
		{
			alert(json.MSG);
			//$('#frmSalesInvoice #cboCustomer').html(json.CustomerHTML);	
			return;
		}
		
		$('#frmSalesInvoice #cboCustomer').html(json.CustomerHTML);
		$('#frmSalesInvoice #cboCustomerInvLoc').html(json.CUSTOMER_INV_LOC_HTML);
        if (companyInvoice == SVAT_INVOICE && json.InvoiceType == SVAT_INVOICE){
            $('#frmSalesInvoice #cboInvoiceType').val(SVAT_INVOICE);
        } else {
            $('#frmSalesInvoice #cboInvoiceType').val(companyInvoice);
        }
		$('#frmSalesInvoice #td_order').html(json.OrderNo_HTML);
		$('#frmSalesInvoice #txtMarketer').val(json.Marketer);
		$('#frmSalesInvoice #cboCurrency').val(json.CurrencyId);
		$('#frmSalesInvoice #cboCurrency').change();
		$('#frmSalesInvoice #tblGL .cls_td_GL').children().val(json.ChartOfAccountID);
		var length 		= json.GRID.length;
		var arrayGrid 	= json.GRID;
		for(var i=0;i<length;i++)
		{
			CreateGrid(arrayGrid[i]['SALES_ORDER_ID'],arrayGrid[i]['SALES_ORDER_NO'],arrayGrid[i]['GRAPHIC_NO'],arrayGrid[i]['STYLE_NO'],arrayGrid[i]['PART'],arrayGrid[i]['QTY'],arrayGrid[i]['PRICE'],arrayGrid[i]['PERCENTAGE_HTML'],arrayGrid[i]['VALUE'],arrayGrid[i]['TAX_HTML'],arrayGrid[i]['COSTCENTER'],arrayGrid[i]['GL_HTML']);
		}
		CaculateTotalValue();
	}
	});
}

function CreateGrid(salesOrderId,salesOrderNo,graphicNo,styleNo,part,qty,price,percentage,value,taxHTML,costCenter,GLHTML)
{
	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);		
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'cls_td_check';
	cell.innerHTML  = "<input type=\"checkbox\" checked=\"checked\" class=\"cls_check\">";
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'cls_td_salesOrderNo';
	cell.id			= salesOrderId;
    cell.innerHTML 	= salesOrderNo+"<input type='hidden' name='Snohidden' id='Snohidden' value='"+salesOrderNo+"'>";
	
	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:left');
    cell.className	= 'btn_modal';
    cell.id			= salesOrderId;

    var orderNo= $('#orderNo').val();
    var orderYear = $('#orderYear'). val();
    var SerialNo = $('#txtInvoiceNo').val();
    var method ="getDispatchNote("+orderNo+','+orderYear+',"'+salesOrderNo+'",'+salesOrderId+")";
    cell.innerHTML 	= "<button type='button' class='btn btn-info btn-lg' onclick='"+method+"' data-toggle='modal'  id='myBtn_0'>+</button>";

    var cell 		= row.insertCell(3);
    cell.setAttribute("style",'text-align:left');
    cell.innerHTML 	= graphicNo;

    var cell 		= row.insertCell(4);
    cell.setAttribute("style",'text-align:left');
    cell.innerHTML 	= styleNo;

    var cell 		= row.insertCell(5);
    cell.setAttribute("style",'text-align:left');
    cell.innerHTML 	= part;

    var cell 		= row.insertCell(6);
    cell.setAttribute("style",'text-align:right');
    cell.className	= 'cls_td_qty';
    cell.innerHTML 	= qty;

    var cell 		= row.insertCell(7);
    cell.setAttribute("style",'text-align:center');
    cell.className	= 'cls_td_PD cls_Subtract';
    cell.innerHTML 	= "<input type=\"textbox\" style=\"width:50px;text-align:right\" value=\"0\">";

    var cell 		= row.insertCell(8);
    cell.setAttribute("style",'text-align:center');
    cell.className	= 'cls_td_FD cls_Subtract';
    cell.innerHTML 	= "<input type=\"textbox\" style=\"width:50px;text-align:right\" value=\"0\">";

    var cell 		= row.insertCell(9);
    cell.setAttribute("style",'text-align:center');
    cell.className	= 'cls_td_sample cls_Subtract';
    cell.innerHTML 	= "<input type=\"textbox\" style=\"width:50px;text-align:right\" value=\"0\">";

    var cell 		= row.insertCell(10);
    cell.setAttribute("style",'text-align:center');
    cell.className	= 'cls_td_missingPanal cls_Subtract';
    cell.innerHTML 	= "<input type=\"textbox\" style=\"width:50px;text-align:right\" value=\"0\">";

    var cell 		= row.insertCell(11);
    cell.setAttribute("style",'text-align:center');
    cell.className	= 'cls_td_other cls_Subtract';
    cell.innerHTML 	= "<input type=\"textbox\" style=\"width:50px;text-align:right\" value=\"0\">";

    var cell 		= row.insertCell(12);
    cell.setAttribute("style",'text-align:center');
    cell.className	= 'cls_td_invoice';
    cell.innerHTML 	= "<input type=\"textbox\" style=\"width:50px;text-align:right\" value=\""+qty+"\">";

    var cell 		= row.insertCell(13);
    cell.setAttribute("style",'text-align:right');
    cell.className	= 'cls_td_price';
    cell.innerHTML 	= price;
	
	 var cell 		= row.insertCell(14);
    cell.setAttribute("style",'text-align:right');
    cell.className	= 'cls_td_percentage';
    cell.innerHTML 	= percentage;

    var cell 		= row.insertCell(15);
    cell.setAttribute("style",'text-align:right');
    cell.className	= 'cls_td_value';
    cell.innerHTML 	= value;

    var cell 		= row.insertCell(16);
    cell.setAttribute("style",'text-align:center');
    cell.className	= 'cls_td_tax';
    cell.id			= 0;
    cell.innerHTML 	= taxHTML;

    var cell 		= row.insertCell(17);
    cell.setAttribute("style",'text-align:center');
    cell.className	= 'cls_td_costCenter';
    cell.innerHTML 	= costCenter;

    var cell 		= row.insertCell(18);
    cell.setAttribute("style",'text-align:center');
    cell.className	= 'cls_td_GL';
    cell.innerHTML 	= GLHTML;
}

function Save()
{		
	if(!IsProcessMonthLocked_date($('#frmSalesInvoice #txtInvoiceDate').val()))
		return;

	showWaiting();
	
	if(!Validate_save()){
		setTimeout("AutoHideFormValidate()",4000);
		hideWaiting();
		return;
	}

	var arr		= "[";
	$('#frmSalesInvoice #tblMain .cls_td_salesOrderNo').each(function(){
		if($(this).parent().find('.cls_td_check').children().is(':checked'))
		{
			arr += "{";
			arr += '"SalesOrderId":"'+$(this).attr('id')+'",' ;
			arr += '"Dispatch":"'+$(this).parent().find('.cls_td_qty').html()+'",';
			arr += '"PDQty":"'+$(this).parent().find('.cls_td_PD').children().val()+'",';
			arr += '"FDQty":"'+$(this).parent().find('.cls_td_FD').children().val()+'",';
			arr += '"Sample":"'+$(this).parent().find('.cls_td_sample').children().val()+'",';
			arr += '"MissingPanal":"'+$(this).parent().find('.cls_td_missingPanal').children().val()+'",';
			arr += '"Other":"'+$(this).parent().find('.cls_td_other').children().val()+'",';	
			arr += '"Invoice":"'+$(this).parent().find('.cls_td_invoice').children().val()+'",';	
			arr += '"Percentage":"'+$(this).parent().find('.cls_cbo_percentage option:selected').val()+'",';	
			arr += '"Price":"'+$(this).parent().find('.cls_td_price').html()+'",';
			arr += '"Tax":"'+$(this).parent().find('.cls_td_tax').children().val()+'",';
			arr += '"CostCenter":"'+$(this).parent().find('.cls_td_costCenter').children().val()+'",';
			arr += '"TaxValue":"'+$(this).parent().find('.cls_td_tax').attr('id')+'",';
			arr += '"GL_ID":"'+$(this).parent().find('.cls_td_GL').children().val()+'"';
			arr +=  '},';
		}
	});
  //alert(arr)
	arr 		 = arr.substr(0,arr.length-1);
	arr 		+= " ]";
	
	var gl_array	= "[";
	$('#frmSalesInvoice #tblGL .cls_GLAmount').each(function(){
			gl_array += "{";
			gl_array += '"GL_ID":"'+$(this).parent().parent().find('.cls_td_GL').children().val()+'",' ;
			gl_array += '"GL_Amount":"'+$(this).val()+'"';
			gl_array +=  '},';
	});

	gl_array 		 = gl_array.substr(0,gl_array.length-1);
	gl_array 		+= " ]";

	var url 	= basePath+"invoice_db.php?RequestType=URLSave";
	var data 	= "CustomerPoNo="+$('#frmSalesInvoice #txtCustomerPONo').val();
		data   += "&OrderNo="+$('#frmSalesInvoice #txtOrderNo').html();
		data   += "&CustomerId="+$('#frmSalesInvoice #cboCustomer').val();
		data   += "&CustomerLocation="+$('#frmSalesInvoice #cboCustomerInvLoc').val();
		data   += "&CurrencyId="+$('#frmSalesInvoice #cboCurrency').val();
		data   += "&InvoicedDate="+$('#frmSalesInvoice #txtInvoiceDate').val();
		data   += "&Remarks="+$('#frmSalesInvoice #txtRemarks').val();
		data   += "&Complete="+($('#frmSalesInvoice #chkCpmplete').is(':checked')?1:0);
		data   += "&InvoiceType="+$('#frmSalesInvoice #cboInvoiceType').val();
		data   += "&SubTotal="+$('#frmSalesInvoice #txtSubTotal').val();
		data   += "&TaxTotal="+$('#frmSalesInvoice #txtTotTax').val();
		data   += "&GrandTotal="+$('#frmSalesInvoice #txtGrandTotal').val();
		data   += "&bankId="+$('#frmSalesInvoice #cboBank').val();
		data   += "&Grid="+arr;
		data   += "&GLGrid="+gl_array;
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type: "POST",
	async:false,
	success:function(json)
		{
			if(json.type=='pass')
			{
				hideWaiting();
				$('#frmSalesInvoice #butSave').validationEngine('showPrompt',json.msg,json.type);
				$('#frmSalesInvoice #txtInvoiceNo').val(json.InvoiceNo);
				$('#frmSalesInvoice #txtSerialNo').val(json.InvoiceNo);
				var t = setTimeout("alertx('butSave')",4000);
			}
			else
			{
				hideWaiting();
				$('#frmSalesInvoice #butSave').validationEngine('showPrompt',json.msg,json.type);
				var t = setTimeout("alertx('butSave')",4000);
			}
		},
	error:function(xhr,status)
		{
			hideWaiting();
			$('#frmSalesInvoice #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			var t = setTimeout("alertx('butSave')",4000);
		}
	});	
}

function alertx(but)
{
	$('#frmSalesInvoice #'+but).validationEngine('hide');
}

function AutoHideFormValidate()
{
	$('#frmSalesInvoice').validationEngine('hide');
}

function New()
{
	window.location.href = '?q='+menuId;
}

function CalculateTax(obj)
{ 

	var url 	= basePath+"invoice_db.php?RequestType=URLCalculateTax";
	var data 	= "TaxId="+obj.parent().parent().find('.cls_cbo_tax').val();
		data   += "&Value="+parseFloat(obj.parent().parent().find('.cls_td_value').html());
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{   
		obj.parent().parent().find('.cls_td_tax').attr('id',json.TaxValue);
		CaculateTotalValue();
	}
	});		
}

function Cancel()
{
	if($('#frmSalesInvoice #txtSerialNo').val()=="")
	{
		return;
	}
	var x = confirm("Are you sure you want to cancel Invoice No :"+$('#frmSalesInvoice #txtInvoiceNo').val()+".");
		if(!x)return;
	
	var url 	= basePath+"invoice_db.php?RequestType=URLCancel";
	var data 	= "SerialNo="+$('#frmSalesInvoice #txtSerialNo').val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		$('#frmSalesInvoice #butCancel').validationEngine('showPrompt',json.msg,json.type );
	}
	});
	var t = setTimeout("alertx('butCancel')",2000);
}

function LoadAutoData(customerPO)
{
	 $('#frmSalesInvoice #txtCustomerPONo').val(atob(customerPO));
	 $('#frmSalesInvoice #btnSearch').click();
	 CaculateTotalValue();
}

function AddNewGL()
{	
	$('#frmSalesInvoice #tblGL tbody tr:last').after("<tr>"+$('#frmSalesInvoice #tblGL .cls_tr_firstRow').html()+"</tr>");
}

function CalculateGLTotals()
{
	var total	= 0;
	$('#frmSalesInvoice #tblGL .cls_td_GLAmount').each(function(){
    	 total	+= isNaN(parseFloat($(this).children().val()))? 0:parseFloat($(this).children().val());
    });
	total	= RoundNumber(total,4,true);
	$('#frmSalesInvoice #tblGL .td_totalGLAlloAmount').val(total);
}

function Validate_save()
{	
	var booDetailAvailable 	= false;
	var total_amount		= parseFloat($('#frmSalesInvoice #txtGrandTotal').val());
	var total_gl_amount		= parseFloat($('#frmSalesInvoice #tblGL .td_totalGLAlloAmount').val());
	
	if(!$('#frmSalesInvoice').validationEngine('validate')){
		return false;
	}
	
	if($('#frmSalesInvoice #txtInvoiceNo').val()!="")
	{
		$('#frmSalesInvoice #butSave').validationEngine('showPrompt','This invoice is already saved.Please refresh the page','fail');
		var t = setTimeout("alertx('butSave')",2000);
		return false;
	}
	
	
	$('#frmSalesInvoice #tblMain .cls_td_salesOrderNo').each(function(){
		if($(this).parent().find('.cls_td_check').children().is(':checked'))
		{
			booDetailAvailable	= true;
		}
	});
	
	if(!booDetailAvailable)
	{
		$('#frmSalesInvoice #butSave').validationEngine('showPrompt','No details available to save.','fail');
		setTimeout("alertx('butSave')",2000);
		return false;
	}
/*	BEGIN - GL VALIDATION REMOVE FROM PAGE BECAUSE GL ACCOUNT WILL SAVE AUTOMATTIVALLY THROUGH THE SYSTEM {
	if(total_amount != total_gl_amount)
	{
		alert("'Total Invoice Amount' and 'Total GL Allocated Amount' should be equal.")
		return false;
	}
	END   - GL VALIDATION REMOVE FROM PAGE BECAUSE GL ACCOUNT WILL SAVE AUTOMATTIVALLY THROUGH THE SYSTEM {
*/
	return true;
}

function CheckAll()
{
	if($(this).is(':checked'))
		$('#frmSalesInvoice #tblMain .cls_check').attr('checked','checked');
	else
		$('#frmSalesInvoice #tblMain .cls_check').removeAttr('checked');
		
	$('#frmSalesInvoice #tblMain .cls_check').each(function(){
		ValidateRowWhenCheck($(this));
	});
}

function ValidateRowWhenCheck(obj)
{
	if(obj.is(':checked'))
	{
		obj.parent().parent().find('.cls_td_invoice').children().removeAttr('disabled');
		obj.parent().parent().find('.cls_td_costCenter').children().addClass('validate[required]');
		obj.parent().parent().find('.cls_td_GL').children().addClass('validate[required]');
	}
	else
	{
		obj.parent().parent().find('.cls_td_invoice').children().attr('disabled','disabled');
		obj.parent().parent().find('.cls_td_costCenter').children().removeClass('validate[required]');
		obj.parent().parent().find('.cls_td_GL').children().removeClass('validate[required]');
	}
	CaculateTotalValue();
}

function ClearObjects()
{
	$('#frmSalesInvoice #txtInvoiceNo').val('');
	$('#frmSalesInvoice #txtSerialNo').val('');
	//$('#frmSalesInvoice #txtCustomerPONo').val('');
	$('#frmSalesInvoice #cboCustomer').html('');
	$('#frmSalesInvoice #cboInvoiceType').val('');
	$('#frmSalesInvoice #txtRemarks').val('');
	$('#frmSalesInvoice #txtMarketer').val('');
	$('#frmSalesInvoice #cboCurrency').val('');
	$('#frmSalesInvoice #td_order').html('');
}

function apply_percentages(obj){
		  //suvini 2018.6.26
	var percentage 	=   obj.parent().parent().find('.cls_cbo_percentage').val();
	var invoice 	=	obj.parent().parent().find('.cls_td_invoice').children().val();
	var price 		=	obj.parent().parent().find('.cls_td_price').html();
	
	parseFloat(obj.parent().parent().find('.cls_td_value').html(RoundNumber((invoice*price) - invoice*price*percentage/100,4)));
	
}
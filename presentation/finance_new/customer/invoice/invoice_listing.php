<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$MenuId				= '701';
$reportId			= 894;
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

//------------------------------------------------------------
   $sql1 = "SELECT DISTINCT 
			C.intId,
			C.strName
			FROM finance_customer_invoice_header CIH			  
			INNER JOIN mst_customer C
			ON C.intId = CIH.CUSTOMER_ID
			ORDER BY
			C.strName ASC

	";
	$result1 = $db->RunQuery($sql1);
	$strCustomers='';
	$j=0;
	$strCustomers = ":All;" ;
	while($row=mysqli_fetch_array($result1))
	{
		$strCustomers .= $row['strName'].":".$row['strName'].";" ;
		$j++;
	}
	$strCustomers = substr($strCustomers,0,-1);
//------------------------------------------------------------
   $sql1 = "SELECT DISTINCT 
			CLH.intId,
			CLH.strName
			FROM finance_customer_invoice_header CIH 
			INNER JOIN mst_customer_locations_header CLH ON CIH.CUSTOMER_LOCATION = CLH.intId
			ORDER BY
			CLH.strName ASC

	";
	$result1 = $db->RunQuery($sql1);
	$strCustomersLoc='';
	$j=0;
	$strCustomersLoc = ":All;" ;
	while($row=mysqli_fetch_array($result1))
	{
		$strCustomersLoc .= $row['strName'].":".$row['strName'].";" ;
		$j++;
	}
	$strCustomersLoc = substr($strCustomersLoc,0,-1);

  //------------------------------------------------------------

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'STATUS'=>'CIH.STATUS',
				'CONCAT_SERIAL_NO'=>"CONCAT(CIH.SERIAL_NO,'/',CIH.SERIAL_YEAR )",
				'INVOICE_NO'=>'CIH.INVOICE_NO',
				'INVOICED_DATE'=>'CIH.INVOICED_DATE',
				'CUSTOMER_PONO'=>'OH.strCustomerPoNo',
				'CUSTOMER_NAME'=>'C.strName',
				'CUST_INV_LOCATION_NAME'=>'CLH.strName'
				);
				
$arr_status = array('Confirmed'=>'1','Canceled'=>'10');
foreach($arr as $k=>$v)
{
	if($v['field']=='STATUS')
	{
		$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND CIH.INVOICED_DATE = '".date('Y-m-d')."'";
	
// }

$sql = "SELECT SUB_1.* FROM
			(SELECT
			  CONCAT(CIH.SERIAL_NO,'/',CIH.SERIAL_YEAR )	 	AS CONCAT_SERIAL_NO,
			  CIH.SERIAL_NO										AS SERIAL_NO,
			  CIH.SERIAL_YEAR									AS SERIAL_YEAR,
			  CIH.INVOICE_NO 									AS INVOICE_NO,
			  CIH.INVOICED_DATE									AS INVOICED_DATE,
			  CIH.ORDER_YEAR 									AS ORDER_YEAR,
			  CIH.ORDER_NO   									AS ORDER_NO,
			  OH.strCustomerPoNo								AS CUSTOMER_PONO,	
			  OH.intCustomerLocation							AS CUST_INV_LOCATION_ID,
			  CLH.strName										AS CUST_INV_LOCATION_NAME,
			  C.strName      									AS CUSTOMER_NAME,			 
			  'View'											AS VIEW,
			  'Report'											AS REPORT,
			  IF(CIH.STATUS=1,'Confirmed','Canceled') 			AS STATUS,
			  (SELECT ROUND(SUM(SUB_CID.VALUE),4)						 		
			   FROM finance_customer_invoice_details SUB_CID
				WHERE SUB_CID.SERIAL_NO = CIH.SERIAL_NO
				  AND SUB_CID.SERIAL_YEAR = CIH.SERIAL_YEAR)		AS INVOICE_VALUE
			FROM finance_customer_invoice_header CIH			  
			  INNER JOIN mst_customer C
				ON C.intId = CIH.CUSTOMER_ID
			  INNER JOIN trn_orderheader OH
				ON OH.intOrderNo = CIH.ORDER_NO
				  AND OH.intOrderYear = CIH.ORDER_YEAR 
			  INNER JOIN mst_customer_locations_header CLH ON CIH.CUSTOMER_LOCATION = CLH.intId
				  WHERE CIH.COMPANY_ID = $session_companyId
				  $where_string
		)  
		AS SUB_1 WHERE 1=1";

$jq 	= new jqgrid('',$db);	
$col 	= array();
$cols 	= array();

//BEGIN - ACTIVE/INACTIVE {
$col["title"] 			= "Status";
$col["name"] 			= "STATUS";
$col["width"] 			= "2"; 						// not specifying width will expand to fill space
$col["align"] 			= "left";
$col["sortable"] 		= true; 						// this column is not sortable
$col["search"] 			= true; 						// this column is not searchable
$col["editable"] 		= false;
$col["stype"] 			= "select";
$str 					= ":All;Confirmed:Confirmed;Canceled:Canceled";  // all row, blank row, then all db values. ; is separator
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;
//END 	- ACTIVE/INACTIVE }

$col["title"] 			= "Serial No";
$col["name"] 			= "SERIAL_NO";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Serial Year";
$col["name"] 			= "SERIAL_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Serial No";
$col["name"] 			= "CONCAT_SERIAL_NO";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col['link']			= '?q='.$MenuId.'&SerialNo={SERIAL_NO}&SerialYear={SERIAL_YEAR}';
$col["linkoptions"] 	= "target='_blank'";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Invoice No";
$col["name"] 			= "INVOICE_NO";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Date";
$col["name"] 			= "INVOICED_DATE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Order Year";
$col["name"] 			= "ORDER_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Order No";
$col["name"] 			= "ORDER_NO";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Customer PONo";
$col["name"] 			= "CUSTOMER_PONO";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Customer";
$col["name"] 			= "CUSTOMER_NAME";
$col["width"] 			= "10";
$col["stype"] 			= "select";
$str 					= $strCustomers ;
$col["editoptions"] 	=  array("value"=> $str);
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Customer Location";
$col["name"] 			= "CUST_INV_LOCATION_NAME";
$col["width"] 			= "10";
$col["stype"] 			= "select";
$str 					= $strCustomersLoc ;
$col["editoptions"] 	=  array("value"=> $str);
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Invoice Value";
$col["name"] 			= "INVOICE_VALUE";
$col["width"] 			= "2";
$col["align"] 			= "right";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Report";
$col["name"] 			= "REPORT";
$col["width"] 			= "1";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= '?q='.$reportId.'&SerialNo={SERIAL_NO}&SerialYear={SERIAL_YEAR}&status={STATUS}';
$col["linkoptions"] 	= "target='_blank'";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Sales Invoice Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'SERIAL_YEAR,SERIAL_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Confirmed"}
     ]
}
SEARCH_JSON;
//$grid["postData"] = array("filters" => $sarr ); 


$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Sales Invoice Listing</title>
<?php
echo $out;
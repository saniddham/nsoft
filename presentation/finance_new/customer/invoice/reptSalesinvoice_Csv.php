<?php
/**
 * Created by PhpStorm.
 * User: Hasitha
 * Date: 3/6/2018
 * Time: 11:29 AM
 */


	session_start();
	$user_id= $_SESSION['userId'];
	if($_SESSION['userId']=='')
        die("Temporary unavailable. Please contact the admin");

//ini_set('display_errors', 'on');


include_once $_SESSION['ROOT_PATH'].'dataAccess/Connector.php';
$backwardseperator = "../../../../../";


/*
 * CREATE DB CONNECTION
 */
//$servername = $_SESSION['Server'];
//$username = $_SESSION['UserName'];
//$password = $_SESSION['Password'];
//$dbname = $_SESSION['Database'];

$dispNo 				= $_REQUEST['serialNo'];
$dispYear 				= $_REQUEST['year'];



//// Create connection
//$conn = new mysqli($servername, $username, $password, $dbname);
////mysql_set_charset('utf8',$conn);
//// Check connection
//if ($conn->connect_error){
//    die("Connection failed: " . $conn->connect_error);
//}



$body =ob_start();
include "rptSalesinvoice_header.php";

$result= getReportDispatch($dispNo,$dispYear);


include "rptSalesinvoice_details.php";

//mysqli_close($conn);


echo $body 			= ob_get_clean();




function getReportDispatch($dispNo,$dispYear) {

    global $db;
    $db->OpenConnection();


    $sql="
    SELECT DISTINCT
OD.strSalesOrderNo AS SALES_ORDER_NO,
OD.strGraphicNo AS GRAPHIC_NO,
OD.strStyleNo AS STYLE_NO,
P.strName AS PART,
CIB.DISPATCHED_GOOD_QTY AS DISPATCHED_GOOD_QTY,
CID.QTY AS INVOICE_QTY,
CID.PRICE AS INVOICE_PRICE,
CID.`VALUE` AS INVOICE_VALUE,
CID.TAX_CODE AS TAX_CODE_ID,
CID.COST_CENTER AS COST_CENTER_ID,
CID.TAX_VALUE AS TAX_VALUE,
trn_orderheader.strCustomerPoNo,
OD.strLineNo,
CIH.ORDER_NO,
CIH.ORDER_YEAR,
CID.SERIAL_NO,
CID.SERIAL_YEAR,
(
SELECT 
	
	GROUP_CONCAT( DISTINCT ware_fabricdispatchdetails.strSize)
FROM
	ware_fabricdispatchheader
INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo
AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
WHERE
ware_fabricdispatchdetails.intSalesOrderId = CID.SALES_ORDER_ID AND
	ware_fabricdispatchheader.intOrderNo = CIH.ORDER_NO AND
	ware_fabricdispatchheader.intOrderYear = CIH.ORDER_YEAR
) AS SIZE,
(
SELECT 
	
		GROUP_CONCAT( DISTINCT ware_fabricdispatchheader.intBulkDispatchNo SEPARATOR '/')
FROM
	ware_fabricdispatchheader
INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo
AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
WHERE
ware_fabricdispatchdetails.intSalesOrderId = CID.SALES_ORDER_ID AND
	ware_fabricdispatchheader.intOrderNo = CIH.ORDER_NO AND
	ware_fabricdispatchheader.intOrderYear = CIH.ORDER_YEAR
) AS DISPATCH_NO,
(
SELECT 
	
	GROUP_CONCAT( DISTINCT ware_fabricdispatchdetails.strCutNo)
FROM
	ware_fabricdispatchheader
INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo
AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
WHERE
ware_fabricdispatchdetails.intSalesOrderId = CID.SALES_ORDER_ID AND
	ware_fabricdispatchheader.intOrderNo = CIH.ORDER_NO AND
	ware_fabricdispatchheader.intOrderYear = CIH.ORDER_YEAR
) AS CUT_NO,
OD.ITEM_CODE_FROM_CUSTOMER,
OD.dblPrice,
OD.intQty,
ware_fabricdispatchdetails_barcode_wise.BARCODE
FROM
finance_customer_invoice_details AS CID
INNER JOIN finance_customer_invoice_header AS CIH ON CIH.SERIAL_YEAR = CID.SERIAL_YEAR AND CIH.SERIAL_NO = CID.SERIAL_NO
INNER JOIN trn_orderdetails AS OD ON OD.intOrderYear = CIH.ORDER_YEAR AND OD.intOrderNo = CIH.ORDER_NO AND OD.intSalesOrderId = CID.SALES_ORDER_ID
INNER JOIN mst_part AS P ON P.intId = OD.intPart
INNER JOIN finance_customer_invoice_balance AS CIB ON CIB.ORDER_YEAR = CIH.ORDER_YEAR AND CIB.ORDER_NO = CIH.ORDER_NO AND OD.intSalesOrderId = CIB.SALES_ORDER_ID
INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = OD.intOrderNo AND trn_orderheader.intOrderYear = OD.intOrderYear AND trn_orderheader.intOrderNo = CIH.ORDER_NO AND trn_orderheader.intOrderYear = CIH.ORDER_YEAR
INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = trn_orderheader.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderheader.intOrderYear AND ware_fabricdispatchheader.intOrderNo = CIH.ORDER_NO AND ware_fabricdispatchheader.intOrderYear = CIH.ORDER_YEAR
LEFT JOIN ware_fabricdispatchdetails_barcode_wise ON ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear AND ware_fabricdispatchdetails_barcode_wise.intPart = P.intId AND ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNo = OD.strSalesOrderNo AND ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNoYear = OD.intSalesOrderId AND ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
WHERE
	CID.SERIAL_YEAR = '$dispYear'
	AND CID.SERIAL_NO = '$dispNo' ";

    $result =$db->RunQuery2($sql);
    return $result;
}





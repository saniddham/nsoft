<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>

<title>Customer Invoice PDF Print</title>

<form id="frmInvoicePrintPdf" name="frmInvoicePrintPdf" autocomplete="off">
<div align="center">
	<div class="trans_layoutS">
	<div class="trans_text">Customer Invoice PDF Print</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    	<tr>
        	<td>
            <table width="100%" border="0" align="center">
            	<tr class="normalfnt">
                	<td width="5%">&nbsp;</td>
                    <td width="24%">Year <span class="compulsoryRed">*</span></td>
                    <td colspan="3"><select style="width:100px;" name="cboYear" id="cboYear" class="validate[required]" >
                   	<option value=""></option>
                    <?php
						$sql = "SELECT DISTINCT SERIAL_YEAR
								FROM finance_customer_invoice_header
								WHERE STATUS = 1
								order by SERIAL_YEAR ";
						
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($row['SERIAL_YEAR']==date('Y'))
								echo "<option value=".$row["SERIAL_YEAR"]." selected=\"selected\">".$row["SERIAL_YEAR"]."</option>";
							else
								echo "<option value=".$row["SERIAL_YEAR"].">".$row["SERIAL_YEAR"]."</option>";
						}
					?>
                    </select></td>
                </tr>
            	<tr class="normalfnt">
            	  <td>&nbsp;</td>
            	  <td>Invoice Type <span class="compulsoryRed">*</span></td>
            	  <td colspan="3"><select style="width:170px;" name="cboInvoiceType" id="cboInvoiceType" class="validate[required]" >
            	    <option value=""></option>
            	    <?php
						$sql = "SELECT intId,strInvoiceType
								FROM mst_invoicetype
								WHERE intStatus = 1
								ORDER BY strInvoiceType ";
						
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=".$row["intId"].">".$row["strInvoiceType"]."</option>";	
						}
					?>
          	    </select><input type="hidden" id="txtReportName" name="txtReportName" value="" /></td>
           	  </tr>
            	<tr class="normalfnt">
            	  <td>&nbsp;</td>
            	  <td>Serial No From <span class="compulsoryRed">*</span></td>
            	  <td width="27%"><input type="text" name="txtSerialNoFrom" id="txtSerialNoFrom" style="width:100px;" class="validate[required,custom[onlyNumberSp]]" /></td>
            	  <td width="8%">To</td>
            	  <td width="36%"><input type="text" name="txtSerialNoTo" id="txtSerialNoTo" style="width:100px;" class="validate[required,custom[onlyNumberSp]]" /></td>
       	      </tr>
            	<tr class="normalfnt">
            	  <td>&nbsp;</td>
            	  <td>&nbsp;</td>
            	  <td>&nbsp;</td>
            	  <td>&nbsp;</td>
            	  <td>&nbsp;</td>
          	  </tr>
            </table>
            </td>
        </tr>
        <tr>
            <td height="32">
            	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                    <td align="center" ><a class="button white medium" id="butNew">New</a><a class="button white medium" id="butPrint">Print</a><a href="main.php" class="button white medium" id="butClose">Close</a>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
    </table>
    </div>
</div>       
</form>
<?php
session_start();
ini_set('display_errors',0);
//BEGIN - SESSION {
$session_companyId	= $_SESSION["CompanyID"];
//END	- SESSION }

//BEGIN - INCLUDE FILES {
$backwardseperator 	= "../../../../../";
require_once ("{$backwardseperator}dataAccess/Connector.php");
require_once ("../../../../../libraries/fpdf/fpdf.php");
require_once ("../../../../../class/masterData/companies/cls_companies_get.php");
require_once ("../../../../../class/finance/customer/cls_invoice_get.php");
require_once ("../../../../../class/finance/customer/cls_invoice_set.php");
//require_once ("../../../../../class/masterData/exchange_rate/cls_exchange_rate_get.php");
include_once ("../../../../../class/finance/cls_convert_amount_to_word.php");
require_once ("../../../../../class/masterData/currency/cls_currency_get.php");
//END	- INCLUDE FILES }

//BEGIN	- CREATE OBJECTS {
$obj_exchange_rate_get		= new cls_exchange_rate_get($db);
$obj_company_get			= new cls_companies_get($db);
$obj_invoice_get			= new Cls_Invoice_Get($db);
$obj_invoice_set			= new Cls_Invoice_Set($db);
$obj_convert_amount_to_word	= new Cls_Convert_Amount_To_Word($db);
$obj_currency_get			= new cls_currency_get($db);
//END	- CREATE OBJECTS }

//BEGIN - PARAMETERS {
//$periodArray = array();
$serialNoFrom	= $_REQUEST["serialNoFrom"];
$serialNoTo		= $_REQUEST["serialNoTo"];
$year			= $_REQUEST["year"];
$title			= "NFE Suspended Tax Invoice";
$font			= 'Times';
//END	- PARAMETERS }
class PDF extends FPDF
{
	function Header1()
	{
		global $result_reportHeader;
		global $title;
		global $session_companyId;
		global $font;
		$h = 5;
		
		$this->Image('../../../../../images/screenline.jpg',8,9);
		$this->SetFont($font,'B',14);$this->Cell(0,$h,$result_reportHeader["COMPANY_NAME"],'0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,$result_reportHeader["LOCATION_ADDRESS"].' '.$result_reportHeader["LOCATION_STREET"].' '.$result_reportHeader["LOCATION_CITY"].' '.$result_reportHeader["COMPANY_COUNTRY"].'.','0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,"Tel : ".$result_reportHeader["LOCATION_PHONE"].'  '."Fax : ".$result_reportHeader["LOCATION_FAX"],'0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,"E-mail : ".$result_reportHeader["LOCATION_EMAIL"].'  '."Web : ".$result_reportHeader["LOCATION_WEB"],'0',1,'C');
		$this->SetFont($font,'B',12);$this->Cell(0,10,$title,'0',1,'C');		
	}
	
	function SetPageWaterMark($status,$printStatus)
	{
		switch($status)
		{
			case 1:
				$msg	= ($printStatus==1?"DUPLICATE  INVOICE":"");
				break;
			case '10':
				$msg	= "CANCELED INVOICE";
				break;
			default:
				$msg	= "NOT A VALID INVOICE";
				break;
		}
		$this->SetFont('Times','B',50);
		$this->SetTextColor(255,192,203);
		$this->RotatedText(35,190,$msg,45);
		$this->SetTextColor(0,0,0);
	}
	
	function RotatedText($x, $y, $txt, $angle)
	{
		//Text rotated around its origin
		$this->Rotate($angle,$x,$y);
		$this->Text($x,$y,$txt);
		$this->Rotate(0);
	}
	
	function ReportHeader($result_header,$result_reportHeader)
	{
		$h = 5;
		$this->SetFont($font,'',11);$this->Cell(34,$h,'To','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(85,$h,$result_header["CUSTOMER_NAME"],'0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Invoice No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$result_header["INVOICE_NO"],'0',1,'L');
		
		$this->SetX(46);
		$this->SetFont($font,'',11);$this->Cell(85,$h,$result_header["ADDRESS"],'0',1,'L');
		$this->SetX(46);
		$this->SetFont($font,'',11);$this->Cell(85,$h,$result_header["CITY"],'0',1,'L');
		$this->SetX(46);
		$this->SetFont($font,'',11);$this->Cell(85,$h,$result_header["COUNTRY_NAME"].'.','0',1,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Attention','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(85,$h,$result_header["CONTACT_PERSON"],'0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Invoice Date','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$result_header["INVOICED_DATE"],'0',1,'L');
		
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Customer VAT No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(85,$h,$result_header["CUSTOMER_VAT"],'0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Company VAT No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$result_reportHeader["COMPANY_VAT"],'0',1,'L');
		
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Customer SVAT No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(85,$h,$result_header["CUSTOMER_SVAT"],'0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Company SVAT No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$result_reportHeader["COMPANY_SVAT"],'0',1,'L');

		$this->SetFont($font,'',11);$this->Cell(34,$h,'Order No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(85,$h,$result_header["ORDER_NO"],'0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Order Location','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$result_header["LOCATION_NAME"],'0',1,'L');
		
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Narration','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->MultiCell(0,5,$result_header["REMARKS"],'0','',0);
	}
	
	function CreateTable($result_header,$result_details,$obj_convert_amount_to_word)
	{
		$count 			= 0;
		
		$this->Table_header();
		while($row = mysqli_fetch_array($result_details))
		{	
			$this->Table_Body($row);	
			if($count == 10){
				$this->AddPage();
				$this->Table_header();
				$count = 0;	
			}			
			$count++;			
		}
		$this->Table_Body_Validate($count);	
		$this->Table_footer($result_header,$obj_convert_amount_to_word);
	}
	
	function Table_header()
	{
		$this->SetXY($this->GetX(),$this->GetY()+2);
		$h = 10;
		$this->SetFont('Times','B',10);
		$this->Cell(30,$h,'Item Name','1',0,'C');
		$this->Cell(90,$h,'Item Description','1',0,'C');
		$this->Cell(15,$h,"Unit",'1',0,'C');
		$this->Cell(18,$h,"Qty",'1',0,'C');
		$this->Cell(27,$h,"Unit Price [USD]",'1',0,'C');
		$this->Cell(20,$h,'Total Value','1',0,'R');
		$this->Ln();		
	}
		
	function Table_Body($row)
	{	
		$h	= 4;
		$this->SetFont('Times','',8);
		$this->Cell(30,$h,$row["ITEM_NAME"],'LR',0,'C');
		$this->Cell(90,$h,"PO No : ".$row["CUSTOMER_PO_NO"],'LR',0,'L');
		$this->Cell(15,$h,$row["UNIT"],'LR',0,'C');
		$this->Cell(18,$h,number_format($row["INVOICE_QTY"],2),'LR',0,'R');
		$this->Cell(27,$h,number_format($row["INVOICE_PRICE"],2),'LR',0,'R');
		$this->Cell(20,$h,number_format($row["INVOICE_VALUE"],2),'LR',0,'R');
		$this->Ln();

		$this->SetFont('Times','',8);
		$this->Cell(30,$h,'','LR',0,'C');
		$this->Cell(90,$h,"Style No : ".$row["STYLE_NO"],'LR',0,'L');
		$this->Cell(15,$h,'','LR',0,'C');
		$this->Cell(18,$h,'','LR',0,'R');
		$this->Cell(27,$h,'','LR',0,'R');
		$this->Cell(20,$h,'','LR',0,'R');
		$this->Ln();

		$this->SetFont('Times','',8);
		$this->Cell(30,$h,'','LR',0,'C');
		$this->Cell(90,$h,"Graphic No : ".$row["GRAPHIC_NO"],'LR',0,'L');
		$this->Cell(15,$h,'','LR',0,'C');
		$this->Cell(18,$h,'','LR',0,'R');
		$this->Cell(27,$h,'','LR',0,'R');
		$this->Cell(20,$h,'','LR',0,'R');
		$this->Ln();

		$this->SetFont('Times','',8);
		$this->Cell(30,$h,'','LBR',0,'C');
		$this->Cell(90,$h,"Sales Order No : ".$row["SALES_ORDER_NO"],'LBR',0,'L');
		$this->Cell(15,$h,'','LBR',0,'C');
		$this->Cell(18,$h,'','LBR',0,'R');
		$this->Cell(27,$h,'','LBR',0,'R');
		$this->Cell(20,$h,'','LBR',0,'R');
		$this->Ln();
	}
	
	function Table_Body_Validate($count)
	{
		$h1	= 0;
		for($i=$count;$i<=5;$i++)
		{ 
			//echo "$i";
			//echo "<br/>";
			$h1 += 16;
		}//echo $count.'/'.$h1;
		$this->SetFont('Times','',7);
		$this->Cell(180,$h1,"",'1',0,'C');
		$this->Cell(20,$h1,"",'1',0,'C');
		$this->Ln();
	}
	
	function Table_footer($result_header,$obj_convert_amount_to_word)
	{
		$this->SetY($this->GetY());
		$this->SetFont('Times','B',10);$this->Cell(180,10,"Total",'1',0,'R');
		$this->Cell(20,10,number_format($result_header["VALUE"],2),'1',0,'R');
		$this->Ln();
		
		$this->SetFont('Times','',8);$this->Cell(180,10,$obj_convert_amount_to_word->Convert_Amount($result_header["VALUE"],$baseCurr_array["CODE"]),'0',0,'L');
		$this->Cell(20,10,"",'0',0,'C');
		$this->Ln();
	}
	
	function ReportFooter($result_header,$exRate_array,$baseCurr_array)
	{
		$this->SetXY(10,$this->GetY()+1);
		$h = 5;		
		$this->SetFont('Times','B',10);$this->Cell(180,$h,"For VAT Purpose Only",'0',0,'L');
		$this->Cell(20,$h,"",'0',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','',10);$this->Cell(180,$h,"Suspended Value Added Tax @ 12 % = ".number_format(($result_header["VALUE"]*12)/100,4),'0',0,'L');
		$this->Cell(20,$h,"",'0',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','',10);$this->Cell(180,$h,"Exchange Rate : ".$baseCurr_array["SYMBOL"].' '.$exRate_array["AVERAGE_RATE"].' / '.$baseCurr_array['CODE'].' '.number_format($result_header["VALUE"]*$exRate_array["AVERAGE_RATE"],2),'0',0,'L');
		$this->Cell(20,$h,"",'0',0,'C');
		$this->Ln();
	}
	
	function Footer1()
	{
		global $session_companyId;
		global $result_header;		
		$this->SetFont('Times','',8);
		
		$this->SetXY(20,$this->GetY()+7);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetX(140);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetXY(20,$this->GetY()+3);
		$this->Cell(50,5,'Authorized By',0,0,'C');
		
		$this->SetX(140);		
		$this->Cell(50,5,'Checked By',0,0,'C');	
		
		$this->SetXY(20,$this->GetY()+10);
		$this->Cell(50,5,$result_header["CREATED_BY_NAME"],0,0,'C');
		
		$this->SetXY(20,$this->GetY()+2);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetX(140);
		$this->Cell(50,5,'..........................................',0,0,'C');

		$this->SetXY(140,$this->GetY()+3);
		$this->Cell(50,5,'Received By',0,0,'C');

		$this->SetX(20);
		$this->Cell(50,5,'Prepared By',0,0,'C');	
	}
	
	function Footer()
	{
		$this->SetY(-5);
		$this->SetFont('Times','I',8);
		$this->Cell(0,2,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf 					= new PDF('P','mm','Letter');

for($i=$serialNoFrom;$i<=$serialNoTo;$i++)
{
	$result_header 		= $obj_invoice_get->GetPaymentVoucherReportHeader($i,$year);
	if($result_header['ORDER_NO']=='')
	{
		continue;
	}
	else
	{
		$result_details			= $obj_invoice_get->GetPaymentVoucherReportDetails($i,$year);
		$result_reportHeader	= $obj_company_get->GetCompanyReportHeader($result_header["LOCATION_ID"]);
		$exRate_array			= $obj_exchange_rate_get->GetAllValues($result_header['CURRENCY_ID'],$deci=2,$result_header['INVOICED_DATE'],$result_header['COMPANY_ID'],'RunQuery');
		$baseCurr_array			= $obj_currency_get->GetAllValues($result_reportHeader["BASE_CURRENCY_ID"],'RunQuery');

		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->Header1();
		$pdf->SetPageWaterMark($result_header["STATUS"],$result_header["PRINT_STATUS"]);
		$pdf->ReportHeader($result_header,$result_reportHeader);
		$pdf->CreateTable($result_header,$result_details,$obj_convert_amount_to_word);
		$pdf->ReportFooter($result_header,$exRate_array,$baseCurr_array);
		$pdf->Footer1();
		$obj_invoice_set->UpdateStatus($i,$year);
	}
}
$pdf->Output('rpt_suspended_tax_invoice_pdf1.pdf','I');

?>
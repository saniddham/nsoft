// JavaScript Document
var basePath	= "presentation/finance_new/customer/invoice/invoicePdf/";
$(document).ready(function(){

	$('#frmInvoicePrintPdf').validationEngine();
	$("#frmInvoicePrintPdf #cboInvoiceType").die('change').live('change',getReportName);
	$("#frmInvoicePrintPdf #butPrint").die('click').live('click',printPdf);
	$("#frmInvoicePrintPdf #butNew").die('click').live('click',clearAll);
});
function getReportName()
{
	if($(this).val()=='')
	{
		$('#frmInvoicePrintPdf #txtReportName').val('');
		return;
	}
	var url				= basePath+"invoice_pdf_print_db.php?requestType=getReportName&invoiceType="+$(this).val();
	var httpObj 		= $.ajax({url:url,async:false})
	
	$('#frmInvoicePrintPdf #txtReportName').val(httpObj.responseText);
}
function printPdf()
{
	var year 	 		= $('#frmInvoicePrintPdf #cboYear').val();
	var serialNoFrom 	= parseFloat($('#frmInvoicePrintPdf #txtSerialNoFrom').val());
	var serialNoTo  	= parseFloat($('#frmInvoicePrintPdf #txtSerialNoTo').val());
	var invoiceType 	= $('#frmInvoicePrintPdf #cboInvoiceType').val();
	var reportName		= $('#frmInvoicePrintPdf #txtReportName').val();
	
	if($('#frmInvoicePrintPdf').validationEngine('validate'))
	{
		if(serialNoFrom>serialNoTo)
		{
			$('#frmInvoicePrintPdf #butPrint').validationEngine('showPrompt','Invalid No range.','fail');
			return;
		}
		window.open('presentation/finance_new/customer/invoice/invoicePdf/'+reportName+'?year='+year+'&serialNoFrom='+serialNoFrom+'&serialNoTo='+serialNoTo);
	}
}
function clearAll()
{
	$('#frmInvoicePrintPdf')[0].reset();
}

<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$bulkReportId	= 896;

include_once ("class/finance/cls_convert_amount_to_word.php");

$obj_convert_amount_to_word	= new Cls_Convert_Amount_To_Word($db);

$serialNo			= $_REQUEST["SerialNo"];
$serialYear			= $_REQUEST["SerialYear"];
$status  =$_REQUEST['status'];
$header_array 		= GetHeaderDetails($serialYear,$serialNo);
$detail_result 		= GetGridDetails($serialYear,$serialNo);
$locationId			= $header_array["LOCATION_ID"];

if(!isset($_REQUEST["SerialNo"]))
	$invoiceDate	= date("Y-m-d");
else
	$invoiceDate	= $header_array["INVOICED_DATE"];	
?>
<head>
<title>Customer Sales Invoice Report</title>
</head>
<body>
<style type="text/css">
.apDiv1 {
	position:absolute;
	left:380px;
	top:100px;
	width:auto;
	height:auto;
	z-index:0;
	opacity:0.1;
}

@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}
</style>
<div id="partPay" class="apDiv1 <?php echo $header_array["STATUS"]=='10'?'maskShow':'maskHide'?>"><img src="images/cancelled.png"  /></div>
<form id="frmSalesInvoice" name="frmSalesInvoice" method="post">
    <table width="900" align="center">


        <tr>
            <td><?php include 'reportHeader.php'?></td>
<!--            <td width="6%"><a target="rptFabricDispatchNote_pdf.php" href="presentation/customerAndOperation/bulk/fabricDispatchNote/listing/rptFabricDispatchNote_pdf.php?serialNo=--><?php //echo $serialNo?><!--&year=--><?php //echo $year?><!--"><img src="images/pdf_icon.png" class="noPrint"/></a></td>-->
           <?php if($header_array["STATUS"]!='10') { ?>
            <td width="6%"><a target="rptFabricDispatchNote_excel.php" href="presentation/finance_new/customer/invoice/reptSalesinvoiceExcel.php?serialNo=<?php echo $serialNo?>&year=<?php echo $serialYear?>&orderNo=<?php echo $header_array['ORDER_NO'];?>&orderYear=<?php echo $header_array['ORDER_YEAR'];?>"><img src="images/excel.PNG" class="noPrint"  style="width: 50px; width: 50px;"/></a></td>
            <td width="5%"><a target="rptFabricDispatchNote_excel_csv.php" href="presentation/finance_new/customer/invoice/reptSalesinvoice_Csv.php?serialNo=<?php echo $serialNo?>&year=<?php echo $serialYear?>&type=1&orderNo=<?php echo $header_array['ORDER_NO'];?>&orderYear=<?php echo $header_array['ORDER_YEAR'];?>"><img src="images/csv.PNG" class="noPrint" style="width: 50px; width: 50px;" /></a></td>
            <td width="13%">&nbsp;</td>
                <?php   } ?>
        </tr>

        <tr>
            <td class="reportHeader" align="center">Customer Sales Invoice</td>
        </tr>
        <tr>
            <td><table width="100%" border="0" class="normalfnt">
                    <tr>
                        <td width="14%">Customer Name</td>
                        <td width="1%">:</td>
                        <td width="43%"><?php echo $header_array["CUSTOMER_NAME"]?></td>
                        <td width="14%">Invoice No</td>
                        <td width="2%">:</td>
                        <td width="26%"><?php echo $header_array["INVOICE_NO"]?></td>
                    </tr>
                    <tr>
                        <td>Customer Address</td>
                        <td>:</td>
                        <td><?php echo $header_array["CUSTOMER_ADDRESS"]?></td>
                        <td>Invoiced Date</td>
                        <td>:</td>
                        <td><?php echo $invoiceDate ?></td>
                    </tr>
                    <tr>
                        <td>Attention By</td>
                        <td>:</td>
                        <td><?php echo $header_array["CONTACT_NAME"]?></td>
                        <td>Order No</td>
                        <td>:</td>
                        <td><a href="?q=<?php echo $bulkReportId; ?>&amp;orderNo=<?php echo $header_array["ORDER_NO"]?>&amp;orderYear=<?php echo $header_array["ORDER_YEAR"]?>" target="_blank"><?php echo $header_array["CONCAT_ORDER_NO"]?></a></td>
                    </tr>
                    <tr>
                        <td>Customer VAT No</td>
                        <td>:</td>
                        <td><?php echo $header_array["CUSTOMER_VAT_NO"]?></td>
                        <td>Customer PONo</td>
                        <td>:</td>
                        <td><?php echo $header_array["CUSTOMER_PONO"]?></td>
                    </tr>
                    <tr>
                        <td>Customer SVAT No</td>
                        <td>:</td>
                        <td><?php echo $header_array["CUSTOMER_SVAT_NO"]?></td>
                        <td>Company VAT No</td>
                        <td>:</td>
                        <td><?php echo $companyVATNo ?></td>
                    </tr>
                    <tr>
                        <td>Customer Location</td>
                        <td>:</td>
                        <td><?php echo $header_array["CUSTOMER_INVOICE_LOCATION"]?></td>
                        <td>Company SVAT No</td>
                        <td>:</td>
                        <td><?php echo $companySVATNo;?></td>
                    </tr>
                    <tr>
                        <td>Narration</td>
                        <td>:</td>
                        <td rowspan="2" valign="top"><?php echo $header_array["REMARKS"]?></td>
                        <td>Bank Account</td>
                        <td>:</td>
                        <td><?php if (isset($header_array["BANK_ID"]) && $header_array["BANK_ID"] != "" ) {
                                $result = GetBankAccDetails($header_array["BANK_ID"]);
                                $row = mysqli_fetch_array($result);
                                echo $row["CHART_OF_ACCOUNT_NAME"];
                            } else {
                                echo "N\A";
                            }
                            ?></td>

                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table></td>
        </tr>
        <tr>
            <td align="right"><a class="button green small no-print" target="<?php echo $header_array["SALES_INVOICE_REPORT_NAME"]?>" href="<?php echo "presentation/finance_new/customer/invoice/".$header_array["SALES_INVOICE_REPORT_NAME"]."?SerialNo=$serialNo&SerialYear=$serialYear"?>">Click here to print invoice</a></td>
        </tr>
        <tr>
            <td ><table width="100%%" border="0" class="rptBordered" id="tblMain">
                    <thead>
                    <tr>
                        <th width="3%">&nbsp;</th>
                        <th width="18%">Sales Order No</th>
                        <th width="17%">Graphic No</th>
                        <th width="15%">Style No</th>
                        <th width="20%">Placement</th>
                        <th width="10%">Invoice Qty</th>
                        <th width="8%">Price</th>
                        <th width="9%">Value</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $total_value	= 0;
                    $total_tax		= 0;
                    $i				= 0;
                    while($row = mysqli_fetch_array($detail_result))
                    {
                        ?>
                        <tr>
                            <td class="cls_td_check" align="center"><?php echo ++$i;?>.</td>
                            <td class="cls_td_salesOrderNo"><?php echo $row["SALES_ORDER_NO"]?></td>
                            <td width="17%"><?php echo $row["GRAPHIC_NO"]?></td>
                            <td width="15%"><?php echo $row["STYLE_NO"]?></td>
                            <td width="20%"><?php echo $row["PART"]?></td>
                            <td class="cls_td_invoice cls_Subtract" style="text-align:right"><?php echo number_format($row["INVOICE_QTY"],2)?></td>
                            <td style="text-align:right"><?php echo number_format($row["INVOICE_PRICE"],4)?></td>
                            <td class="cls_td_value" style="text-align:right"><?php echo number_format($row["INVOICE_VALUE"],2)?></td>
                        </tr>
                        <?php
                        $total_value 	+= round($row["INVOICE_VALUE"],2);
                        $total_tax 		+= round($row["TAX_VALUE"],2);
                    }
                    $grandTotal = round($total_value,2) - round($total_tax,2)
                    ?>
                    </tbody>
                </table></td>
        </tr>
        <tr>
            <td class="normalfnt"><b><?php echo $obj_convert_amount_to_word->Convert_Amount($grandTotal,$header_array["CURRENCY_CODE"]) ?></b></td>
        </tr>
        <tr>
            <td ><table width="200" border="0" align="right" class="normalfnt">
                    <tr>
                        <td width="106">Sub Total</td>
                        <td width="10">:</td>
                        <td width="70" align="right"><?php echo number_format($total_value,2)?></td>
                    </tr>
                    <tr>
                        <td>Tax Total</td>
                        <td>:</td>
                        <td align="right"><?php echo number_format($total_tax,2)?></td>
                    </tr>
                    <tr>
                        <td>Grand Total</td>
                        <td>:</td>
                        <td align="right"><?php echo number_format(round($total_value,2) + round($total_tax,2),2)?></td>
                    </tr>
                </table></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
</form>
</body>
<?php
function GetHeaderDetails($serialYear,$serialNo)
{
	global $db;
	
	$sql = "SELECT
			  CIH.INVOICE_NO								AS INVOICE_NO,
			  CIH.INVOICED_DATE								AS INVOICED_DATE,
			  CIH.ORDER_NO									AS ORDER_NO,
			  CIH.ORDER_YEAR								AS ORDER_YEAR,
			  CONCAT(CIH.ORDER_YEAR,'/',CIH.ORDER_NO)		AS CONCAT_ORDER_NO,
			  OH.strCustomerPoNo    						AS CUSTOMER_PONO,
			  CIH.LEDGER_ID    								AS LEDGER_ID,
			  CIH.BANK_ACCOUNT_ID                           AS BANK_ID,
			  CIH.INVOICE_TYPE                              AS INVOICE_TYPE,
			  (SELECT SUB_U.strUserName 
			  FROM sys_users SUB_U 
			  WHERE SUB_U.intUserId = OH.intMarketer)  		AS MARKETER_ID,
			  OH.intCurrency								AS CURRENCY_ID,
			  CU.strInvoiceType								AS INVOICE_TYPE,
			  CU.intId										AS CUSTOMER_ID,
			  CIH.REMARKS									AS REMARKS,
    	 	  CU.strName									AS CUSTOMER_NAME,
			  CU.strAddress									AS CUSTOMER_ADDRESS,
			  CU.strContactPerson							AS CONTACT_NAME,
			  CU.strVatNo									AS CUSTOMER_VAT_NO,
			  CU.strSVatNo									AS CUSTOMER_SVAT_NO,
			  CURR.strCode									AS CURRENCY_CODE,
			  CIH.LOCATION_ID								AS LOCATION_ID,
			  CIH.STATUS									AS STATUS,
			  IT.SALES_INVOICE_REPORT_NAME					AS SALES_INVOICE_REPORT_NAME,
			  CLH.strName									AS CUSTOMER_INVOICE_LOCATION
			FROM finance_customer_invoice_header CIH
				LEFT JOIN mst_customer_locations_header CLH ON CLH.intId = CIH.CUSTOMER_LOCATION
			INNER JOIN trn_orderheader OH
    		  ON OH.intOrderNo = CIH.ORDER_NO
      		    AND OH.intOrderYear = CIH.ORDER_YEAR
			INNER JOIN mst_customer CU
			  ON CU.intId = OH.intCustomer
			INNER JOIN mst_financecurrency CURR ON CURR.intId = CIH.CURRENCY_ID
			LEFT JOIN mst_invoicetype IT ON IT.intId = CU.strInvoiceType
			WHERE CIH.SERIAL_YEAR = '$serialYear'
				AND CIH.SERIAL_NO = '$serialNo'";
	$result = $db->RunQuery($sql);
	$header_array = mysqli_fetch_array($result);
	return $header_array;
}

function GetGridDetails($serialYear,$serialNo)
{
	global $db;
	
	$sql = "SELECT 
			OD.strSalesOrderNo								AS SALES_ORDER_NO,
			OD.strGraphicNo									AS GRAPHIC_NO,
			OD.strStyleNo 									AS STYLE_NO,
			P.strName										AS PART,
			CIB.DISPATCHED_GOOD_QTY							AS DISPATCHED_GOOD_QTY,
			CID.QTY											AS INVOICE_QTY,	
			CID.PRICE 										AS INVOICE_PRICE,
			CID.VALUE 										AS INVOICE_VALUE,
			CID.TAX_CODE 									AS TAX_CODE_ID,
			CID.COST_CENTER 								AS COST_CENTER_ID,
			CID.TAX_VALUE 									AS TAX_VALUE			
			FROM finance_customer_invoice_details CID
			INNER JOIN finance_customer_invoice_header CIH ON CIH.SERIAL_YEAR = CID.SERIAL_YEAR AND CIH.SERIAL_NO = CID.SERIAL_NO
			INNER JOIN trn_orderdetails OD ON OD.intOrderYear = CIH.ORDER_YEAR AND OD.intOrderNo = CIH.ORDER_NO AND OD.intSalesOrderId = CID.SALES_ORDER_ID
			INNER JOIN mst_part P ON P.intId = OD.intPart
			INNER JOIN finance_customer_invoice_balance CIB ON CIB.ORDER_YEAR = CIH.ORDER_YEAR AND CIB.ORDER_NO = CIH.ORDER_NO AND OD.intSalesOrderId = CIB.SALES_ORDER_ID			
			WHERE CID.SERIAL_YEAR = '$serialYear' AND CID.SERIAL_NO = '$serialNo'";
	return $db->RunQuery($sql);
}

function GetBankAccDetails($accNo)
{
    global $db;

    $sql = "select CHART_OF_ACCOUNT_NAME from finance_mst_chartofaccount where CHART_OF_ACCOUNT_ID = '$accNo'";
    return $db->RunQuery($sql);
}
?>
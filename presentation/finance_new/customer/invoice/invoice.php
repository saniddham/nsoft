<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$bulkReportId	= 896;
	
include_once "class/finance/cls_get_gldetails.php";
include_once ("class/cls_commonFunctions_get.php");
include_once "class/masterData/exchange_rate/cls_exchange_rate_get.php";
//include 	 "include/javascript.html";

$session_locationId 	= $_SESSION["CompanyID"];
$session_companyId 		= $_SESSION["headCompanyId"];

$serialNo				= (!isset($_REQUEST["SerialNo"])?'':$_REQUEST["SerialNo"]);
$serialYear				= (!isset($_REQUEST["SerialYear"])?'':$_REQUEST["SerialYear"]);
$customerPO				= (!isset($_REQUEST["CustomerPO"])?'':$_REQUEST["CustomerPO"]);
$programCode			= 'P0701';

$obj_GLDetails_get		= new Cls_Get_GLDetails($db);
$obj_common_function_get= new cls_commonFunctions_get($db);
$obj_exchgRate_get		= new cls_exchange_rate_get($db);
$COMMERCIAL_INVOICE = 1;
$TAX_INVOICE = 2;
$SVAT_INVOICE = 3;
$INVOICE = 5;
$invoiceType = $INVOICE;
$companyInvoiceType = $INVOICE;
if($serialNo!='' && $serialNo!='')
{
	$header_array 		= GetHeaderDetails($serialYear,$serialNo);
	$detail_result 		= GetGridDetails($serialYear,$serialNo);
	$gl_result 			= GetGLGridDetails($serialYear,$serialNo);
	$exchngRate_array	= $obj_exchgRate_get->GetAllValues($header_array['CURRENCY_ID'],2,$header_array['INVOICED_DATE'],$session_companyId,'RunQuery');
}
$company_vat_numbers 		= GetCompanyDetails($session_companyId);

if (isset($company_vat_numbers["strSVatNo"]) && $company_vat_numbers["strSVatNo"] != ''){
    $companyInvoiceType = $SVAT_INVOICE;
} else if (isset($company_vat_numbers["strVatNo"]) && $company_vat_numbers["strVatNo"] != ''){
    $companyInvoiceType = $TAX_INVOICE;
}

$orderComplete = false; 

if($obj_common_function_get->ValidateSpecialPermission('22',$_SESSION["userId"],'RunQuery'))
	$dateDisabled		= "";
else
	$dateDisabled		= "disabled='disabled'";	

if(!isset($_REQUEST["SerialNo"]))
	$invoiceDate		= date("Y-m-d");
else
	$invoiceDate		= $header_array["INVOICED_DATE"];

if(isset($header_array['OrderStatus']) && $header_array['OrderStatus'] == -10)
	$orderComplete = true; 
?>
    <html>
    <head>
        <title>Sales Invoice</title>
        <!--<script type="text/javascript" src="presentation/finance_new/customer/invoice/invoice.js"></script>-->

        <script>


            var customerPOAL	='<?php echo $customerPO; ?>';
        </script>
        <style>
            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: scroll; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: 80%;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
    </head>
    <body>
    <input type="hidden" name="companyInvoiceType" id="companyInvoiceType" style="width:150px;display:none" value="<?php echo $companyInvoiceType?>"/>
    <form id="frmSalesInvoice" name="frmSalesInvoice" method="post">
        <div align="center">
            <div class="trans_layoutS" style="width:1100px">
                <div class="trans_text">Sales Invoice</div>
                <table width="1100">
                    <tr>
                        <td colspan="2"><table width="100%%" border="0" class="normalfnt">
                                <tr>
                                    <td>Invoice No</td>
                                    <td><input type="text" name="txtInvoiceNo" id="txtInvoiceNo" style="width:150px" disabled="disabled" value="<?php echo (!isset($_REQUEST["SerialNo"])?'':$header_array["INVOICE_NO"])?>"/>
                                        <input type="text" name="txtSerialNo" id="txtSerialNo" style="width:150px;display:none" disabled="disabled" value="<?php echo $header_array["CONCAT_SERIAL_NO"]?>"/></td>
                                    <td>Date</td>
                                    <td><input name="txtInvoiceDate" type="text" value="<?php echo $invoiceDate ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtInvoiceDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo $dateDisabled?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                </tr>
                                <tr>
                                    <td width="16%">Customer PONo <span class="compulsoryRed">*</span></td>
                                    <td width="35%"><input type="text" name="txtCustomerPONo" id="txtCustomerPONo" style="width:150px" value="<?php echo (!isset($_REQUEST["SerialNo"])?'':$header_array["CUSTOMER_PONO"]);?>" class="validate[required]"/>
                                        <a class="button green small" id="btnSearch" >Search</a></td>
                                    <td width="16%">Shipment Date</td>
                                    <td width="33%"><input name="txtShipmentDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="txtShipmentDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                </tr>
                                <tr>
                                    <td>Customer <span class="compulsoryRed">*</span></td>
                                    <td><select id="cboCustomer" style="width:280px;height:20px" class="validate[required]">
                                            <?php echo $header_array["CUSTOMER_HTML"]?>
                                        </select></td>
                                    <td>Customer Invoice Location</td>
                                    <td><select name="cboCustomerInvLoc" class="validate[required]" id="cboCustomerInvLoc" style="width:280px;height:20px">
                                            <?php echo $header_array["CUSTOMER_INV_LOC_HTML"]?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td>Marketer</td>
                                    <td><input type="text" name="txtMarketer" id="txtMarketer" style="width:280px" value="<?php echo (!isset($_REQUEST["SerialNo"])?'':$header_array["MARKETER_ID"]);?>" disabled="disabled"/></td>
                                    <td>Currency</td>
                                    <td><select name="cboCurrency" id="cboCurrency" style="width:280px" disabled="disabled" class="cls_cbo_CurrencyCalExchangeRate">
                                            <option value=""></option>
                                            <?php
                                            $sql = "SELECT intId,strCode FROM  mst_financecurrency WHERE intStatus = 1 ORDER BY intId";
                                            $result = $db->RunQuery($sql);
                                            while($row = mysqli_fetch_array($result))
                                            {
                                                if($row["intId"]==$header_array["CURRENCY_ID"])
                                                    echo "<option value=\"".$row["intId"]."\" selected=\"selected\">".$row["strCode"]."</option>";
                                                else
                                                    echo "<option value=\"".$row["intId"]."\">".$row["strCode"]."</option>";
                                            }
                                            ?>
                                        </select> <input type="text" name="txtExchangeRate" id="txtExchangeRate" style="width:60px;text-align:right" disabled="disabled" value="<?php echo (!isset($_REQUEST["SerialNo"])?'':$exchngRate_array["AVERAGE_RATE"]);?>"title="Exchange Rate" class="cls_txt_exchangeRate validate[required]"/></td>
                                </tr>              <tr>
                                    <td valign="">Invoice Type</td>
                                    <td><select name="cboInvoiceType" id="cboInvoiceType" style="width:280px" class="validate[required]">
                                            <option value=""></option>
                                            <?php
                                            $sql = "SELECT intId, strInvoiceType FROM mst_invoicetype ORDER BY strInvoiceType ";
                                            $result = $db->RunQuery($sql);
                                            if ($header_array["INVOICE_TYPE"] == $SVAT_INVOICE && $companyInvoiceType == $SVAT_INVOICE){
                                                $invoiceType = $SVAT_INVOICE;
                                            } else if ($companyInvoiceType != $INVOICE ){
                                                $invoiceType = $TAX_INVOICE;
                                            }

                                            if (isset($header_array["INVOICE_TYPE"]) && $header_array["INVOICE_TYPE"] != 0){
                                                $invoiceType = $header_array["INVOICE_TYPE"] ;
                                            }
                                            while($row = mysqli_fetch_array($result))
                                            {
                                                if($row["intId"]==$invoiceType)
                                                    echo "<option value=\"".$row["intId"]."\" selected=\"selected\">".$row["strInvoiceType"]."</option>";
                                                else
                                                    echo "<option value=\"".$row["intId"]."\">".$row["strInvoiceType"]."</option>";
                                            }
                                            ?>
                                        </select></td>
                                    <td>Bank Account <span class="compulsoryRed">*</span></td>
                                    <td><select id="cboBank" name="cboBank" style="width:280px;height:20px" class="validate[required]">
                                            <?php   echo $obj_GLDetails_get->getBankGLCombo($header_array["BANK_ACCOUNT_ID"]); ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td valign="top">Remarks</td>
                                    <td rowspan="3" valign="top"><textarea name="txtRemarks" id="txtRemarks" style="width:280px;height:50px" ><?php echo (!isset($_REQUEST["SerialNo"])?'':$header_array["REMARKS"]);?></textarea></td>
                                    <td valign="top">Order No</td>
                                    <td valign="top" id="td_order"><?php echo (!isset($_REQUEST["SerialNo"])?'':$header_array["ORDER_HTML"]);?></td>
                                    <td style="display: none"><input type="hidden" name="orderNo" id="orderNo" value="<?php echo (!isset($_REQUEST["SerialNo"])?'':$header_array["ORDER_NO"]);?>"></td>
                                    <td style="display: none"><input type="hidden" name="orderYear" id="orderYear" value="<?php echo (!isset($_REQUEST["SerialNo"])?'':$header_array["ORDER_YEAR"]);?>"></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td valign="top">Complete</td>
                                    <td valign="top"><input type="checkbox" <?php if($orderComplete == true){echo 'checked="checked"';} ?>  id="chkCpmplete"/></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td valign="top">&nbsp;</td>
                                    <td valign="top">&nbsp;</td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td colspan="2" ><div style="overflow:scroll;width:1100px;height:250px;">
                                <table width="100%%" border="0" class="bordered" id="tblMain">
                                    <thead>
                                    <tr>
                                        <th width="3%"><input type="checkbox" checked="checked" class="cls_check_all"></th>
                                        <th width="15%">Sales Order No</th>
                                        <th width="15%">Dispatch Note</th>
                                        <th width="15%">Graphic No</th>
                                        <th width="13%">Style No</th>
                                        <th width="13%">Placement</th>
                                        <th width="8%">Dispatched</th>
                                        <th width="8%">PD</th>
                                        <th width="8%">FD</th>
                                        <th width="8%">Sample</th>
                                        <th width="8%">Missing Panel</th>
                                        <th width="8%">Other</th>
                                        <th width="9%">Invoice <span class="compulsoryRed">*</span></th>
                                        <th width="9%">Price</th>
                                        <th width="9%">Discount Percentage</th>
                                        <th width="11%">Value</th>
                                        <th width="11%">Tax</th>
                                        <th width="11%">Cost Center <span class="compulsoryRed">*</span></th>
                                        <th width="11%">Item <span class="compulsoryRed">*</span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $loop = 0;
                                    if(isset($_REQUEST["SerialNo"]))
                                    {       $x=0;
                                        while($row = mysqli_fetch_array($detail_result))
                                        {
                                            ?>
                                            <tr>
                                                <td class="cls_td_check" style="text-align:center"><input type="checkbox" checked="checked" class="cls_check"></td>
                                                <td class="cls_td_salesOrderNo" id="<?php echo $row["SALES_ORDER_ID"]; ?>"><?php echo $row["SALES_ORDER_NO"]?> <input type='hidden' value="<?php echo $row["SALES_ORDER_NO"]?>" name="Snohidden" id="Snohidden"></td>
                                                <td class="btn_modal" width="15%"> <button type="button" class="btn btn-info btn-lg" onClick="getDispatchNote(<?php echo $header_array['ORDER_NO']?>,<?php echo $header_array['ORDER_YEAR']?>,'<?php echo (!isset($_REQUEST["SerialNo"])?'':$header_array["INVOICE_NO"])?>','<?php echo $row["SALES_ORDER_ID"]; ?>');" data-toggle="modal"  id="myBtn_<?php echo $x;?>">+</button></td>

                                                <td width="15%"><?php echo $row["GRAPHIC_NO"]?></td>
                                                <td width="13%"><?php echo $row["STYLE_NO"]?></td>
                                                <td width="13%"><?php echo $row["PART"]?></td>
                                                <td class="cls_td_qty" style="text-align:right"><?php echo $row["DISPATCHED_GOOD_QTY"]?></td>
                                                <td class="cls_td_PD cls_Subtract" style="text-align:center"><input type="textbox" style="width:50px;text-align:right" value="<?php echo $row["PRODUCTION_DAMAGE"]?>"></td>
                                                <td class="cls_td_FD cls_Subtract" style="text-align:center"><input type="textbox" style="width:50px;text-align:right" value="<?php echo $row["FABRIC_DAMAGE"]?>"></td>
                                                <td class="cls_td_sample cls_Subtract" style="text-align:center"><input type="textbox" style="width:50px;text-align:right" value="<?php echo $row["SAMPLE_QTY"]?>"></td>
                                                <td class="cls_td_missingPanal cls_Subtract" style="text-align:center"><input type="textbox" style="width:50px;text-align:right" value="<?php echo $row["MISSING_PANAL"]?>"></td>
                                                <td class="cls_td_other cls_Subtract" style="text-align:center"><input type="textbox" style="width:50px;text-align:right" value="<?php echo $row["OTHER"]?>"></td>
                                                <td class="cls_td_invoice cls_Subtract" style="text-align:center"><input type="textbox" style="width:50px;text-align:right" value="<?php echo $row["INVOICE_QTY"]?>"></td>
                                                <td style="text-align:right"><?php echo $row["INVOICE_PRICE"]?></td>
                                                <td style="text-align:right" class="cls_td_percentage">
                                                <select class="cls_cbo_percentage">
                                                
  												<option value="0">0%</option>
  												<option value="3">3%</option>
                                                </select>
                                                </td>
                                                <td class="cls_td_value" style="text-align:right"><?php echo $row["INVOICE_VALUE"]?></td>
                                                <td style="text-align:center" class="cls_td_tax" id="<?php echo $row["TAX_VALUE"]?>"><?php echo GetTax(++$loop,$row["TAX_CODE_ID"])?></td>
                                                <td style="text-align:center"><?php echo GetCostCenter(++$loop,$row["COST_CENTER_ID"])?></td>
                                                <td style="text-align:center"><?php echo GetItem_HTML(++$loop,$row["GL_ACCOUNT"])?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div></td>
                    </tr>
                    <tr>
                        <td width="510" ><table width="87%" class="bordered" id="tblGL" style="display:none">
                                <thead>
                                <tr>
                                    <th colspan="3" >GL Allocation
                                        <div style="float:right"><a class="button white small" id="butAddNewGL">Add New GL</a></div></th>
                                </tr>
                                <tr>
                                    <th width="6%" >Del</th>
                                    <th width="69%" >GL Account <span class="compulsoryRed">*</span></th>
                                    <th width="25%" >Amount <span class="compulsoryRed">*</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $booAvailable	= '';
                                if(isset($_REQUEST["SerialNo"]))
                                {
                                    while($row = mysqli_fetch_array($gl_result))
                                    {
                                        $booAvailable	= true;
                                        ?>
                                        <tr class="cls_tr_firstRow">
                                            <td width="6%" style="text-align:center" ><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover removeRow" id="butDel" tabindex="24"/></td>
                                            <td width="69%" class="cls_td_GL"><select name="cboLedgerAc" id="cboLedgerAc" class="clsLedgerAc validate[required]"  style="width:100%" >
                                                    <?php
                                                    echo $obj_GLDetails_get->getGLCombo('CUSTOMER_INVOICE',$row["GL_ACCOUNT"]);
                                                    //echo Get_GL_HTML($row["GL_ACCOUNT"]);
                                                    ?>
                                                </select></td>
                                            <td width="25%" style="text-align:center" class="cls_td_GLAmount"><input id="txtGLAmount" name="txtGLAmount" class="validate[required] cls_GLAmount" type="text" style="width:100%;text-align:right" value="<?php echo ($row['GL_AMOUNT']==''?0:round($row['GL_AMOUNT'],2))?>"/></td>
                                        </tr>
                                        <?php
                                    }
                                }

if(!$booAvailable)
{
?>
                <tr class="cls_tr_firstRow">
                  <td width="6%" style="text-align:center" ><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover removeRow" id="butDel" tabindex="24"/></td>
                  <td width="69%" class="cls_td_GL"><select name="cboLedgerAc" id="cboLedgerAc" class="clsLedgerAc validate[required]"  style="width:100%" >
                      <?php
					  		echo $obj_GLDetails_get->getGLCombo('CUSTOMER_INVOICE',$row["GL_ACCOUNT"]);
							//echo Get_GL_HTML($row["GL_ACCOUNT"]);
                      ?>
                    </select></td>
                  <td width="25%" style="text-align:center" class="cls_td_GLAmount"><input id="txtGLAmount" name="txtGLAmount" class="validate[required] cls_GLAmount" type="text" style="width:100%;text-align:right" value="<?php echo ($row['GL_AMOUNT']==''?0:round($row['GL_AMOUNT'],2))?>"/></td>
                </tr>
                <?php
}
?>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2" style="text-align:center"><b>TOTAL</b></td>
                  <td colspan="3"><input type="text" style="width:100%;text-align:right" value="0" disabled="disabled" class="td_totalGLAlloAmount"/></td>
                </tr>
              </tfoot>
            </table></td>
          <td width="586" valign="top"><table width="200" border="0" align="right" class="normalfnt" cellpadding="0" cellspacing="2">
              <tr>
                <td width="80">Sub Total</td>
                <td width="10">:</td>
                <td width="96"><input name="txtSubTotal" type="text" disabled="disabled" id="txtSubTotal" value="0.0000" style="width:100%;text-align:right" /></td>
              </tr>
              <tr>
                <td>Tax Total</td>
                <td>:</td>
                <td><input name="txtTotTax" type="text" disabled="disabled" id="txtTotTax" value="0.0000" style="width:100%;text-align:right" /></td>
              </tr>
              <tr>
                <td>Grand Total</td>
                <td>:</td>
                <td><input name="txtGrandTotal" type="text" disabled="disabled" id="txtGrandTotal" value="0.0000" style="width:100%;text-align:right" /></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2" align="center"><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($form_permision['edit']==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');?><a class="button white medium" id="butCancel">Cancel</a><a class="button white medium" id="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
        </tr>
      </table>
    </div>
  </div>
</form>

    <!-- The Modal -->
    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">

            <span class="close">&times;</span>
            <h5>Dispatch Note</h5>
            <form name="interval_form">
                <input type="hidden" name="Sid" id="Sid" value="" >
                <input type="hidden" name="orderNo" id="orderNohidden" value="" >
                <input type="hidden" name="orderYear" id="orderYearhidden" value="" >
                <table class="bordered" id="tbl_dispatch" border="1" width="100%">
                    <thead>
                    <tr id="first">
                        <th><input type='checkbox' class='cls_checkALL_modal'></th>
                        <th>Dispatch No</th>
                        <th>SO</th>
                        <th>Part</th>
                        <th>Graphic</th>
                        <th>BG Colour</th>
                        <th>Line No</th>
                        <th  style='display: none'>Cut No</th>
                        <th  style='display: none'>Size</th>
                        <th> Dispatch Good Qty</th>
                        <th  style='display: none'>PD</th>
                        <th  style='display: none'>FD</th>
                        <th  style='display: none'>Sample No</th>
                        <th  style='display: none'>Missing Pannel</th>
                        <th  style='display: none'>Other</th>
                        <th style="display: none">Invoice Qty</th>
                        <th  style='display: none'>Invoice Price</th>
                        <th style="display:none">SO id</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                <br/>
                <button type="button" id="btnModal" name="btnModal">SAVE</button>
            </form>
        </div>

    </div>






    <script>

        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the button that opens the modal
        var btn =  document.getElementById("myBtn_0");


        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal



        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
            // var x= $('#frmShift #cboShift').val();


        };
        function getDispatchNote(orderNo,orderYear,SALES_ORDER_ID,sNo) {
            if(orderNo =='' && orderYear == '' ){
                alert('Please Save Before Select Dispatch No');
                return false;
            }




            $('#Sid').val(sNo);
            $('#orderNohidden').val(orderNo);
            $('#orderYearhidden').val(orderYear);
           // var Sno=$('.cls_td_salesOrderNo').val();

            //POHHH
            var SerialNo =$('#frmSalesInvoice #txtInvoiceNo').val();
            var Sno=SerialNo.split("-");
            var SaleorderId = $('#Snohidden').val();

            if(Sno[0] == ''){
                alert("Please Save Sales Invoice !")
                return false;
            }
            $(".btn-info").click(function(){
                modal.style.display = "block";

                $('.cls_checkALL_modal').click(function() { $(this.form.elements).filter(':checkbox').prop('checked', this.checked);
                });
            });
            // console.log(orderNo);
            // console.log(orderYear);
            // console.log(salesOrder);
            var url = "presentation/finance_new/customer/invoice/invoice_db.php?requestType=showDispatachNote";
            $("#tbl_dispatch > tbody").empty();
            var request = $.ajax({
                url: url,
                type: "POST",
                data: {RequestType:'showDispatachNote',orderNo:orderNo,orderYear:orderYear,SerialNo:SerialNo,SaleorderId:sNo},
                Type: 'json',
                success: function(html) {
                    $("#tbl_dispatch > tbody").empty();
                    $("#tbl_dispatch > tbody").append(html);


                    $('#tbl_dispatch >tbody >tr').not("#first").each(function() {

                        var fabricDetails =  $(this).find('#fabricDetails').val();

                        $(this).find('#modal_check').change(function(){


                        if($(this).attr("checked"))
                        {

                        }
                        else
                        {
                            $.ajax({
                                url: url,
                                type: "POST",
                                data: {RequestType:'DeleteUnchecked',orderNo:orderNo,orderYear:orderYear,SerialNo:SerialNo,SaleorderId:sNo,fabricDetails:fabricDetails},
                                Type: 'json',
                                success: function(html) {
                                    console.log(html);
                                }
                            })

                        }
                    });

                    });

                    if($('#type').val()=='fail') {
                       // $("#tbl_dispatch").css("display", "none");
                        var msg=$('#msg').val();
                        $('#btnModal').css("display", "none");
                        $( "#tbl_dispatch" ).replaceWith( "<h2 style='color: #cc4444    '>"+msg+"</h2>" );
                        //modal.style.display = "none";

                    }




                }
            });

        }


    </script>

    </body>
    </html>
<?php
function GetHeaderDetails($serialYear,$serialNo)
{
	global $db;
	global $bulkReportId;
	
	$sql = "SELECT
			  CONCAT(CIH.SERIAL_NO,'-',CIH.SERIAL_YEAR)		AS CONCAT_SERIAL_NO,
			  CIH.INVOICE_NO								AS INVOICE_NO,
			  CIH.INVOICED_DATE								AS INVOICED_DATE,
			  CIH.ORDER_NO									AS ORDER_NO,
			  CIH.ORDER_YEAR								AS ORDER_YEAR,
			  CIH.INVOICE_TYPE                              AS INVOICE_TYPE,
			  CONCAT(CIH.ORDER_YEAR,'/',CIH.ORDER_NO)		AS CONCAT_ORDER_NO,
			  OH.strCustomerPoNo    						AS CUSTOMER_PONO,
			  CIH.CUSTOMER_LOCATION							AS CUST_INV_LOCATION_ID,
			  CIH.LEDGER_ID    								AS LEDGER_ID,
			  CIH.BANK_ACCOUNT_ID    						AS BANK_ACCOUNT_ID,
			  (SELECT SUB_U.strUserName 
			  FROM sys_users SUB_U 
			  WHERE SUB_U.intUserId = OH.intMarketer)  		AS MARKETER_ID,
			  OH.intCurrency								AS CURRENCY_ID,
			  CU.intId										AS CUSTOMER_ID,
			  CIH.REMARKS									AS REMARKS,
    	 	  CU.strName									AS CUSTOMER_NAME,
			  CIH.INVOICED_DATE								AS INVOICED_DATE,
			   OH.intStatus    								AS OrderStatus
			FROM finance_customer_invoice_header CIH
			INNER JOIN trn_orderheader OH
    		  ON OH.intOrderNo = CIH.ORDER_NO
      		    AND OH.intOrderYear = CIH.ORDER_YEAR 
			INNER JOIN mst_customer CU
			  ON CU.intId = OH.intCustomer
			WHERE CIH.SERIAL_YEAR = '$serialYear'
				AND CIH.SERIAL_NO = '$serialNo'";
				
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);

	$header_array["INVOICE_NO"]				= $row["INVOICE_NO"];
	$header_array["INVOICE_TYPE"]	        = $row["INVOICE_TYPE"];
	$header_array["CONCAT_SERIAL_NO"]		= $row["CONCAT_SERIAL_NO"];
	$header_array["INVOICED_DATE"]			= $row["INVOICED_DATE"];
	$header_array["CUSTOMER_PONO"]			= $row["CUSTOMER_PONO"];
	$header_array["LEDGER_ID"]				= $row["LEDGER_ID"];
	$header_array["MARKETER_ID"]			= $row["MARKETER_ID"];
	$header_array["CURRENCY_ID"]			= $row["CURRENCY_ID"];
	$header_array["BANK_ACCOUNT_ID"]		= $row["BANK_ACCOUNT_ID"];
	$header_array["INVOICE_TYPE"]			= $row["INVOICE_TYPE"];
	$header_array["REMARKS"]				= $row["REMARKS"];
	$header_array["OrderStatus"]				= $row["OrderStatus"];
	$header_array["CUSTOMER_HTML"]			= "<option value=\"".$row["CUSTOMER_ID"]."\">".$row["CUSTOMER_NAME"]."</option>";
	$header_array["CUSTOMER_INV_LOC_HTML"]	= CreateCustomerLocationHTML($row["CUSTOMER_ID"],$row["CUST_INV_LOCATION_ID"]);
	$header_array["ORDER_HTML"]				= "<a href=\"?q=$bulkReportId&orderNo=".$row["ORDER_NO"]."&orderYear=".$row["ORDER_YEAR"]."\" id=\"txtOrderNo\" target=\"_blank\">".$row["CONCAT_ORDER_NO"]."</a>";
    $header_array['ORDER_NO']   =$row["ORDER_NO"];
    $header_array['ORDER_YEAR']   =$row["ORDER_YEAR"];


    return $header_array;
}

function GetCompanyDetails($companyId)
{
    global $db;

    $sql = "select strVatNo, strSVatNo from mst_companies WHERE intId = '$companyId'";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);

    return $row;
}

function GetGridDetails($serialYear,$serialNo)
{
	global $db;
	
	$sql = "SELECT 
			OD.strSalesOrderNo								AS SALES_ORDER_NO,
			OD.intSalesOrderId								AS SALES_ORDER_ID,
			OD.strGraphicNo									AS GRAPHIC_NO,
			OD.strStyleNo 									AS STYLE_NO,
			P.strName										AS PART,
			CIB.DISPATCHED_GOOD_QTY							AS DISPATCHED_GOOD_QTY,
			CID.PRODUCTION_DAMAGE_QTY						AS PRODUCTION_DAMAGE,
			CID.FABRIC_DAMAGE_QTY							AS FABRIC_DAMAGE,
			CID.SAMPLE_QTY									AS SAMPLE_QTY,
			CID.MISSING_PANAL								AS MISSING_PANAL,
			CID.OTHER										AS OTHER,
			CID.QTY											AS INVOICE_QTY,	
			CID.PRICE 										AS INVOICE_PRICE,
			CID.VALUE 										AS INVOICE_VALUE,
			CID.TAX_CODE 									AS TAX_CODE_ID,
			CID.COST_CENTER 								AS COST_CENTER_ID,
			CID.TAX_VALUE									AS TAX_VALUE,
			CID.GL_ACCOUNT									AS GL_ACCOUNT
			FROM finance_customer_invoice_details CID
			INNER JOIN finance_customer_invoice_header CIH ON CIH.SERIAL_YEAR = CID.SERIAL_YEAR AND CIH.SERIAL_NO = CID.SERIAL_NO
			INNER JOIN trn_orderdetails OD ON OD.intOrderYear = CIH.ORDER_YEAR AND OD.intOrderNo = CIH.ORDER_NO AND OD.intSalesOrderId = CID.SALES_ORDER_ID
			INNER JOIN mst_part P ON P.intId = OD.intPart
			INNER JOIN finance_customer_invoice_balance CIB ON CIB.ORDER_YEAR = CIH.ORDER_YEAR AND CIB.ORDER_NO = CIH.ORDER_NO AND OD.intSalesOrderId = CIB.SALES_ORDER_ID
			WHERE CID.SERIAL_YEAR = '$serialYear' AND CID.SERIAL_NO = '$serialNo'";
	return $db->RunQuery($sql);
}

function GetTax($loop,$id)
{
	global $db;
	$string 	= "";
	
	$sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strCode
			FROM
			mst_financetaxgroup
			ORDER BY strCode";
	$result = $db->RunQuery($sql);
		$string .= "<select style=\"width:90px\" id=\"cboTax$loop\" class=\"cls_cbo_tax\">";
		$string .= "<option value=\""."NULL"."\">".""."</option>";
	while($row = mysqli_fetch_array($result))
	{
		if($id==$row['intId'])
			$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";
		else
			$string .= "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
	}
		$string .= "</select>";
	return $string;
}

function GetCostCenter($loop,$id)
{
	global $db;
	$string 	= "";
	$sql = "SELECT
				intId,
				strName
			FROM mst_financedimension
			WHERE
				intStatus = 1
			ORDER BY strName";
	$result = $db->RunQuery($sql);
		$string .= "<select style=\"width:90px\" class=\"validate[required]\" id=\"cboCostCenter$loop\">";
		$string .= "<option value=\"".""."\">".""."</option>";
	while($row = mysqli_fetch_array($result))
	{
		if($id==$row['intId'])
			$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
		else
			$string .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
	}
		$string .= "</select>";
	return $string;
}

function GetItem_HTML($loop,$id)
{
	global $db;
	global $session_companyId;
	
	$sql = "SELECT intId , strName FROM mst_financecustomeritem ORDER BY strName";
	$result = $db->RunQuery($sql);
		$string = "<select style=\"width:90px\" id=\"cboGL$loop\" class=\"validate[required]\">";
		$string .= "<option value=\"".""."\">".""."</option>";
	while($row = mysqli_fetch_array($result))
	{
		if($id == $row['intId'])
			$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
		else
			$string .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
	}
		$string .= "</select>";
	return $string;
}

function GetGLGridDetails($serialYear,$serialNo)
{
	global $db;
	
	$sql = "SELECT GL_ACCOUNT ,GL_AMOUNT 
			FROM finance_customer_invoice_gl 
			WHERE SERIAL_NO = '$serialNo' AND SERIAL_YEAR = '$serialYear'
			ORDER BY ORDER_BY_ID";
	return $db->RunQuery($sql);
}

function Get_GL_HTML($gl_id)
{
	global $db;
	
	  $sql = "SELECT
			mst_financechartofaccounts.intId,
			mst_financechartofaccounts.strCode,
			mst_financechartofaccounts.strName
			FROM
			mst_financechartofaccounts
			WHERE
			mst_financechartofaccounts.strType = 'Posting' AND
			mst_financechartofaccounts.intStatus =  '1' AND
			(mst_financechartofaccounts.intFinancialTypeId =  '6' OR mst_financechartofaccounts.intFinancialTypeId =  '5' OR mst_financechartofaccounts.intFinancialTypeId =  '8')
			order by strCode";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		if($row['intId'] == $gl_id)
			echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']." - ".$row['strName']."</option>";
		else
			echo "<option value=\"".$row['intId']."\">".$row['strCode']." - ".$row['strName']."</option>";
	}
}

function CreateCustomerLocationHTML($customerId,$cusLocationId)
{
	global $db;
	$html	= "";
	$sql = "SELECT
			  CLH.intId	AS LOCATION_ID,
			  CLH.strName	AS LOCATION_NAME
			FROM mst_customer_locations CL
			  INNER JOIN mst_customer_locations_header CLH
				ON CLH.intId = Cl.intLocationId
			WHERE intCustomerId = $customerId
			ORDER BY CLH.strName";				
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		if($cusLocationId==$row["LOCATION_ID"])
			$html 	.= "<option value=\"".$row["LOCATION_ID"]."\" selected=\"selected\">".$row["LOCATION_NAME"]."</option>";
		else
			$html 	.= "<option value=\"".$row["LOCATION_ID"]."\">".$row["LOCATION_NAME"]."</option>";
			
	}
	return $html;
}
?>
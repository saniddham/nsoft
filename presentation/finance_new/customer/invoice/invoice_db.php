<?php
//ini_set('display_errors',1);
session_start();
//BEGIN - INCLUDE FILES {
include  "../../../../dataAccess/Connector.php";
include  "../../../../class/finance/customer/cls_invoice_get.php";
include  "../../../../class/finance/customer/cls_invoice_set.php";
include  "../../../../class/finance/cls_calculate_tax.php";
//include_once ("../../../../class/finance/customer/cls_common_get.php");
//include_once  "../../../../class/cls_commonFunctions_get.php";

//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {

$obj_common_get		= new Cls_Common_Get($db);
$obj_invoice_get	= new Cls_Invoice_Get($db,$obj_common_get);
$obj_invoice_set	= new Cls_Invoice_Set($db);
$obj_calculate_tax	= new Cls_Calculate_Tax($db);
$obj_common			= new cls_commonFunctions_get($db);

//END 	- CREATE OBJECTS }
$requestType	= $_REQUEST["RequestType"];

if($requestType=="URLLoadData")
{
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	$customerPoNo		= $obj_common->replace($arrHeader["CustomerPoNo"]);
	
	echo $obj_invoice_get->LoadData($customerPoNo);
}
elseif($requestType=="URLLoadMultiData")
{
	$customerPoNo		= $_REQUEST["CustomerPoNo"];
	$customerId			= $_REQUEST["CustomerId"];
	
	echo $obj_invoice_get->LoadMultiData($customerPoNo,$customerId);
}
elseif($requestType=="URLSave")
{
	$customerPoNo		= $_REQUEST["CustomerPoNo"];
	$orderNoArray		= explode('/',$_REQUEST["OrderNo"]);
	$orderNo			= $orderNoArray[1];
	$orderYear			= $orderNoArray[0];
	$customerId			= $_REQUEST["CustomerId"];
	$customerLocation	= $_REQUEST["CustomerLocation"];
	$invoicedDate		= $_REQUEST["InvoicedDate"];
	$remarks			= $_REQUEST["Remarks"];
	$complete			= $_REQUEST["Complete"];
	$currencyId			= $_REQUEST["CurrencyId"];
	$invoiceType		= $_REQUEST["InvoiceType"];
	$subTotal			= $_REQUEST["SubTotal"];
	$taxTotal			= $_REQUEST["TaxTotal"];
	$grandTotal			= $_REQUEST["GrandTotal"];
	$bankId				= $_REQUEST["bankId"];
	$grid				= json_decode($_REQUEST["Grid"],true);
	$GLGrid				= json_decode($_REQUEST["GLGrid"],true);
	
	echo $obj_invoice_set->Save($customerPoNo,$orderNo,$orderYear,$customerId,$customerLocation,$invoicedDate,$remarks,$complete,$grid,$currencyId,$GLGrid,$invoiceType,$subTotal,$taxTotal,$grandTotal,$bankId);
}
elseif($requestType=="URLCalculateTax")
{
	$taxId				= $_REQUEST["TaxId"];
	$value				= $_REQUEST["Value"];
	echo $obj_calculate_tax->CalculateTax($taxId,$value);
	
}
elseif($requestType=="URLCancel")
{
	$serialNo			= explode('-',$_REQUEST["SerialNo"]);
	
	$response		= $obj_invoice_get->ValidateBeforeCancel($serialNo[0],$serialNo[1]);
	
	if($response["type"]=='false')
	{
		echo json_encode($response);
		return;
	}
	
    echo $obj_invoice_set->Cancel($serialNo[1],$serialNo[0]);
}elseif($requestType=="showDispatachNote"){
    $orderNo=$_REQUEST['orderNo'];
    $orderYear=$_REQUEST['orderYear'];
    $SerialNo=$_REQUEST['SerialNo'];
    $SaleorderId=$_REQUEST['SaleorderId'];
    $checkDuplicate = $obj_invoice_get->CheckDuplicates($orderNo,$orderYear,$SerialNo,$SaleorderId);
    $status =json_decode($checkDuplicate ,true);
    if($status['type'] == 'pass'){
        $result = $obj_invoice_get->GetDispatchNote($orderNo, $orderYear, $SerialNo, $SaleorderId);
        $html = '';
        $x = 1;
        while ($row = mysqli_fetch_array($result)) {


            $salesOrder = $row['SALES_ORDER_NO'];
            $GRAPHIC_NO = $row['GRAPHIC_NO'];
            $STYLE_NO = $row['STYLE_NO'];
            $PART = $row['PART'];
            $DISPATCHED_GOOD_QTY = $row['DISPATCHED_GOOD_QTY'];
            $PRODUCTION_DAMAGE = $row['PRODUCTION_DAMAGE'];
            $FABRIC_DAMAGE = $row['FABRIC_DAMAGE'];
            $SAMPLE_QTY = $row['SAMPLE_QTY'];
            $MISSING_PANAL = $row['MISSING_PANAL'];
            $OTHER = $row['OTHER'];
            $INVOICE_QTY = $row['INVOICE_QTY'];
            $INVOICE_PRICE = $row['INVOICE_PRICE'];
            $INVOICE_VALUE = $row['INVOICE_VALUE'];
            $FabricDispatchNo = $row['FabricDispatchNo'];
            $FabricDispatchYear = $row['FabricDispatchYear'];
            $SampleNo = $row['SampleNo'];
            $SampleYear = $row['SampleYear'];
            $Combo = $row['Combo'];
            $Print = $row['PrintName'];
            $RevisionNo = $row['RevisionNo'];
            $CutNo = $row['CUT_NO'];
            $LineNo = $row['LineNo'];
            $SALES_ORDER_ID = $row['SALES_ORDER_ID'];
            $size = $row['Size'];

            $ORDER_NO = $row['ORDER_NO'];
            $ORDER_YEAR = $row['ORDER_YEAR'];

            $GL_ACCOUNT = $row['GL_ACCOUNT'];
            $TAX_CODE_ID = $row['TAX_CODE_ID'];
            $COST_CENTER_ID = $row['COST_CENTER_ID'];
            $SAMPLE_QTY = $row['SAMPLE_QTY'];
            $TAX_VALUE = $row['TAX_VALUE'];
            $SERIAL_NO = $row['SERIAL_NO'];
            $SERIAL_YEAR = $row['SERIAL_YEAR'];
            $groundColor = getGroundColor($SampleNo, $SampleYear, $Print, $Combo, $RevisionNo);
            $getSize = getSize($ORDER_NO, $ORDER_YEAR);
            $salesOrder = getSalesOrderNo($ORDER_NO,$ORDER_YEAR,$SALES_ORDER_ID);
            //$goodqty = getGoodDqty($FabricDispatchNo,$FabricDispatchYear,$SALES_ORDER_ID);
           $LineNo= getLineNo($ORDER_NO,$ORDER_YEAR,$SALES_ORDER_ID);
            $radio_flag = $row['flag'];

           if ($DISPATCHED_GOOD_QTY != '') {
                $html .= "<tr>";
                if ($radio_flag != '') {
                    $html .= "<td><input id='modal_check' type='checkbox' value='$x' class='cls_check_modal' checked></td>";
                } else {
                    $html .= "<td><input id='modal_check' type='checkbox' value='$x' class='cls_check_modal'></td>";
                }

                $html .= "<td>$FabricDispatchNo/$FabricDispatchYear <input type='hidden' value='$FabricDispatchNo/$FabricDispatchYear' name ='fabricDetails' id='fabricDetails'></td>";
                $html .= "<td>$salesOrder <input type='hidden' value='$salesOrder' name ='salesOrder' id='salesOrder'></td>";
                $html .= "<td>$PART <input type='hidden' value='$PART' name ='part' id='part'></td>";
                $html .= "<td>$GRAPHIC_NO <input type='hidden' value='$GRAPHIC_NO' name ='graphic_no' id='graphic_no'></td>";
                $html .= "<td>$groundColor<input type='hidden' value='$groundColor' name ='groundColour' id='groundColour'></td>";
                $html .= "<td>$LineNo <input type='hidden' value='$LineNo' name ='lineNo' id='lineNo'></td>";
                $html .= "<td style='display: none'>$CutNo <input type='hidden' value='$CutNo' name ='CutNo' id='CutNo'></td>";
                $html .= "<td style='display: none'>$size <input type='hidden' value='$size' name ='Size' id='Size'></td>";
                $html .= "<td>$DISPATCHED_GOOD_QTY <input type='hidden' value='$DISPATCHED_GOOD_QTY' name ='DispatchGoodQty' id='DispatchGoodQty'></td>";
                $html .= "<td style='display: none'>$PRODUCTION_DAMAGE <input type='hidden' value='$PRODUCTION_DAMAGE' name ='ProductionDamage' id='ProductionDamage'></td>";
                $html .= "<td style='display: none'>$FABRIC_DAMAGE <input type='hidden' value='$FABRIC_DAMAGE' name ='FabricDamge' id='FabricDamge'></td>";
                $html .= "<td style='display: none'>$SampleNo <input type='hidden' value='$SampleNo' name ='SampleNo' id='SampleNo'></td>";
                $html .= "<td style='display: none'>$MISSING_PANAL <input type='hidden' value='$MISSING_PANAL' name ='missingPanel' id='missingPanel'></td>";
                $html .= "<td style='display: none'>$OTHER <input type='hidden' value='$OTHER' name ='other' id='other'></td>";
                $html .= "<td style='display: none'>$INVOICE_QTY <input type='hidden' value='$INVOICE_QTY' name ='invoiceQty' id='invoiceQty'></td>";
                $html .= "<td style='display: none'>$INVOICE_PRICE <input type='hidden' value='$INVOICE_PRICE' name ='invoicePrice' id='invoicePrice'></td>";
                $html .= "<td style='display: none'>$INVOICE_VALUE <input type='hidden' value='$INVOICE_VALUE' name ='invoiceValue' id='invoiceValue'></td>";
//hidden
                $html .= "<td style='display: none'>$SALES_ORDER_ID <input type='hidden' value='$SALES_ORDER_ID' name ='salesOrderId' id='salesOrderId'></td>";
                $html .= "<td style='display: none'>$GL_ACCOUNT <input type='hidden' value='$GL_ACCOUNT' name ='GLAccount' id='GLAccount'></td>";
                $html .= "<td style='display: none'>$TAX_CODE_ID <input type='hidden' value='$TAX_CODE_ID' name ='TaxCode' id='TaxCode'></td>";
                $html .= "<td style='display: none'>$COST_CENTER_ID <input type='hidden' value='$COST_CENTER_ID' name ='CostCenter' id='CostCenter'></td>";
                $html .= "<td style='display: none'>$SAMPLE_QTY <input type='hidden' value='$SAMPLE_QTY' name ='SampleQty' id='SampleQty'></td>";
                $html .= "<td style='display: none'>$SERIAL_YEAR <input type='hidden' value='$SERIAL_YEAR' name ='SerialYear' id='SerialYear'></td>";
                $html .= "<td style='display: none'>$SERIAL_NO <input type='hidden' value='$SERIAL_NO' name ='SerialNo' id='SerialNo'></td>";

                $html .= "<td style='display: none'>$TAX_VALUE <input type='hidden' value='$TAX_VALUE' name ='TaxValue' id='TaxValue'></td>";

                $html .= "</tr>";
                $x++;
            }
        }

        echo $html;
    }else if($status['type'] =='fail'){
        $msg =$status['msg'];
        echo "<td style='display: none'><input  name='msg' id='msg' type='hidden' value='$msg'></td>
        <td style='display: none'><input  name='type' id='type' type='hidden' value='fail'></td>";

    }
} elseif($requestType=="AddModalData"){

    $arr = json_decode($_REQUEST['arr'],true);
    $Maindetails = array (
    'SalesorderId'=>$_REQUEST['SalesorderId'],
    'Orderno'=>$_REQUEST['orderNo'],
    'Orderyear'=>$_REQUEST['orderYear'],
    'SerialNo'=>$_REQUEST['SerialNo'],
    'Serialyear'=>$_REQUEST['SerialYear'],
    );






    echo $obj_invoice_get->AddModalData($arr,$Maindetails);

}else if($requestType == 'DeleteUnchecked') {

  $Serial =explode( '-', $_REQUEST['SerialNo'] );

  $fabricdetails =explode( '/',$_REQUEST['fabricDetails']);




    echo $obj_invoice_get->DeleteUnchecked($Serial,$fabricdetails,$_REQUEST['SaleorderId']);

}





function getGroundColor($sampNo,$sampleYear,$printName,$combo,$revNo)
{
    global $db;
    $sql = "SELECT DISTINCT
				    trn_sampleinfomations_details.intGroundColor,
				    mst_colors_ground.strName
				    FROM
					trn_sampleinfomations_details 
					Inner Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
				    WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear'  AND
					trn_sampleinfomations_details.strPrintName =  '$printName' AND
					trn_sampleinfomations_details.strComboName =  '$combo' AND
					trn_sampleinfomations_details.intRevNo =  '$revNo' ";

    $result = $db->RunQuery($sql);
    $row=mysqli_fetch_array($result);

    $g= $row['strName'];
    if($revNo=='')
        $g='';
    return $g;
}

function getSize($ORDER_NO,$ORDER_YEAR)
{

    global $db;
    $sql = "SELECT
	ware_fabricreceiveddetails.strSize
FROM
	ware_fabricreceiveddetails
INNER JOIN ware_fabricreceivedheader ON ware_fabricreceiveddetails.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo
AND ware_fabricreceiveddetails.intFabricReceivedYear = ware_fabricreceivedheader.intFabricReceivedYear
WHERE
	ware_fabricreceivedheader.intOrderNo = '$ORDER_NO' 
	AND 
	ware_fabricreceivedheader.intOrderYear = '$ORDER_YEAR' 
	";
    $result = $db->RunQuery($sql);
    return $result;




}
function getGoodDqty($FabricDispatchNo, $FabricDispatchYear, $SALES_ORDER_ID)
{
//var_dump($FabricDispatchNo);
//var_dump($FabricDispatchYear);
//var_dump($SALES_ORDER_ID);

    global $db;

$sql = " SELECT DISTINCT
SUM(ware_fabricdispatchdetails.dblGoodQty) AS gqty
FROM
	ware_fabricdispatchdetails
INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo
AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
WHERE  ware_fabricdispatchheader.intBulkDispatchNo = '$FabricDispatchNo' AND ware_fabricdispatchheader.intBulkDispatchNoYear = '$FabricDispatchYear'
AND ware_fabricdispatchdetails.intSalesOrderId = '$SALES_ORDER_ID'";

    $result = $db->RunQuery($sql);
    $row=mysqli_fetch_array($result);
    $qty =$row['gqty'];
    return $qty;
}
function getSalesOrderNo($ORDER_NO,$ORDER_YEAR,$SALES_ORDER_ID){

    global $db;
    $sql = "
            SELECT
	trn_orderdetails.intOrderNo,
	trn_orderdetails.intOrderYear,
	trn_orderdetails.strSalesOrderNo,
	trn_orderdetails.intSalesOrderId
FROM
	trn_orderdetails
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
WHERE
trn_orderdetails.intOrderNo = '$ORDER_NO' AND 
	trn_orderdetails.intOrderYear = '$ORDER_YEAR' AND
trn_orderdetails.intSalesOrderId = '$SALES_ORDER_ID'";
    $result = $db->RunQuery($sql);
    $row=mysqli_fetch_array($result);
    $strSalesOrderNo=$row['strSalesOrderNo'];
    return $strSalesOrderNo;
}

function getLineNo($ORDER_NO,$ORDER_YEAR,$SALES_ORDER_ID){
    global $db;
    $sql = "
            SELECT
	trn_orderdetails.intOrderNo,
	trn_orderdetails.intOrderYear,
	trn_orderdetails.strSalesOrderNo,
	trn_orderdetails.intSalesOrderId,
	trn_orderdetails.strLineNo
FROM
	trn_orderdetails
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
WHERE
trn_orderdetails.intOrderNo = '$ORDER_NO' AND 
	trn_orderdetails.intOrderYear = '$ORDER_YEAR' AND
trn_orderdetails.intSalesOrderId = '$SALES_ORDER_ID'";

    $result = $db->RunQuery($sql);
    $row=mysqli_fetch_array($result);
    $strLineNo=$row['strLineNo'];
    return $strLineNo;
}
?>
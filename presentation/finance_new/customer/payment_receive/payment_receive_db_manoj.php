<?php
session_start();
ini_set('max_execution_time',600000);

//BEGIN - INCLUDE FILES {
include  "../../../../dataAccess/Connector.php";
include  "../../../../class/finance/customer/payment_receive/cls_payment_receive_get.php";
include  "../../../../class/finance/customer/payment_receive/cls_payment_receive_set.php";
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {
$obj_payment_receive_get	= new Cls_Payment_Receive_Get($db);
$obj_payment_receive_set	= new Cls_Payment_Receive_Set($db);
//END 	- CREATE OBJECTS }

$requestType		= $_REQUEST["RequestType"];
if($requestType=="URLGetCustomer")
{
	$ledgerId		= $_REQUEST["LedgerId"];
	
	echo $obj_payment_receive_get->GetCustomer($ledgerId);
}
elseif($requestType=="URLLoadCustomerChange")
{
	$customerId		= $_REQUEST["CustomerId"];
	
	echo $obj_payment_receive_get->LoadCustomerChange($customerId);
}
elseif($requestType=="URLLoadMainDetails")
{
	$customerId		= $_REQUEST["CustomerId"];
	$currencyId  	= $_REQUEST["CurrencyId"];
	
	echo $obj_payment_receive_get->LoadMainDetails($customerId,$currencyId);
}
elseif($requestType=="URLSave")
{
	$customerId		= $_REQUEST["CustomerId"];
	$currencyId  	= $_REQUEST["CurrencyId"];
	$remarks		= $_REQUEST["Remarks"];
	$receiptDate	= $_REQUEST["ReceiptDate"];
	$paymentMode	= $_REQUEST["PaymentMode"];
	$detailArray	= json_decode($_REQUEST["DetailArray"],true);
	$glArray		= json_decode($_REQUEST["GLArray"],true);
	echo $obj_payment_receive_set->Save($customerId,$currencyId,$remarks,$detailArray,$glArray,$receiptDate,$paymentMode);
}
elseif($requestType=="URLWhenChangeGL")
{
	$GLID			= $_REQUEST["GLID"];
	echo $obj_payment_receive_get->WhenChangeGL($GLID);
}
elseif($requestType=="URLCancel")
{
	$receiptNo			= $_REQUEST["ReceiptNo"];
	$receiptYear		= $_REQUEST["ReceiptYear"];
	
	$response		= $obj_payment_receive_get->ValidateBeforeCancel($receiptNo,$receiptYear);
	
	if($response["type"]=='false')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_payment_receive_set->Cancel($receiptNo,$receiptYear);
}
elseif($requestType=="confirmPayment")
{
	$savedStatus	= true;
	$savedMsg		= '';
	$errorSql		= '';
	
	$arr = json_decode($_REQUEST['confirmDetails'], true);
	
	$db->begin();
	
	foreach($arr as $arrVal)
	{
		$receiptNo 	   = $arrVal['receiptNo'];
		$receiptYear   = $arrVal['receiptYear'];
		
		$dataArr	= $obj_payment_receive_set->confirmPaymnet($receiptNo,$receiptYear);	
		if($dataArr['savedStatus']=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMsg 		= $dataArr['savedMassege'];
			$errorSql 		= $dataArr['error_sql'];
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=="clearData"){
	//$customerId,$currencyId,$remarks,$detailArray,$glArray,$receiptDate,$paymentMode
	$lastDate			= $_REQUEST["lastDate"];
	 $customerId		= $_REQUEST["customerID"];
	 $bankID			= $_REQUEST["bankID"];
	 $paymentMode		= $_REQUEST["paymentMode"];
	 $currencyId  	= 1;
	 $remarks		= '';
	 $receiptDate	= date('Y-m-d');
	 
	 //$customerId		= $_REQUEST["CustomerId"];
	//$currencyId  	= $_REQUEST["CurrencyId"];
	
	
	$values = LoadMainDetails($customerId,$lastDate,$db);
	$detailArray =array();
	$costCenter = '';
	$totVal = '';
	while($row = mysqli_fetch_array($values))
		{
			$invoiceNO = explode('-',$row['INVOICE_NO']);
			$data['OrderNo'] = $row['ORDER_YEAR'].'/'. $row['ORDER_NO'];
			$data['InvoiceNo'] = $invoiceNO[1].'/'. $invoiceNO[0];
			$data['ToBeReceive'] = $row['VALUE'];
			$data['PayAmount'] = $row['VALUE'];
			$data['Type'] = "PAYRECEIVE";
			$totVal += $row['VALUE'];
			$detailArray[] = $data;
			if($costCenter == ''){
			$costCenter = getCostCenter($row['SERIAL_NO'],$row['SERIAL_YEAR'],$db);
			}
		}

	$glArray = array();
	//DebitArray
	 $gldata['GLAccount'] = $bankID;
	 $gldata['GLValue_DR'] = $totVal;
	 $gldata['GLValue_CR'] = 0;
	 $gldata['GLRemarks'] = '';
	 $gldata['GLCostCenter'] = $costCenter;
	 $gldata['TransType'] = 'D';
	  $glArray[] = $gldata;
	 $glAccountCustomer = getCustomerGlAccount($customerId,$db);
	 //Credit Array
	 $gldata['GLAccount'] = $glAccountCustomer;
	 $gldata['GLValue_DR'] = 0;
	 $gldata['GLValue_CR'] = $totVal;
	 $gldata['GLRemarks'] = '';
	 $gldata['GLCostCenter'] = $costCenter;
	 $gldata['TransType'] = 'C';
	 
	 $glArray[] = $gldata;
	 
	// var_dump($customerId,$currencyId,$remarks,$detailArray,$glArray,$receiptDate,$paymentMode);die;
	// $paymentMode	= $_REQUEST["PaymentMode"];
	// $detailArray	= json_decode($_REQUEST["DetailArray"],true);
	// $glArray		= json_decode($_REQUEST["GLArray"],true);
	
	 echo $obj_payment_receive_set->Save($customerId,$currencyId,$remarks,$detailArray,$glArray,$receiptDate,$paymentMode);
}

function LoadMainDetails($customerId,$lastDate,$db){
	$sql = "(SELECT
		 		  'PAYRECEIVE'											AS TYPE,
				  CIH.SERIAL_NO											AS SERIAL_NO,
				  CIH.SERIAL_YEAR										AS SERIAL_YEAR,
				  CIH.INVOICE_NO										AS INVOICE_NO,
				  CIH.ORDER_NO											AS ORDER_NO,
				  CIH.ORDER_YEAR										AS ORDER_YEAR,
				  CIH.INVOICED_DATE							    AS INVOICED_DATE,
				  CIH.REMARKS											AS INVOICE_REMARKS,
				  ROUND(SUM(CT.VALUE),2)								AS VALUE,
				  
				  ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'INVOICE'),0),2) 		AS INVOICED_VALUE,

				  ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'CREDIT'),0),2) 			AS CREDIT_VALUE,
					
					ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'DEBIT'),0),2) 			AS DEBIT_VALUE,
					
				  ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'),0),2) 		AS ADVANCE_VALUE,
					
				ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR IS NULL 
					AND SUB_CT.INVOICE_NO IS NULL 
					AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'),0),2) 		AS UNSETTLE_ADVANCE_VALUE,
					
				ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2) 		AS RECEIVED_VALUE,
					
				ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'INV_SETTLEMENT'),0),2) 	AS INV_SETTLEMENT
					
				FROM finance_customer_transaction CT
				INNER JOIN finance_customer_invoice_header CIH 
					ON CIH.SERIAL_YEAR = CT.INVOICE_YEAR 
					AND CIH.SERIAL_NO = CT.INVOICE_NO
				WHERE CT.CUSTOMER_ID 	= '$customerId'
					AND CT.CURRENCY_ID 	= 1
					AND INVOICED_DATE <  '$lastDate'
				GROUP BY
				 	CT.CUSTOMER_ID,
					CT.CURRENCY_ID,
					CIH.INVOICE_NO
				HAVING VALUE > 0)
			";
			$result = $db->RunQuery($sql);
			return $result;
}
	function getCostCenter($serialNo,$serialYear,$db){
	
		$sql = "SELECT COST_CENTER FROM
		 		  finance_customer_invoice_details CID
				WHERE CID.SERIAL_NO 	= '$serialNo'
					AND CID.SERIAL_YEAR 	= '$serialYear' LIMIT 1							
			";
			$result = $db->RunQuery($sql);
			$row	= mysqli_fetch_array($result);	
		return $row["COST_CENTER"];
	
	}
	function getCustomerGlAccount($customerId,$db){
		$sql 	= "SELECT
					  FCOA.CHART_OF_ACCOUNT_ID,
					  CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE) AS ACCOUNT_CODE,
					  FCOA.CHART_OF_ACCOUNT_NAME
					FROM finance_mst_chartofaccount FCOA
					INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
					INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
					INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
					WHERE FCOA.CATEGORY_TYPE = 'C'
						AND FCOA.CATEGORY_ID = '$customerId'";
		$result = $db->RunQuery($sql);
        $row	= mysqli_fetch_array($result);	
		return $row["CHART_OF_ACCOUNT_ID"];		
	}
?>
<?php
session_start();
ini_set('display_errors',0);
//BEGIN - SESSION {
$session_companyId	= $_SESSION["CompanyID"];
//END	- SESSION }

//BEGIN - INCLUDE FILES {
$backwardseperator 	= "../../../../";
include_once ("{$backwardseperator}dataAccess/Connector.php");
include_once ("../../../../libraries/fpdf/fpdf.php");
include_once ("../../../../class/masterData/companies/cls_companies_get.php");
include_once ("../../../../class/finance/customer/payment_receive/cls_payment_receive_get.php");
include_once ("../../../../class/finance/cls_convert_amount_to_word.php");
include_once ("../../../../class/finance/cls_common_get.php");
//END	- INCLUDE FILES }

//BEGIN	- CREATE OBJECTS {
$obj_company_get			= new cls_companies_get($db);
$obj_payment_receive_get	= new Cls_Payment_Receive_Get($db);
$obj_convert_amount_to_word	= new Cls_Convert_Amount_To_Word($db);
$obj_fin_com				= new Cls_Common_Get($db);
//END	- CREATE OBJECTS }

//BEGIN - PARAMETERS {

$serialNo		= $_REQUEST["SerialNo"];
$serialYear		= $_REQUEST["SerialYear"];

$title			= "PAYMENT RECEIPT";
$font			= 'Times';

//END	- PARAMETERS }
class PDF extends FPDF
{
	function Header1()
	{
		global $result_reportHeader;
		global $title;
		global $session_companyId;
		global $font;
		$h = 5;
		
		$this->Image('../../../../images/screenline.jpg',8,9);
		$this->SetFont($font,'B',14);$this->Cell(0,$h,$result_reportHeader["COMPANY_NAME"],'0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,$result_reportHeader["LOCATION_ADDRESS"].' '.$result_reportHeader["LOCATION_STREET"].' '.$result_reportHeader["LOCATION_CITY"].' '.$result_reportHeader["COMPANY_COUNTRY"].'.','0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,"Tel : ".$result_reportHeader["LOCATION_PHONE"].'  '."Fax : ".$result_reportHeader["LOCATION_FAX"],'0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,"E-mail : ".$result_reportHeader["LOCATION_EMAIL"].'  '."Web : ".$result_reportHeader["LOCATION_WEB"],'0',1,'C');
		$this->SetFont($font,'B',12);$this->Cell(0,10,$title,'0',1,'C');		
	}
	
	function SetPageWaterMark($status,$printStatus)
	{
		//Put the watermark
		switch($status)
		{
			case 1:
				$msg	= ($printStatus==1?"DUPLICATE":"");
				break;
		}
		$this->SetFont('Times','B',50);
		$this->SetTextColor(255,192,203);
		$this->RotatedText(35,190,$msg,45);
		$this->SetTextColor(0,0,0);
	}
	
	function RotatedText($x, $y, $txt, $angle)
	{
		//Text rotated around its origin
		$this->Rotate($angle,$x,$y);
		$this->Text($x,$y,$txt);
		$this->Rotate(0);
	}
		
	function ReportHeader($result_header,$result_reportHeader,$receiptNo)
	{
		$h = 5;
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Receipt No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$receiptNo,'0',1,'L');
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Receipt Date','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["RECEIPT_DATE"],'0',1,'L');
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Payment Method','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["PAYMENT_MODE"],'0',1,'L');
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Currency','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["CURRENCY_CODE"],'0',1,'L');
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Customer Information','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["CUSTOMER_NAME"],'0',1,'L');
		$this->SetX(52);
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["ADDRESS"],'0',1,'L');
		$this->SetX(52);
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["CITY"],'0',1,'L');
		$this->SetX(52);
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["COUNTRY_NAME"].'.','0',1,'L');		
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Remarks','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->MultiCell(0,5,$result_header["REMARKS"],'0','',0);
	}
	
	function CreateTable($result_header,$result_details,$result_damage,$obj_convert_amount_to_word)
	{
		$count 			= 1;
		
		$this->Table_header();
		while($row = mysqli_fetch_array($result_details))
		{	
			$this->Table_Body($row);	
			if($count == 8)
			{
				$this->AddPage();
				$this->Table_header();
				$count = 0;	
			}			
			$count++;			
		}
		$this->Table_Body_Validate($count);	
		$this->Table_footer($result_header,$result_damage,$obj_convert_amount_to_word);
	}
	
	function Table_header()
	{
		$this->SetXY($this->GetX(),$this->GetY()+2);
		$h = 10;
		$this->SetFont('Times','B',10);
		$this->Cell(50,$h,'Item Name','1',0,'C');
		$this->Cell(120,$h,'Item Description','1',0,'C');
		$this->Cell(30,$h,'Total Value','1',0,'R');
		$this->Ln();		
	}
		
	function Table_Body($row)
	{	
		$h	= 4;
		$this->SetFont('Times','',8);
		$this->Cell(50,$h,$row["ITEM_NAME"],'LR',0,'C');
		$this->Cell(120,$h,"PO No : ".$row["CUSTOMER_PO_NO"],'LR',0,'L');
		$this->Cell(30,$h,number_format($row["PAY_AMOUNT"],2),'LR',0,'R');
		$this->Ln();

		$this->SetFont('Times','',8);
		$this->Cell(50,$h,'','LR',0,'C');
		$this->Cell(120,$h,"Style No : ".$row["STYLE_NO"],'LR',0,'L');
		$this->Cell(30,$h,'','LR',0,'R');
		$this->Ln();

		$this->SetFont('Times','',8);
		$this->Cell(50,$h,'','LR',0,'C');
		$this->Cell(120,$h,"Graphic No : ".$row["GRAPHIC_NO"],'LR',0,'L');
		$this->Cell(30,$h,'','LR',0,'R');
		$this->Ln();

		$this->SetFont('Times','',8);
		$this->Cell(50,$h,'','LR',0,'C');
		$this->Cell(120,$h,"Sales Order No : ".$row["SALES_ORDER_NO"],'LR',0,'L');
		$this->Cell(30,$h,'','LR',0,'R');
		$this->Ln();
		
		$this->SetFont('Times','',8);
		$this->Cell(50,$h,'','LBR',0,'C');
		$this->Cell(120,$h,"Invoice No : ".$row["INVOICE_NO"],'LBR',0,'L');
		$this->Cell(30,$h,'','LBR',0,'R');
		$this->Ln();
	}
	
	function Table_Body_Validate($count)
	{
		$h1	= 0;
		for($i=$count;$i<=3;$i++)
		{
			$h1 += 16;
		}
		$this->SetFont('Times','',7);
		$this->Cell(170,$h1,"",'1',0,'C');
		$this->Cell(30,$h1,"",'1',0,'C');
		$this->Ln();
	}
	
	function Table_footer($result_header,$result_damage,$obj_convert_amount_to_word)
	{
		$this->SetY($this->GetY());
		$totDamage = 0;
		while($row = mysqli_fetch_array($result_damage))
		{
			$this->SetFont('Times','',10);
			$this->Cell(4,10,"",'LBT',0,'L');
			$this->Cell(166,10,$row["CHART_OF_ACCOUNT_NAME"],'TB',0,'L');
			$this->Cell(30,10,"(".$row["PAY_AMOUNT"].")",'1',0,'R');
			$this->Ln();
			$totDamage+=$row["PAY_AMOUNT"];
		}
		
		$this->SetFont('Times','B',10);$this->Cell(26,10,"Through",'LBT',0,'L');
		$this->Cell(4,10,":",'TB',0,'L');
		$this->SetFont('Times','',10);$this->Cell(140,10,"",'BTR',0,'C');
		$this->Cell(30,10,"",'1',0,'C');
		$this->Ln();
		
		$this->Cell(4,10,"",'LBT',0,'L');
		$this->Cell(166,10,$result_header["BANK_GL_NAME"],'TB',0,'L');
		$this->Cell(30,10,"",'1',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','B',10);$this->Cell(26,10,"Amount(In Words)",'LBT',0,'L');
		$this->Cell(4,10,":",'TB',0,'L');
		$this->SetFont('Times','',10);$this->Cell(140,10,"",'BTR',0,'C');
		$this->Cell(30,10,"",'1',0,'C');
		$this->Ln();
		
		$this->Cell(4,10,"",'LBT',0,'L');
		$this->SetFont('Times','',8);
		$this->Cell(166,10,$obj_convert_amount_to_word->Convert_Amount(($result_header["VALUE"]-$totDamage),$result_header["CURRENCY_CODE"]),'0',0,'L');
		$this->Cell(30,10,"",'1',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','B',10);
		$this->Cell(170,10,"Total",'1',0,'R');
		$this->Cell(30,10,number_format(($result_header["VALUE"]-$totDamage),2),'1',0,'R');
		$this->Ln();
		
	}	
	function Footer1()
	{
		global $session_companyId;
		global $result_header;	
			
		$this->SetFont('Times','',8);
		
		$this->SetXY(20,$this->GetY()+10);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetX(140);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetXY(20,$this->GetY()+3);
		$this->Cell(50,5,'Authorized By',0,0,'C');
		
		$this->SetX(140);		
		$this->Cell(50,5,'Checked By',0,0,'C');	
		
		$this->SetXY(20,$this->GetY()+10);
		$this->Cell(50,5,$result_header["CREATED_BY_NAME"],0,0,'C');
		
		$this->SetXY(20,$this->GetY()+2);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetX(140);
		$this->Cell(50,5,'..........................................',0,0,'C');

		$this->SetXY(140,$this->GetY()+3);
		$this->Cell(50,5,'Received By',0,0,'C');

		$this->SetX(20);
		$this->Cell(50,5,'Prepared By',0,0,'C');	
	}
	
	function Footer()
	{
		$this->SetY(-5);
		$this->SetFont('Times','I',8);
		$this->Cell(0,2,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf 					= new PDF('P','mm','Letter');

$result_header 			= $obj_payment_receive_get->getPaymentReceiveReportHeader($serialNo,$serialYear);
$receiptNo				= $obj_fin_com->getSerialNo($result_header["SERIAL_NO"],$result_header["SERIAL_DATE"],$session_companyId,'RunQuery');
$result_details			= $obj_payment_receive_get->getPaymentReceiveReportDetails($serialNo,$serialYear);
$result_reportHeader	= $obj_company_get->GetCompanyReportHeader($result_header["LOCATION_ID"]);
$result_damage			= $obj_payment_receive_get->getPaymentReceiveReportDamage($serialNo,$serialYear);

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetPageWaterMark($result_header["STATUS"],$result_header["PRINT_STATUS"]);
$pdf->Header1();
$pdf->ReportHeader($result_header,$result_reportHeader,$receiptNo);
$pdf->CreateTable($result_header,$result_details,$result_damage,$obj_convert_amount_to_word);
$pdf->Footer1();
$obj_payment_receive_get->UpdateStatus($serialNo,$serialYear);
$pdf->Output('rpt_payment_receive_pdf.pdf','I');
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$session_locationId	 	= $_SESSION["CompanyID"];
$session_companyId 		= $_SESSION["headCompanyId"];

include_once ("class/finance/customer/cls_common_get.php");
include_once ("class/finance/cls_get_gldetails.php");
include_once ("class/cls_commonFunctions_get.php");
//include 	  "include/javascript.html";

$obj_common_get			= new Cls_Common_Get($db);
$obj_GLDetails_get		= new Cls_Get_GLDetails($db);
$obj_common_function_get= new cls_commonFunctions_get($db);

$serialNo				= (!isset($_REQUEST["SerialNo"])?'':$_REQUEST["SerialNo"]);
$serialYear				= (!isset($_REQUEST["SerialYear"])?'':$_REQUEST["SerialYear"]);
$customerId				= (!isset($_REQUEST["CustomerId"])?'':$_REQUEST["CustomerId"]);
$currencyId				= (!isset($_REQUEST["CurrencyId"])?'':$_REQUEST["CurrencyId"]);
$programCode			= 'P0706';

$header_array 			= GetHeaderDetails($serialYear,$serialNo);
$detail_result 			= GetGridDetails($serialYear,$serialNo);
$gl_result 				= GetGLGridDetails($serialYear,$serialNo);

if($obj_common_function_get->ValidateSpecialPermission('26',$_SESSION["userId"],'RunQuery'))
	$dateDisabled		= "";
else
	$dateDisabled		= "disabled='disabled'";	
	
if(!isset($_REQUEST["SerialNo"]))
	$invoiceDate		= date("Y-m-d");
else
	$invoiceDate		= $header_array["RECEIPT_DATE"];
	
$cls_display_butCancel	= 'maskHide'; //maskShow/maskHide
?>
<title>Payment Receive</title>

<!--<script type="text/javascript" src="presentation/finance_new/customer/payment_receive/payment_receive.js"></script>-->
<script>
	var customerIdAL	='<?php echo $customerId; ?>';
	var currencyIdAL	='<?php echo $currencyId; ?>';
</script>



<?php //if($_SESSION["userId"] == 2){ ?>
<!-- Customer Payment Clearing part Temperary by krishantha -->
<form id="clearingGLAccount" method="post">
<span>Customer ID :</span><input type="text" name="customerID" id="customerID" value="181">
<span>Last Date :</span><input type="text" name="lastDate" id="lastDate" value="2015-09-30">
<span>Bank ID :</span><input type="text" name="bankID" id="bankID" value="167">
<span>Payment Mode :</span><input type="text" name="paymentMode" id="paymentMode" value="3">
<button type="button" name="submit" id="clearSubmit">Submit</button>
</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>
//$(document).ready(function(){
$('#clearingGLAccount #clearSubmit').live('click',clear);
//});
function clear(){
	 //e.preventDefault();
	var basePath		= "presentation/finance_new/customer/payment_receive/";
	var url 	= basePath+"payment_receive_db_manoj.php?RequestType=clearData";
	var data 	= "lastDate="+$('#clearingGLAccount #lastDate').val();
		data   += "&customerID="+$('#clearingGLAccount #customerID').val();
		data   += "&bankID="+$('#clearingGLAccount #bankID').val();
		data   += "&paymentMode="+$('#clearingGLAccount #paymentMode').val();
	
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type: "POST",
	async:false,
	success:function(json)
		{
			//alert('done');
			
		}
	
	});
}
	
	
	

</script>
<!-- End of Customer Clearing Part -->

<?php //} ?>


<form id="frmPaymentReceive" name="frmPaymentReceive" method="post">
  <div align="center">
    <div class="trans_layoutS" style="width:1100px">
      <div class="trans_text">Payment Receive</div>
      <table width="1100">
        <tr>
          <td><table width="100%%" border="0" class="normalfnt">
              <tr>
                <td width="16%"> Receipt Number</td>
                <td width="36%"><input type="text" name="txtReceiptNo" id="txtReceiptNo" style="width:100px" disabled="disabled" value="<?php echo $header_array["RECEIPT_NO"]?>"/>
                  <input type="text" name="txtReceiptYear" id="txtReceiptYear" style="width:50px" disabled="disabled" value="<?php echo $header_array["RECEIPT_YEAR"]?>"/></td>
                <td width="18%">&nbsp;</td>
                <td width="30%">&nbsp;</td>
              </tr>
              <tr>
                <td>Customer <span class="compulsoryRed">*</span></td>
                <td><select name="cboCustomer" id="cboCustomer" style="width:280px;height:20px" class="cls_load_details validate[required]">
                  <?php 		
		if(!isset($_REQUEST["SerialNo"]))
		{			
			$sql = "SELECT DISTINCT
					  CU.intId   AS CUSTOMER_ID,
					  CU.strName AS CUSTOMER_NAME
					FROM mst_customer CU
					  INNER JOIN finance_customer_transaction FCT
						ON FCT.CUSTOMER_ID = CU.intId
					WHERE FCT.COMPANY_ID = '$session_companyId'
					GROUP BY FCT.INVOICE_NO,FCT.INVOICE_YEAR,FCT.CUSTOMER_ID,FCT.CURRENCY_ID
					HAVING ROUND(SUM(FCT.VALUE),2)>0
					ORDER BY strName";
			$result = $db->RunQuery($sql);
				echo "<option value=\"".""."\">&nbsp;</option>";
			while($row = mysqli_fetch_array($result))
			{
				echo "<option value=\"".$row["CUSTOMER_ID"]."\">".$row["CUSTOMER_NAME"]."</option>";
			}
		}
		else
		{
			echo $header_array["CUSTOMER_HTML"];
		}
?>
                </select></td>
                <td>Date</td>
                <td><input name="txtReceiptDate" type="text" value="<?php echo $invoiceDate ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtReceiptDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo $dateDisabled;?> /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
              <tr>
                <td>Currency <span class="compulsoryRed">*</span></td>
                <td><select name="cboCurrency" id="cboCurrency" style="width:280px" class="cls_load_details validate[required] cls_cbo_CurrencyCalExchangeRate">
                  <?php
	if(!isset($_REQUEST["SerialNo"]))
	{
		echo "<option value=\"".""."\">&nbsp;</option>";
		$sql = "SELECT DISTINCT
				  CU.intId   AS CURRENCY_ID,
				  CU.strCode AS CURRENCY_NAME
				FROM mst_financecurrency CU
				  INNER JOIN finance_customer_transaction FCT
					ON FCT.CURRENCY_ID = CU.intId
				WHERE FCT.COMPANY_ID = '$session_companyId'
				GROUP BY FCT.INVOICE_NO,FCT.INVOICE_YEAR
				HAVING ROUND(SUM(FCT.VALUE),2) > 0
				ORDER BY CU.strCode";
		$result = $db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			if($row["CURRENCY_ID"]==$header_array["CURRENCY_ID"])
				echo "<option value=\"".$row["CURRENCY_ID"]."\" selected=\"selected\">".$row["CURRENCY_NAME"]."</option>";
			else
				echo "<option value=\"".$row["CURRENCY_ID"]."\">".$row["CURRENCY_NAME"]."</option>";
		}
	}
	else
	{
		echo $header_array["CURRENCY_HTML"];
	}
?>
                </select>
                <input type="text" name="txtExchangeRate" id="txtExchangeRate" style="width:60px;text-align:right" disabled="disabled" value="0.00" title="Exchange Rate" class="cls_txt_exchangeRate validate[required]"/></td>
                <td>Payment Method <span class="compulsoryRed">*</span></td>
                <td><select name="cboPaymentsMethods" id="cboPaymentsMethods" style="width:280px" class="validate[required]">
                    <option value=""></option>
                    <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_financepaymentsmethods
						WHERE
							intStatus = 1
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($header_array["PAYMENT_MODE"]==$row['intId'])
								echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
							else
								echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
                  </select></td>
              </tr>
              <tr>
                <td>Remarks</td>
                <td><textarea name="txtRemarks" id="txtRemarks2" style="width:280px;height:50px" ><?php echo $header_array["REMARKS"]?></textarea></td>
                <td valign="top"><b>Unsettle Advance</b></td>
                <td class="normalfnt" valign="top"><input type="text" style="width:98px;text-align:right" name="txtUnsettleAdvance" id="txtUnsettleAdvance" disabled="disabled"/><a class="button white small" id="hyper_settle">Settle</a></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td ><div style="overflow:scroll;width:1100px;height:250px;">
              <table width="100%%" border="0" class="bordered" id="tblMain">
                <thead>
                  <tr>
                    <th width="3%"><input type="checkbox" class="cls_th_checkAll"></th>
                    <th width="12%">Invoice No</th>
                    <th width="21%">Invoice Remarks</th>
                    <th width="9%">Invoiced Date</th>
                    <th width="8%">Invoiced </th>
                    <th width="8%">Credit Note</th>
                    <th width="8%">Debit Note</th>
                    <th width="7%">Advanced</th>
                    <th width="7%">Received </th>
                    <th width="8%">To Be Receive</th>
                    <th width="9%">Receiving Amount <span class="compulsoryRed">*</span></th>
                  </tr>
                </thead>
                <tbody>
                  <?php
$totRecieveAmount	= 0;
while($row = mysqli_fetch_array($detail_result))
{ 
	$toBeReceived		= round((abs($row["INVOICE_VALUE"]) + abs($row["DEBIT_VALUE"])) - (abs($row["CREDIT_VALUE"]) + abs($row["ADVANCE_VALUE"]) + abs($row["RECEIVED_VALUE"])),2);
	$totRecieveAmount+=$row["RECEIVED_AMOUNT"] ;
?>
                  <tr>
                    <td class="cls_td_check" style="text-align:center"><input type="checkbox" checked="checked" disabled="disabled" class="cls_check"></td>
                    <td class="cls_td_invoiceNo"><?php echo $row["INVOICE_NO"]?></td>
                    <td class="cls_td_invoiceRemarks"><?php echo ($row["INVOICE_REMARKS"]==''?'&nbsp;':$row["INVOICE_REMARKS"])?></td>
                    <td class="cls_td_invoicedDate"><?php echo $row["INVOICED_DATE"]?></td>
                    <td class="cls_td_invoicedAmount" style="text-align:right"><?php echo $row["INVOICE_VALUE"]?></td>
                    <td class="cls_td_toBePaid" style="text-align:right"><?php echo abs($row["CREDIT_VALUE"])?></td>
                    <td class="cls_td_toBePaid" style="text-align:right"><?php echo abs($row["DEBIT_VALUE"])?></td>
                    <td class="cls_td_toBePaid" style="text-align:right"><?php echo $row["ADVANCE_VALUE"]?></td>
                    <td class="cls_td_toBePaid" style="text-align:right"><?php echo abs($row["RECEIVED_VALUE"])?></td>
                    <td class="cls_td_toBePaid" style="text-align:right"><?php echo $toBeReceived?></td>
                    <td class="cls_td_payingAmount" style="text-align:right"><?php echo $row["RECEIVED_AMOUNT"]?></td>
                  </tr>
                  <?php
}
?>
                </tbody>
              </table>
          </div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td ><table width="100%%" border="0" class="bordered" id="tblGL">
              <thead>
                <tr>
                  <th colspan="6">GL Allocation
                    <div style="float:right"><a class="button white small" id="cls_addNewGL">Add New GL</a></div></th>
                </tr>
                <tr>
                  <th width="2%">&nbsp;</th>
                  <th width="28%">GL Account <span class="compulsoryRed">*</span></th>
                  <th width="10%">Debit <span class="compulsoryRed">*</span></th>
                  <th width="10%">Credit<span class="compulsoryRed"> *</span></th>
                  <th width="34%">Remarks</th>
                  <th width="16%">Cost Center <span class="compulsoryRed">*</span></th>
                </tr>
              </thead>
              <tbody>
                <?php 
 $totDebitAmount	= 0;
 $totCeditAmount	= 0;
 while($row = mysqli_fetch_array($gl_result))
 {
	 $booAvailable	 = true;
	 $totDebitAmount+= $row["DEBIT_AMOUNT"];
	 $totCeditAmount+= $row["CREDIT_AMOUNT"];
 ?>
                <tr class="cls_tr_firstRow">
                  <td class="cls_td_check" style="text-align:center"><img src="images/del.png" class="removeRow mouseover"/></td>
                  <td class="cls_td_GL" title="D"><select id="cboGL" style="width:100%" class="validate[required]" >
                      <?php echo $obj_GLDetails_get->getGLCombo('CUSTOMER_PAYMENT_RECEIVE',$row["GL_ID"]);?>
                    </select></td>
                  <td class="cls_td_GLAmount cls_td_glAmount_dr"><input id="txtGLAmount2" type="text" style="width:100%;text-align:right" class="validate[required,custom[number]] cls_GLAmount cls_cbo_numberOnly" value="<?php echo number_format($row["DEBIT_AMOUNT"],2,'.',''); ?>"/></td>
                  <td class="cls_td_GLAmount cls_td_glAmount_cr"><input id="txtGLAmount" type="text" style="width:100%;text-align:right" class="validate[required,custom[number]] cls_GLAmount cls_cbo_numberOnly" value="<?php echo number_format($row["CREDIT_AMOUNT"],2,'.',''); ?>"/></td>
                  <td class="cls_td_remarks"><textarea id="txtRemarks" style="width:100%;height:20px" ><?php echo $row['REMARKS']; ?></textarea></td>
                  <td class="cls_td_costCenter"><select id="cboCostCenter" style="width:100%;"  class="validate[required]" >
                      <?php echo GetCostCenter($row["COST_CENTER_ID"]);?>
                    </select></td>
                </tr>
                <?php 
 }
?>
                <?php
if(!$booAvailable){
?>
                <tr class="cls_tr_firstRow">
                  <td class="cls_td_check" style="text-align:center"><img src="images/del.png" class="removeRow mouseover"/></td>
                  <td class="cls_td_GL" title="D"><select id="cboGL" style="width:100%" class="validate[required]" >
                      <?php echo $obj_GLDetails_get->getGLCombo('CUSTOMER_PAYMENT_RECEIVE',$row["GL_ID"]);?>
                    </select></td>
                  <td class="cls_td_GLAmount cls_td_glAmount_dr"><input id="txtGLAmount3" type="text" style="width:100%;text-align:right" class="validate[required,custom[number]] cls_GLAmount cls_cbo_numberOnly" value="<?php echo ($row['GL_AMOUNT']==''?0:round($row['GL_AMOUNT'],2))?>"/></td>
                  <td class="cls_td_GLAmount cls_td_glAmount_cr"><input id="txtGLAmount" type="text" style="width:100%;text-align:right" class="validate[required,custom[number]] cls_GLAmount cls_cbo_numberOnly" value="<?php echo ($row['GL_AMOUNT']==''?0:round($row['GL_AMOUNT'],2))?>"/></td>
                  <td class="cls_td_remarks"><textarea id="txtRemarks" style="width:100%;height:20px" ><?php echo $row['REMARKS']; ?></textarea></td>
                  <td class="cls_td_costCenter"><select id="cboCostCenter" style="width:100%;"  class="validate[required]" >
                      <?php echo GetCostCenter($row["COST_CENTER_ID"]);?>
                    </select></td>
                </tr>
                <?php
}
?>
              </tbody>
              <tfoot>
              <tr style="font-weight:bold">
                  <td>&nbsp;</td>
                  <td style="text-align:center">TOTAL</td>
                  <td class="cls_td_totAmount_dr" style="text-align:right"><?php echo number_format($totDebitAmount,2,'.',''); ?></td>
                  <td class="cls_td_totAmount_cr" style="text-align:right"><?php echo number_format($totCeditAmount,2,'.',''); ?></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </tfoot>
            </table></td>
        </tr>
        <tr>
          <td><table width="301" border="0" align="right" class="normalfnt" style="font-weight:bold">
              <tr>
                <td width="166">Total Paying Amount</td>
                <td width="11" style="text-align:center"><b>:</b></td>
                <td width="91" style="text-align:right;font-weight:bold" id="td_totalPayingAmount"><?php echo number_format($totRecieveAmount,2,'.','');?></td>
              </tr>
              <tr>
                <td>Total GL Allocated Amount</td>
                <td style="text-align:center"><b>:</b></td>
                <td style="text-align:right;font-weight:bold" id="td_totalGLAlloAmount"><?php echo number_format($totDebitAmount,2,'.',''); ?></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="center"><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($form_permision['edit']==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');?><a class="button white medium <?php echo $cls_display_butCancel?>" style="display:none" id="butCancel">Cancel</a><a class="button white medium" id="butReport" name="butReport">&nbsp;Report&nbsp;</a><a href="main.php" class="button white medium" id="butClose">Close</a></td>
        </tr>
      </table>
    </div>
  </div>
</form>
<?php
function GetHeaderDetails($serialYear,$serialNo)
{
	global $db;
				
	$sql = "SELECT 
				CPRH.RECEIPT_NO 		AS RECEIPT_NO,
				CPRH.RECEIPT_YEAR 		AS RECEIPT_YEAR,
				CU.intId 				AS CUSTOMER_ID,
				CU.strName 				AS CUSTOMER_NAME,
				CURR.intId				AS CURRENCY_ID,
				CURR.strCode			AS CURRENCY_CODE,
				CPRH.REMARKS			AS REMARKS,
				CPRH.PAYMENT_MODE		AS PAYMENT_MODE,
				CPRH.RECEIPT_DATE		AS RECEIPT_DATE
			FROM finance_customer_pay_receive_header CPRH
			INNER JOIN mst_customer CU ON CU.intId = CPRH.CUSTOMER_ID
			INNER JOIN mst_financecurrency CURR ON CURR.intId = CPRH.CURRENCY_ID
			WHERE CPRH.RECEIPT_NO = '$serialNo' AND CPRH.RECEIPT_YEAR = '$serialYear'";
				
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	$header_array["RECEIPT_NO"]			= $row["RECEIPT_NO"];
	$header_array["RECEIPT_YEAR"]		= $row["RECEIPT_YEAR"];
	$header_array["CUSTOMER_HTML"]		= "<option value=\"".$row["CUSTOMER_ID"]."\">".$row["CUSTOMER_NAME"]."</option>";
	$header_array["CURRENCY_HTML"]		= "<option value=\"".$row["CURRENCY_ID"]."\">".$row["CURRENCY_CODE"]."</option>";
	$header_array["REMARKS"]			= $row["REMARKS"];
	$header_array["PAYMENT_MODE"]		= $row["PAYMENT_MODE"];
	$header_array["RECEIPT_DATE"]		= $row["RECEIPT_DATE"];		
	return $header_array;
}

function GetGridDetails($serialYear,$serialNo)
{
	global $db;
	
	$sql = "SELECT 
				CIH.INVOICE_NO										AS INVOICE_NO,
				CIH.INVOICED_DATE 									AS INVOICED_DATE,
				CIH.REMARKS											AS INVOICE_REMARKS,
				
				ROUND(COALESCE((SELECT SUM(CPRD.PAY_AMOUNT) FROM 
				finance_customer_pay_receive_details SUB_CPRD
				WHERE SUB_CPRD.RECEIPT_NO = CPRD.RECEIPT_NO 
					AND SUB_CPRD.RECEIPT_YEAR = CPRD.RECEIPT_YEAR
					AND SUB_CPRD.INVOICE_NO = CIH.SERIAL_NO
					AND SUB_CPRD.INVOICE_YEAR = CIH.SERIAL_YEAR
				GROUP BY SUB_CPRD.RECEIPT_NO,
					SUB_CPRD.RECEIPT_YEAR,
					SUB_CPRD.INVOICE_NO,
					SUB_CPRD.INVOICE_YEAR),0),2) 							AS RECEIVED_AMOUNT,
				
				ROUND(COALESCE((SELECT SUM(SUB_CID.VALUE) 
				FROM finance_customer_invoice_header SUB_CIH 
				INNER JOIN finance_customer_invoice_details SUB_CID 
				ON SUB_CIH.SERIAL_NO = SUB_CID.SERIAL_NO 
					AND SUB_CIH.SERIAL_YEAR = SUB_CID.SERIAL_YEAR
				WHERE SUB_CIH.SERIAL_NO = CIH.SERIAL_NO
					AND SUB_CIH.SERIAL_YEAR = CIH.SERIAL_YEAR
				GROUP BY SUB_CIH.SERIAL_NO,
					SUB_CIH.SERIAL_YEAR),0),2)							AS INVOICE_VALUE,
				  
				ROUND(COALESCE((SELECT
				SUM(SUB_CT.VALUE)
				FROM finance_customer_transaction SUB_CT 
				WHERE SUB_CT.CUSTOMER_ID = CIH.CUSTOMER_ID
				AND SUB_CT.CURRENCY_ID = CIH.CURRENCY_ID
				AND SUB_CT.INVOICE_YEAR = CIH.SERIAL_YEAR
				AND SUB_CT.INVOICE_NO = CIH.SERIAL_NO
				AND SUB_CT.DOCUMENT_TYPE = 'CREDIT'),0),2) 			AS CREDIT_VALUE,
				
				ROUND(COALESCE((SELECT
				SUM(SUB_CT.VALUE)
				FROM finance_customer_transaction SUB_CT 
				WHERE SUB_CT.CUSTOMER_ID = CIH.CUSTOMER_ID
				AND SUB_CT.CURRENCY_ID = CIH.CURRENCY_ID
				AND SUB_CT.INVOICE_YEAR = CIH.SERIAL_YEAR
				AND SUB_CT.INVOICE_NO = CIH.SERIAL_NO
				AND SUB_CT.DOCUMENT_TYPE = 'DEBIT'),0),2) 			AS DEBIT_VALUE,
				
				ROUND(COALESCE((SELECT
				SUM(SUB_CT.VALUE)
				FROM finance_customer_transaction SUB_CT 
				WHERE SUB_CT.CUSTOMER_ID = CIH.CUSTOMER_ID
				AND SUB_CT.CURRENCY_ID = CIH.CURRENCY_ID
				AND SUB_CT.INVOICE_YEAR = CIH.SERIAL_YEAR
				AND SUB_CT.INVOICE_NO = CIH.SERIAL_NO
				AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'),0),2) 			AS ADVANCE_VALUE,
				
				ROUND(COALESCE((SELECT
				SUM(SUB_CT.VALUE)
				FROM finance_customer_transaction SUB_CT 
				WHERE SUB_CT.CUSTOMER_ID = CIH.CUSTOMER_ID
				AND SUB_CT.CURRENCY_ID = CIH.CURRENCY_ID
				AND SUB_CT.INVOICE_YEAR = CIH.SERIAL_YEAR
				AND SUB_CT.INVOICE_NO = CIH.SERIAL_NO
				AND SUB_CT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2) 		AS RECEIVED_VALUE
				 
			FROM finance_customer_pay_receive_details CPRD
			INNER JOIN finance_customer_invoice_header CIH ON CIH.SERIAL_NO = CPRD.INVOICE_NO AND CIH.SERIAL_YEAR = CPRD.INVOICE_YEAR
			WHERE CPRD.RECEIPT_NO = '$serialNo' AND CPRD.RECEIPT_YEAR = '$serialYear'
			GROUP BY CPRD.RECEIPT_NO,CPRD.RECEIPT_YEAR,CPRD.INVOICE_NO,CPRD.INVOICE_YEAR";
			//die($sql);
	return $db->RunQuery($sql);
}

function GetGLGridDetails($serialYear,$serialNo)
{
	global $db;
	
	$sql = "SELECT 
				CPRGL.LEDGER_ID					AS GL_ID,
				CPRGL.PAY_AMOUNT				AS GL_AMOUNT,
				CPRGL.REMARKS					AS REMARKS,
				IF(CPRGL.TRANSACTION_TYPE='D',CPRGL.PAY_AMOUNT,0) AS DEBIT_AMOUNT,
				IF(CPRGL.TRANSACTION_TYPE='C',CPRGL.PAY_AMOUNT,0) AS CREDIT_AMOUNT,
				CPRGL.COST_CENTER_ID			AS COST_CENTER_ID
			FROM finance_customer_pay_receive_gl CPRGL
			WHERE CPRGL.RECEIPT_NO = '$serialNo' 
				AND CPRGL.RECEIPT_YEAR = '$serialYear'";
	return $db->RunQuery($sql);
}

function GetGL($id)
{
	global $db;
	global $session_companyId;
	
	$sql = "SELECT
			mst_financechartofaccounts.intId,
			mst_financechartofaccounts.strCode,
			mst_financechartofaccounts.strName,
			mst_financechartofaccounts_companies.intCompanyId,
			mst_financechartofaccounts_companies.intChartOfAccountId
			FROM
			mst_financechartofaccounts
			Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
			WHERE
			intStatus = '1' AND  (intFinancialTypeId = '24'  OR  intFinancialTypeId = '23' OR  intFinancialTypeId = '11' OR  intFinancialTypeId = '6') AND strType = 'Posting' AND intCompanyId = '$session_companyId'
			order by strCode";
	$result = $db->RunQuery($sql);
		$string = "<option value=\"".""."\"></option>";
	while($row = mysqli_fetch_array($result))
	{
		if($id == $row['intId'])
			$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."-".$row['strName']."</option>";
		else
			$string .= "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";
	}
	return $string;
}

function GetCostCenter($id)
{
	global $db;
	
	$sql = "SELECT intId,strName FROM mst_financedimension WHERE intStatus = 1 order by strName";
	$result = $db->RunQuery($sql);
		$string = "<option value=\"".""."\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		if($id == $row['intId'])
			$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
		else
			$string .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
	}
	return $string;
}
?>
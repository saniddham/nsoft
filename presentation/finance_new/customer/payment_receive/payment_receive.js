var basePath		= "presentation/finance_new/customer/payment_receive/";
var settleMenuId	= 722;
var reportId		= 930;
var menuId			= 706;
$(document).each(function(index, element) {
	
	$('#frmPaymentReceive #cboCustomer').focus();
	$('#frmPaymentReceive #cboCustomer').die('change').live('change',LoadCustomerChange);
	$('#frmPaymentReceive #tblMain .cls_th_checkAll').die('click').live('click',CheckAll);
	$('#frmPaymentReceive .cls_load_details').die('change').live('change',LoadMainDetails);
	$('#frmPaymentReceive #butSave').die('click').live('click',Save);
	$('#frmPaymentReceive #butNew').die('click').live('click',New);
	$('#frmPaymentReceive #butCancel').die('click').live('click',Cancel);
	$('#frmPaymentReceive #butReport').die('click').live('click',Report);
	$('#frmPaymentReceive #cls_addNewGL').die('click').live('click',AddNewGL);
	$('#frmPaymentReceive #hyper_settle').die('click').live('click',SettleReport);
	$('#frmPaymentReceive #tblGL #cboGL').die('change').live('change',WhenChangeGL);
	$('#butRConfirm').die('click').live('click',confirmPayment);
	
	$('#frmPaymentReceive #tblMain .cls_check').die('click').live('click',function(){
		ValidateRowWhenCheck($(this));
	});
	
	$('#frmPaymentReceive #tblMain .cls_td_payingAmount').die('keyup').live('keyup',function(){ CalculateTotals(); });
	
	$('#frmPaymentReceive #tblGL .cls_GLAmount').die('keyup').live('keyup',function(){ CalculateGLTotals(); });

	$('#frmPaymentReceive #tblGL .removeRow').die('click').live('click',function(){
		
		if($(this).parent().parent().prop('class')=="")
			$(this).parent().parent().remove(); 	
		CalculateGLTotals();
	});
	
	$('#frmPaymentReceive #tblMain input').die('keyup').live('keyup',function(){
		 if($(this).val()=="" || isNaN($(this).val())){
				$(this).val(0);
				$(this).select();
			}
	});
	
	$('#frmPaymentReceive #tblGL .cls_cbo_numberOnly').die('keyup').live('keyup',function(){
		 if($(this).val()=="" || isNaN($(this).val())){
				$(this).val(0);
				$(this).select();
			}
	});	
	
	if(customerIdAL!='')
		LoadAutoData(customerIdAL,currencyIdAL);
});

function New()
{
	window.location.href = '?q='+menuId;
}

function LoadCustomerChange()
{	
	var url 	= basePath+"payment_receive_db.php?RequestType=URLLoadCustomerChange";
	var data 	= "CustomerId="+$('#frmPaymentReceive #cboCustomer').val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		$('#frmPaymentReceive #cboPaymentsMethods').val(json.PayMethod);
	}
	});
}

function LoadMainDetails()
{
	ClearObjects();
	showWaiting();
	var url 	= basePath+"payment_receive_db.php?RequestType=URLLoadMainDetails";
	var data 	= "CustomerId="+$('#frmPaymentReceive #cboCustomer').val();
		 data  += "&CurrencyId="+$('#frmPaymentReceive #cboCurrency').val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		if(json.GRID == null)
		{
			hideWaiting();
		}		
		else
		{
			var length 		= json.GRID.length;
			var data 		= json.GRID;
			
			for(var i=0;i<length;i++)
			{
				$('#frmPaymentReceive #txtUnsettleAdvance').val(data[i]['UNSETTLE_ADVANCE']);
				CreateGrid(data[i]['INVOICE_HTML'],data[i]['INVOICED_DATE'],data[i]['INVOICE_REMARKS'],data[i]['INVOICED_VALUE'],data[i]['RECEIVED_VALUE']+data[i]['INV_SETTLEMENT'],data[i]['CREDIT_VALUE'],data[i]['ADVANCE_VALUE'],data[i]['TOBEPAID'],data[i]['ORDER_NO'],data[i]['UN_SETTLE'],data[i]['DEBIT_VALUE'],data[i]['TYPE']);
			}
				CreateGridLastRow();
			
			var gl_data		= json.GL_GRID;
			
			for(var i=0;i<json.GL_GRID.length;i++)
			{
				$('#frmPaymentReceive #tblGL tbody tr:last').after("<tr class=\"cls_tr_autoAdd\">"+$('#frmPaymentReceive #tblGL .cls_tr_firstRow').html()+"</tr>");
				$('#frmPaymentReceive #tblGL tr:eq(3)').find('.cls_td_GL').children().html(gl_data[i]['GL_HTML']);
				$('#frmPaymentReceive #tblGL tr:eq(3)').find('.cls_td_GL').addClass('cls_td_autoAdd');
				$('#frmPaymentReceive #tblGL tr:eq(3)').find('.cls_td_GL').attr('title','C');
				$('#frmPaymentReceive #tblGL tr:eq(3)').find('.cls_td_glAmount_cr').children().addClass('cls_txt_autoAdd_cr');
				$('#frmPaymentReceive #tblGL tr:eq(3)').find('.cls_td_glAmount_cr').children().attr('disabled',false);
				$('#frmPaymentReceive #tblGL tr:eq(3)').find('.cls_td_glAmount_dr').children().attr('disabled',true);			
			}
		}
		hideWaiting();
	}
	});
	
	hideWaiting();
}

function CreateGrid(invoiceNo,invoicedDate,invoiceRemarks,invoiceValue,receivedValue,creditValue,advanceValue,tobePaid,orderNo,unSettle,debitValue,type)
{
	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);		
	row.setAttribute('bgcolor',unSettle);
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'cls_td_check';
	if(unSettle!='#FFFFF')
		cell.innerHTML  = "<input type=\"checkbox\" class=\"cls_check\" disabled=\"disabled\">";
	else
		cell.innerHTML  = "<input type=\"checkbox\" class=\"cls_check\">";
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'cls_td_invoiceNo';
	cell.id			= orderNo;
	cell.innerHTML 	= invoiceNo;
	
	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'cls_td_invoiceRemarks';
	cell.innerHTML 	= (invoiceRemarks==''?'&nbsp;':invoiceRemarks);
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'cls_td_invoiceDate';
	cell.id			= type;
	cell.innerHTML 	= invoicedDate;
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:right');
	cell.innerHTML 	= invoiceValue;
	
	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:right');
	cell.innerHTML 	= creditValue;
	
	var cell 		= row.insertCell(6);
	cell.setAttribute("style",'text-align:right');
	cell.innerHTML 	= debitValue;
	
	var cell 		= row.insertCell(7);
	cell.setAttribute("style",'text-align:right');
	cell.innerHTML 	= advanceValue;
	
	var cell 		= row.insertCell(8);
	cell.setAttribute("style",'text-align:right');
	cell.innerHTML 	= receivedValue;
	
	var cell 		= row.insertCell(9);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'cls_td_toBePaid';
	cell.innerHTML 	= tobePaid;
	
	var cell 		= row.insertCell(10);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'cls_td_payingAmount';
	cell.innerHTML 	= "<input type=\"textbox\" style=\"width:100%;text-align:right\" value=\""+0+"\" disabled=\"disabled\" >";
}

function CreateGridLastRow()
{
	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);		
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:center;font-weight:bold');
	cell.innerHTML 	= "TOTAL";
	
	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:center;font-weight:bold');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:center;font-weight:bold');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:right;font-weight:bold');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:right;font-weight:bold');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(6);
	cell.setAttribute("style",'text-align:right;font-weight:bold');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(7);
	cell.setAttribute("style",'text-align:right;font-weight:bold');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(8);
	cell.setAttribute("style",'text-align:right;font-weight:bold');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(9);
	cell.setAttribute("style",'text-align:right;font-weight:bold');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(10);
	cell.setAttribute("style",'text-align:right;font-weight:bold');
	cell.className	= 'cls_td_total_toBePaid';
	cell.innerHTML  = "&nbsp;";
}

function ValidateRowWhenCheck(obj)
{
	if(obj.is(':checked'))
	{
		var toBePaidValue = parseFloat(obj.parent().parent().find('.cls_td_toBePaid').html());
		obj.parent().parent().find('.cls_td_payingAmount').children().val(toBePaidValue);
		obj.parent().parent().find('.cls_td_payingAmount').children().removeAttr('disabled');;
	}
	else
	{
		obj.parent().parent().find('.cls_td_payingAmount').children().val(0);
		obj.parent().parent().find('.cls_td_payingAmount').children().attr('disabled','disabled');;
	}
	CalculateTotals();
}

function CalculateTotals()
{
	var totalPaying	= 0;
	$('#frmPaymentReceive #tblMain .cls_td_payingAmount').each(function(index, element) {
    	if($(this).parent().find('.cls_td_check').children().is(':checked')){
			var toBePaid		= parseFloat($(this).parent().find('.cls_td_toBePaid').html());
			var payingAmount	= parseFloat($(this).children().val());
			
			/*BEGIN - COMMENTER THIS LINE BECUASE USER MUST ABLE TO ENTER MORE THAN INVOICE VALUE & SYSTEM WILL RAISE PAYMENT ADVANCE FOR EXCEEDED VALUE AUTOMATICALLY {
				if(toBePaid<payingAmount)
				$(this).children().val(toBePaid);
			  END   - }*/
			
			totalPaying 	+= parseFloat($(this).children().val());
		}			
    });
	totalPaying	= RoundNumber(totalPaying,2,true);
	$('#frmPaymentReceive #tblMain .cls_td_total_toBePaid').html(totalPaying);
	$('#frmPaymentReceive #td_totalPayingAmount').html(totalPaying);
}

function CalculateGLTotals()
{
	var total	= 0;
	$('#frmPaymentReceive #tblGL tbody .cls_td_glAmount_dr').each(function(){
    		total	+= parseFloat($(this).children().val());
    });
	total	= RoundNumber(total,2,true);
	$('#frmPaymentReceive #tblGL tfoot .cls_td_totAmount_dr').html(total);
	
	var total	= 0;
	$('#frmPaymentReceive #tblGL tbody .cls_td_glAmount_cr').each(function(){
    		total	+= parseFloat($(this).children().val());
    });
	total	= RoundNumber(total,2,true);
	$('#frmPaymentReceive #tblGL tfoot .cls_td_totAmount_cr').html(total);
	
	var total	= 0;
	$('#frmPaymentReceive #tblGL .cls_td_GLAmount').each(function(){
		if($(this).parent().find('.cls_td_GL').attr('title')=='D')
    		total	+= parseFloat($(this).children().val());
    });
	total	= RoundNumber(total,2,true);
	$('#frmPaymentReceive #td_totalGLAlloAmount').html(total);
}

function AddNewGL()
{
	if(!$('#frmPaymentReceive').validationEngine('validate')){
		setTimeout("HideValidation('#frmPaymentReceive','')",2000);	
		return false;
	}
	$('#frmPaymentReceive #tblGL tbody tr:last').after("<tr>"+$('#frmPaymentReceive #tblGL .cls_tr_firstRow').html()+"</tr>");
}

function Save()
{
	if(!IsProcessMonthLocked_date($('#frmPaymentReceive #txtReceiptDate').val()))
		return;
		
	if(!Validation_Save()){
		setTimeout("HideValidation('#frmPaymentReceive','')",4000);	
		return;
	}
	
	showWaiting();
	var detail_array		= "[";
	$('#frmPaymentReceive #tblMain .cls_td_check').children(':checked').each(function(){
			detail_array += "{";
			detail_array += '"OrderNo":"'+$(this).parent().parent().find('.cls_td_invoiceNo').attr('id')+'",';
			detail_array += '"InvoiceNo":"'+$(this).parent().parent().find('.cls_td_invoiceNo').children().attr('id')+'",';
			detail_array += '"ToBeReceive":"'+$(this).parent().parent().find('.cls_td_toBePaid').html()+'",';
			detail_array += '"PayAmount":"'+$(this).parent().parent().find('.cls_td_payingAmount').children().val()+'",';
			detail_array += '"Type":"'+$(this).parent().parent().find('.cls_td_invoiceDate').attr('id')+'"';
			detail_array +=  '},';
	});

	detail_array 		 = detail_array.substr(0,detail_array.length-1);
	detail_array 		+= " ]";
	
	var gl_Array		= "[";
	$('#frmPaymentReceive #tblGL .cls_td_GL').each(function(){
			gl_Array += "{";
			gl_Array += '"GLAccount":"'+$(this).parent().find('.cls_td_GL').children().val()+'",';
			gl_Array += '"GLValue_DR":"'+$(this).parent().find('.cls_td_glAmount_dr').children().val()+'",';
			gl_Array += '"GLValue_CR":"'+$(this).parent().find('.cls_td_glAmount_cr').children().val()+'",';
			gl_Array += '"GLRemarks":"'+$(this).parent().find('.cls_td_remarks').children().val()+'",';
			gl_Array += '"GLCostCenter":"'+$(this).parent().find('.cls_td_costCenter').children().val()+'",';
			gl_Array += '"TransType":"'+$(this).parent().find('.cls_td_GL').attr('title')+'"';
			gl_Array +=  '},';
	});

	gl_Array 		 = gl_Array.substr(0,gl_Array.length-1);
	gl_Array 		+= " ]";

	var url 	= basePath+"payment_receive_db.php?RequestType=URLSave";
	var data 	= "CustomerId="+$('#frmPaymentReceive #cboCustomer').val();
		data   += "&CurrencyId="+$('#frmPaymentReceive #cboCurrency').val();
		data   += "&Remarks="+$('#frmPaymentReceive #txtRemarks').val();
		data   += "&ReceiptDate="+$('#frmPaymentReceive #txtReceiptDate').val();
		data   += "&PaymentMode="+$('#frmPaymentReceive #cboPaymentsMethods').val();
		data   += "&DetailArray="+detail_array;
		data   += "&GLArray="+gl_Array;
	
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type: "POST",
	async:false,
	success:function(json)
		{
			$('#frmPaymentReceive #butSave').validationEngine('showPrompt', json.msg,json.type);
			if(json.type == 'pass')
			{
				$('#frmPaymentReceive #txtReceiptNo').val(json.ReceiptNo);
				$('#frmPaymentReceive #txtReceiptYear').val(json.ReceiptYear);
				$('#frmPaymentReceive #butSave').hide('slow');
				$('#frmPaymentReceive #butCancel').hide('slow');
				hideWaiting();
			}
			else
			{
				hideWaiting();
			}
		},
	error:function(xhr,status)
		{
			$('#frmPaymentReceive #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			hideWaiting();
		}
	});
	hideWaiting();
}

function Validation_Save()
{
	var totalPaying 		= parseFloat($('#frmPaymentReceive #td_totalPayingAmount').html());
	var totalGL				= parseFloat($('#frmPaymentReceive #td_totalGLAlloAmount').html());
	var tot_debitAmount		= parseFloat($('#frmPaymentReceive #tblGL tfoot .cls_td_totAmount_dr').html())
	var tot_creditAmount	= parseFloat($('#frmPaymentReceive #tblGL tfoot .cls_td_totAmount_cr').html())
	var customerGLAmount	= parseFloat($('#frmPaymentReceive #tblGL tbody .cls_txt_autoAdd_cr').val());
		
	if(!$('#frmPaymentReceive').validationEngine('validate'))
		return false;
	
	if($('#frmPaymentReceive #txtReceiptNo').val()!="")
	{
		alert("Receipt No already saved.");
		return false;
	}
	
	if(totalPaying<=0)
	{
		alert('Total paying amount shold be more than zero.');
		return false;
	}
	
	if(customerGLAmount!=totalPaying)
	{
		alert("'Total Paying Amount' and 'Debtor Credit Amount' should be equal.");
		return false;
	}
	
	/*BEGIN - This if condition commented and add another if condition to validate credit debit balance only {.
	if(totalPaying != totalGL)
	{
		alert("'Total Receiving Amount' and 'Total GL Allocated Amount' should be equal.")
		return false;
	}
	END - This if condition commented and add another if condition to validate credit debit balance only.  } */
	if(tot_debitAmount != tot_creditAmount)
	{
		alert("'Credit Amount' and 'Debit Amount' should be equal.")
		return false;
	}
	
return true;
}

function Report()
{
	if($('#frmPaymentReceive #txtReceiptNo').val()=="")
	{
		alert("No details available to view report.");
		return;
	}
	var url  = "?q="+reportId+"&SerialNo="+$('#frmPaymentReceive #txtReceiptNo').val()
	    url += "&SerialYear="+$('#frmPaymentReceive #txtReceiptYear').val();
	window.open(url,'rptpayment_receive.php');
}

function CheckAll()
{
	if($(this).is(':checked'))
		$('#frmPaymentReceive #tblMain .cls_check').attr('checked','checked');
	else
		$('#frmPaymentReceive #tblMain .cls_check').removeAttr('checked');
		
	$('#frmPaymentReceive #tblMain .cls_check').each(function(){
		ValidateRowWhenCheck($(this));
	});
}

function ClearObjects()
{
	$("#frmPaymentReceive #tblMain tr:gt(0)").remove();
	$("#frmPaymentReceive #tblGL tbody tr:gt(0)").remove();
	$('#frmPaymentReceive #tblMain .cls_th_checkAll').removeAttr('checked');
	
	$("#frmPaymentReceive #tblGL .cls_td_GL").children().val('');
	$("#frmPaymentReceive #tblGL .cls_td_GLAmount").children().val(0);
	$("#frmPaymentReceive #tblGL .cls_td_remarks").children().val('');
	$("#frmPaymentReceive #tblGL .cls_td_costCenter").children().val('');
	
	$('#frmPaymentReceive #tblGL .removeRow').each(function(){
		if($(this).parent().parent().attr('class')=="")
		 	$(this).parent().parent().remove();		
	});
	
	CalculateTotals();
	CalculateGLTotals();
}

function SettleReport()
{
	var url  = "?q="+settleMenuId;
		url += "&cboCustomer="+$('#frmPaymentReceive #cboCustomer').val(); 
		url += "&cboCurrency="+$('#frmPaymentReceive #cboCurrency').val();
	window.open(url,'advance_settlement.php');
}

function LoadAutoData(customerId,currencyId)
{
	$('#frmPaymentReceive #cboCustomer').val(customerId);
	LoadCustomerChange();
	$('#frmPaymentReceive #cboCurrency').val(currencyId);
	//LoadMainDetails();
	$('#frmPaymentReceive #cboCurrency').change();
}

function WhenChangeGL()
{
	var obj		= $(this);
	var url 	= basePath+"payment_receive_db.php?RequestType=URLWhenChangeGL";
	var data 	= "GLID="+$(this).val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		obj.parent().parent().find('.cls_td_GL').attr('title',json.ADD);
		if(json.ADD=='D'){
			obj.parent().parent().find('.cls_td_glAmount_dr').children().val('0');
			obj.parent().parent().find('.cls_td_glAmount_cr').children().val('0');
			obj.parent().parent().find('.cls_td_glAmount_cr').children().attr('disabled',true);
			obj.parent().parent().find('.cls_td_glAmount_dr').children().attr('disabled',false);			
			obj.parent().parent().find('.cls_td_glAmount_dr').children().select();
		}else if(json.ADD=='C'){
			obj.parent().parent().find('.cls_td_glAmount_cr').children().val('0');
			obj.parent().parent().find('.cls_td_glAmount_dr').children().val('0');
			obj.parent().parent().find('.cls_td_glAmount_dr').children().attr('disabled',true);
			obj.parent().parent().find('.cls_td_glAmount_cr').children().attr('disabled',false);
     		obj.parent().parent().find('.cls_td_glAmount_cr').children().select();
			
		}
	}
	});
}

function Cancel()
{
	if($('#frmPaymentReceive #txtReceiptNo').val()=="")
	{
		return;
	}
	var x = confirm("Are you sure you want to cancel Payment Received No : "+$('#frmPaymentReceive #txtReceiptNo').val()+"/"+$('#frmPaymentReceive #txtReceiptYear').val()+" .");
		if(!x)return;
	
	var url 	= basePath+"payment_receive_db.php?RequestType=URLCancel";
	var data 	= "ReceiptNo="+$('#frmPaymentReceive #txtReceiptNo').val();
	 	data   += "&ReceiptYear="+$('#frmPaymentReceive #txtReceiptYear').val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		$('#frmPaymentReceive #butCancel').validationEngine('showPrompt',json.msg,json.type );
	}
	});
	var t = setTimeout("HideValidation('#butCancel')",2000);
}

function HideValidation(form,button)
{
	$(form+' '+button).validationEngine('hide');
}
function confirmPayment()
{
	var x = confirm("Are you sure you want to Confirm this Payment/s.");
	if(!x)
		return;
					
	var data = "RequestType=confirmPayment";
	value = "[";
	$('.cls_chkConfirm').children().each(function(){
		
		if($(this).attr('checked'))
		{
			var receiptNo 	= $(this).parent().parent().find('.cls_receiptNo').html();
			var receiptYear	= $(this).parent().parent().find('.cls_receiptYear').html();
			
			value += "{";
			value += '"receiptNo":"'+ receiptNo +'",' ;
			value += '"receiptYear":"'+ receiptYear +'"' ;
			value += "},";
		}
	});
	value = value.substr(0,value.length-1);
	value += "]";
	
	data += "&confirmDetails="+value;
	
	var url = basePath+"payment_receive_db.php";
	var obj = $.ajax({
		url:url,
		type:'post',
		dataType:'json',  
		data:data,
		async:false,
		success:function(json){
				if(json.type=='pass')
				{
					alert("Confirmed Sucessfully");
					window.location.href = 'receipt_confermation.php';
				}
				else
				{
				alert(json.msg);
				}
			}	
		});
}
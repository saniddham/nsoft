<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$menuId				= 706;
$reportId			= 930;

$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'STATUS'=>'CPRH.STATUS',
				'CONCAT_RECEIPT'=>"CONCAT(CPRH.RECEIPT_NO,'/',CPRH.RECEIPT_YEAR)",
				'CUSTOMER_NAME'=>'CU.strName',
				'RECEIPT_DATE'=>'CPRH.RECEIPT_DATE',
				'REMARKS'=>'CPRH.REMARKS',
				'CURRENCY_NAME'=>'C.strCode'
				);
$arr_status = array('Confirmed'=>'1','Canceled'=>'-2');				
foreach($arr as $k=>$v)
{
	if($v['field']=='STATUS')
	{
		$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND CPRH.RECEIPT_DATE = '".date('Y-m-d')."'";
	
// }

$sql = "SELECT SUB_1.* FROM
			(SELECT
			  CPRH.RECEIPT_YEAR										AS RECEIPT_YEAR,
			  CPRH.RECEIPT_NO										AS RECEIPT_NO,
			  CONCAT(CPRH.RECEIPT_NO,'/',CPRH.RECEIPT_YEAR)			AS CONCAT_RECEIPT,
			  CU.intId												AS CUSTOMER_ID,
			  CU.strName											AS CUSTOMER_NAME,
			  'View'												AS REPORT,
			  C.strCode												AS CURRENCY_NAME,
			  CPRH.RECEIPT_DATE										AS RECEIPT_DATE,
			  IF(CPRH.STATUS=1,'Confirmed','Canceled') 				AS STATUS,
			  CPRH.REMARKS											AS REMARKS,
			  
			  (SELECT ROUND(SUM(SUB_CPRD.PAY_AMOUNT),2)						 		
		      FROM finance_customer_pay_receive_details SUB_CPRD
			  WHERE SUB_CPRD.RECEIPT_NO = CPRH.RECEIPT_NO
			    AND SUB_CPRD.RECEIPT_YEAR = CPRH.RECEIPT_YEAR)		AS INVOICE_VALUE
				  
			FROM finance_customer_pay_receive_header CPRH
			INNER JOIN mst_customer CU ON CU.intId = CPRH.CUSTOMER_ID
			INNER JOIN mst_financecurrency C ON C.intId = CPRH.CURRENCY_ID
			WHERE
			CPRH.COMPANY_ID = $session_companyId
			$where_string
		)  
		AS SUB_1 WHERE 1=1";

$jq 	= new jqgrid('',$db);	
$col 	= array();
$cols 	= array();

$col["title"] 			= "Status";
$col["name"] 			= "STATUS";
$col["width"] 			= "2"; 						// not specifying width will expand to fill space
$col["align"] 			= "left";
$col["sortable"] 		= true; 						// this column is not sortable
$col["search"] 			= true; 						// this column is not searchable
$col["editable"] 		= false;
$col["stype"] 			= "select";
$str 					= ":All;Confirmed:Confirmed;Canceled:Canceled";  // all row, blank row, then all db values. ; is separator
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Serial No";
$col["name"] 			= "RECEIPT_NO";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Serial Year";
$col["name"] 			= "RECEIPT_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Payment Received No";
$col["name"] 			= "CONCAT_RECEIPT";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col['link']			= '?q='.$menuId.'&SerialNo={RECEIPT_NO}&SerialYear={RECEIPT_YEAR}';
$col["linkoptions"] 	= "target='payment_receive.php'";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Customer";
$col["name"] 			= "CUSTOMER_NAME";
$col["width"] 			= "10";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Date";
$col["name"] 			= "RECEIPT_DATE";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Remarks";
$col["name"] 			= "REMARKS";
$col["width"] 			= "8";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "CURRENCY_NAME";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Amount";
$col["name"] 			= "INVOICE_VALUE";
$col["width"] 			= "2";
$col["align"] 			= "right";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Report";
$col["name"] 			= "REPORT";
$col["width"] 			= "1";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= '?q='.$reportId.'&SerialNo={RECEIPT_NO}&SerialYear={RECEIPT_YEAR}';
$col["linkoptions"] 	= "target='rptpayment_receive.php'";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Payment Receive Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'RECEIPT_YEAR,RECEIPT_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "ASC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND","rules":[{"field":"Status","op":"eq","data":"Confirmed"}]
}
SEARCH_JSON;
//$grid["postData"] = array("filters" => $sarr ); 

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Payment Receive Listing</title>
<?php
echo $out;
<?php
date_default_timezone_set('Asia/Kolkata');
session_start();
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

$backwardseperator 	= "../../../../";
require_once "../../../../dataAccess/Connector.php";
require_once("../../../../libraries/jqgrid2/inc/jqgrid_dist.php");

$sql = "SELECT SUB_1.* FROM
			(SELECT
			  CPRH.RECEIPT_YEAR										AS RECEIPT_YEAR,
			  CPRH.RECEIPT_NO										AS RECEIPT_NO,
			  CONCAT(CPRH.RECEIPT_NO,'/',CPRH.RECEIPT_YEAR)			AS CONCAT_RECEIPT,
			  CU.intId												AS CUSTOMER_ID,
			  CU.strName											AS CUSTOMER_NAME,
			  'View'												AS REPORT,
			  C.strCode												AS CURRENCY_NAME,
			  CPRH.RECEIPT_DATE										AS RECEIPT_DATE,
			  IF(CPRH.RECEIPT_CONFIRMATION=1,'Confirmed','Pending') AS STATUS,
			  CPRH.REMARKS											AS REMARKS,
			  
			  (SELECT ROUND(SUM(SUB_CPRD.PAY_AMOUNT),2)						 		
		      FROM finance_customer_pay_receive_details SUB_CPRD
			  WHERE SUB_CPRD.RECEIPT_NO = CPRH.RECEIPT_NO
			    AND SUB_CPRD.RECEIPT_YEAR = CPRH.RECEIPT_YEAR)		AS INVOICE_VALUE
				  
			FROM finance_customer_pay_receive_header CPRH
			INNER JOIN mst_customer CU ON CU.intId = CPRH.CUSTOMER_ID
			INNER JOIN mst_financecurrency C ON C.intId = CPRH.CURRENCY_ID
			WHERE
				CPRH.COMPANY_ID = $session_companyId AND
				CPRH.STATUS = 1
		)  
		AS SUB_1 WHERE 1=1";

$jq = new jqgrid();	
$col = array();

$col["title"] 			= "Status";
$col["name"] 			= "STATUS";
$col["width"] 			= "2"; 						// not specifying width will expand to fill space
$col["align"] 			= "left";
$col["sortable"] 		= true; 						// this column is not sortable
$col["search"] 			= true; 						// this column is not searchable
$col["editable"] 		= false;
$col["stype"] 			= "select";
$str 					= "Pending:Pending;Confirmed:Confirmed";  // all row, blank row, then all db values. ; is separator
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Serial No";
$col["name"] 			= "RECEIPT_NO";
$col["classes"] 		= "cls_receiptNo";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= true;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Serial Year";
$col["name"] 			= "RECEIPT_YEAR";
$col["classes"] 		= "cls_receiptYear";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= true;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Payment Received No";
$col["name"] 			= "CONCAT_RECEIPT";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col['link']			= 'payment_receive.php?SerialNo={RECEIPT_NO}&SerialYear={RECEIPT_YEAR}';
$col["linkoptions"] 	= "target='payment_receive.php'";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Customer";
$col["name"] 			= "CUSTOMER_NAME";
$col["width"] 			= "10";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Date";
$col["name"] 			= "RECEIPT_DATE";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Remarks";
$col["name"] 			= "REMARKS";
$col["width"] 			= "8";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "CURRENCY_NAME";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Amount";
$col["name"] 			= "INVOICE_VALUE";
$col["width"] 			= "2";
$col["align"] 			= "right";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Confirm";
$col["name"] 			= "RECEIPT_CONFIRMATION";
$col["width"] 			= "2";
$col["classes"] 		= "cls_chkConfirm";
$col["align"] 			= "center";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 	
$col["edittype"]		= "checkbox";		
$col["editable"] 		= true;
$col["editoptions"] 	= array("value"=>"True:False");
$col["formatter"]		= "checkbox";
$col["formatoptions"] 	= array("disabled"=>false);
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Report";
$col["name"] 			= "REPORT";
$col["width"] 			= "1";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= 'rptpayment_receive.php?SerialNo={RECEIPT_NO}&SerialYear={RECEIPT_YEAR}';
$col["linkoptions"] 	= "target='rptpayment_receive.php'";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Receipt Confirmation Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'RECEIPT_YEAR,RECEIPT_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "ASC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND","rules":[{"field":"Status","op":"eq","data":"Pending"}]
}
SEARCH_JSON;
$grid["postData"] = array("filters" => $sarr ); 

$jq->set_actions(array(	
						"add"=>false, // allow/disallow add
						"edit"=>true, // allow/disallow edit
						"delete"=>false, // allow/disallow delete
						"rowactions"=>true, // show/hide row wise edit/del/save option
						"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
						"export"=>true
						) 
				);

$jq->select_command = $sql;
$jq->set_options($grid);
$jq->set_columns($cols);				
$out = $jq->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Receipt Confirmation Listing</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>
<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $backwardseperator?>libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $backwardseperator?>libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	

<script src="<?php echo $backwardseperator?>libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $backwardseperator?>libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="<?php echo $backwardseperator?>libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
<script src="<?php echo $backwardseperator?>libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
<script src="<?php echo $backwardseperator?>libraries/javascript/script.js" type="text/javascript"></script>
<script type="application/javascript" src="payment_receive.js"></script>

<script type="application/javascript" src="<?php echo $backwardseperator?>libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="<?php echo $backwardseperator?>libraries/javascript/jquery-impromptu.min.js"></script>

   <td><table width="100%" border="0">
      <tr>
        <td>
        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
        </td>
      </tr>
       <tr>
        <td align="center" ><a class="button green medium" id="butRConfirm" name="butRConfirm">&nbsp;Confirm&nbsp;</a></td>
      </tr>
    </table></td>
    </tr>
</form>
</body>
</html>
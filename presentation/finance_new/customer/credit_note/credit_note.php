<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$ordReportId		= 896;
$session_companyId	= $_SESSION["headCompanyId"];

include_once ("class/finance/cls_get_gldetails.php");
include_once ("class/cls_commonFunctions_get.php");
//include 	  "include/javascript.html";

$obj_GLDetails_get			= new Cls_Get_GLDetails($db);
$obj_common_function_get	= new cls_commonFunctions_get($db);

$serialNo					= (!isset($_REQUEST["SerialNo"])?'':$_REQUEST["SerialNo"]);
$serialYear					= (!isset($_REQUEST["SerialYear"])?'':$_REQUEST["SerialYear"]);
$customerId					= (!isset($_REQUEST["CustomerId"])?'':$_REQUEST["CustomerId"]);
$programCode				= 'P0704';

$header_array 				= GetHeaderDetails($serialYear,$serialNo);
$detail_result 				= GetGridDetails($serialYear,$serialNo);

if($obj_common_function_get->ValidateSpecialPermission('24',$_SESSION["userId"],'RunQuery'))
	$dateDisabled			= "";
else
	$dateDisabled			= "disabled='disabled'";	
	
if(!isset($_REQUEST["SerialNo"]))
	$invoiceDate			= date("Y-m-d");
else
	$invoiceDate			= $header_array["INVOICED_DATE"];
?>
<title>Customer Credit Note</title>

<!--<script type="text/javascript" src="presentation/finance_new/customer/credit_note/credit_note.js"></script>-->
<script>
	var customerIdAL	='<?php echo $customerId; ?>';
</script>

<form id="frmCreditNote" name="frmCreditNote" method="post">
<div align="center">
  <div class="trans_layoutS" style="width:1100px">
    <div class="trans_text">Customer Credit Note</div>
    <table width="1100">
      <tr>
        <td><table width="100%%" border="0" class="normalfnt">
          <tr>
            <td width="16%">Credit Number</td>
            <td width="36%"><input type="text" name="txtCreditNo" id="txtCreditNo" style="width:80px" disabled="disabled" value="<?php echo $header_array["INVOICE_NO"]?>"/> <input type="text" name="txtCreditYear" id="txtCreditYear" style="width:40px" disabled="disabled" value="<?php echo $header_array["CONCAT_SERIAL_NO"]?>"/></td>
            <td width="18%">&nbsp;</td>
            <td width="30%"></td>
          </tr>
          <tr>
            <td>Customer <span class="compulsoryRed">*</span></td>
            <td><select id="cboCustomer" style="width:280px;height:20px">
            <option value="">&nbsp;</option>
              <?php $sql = "SELECT DISTINCT
							 CU.strName 			AS CUSTOMER_NAME,
							 ST.CUSTOMER_ID			AS CUSTOMER_ID
							FROM finance_customer_transaction ST
							  INNER JOIN mst_customer CU ON CU.intId = ST.CUSTOMER_ID
							WHERE ST.COMPANY_ID = '$session_companyId'
							GROUP BY ST.INVOICE_YEAR,ST.INVOICE_NO
							HAVING ROUND(SUM(ST.VALUE),2)  > 0";
					$result = $db->RunQuery($sql);
					while($row = mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row["CUSTOMER_ID"]."\">".$row["CUSTOMER_NAME"]."</option>";
					}
			?>
              </select></td>
            <td>Date</td>
            <td><input name="txtDate" type="text" value="<?php echo $invoiceDate ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo $dateDisabled?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
          <tr>
            <td>Invoice No <span class="compulsoryRed">*</span></td>
            <td><select name="cboInvoiceNo" id="cboInvoiceNo" style="width:280px" >
              <option value="">&nbsp;</option>
            </select> <a class="button green small" id="hylink_invoice" >Report</a></td>
            <td>Order No</td>
            <td  id="td_order"><?php echo $header_array["ORDER_HTML"]?></td>
          </tr>
          <tr>
            <td>Currency</td>
            <td><select name="cboCurrency" id="cboCurrency" style="width:280px" disabled="disabled" class="cls_cbo_CurrencyCalExchangeRate">
              <option value=""></option>
              <?php
$sql = "SELECT intId,strCode FROM  mst_financecurrency WHERE intStatus = 1 ORDER BY intId";
$result = $db->RunQuery($sql);
while($row = mysqli_fetch_array($result))
{
	if($row["intId"]==$header_array["CURRENCY_ID"])
		echo "<option value=\"".$row["intId"]."\" selected=\"selected\">".$row["strCode"]."</option>";
	else
		echo "<option value=\"".$row["intId"]."\">".$row["strCode"]."</option>";
}
?>
              </select>
              <input type="text" name="txtExchangeRate" id="txtExchangeRate" style="width:60px;text-align:right" disabled="disabled" value="" title="Exchange Rate" class="cls_txt_exchangeRate validate[required]"/></td>
            <td>Ledger Account</td>
            <td><select name="cboLedgerAccount" disabled="disabled" id="cboLedgerAccount" style="width:280px">
              <option value="">&nbsp;</option>
              <?php
			 $sql = "SELECT
					  COF.intId AS LEDGER_ID,
					  CONCAT(COF.strCode,'-',COF.strName) AS LEDGER_NAME
					FROM mst_financechartofaccounts COF
					WHERE COF.intStatus = 1
					ORDER BY COF.strCode";
			$result = $db->RunQuery($sql);
			while($row = mysqli_fetch_array($result))
			{
				if($row["LEDGER_ID"]==$header_array["LEDGER_ID"])
					echo "<option value=\"".$row["LEDGER_ID"]."\" selected=\"selected\">".$row["LEDGER_NAME"]."</option>";
				else
					echo "<option value=\"".$row["LEDGER_ID"]."\">".$row["LEDGER_NAME"]."</option>";
			}
			 ?>
              </select></td>
          </tr>
          <tr>
            <td>Invoice Type</td>
            <td><select name="cboInvoiceType" id="cboInvoiceType" style="width:280px" disabled="disabled">
              <option value=""></option>
              <?php
$sql = "SELECT intId, strInvoiceType FROM mst_invoicetype ORDER BY strInvoiceType ";
$result = $db->RunQuery($sql);
while($row = mysqli_fetch_array($result))
{
	if($row["intId"]==$header_array["INVOICE_TYPE"])
		echo "<option value=\"".$row["intId"]."\" selected=\"selected\">".$row["strInvoiceType"]."</option>";
	else
		echo "<option value=\"".$row["intId"]."\">".$row["strInvoiceType"]."</option>";
}
?>
            </select></td>
            <td valign="top">Invoice Balance</td>
            <td valign="top"><input type="text" name="txtInvoiceBalance" id="txtInvoiceBalance" style="width:98px;text-align:right" disabled="disabled" value="<?php echo $header_array["INVOICE_NO"]?>"/></td>
          </tr>
          <tr>
            <td>Remarks</td>
            <td><textarea name="txtRemarks" id="txtRemarks" style="width:280px;height:50px" ><?php echo $header_array["REMARKS"]?></textarea></td>
            <td valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td ><div style="overflow:scroll;width:1100px;height:250px;"><table width="100%%" border="0" class="bordered" id="tblMain">
        <thead>
          <tr>
            <th width="3%"><input type="checkbox" checked="checked" class="cls_chk_checkAll"></th>
            <th width="12%">Sales Order No</th>
            <th width="12%">Graphic No</th>
            <th width="10%">Style No</th>
            <th width="15%">Placement</th>
            <th width="5%">Qty <span class="compulsoryRed">*</span></th>
            <th width="8%">Price</th>
            <th width="8%">Value</th>
            <th width="9%">Tax</th>
            <th width="9%">Cost Center <span class="compulsoryRed">*</span></th>
            <th width="9%">Item <span class="compulsoryRed">*</span></th>
            </tr>
        </thead>        
        <tbody>
<?php
$loop = 0;
while($row = mysqli_fetch_array($detail_result))
{ 
?>
		<tr>
            <td class="cls_td_check" style="text-align:center"><input type="checkbox" checked="checked" class="cls_check"></td>
            <td class="cls_td_salesOrderNo"><?php echo $row["SALES_ORDER_NO"]?></td>
            <td width="12%"><?php echo $row["GRAPHIC_NO"]?></td>
            <td width="10%"><?php echo $row["STYLE_NO"]?></td>
            <td width="15%"><?php echo $row["PART"]?></td>
            <td class="cls_td_invoice cls_Subtract" style="text-align:center"><input type="textbox" style="width:50px;text-align:right" value="<?php echo $row["INVOICE_QTY"]?>"></td>
            <td style="text-align:right"><?php echo $row["INVOICE_PRICE"]?></td>
            <td class="cls_td_value" style="text-align:right"><?php echo $row["INVOICE_VALUE"]?></td>
            <td style="text-align:center" class="cls_td_tax" id="<?php echo $row["TAX_VALUE"]?>"><?php echo GetTax(++$loop,$row["TAX_CODE_ID"])?></td>
            <td style="text-align:center"><?php echo GetCostCenter(++$loop,$row["COST_CENTER_ID"])?></td>
            <td style="text-align:center"><?php echo GetGL_HTML(++$loop,$row["GL_ACCOUNT"])?></td>
		</tr>
<?php
}
?>
        </tbody>
        </table></div></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td ><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="52%" ><table width="99%" class="bordered" id="tblGL" style="display:none">
              <thead>
                <tr>
                  <th colspan="3" >GL Allocation
                    <div style="float:right"><a class="button white small" id="butAddNewGL">Add New GL</a></div></th>
                </tr>
                <tr>
                  <th width="6%" >Del</th>
                  <th width="69%" >GL Account <span class="compulsoryRed">*</span></th>
                  <th width="25%" >Amount <span class="compulsoryRed">*</span></th>
                </tr>
              </thead>
              <tbody>
                <?php  
while($row = mysqli_fetch_array($gl_result))
{
	$booAvailable	= true;
?>
                <tr class="cls_tr_firstRow">
                  <td width="6%" style="text-align:center" ><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover removeRow" id="butDel" tabindex="24"/></td>
                  <td width="69%" class="cls_td_GL"><select name="cboLedgerAc" id="cboLedgerAc" class="clsLedgerAc validate[required]"  style="width:100%" >
                      <?php
							echo $obj_GLDetails_get->getGLCombo('CUSTOMER_CREDITNOTE',$row["GL_ACCOUNT"]);
                      ?>
                    </select></td>
                  <td width="25%" style="text-align:center" class="cls_td_GLAmount"><input id="txtGLAmount" name="txtGLAmount" class="validate[required] cls_GLAmount" type="text" style="width:100%;text-align:right" value="<?php echo ($row['GL_AMOUNT']==''?0:round($row['GL_AMOUNT'],2))?>"/></td>
                </tr>
                <?php
}

if(!$booAvailable)
{
?>
                <tr class="cls_tr_firstRow">
                  <td width="6%" style="text-align:center" ><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover removeRow" id="butDel" tabindex="24"/></td>
                  <td width="69%" class="cls_td_GL"><select name="cboLedgerAc" id="cboLedgerAc" class="clsLedgerAc validate[required]"  style="width:100%" >
                      <?php
							echo $obj_GLDetails_get->getGLCombo('CUSTOMER_CREDITNOTE',$row["GL_ACCOUNT"]);
                      ?>
                    </select></td>
                  <td width="25%" style="text-align:center" class="cls_td_GLAmount"><input id="txtGLAmount" name="txtGLAmount" class="validate[required] cls_GLAmount" type="text" style="width:100%;text-align:right" value="<?php echo ($row['GL_AMOUNT']==''?0:round($row['GL_AMOUNT'],2))?>"/></td>
                </tr>
                <?php
}
?>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2" style="text-align:center"><b>TOTAL</b></td>
                  <td colspan="3"><input type="text" style="width:100%;text-align:right" value="0" disabled="disabled" class="td_totalGLAlloAmount"/></td>
                </tr>
              </tfoot>
            </table></td>
            <td width="48%" valign="top"><table width="200" border="0" align="right" class="normalfnt" >
              <tr>
                <td width="81">Sub Total</td>
                <td width="19">:</td>
                <td width="100"><span class="normalfntMid">
                  <input name="txtSubTotal" type="text" disabled="disabled" id="txtSubTotal" value="0.0000" style="width:100px;text-align:right" />
                </span></td>
              </tr>
              <tr>
                <td>Tax Total</td>
                <td>:</td>
                <td><span class="normalfntMid">
                  <input name="txtTotTax" type="text" disabled="disabled" id="txtTotTax" value="0.0000" style="width:100px;text-align:right" />
                </span></td>
              </tr>
              <tr>
                <td>Grand Total</td>
                <td>:</td>
                <td><span class="normalfntMid">
                  <input name="txtGrandTotal" type="text" disabled="disabled" id="txtGrandTotal" value="0.0000" style="width:100px;text-align:right" />
                </span></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align="center"><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($form_permision['edit']==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');?><a class="button white medium" id="butReport" style="display:none">Report</a><a href="main.php" class="button white medium" id="butClose">Close</a></td>
      </tr>
      </table>
</div>
</div>
</form>
<?php
function GetHeaderDetails($serialYear,$serialNo)
{
	global $db;
	
	$sql = "SELECT
			  CONCAT(CIH.SERIAL_YEAR,'/',CIH.SERIAL_NO)		AS CONCAT_SERIAL_NO,
			  CIH.SERIAL_YEAR,
			  CIH.SERIAL_NO,
			  CIH.INVOICE_NO								AS INVOICE_NO,
			  CIH.INVOICED_DATE								AS INVOICED_DATE,
			  CIH.ORDER_NO									AS ORDER_NO,
			  CIH.ORDER_YEAR								AS ORDER_YEAR,
			  CONCAT(CIH.ORDER_YEAR,'/',CIH.ORDER_NO)		AS CONCAT_ORDER_NO,
			  OH.strCustomerPoNo    						AS CUSTOMER_PONO,
			  CIH.LEDGER_ID    								AS LEDGER_ID,
			  (SELECT SUB_U.strUserName 
			  FROM sys_users SUB_U 
			  WHERE SUB_U.intUserId = OH.intMarketer)  		AS MARKETER_ID,
			  OH.intCurrency								AS CURRENCY_ID,
			  CU.strInvoiceType								AS INVOICE_TYPE,
			  CU.intId										AS CUSTOMER_ID,
			  CIH.REMARKS									AS REMARKS,
    	 	  CU.strName									AS CUSTOMER_NAME
			FROM finance_customer_invoice_header CIH
			INNER JOIN trn_orderheader OH
    		  ON OH.intOrderNo = CIH.ORDER_NO
      		    AND OH.intOrderYear = CIH.ORDER_YEAR
			INNER JOIN mst_customer CU
			  ON CU.intId = OH.intCustomer
			WHERE CIH.SERIAL_YEAR = '$serialYear'
				AND CIH.SERIAL_NO = '$serialNo'";
				
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);

	$header_array["INVOICE_NO"]			= $row["INVOICE_NO"];
	$header_array["CONCAT_SERIAL_NO"]	= $row["CONCAT_SERIAL_NO"];
	$header_array["INVOICED_DATE"]		= $row["INVOICED_DATE"];
	$header_array["CUSTOMER_PONO"]		= $row["CUSTOMER_PONO"];
	$header_array["LEDGER_ID"]			= $row["LEDGER_ID"];
	$header_array["MARKETER_ID"]		= $row["MARKETER_ID"];
	$header_array["CURRENCY_ID"]		= $row["CURRENCY_ID"];
	$header_array["INVOICE_TYPE"]		= $row["INVOICE_TYPE"];
	$header_array["REMARKS"]			= $row["REMARKS"];
	$header_array["CUSTOMER_HTML"]		= "<option value=\"".$row["CUSTOMER_ID"]."\">".$row["CUSTOMER_NAME"]."</option>";
	$header_array["ORDER_HTML"]			= "<a href=\"?q=$ordReportId&orderNo=".$row["ORDER_NO"]."&orderYear=".$row["ORDER_YEAR"]."\" id=\"txtOrderNo\" target=\"rptBulkOrder.php\">".$row["CONCAT_ORDER_NO"]."</a>";
	
	
	return $header_array;
}

function GetGridDetails($serialYear,$serialNo)
{
	global $db;
	
	$sql = "SELECT 
			OD.strSalesOrderNo								AS SALES_ORDER_NO,
			OD.strGraphicNo									AS GRAPHIC_NO,
			OD.strStyleNo 									AS STYLE_NO,
			P.strName										AS PART,
			CIB.DISPATCHED_GOOD_QTY							AS DISPATCHED_GOOD_QTY,
			CID.QTY											AS INVOICE_QTY,	
			CID.PRICE 										AS INVOICE_PRICE,
			CID.VALUE 										AS INVOICE_VALUE,
			CID.TAX_CODE 									AS TAX_CODE_ID,
			CID.COST_CENTER 								AS COST_CENTER_ID,
			CID.TAX_VALUE									AS TAX_VALUE
			FROM finance_customer_invoice_details CID
			INNER JOIN finance_customer_invoice_header CIH ON CIH.SERIAL_YEAR = CID.SERIAL_YEAR AND CIH.SERIAL_NO = CID.SERIAL_NO
			INNER JOIN trn_orderdetails OD ON OD.intOrderYear = CIH.ORDER_YEAR AND OD.intOrderNo = CIH.ORDER_NO AND OD.intSalesOrderId = CID.SALES_ORDER_ID
			INNER JOIN mst_part P ON P.intId = OD.intPart
			INNER JOIN finance_customer_invoice_balance CIB ON CIB.ORDER_YEAR = CIH.ORDER_YEAR AND CIB.ORDER_NO = CIH.ORDER_NO AND OD.intSalesOrderId = CIB.SALES_ORDER_ID
			WHERE CID.SERIAL_YEAR = '$serialYear' AND CID.SERIAL_NO = '$serialNo'";
	return $db->RunQuery($sql);
}

function GetTax($loop,$id)
{
	global $db;
	$string 	= "";
	
	$sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strCode
			FROM
			mst_financetaxgroup
			ORDER BY strCode";
	$result = $db->RunQuery($sql);
		$string .= "<select style=\"width:90px\" id=\"cboTax$loop\" class=\"cls_cbo_tax\">";
		$string .= "<option value=\""."NULL"."\">".""."</option>";
	while($row = mysqli_fetch_array($result))
	{
		if($id==$row['intId'])
			$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";
		else
			$string .= "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
	}
		$string .= "</select>";
	return $string;
}

function GetCostCenter($loop,$id)
{
	global $db;
	$string 	= "";
	$sql = "SELECT
				intId,
				strName
			FROM mst_financedimension
			WHERE
				intStatus = 1
			ORDER BY strName";
	$result = $db->RunQuery($sql);
		$string .= "<select style=\"width:90px\" class=\"validate[required]\" id=\"cboCostCenter$loop\">";
		$string .= "<option value=\"".""."\">".""."</option>";
	while($row = mysqli_fetch_array($result))
	{
		if($id==$row['intId'])
			$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
		else
			$string .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
	}
		$string .= "</select>";
	return $string;
}

function GetGL_HTML($loop,$id)
{
	global $db;
	global $session_companyId;
	
	$string 	= "";
	$sql = "SELECT intId , strName,intDefaultFocus FROM mst_financecustomeritem WHERE intStatus = 1 ORDER BY strName";
	$result = $db->RunQuery($sql);
		$string .= "<select style=\"width:90px\" id=\"cboGL$id\" class=\"validate[required]\">";
		$string .= "<option value=\"".""."\">".""."</option>";
	while($row = mysqli_fetch_array($result))
	{
		if($id==$row['intId'])
			$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
		else
			$string .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
	}
		$string .= "</select>";
	return $string;
	
}

function Get_GL_HTML($gl_id)
{
	global $db;
	
	  $sql = "SELECT
			mst_financechartofaccounts.intId,
			mst_financechartofaccounts.strCode,
			mst_financechartofaccounts.strName
			FROM
			mst_financechartofaccounts
			WHERE
			mst_financechartofaccounts.strType = 'Posting' AND
			mst_financechartofaccounts.intStatus =  '1' AND
			(mst_financechartofaccounts.intFinancialTypeId =  '6' OR mst_financechartofaccounts.intFinancialTypeId =  '5' OR mst_financechartofaccounts.intFinancialTypeId =  '8')
			order by strCode";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		if($row['intId'] == $gl_id)
			echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']." - ".$row['strName']."</option>";
		else
			echo "<option value=\"".$row['intId']."\">".$row['strCode']." - ".$row['strName']."</option>";
	}
}
?>
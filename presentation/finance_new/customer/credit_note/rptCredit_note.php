<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once "class/finance/cls_common_get.php";
require_once "class/finance/cls_convert_amount_to_word.php";

$obj_fin_com				= new Cls_Common_Get($db); 
$obj_convert_amount_to_word	= new Cls_Convert_Amount_To_Word($db);

$locationId 		= $_SESSION['CompanyID'];
$serialNo			= $_REQUEST["SerialNo"];
$serialYear			= $_REQUEST["SerialYear"];

$header_array 		= GetHeaderDetails($serialYear,$serialNo);
$detail_result 		= GetGridDetails($serialYear,$serialNo);

$companyId			= $header_array["COMPANY_ID"];
?>
<head>
<title>Customer Credit Note</title>
</head>
<body>
<style type="text/css">
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}
</style>
<form id="frmSalesInvoice" name="frmSalesInvoice" method="post">
  <table width="900" align="center">
    <tr>
      <td><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
      <td class="reportHeader" align="center">Customer Credit Note</td>
    </tr>
    <tr>
      <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td>Customer Name</td>
            <td>:</td>
            <td><?php echo $header_array["CUSTOMER_NAME"]?></td>
            <td>Credit Note No</td>
            <td>:</td>
            <td><?php echo $obj_fin_com->getSerialNo($header_array["SERIAL_NO"],$header_array["SERIAL_DATE"],$companyId,'RunQuery')//echo $header_array["ADVANCE_NO"]; ?></td>
          </tr>
          <tr>
            <td>Customer Address</td>
            <td>:</td>
            <td><?php echo $header_array["CUSTOMER_ADDRESS"]?></td>
            <td>Invoice No</td>
            <td>:</td>
            <td><?php echo $header_array["INVOICE_NO"]?></td>
          </tr>
          <tr>
            <td width="14%">Customer PONo</td>
            <td width="1%">:</td>
            <td width="47%"><?php echo $header_array["CUSTOMER_PONO"]?></td>
            <td width="13%">Invoiced Date</td>
            <td width="1%">:</td>
            <td width="24%"><?php echo $header_array["CREDIT_DATE"]; ?></td>
          </tr>
          <tr>
            <td>Attention By</td>
            <td>:</td>
            <td><?php echo $header_array["CONTACT_NAME"]?></td>
            <td>Order No</td>
            <td>:</td>
            <td><?php echo $header_array["ORDER_NO"]?></td>
          </tr>
          <tr>
            <td>Customer VAT No</td>
            <td>:</td>
            <td><?php echo $header_array["CUSTOMER_VAT_NO"]?></td>
            <td>Customer VAT No</td>
            <td>:</td>
            <td><?php echo $companyVATNo ?></td>
          </tr>
          <tr>
            <td>Customer SVAT No</td>
            <td>:</td>
            <td><?php echo $header_array["CUSTOMER_SVAT_NO"]?></td>
            <td>Customer SVAT No</td>
            <td>:</td>
            <td><?php echo $companySVATNo;?></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td align="right"><a class="button green small no-print" target="<?php echo $header_array["CREDIT_NOTE_REPORT_NAME"]?>" href="<?php echo "presentation/finance_new/customer/credit_note/".$header_array["CREDIT_NOTE_REPORT_NAME"]."?SerialNo=$serialNo&SerialYear=$serialYear"?>">Click here to print invoice</a></td>
    </tr>
    <tr>
      <td ><table width="100%%" border="0" class="rptBordered" id="tblMain">
          <thead>
            <tr>
              <th width="3%">&nbsp;</th>
              <th width="18%">Sales Order No</th>
              <th width="17%">Graphic No</th>
              <th width="15%">Style No</th>
              <th width="20%">Placement</th>
              <th width="10%"> Qty</th>
              <th width="8%">Price</th>
              <th width="9%">Value</th>
            </tr>
          </thead>
          <tbody>
            <?php
$total_value	= 0;
$total_tax		= 0;
$i				= 0;
while($row = mysqli_fetch_array($detail_result))
{ 
?>
            <tr>
              <td class="cls_td_check" align="center"><?php echo ++$i;?>.</td>
              <td class="cls_td_salesOrderNo"><?php echo $row["SALES_ORDER_NO"]?></td>
              <td width="17%"><?php echo $row["GRAPHIC_NO"]?></td>
              <td width="15%"><?php echo $row["STYLE_NO"]?></td>
              <td width="20%"><?php echo $row["PART"]?></td>
              <td class="cls_td_invoice cls_Subtract" style="text-align:right"><?php echo $row["INVOICE_QTY"]?></td>
              <td style="text-align:right"><?php echo $row["INVOICE_PRICE"]?></td>
              <td class="cls_td_value" style="text-align:right"><?php echo $row["INVOICE_VALUE"]?></td>
            </tr>
            <?php
	$total_value 	+= $row["INVOICE_VALUE"];
	$total_tax 		+= $row["TAX_VALUE"];
}
	$grandTotal = round($total_value,4) - round($total_tax,4)
?>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td class="normalfnt"><b><?php echo $obj_convert_amount_to_word->Convert_Amount($grandTotal,$header_array["CURRENCY_CODE"]) ?></b></td>
    </tr>
    <tr>
      <td ><table width="200" border="0" align="right" class="normalfnt">
          <tr>
            <td width="106">Sub Total</td>
            <td width="10">:</td>
            <td width="70" align="right"><?php echo number_format($total_value,4)?></td>
          </tr>
          <tr>
            <td>Tax Total</td>
            <td>:</td>
            <td align="right"><?php echo number_format($total_tax,4)?></td>
          </tr>
          <tr>
            <td>Grand Total</td>
            <td>:</td>
            <td align="right"><?php echo number_format(round($total_value,4) - round($total_tax,4),4)?></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
</body>
<?php
function GetHeaderDetails($serialYear,$serialNo)
{
	global $db;
	
	$sql = "SELECT
				CCNH.CREDIT_NO								AS SERIAL_NO,
				CCNH.COMPANY_ID								AS COMPANY_ID,
				CCNH.CREDIT_DATE							AS SERIAL_DATE,
				CONCAT(CCNH.CREDIT_NO,'/',CCNH.CREDIT_YEAR) AS CREDIT_SERIAL_NO,  
				CU.strName									AS CUSTOMER_NAME,
				CU.strAddress								AS CUSTOMER_ADDRESS,
				CU.strContactPerson							AS CONTACT_NAME,
				CU.strVatNo									AS CUSTOMER_VAT_NO,
				CU.strSVatNo								AS CUSTOMER_SVAT_NO,
				OH.strCustomerPoNo    						AS CUSTOMER_PONO,
				CIH.INVOICE_NO								AS INVOICE_NO,
				CCNH.CREDIT_DATE							AS CREDIT_DATE,
			 	CONCAT(CCNH.ORDER_YEAR,'/',CCNH.ORDER_NO)	AS ORDER_NO,
				CURR.strCode								AS CURRENCY_CODE,
				IT.CREDIT_NOTE_REPORT_NAME				AS CREDIT_NOTE_REPORT_NAME
			FROM finance_customer_credit_note_header CCNH
			INNER JOIN trn_orderheader OH ON OH.intOrderNo = CCNH.ORDER_NO AND OH.intOrderYear = CCNH.ORDER_YEAR
			INNER JOIN mst_customer CU ON CU.intId = CCNH.CUSTOMER_ID
			INNER JOIN finance_customer_invoice_header CIH ON CIH.SERIAL_NO = CCNH.INVOICE_NO AND CIH.SERIAL_YEAR = CCNH.INVOICE_YEAR
			INNER JOIN mst_financecurrency CURR ON CURR.intId = CCNH.CURRENCY_ID
			LEFT JOIN mst_invoicetype IT ON IT.intId = CU.strInvoiceType
			WHERE CCNH.CREDIT_YEAR = '$serialYear' AND CCNH.CREDIT_NO = '$serialNo'";
				
	$result = $db->RunQuery($sql);
	$header_array = mysqli_fetch_array($result);
	return $header_array;
}

function GetGridDetails($serialYear,$serialNo)
{
	global $db;
	
	$sql = "SELECT
			OD.strSalesOrderNo								AS SALES_ORDER_NO,
			OD.strGraphicNo									AS GRAPHIC_NO,
			OD.strStyleNo 									AS STYLE_NO,
			P.strName										AS PART,
			CCND.QTY										AS INVOICE_QTY,	
			CCND.PRICE 										AS INVOICE_PRICE,
			CCND.VALUE 										AS INVOICE_VALUE,
			CCND.TAX_VALUE 									AS TAX_VALUE,
			CCND.TAX_ID 									AS TAX_CODE_ID,
			CCND.COST_CENTER_ID 							AS COST_CENTER_ID
			FROM finance_customer_credit_note_header CCNH
			INNER JOIN finance_customer_credit_note_detail CCND ON CCND.CREDIT_NO = CCNH.CREDIT_NO AND CCND.CREDIT_YEAR = CCNH.CREDIT_YEAR
			INNER JOIN trn_orderdetails OD ON OD.intOrderYear = CCNH.ORDER_YEAR AND OD.intOrderNo = CCNH.ORDER_NO AND OD.intSalesOrderId = CCND.SALES_ORDER_ID
			INNER JOIN mst_part P ON P.intId = OD.intPart
			WHERE CCNH.CREDIT_YEAR = '$serialYear' AND CCNH.CREDIT_NO = '$serialNo'";
	return $db->RunQuery($sql);
}
?>
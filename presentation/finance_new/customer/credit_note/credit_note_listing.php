<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$invReportId		= 894;
$ordReportId		= 896;
$reportId			= 913;

$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'CONCAT_CREDIT_NO'=>"CONCAT(CNH.CREDIT_NO,'/',CNH.CREDIT_YEAR)",
				'CREDIT_DATE'=>'CNH.CREDIT_DATE',
				'INVOICE_NO'=>'CIH.INVOICE_NO',
				'CONCAT_ORDER_NO'=>"CONCAT(CNH.ORDER_NO,'/',CNH.ORDER_YEAR)",
				'CUSTOMER_PONO'=>'OH.strCustomerPoNo',
				'CUSTOMER_NAME'=>'C.strName',
				'REMARKS'=>'CNH.REMARKS',
				'CURRENCY_NAME'=>'CU.strCode'
				);
				
foreach($arr as $k=>$v)
{
	if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND CNH.CREDIT_DATE = '".date('Y-m-d')."'";
	
// }

$sql = "SELECT SUB_1.* FROM
			(SELECT
			  CONCAT(CNH.CREDIT_NO,'/',CNH.CREDIT_YEAR )	 	AS CONCAT_CREDIT_NO,
			  CNH.CREDIT_NO										AS CREDIT_NO,
			  CNH.CREDIT_YEAR									AS CREDIT_YEAR,
			  CNH.INVOICE_NO 									AS INVOICE_SERIAL_NO,
			  CNH.INVOICE_YEAR 									AS INVOICE_SERIAL_YEAR,
			  CIH.INVOICE_NO								 	AS INVOICE_NO,
			  CNH.ORDER_YEAR 									AS ORDER_YEAR,
			  CNH.ORDER_NO   									AS ORDER_NO,
			  CONCAT(CNH.ORDER_NO,'/',CNH.ORDER_YEAR)			AS CONCAT_ORDER_NO,
			  OH.strCustomerPoNo								AS CUSTOMER_PONO,		 
			  C.strName      									AS CUSTOMER_NAME,
			  CU.strCode										AS CURRENCY_NAME,			 
			  'View'											AS VIEW,
			  'Report'											AS REPORT,
			  CNH.REMARKS										AS REMARKS,
			  CNH.CREDIT_DATE									AS CREDIT_DATE,
			  (SELECT ROUND(SUM(SUB_CND.VALUE),4)						 		
			   FROM finance_customer_credit_note_detail SUB_CND
				WHERE SUB_CND.CREDIT_NO = CNH.CREDIT_NO
				  AND SUB_CND.CREDIT_YEAR = CNH.CREDIT_YEAR)		AS INVOICE_VALUE
			FROM finance_customer_credit_note_header CNH			  
			  INNER JOIN mst_customer C
				ON C.intId = CNH.CUSTOMER_ID
			  INNER JOIN trn_orderheader OH
				ON OH.intOrderNo = CNH.ORDER_NO
				  AND OH.intOrderYear = CNH.ORDER_YEAR
			  INNER JOIN finance_customer_invoice_header CIH
			  	ON CIH.SERIAL_NO = CNH.INVOICE_NO 
				AND CIH.SERIAL_YEAR = CNH.INVOICE_YEAR
			  INNER JOIN mst_financecurrency CU 
			    ON CU.intId = CNH.CURRENCY_ID
			WHERE CIH.COMPANY_ID = $session_companyId
			$where_string
		)  
		AS SUB_1 WHERE 1=1";

$jq 	= new jqgrid('',$db);	
$col 	= array();
$cols 	= array();

$col["title"] 			= "Serial No";
$col["name"] 			= "CREDIT_NO";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Serial Year";
$col["name"] 			= "CREDIT_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;


$col["title"] 			= "Date";
$col["name"] 			= "CREDIT_DATE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;


$col["title"] 			= "Credit No";
$col["name"] 			= "CONCAT_CREDIT_NO";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
//$col['link']			= 'credit_note.php?SerialNo={CREDIT_NO}&SerialYear={CREDIT_YEAR}';
//$col["linkoptions"] 	= "target='credit_note.php'";
$cols[] 				= $col;	
$col					= NULL;


$col["title"] 			= "Serial No";
$col["name"] 			= "INVOICE_SERIAL_NO";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Serial Year";
$col["name"] 			= "INVOICE_SERIAL_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Invoice No";
$col["name"] 			= "INVOICE_NO";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col['link']			= '?q='.$invReportId.'&SerialNo={INVOICE_SERIAL_NO}&SerialYear={INVOICE_SERIAL_YEAR}';
$col["linkoptions"] 	= "target='rptInvoice.php'";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Order No";
$col["name"] 			= "ORDER_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Order No";
$col["name"] 			= "ORDER_NO";
$col["width"] 			= "1";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Order No";
$col["name"] 			= "CONCAT_ORDER_NO";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col['link']			= '?q='.$ordReportId.'&orderNo={ORDER_NO}&orderYear={ORDER_YEAR}';
$col["linkoptions"] 	= "target='credit_note.php'";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Customer PONo";
$col["name"] 			= "CUSTOMER_PONO";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Customer";
$col["name"] 			= "CUSTOMER_NAME";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Remarks";
$col["name"] 			= "REMARKS";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "CURRENCY_NAME";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Value";
$col["name"] 			= "INVOICE_VALUE";
$col["width"] 			= "2";
$col["align"] 			= "right";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Report";
$col["name"] 			= "REPORT";
$col["width"] 			= "1";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= '?q='.$reportId.'&SerialNo={CREDIT_NO}&SerialYear={CREDIT_YEAR}';
$col["linkoptions"] 	= "target='rptInvoice.php'";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Customer Credit Note Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'CREDIT_YEAR,CREDIT_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "ASC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Customer Credit Note Listing</title>
<?php
echo $out;
<?php 
session_start();
//BEGIN - INCLUDE FILES {
include  "../../../../dataAccess/Connector.php";
include  "../../../../class/finance/customer/credit_note/cls_credit_note_get.php";
include  "../../../../class/finance/customer/credit_note/cls_credit_note_set.php";
include  "../../../../class/finance/cls_calculate_tax.php";
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {
$obj_credit_note_get	= new Cls_Credit_Note_Get($db);
$obj_credit_note_set	= new Cls_Credit_Note_Set($db);
$obj_calculate_tax		= new Cls_Calculate_Tax($db);
//END 	- CREATE OBJECTS }
$requestType		= $_REQUEST["RequestType"];

if($requestType == "URLLoadInvoiceNo")
{
	$customerId		= $_REQUEST["CustomerId"];
	echo $obj_credit_note_get->LoadInvoiceNo($customerId);
}
elseif($requestType == "URLLoadDetails")
{
	$invoice_array	= explode('/',$_REQUEST["InvoiceNo"]);
	$invoiceNo		= $invoice_array[0];
	$invoiceYear	= $invoice_array[1];
	echo $obj_credit_note_get->LoadDetails($invoiceNo,$invoiceYear);
}
elseif($requestType == "URLSave")
{
	$customerId		= $_REQUEST["CustomerId"];
	$invoiceNo		= $_REQUEST["InvoiceNo"];
	$orderNo		= $_REQUEST["OrderNo"];
	$currencyId		= $_REQUEST["CurrencyId"];
	$ledgerId		= (int)$_REQUEST["LedgerId"];
	$date			= $_REQUEST["Date"];
	$remarks		= $_REQUEST["Remarks"];
	$invoiceType	= $_REQUEST["InvoiceType"];
	$subTotal		= $_REQUEST["SubTotal"];
	$grandTotal		= $_REQUEST["GrandTotal"];
	$details		= json_decode($_REQUEST["Grid"],true);
	$GLGrid			= json_decode($_REQUEST["GLGrid"],true);
	
	$response 		= $obj_credit_note_get->SaveValidation($invoiceNo,$currencyId,$customerId,$grandTotal);
	if($response["type"]=='fail')
	{	
		echo json_encode($response);
		return;
	}
	
	echo $obj_credit_note_set->Save($customerId,$invoiceNo,$orderNo,$currencyId,$ledgerId,$date,$remarks,$details,$GLGrid,$invoiceType,$subTotal,$grandTotal);
}
elseif($requestType=="URLCalculateTax")
{
	$taxId				= $_REQUEST["TaxId"];
	$value				= $_REQUEST["Value"];
	echo $obj_calculate_tax->CalculateTax($taxId,$value);
	
}
?>
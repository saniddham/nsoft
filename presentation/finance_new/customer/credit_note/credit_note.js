var basePath	= "presentation/finance_new/customer/credit_note/";
var invReportId	= 894;
var reportId	= 913;
var menuId		= 704;
$(document).ready(function(e) {
	
	CaculateTotalValue();
    $('#frmCreditNote #cboCustomer').focus();
	$('#frmCreditNote #cboCustomer').die('change').live('change',LoadInvoiceNo);
	$('#frmCreditNote #cboInvoiceNo').die('change').live('change',LoadDetails);
	$('#frmCreditNote #tblMain .cls_check').die('click').live('click',CaculateTotalValue);
	$('#frmCreditNote .cls_cbo_tax').die('change').live('change',CalculateTax);
	$('#frmCreditNote #butNew').die('click').live('click',New);
	$('#frmCreditNote #hylink_invoice').die('click').live('click',ViewInvoiceReport);
	$('#frmCreditNote #tblMain .cls_chk_checkAll').die('click').live('click',CheckAll);
	$('#frmCreditNote #butReport').die('click').live('click',ViewReport);
	$('#frmCreditNote #tblGL #butAddNewGL').die('click').live('click',AddNewGL);
	$('#frmCreditNote #tblGL .cls_GLAmount').die('keyup').live('keyup',function(){ CalculateGLTotals(); });
	$('#frmCreditNote #butSave').die('click').live('click',Save);
	
	$('#frmCreditNote #tblMain input').die('keyup').live('keyup',function(e){
		if($(this).val()=="" || isNaN($(this).val())){
			$(this).val(0);
			$(this).select();
		}
	});
	
	$('#frmCreditNote #tblGL input').die('keyup').live('keyup',function(e){			
		if($(this).val()=="" || isNaN($(this).val())){
			$(this).val(0);
			$(this).select();
		}
	});
	
	$('#frmCreditNote #tblMain .cls_calculate').die('keyup').live('keyup',function(){
		var value		= 0;
		var qty			= $(this).parent().parent().find('.cls_td_invoice').children().val();
		var price		= $(this).parent().parent().find('.cls_td_price').html();
			value		= RoundNumber(qty * price,4);
		
		$(this).parent().parent().find('.cls_td_value').html(value);
		CaculateTotalValue()
	});
	
	$('#frmCreditNote #tblGL .removeRow').die('click').live('click',function(){
		if($(this).parent().parent().attr('class')=="")
		 	$(this).parent().parent().remove();
		CalculateGLTotals();
	});
	
	if(customerIdAL!='')
		LoadAutoData(customerIdAL);
	
});

function ViewInvoiceReport()
{
	var no	= $('#frmCreditNote #cboInvoiceNo').val();
	if(no=="")
		return;
	var no_array	= no.split('/');
	var url  = "?q="+invReportId;
		url += "&SerialNo="+no_array[0];
	    url += "&SerialYear="+no_array[1];
	window.open(url,'rptinvoice.php');
}

function New()
{
	window.location.href = '?q='+menuId;
}

function CaculateTotalValue()
{ 
	var totalValue		= 0;
	var grandTotal		= 0;
	var taxValue		= 0;
	
	$('#frmCreditNote #tblMain .cls_td_value').each(function(){
		if($(this).parent().find('.cls_td_check').children().is(':checked'))
		{
			totalValue += parseFloat($(this).html());
			taxValue   += parseFloat($(this).parent().find('.cls_td_tax').attr('id'));
		}
	});
	taxValue	= parseFloat(RoundNumber(taxValue,4));
	totalValue	= parseFloat(RoundNumber(totalValue,4));
	grandTotal	= totalValue + taxValue;
	
	$('#frmCreditNote #txtTotTax').val(RoundNumber(taxValue,4));	
	$('#frmCreditNote #txtSubTotal').val(RoundNumber(totalValue,4));	
	$('#frmCreditNote #txtGrandTotal').val(RoundNumber(grandTotal,4));
}

function LoadInvoiceNo()
{
	showWaiting();
	var url 	= basePath+"credit_note_db.php?RequestType=URLLoadInvoiceNo";
	var data 	= "CustomerId="+$(this).val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		$('#frmCreditNote #cboInvoiceNo').html(json.HTML);
		$('#frmCreditNote #cboInvoiceType').val(json.INVOICE_TYPE);
		$('#frmCreditNote #cboInvoiceNo').focus();
	}
	});	
	hideWaiting();	
}

function LoadDetails()
{
	showWaiting();
	$("#frmCreditNote #tblMain tr:gt(0)").remove();
	
	var url 	= basePath+"credit_note_db.php?RequestType=URLLoadDetails";
	var data 	= "InvoiceNo="+$(this).val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		$('#frmCreditNote #cboCurrency').val(json.CURRENCY_ID);
		$('#frmCreditNote #cboLedgerAccount').val(json.LEDGER_ID);
		$('#frmCreditNote #td_order').html(json.ORDER_HTML);
		$('#frmCreditNote #txtInvoiceBalance').val(json.INVOICE_BALANCE);
		GetCommonExchangeRate();
		
		if(json.GRID == null)
		{
			hideWaiting();
			return;
		}
		
		var length 		= json.GRID.length;
		var arrayGrid 	= json.GRID;
		
		for(var i=0;i<length;i++)
		{
			CreateGrid(arrayGrid[i]['SALES_ORDER_ID'],arrayGrid[i]['SALES_ORDER_NO'],arrayGrid[i]['GRAPHIC_NO'],arrayGrid[i]['STYLE_NO'],arrayGrid[i]['PART'],arrayGrid[i]['QTY'],arrayGrid[i]['PRICE'],arrayGrid[i]['VALUE'],arrayGrid[i]['TAX_HTML'],arrayGrid[i]['COSTCENTER'],arrayGrid[i]['GL_HTML']);
		}
		CaculateTotalValue();
		
	},
	error:function(xhr,status)
	{
		hideWaiting();
	}
	});	
	hideWaiting();
}

function CreateGrid(salesOrderId,salesOrderNo,graphicNo,styleNo,part,qty,price,value,taxHTML,costCenter,GLHTML)
{
	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);		
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'cls_td_check';
	cell.innerHTML  = "<input type=\"checkbox\" checked=\"checked\" class=\"cls_check\">";
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'cls_td_salesOrderNo';
	cell.id			= salesOrderId;
	cell.innerHTML 	= salesOrderNo;
	
	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:left');
	cell.innerHTML 	= graphicNo;
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:left');
	cell.innerHTML 	= styleNo;
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:left');
	cell.innerHTML 	= part;

	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'cls_td_invoice';
	cell.id			= qty;
	cell.innerHTML 	= "<input type=\"textbox\" style=\"width:80px;text-align:right\" value=\""+qty+"\" class=\"cls_calculate\">";
	
	var cell 		= row.insertCell(6);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'cls_td_price';
	cell.innerHTML 	= price;
	
	var cell 		= row.insertCell(7);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'cls_td_value';
	cell.innerHTML 	= value;
	
	var cell 		= row.insertCell(8);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'cls_td_tax';
	cell.id			= 0;
	cell.innerHTML 	= taxHTML;
	
	var cell 		= row.insertCell(9);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'cls_td_costCenter';
	cell.innerHTML 	= costCenter;
	
	var cell 		= row.insertCell(10);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'cls_td_GL';
	cell.innerHTML 	= GLHTML;
}

function Save()
{
	if(!IsProcessMonthLocked_date($('#frmCreditNote #txtDate').val()))
		return;
		
	showWaiting();
	
	if(!Validate_Save()){
		hideWaiting();
		return;
	}

	var arr		= "[";
	$('#tblMain .cls_td_check').each(function(){
		if($(this).parent().find('.cls_td_check').children().is(':checked'))
		{
			arr += "{";
			arr += '"SalesOrderId":"'+$(this).parent().find('.cls_td_salesOrderNo').attr('id')+'",';
			arr += '"Qty":"'+$(this).parent().find('.cls_td_invoice').children().val()+'",';		
			arr += '"Price":"'+$(this).parent().find('.cls_td_price').html()+'",';
			arr += '"Tax":"'+$(this).parent().find('.cls_td_tax').children().val()+'",';
			arr += '"CostCenter":"'+$(this).parent().find('.cls_td_costCenter').children().val()+'",';
			arr += '"TaxValue":"'+$(this).parent().find('.cls_td_tax').attr('id')+'",';
			arr += '"GL_ID":"'+$(this).parent().find('.cls_td_GL').children().val()+'"';
			arr +=  '},';
		}
	});

	arr 		 = arr.substr(0,arr.length-1);
	arr 		+= " ]";

	var gl_array	= "[";
	$('#frmCreditNote #tblGL .cls_GLAmount').each(function(){
			gl_array += "{";
			gl_array += '"GL_ID":"'+$(this).parent().parent().find('.cls_td_GL').children().val()+'",' ;
			gl_array += '"GL_Amount":"'+$(this).val()+'"';
			gl_array +=  '},';
	});

	gl_array 		 = gl_array.substr(0,gl_array.length-1);
	gl_array 		+= " ]";

	var url 	= basePath+"credit_note_db.php?RequestType=URLSave";
	var data 	= "CustomerId="+$('#frmCreditNote #cboCustomer').val();
		data   += "&InvoiceNo="+$('#frmCreditNote #cboInvoiceNo').val();
		data   += "&OrderNo="+$('#frmCreditNote #txtOrderNo').html();
		data   += "&CurrencyId="+$('#frmCreditNote #cboCurrency').val();
		data   += "&LedgerId="+$('#frmCreditNote #cboLedgerAccount').val();
		data   += "&Date="+$('#frmCreditNote #txtDate').val();
		data   += "&Remarks="+URLEncode($('#frmCreditNote #txtRemarks').val());
		data   += "&InvoiceType="+$('#frmCreditNote #cboInvoiceType').val();
		data   += "&SubTotal="+$('#frmCreditNote #txtSubTotal').val();
		data   += "&GrandTotal="+$('#frmCreditNote #txtGrandTotal').val();
		data   += "&Grid="+arr;
		data   += "&GLGrid="+gl_array;
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
		{
			if(json.type=='pass')
			{
				$('#frmCreditNote #butSave').validationEngine('showPrompt', json.msg,json.type );
				$('#frmCreditNote #txtCreditNo').val(json.credit_no);
				$('#frmCreditNote #txtCreditYear').val(json.credit_year);			
				$('#frmCreditNote #butReport').show('slow');
			}
			else
			{
				$('#frmCreditNote #butSave').validationEngine('showPrompt', json.msg,json.type);
				$('#frmCreditNote #butReport').hide('slow');
			}
		},
	error:function(xhr,status)
		{
			$('#frmCreditNote #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
		}
	});
	hideWaiting();
}

function Validate_Save()
{
	var booAvailable	= false;
	var subTotal		= parseFloat($('#frmCreditNote #txtSubTotal').val());
	var grandTotal		= parseFloat($('#frmCreditNote #txtGrandTotal').val());
	var invoiceBalance	= parseFloat($('#frmCreditNote #txtInvoiceBalance').val());
	var total_gl_amount	= parseFloat($('#frmCreditNote #tblGL .td_totalGLAlloAmount').val());
	
	if(!$('#frmCreditNote').validationEngine('validate')){
		return false;
	}
	
	if($('#frmCreditNote #txtCreditNo').val()!="")
	{
		alert("Credit Note already saved.");
		return false;
	}
	
	if(subTotal>invoiceBalance)
	{
		alert("'Credit Note Sub Total' cannot be greater than 'Invoice Balance Amount'.");
		return false;
	}
	
	$('#tblMain .cls_td_check').each(function(){
		if($(this).parent().find('.cls_td_check').children().is(':checked'))
		{
			booAvailable	= true;
		}
	});
	
	if(!booAvailable)
	{
		alert("No details appear to proceed.");
		return false;
	}
	return true;
}

function CalculateTax()
{
	var obj		= $(this);
	var url 	= basePath+"credit_note_db.php?RequestType=URLCalculateTax";
	var data 	= "TaxId="+$(this).val();
		data   += "&Value="+parseFloat($(this).parent().parent().find('.cls_td_value').html());
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		obj.parent().attr('id',json.TaxValue);
		CaculateTotalValue();
	}
	});		
}

function CheckAll()
{
	if($(this).is(':checked'))
		$('#frmCreditNote #tblMain .cls_check').attr('checked','checked');
	else
		$('#frmCreditNote #tblMain .cls_check').removeAttr('checked');
		
	CaculateTotalValue();
}

function AddNewGL()
{	
	$('#frmCreditNote #tblGL tbody tr:last').after("<tr>"+$('#frmCreditNote #tblGL .cls_tr_firstRow').html()+"</tr>");
}

function CalculateGLTotals()
{
	var total	= 0;
	$('#frmCreditNote #tblGL .cls_td_GLAmount').each(function(){
    	 total	+= isNaN(parseFloat($(this).children().val()))? 0:parseFloat($(this).children().val());
    });
	total	= RoundNumber(total,4,true);
	$('#frmCreditNote #tblGL .td_totalGLAlloAmount').val(total);
}

function LoadAutoData(customerId)
{
	$('#frmCreditNote #cboCustomer').val(customerId);
	$('#frmCreditNote #cboCustomer').change();
}

function ViewReport()
{
	var url  = "?q="+reportId;	
	    url += "&SerialNo="+$('#frmCreditNote #txtCreditNo').val();
		url += "&SerialYear="+$('#frmCreditNote #txtCreditYear').val();;
	window.open(url,'rptCredit_note.php');
}
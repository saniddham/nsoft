var basePath	= "presentation/finance_new/customer/payment_received_settlement/";
var reportId	= 933;
var menuId		= 733;
$(document).ready(function() {
	
 	$('#frmPayment_received_settlement').validationEngine();
	
	$('#frmPayment_received_settlement #cboCustomer').die('change').live('change',LoadMainDetails);
	$('#frmPayment_received_settlement #cboCurrency').die('change').live('change',LoadMainDetails);
 	$('#frmPayment_received_settlement #butNew').die('click').live('click',New);
	$('#frmPayment_received_settlement #butSave').die('click').live('click',Save);
	$('#frmPayment_received_settlement #butReport').die('click').live('click',Report);
	$('#frmPayment_received_settlement #butCancel').die('click').live('click',Cancel);
	$('#frmPayment_received_settlement #tblGL .cal').die('keyup').live('keyup',function(e){
		calculateGLtotal(this);
	});
 	$('#frmPayment_received_settlement #tblMain .invAmmount').die('keyup').live('keyup',function(e){
		calculateInvoicetotal(this);
	});
  	
	$('#frmPayment_received_settlement #tblMain .cls_check_all').die('click').live('click',CheckAll);
	
	$('#frmPayment_received_settlement #tblMain .cls_check').die('click').live('click',function(){
		ValidateRowWhenCheck($(this));
	});

	
  });


function calculateGLtotal(obj){
	if($(obj).val()=="" || isNaN($(obj).val())){
		$(obj).val(0);
		$(obj).select();
	}
	CaculateGLTotalValue();
}

function calculateInvoicetotal(obj){
	if($(obj).val()=="" || isNaN($(obj).val())){
		$(obj).val(0);
		$(obj).select();
	}
	CaculateInvoiceTotalValue();
}
   
function Report()
{
	if($('#frmPayment_received_settlement #txtSerialNo').val()=="")
	{
		alert("No details available to view report.");
		return;
	}
 	var url  = "?q="+reportId+"&SerialNo="+$('#frmPayment_received_settlement #txtSerialNo').val();
	    url += "&SerialYear="+$('#frmPayment_received_settlement #txtSerialYear').val();
	window.open(url,'rptPayment_received_settlement.php');
}

function CaculateGLTotalValue()
{
 	var total		= 0;
	
	$('#frmPayment_received_settlement #tblGL .cal').each(function(){
			total += parseFloat($(this).val());
  	});
	total	= parseFloat(RoundNumber(total,4));
 	
	$('#frmPayment_received_settlement .glTotal').val(RoundNumber(total,4));	
 }

function CaculateInvoiceTotalValue()
{
 	var total		= 0;
	var r=0;
	var c=0;
	
	$('#frmPayment_received_settlement #tblMain .invAmmount').each(function(){
		r++;
		if($(this).parent().parent().find('.cls_check').is(':checked'))
		{
			c++;
 			total += parseFloat($(this).val());

		}
  	});
	if(c!=r){
		$('#frmPayment_received_settlement #tblMain .cls_check_all').removeAttr('checked');
	}
	else{
		$('#frmPayment_received_settlement #tblMain .cls_check_all').attr('checked','checked');
	}
	total	= parseFloat(RoundNumber(total,4));
 	$('#frmPayment_received_settlement #td_totalInvAmount').html(RoundNumber(total,4));
		
 }
 
function Save()
{
	if(!IsProcessMonthLocked_date($('#frmPayment_received_settlement #txtSettlementDate').val()))
		return;
		
	if(!Validation_Save())
		return;
	
	var detail_array		= "[";
	$('#frmPayment_received_settlement #tblMain .invAmmount').each(function(){
		if($(this).parent().parent().find('.invAmmount').val() > 0){
		if($(this).parent().parent().find('.cls_check').is(':checked'))
			{
				detail_array += "{";
				detail_array += '"OrderNo":"'+$(this).parent().parent().find('.cls_td_invoiceNo').attr('id')+'",';
				detail_array += '"InvoiceNo":"'+$(this).parent().parent().find('.cls_td_invoiceNo').children().attr('id')+'",';
				detail_array += '"PayAmount":"'+$(this).parent().parent().find('.cls_td_payingAmount').children().val()+'"';
				detail_array +=  '},';
			}
		}
	});
 	detail_array 		 = detail_array.substr(0,detail_array.length-1);
	detail_array 		+= " ]";
	
	
	var gl_Array		= "[";
	$('#frmPayment_received_settlement #tblGL .cal').each(function(){
				gl_Array += "{";
				gl_Array += '"GLAccount":"'+$(this).parent().parent().find('.cls_td_GL').attr('id')+'",';
				gl_Array += '"GLValue":"'+$(this).parent().parent().find('.cls_td_amount').children().val()+'",';
				gl_Array += '"GLCostCenter":"'+$(this).parent().parent().find('.cls_td_Cost').attr('id')+'"';
				gl_Array +=  '},';
	});
 		gl_Array 		 = gl_Array.substr(0,gl_Array.length-1);
		gl_Array 		+= " ]";

	var url 	= basePath+"payment_received_settlement_db.php?RequestType=URLSave";
	var data 	= "CustomerId="+$('#frmPayment_received_settlement #cboCustomer').val();
 		data   += "&serialNo="+$('#frmPayment_received_settlement #txtSerialNo').val();
 		data   += "&serialYear="+$('#frmPayment_received_settlement #txtSerialYear').val();
 		data   += "&CurrencyId="+$('#frmPayment_received_settlement #cboCurrency').val();
 		data   += "&ReceiptDate="+$('#frmPayment_received_settlement #txtSettlementDate').val();
 		data   += "&DetailArray="+detail_array;
		data   += "&GLArray="+gl_Array;
	
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
		{
			$('#frmPayment_received_settlement #butSave').validationEngine('showPrompt', json.msg,json.type);
			if(json.type == 'pass')
			{
				$('#frmPayment_received_settlement #txtSerialNo').val(json.ReceiptNo);
				$('#frmPayment_received_settlement #txtSerialYear').val(json.ReceiptYear);
				$('#frmPayment_received_settlement #butSave').hide('slow');
				$('#frmPayment_received_settlement #butCancel').show('slow');
			}
		},
	error:function(xhr,status)
		{
			$('#frmPayment_received_settlement #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
		}
	});
}

function Validation_Save()
{
	var booDetailAvailable 	= false;
	var totalPaying 	= parseFloat($('#frmPayment_received_settlement #td_totalInvAmount').html());
	var totalGL			= parseFloat($('#frmPayment_received_settlement .glTotal').val());
 	var creditAmount	= 0;
	var debitAmount		= 0;
	
	
	$('#tblMain .cls_td_check').each(function(){
		if($(this).parent().find('.cls_td_check').children().is(':checked'))
		{
			booDetailAvailable	= true;
		}
	});
	
	if(!booDetailAvailable)
	{
		$('#frmPayment_received_settlement #butSave').validationEngine('showPrompt','No details available to save.','fail');
		setTimeout("alertx('butSave')",2000);
		return false;
	}
	
	if(!$('#frmPayment_received_settlement').validationEngine('validate'))
		return false;
	
	if($('#frmPayment_received_settlement #txtSerialNo').val()!="")
	{
		alert("Settlement No already saved.");
		return false;
	}
	
	if(totalPaying<=0)
	{
		alert('Total invoice amount shold be more than zero.');
		return false;
	}
	
 	if(totalPaying != totalGL)
	{
		alert("Total invoice amount and total GL amount should be equal.")
		return false;
	}
	
	return true;
}

function alertx(but)
{
	$('#frmPayment_received_settlement #'+but).validationEngine('hide')	;
}

function New()
{
	window.location.href = '?q='+menuId;
}

function Cancel()
{
	if($('#frmPayment_received_settlement #txtSerialNo').val()=="")
	{
		return;
	}
	var x = confirm("Are you sure you want to cancel Payment Settlement No : "+$('#frmPayment_received_settlement #txtSerialNo').val()+"/"+$('#frmPayment_received_settlement #txtSerialYear').val()+" .");
		if(!x)return;
	
	var url 	= basePath+"payment_received_settlement_db.php?RequestType=URLCancel";
	var data 	= "serialNo="+$('#frmPayment_received_settlement #txtSerialNo').val();
	 	data   += "&serialYear="+$('#frmPayment_received_settlement #txtSerialYear').val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		$('#frmPayment_received_settlement #butCancel').validationEngine('showPrompt',json.msg,json.type );
	}
	});
	var t = setTimeout("HideValidation('butCancel')",2000);
}
 
function LoadMainDetails()
{
	ClearGLAccGrid();
	Clear_invoice_grid();

	loadMainDetailsAccounts();
	
	var url 	= basePath+"payment_received_settlement_db.php?RequestType=URLLoadMainDetails";
	var data 	= "CustomerId="+$('#frmPayment_received_settlement #cboCustomer').val();
 		data  += "&CurrencyId="+$('#frmPayment_received_settlement #cboCurrency').val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		var length 		= json.GRID.length;
		var data 		= json.GRID;

		for(var i=0;i<length;i++)
		{
 			CreateGrid_invoice(data[i]['INVOICE_HTML'],data[i]['INVOICED_DATE'],data[i]['INVOICE_REMARKS'],data[i]['INVOICED_VALUE'],data[i]['RECEIVED_VALUE'],data[i]['CREDIT_VALUE'],data[i]['ADVANCE_VALUE'],data[i]['TOBEPAID'],data[i]['INV_SETTLEMENT'],data[i]['ORDER_NO'],data[i]['UN_SETTLE'],data[i]['DEBIT_VALUE']);
		}
		//CreateGridLastRow();
  	}
	});
}
 
function loadMainDetailsAccounts()
{
 	
	var url 	= basePath+"payment_received_settlement_db.php?RequestType=loadMainDetailsAccounts";
	var data 	= "CustomerId="+$('#frmPayment_received_settlement #cboCustomer').val();
 		data  += "&CurrencyId="+$('#frmPayment_received_settlement #cboCurrency').val();
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		var length 		= json.GRID.length;
		var data 		= json.GRID;

		for(var i=0;i<length;i++)
		{
			var LEDGER_ID 				= data[i]['LEDGER_ID'];
			var CHART_OF_ACCOUNT_NAME 	= data[i]['CHART_OF_ACCOUNT_NAME'];
			var RECEIVED_VALUE 			= data[i]['RECEIVED_VALUE'];
			var COST_CENTER_ID 			= data[i]['COST_CENTER_ID'];
			var COST_CENTER 			= data[i]['COST_CENTER'];
			var balAmmount 				= data[i]['balAmmount'];
		
			CreateGrid_account(LEDGER_ID,CHART_OF_ACCOUNT_NAME,RECEIVED_VALUE,COST_CENTER_ID,COST_CENTER,balAmmount);
  		}
	}
	});
}

function CreateGrid_invoice(invoiceNo,invoicedDate,invoiceRemarks,invoiceValue,receivedValue,creditValue,advanceValue,tobePaid,settled,orderNo,unSettle,debitValue)
{
 	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);		
	row.setAttribute('bgcolor',unSettle);
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'cls_td_check';
	cell.innerHTML  = "<input type=\"checkbox\" checked=\"checked\" class=\"cls_check\">";

	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'cls_td_invoiceNo';
	cell.id			= orderNo;
	cell.innerHTML 	= invoiceNo;
	
	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'cls_td_invoiceRemarks';
	cell.innerHTML 	= invoiceRemarks;
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= invoicedDate;
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:right');
	cell.innerHTML 	= invoiceValue;
	
	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:right');
	cell.innerHTML 	= creditValue;
	
	var cell 		= row.insertCell(6);
	cell.setAttribute("style",'text-align:right');
	cell.innerHTML 	= debitValue;
	
	var cell 		= row.insertCell(7);
	cell.setAttribute("style",'text-align:right');
	cell.innerHTML 	= advanceValue;
	
	var cell 		= row.insertCell(8);
	cell.setAttribute("style",'text-align:right');
	cell.innerHTML 	= receivedValue;
	
	var cell 		= row.insertCell(9);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'cls_td_toBePaid';
	cell.innerHTML 	= settled;
	
	var cell 		= row.insertCell(10);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'cls_td_toBePaid';
	cell.innerHTML 	= tobePaid;

	var cell 		= row.insertCell(11);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'cls_td_payingAmount';
	cell.innerHTML 	= "<input type=\"textbox\" class=\"validate[custom[number],max["+tobePaid+"]] invAmmount\" id=\"am\" style=\"width:100%;text-align:right\" value=\""+0+"\" >";
}

function CreateGrid_account(LEDGER_ID,CHART_OF_ACCOUNT_NAME,RECEIVED_VALUE,COST_CENTER_ID,COST_CENTER,balAmmount)
{
	var tbl 		= document.getElementById('tblGL');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow-1);		
  	
	var cell 		= row.insertCell(0);
	cell.setAttribute("style",'text-align:left');
 	cell.id			= LEDGER_ID;
	cell.innerHTML 	= CHART_OF_ACCOUNT_NAME;
	cell.className	= 'cls_td_GL';
	
	var cell 		= row.insertCell(1);
 	cell.id			= COST_CENTER_ID;
	cell.innerHTML 	= COST_CENTER;
	cell.className	= 'cls_td_Cost';

	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:right');
	cell.innerHTML 	= balAmmount;
	cell.className	= 'cls_td_balAmount';
 
 	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'cls_td_amount';
	cell.innerHTML 	= "<input class=\"text-input validate[custom[number],max["+balAmmount+"]] cal\" type=\"text\" style=\"width:100%;text-align:right\" value=\"0\" name=\"txtUnsettleAdvance\" id=\"txtUnsettleAdvance\"/>";
}

function CreateGridLastRow()
{
	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);		
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'cls_td_check';
	cell.innerHTML  = "<input type=\"checkbox\" checked=\"checked\" class=\"cls_check\">";

	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:center;font-weight:bold');
	cell.innerHTML 	= "TOTAL";
	
	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:center;font-weight:bold');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:right;font-weight:bold');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:right;font-weight:bold');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:right;font-weight:bold');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(6);
	cell.setAttribute("style",'text-align:right;font-weight:bold');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(7);
	cell.setAttribute("style",'text-align:right;font-weight:bold');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(8);
	cell.setAttribute("style",'text-align:right;font-weight:bold');
	cell.innerHTML  = "&nbsp;";
	
	var cell 		= row.insertCell(9);
	cell.setAttribute("style",'text-align:right;font-weight:bold');
	cell.className	= 'cls_td_total_toBePaid';
	cell.innerHTML  = "&nbsp;";
}

function Clear_invoice_grid()
{
	$("#frmPayment_received_settlement #tblMain tr:gt(0)").remove();
	$('#frmPayment_received_settlement #tblMain .cls_th_checkAll').removeAttr('checked');
	
	$("#frmPayment_received_settlement #tblGL .cls_td_GL").children().val('');
	$("#frmPayment_received_settlement #tblGL .cls_td_GLAmount").children().val(0);
	$("#frmPayment_received_settlement #tblGL .cls_td_remarks").children().val('');
	$("#frmPayment_received_settlement #tblGL .cls_td_costCenter").children().val('');
	
	$('#frmPayment_received_settlement #tblGL .removeRow').each(function(){
		if($(this).parent().parent().attr('class')=="")
		 	$(this).parent().parent().remove();		
	});
	
 }
 
function ClearGLAccGrid()
{
	$('#frmPayment_received_settlement #tblGL .cal').each(function(){
 		 	$(this).parent().parent().remove();		
	});
	
 }

function CheckAll()
{
	if($(this).is(':checked'))
		$('#frmPayment_received_settlement #tblMain .cls_check').attr('checked','checked');
	else
		$('#frmPayment_received_settlement #tblMain .cls_check').removeAttr('checked');
		
	$('#frmPayment_received_settlement #tblMain .cls_check').each(function(){
		ValidateRowWhenCheck($(this));
	});
}

function ValidateRowWhenCheck(obj)
{
	if(obj.is(':checked'))
	{
		obj.parent().parent().find('.invAmmount').removeAttr('disabled');
	}
	else
	{
		obj.parent().parent().find('.invAmmount').attr('disabled','disabled');
	}
	CaculateInvoiceTotalValue();
}

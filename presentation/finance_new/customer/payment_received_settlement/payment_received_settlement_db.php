<?php
session_start();
ini_set('display_errors',0);
include_once  ("../../../../dataAccess/Connector.php");
 include_once ("../../../../class/finance/cls_get_gldetails.php");
include_once  "../../../../class/finance/customer/payment_receive/cls_payment_receive_get.php";
include_once  "../../../../class/finance/customer/payment_received_settlement/cls_payment_received_settlement_get.php";
include_once  "../../../../class/finance/customer/payment_received_settlement/cls_payment_received_settlement_set.php";

$obj_GLDetails_get		= new Cls_Get_GLDetails($db);
$obj_payment_receive_get= new Cls_Payment_Receive_Get($db);
$obj_fin_receivd_set	= new Cls_PaymentReceivedSettlement_Set($db);
$obj_fin_receivd_get	= new Cls_PaymentReceivedSettlement_Get($db);

$requestType			= $_REQUEST["RequestType"];

if($requestType=="URLLoadMainDetails")
{
	$customerId		= $_REQUEST["CustomerId"];
 	$currencyId  	= $_REQUEST["CurrencyId"];
	
	echo $obj_payment_receive_get->LoadMainDetails($customerId,$currencyId);
 	
}
else if($requestType=="loadMainDetailsAccounts")
{
	$customerId		= $_REQUEST["CustomerId"];
 	$currencyId  	= $_REQUEST["CurrencyId"];
	
 	echo $obj_fin_receivd_get->getGridDetails_accounts_details($customerId);
	
}
else if($requestType=="URLSave")
{
	
	$serialNo		= $_REQUEST["serialNo"];
	$serialYear		= $_REQUEST["serialYear"];
	$customerId		= $_REQUEST["CustomerId"];
 	$currencyId  	= $_REQUEST["CurrencyId"];
 	$receiptDate	= $_REQUEST["ReceiptDate"];
 	$detailArray	= json_decode($_REQUEST["DetailArray"],true);
	$glArray		= json_decode($_REQUEST["GLArray"],true);
  	
	$response			= $obj_fin_receivd_get->ValidateBeforeSave($serialYear,$serialNo);
	
	if($response["type"]=='false')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_fin_receivd_set->Save($customerId,$currencyId,$detailArray,$glArray,$receiptDate);
} 
else if($requestType=="URLCancel")
{
	$serialNo		= $_REQUEST["serialNo"];
	$serialYear		= $_REQUEST["serialYear"];
	
	$response		= $obj_fin_receivd_get->ValidateBeforeCancel($serialYear,$serialNo);
	
	if($response["type"]=='false')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_fin_receivd_set->Cancel($serialNo,$serialYear);
}
 
 
?>
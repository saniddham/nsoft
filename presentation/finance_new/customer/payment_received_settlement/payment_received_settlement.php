<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php 
include_once ("class/finance/cls_common_get.php");
include_once ("class/finance/customer/payment_received_settlement/cls_payment_received_settlement_get.php");
include_once "class/cls_commonErrorHandeling_get.php";
//include_once ("class/cls_commonFunctions_get.php");
//include 	  "include/javascript.html";

$session_locationId 	= $_SESSION["CompanyID"];
$session_companyId 		= $_SESSION["headCompanyId"];
$session_userId 		= $_SESSION["userId"];

$serialNo				= (!isset($_REQUEST["SerialNo"])?'':$_REQUEST["SerialNo"]);
$serialYear				= (!isset($_REQUEST["SerialYear"])?'':$_REQUEST["SerialYear"]);
 
$obj_fin_com_get		= new Cls_Common_Get($db);
$obj_fin_receivd_get	= new Cls_PaymentReceivedSettlement_Get($db);
$obj_commonErr			= new cls_commonErrorHandeling_get($db);
$obj_common_function_get= new cls_commonFunctions_get($db);

$header_array 			= $obj_fin_receivd_get->getHeaderDetails_array($serialYear,$serialNo,$customer);
$customer				= $header_array['CUTOMER_ID'];
$currency				= $header_array['CURRENCY_ID'];

$header_accounts_result = $obj_fin_receivd_get->getSavedGridDetails_accounts_result($serialYear,$serialNo,$customer,'','');
$details_invoice_result = $obj_fin_receivd_get->getSavedGridDetails_invoice_result($serialYear,$serialNo,$customer,$currency);

$permision_save			= $obj_fin_receivd_get->get_permision_save($serialYear,$serialNo,$session_userId);
$permision_cancel		= $obj_fin_receivd_get->get_permision_cancel($serialYear,$serialNo,$session_userId);
 
if($obj_common_function_get->ValidateSpecialPermission('18',$_SESSION["userId"],'RunQuery'))
	$dateDisabled		= "";
else
	$dateDisabled		= "disabled='disabled'";
	
if(!isset($header_array["JOURNAL_ENTRY_NO"]))
	$journalEntDate		= date("Y-m-d");
else
	$journalEntDate		= $header_array["JOURNAL_ENTRY_DATE"];
?>
<title>Payment Received Settlement</title>

<!--<script type="text/javascript" src="presentation/finance_new/customer/payment_received_settlement/payment_received_settlement.js"></script>-->

<form id="frmPayment_received_settlement" name="frmPayment_received_settlement" method="post">
  <div align="center">
    <div class="trans_layoutS" style="width:990px">
      <div class="trans_text">Payment Received Settlement</div>
      <table width="990">
        <tr>
          <td colspan="2"><table width="100%" border="0" class="normalfnt">
              <tr>
                <td>Serial No</td>
                <td><input type="text" name="txtSerialNo" id="txtSerialNo" style="width:80px" disabled="disabled" value="<?php echo $header_array["SETTLE_NO"]?>"/>&nbsp;<input type="text" name="txtSerialYear" id="txtSerialYear" style="width:50px" disabled="disabled" value="<?php echo $header_array["SETTLE_YEAR"]?>"/></td>
                <td>Date</td>
                <td><input name="txtSettlementDate" type="text" value="<?php echo $journalEntDate ?>" class="validate[required]" id="txtSettlementDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo $dateDisabled?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
              <tr>
                <td width="9%">Customer</td>
                <td width="64%"><select name="cboCustomer" id="cboCustomer" style="width:200px">
                  <?php 
 				  echo $obj_fin_com_get->getCustomerCombo($header_array["CUTOMER_ID"]); ?>
                </select></td>
                <td width="14%">Currency<span class="compulsoryRed"> *</span></td>
                <td width="13%"><select name="cboCurrency" id="cboCurrency" style="width:98px">
                  <?php 
 				  echo $obj_fin_com_get->getCurrencyCombo($header_array["CURRENCY_ID"]); ?>
                </select></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td width="400" ><table width="900"  border="0" class="bordered" id="tblGL">
              <thead>
                <tr>
                  <th colspan="4">GL Accounts</th>
                </tr>
                <tr>
                  <th width="40%" >GL Account</th>
                  <th width="27%" >Cost Center</th>
                  <th width="16%" >Balance Amount</th>
                  <th width="17%" >Settle Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php 
				$totAccAmount=0;
 while($row = mysqli_fetch_array($header_accounts_result))
 {
	 $booAvailable	= true;
 ?>
                <tr class="cls_tr_firstRow">
                  <td class="cls_td_GL" id="<?php echo $row['LEDGER_ID']; ?>"><?php echo $row['CHART_OF_ACCOUNT_NAME']; ?></td>
                  <td class="cls_td_Cost" id="<?php echo $row['COST_CENTER_ID']; ?>"><?php echo $row['COST_CENTER']; ?></td>
                  <td class="cls_td_balAmount" id="<?php echo $row['balAmmount']; ?>" align="right"><?php echo $row['balAmmount']; ?></td>
                  <td class="cls_td_amount" align="center"><input class="text-input validate[custom[number],max[<?php echo $row['balAmmount'];?>]] cal" type="text" style="width:100%;text-align:right" value="<?php echo $row['AMOUNT']; ?>" name="txtUnsettleAdvance" id="txtUnsettleAdvance"/></td>
                </tr>
                <?php 
				$totAccAmount +=$row['AMOUNT'];
 }
?>
                <?php
if(!$booAvailable){
  //die();
 }
?>
<tr class="cls_tr_firstRow">
                  <td class="cls_td_GL" colspan="3" id="" align="right">Total&nbsp;&nbsp;&nbsp;&nbsp; </td>
                  <td class="cls_td_costCenter" align="center"><input class="text-input glTotal" type="text" style="width:100%;text-align:right" name="txtUnsettleAdvance2" id="txtUnsettleAdvance2" disabled="disabled" value="<?php echo $totAccAmount; ?>"/></td>
</tr>
              </tbody>
            </table></td>
            <td width="20"></td>
        </tr>
        <tr>
          <td colspan="2" height="34"></td>
        </tr>
        <tr>
          <td ><div style="overflow:scroll;width:960px;height:250px;">
              <table width="900" border="0" class="bordered" id="tblMain">
                <thead>
                  <tr>
                    <th class="cls_td_check" width="3%"><input class="cls_check_all" type="checkbox" checked="checked"></th>
                    <th width="10%">Invoice No</th>
                    <th width="13%">Invoice Remarks</th>
                    <th width="8%">Invoiced Date</th>
                    <th width="8%">Invoiced </th>
                    <th width="6%">Credit Note</th>
                    <th width="5%">Debit Note</th>
                    <th width="8%">Advanced</th>
                    <th width="8%">Received </th>
                    <th width="6%">Settled</th>
                    <th width="9%">To Be Receive</th>
                    <th width="16%">Receiving Amount</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
  $totInvAmount=0;
 while($row = mysqli_fetch_array($details_invoice_result))
{ 
 			$toBeReceived	= round((abs($row["INVOICED_VALUE"]) + abs($row["DEBIT_VALUE"])) - (abs($row["CREDIT_VALUE"]) + abs($row["ADVANCE_VALUE"]) + abs($row["RECEIVED_VALUE"]) + abs($row["INV_SETTLEMENT"])),2);
			$settled		= abs($row["INV_SETTLEMENT"]);					
?>
                  <tr>
                    <td class="cls_td_invoiceNo"><input class="cls_check" type="checkbox" checked="checked"></td>
                    <td class="cls_td_invoiceNo"><?php echo $row["INVOICE_NO"]?></td>
                    <td class="cls_td_invoiceRemarks"><?php echo ($row["INVOICE_REMARKS"]==''?'&nbsp;':$row["INVOICE_REMARKS"])?></td>
                    <td class="cls_td_invoicedDate"><?php echo $row["INVOICED_DATE"]?></td>
                    <td class="cls_td_invoicedAmount" style="text-align:right"><?php echo abs($row["INVOICED_VALUE"])?></td>
                    <td class="cls_td_toBePaid" style="text-align:right"><?php echo abs($row["CREDIT_VALUE"])?></td>
                    <td class="cls_td_toBePaid" style="text-align:right"><?php echo abs($row["DEBIT_VALUE"])?></td>
                    <td class="cls_td_toBePaid" style="text-align:right"><?php echo abs($row["ADVANCE_VALUE"])?></td>
                    <td class="cls_td_toBePaid" style="text-align:right"><?php echo abs($row["RECEIVED_VALUE"])?></td>
                    <td class="cls_td_payingAmount" style="text-align:right"><span class="cls_td_toBePaid" style="text-align:right"><?php echo $settled?></span></td>
                    <td class="cls_td_toBePaid" style="text-align:right"><?php echo $toBeReceived?></td>
                    <td class="cls_td_payingAmount" style="text-align:right"><input class="invAmmount invAmount" id="amm" style="text-align:right; width:100%"  type="text" value="<?php echo $row["INVOICE_AMOUNT"]?>" /></td>
                  </tr>
                  <?php
  $totInvAmount +=$row["INVOICE_AMOUNT"];
}
?>

                </tbody>
              </table>
          </div></td>
          <td></td>
        </tr>
        <tr>
          <td colspan="2"><table width="351" border="0" align="right" class="normalfnt">
              <tr>
                <td width="212">Total Invoice Allocated Amount</td>
                <td width="9" style="text-align:center"><b>:</b></td>
                <td width="71" style="text-align:right" id="td_totalInvAmount"><?php echo $totInvAmount; ?></td>
            <td width="41">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2" align="center">
                <a class="button white medium" id="butNew">New</a>
                <a class="button white medium" id="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a>
                <a class="button white medium" id="butCancel" <?php if($permision_cancel!=1){ ?>style="display:none"<?php } ?>>Cancel</a>
                 <a class="button white medium" id="butReport">Report</a>
                <a href="main.php" class="button white medium" id="butClose">Close</a>
          </td>
        </tr>
      </table>
    </div>
  </div>
</form>

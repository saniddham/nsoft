<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/finance/cls_convert_amount_to_word.php";
include_once "class/finance/customer/payment_received_settlement/cls_payment_received_settlement_get.php";
require_once "class/finance/cls_common_get.php";

$locationId 				= $_SESSION['CompanyID'];
$serialNo					= $_REQUEST["SerialNo"];
$serialYear					= $_REQUEST["SerialYear"];

$obj_fin_com				= new Cls_Common_Get($db); 
$obj_fin_receivd_get		= new Cls_PaymentReceivedSettlement_Get($db);
$obj_convert_amount_to_word	= new Cls_Convert_Amount_To_Word($db);

$header_array 				= $obj_fin_receivd_get->get_ReportHeaderDetails_array($serialYear,$serialNo);
$detail_result 				= $obj_fin_receivd_get->get_ReportGridDetails_invoice_result($serialYear,$serialNo);
$gl_result 					= $obj_fin_receivd_get->get_ReportGridDetails_gl_result($serialYear,$serialNo);

if(!isset($_REQUEST["SerialNo"]))
	$invoiceDate			= date("Y-m-d");
else
	$invoiceDate			= $header_array["RECEIPT_DATE"];
	
$company_Id			= $header_array["COMPANY_ID"];
?>
<head>
<title>Payment Received Settlement Report</title>
<style type="text/css">
.apDiv1 {position:absolute;left:380px;top:100px;width:auto;height:auto;z-index:0;opacity:0.1;}
</style>
</head>
<body>
<div id="partPay" class="apDiv1 <?php echo $header_array["STATUS"]=='10'?'maskShow':'maskHide'?>"><img src="images/cancelled.png"  /></div>
<form id="frmSalesInvoice" name="frmSalesInvoice" method="post" action="invoice.php" autocomplete="off">
  <table width="900" align="center">
    <tr>
      <td><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
      <td style="text-align:center" class="reportHeader">Payment Received Settlement Receipt</td>
    </tr>
    <tr>
      <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td width="14%">Receipt No</td>
            <td width="1%">:</td>
            <td><?php echo $obj_fin_com->getSerialNo($header_array["SERIAL_NO"],$header_array["SERIAL_DATE"],$company_Id,'RunQuery')//echo $header_array["ADVANCE_NO"]; ?></td>
            <td width="11%">Receipt Date</td>
            <td width="1%">:</td>
            <td width="34%"><?php echo $header_array["RECEIPT_DATE"]?></td>
          </tr>
          <tr>
            <td>Received From</td>
            <td>:</td>
            <td><?php echo $header_array["CUSTOMER_NAME"]?></td>
            <td>Currency</td>
            <td>:</td>
            <td><?php echo $header_array["CURRENCY_CODE"]?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="normalfnt"><table width="100%%" border="0" class="rptBordered" id="tblMain2">
          <thead>
            <tr>
              <th width="34%">Account Name</th>
              <th width="26%">Cost Center</th>
              <th width="10%">Amount</th>
            </tr>
          </thead>
          <tbody>
            <?php
$total_value	= 0;
$totGlAmount	= 0;
while($row = mysqli_fetch_array($gl_result))
{ 
?>
            <tr>
              <td style="text-align:left"><?php echo $row["GL_NAME"]?></td>
              <td style="text-align:left"><?php echo $row["COST_CENTER"]?></td>
              <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
            </tr>
            <?php
			$totGlAmount	+= round($row["AMOUNT"],2);
}
?>
            <tr style="font-weight:bold">
              <td colspan="2" style="text-align:center">TOTAL</td>
              <td style="text-align:right"><?php echo number_format($totGlAmount,2)?></td>
            </tr>
        
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td class="normalfnt">&nbsp;</td>
    </tr>
    <tr>
      <td ><table width="100%" border="0" class="rptBordered" id="tblMain">
          <thead>
            <tr>
              <th width="3%">&nbsp;</th>
              <th width="83%">Invoice No</th>
              <th width="14%">Amount</th>
            </tr>
          </thead>
          <tbody>
            <?php
$total_value	= 0;
$i				= 0;
while($row = mysqli_fetch_array($detail_result))
{ 
?>
            <tr>
              <td class="cls_td_check" align="center"><?php echo ++$i.'.';?></td>
              <td class="cls_td_salesOrderNo"><?php echo $row["INVOICE_NO"]?></td>
              <td width="14%" style="text-align:right"><?php echo number_format($row["PAY_AMOUNT"],2)?></td>
            </tr>
            <?php
	$total_value 	+= round($row["PAY_AMOUNT"],2);
}
?>
            <tr style="font-weight:bold">
              <td colspan="2" align="center" class="cls_td_check">TOTAL</td>
              <td width="14%" style="text-align:right"><?php echo number_format($total_value,2)?></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td class="normalfnt"><b><?php echo $obj_convert_amount_to_word->Convert_Amount($total_value,$header_array["CURRENCY_CODE"])?></b></td>
    </tr>
    <tr>
      <td >&nbsp;</td>
    </tr>
  </table>
</form>
</body>

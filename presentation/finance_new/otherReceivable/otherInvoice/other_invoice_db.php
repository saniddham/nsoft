<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php


/*
SAVE
	1. check whether month end is locked
	2. check exchange rate for current date
	3. check permissions.
	4. display error msg if details not available.
	5. check saved location when edit.
	

APPROVE
	1. check whether month end is locked
	2. check permissions
	3. check saved location when approve.
	4. insert into `finance_other_receivable_transaction` table - when final approval. [Document Type 'BILLINVOICE' , Amount '+']
	5. insert into `finance_transaction` table - when final approval [Document Type = 'BILLINVOICE',TRANSACTION_CATEGORY 'OR',TRANSACTION_CATEGORY_ID 'GL_ID'] [Customer - D , Other Items + tax - C.]

CANCEL
	1. check whether month end is locked		
	2. check permissions.
	3. check saved location when cancel.
	4. check whether some payment is raised
	5. update header status as '-2'.
	6. rollback 'finance_other_receivable_transaction'
	7. rollback 'finance_transaction'
*/
try{
	$userId 			= $sessions->getUserId();
	$locationId	  		= $sessions->getLocationId();
	$companyId			= $sessions->getCompanyId();
	$menuID				= '1078';
	$programCode		= 'P1078';
	
	include_once "class/finance/cls_calculate_tax.php";							$obj_calculate_tax								= new Cls_Calculate_Tax($db);
	include_once "class/tables/menupermision.php";								$menupermision									= new menupermision($db);
	include_once "class/tables/sys_approvelevels.php";							$sys_approvelevels								= new sys_approvelevels($db);
	include_once "class/tables/sys_no.php";										$sys_no											= new sys_no($db);
	include_once "class/tables/finance_other_receivable_invoice_header.php";	$finance_other_receivable_invoice_header		= new  finance_other_receivable_invoice_header($db);
	include_once "class/tables/finance_other_receivable_invoice_details.php";	$finance_other_receivable_invoice_details		= new finance_other_receivable_invoice_details($db);
	include_once "class/dateTime.php";											$dateTimes										= new dateTimes($db);
	include_once "class/tables/mst_financeexchangerate.php";					$mst_financeexchangerate						= new mst_financeexchangerate($db);
	include_once "class/tables/mst_customer.php";								$mst_customer									= new mst_customer($db);
	include_once "class/tables/mst_locations.php";								$mst_locations									= new mst_locations($db);
	include_once "class/tables/finance_other_receivable_invoice_approve_by.php";$finance_other_receivable_invoice_approve_by	= new finance_other_receivable_invoice_approve_by($db);
	include_once "class/tables/finance_mst_chartofaccount.php";					$finance_mst_chartofaccount						= new finance_mst_chartofaccount($db);
	include_once "class/tables/finance_other_receivable_transaction.php";		$finance_other_receivable_transaction			= new finance_other_receivable_transaction($db);
	include_once "class/tables/finance_transaction.php";						$finance_transaction							= new finance_transaction($db);
	include_once "class/tables/finance_mst_chartofaccount_tax.php";				$finance_mst_chartofaccount_tax					= new finance_mst_chartofaccount_tax($db);
	include_once "class/tables/mst_financetaxgroup.php";						$mst_financetaxgroup							= new mst_financetaxgroup($db);
	include_once "class/tables/finance_month_end_process.php";					$finance_month_end_process						= new finance_month_end_process($db);

	$requestType	= $_REQUEST['requestType'];
	
	if($requestType =='calculateTax')
	{
		$taxId		= $_REQUEST['taxId'];
		$amount		= ($_REQUEST['amount']==''?0:$_REQUEST['amount']);
		
		$taxAmount	= $obj_calculate_tax->CalculateTax($taxId,$amount);
		$response	= json_decode($taxAmount);
	}
	else if($requestType == 'loadExchangeRate')
	{
		$currency				= $_REQUEST['currencyId'];
		$date	  				= $_REQUEST['date'];
		
		$mst_financeexchangerate->set($currency,$date,$companyId);
		$exchangeRate			= $mst_financeexchangerate->getdblExcAvgRate();
		$response['exchgRate']	= $exchangeRate;
	}
	else if($requestType == 'loadCurrency')
	{
		$customerId				= $_REQUEST['customerId'];
		$date	  				= $_REQUEST['date'];
		
		$mst_customer->set($customerId);
		$currency				= $mst_customer->getintCurrencyId();
		
		$mst_financeexchangerate->set($currency,$date,$companyId);
		$exchangeRate			= $mst_financeexchangerate->getdblExcAvgRate();
		$response['currencyId'] = $currency;
		$response['exchgRate']	= $exchangeRate;
			
	}
	else if($requestType == 'saveData')
	{
		$db->connect();$db->begin();	
		$editMode			= false;
		$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
		$arrDetails 		= json_decode($_REQUEST['arrDetails'],true);
		
		$invoiceNo			= $arrHeader['invoiceNo'];
		$invoiceYear		= $arrHeader['invoiceYear'];
		$customerID			= $arrHeader['customerID'];
		$currencyID			= $arrHeader['currencyID'];
		$remark				= $arrHeader['remark'];
		$referenceNo		= $arrHeader['referenceNo'];		
		$invDate			= $arrHeader['invDate'];
		$grandTotal			= $arrHeader['grandTotal'];
		
		$mep_status			= $finance_month_end_process->getIsMonthEndProcessLocked($invDate,$companyId);
		
		####################################################
		## 1.check whether month end is locked	 		  ##
		####################################################

		if($mep_status)
				throw new Exception("Month end Process locked for the date '".$invDate."' .");
				
		$menupermision->set($programCode,$userId);
		
		## get approve levels and status
		$sys_approvelevels->set($programCode);
		$approveLevel		= $sys_approvelevels->getintApprovalLevel();
		$status				= $approveLevel+1; 
		
		####################################################
		## 2.check exchange rate for current date		  ##
		####################################################
		$exchResult				= $mst_financeexchangerate->checkCurrencyAvailableForSelectedDate($invDate,$companyId);
		if(!$exchResult)
			throw new Exception("Please enter exchange rates for ".$invDate);
		
		## insert data to header 						
		if($invoiceNo == '' && $invoiceYear == '')
		{
		
		####################################################
		## 3.check permissions							  ##
		####################################################
			$permissionArr	= $menupermision->checkMenuPermision('Edit',NULL,NULL);
			if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
		####################################################
		## 4.display error msg if details not available.  ##
		####################################################
		
			if(!$arrDetails)
			{
				throw new Exception("No details to save");
			}
			//get serial no	
			$invoiceNo			= $sys_no->getSerialNoAndUpdateSysNo('OTHER_RECEIVABLE_INVOICE_NO',$locationId);
			$invoiceYear		= $dateTimes->getCurruntYear();

			// save header data
			$result_arr		= $finance_other_receivable_invoice_header->insertRec($invoiceNo,$invoiceYear,$customerID,$currencyID,$referenceNo,$remark,$invDate,$status,$approveLevel,$companyId,$locationId,$userId,$dateTimes->getCurruntDateTime(),'NULL',$dateTimes->getCurruntDateTime());
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
		}
		## update data and delete datail data			
		else
		{
			$finance_other_receivable_invoice_header->set($invoiceNo,$invoiceYear);
			$status_saved			= $finance_other_receivable_invoice_header->getSTATUS();
			$approveLevel_saved		= $finance_other_receivable_invoice_header->getAPPROVE_LEVELS();
			
			$permissionArr	= $menupermision->checkMenuPermision('Edit',$status_saved,$approveLevel_saved);

			if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
				
		####################################################
		## 5.check whether saved location			  	  ##
		####################################################
		$location	= $finance_other_receivable_invoice_header->getLOCATION_ID();
		if($location != $locationId)
				throw new Exception("This is not saved location");
 		
		## update header data							
			$finance_other_receivable_invoice_header->set($invoiceNo,$invoiceYear);

			$finance_other_receivable_invoice_header->setCUSTOMER_ID($customerID);
			$finance_other_receivable_invoice_header->setCURRENCY_ID($currencyID);
			$finance_other_receivable_invoice_header->setREFERENCE_NO($referenceNo);
			$finance_other_receivable_invoice_header->setREMARKS($remark);
			$finance_other_receivable_invoice_header->setBILL_DATE($invDate);
			$finance_other_receivable_invoice_header->setSTATUS($status);
			$finance_other_receivable_invoice_header->setAPPROVE_LEVELS($approveLevel);
			$finance_other_receivable_invoice_header->setLAST_MODIFY_BY($userId);
			$finance_other_receivable_invoice_header->commit();
				
		## update max status in approve by table	
			$finance_other_receivable_invoice_approve_by->updateMaxStatus($invoiceNo,$invoiceYear);

		## delete issue to production detail data		
			$resultDel		= $finance_other_receivable_invoice_details->delete(" BILL_INVOICE_NO = '$invoiceNo' AND BILL_INVOICE_YEAR = '$invoiceYear'");
			
			if(!$resultDel['status'])	
			{
				throw new Exception("Delete Error!");	
			}
						
			$editMode		= 1;	
		}
		## Save Detail Data							 

		foreach($arrDetails as $detail)
		{
			$chartOfAccId	= $detail['chartOfAccId'];
			$itemDisc		= $detail['itemDisc'];
			$uom			= $detail['uom'];
			$qty			= $detail['qty'];
			$unitPrice		= $detail['unitPrice'];
			$discount		= ($detail['discount']==''?'0':$detail['discount']);
			//$discount		= $detail['discount'];
			$taxAmount		= $detail['taxAmount'];
			$taxId			= ($detail['taxId']==''?'NULL':$detail['taxId']);
			$costCenter		= $detail['costCenter'];
			
			$saveResult		= $finance_other_receivable_invoice_details->insertRec($invoiceNo,$invoiceYear,$chartOfAccId,$itemDisc,$uom,$qty,$unitPrice,$discount,$taxAmount,$taxId,$costCenter);
			if(!$saveResult['status'])
				throw new Exception($saveResult['msg']);	
			
		}
		if($approveLevel == 1)
		{
			insertToTransactionTables($invoiceNo,$invoiceYear);
		} 
		$approve_perm	= $menupermision->checkMenuPermision('Approve',$status,$approveLevel);
		$cancel_perm	= $menupermision->checkMenuPermision('Cancel',$status,$approveLevel);

		$sys_no->getSerialNoAndUpdateSysNo('OTHER_RECEIVABLE_INVOICE_NO',$locationId);
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode == 1)
			$response['msg'] 		= "Updated Successfully.";
		else
			$response['msg'] 		= "Saved Successfully.";
		$response['InvoiceNo']		= $invoiceNo;
		$response['InvoiceYear']	= $invoiceYear;
		$response['app_perm']		= $approve_perm['type'];
		$response['cancel_perm']	= $cancel_perm['type'];
		
		$db->disconnect();
	}
	else if($requestType == 'approve')
	{
		$db->connect();$db->begin();	
		$invoiceNo			= $_REQUEST['invoiceNo'];
		$invoiceYear		= $_REQUEST['invoiceYear'];
		
		## get header status and approve level	
		$finance_other_receivable_invoice_header->set($invoiceNo,$invoiceYear);
		$status				= $finance_other_receivable_invoice_header->getSTATUS(); 
		$approveLevels		= $finance_other_receivable_invoice_header->getAPPROVE_LEVELS();

		$invoiceDate		= $finance_other_receivable_invoice_header->getBILL_DATE();
		$mep_status			= $finance_month_end_process->getIsMonthEndProcessLocked($invoiceDate,$companyId);
		
		####################################################
		## 1. check whether month end is locked			  ##
		####################################################
		if($mep_status)
				throw new Exception("Month end Process locked for the date '".$invoiceDate."' .");

		$menupermision->set($programCode,$userId);
		
		####################################################
		## 2.check Approve permission					  ##
		####################################################
		$permissionArr	= $menupermision->checkMenuPermision('Approve',$status,$approveLevels);
		if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
		
		####################################################
		## 3.check whether saved location			  	  ##
		####################################################
		$location	= $finance_other_receivable_invoice_header->getLOCATION_ID();
		if($location != $locationId)
				throw new Exception("This is not saved location");
				
		## Update header status				
		$where = "BILL_INVOICE_NO = '$invoiceNo' AND BILL_INVOICE_YEAR= '$invoiceYear'";
		
		$data  =array(	'STATUS'=> -1
		);
		$result_update_h		= $finance_other_receivable_invoice_header->upgrade($data,$where);
		if(!$result_update_h['status']) 
			throw new Exception($result_update_h['msg']);
		
		## insert to approve by table				  	  

		$finance_other_receivable_invoice_header->set($invoiceNo,$invoiceYear);
		$hstatus		= $finance_other_receivable_invoice_header->getSTATUS();
		$aplevel		= $finance_other_receivable_invoice_header->getAPPROVE_LEVELS();
		$level			= (int)$aplevel+1- (int)$hstatus; 
		$inserToApprovedBy	= $finance_other_receivable_invoice_approve_by->insertRec($invoiceNo,$invoiceYear,$level,$userId,$dateTimes->getCurruntDateTime(),0);
		if(!$inserToApprovedBy['status']) 
			throw new Exception($inserToApprovedBy['msg']);
		
		if($hstatus == 1)
		{
			insertToTransactionTables($invoiceNo,$invoiceYear);
		}
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Approved Successfully.";	
		$db->disconnect();
	}
	else if($requestType == 'reject')
	{
		$db->connect();$db->begin();
		$invoiceNo			= $_REQUEST['invoiceNo'];
		$invoiceYear		= $_REQUEST['invoiceYear'];
		
		
		$finance_other_receivable_invoice_header->set($invoiceNo,$invoiceYear);
		$status				= $finance_other_receivable_invoice_header->getSTATUS(); 
		$approveLevels		= $finance_other_receivable_invoice_header->getAPPROVE_LEVELS();

		$menupermision->set($programCode,$userId);
		
		## check Reject permission	
		$permissionArr	= $menupermision->checkMenuPermision('Reject',$status,$approveLevels);
		if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
		
		## check whether saved location	
		$location	= $finance_other_receivable_invoice_header->getLOCATION_ID();
		if($location != $locationId)
				throw new Exception("This is not saved location");
						
		$finance_other_receivable_invoice_header->setSTATUS(0);
		$finance_other_receivable_invoice_header->commit();
		
		$inserToApprovedBy	= $finance_other_receivable_invoice_approve_by->insertRec($invoiceNo,$invoiceYear,0,$userId,$dateTimes->getCurruntDateTime(),0);
		if(!$inserToApprovedBy['status']) 
			throw new Exception($inserToApprovedBy['msg']);
				
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Rejected Successfully.";
		$db->disconnect();	
	}
	else if($requestType == 'cancel')
	{
		$db->connect();$db->begin();
		$invoiceNo			= $_REQUEST['invoiceNo'];
		$invoiceYear		= $_REQUEST['invoiceYear'];
		
		## get header status and approve level	
		$finance_other_receivable_invoice_header->set($invoiceNo,$invoiceYear);
		$status				= $finance_other_receivable_invoice_header->getSTATUS(); 
		$approveLevels		= $finance_other_receivable_invoice_header->getAPPROVE_LEVELS();
		$invoiceDate		= $finance_other_receivable_invoice_header->getBILL_DATE();
		$mep_status			= $finance_month_end_process->getIsMonthEndProcessLocked($invoiceDate,$companyId);
		
		####################################################
		## 1. check whether month end is locked			  ##
		####################################################
		if($mep_status)
				throw new Exception("Month end Process locked for the date '".$invoiceDate."' .");
		
		$menupermision->set($programCode,$userId);
		
		####################################################
		## 2.check Cancel permission					  ##
		####################################################
		$permissionArr	= $menupermision->checkMenuPermision('Cancel',$status,$approveLevels);
		if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
		
		####################################################
		## 3.check whether saved location			  	  ##
		####################################################
		$location	= $finance_other_receivable_invoice_header->getLOCATION_ID();
		if($location != $locationId)
				throw new Exception("This is not saved location");	
	
		####################################################
		## 4.check whether some payment is raised	  	  ##
		####################################################
		$paidAmt	= $finance_other_receivable_transaction->getTransactionAMount($invoiceNo,$invoiceYear,'payment');
		if($paidAmt>0)
			throw new Exception("Cannot cancel this Invoice.Some payment already raised.");
			
		####################################################
		## 5.update header status  					 	  ##
		####################################################
		$finance_other_receivable_invoice_header->setSTATUS(-2);
		$finance_other_receivable_invoice_header->commit();
		
		## update approved by table	 				

		$inserToApprovedBy	= $finance_other_receivable_invoice_approve_by->insertRec($invoiceNo,$invoiceYear,-2,$userId,$dateTimes->getCurruntDateTime(),0);
		if(!$inserToApprovedBy['status']) 
			throw new Exception($inserToApprovedBy['msg']);
			
		####################################################
		## 6.rollback finance_other_receivable_transaction##
		#################################################### 

		$deleteORTrans	= $finance_other_receivable_transaction->delete("BILL_INVOICE_NO = '$invoiceNo' AND BILL_INVOICE_YEAR = '$invoiceYear' AND DOCUMENT_TYPE = 'BILLINVOICE'");
		if(!$deleteORTrans['status']) 
			throw new Exception($deleteORTrans['msg']);
			
		####################################################
		## 7.rollback finance_transaction					##
		#################################################### 
		$deleteFinTrans	= $finance_transaction->delete("INVOICE_NO = '$invoiceNo' AND INVOICE_YEAR = '$invoiceYear' AND DOCUMENT_TYPE = 'BILLINVOICE' AND TRANSACTION_CATEGORY = 'OR'");
		if(!$deleteFinTrans['status']) 
			throw new Exception($deleteFinTrans['msg']);
				
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "cancel Successfully.";
		$db->disconnect();	
			
	}
	
}
catch(Exception $e)
{
	$db->rollback();
	$response['msg'] 	=  $e->getMessage();
	$response['error'] 	=  $error_handler->jTraceEx($e);
	$response['type']	=  'fail';
	$response['sql']	=  $db->getSql();
}
	
$db->disconnect();		
echo json_encode($response);

function insertToTransactionTables($invoiceNo,$invoiceYear)
{
	global $finance_other_receivable_invoice_header;
	global $mst_customer;
	global $finance_mst_chartofaccount;
	global $finance_other_receivable_invoice_details;
	global $finance_other_receivable_transaction;
	global $finance_transaction;
	global $finance_mst_chartofaccount_tax;
	global $mst_financetaxgroup;
	global $dateTimes;
	global $userId;
	
	$finance_other_receivable_invoice_header->set($invoiceNo,$invoiceYear);
	$invoiceDate	= $finance_other_receivable_invoice_header->getBILL_DATE();
	$customerID		= $finance_other_receivable_invoice_header->getCUSTOMER_ID();
	$currencyID		= $finance_other_receivable_invoice_header->getCURRENCY_ID();
	$remark			= $finance_other_receivable_invoice_header->getREMARKS();
	$refNo			= $finance_other_receivable_invoice_header->getREFERENCE_NO();
	$creteCompanyId	= $finance_other_receivable_invoice_header->getCOMPANY_ID();
	$location		= $finance_other_receivable_invoice_header->getLOCATION_ID();
	
	$mst_customer->set($customerID);
	$customerGL		= $finance_mst_chartofaccount->getCatogoryTypeWiseGL('C',$customerID);
	$amtResult		= $finance_other_receivable_invoice_details->getDetailData_result($invoiceNo,$invoiceYear);
	$totAmount 		= 0;
	$totTaxAmount 	= 0;
	while($rowDetail = mysqli_fetch_array($amtResult))
	{
		$amount 		= ($rowDetail['QTY']*$rowDetail['UNIT_PRICE'])*((100-$rowDetail['DISCOUNT'])/100);
		$finalAmount 	= $amount;
		$totAmount 		= $totAmount+$finalAmount;
		$taxID			= $rowDetail['TAX_GROUP_ID'];
		$totTaxAmount 	= $totTaxAmount+$rowDetail['TAX_AMOUNT'];
		$glID			= $rowDetail['CHART_OF_ACCOUNT_ID'];
		$ORTransaction	= $finance_other_receivable_transaction->insertRec($customerID,$currencyID,$invoiceYear,$invoiceNo,'BILLINVOICE',$invoiceYear,$invoiceNo,$glID,$finalAmount,$creteCompanyId,$location,$userId,$invoiceDate);
		$finTransaction	= $finance_transaction->insertRec($glID,$finalAmount,$invoiceNo,$invoiceYear,'BILLINVOICE',$invoiceNo,$invoiceYear,'C','OR',$customerID,$currencyID,NULL,NULL,0,$location,$creteCompanyId,$userId,$invoiceDate,$dateTimes->getCurruntDateTime());

		if($taxID != NULL || $taxID !='' )
		{
			$taxGL			= $finance_mst_chartofaccount_tax->getTaxGL_ID($taxID);
			$taxORTrans		= $finance_other_receivable_transaction->insertRec($customerID,$currencyID,$invoiceYear,$invoiceNo,'BILLINVOICE',$invoiceYear,$invoiceNo,$taxGL,$rowDetail['TAX_AMOUNT'],$creteCompanyId,$location,$userId,$invoiceDate);
			$taxFinTrans	= $finance_transaction->insertRec($taxGL,$rowDetail['TAX_AMOUNT'],$invoiceNo,$invoiceYear,'BILLINVOICE',$invoiceNo,$invoiceYear,'C','OR',$customerID,$currencyID,NULL,NULL,0,$location,$creteCompanyId,$userId,$invoiceDate,$dateTimes->getCurruntDateTime());

		}
		
	}
	$grandTotal 	= $totAmount+$totTaxAmount; 
	$insertTransaction	= $finance_transaction->insertRec($customerGL,$grandTotal,$invoiceNo,$invoiceYear,'BILLINVOICE',$invoiceNo,$invoiceYear,'D','OR',$customerID,$currencyID,NULL,NULL,0,$location,$creteCompanyId,$userId,$invoiceDate,$dateTimes->getCurruntDateTime());	
}	
?>
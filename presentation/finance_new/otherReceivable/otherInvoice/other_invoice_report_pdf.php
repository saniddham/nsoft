<?php
session_start();
//ini_set('display_errors',1);

//BEGIN - INCLUDE FILES {
$backwardseperator 	= "../../../../";
include_once "../../../../dataAccess/DBManager2.php";										$db						= new DBManager2();
require_once "../../../../libraries/fpdf/fpdf.php";
include_once "../../../../class/sessions.php";												$sessions				= new sessions();
include_once "../../../../class/tables/mst_locations.php";									$mst_locations			= new mst_locations($db);
include_once "../../../../class/tables/mst_companies.php";									$mst_companies			= new mst_companies($db);
include_once "../../../../class/tables/mst_customer.php";									$mst_customer			= new mst_customer($db);
include_once "../../../../class/tables/mst_country.php";									$mst_country			= new mst_country($db);
include_once "../../../../class/tables/sys_users.php";										$sys_users				= new sys_users($db);
include_once "../../../../class/tables/mst_financecurrency.php";							$mst_financecurrency	= new mst_financecurrency($db);
include_once "../../../../class/finance/cls_convert_amount_to_word.php";					$obj_AmtName	   		= new Cls_Convert_Amount_To_Word($db);
include_once "../../../../class/tables/mst_financeexchangerate.php";						$mst_financeexchangerate						= new mst_financeexchangerate($db);
include_once "../../../../class/tables/finance_other_receivable_invoice_header.php";		$finance_other_receivable_invoice_header		= new finance_other_receivable_invoice_header($db);
include_once "../../../../class/tables/finance_other_receivable_invoice_details.php";		$finance_other_receivable_invoice_details		= new finance_other_receivable_invoice_details($db);
include_once "../../../../class/tables/menupermision.php";	 								$menupermision									= new menupermision($db);
									

//END	- INCLUDE FILES }
$userId  			= $sessions->getUserId();

//BEGIN - PARAMETERS {
//$periodArray = array();
$invoiceNo			= $_REQUEST["invoiceNo"];
$invoiceYear		= $_REQUEST["invoiceYear"];
$title				= "Suspended Tax Invoice";
$font				= 'Times';
$totValue			= 0;
//END	- PARAMETERS }
class PDF extends FPDF
{
	
	function Header1()
	{
		global $result_reportHeader;
		global $title;
		global $font;
		$h = 5;
		
		$this->Image('../../../../images/screenline.jpg',8,9);
		$this->SetFont($font,'B',14);$this->Cell(0,$h,$result_reportHeader["COMPANY_NAME"],'0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,$result_reportHeader["LOCATION_ADDRESS"].' '.$result_reportHeader["LOCATION_STREET"].' '.$result_reportHeader["LOCATION_CITY"].' '.$result_reportHeader["COMPANY_COUNTRY"].'.','0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,"Tel : ".$result_reportHeader["LOCATION_PHONE"].'  '."Fax : ".$result_reportHeader["LOCATION_FAX"],'0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,"E-mail : ".$result_reportHeader["LOCATION_EMAIL"].'  '."Web : ".$result_reportHeader["LOCATION_WEB"],'0',1,'C');
		$this->SetFont($font,'B',12);$this->Cell(0,10,$title,'0',1,'C');		
	}
	
	function SetPageWaterMark($status,$printStatus)
	{
		switch($status)
		{
			case 1:
				$msg	= ($printStatus==1?"DUPLICATE  INVOICE":"");
				break;
			case '10':
				$msg	= "CANCELED INVOICE";
				break;
			default:
				$msg	= "NOT A VALID INVOICE";
				break;
		}
		$this->SetFont('Times','B',50);
		$this->SetTextColor(255,192,203);
		$this->RotatedText(35,190,$msg,45);
		$this->SetTextColor(0,0,0);
	}
	
	function RotatedText($x, $y, $txt, $angle)
	{
		//Text rotated around its origin
		$this->Rotate($angle,$x,$y);
		$this->Text($x,$y,$txt);
		$this->Rotate(0);
	}
	
	function ReportHeader($invoiceNo,$invoiceYear,$result_reportHeader)
	{
		global $finance_other_receivable_invoice_header;
		global $mst_customer;
		global $mst_country;

		$finance_other_receivable_invoice_header->set($invoiceNo,$invoiceYear);	
		$mst_customer->set(	$finance_other_receivable_invoice_header->getCUSTOMER_ID());
		$mst_country->set($mst_customer->getintCountryId());
		
		$h = 5;
		$this->SetFont($font,'',11);$this->Cell(34,$h,'To','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(85,$h,$mst_customer->getstrName(),'0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Invoice No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$invoiceNo.' - '.$invoiceYear,'0',1,'L');
		
		$this->SetX(46);
		$this->SetFont($font,'',11);$this->Cell(85,$h,$mst_customer->getstrAddress(),'0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Reference No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$finance_other_receivable_invoice_header->getREFERENCE_NO(),'0',1,'L');
		$this->SetX(46);
		$this->SetFont($font,'',11);$this->Cell(85,$h,$mst_customer->getstrCity(),'0',1,'L');
		$this->SetX(46);
		$this->SetFont($font,'',11);$this->Cell(85,$h,$mst_country->getstrCountryName().'.','0',1,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Attention','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(85,$h,$mst_customer->getstrContactPerson(),'0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Invoice Date','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$finance_other_receivable_invoice_header->getBILL_DATE(),'0',1,'L');
		
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Customer VAT No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(85,$h,$mst_customer->getstrVatNo(),'0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Company VAT No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$result_reportHeader["COMPANY_VAT"],'0',1,'L');
		
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Customer SVAT No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(85,$h,$mst_customer->getstrSVatNo(),'0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(34,$h,'Company SVAT No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(44,$h,$result_reportHeader["COMPANY_SVAT"],'0',1,'L');
		

	}
	
	function CreateTable($result_details,$currCode,$obj_convert_amount_to_word)
	{
		global $totValue;
		$count 			= 0;
		
		$this->Table_header($currCode);
		while($row = mysqli_fetch_array($result_details))
		{
			$totValue	+= $row['UNIT_PRICE']*$row['QTY'];	
			$this->Table_Body($row);	
			if($count == 10){
				$this->AddPage();
				$this->Table_header($currCode);
				$count = 0;	
			}			
			$count++;			
		}
		$this->Table_Body_Validate($count);	
		$this->Table_footer($totValue,$obj_convert_amount_to_word);
	}
	
	function Table_header($currCode)
	{
		$this->SetXY($this->GetX(),$this->GetY()+2);
		$h = 10;
		$this->SetFont('Times','B',10);
		$this->Cell(40,$h,'Item Name','1',0,'C');
		$this->Cell(80,$h,'Item Description','1',0,'C');
		$this->Cell(15,$h,"Unit",'1',0,'C');
		$this->Cell(18,$h,"Qty",'1',0,'C');
		$this->Cell(27,$h,"Price[$currCode]",'1',0,'C');
		$this->Cell(20,$h,'Total Value','1',0,'R');
		$this->Ln();		
	}
		
	function Table_Body($row)
	{	
		$h	= 8;
		$this->SetFont('Times','',8);
		$this->Cell(40,$h,$row["CHART_OF_ACCOUNT_NAME"],'LRB',0,'L');
		$this->Cell(80,$h,$row["ITEM_DESCRIPTION"],'LRB',0,'L');
		$this->Cell(15,$h,$row["UNIT"],'LRB',0,'C');
		$this->Cell(18,$h,number_format($row["QTY"],2),'LRB',0,'R');
		$this->Cell(27,$h,number_format($row["UNIT_PRICE"],2),'LRB',0,'R');
		$this->Cell(20,$h,number_format($row["QTY"]*$row["UNIT_PRICE"],2),'LRB',0,'R');
		$this->Ln();
	}
	
	function Table_Body_Validate($count)
	{
		$h1	= 0;
		for($i=$count;$i<=12;$i++)
		{ 
			//echo "$i";
			//echo "<br/>";
			$h1 += 8;
		}//echo $count.'/'.$h1;
		$this->SetFont('Times','',7);
		$this->Cell(180,$h1,"",'1',0,'C');
		$this->Cell(20,$h1,"",'1',0,'C');
		$this->Ln();
	}
	
	function Table_footer($totValue,$obj_convert_amount_to_word)
	{
		$this->SetY($this->GetY());
		$this->SetFont('Times','B',10);$this->Cell(180,10,"Total",'1',0,'R');
		$this->Cell(20,10,number_format($totValue,2),'1',0,'R');
		$this->Ln();
		
		$this->SetFont('Times','',8);$this->Cell(180,10,$obj_convert_amount_to_word->Convert_Amount($totValue,$baseCurr_array["CODE"]),'0',0,'L');
		$this->Cell(20,10,"",'0',0,'C');
		$this->Ln();
	}
	
	function ReportFooter($exchgRate,$baseCurrencyId)
	{
		global $totValue;
		global $mst_financecurrency;

		$mst_financecurrency->set($baseCurrencyId);
		$this->SetXY(10,$this->GetY()+1);
		$h = 5;		
		$this->SetFont('Times','B',10);$this->Cell(180,$h,"For VAT Purpose Only",'0',0,'L');
		$this->Cell(20,$h,"",'0',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','',10);$this->Cell(180,$h,"Suspended Value Added Tax @ 12 % = ".number_format(($totValue*12)/100,4),'0',0,'L');
		$this->Cell(20,$h,"",'0',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','',10);$this->Cell(180,$h,"Exchange Rate : ".$mst_financecurrency->getstrSymbol().' '.$exchgRate.' / '.$mst_financecurrency->getstrCode().' '.number_format($totValue*$exchgRate,2),'0',0,'L');
		$this->Cell(20,$h,"",'0',0,'C');
		$this->Ln();
		
	}
	
	function Footer1($creator)
	{		
		$this->SetFont('Times','',8);
		
		$this->SetXY(20,$this->GetY()+7);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetX(140);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetXY(20,$this->GetY()+3);
		$this->Cell(50,5,'Authorized By',0,0,'C');
		
		$this->SetX(140);		
		$this->Cell(50,5,'Checked By',0,0,'C');	
		
		$this->SetXY(20,$this->GetY()+10);
		$this->Cell(50,5,$creator,0,0,'C');
		
		$this->SetXY(20,$this->GetY()+2);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetX(140);
		$this->Cell(50,5,'..........................................',0,0,'C');

		$this->SetXY(140,$this->GetY()+3);
		$this->Cell(50,5,'Received By',0,0,'C');

		$this->SetX(20);
		$this->Cell(50,5,'Prepared By',0,0,'C');	
	}
	
	function Footer()
	{
		$this->SetY(-5);
		$this->SetFont('Times','I',8);
		$this->Cell(0,2,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf 					= new PDF('P','mm','Letter');

$db->connect();

$menupermision->set('P1078',$userId);
$pdfPermission			= $menupermision->getintExportToPDF();
if(!$pdfPermission)
{	
	echo "No Permission to View PDF Report";
	return;
}

$finance_other_receivable_invoice_header->set($invoiceNo,$invoiceYear);
$mst_financecurrency->set($finance_other_receivable_invoice_header->getCURRENCY_ID());	
$mst_companies->set($finance_other_receivable_invoice_header->getCOMPANY_ID());
$sys_users->set($finance_other_receivable_invoice_header->getCREATED_BY());

//$result_header 		= $obj_invoice_get->GetPaymentVoucherReportHeader($serialNo,$serialYear);
$result_details			= $finance_other_receivable_invoice_details->getDetailData_result($invoiceNo,$invoiceYear);
$result_reportHeader	= $mst_locations->getCompanyReportHeaderData($finance_other_receivable_invoice_header->getLOCATION_ID());
$exchgRate				= 0;

if($mst_financeexchangerate->checkCurrencyAvailableForSelectedDate($finance_other_receivable_invoice_header->getBILL_DATE(),$finance_other_receivable_invoice_header->getCOMPANY_ID()) && $finance_other_receivable_invoice_header->getCURRENCY_ID()!='')
{
	$mst_financeexchangerate->set($finance_other_receivable_invoice_header->getCURRENCY_ID(),$finance_other_receivable_invoice_header->getBILL_DATE(),$finance_other_receivable_invoice_header->getCOMPANY_ID());
	$exchgRate			= $mst_financeexchangerate->getdblExcAvgRate();
}
$baseCurrencyId			= $mst_companies->getintBaseCurrencyId();

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Header1();
$pdf->SetPageWaterMark($finance_other_receivable_invoice_header->getSTATUS(),$finance_other_receivable_invoice_header->getPRINT_STATUS());
$pdf->ReportHeader($invoiceNo,$invoiceYear,$result_reportHeader);
$pdf->CreateTable($result_details,$mst_financecurrency->getstrCode(),$obj_AmtName);
$pdf->ReportFooter($exchgRate,$baseCurrencyId);
$pdf->Footer1($sys_users->getstrUserName());

$finance_other_receivable_invoice_header->set($invoiceNo,$invoiceYear);
$status		= $finance_other_receivable_invoice_header->getSTATUS();
if($status == 1)
{
	$finance_other_receivable_invoice_header->setPRINT_STATUS(1);
	
	$result_arr	= $finance_other_receivable_invoice_header->commit();	
	$db->commit();
			
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
}
$db->disconnect();
				
$pdf->Output('rpt_other_receivable_invoice_pdf.pdf','I');

?>
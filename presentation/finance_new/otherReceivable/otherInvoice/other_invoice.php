<?php 
(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');
?>
<?php 
include_once "class/tables/mst_customer.php";								$mst_customer									= new mst_customer($db);
include_once "class/tables/mst_financecurrency.php";						$mst_financecurrency							= new mst_financecurrency($db);
include_once "class/tables/mst_units.php";									$mst_units										= new mst_units($db);
include_once "class/finance/cls_get_gldetails.php";							$obj_get_GLCombo								= new Cls_Get_GLDetails($db);
include_once "class/tables/mst_financetaxgroup.php";						$mst_financetaxgroup							= new mst_financetaxgroup($db);
include_once "class/tables/mst_financedimension.php";						$mst_financedimension							= new mst_financedimension($db);
include_once "class/tables/finance_other_receivable_invoice_header.php";	$finance_other_receivable_invoice_header		= new  finance_other_receivable_invoice_header($db);
include_once "class/tables/finance_other_receivable_invoice_details.php";	$finance_other_receivable_invoice_details		= new finance_other_receivable_invoice_details($db);
include_once "class/tables/finance_mst_chartofaccount.php";					$finance_mst_chartofaccount						= new finance_mst_chartofaccount($db);
include_once "class/tables/sys_users.php";									$sys_users										= new sys_users($db);
include_once "class/tables/mst_locations.php";								$mst_locations									= new mst_locations($db);
require_once "class/tables/menus_special.php";            					$menus_special               					= new menus_special($db);

$locationId		= $sessions->getLocationId();
$userId			= $sessions->getUserId();
$companyId		= $sessions->getCompanyId();

$dateSP			= $menus_special->loadSpecialMenuPermission(66,$userId);

$programCode	= 'P1078';
$invoiceNo 		= $_REQUEST['invoiceNo'];
$invoiceYear 	= $_REQUEST['invoiceYear'];

if($invoiceNo != '' && $invoiceYear != '')
	{
		$finance_other_receivable_invoice_header->set($invoiceNo,$invoiceYear);
		$invoiceDate	= $finance_other_receivable_invoice_header->getBILL_DATE();
		$customerID		= $finance_other_receivable_invoice_header->getCUSTOMER_ID();
		$currencyID		= $finance_other_receivable_invoice_header->getCURRENCY_ID();
		$remark			= $finance_other_receivable_invoice_header->getREMARKS();
		$refNo			= $finance_other_receivable_invoice_header->getREFERENCE_NO();
		$status			= $finance_other_receivable_invoice_header->getSTATUS();
		$approveLevel	= $finance_other_receivable_invoice_header->getAPPROVE_LEVELS();
		$creteCompanyId	= $finance_other_receivable_invoice_header->getCOMPANY_ID();
	
		$save_perm				= $menupermision->checkMenuPermision('Edit',$status,$approveLevel);
		$appr_perm				= $menupermision->checkMenuPermision('Approve',$status,$approveLevel);
		$cancel_perm			= $menupermision->checkMenuPermision('Cancel',$status,$approveLevel);
	}
	else
	{
		$save_perm				= $menupermision->checkMenuPermision('Edit','null','null');
	}
?>
<title>Other Receivable Invoice</title>
<form id="frmOtherReceivable" name="frmOtherReceivable" method="post">
  <div align="center">
    <div class="trans_layoutXL" style="width:1000px">
      <div class="trans_text">Other Receivable Invoice</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
      	<tr>
        	<td>
            <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td class="normalfnt">Invoice No</td>
                <td class="normalfnt"><input name="txtInvNo" type="text" disabled="disabled" id="txtInvNo" style="width:60px" value="<?php echo $invoiceNo?>" />&nbsp;<input name="txtInvYear" type="text" disabled="disabled" id="txtInvYear" style="width:35px" value="<?php echo $invoiceYear?>"  /></td>
                <td class="normalfnt">Date</td>
                <td class="normalfnt"><input name="txtDate" type="text" value="<?php echo($invoiceNo==''?date("Y-m-d"):$invoiceDate); ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtDate" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($dateSP!=1?'disabled="disabled"':''); ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
            </tr>
            <tr>
                <td width="16%" class="normalfnt">Customer <span class="compulsoryRed">*</span></td>
                <td width="56%" class="normalfnt">
                <select name="cboCustomer" id="cboCustomer"  style="width:270px" class="validate[required]" >
               <?php
			   	if($invoiceNo != '' && $invoiceYear != '')
					echo $mst_customer->getCombo($customerID,"intStatus = 1 AND intTypeId = 13");
				else	
					echo $mst_customer->getCombo(NULL,"intStatus = 1 AND intTypeId = 13");
				
			    ?>
                </select></td>
                <td width="15%" class="normalfnt">Currency <span class="compulsoryRed">*</span></td>
                <td width="13%" class="normalfnt"><select name="cboCurrency" id="cboCurrency"  style="width:104px" class="validate[required] CurrencyCalExchangeRate " >
               <?php 
			   	if($invoiceNo != '' && $invoiceYear != '')
			  		echo $mst_financecurrency->getCombo($currencyID,"intStatus = 1");
				else
					echo $mst_financecurrency->getCombo(NULL,"intStatus = 1");
			   ?>
                </select></td>
            </tr>
            <tr>
                <td class="normalfnt" valign="top">Remark</td>
                <td rowspan="3" valign="top" class="normalfnt"><textarea name="txtRemarks" id="txtRemarks" style="width:270px;resize:none" rows="2" ><?php echo $remark;?></textarea></td>
                <td class="normalfnt" valign="top">Rate</td>
                <td class="normalfnt" valign="top"><input type="text" name="txtCurrencyRate" id="txtCurrencyRate" style="width:100px" disabled="disabled" value="1.0000" class="cls_txt_exchangeRate"/></td>
            </tr>
            <tr>
              <td class="normalfnt" valign="top">&nbsp;</td>
              <td class="normalfnt" valign="top">Reference No  <span class="compulsoryRed">*</span></td>
              <td class="normalfnt" valign="top"><input type="text" name="txtReferenceNo" id="txtReferenceNo" style="width:100px" maxlength="50" class="validate[required]" value="<?php echo $refNo?>" /></td>
            </tr>
            <tr>
                <td class="normalfnt" valign="top">&nbsp;</td>
                <td class="normalfnt" valign="top">&nbsp;</td>
                <td class="normalfnt" valign="top">&nbsp;</td>
            </tr>
</table>
            </td></tr>
                <tr>
                	<td><div style="overflow:scroll;overflow-x:hidden;width:auto;height:250px;" id="divGrid">
                    <table width="100%" border="0" class="bordered" id="tblOtherIncome">
                   <tr>
                    	<th colspan="10" >Items<div style="float:right"><a class="button white small" id="butInsertRow">Add New Item</a></div></th>
                    </tr> 
  <tr>
    <th width="15" height="30" >Del</th>
    <th width="220" >Item /Income<span class="compulsoryRed">*</span></th>
    <th width="250"  >Item Description <span class="compulsoryRed">*</span></th>
    <th width="80" >UOM <span class="compulsoryRed">*</span></th>
    <th width="80" >Qty <span class="compulsoryRed">*</span></th>
    <th width="80" >Unit Price <span class="compulsoryRed">*</span></th>
    <th width="80" >Discount %</th>
    <th width="100" >Amount</th>
    <th width="100" >Tax</th>
    <th width="100" >Cost Center <span class="compulsoryRed">*</span></th> 
  </tr>
  <?php  if($invoiceNo == '' && $invoiceYear == '')
  { ?>
  <tr class="normalfnt cls_tr_firstRow">
   <td style="text-align:center"><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>
    <td><select name="cboLedgerAc" id="cboLedgerAc" class="clsLedgerAc validate[required]"  style="width:100%" >
    	 <?php
          echo $obj_get_GLCombo->getGLCombo('OTHER_RECEIVABLE_INVOICE','');
         ?>
    </select></td>
    <td><textarea name="txtItemDisc" id="txtItemDisc" class="clsItemDesc validate[required]" style="width:100%;height:19px; resize:none"></textarea></td>
    <td><select name="cboUOM" id="cboUOM" class="clsUOM validate[required]" style="width:100%">
    <?php 
		echo $mst_units->getCombo(NULL,"intStatus=1");
	?>
    </select></td>
    <td><input type="textbox" id="txtQty" class="clsQty validate[required,custom[number]] cls_input_number_validation" style="width:100%;text-align:right" value="" /></td>
    <td><input type="textbox" id="txtUnitPrice" class="clsUnitPrice validate[required,decimal[2],custom[number]] cls_input_number_validation" style="width:100%;text-align:right" value=""></td>
    <td><input type="textbox" id="txtDiscount" class="clsDiscount validate[decimal[2],custom[number]] cls_input_number_validation" style="width:100%;text-align:right" value=""></td>
    <td><input type="textbox" id="txtAmount" class="clsAmount" style="width:98%;text-align:right" value="0.00" disabled="disabled" /></td>
    <td><select name="cboTax" id="cboTax" class="clsTax" style="width:100%">
    <?php 
	echo $mst_financetaxgroup->getCombo(NULL,"intStatus=1");
	?>
    </select><input type="hidden" id="hidTaxRate" class="clsTaxAmount" value="0"/></td>
    <td><select name="cboCostCenter" id="cboCostCenter" class="clsCostCenter validate[required]" style="width:100%">
    <?php 
		echo $mst_financedimension->getCombo(NULL,"intStatus=1");
	?>
    </select></td>
  </tr>
  <?php 
  }
  else  { 
			$result		= $finance_other_receivable_invoice_details->getDetailData_result($invoiceNo,$invoiceYear);
			$totAmount 		= 0;
            $totTaxAmount 	= 0;
			while($row		= mysqli_fetch_array($result)) 
			{	
				$amount 		= ($row['QTY']*$row['UNIT_PRICE'])*((100-$row['DISCOUNT'])/100);
				$finalAmount 	= $amount;
  ?>
	 <tr class="normalfnt cls_tr_firstRow">
   <td style="text-align:center"><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>
    <td><select name="cboLedgerAc" id="cboLedgerAc" class="clsLedgerAc validate[required]"  style="width:100%" >
    	 <?php
           echo $obj_get_GLCombo->getGLCombo('OTHER_RECEIVABLE_INVOICE',$row['CHART_OF_ACCOUNT_ID']);
         ?>
    </select></td>
    <td><textarea name="txtItemDisc" id="txtItemDisc" class="clsItemDesc validate[required]" style="width:100%;height:19px;resize:none"><?php echo $row['ITEM_DESCRIPTION']?></textarea></td>
    <td><select name="cboUOM" id="cboUOM" class="clsUOM validate[required]" style="width:100%">
    <?php 
		echo $mst_units->getCombo($row['UNIT_ID'],"intStatus=1");
	?>
    </select></td>
    <td><input type="textbox" id="txtQty" class="clsQty validate[required,custom[number]] cls_input_number_validation" style="width:100%;text-align:right" value="<?php echo $row['QTY']?>" /></td>
    <td><input type="textbox" id="txtUnitPrice" class="clsUnitPrice validate[required,decimal[2],custom[number]] cls_input_number_validation" style="width:100%;text-align:right" value="<?php echo $row['UNIT_PRICE']?>"></td>
    <td><input type="textbox" id="txtDiscount" class="clsDiscount validate[decimal[2],custom[number]] cls_input_number_validation" style="width:100%;text-align:right" value="<?php echo $row['DISCOUNT']?>"></td>
    <td><input type="textbox" id="txtAmount" class="clsAmount" style="width:98%;text-align:right" value="<?php echo number_format($finalAmount, 2, '.', '')?>" disabled="disabled" /></td>
    <td><select name="cboTax" id="cboTax" class="clsTax" style="width:100%">
    <?php 
	echo $mst_financetaxgroup->getCombo($row['TAX_GROUP_ID'],"intStatus=1");
	?>
    </select><input type="hidden" id="hidTaxRate" class="clsTaxAmount" value="<?php echo $row['TAX_AMOUNT']?>"/></td>
    <td><select name="cboCostCenter" id="cboCostCenter" class="clsCostCenter" style="width:100%">
    <?php 
		echo $mst_financedimension->getCombo($row['COST_CENTER'],"intStatus=1");
	?>
    </select></td>
  </tr>  
  <?php 
 		 $totAmount 		= $totAmount+$finalAmount;
		$totTaxAmount 	= $totTaxAmount+$row['TAX_AMOUNT'];
			}
	   $grandTotal 	= $totAmount+$totTaxAmount; 
	
  }
  ?>
 
</table></div>
</td>			
                </tr>
                <tr>
                
                <td style="text-align:right" class="normalfnt"><table width="100%" border="0">
                    <tbody><tr>
   <td width="79%" class="normalfnt" style="text-align:right"></td>
                        <td width="10%" class="normalfnt" style="text-align:right">Sub Total</td>
                        <td width="8%" class="normalfnt" style="text-align:right">:</td>
                       <td> <input type="text" name="txtSubTotal" disabled="disabled" id="txtSubTotal" style="width:100px;text-align:right" value="<?php echo number_format($totAmount, 2, '.', '');?>">
                        </td>
                    </tr>
                    <tr>
    <td width="79%" class="normalfnt" style="text-align:right"></td>
                        <td width="10%" class="normalfnt" style="text-align:right">Tax</td>
                        <td width="8%" class="normalfnt" style="text-align:right">:</td>
                       <td> <input name="txtTaxTotal" type="text" disabled="disabled" id="txtTaxTotal" style="width:100px;text-align:right" value="<?php echo number_format($totTaxAmount, 2, '.', ''); ?>" />
                        </td>
                    </tr>
                    <tr>
   <td style="text-align:right" class="normalfnt"></td>
                        <td style="text-align:right" class="normalfnt">Total</td>
                        <td style="text-align:right" class="normalfnt">:</td>       
                        <td><input type="text" value="<?php echo number_format($grandTotal, 2, '.', ''); ?>" style="width:100px;text-align:right" id="txtTotal" disabled="disabled" name="txtTotal">
                        </td>
                    </tr>
                </tbody></table></td>
                <td class="normalfnt">
                
                </td>
            </tr>
                  <tr >
        <td  height="32" >
            <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            <tr>
            	<td align="center" class="tableBorder_allRound"><a href="?q=1078" class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" name="butSave" style=" <?php if($save_perm['type']){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>">Save</a><a class="button white medium" id="butConfirm" name="butConfirm" style=" <?php if($appr_perm['type']){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>">Approve</a><a class="button white medium" id="butCancle" name="butCancle" style=" <?php if($cancel_perm['type']){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>">Cancel</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
            </tr>
            </table>
        </td>
    </tr>
            </table></td>
        </tr>
      
      </table>
    </div>
  </div>
</form>

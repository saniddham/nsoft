var menuID	= 1078;
$(document).ready(function(e) {
	$('#frmOtherReceivable').validationEngine();
    $('#frmOtherReceivable #butInsertRow').die('click').live('click',addNewRow);
	$('#frmOtherReceivable .clsDel').die('click').live('click',deleteRow);
	$('#frmOtherReceivable .txtQty').die('keyup').live('keyup',setAmount);
	$('#frmOtherReceivable .clsUnitPrice').die('keyup').live('keyup',setAmount);
	$('#frmOtherReceivable .clsDiscount').die('keyup').live('keyup',setAmount);
	$('#frmOtherReceivable .clsTax').die('change').live('change',function(){calculateTax(this)});
	$('#frmOtherReceivable .clsQty').die('blur').live('blur',function(){calculateTax(this)});
	$('#frmOtherReceivable .clsUnitPrice').die('blur').live('blur',function(){calculateTax(this)});
	$('#frmOtherReceivable .clsDiscount').die('blur').live('blur',function(){calculateTax(this)});
	$('#frmOtherReceivable #cboCurrency').die('change').live('change',loadExchangeRate);
	$('#frmOtherReceivable #cboCustomer').die('change').live('change',loadCurrency);
	$('#frmOtherReceivable #butSave').die('click').live('click',saveData);
	$('#frmOtherReceivable #butConfirm').die('click').live('click',urlApprove);
	$('#frmOtherReceivable #butReport').die('click').live('click',urlReport);
	$('#frmOtherReceivable #butCancle').die('click').live('click',urlCancel);
	$('#frmOtherInvoiceReport #butRptConfirm').die('click').live('click',approve);
	$('#frmOtherInvoiceReport #butRptCancel').die('click').live('click',cancel);
	$('#frmOtherInvoiceReport #butRptReject').die('click').live('click',reject);

});

function addNewRow()
{
	if($('#frmOtherReceivable').validationEngine('validate'))
	{
		$('#frmOtherReceivable #tblOtherIncome tbody tr:last').after("<tr>"+$('#frmOtherReceivable #tblOtherIncome .cls_tr_firstRow').html()+"</tr>");
		$('#frmOtherReceivable #tblOtherIncome tbody tr:last #cboLedgerAc').val('');
		$('#frmOtherReceivable #tblOtherIncome tbody tr:last #txtItemDisc').val('');
		$('#frmOtherReceivable #tblOtherIncome tbody tr:last #cboUOM').val('');
		$('#frmOtherReceivable #tblOtherIncome tbody tr:last #txtQty').val('');
		$('#frmOtherReceivable #tblOtherIncome tbody tr:last #txtUnitPrice').val('');
		$('#frmOtherReceivable #tblOtherIncome tbody tr:last #txtDiscount').val('');	
		$('#frmOtherReceivable #tblOtherIncome tbody tr:last #txtAmount').val(0);	
		$('#frmOtherReceivable #tblOtherIncome tbody tr:last #cboTax').val('');
		$('#frmOtherReceivable #tblOtherIncome tbody tr:last #hidTaxRate').val(0);		
	}
}

function deleteRow()
{
	var delRowCount = parseInt(document.getElementById('tblOtherIncome').rows.length);
	
	if(delRowCount>3)
		$(this).parent().parent().remove();
	
	if(parseInt(document.getElementById('tblOtherIncome').rows.length)==3)
		$('#frmOtherReceivable #tblOtherIncome tbody tr:last').addClass('cls_tr_firstRow');
	
	CaculateTotalValue();
}

function setAmount()
{
	var qty 		= parseFloat($(this).parent().parent().find('.clsQty').val()==''?0:$(this).parent().parent().find('.clsQty').val());
	var unitPrice 	= parseFloat($(this).parent().parent().find('.clsUnitPrice').val()==''?0:$(this).parent().parent().find('.clsUnitPrice').val());
	var discount	= parseFloat($(this).parent().parent().find('.clsDiscount').val()==''?0:$(this).parent().parent().find('.clsDiscount').val());
	//alert(discount);
	var amount		= (qty*unitPrice*(100-discount)/100);
	
	$(this).parent().parent().find('.clsAmount').val(RoundNumber(amount,2));
	
	CaculateTotalValue();
}

function CaculateTotalValue()
{
	var totalValue		= 0;
	var grandTotal		= 0;
	var taxValue		= 0;
	
	$('#frmOtherReceivable #tblOtherIncome .clsAmount').each(function(){
		
		totalValue += parseFloat($(this).val());
		taxValue   += parseFloat($(this).parent().parent().find('.clsTaxAmount').val());	
	});
	
	taxValue	 = RoundNumber(taxValue,2);
	totalValue	 = RoundNumber(totalValue,2);
	grandTotal	 = parseFloat(totalValue)+parseFloat(taxValue);
	
	$('#frmOtherReceivable #txtSubTotal').val(totalValue);
	$('#frmOtherReceivable #txtTaxTotal').val(taxValue);	
	$('#frmOtherReceivable #txtTotal').val(RoundNumber(grandTotal,2));
}

function calculateTax(obj)
{
	var url 	=  "controller.php?q="+menuID+"&requestType=calculateTax";
	var data 	= "taxId="+$(obj).parent().parent().find('.clsTax').val()+"&amount="+$(obj).parent().parent().find('.clsAmount').val();
	var httpobj = $.ajax({
							url:url,
							data:data,
							dataType:'json',
							type:'POST',
							async:false,
							success:function(json)
							{
								$(obj).parent().parent().find('.clsTaxAmount').val(RoundNumber(json.TaxValue,2));
							}
						});	
	CaculateTotalValue();	
}

function loadExchangeRate()
{
	if($('#frmOtherReceivable #cboCurrency').val()=='')
	{
		$('#frmOtherReceivable #txtCurrencyRate').val('0.0000');
		return;
	}
	var url 	= "controller.php?q="+menuID+"&requestType=loadExchangeRate";
	var data 	= "currencyId="+$('#frmOtherReceivable #cboCurrency').val()+"&date="+$('#frmOtherReceivable #txtDate').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmOtherReceivable #txtCurrencyRate').val(json.exchgRate);
			}
	});
}

function loadCurrency()
{
	if($(this).val() == '')
	{
		$('#frmOtherReceivable #cboCurrency').val('');
		clearAll();
	}
	clearAll();
 	var url 	= "controller.php?q="+menuID+"&requestType=loadCurrency";
	var data 	= "customerId="+$(this).val();
	data 	   += "&date="+$('#frmOtherReceivable #txtDate').val();
	
	var obj 	= $.ajax({
						url:url,
						dataType:'json',
						type:'POST',  
						data:data,
						async:false,
						success:function(json){
								
								$('#frmOtherReceivable #cboCurrency').val(json.currencyId);
								$('#frmOtherReceivable #txtCurrencyRate').val(json.exchgRate);
						},
						error:function(xhr,status){
							
						}		
						});
 
}

function saveData()
{
	if(!IsProcessMonthLocked_date($('#frmOtherReceivable #txtDate').val()))
		return;
		
	showWaiting();
	var invoiceNo	= $('#frmOtherReceivable #txtInvNo').val();
	var invoiceYear	= $('#frmOtherReceivable #txtInvYear').val();	
	var customerID	= $('#frmOtherReceivable #cboCustomer').val();
	var currencyID	= $('#frmOtherReceivable #cboCurrency').val();
	var referenceNo	= $('#frmOtherReceivable #txtReferenceNo').val();
	var remark		= $('#frmOtherReceivable #txtRemarks').val();
	var invDate		= $('#frmOtherReceivable #txtDate').val();
	var grandTotal	= $('#frmOtherReceivable #txtTotal').val();
	
	if($('#frmOtherReceivable').validationEngine('validate'))
	{
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"invoiceNo":"'+invoiceNo+'",' ;
							arrHeader += '"invoiceYear":"'+invoiceYear+'",' ;
							arrHeader += '"customerID":"'+customerID+'",' ;
							arrHeader += '"currencyID":"'+currencyID+'",' ;
							arrHeader += '"remark":'+URLEncode_json(remark)+',' ;
							arrHeader += '"referenceNo":'+URLEncode_json(referenceNo)+',' ;
							arrHeader += '"invDate":"'+invDate+'",' ;
							arrHeader += '"grandTotal":"'+grandTotal+'"' ;
			arrHeader += ' }';
			
		var chkStatus	= false;
		var arrDetails	= "";
		$('#tblOtherIncome .clsAmount').each(function(){
			
			var chartOfAccId	= $(this).parent().parent().find('.clsLedgerAc').val();
			var itemDisc		= $(this).parent().parent().find('.clsItemDesc').val();
 			var uom			 	= $(this).parent().parent().find('.clsUOM').val();
			var qty		 		= $(this).parent().parent().find('.clsQty').val();
			var amonut			= $(this).val();
			var unitPrice 		= $(this).parent().parent().find('.clsUnitPrice').val();
			var discount 		= $(this).parent().parent().find('.clsDiscount').val();
			var taxId 			= $(this).parent().parent().find('.clsTax').val();
			var costCenter 		= $(this).parent().parent().find('.clsCostCenter').val();
			var taxAmount 		= $(this).parent().parent().find('.clsTaxAmount').val();

			if(parseFloat(amonut)>0)
			{
				chkStatus   = true;
				arrDetails += "{";
				arrDetails += '"chartOfAccId":"'+ chartOfAccId +'",' ;
				arrDetails += '"itemDisc":'+ URLEncode_json(itemDisc) +',' ;
				arrDetails += '"uom":"'+ uom +'",' ;
				arrDetails += '"qty":"'+ qty +'",' ;
				arrDetails += '"unitPrice":"'+ unitPrice +'",' ;
				arrDetails += '"discount":"'+ discount +'",' ;
				arrDetails += '"taxId":"'+ taxId +'",' ;
				arrDetails += '"costCenter":"'+ costCenter +'",' ;
				arrDetails += '"taxAmount":"'+ taxAmount +'"' ;
				arrDetails +=  '},';
			}
			
		});
		
		if(!chkStatus)
		{
			$('#frmOtherReceivable #butSave').validationEngine('showPrompt','No records to save.','fail');
			hideWaiting();
			return;
		}
		
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		
		var arrHeader	= arrHeader;
		var arrDetails	= '['+arrDetails+']';
		data+="&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		var url = "controller.php?q="+menuID;
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmOtherReceivable #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						$('#frmOtherReceivable #txtInvNo').val(json.InvoiceNo);
						$('#frmOtherReceivable #txtInvYear').val(json.InvoiceYear);
						if(json.app_perm)
							$('#frmOtherReceivable #butConfirm').show();
						if(json.cancel_perm)
							$('#frmOtherReceivable #butCancle').show();
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
						$('#frmOtherReceivable #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
	}
	else
	{
		hideWaiting();
	}
}

function urlApprove()
	{
		var url  = "?q=1087&invoiceNo="+$('#frmOtherReceivable #txtInvNo').val();
			url += "&invoiceYear="+$('#frmOtherReceivable #txtInvYear').val();
			url += "&mode=Confirm";
		window.open(url,'other_invoice_report.php');
	}
function urlCancel()
	{
		var url  = "?q=1087&invoiceNo="+$('#frmOtherReceivable #txtInvNo').val();
			url += "&invoiceYear="+$('#frmOtherReceivable #txtInvYear').val();
			url += "&mode=Cancel";
		window.open(url,'other_invoice_report.php');
	}
function urlReport()
	{
		var url  = "?q=1087&invoiceNo="+$('#frmOtherReceivable #txtInvNo').val();
			url += "&invoiceYear="+$('#frmOtherReceivable #txtInvYear').val();
		window.open(url,'other_invoice_report.php');
	}


function approve()
{
	var val = $.prompt('Are you sure you want to approve this Other Receivable Invoice ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
			showWaiting();
			var url = "controller.php?q=1087"+window.location.search+'&requestType=approve';
			var obj = $.ajax({
				url:url,
				type:'post',
				dataType: "json",  
				data:'',
				async:false,						
				success:function(json){
					$('#frmOtherInvoiceReport #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//var t=setTimeout("alertx1()",1000);
						window.location.href = window.location.href;
						window.opener.location.reload();
						return;
					}
					 hideWaiting();
				},
				error:function(xhr,status){						
						$('#frmOtherInvoiceReport #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						//var t=setTimeout("alertx1()",3000);
						 hideWaiting();
				}		
			});	
		}			  
	}});		
}
function reject()
{
	var val = $.prompt('Are you sure you want to reject this invoice?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "controller.php?q=1087"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,						
						success:function(json){
								$('#frmOtherInvoiceReport #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									//var t=setTimeout("alertxR2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){								
								$('#frmOtherInvoiceReport #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								//var t=setTimeout("alertxR2()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});		
}
function cancel()
{
	var val = $.prompt('Are you sure you want to Cancel this invoice ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url 	 ="controller.php?q=1087"+window.location.search+'&requestType=cancel';
					
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType:'json',  
						async:false,
						success:function(json){
					$('#frmOtherInvoiceReport #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//var t=setTimeout("alertx1()",1000);
						window.location.href = window.location.href;
						window.opener.location.reload();
						return;
					}
					 hideWaiting();
					},
					error:function(xhr,status){						
							$('#frmOtherInvoiceReport #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							//var t=setTimeout("alertx1()",3000);
							 hideWaiting();
					}		
						});
					
					}
				
			}});
}
function clearAll()
{
	$("#frmOtherReceivable #tblOtherIncome tr:gt(2)").remove();
	$('#frmOtherReceivable #tblOtherIncome #cboLedgerAc').val('');
	$('#frmOtherReceivable #tblOtherIncome #txtItemDisc').val('');
	$('#frmOtherReceivable #tblOtherIncome #cboUOM').val('');
	$('#frmOtherReceivable #tblOtherIncome .clsAmount').val('');
	$('#frmOtherReceivable #tblOtherIncome #txtQty').val('');
	$('#frmOtherReceivable #tblOtherIncome #txtUnitPrice').val('');
	$('#frmOtherReceivable #tblOtherIncome #txtDiscount').val('');
	$('#frmOtherReceivable #tblOtherIncome #cboTax').val('');
	$('#frmOtherReceivable #tblOtherIncome #cboCostCenter').val('');
	$('#frmOtherReceivable #txtSubTotal').val('');
	$('#frmOtherReceivable #txtTaxTotal').val('');
	$('#frmOtherReceivable #txtTotal').val('');
	$('#frmOtherReceivable #txtReferenceNo').val('');
	$('#frmOtherReceivable #txtRemarks').val('');
	
	return;
}
function RoundNumber(num,dec)
{
	num = parseFloat(num).toFixed(parseFloat(dec)+4);
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return parseFloat(result).toFixed(dec);
}
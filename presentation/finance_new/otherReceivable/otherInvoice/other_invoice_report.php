<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$location	 	= $sessions->getLocationId();
$userId  		= $sessions->getUserId();
$companyId  	= $sessions->getCompanyId(); 
$programCode	= 'P1078';
$invoiceNo 		= $_REQUEST['invoiceNo'];
$invoiceYear 	= $_REQUEST['invoiceYear'];
$mode 			= $_REQUEST['mode'];

include_once "class/tables/finance_other_receivable_invoice_header.php";	$finance_other_receivable_invoice_header		= new  finance_other_receivable_invoice_header($db);
include_once "class/tables/finance_other_receivable_invoice_details.php";	$finance_other_receivable_invoice_details		= new finance_other_receivable_invoice_details($db);
include_once "class/tables/finance_other_receivable_invoice_approve_by.php";$finance_other_receivable_invoice_approve_by	= new finance_other_receivable_invoice_approve_by($db);
include_once "class/tables/mst_customer.php";								$mst_customer									= new mst_customer($db);
include_once "class/tables/mst_financecurrency.php";						$mst_financecurrency							= new mst_financecurrency($db);
include_once "class/tables/finance_mst_chartofaccount.php";					$finance_mst_chartofaccount						= new finance_mst_chartofaccount($db);
include_once "class/tables/mst_units.php";									$mst_units										= new mst_units($db);
include_once "class/finance/cls_convert_amount_to_word.php";				$obj_AmtName	   								= new Cls_Convert_Amount_To_Word($db);
include_once "class/tables/sys_users.php";									$sys_users										= new sys_users($db);
include_once "class/tables/menupermision.php"; 								$menupermision									= new menupermision($db);
include_once "class/tables/mst_locations.php";								$mst_locations									= new mst_locations($db);
include_once "class/tables/mst_invoicetype.php";							$mst_invoicetype								= new mst_invoicetype($db);

$finance_other_receivable_invoice_header->set($invoiceNo,$invoiceYear);
$invoiceDate	= $finance_other_receivable_invoice_header->getBILL_DATE();
$customerID		= $finance_other_receivable_invoice_header->getCUSTOMER_ID();
$currencyID		= $finance_other_receivable_invoice_header->getCURRENCY_ID();
$remark			= $finance_other_receivable_invoice_header->getREMARKS();
$refNo			= $finance_other_receivable_invoice_header->getREFERENCE_NO();
$status			= $finance_other_receivable_invoice_header->getSTATUS();
$approveLevel	= $finance_other_receivable_invoice_header->getAPPROVE_LEVELS();
$creteCompanyId	= $finance_other_receivable_invoice_header->getCOMPANY_ID();
$locationId		= $finance_other_receivable_invoice_header->getLOCATION_ID();
$createdBy		= $finance_other_receivable_invoice_header->getCREATED_BY();
$invoicePdfRpt	= '';

$header_array['STATUS']	= $status;
$header_array['LEVELS'] = $approveLevel;

$menupermision->set($programCode,$userId);

$perRejArr				= $menupermision->checkMenuPermision('Reject',$status,$approveLevel);
$perApproArr			= $menupermision->checkMenuPermision('Approve',$status,$approveLevel);
$perCancelArr			= $menupermision->checkMenuPermision('Cancel',$status,$approveLevel);
$pdfPermission			= $menupermision->getintExportToPDF();

$permision_reject		= ($perRejArr['type']?1:0);
$permision_confirm		= ($perApproArr['type']?1:0);
$permision_cancel		= ($perCancelArr['type']?1:0);

$mst_customer->set($customerID);
$customer				= $mst_customer->getstrName();
$invoiceType			= $mst_customer->getstrInvoiceType();

if($invoiceType!='')
{
	$mst_invoicetype->set($invoiceType);
	$invoicePdfRpt		= $mst_invoicetype->getOTHER_RECEIVE_INVOICE_REPORT_NAME();
}

$mst_financecurrency->set($currencyID);
$currency		= $mst_financecurrency->getstrCode();
?>
<head>
<title>Other Receivable - Bill Invoice Report</title>
<script type="application/javascript" src="presentation/finance_new/otherReceivable/otherInvoice/other_invoice_js.js"></script>
<style>
#apDiv1 {
	position: absolute;
	left: 276px;
	top: 143px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
</style>

</head>


<body>
<div id="rpt_div_main">
<?php include 'presentation/report_ribbon_watermark.php'?>
<div id="rpt_div_table">

<form id="frmOtherInvoiceReport" name="frmOtherInvoiceReport" method="post" action="invoice.php" autocomplete="off">
<table width="900" align="center">
    <tr>
		<td colspan="2">	
				 <?php include 'report_header_latest.php'?>
			</td>
	</tr>
    
    <tr>
    	<td colspan="2" class="reportHeader" align="center">OTHER RECEIVABLE INVOICE REPORT</td>
    </tr>
    <tr>
    <td colspan="2"><table width="100%" align="center">
     <?php include "presentation/report_approve_status_and_buttons_latest.php" ?>
    </table></td>
    </tr>
    <tr>
        <td colspan="2">
            <table width="100%" border="0" class="normalfnt">
                <tr>
                    <td width="14%">&nbsp;</td>
                    <td width="1%">&nbsp;</td>
                    <td width="44%">&nbsp;</td>
                    <td width="14%">&nbsp;</td>
                    <td width="1%">&nbsp;</td>
                    <td width="26%">&nbsp;</td>
                </tr>
                <tr>
                    <td>Invoice No</td>
                    <td>:</td>
            		<td><?php echo $invoiceNo.'/'.$invoiceYear ?></td>
                    <td>Invoiced Date</td>
                    <td>:</td>
                    <td><?php echo $invoiceDate;?></td>
                </tr>
                <tr>
                    <td>Customer</td>
                    <td>:</td>
                    <td><?php echo $customer?></td>
                    <td>Currency</td>
                    <td>:</td>
                    <td><?php echo $currency?></td>
                </tr>
                <tr>
                    <td valign="top">Remarks</td>
                    <td valign="top">:</td>
                    <td rowspan="2" valign="top"><?php echo $remark?></td>
                    <td valign="top">Reference No</td>
                     <td valign="top">:</td>
                    <td valign="top"><?php echo $refNo?></td>
                </tr>
                <tr>
                    <td height="16">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="right"><a class="button green small no-print" id = "rpt_export_pdf" style=" <?php  if($pdfPermission == 1){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  }?>" target="<?php echo $invoicePdfRpt;?>" href="<?php echo "presentation/finance_new/otherReceivable/otherInvoice/".$invoicePdfRpt."?invoiceNo=$invoiceNo&invoiceYear=$invoiceYear"?>">Click here to print invoice</a></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" >
            <table width="100%" border="0" class="rptBordered" id="tblMain">
            <thead>
                <tr>
                    <th width="4%">&nbsp;</th>
                    <th width="26%">Item / Expenses</th>
                    <th width="20%">Item Desc.</th>
                    <th width="10%">UOM</th>
                    <th width="10%">Unit Price</th>
                    <th width="10%">Qty</th>
                    <th width="10%">Discount(%)</th>
                    <th width="10%">Value</th>
                </tr>
            </thead>        
            <tbody>
         	<?php 
			$result			= $finance_other_receivable_invoice_details->getDetailData_result($invoiceNo,$invoiceYear);
			$totAmount 		= 0;
            $totTaxAmount 	= 0;
			$i				= 0;
			while($row		= mysqli_fetch_array($result)) 
			{	
				$chartOfAcc	= $row['CHART_OF_ACCOUNT_NAME'];
				$UOM		= $row['UNIT'];
			
				$amount 		= ($row['QTY']*$row['UNIT_PRICE'])*((100-$row['DISCOUNT'])/100);
				$finalAmount 	= $amount;
				?>
					<tr>
						<td align="center"><?php echo ++$i;?></td>
						<td ><?php echo $chartOfAcc?></td>
						<td width="20%"><?php echo $row['ITEM_DESCRIPTION'];?></td>
						<td width="10%"><?php echo $UOM;?></td>
						<td width="10%" style="text-align:right"><?php echo $row['UNIT_PRICE']?></td>
						<td class="cls_td_invoice cls_Subtract" style="text-align:right"><?php echo $row['QTY']?></td>
						<td style="text-align:right"><?php echo $row['DISCOUNT']?></td>
						<td class="cls_td_value" style="text-align:right"><?php echo number_format($finalAmount,2); ?></td>
					</tr>
             <?php 
				$totAmount 		= $totAmount+$finalAmount;
				$totTaxAmount 	= $totTaxAmount+$row['TAX_AMOUNT'];
            }
            $grandTotal 	= $totAmount+$totTaxAmount; 
			?>
            </tbody>
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="2" class="normalfnt"><b><?php echo $obj_AmtName->Convert_Amount($grandTotal,$currency);?></b></td>
    </tr>
    <tr>
    	<td width="658" valign="top">&nbsp;</td>
        <td width="230" valign="top" >
            <table width="230" border="0" align="right" class="normalfnt">
                <tr>
                    <td width="100">Sub Total</td>
                    <td width="10">:</td>
                    <td width="106" align="right"><?php echo number_format($totAmount,2);?></td>
                </tr>
                <tr>
                    <td>Tax Total</td>
                    <td>:</td>
                    <td align="right"><?php echo number_format($totTaxAmount,2);?></td>
                </tr>
                <tr>
                    <td>Grand Total</td>
                    <td>:</td>
                    <td align="right"><?php echo number_format($grandTotal,2);?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="2" class="normalfnt">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="2">
        <?php
			$db->connect();$db->begin();
			$sys_users->set($finance_other_receivable_invoice_header->getCREATED_BY());
			
			$creator		= $sys_users->getstrUserName();
			$createdDate	= $finance_other_receivable_invoice_header->getCREATED_DATE();
			
			$resultA		= $finance_other_receivable_invoice_approve_by->getApprovedByData($invoiceNo,$invoiceYear);
			include "presentation/report_approvedBy_details.php";
			$db->disconnect();
 	?>
        </td></tr>
	<?php
    if($mode=='Confirm' && $status > 1)
    {
    ?>
    <tr>
    <?php
    $createCompanyId = $creteCompanyId;
    $url  = "presentation/send_to_approval_latest.php";
    $url .= "?status=$status";										// * set recent status
    $url .= "&approveLevels=$approveLevel";								// * set approve levels in order header
    $url .= "&programCode=$programCode";								// * program code (ex:P10001)
    $url .= "&program=Other Receivable Invoice";									// * program name (ex:Purchase Order)
    $url .= "&companyId=$creteCompanyId";									// * created company id
    $url .= "&createUserId=$createdBy";
    
    $url .= "&subject=OTHER RECEIVABLE INVOICE FOR APPROVAL (".$invoiceNo."/".$invoiceYear.")";	
    
    $url .= "&statement1=Please Approve this";	
    $url .= "&statement2=to Approve this";	
                    // * doc year
    $url .= "&link=".urlencode(base64_encode(MAIN_URL."?q=1087&invoiceNo=".$invoiceNo."&invoiceYear=".$invoiceYear."&mode=Confirm"));
    ?>
    <td colspan="2" align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"></iframe></td>
    </tr>
    <?php
    }
    ?>
<tr>
  <td colspan="2" align="center" class="normalfntMid"><strong>Printed Date:  <?php echo date("Y/m/d") ?></strong></td>
</tr>
       </table>
</form></div></div>
</body>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$location	 	= $sessions->getLocationId();
$userId  		= $sessions->getUserId();
$companyId  	= $sessions->getCompanyId(); 
$programCode	= 'P1092';

include_once "class/finance/cls_get_gldetails.php";								$obj_get_GLCombo				= new Cls_Get_GLDetails($db);
include_once "class/tables/mst_customer.php";									$mst_customer					= new mst_customer($db);
include_once "class/tables/mst_financecurrency.php";							$mst_financecurrency			= new mst_financecurrency($db);
include_once "class/tables/mst_financepaymentsmethods.php";						$mst_financepaymentsmethods		= new mst_financepaymentsmethods($db);
include_once "class/tables/mst_financedimension.php";							$mst_financedimension			= new mst_financedimension($db);
include_once "class/tables/mst_financeexchangerate.php";						$mst_financeexchangerate						= new mst_financeexchangerate($db);
include_once "class/tables/mst_customer.php";									$mst_customer									= new mst_customer($db);
include_once "class/tables/finance_other_receivable_receipt_details.php";		$finance_other_receivable_receipt_details		= new finance_other_receivable_receipt_details($db);	
include_once "class/tables/finance_other_receivable_invoice_header.php";		$finance_other_receivable_invoice_header		= new finance_other_receivable_invoice_header($db);
include_once "class/tables/finance_other_receivable_transaction.php";			$finance_other_receivable_transaction			= new finance_other_receivable_transaction($db);
include_once "class/tables/finance_other_receivable_receipt_header.php";		$finance_other_receivable_receipt_header		= new finance_other_receivable_receipt_header($db);
include_once "class/dateTime.php";												$dateTimes										= new dateTimes($db);
include_once "class/tables/finance_other_receivable_receipt_gl.php";			$finance_other_receivable_receipt_gl			= new finance_other_receivable_receipt_gl($db);
include_once "class/tables/menupermision.php"; 									$menupermision									= new menupermision($db);
include_once "class/tables/finance_mst_chartofaccount.php";						$finance_mst_chartofaccount						= new finance_mst_chartofaccount($db);
include_once "class/tables/sys_users.php";										$sys_users										= new sys_users($db);
include_once "class/tables/finance_other_receivable_receipt_approve_by.php";	$finance_other_receivable_receipt_approve_by	= new finance_other_receivable_receipt_approve_by($db);
include_once "class/finance/cls_convert_amount_to_word.php";				$obj_AmtName	   								= new Cls_Convert_Amount_To_Word($db);

$receiveNo		= $_REQUEST['receiveNo'];
$receiveYear	= $_REQUEST['receiveYear'];
$mode 			= $_REQUEST['mode'];

$finance_other_receivable_receipt_header->set($receiveNo,$receiveYear);
$customerID		= $finance_other_receivable_receipt_header->getCUSTOMER_ID();
$currencyID		= $finance_other_receivable_receipt_header->getCURRENCY_ID();
$recvDate		= $finance_other_receivable_receipt_header->getRECEIPT_DATE();
$recvMode		= $finance_other_receivable_receipt_header->getPAYMENT_MODE_ID();
$remark			= $finance_other_receivable_receipt_header->getREMARKS();
$bankRefNo		= $finance_other_receivable_receipt_header->getBANK_REFERENCE_NO();
$status			= $finance_other_receivable_receipt_header->getSTATUS();
$appLevel		= $finance_other_receivable_receipt_header->getAPPROVE_LEVELS();
$creteCompanyId	= $finance_other_receivable_receipt_header->getCOMPANY_ID();
$locationId		= $finance_other_receivable_receipt_header->getLOCATION_ID();
$createdBy		= $finance_other_receivable_receipt_header->getCREATED_BY();


$GL_result		= $finance_other_receivable_receipt_gl->getGLDetails_result($receiveNo,$receiveYear);
$glID			= $GL_result['CHART_OF_ACCOUNT_ID'];
$finance_mst_chartofaccount->set($glID);
$chartOfAcc		= $finance_mst_chartofaccount->getCHART_OF_ACCOUNT_NAME();
$glAmount		= $GL_result['AMOUNT'];
$memo			= $GL_result['REMARKS'];
$costCenterID	= $GL_result['COST_CENTER'];
$mst_financedimension->set($costCenterID);
$costCenter		= $mst_financedimension->getstrName();

$header_array['STATUS']	= $status;
$header_array['LEVELS'] = $appLevel;

$menupermision->set($programCode,$userId);

$perRejArr				= $menupermision->checkMenuPermision('Reject',$status,$appLevel);
$perApproArr			= $menupermision->checkMenuPermision('Approve',$status,$appLevel);
$perCancelArr			= $menupermision->checkMenuPermision('Cancel',$status,$appLevel);
$pdfPermission			= $menupermision->getintExportToPDF();
$permision_reject		= ($perRejArr['type']?1:0);
$permision_confirm		= ($perApproArr['type']?1:0);
$permision_cancel		= ($perCancelArr['type']?1:0);

$mst_customer->set($customerID);
$customer		= $mst_customer->getstrName();

$mst_financecurrency->set($currencyID);
$currency		= $mst_financecurrency->getstrCode();

$mst_financepaymentsmethods->set($recvMode);
$recevMethod	= $mst_financepaymentsmethods->getstrName();
?>
<head>
<title>Other Receivable - Receipt Report</title>
<script type="application/javascript" src="presentation/finance_new/otherReceivable/otherReceipt/other_receipt_js.js"></script>
<style>
#apDiv1 {
	position: absolute;
	left: 276px;
	top: 143px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
</style>

</head>


<body>
<div id="rpt_div_main">
<?php include 'presentation/report_ribbon_watermark.php'?>
<div id="rpt_div_table">

<form id="frmOtherReceiptReport" name="frmOtherReceiptReport" method="post" action="invoice.php" autocomplete="off">
<table width="900" align="center">
    <tr>
		<td colspan="2">
		<?php include 'report_header_latest.php'?>
        </td>
	</tr>
    
    <tr>
    	<td colspan="2" class="reportHeader" align="center">OTHER RECEIVABLE RECEIPT REPORT</td>
    </tr>
    <tr>
    <td colspan="2"><table width="100%" align="center">
     <?php include "presentation/report_approve_status_and_buttons_latest.php" ?>
    </table></td></tr>
    <tr>
        <td colspan="2">
            <table width="100%" border="0" class="normalfnt">
                <tr>
                    <td width="14%">&nbsp;</td>
                    <td width="1%">&nbsp;</td>
                    <td width="44%">&nbsp;</td>
                    <td width="14%">&nbsp;</td>
                    <td width="1%">&nbsp;</td>
                    <td width="26%">&nbsp;</td>
                </tr>
                <tr>
                    <td>Receipt No</td>
                    <td>:</td>
            		<td><?php echo $receiveNo.'/'.$receiveYear ?></td>
                    <td>Receipt Date</td>
                    <td>:</td>
                    <td id="receiptDate"><?php echo $recvDate;?></td>
                </tr>
                <tr>
                    <td>Customer</td>
                    <td>:</td>
                    <td><?php echo $customer?></td>
                    <td>Currency</td>
                    <td>:</td>
                    <td><?php echo $currency?></td>
                </tr>
                <tr>
                    <td>Receive Mode</td>
                    <td>:</td>
                    <td><?php echo $recevMethod?></td>
                    <td>Reference No</td>
                    <td>:</td>
                    <td><?php echo $bankRefNo?></td>
                </tr>
                <tr>
                    <td valign="top">Remarks</td>
                    <td valign="top">:</td>
                    <td rowspan="2" valign="top"><?php echo $remark?></td>
                    <td valign="top"></td>
                     <td valign="top"></td>
                    <td valign="top"></td>
                </tr>
                <tr>
                    <td height="16">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td style="text-align:right"><a class="button green small no-print"  id = "rpt_export_pdf" style=" <?php  if($pdfPermission == 1){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  }?>" target="rpt_other_receivable_receipt_pdf.php" href="presentation/finance_new/otherReceivable/otherReceipt/other_receipt_report_pdf.php?receiveNo=<?php echo $receiveNo; ?>&receiveYear=<?php echo $receiveYear; ?>">Click here to print receipt</a></td>
                </tr> 
            </table>
        </td>
    </tr>
    <tr>
         <td colspan="2" class="normalfnt"><table width="100%" border="0" class="rptBordered" id="tblMain2">
           <thead>
             <tr>
               <th width="3%">&nbsp;</th>
                <th width="15%">Invoice No</th>
                <th width="17%">Reference No</th>
                <th width="13%">Invoiced Date</th>
                <th width="13%">Invoiced Amount</th>
                 <th width="13%">Received Amount</th>
                <th width="13%">To Be Receive</th>
                <th width="13%">Receiving Amount</th>
             </tr>
           </thead>
           <tbody>
             <?php
$totAmount 		= 0;
							$detailResult	= $finance_other_receivable_receipt_details->getReceiptDetail_result($receiveNo,$receiveYear);
							while($row		= mysqli_fetch_array($detailResult))
							{
								$invoiceNo	= $row['BILL_INVOICE_NO'];
								$invoiceYear= $row['BILL_INVOICE_YEAR'];
								
								$finance_other_receivable_invoice_header->set($invoiceNo,$invoiceYear);
								$refNo		= $finance_other_receivable_invoice_header->getREFERENCE_NO();
								$invDate	= $finance_other_receivable_invoice_header->getBILL_DATE();
								
								$grandTotal	= $finance_other_receivable_transaction->getTransactionAMount($invoiceNo,$invoiceYear,'invoice');
								$paidAmt	= $finance_other_receivable_transaction->getTransactionAMount($invoiceNo,$invoiceYear,'payment');
								$tobeRecv	= $finance_other_receivable_transaction->getTransactionAMount($invoiceNo,$invoiceYear,NULL);
								$totReceiveAmt += $row['AMOUNT']; 
								
						?>
 
            <tr>
                <td align="center"><?php echo ++$i;?>.</td>
                <td class="clsInvoice" style="text-align:left"><?php echo $invoiceNo.'/'.$invoiceYear?></td>
                <td class="clsInvoice" style="text-align:left"><?php echo $refNo?></td>
                <td class="clsInvoiceDate" style="text-align:center"><?php echo $invDate?></td>
                <td class="clsInvoiceAmt" style="text-align:right"><?php echo number_format($grandTotal,2)?></td>
                <td class="clsPaidAmt" style="text-align:right"><?php echo number_format($paidAmt,2);?></td>
                <td class="clsToBePaid" style="text-align:right"><?php echo number_format($tobeRecv,2)?></td>
                <td class="clsPayAmount" style="text-align:right"><?php echo number_format($row['AMOUNT'],2); ?></td>
            </tr>
             <?php
}
?>
             <tr>
               <td colspan="7" align="right"><b>Total :</b></td>
               <td width="13%" style="text-align:right"><b><?php echo number_format($totReceiveAmt,2); ?></b></td>
             </tr>
           </tbody>
         </table></td>
       </tr>
       <tr><td>&nbsp;</td></tr>
       <tr>
        <td colspan="2"><table width="100%" border="0" class="rptBordered" id="tblMain">
        <thead>
          <tr>
            <th width="37%">Account</th>
            <th width="35%">Memo</th>
            <th width="16%">Cost Center</th>
            <th width="12%">Amount</th>
          </tr>
        </thead>        
        <tbody>

		<tr>
        	<td class="cls_td_salesOrderNo"><?php echo $chartOfAcc;?></td>
       	 	<td width="35%"><?php echo $memo?></td>
        	<td width="16%"><?php echo $costCenter?></td>
        	<td width="12%" style="text-align:right"><?php echo number_format($glAmount,2);?></td>
        </tr>
		<tr>
        	<td colspan="3" align="right"><b>Total :</b></td>
        	<td width="12%" style="text-align:right"><b><?php echo number_format($glAmount,2);?></b></td>
        </tr>
        </tbody>
        </table></td>
      </tr>
    <tr>
    	<td colspan="2" class="normalfnt"><b><?php echo $obj_AmtName->Convert_Amount($totReceiveAmt,$currency);?></b></td>
    </tr>
    <tr>
    	<td width="658" valign="top">&nbsp;</td>
        <td width="230" valign="top" >
            <table width="230" border="0" align="right" class="normalfnt">
                <tr>
                    <td width="100">Sub Total</td>
                    <td width="10">:</td>
                    <td width="106" align="right"><?php echo number_format($totReceiveAmt,2);?></td>
                </tr>
                <tr>
                    <td>GL Amount</td>
                    <td>:</td>
                    <td align="right"><?php echo number_format($glAmount,2);?></td>
                </tr>
               
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="2" class="normalfnt">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="2">
        <?php
			$db->connect();
			$sys_users->set($finance_other_receivable_receipt_header->getCREATED_BY());
			
			$creator		= $sys_users->getstrUserName();
			$createdDate	= $finance_other_receivable_receipt_header->getCREATED_DATE();
			
			$resultA		= $finance_other_receivable_receipt_approve_by->getApprovedByData($receiveNo,$receiveYear);
			include "presentation/report_approvedBy_details.php";
			$db->disconnect();
 	?>
        </td></tr>
	<?php
    if($mode=='Confirm' && $status > 1)
    {
    ?>
    <tr>
    <?php
    $createCompanyId = $creteCompanyId;
    $url  = "presentation/send_to_approval_latest.php";
    $url .= "?status=$status";										// * set recent status
    $url .= "&approveLevels=$appLevel";								// * set approve levels in order header
    $url .= "&programCode=$programCode";								// * program code (ex:P10001)
    $url .= "&program=Other Receivable Receipt";									// * program name (ex:Purchase Order)
    $url .= "&companyId=$creteCompanyId";									// * created company id
    $url .= "&createUserId=$createdBy";
    
    $url .= "&subject=OTHER RECEIVABLE RECEIPT FOR APPROVAL (".$receiveNo."/".$receiveYear.")";	
    
    $url .= "&statement1=Please Approve this";	
    $url .= "&statement2=to Approve this";	
                    // * doc year
    $url .= "&link=".urlencode(base64_encode(MAIN_URL."?q=1093&receiveNo=".$receiveNo."&receiveYear=".$receiveYear."&mode=Confirm"));
    ?>
    <td colspan="2" align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:100px;border:none"></iframe></td>
    </tr>
    <?php
    }
    ?>
<tr>
  <td colspan="2" align="center" class="normalfntMid"><strong>Printed Date:  <?php echo date("Y/m/d") ?></strong></td>
</tr>
      </table>
</form></div></div>
</body>
var menuID		= 1092;
$(document).ready(function(e) {
	$('#frmOtherReceipt').validationEngine();
    $('#frmOtherReceipt #cboCurrency').die('change').live('change',loadDetails);
	$('#frmOtherReceipt #cboCustomer').die('change').live('change',loadDetails);
	$('#frmOtherReceipt #butSave').die('click').live('click',saveData);
	$('#frmOtherReceipt #chkAll').die('click').live('click',checkAll);
	$('#frmOtherReceipt .clsChkInvoice').die('click').live('click',setCheckAmount);
	$('#frmOtherReceipt .clsReceiveAmt').die('keyup').live('keyup',setAmount);
	$('#frmOtherReceipt .clsAmount').die('keyup').live('keyup',setGLAmount);
	$('#frmOtherReceipt #butConfirm').die('click').live('click',urlApprove);
	$('#frmOtherReceipt #butReport').die('click').live('click',urlReport);
	$('#frmOtherReceipt #butCancle').die('click').live('click',urlCancel);
	$('#frmOtherReceiptReport #butRptConfirm').die('click').live('click',approve);
	$('#frmOtherReceiptReport #butRptCancel').die('click').live('click',cancel);
	$('#frmOtherReceiptReport #butRptReject').die('click').live('click',reject);

	
});

function loadExchangeRate()
{
	if($('#frmOtherReceipt #cboCurrency').val()=='')
	{
		$('#frmOtherReceipt #txtCurrencyRate').val('0.0000');
		return;
	}
	var url 	= "controller.php?q="+menuID+"&requestType=loadExchangeRate";
	var data 	= "currencyId="+$('#frmOtherReceipt #cboCurrency').val()+"&date="+$('#frmOtherReceipt #txtDate').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				
				$('#frmOtherReceipt #txtCurrencyRate').val(json.exchgRate);
				
			}
	});
}
function loadDetails(val)
{
	showWaiting();
	
	if($('#frmOtherReceipt #cboCustomer').val() == '')
	{
		$('#frmOtherReceipt #cboCurrency').val('');
		clearData();
		hideWaiting();
	}
	clearData();
	loadExchangeRate();
	var customerID	= $('#frmOtherReceipt #cboCustomer').val();
	var currency	= $('#frmOtherReceipt #cboCurrency').val();

	var url 	= "controller.php?q="+menuID+"&requestType=loadDetails";
	var data	= "customerID="+customerID;
		data	+= "&currency="+currency;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmOtherReceipt #tblMain tr:gt(2)').remove(); 
				$('#frmOtherReceipt #tblMain #tblBody').html(json.gridDetail);
				hideWaiting();

			},
			error:function(xhr,status){
					hideWaiting();
					return;
			}		
	});
	hideWaiting();

}
function checkAll()
{
	if($(this).attr('checked'))
	{
		$('#frmOtherReceipt .clsChkInvoice').attr("checked",true);
		
		$('#frmOtherReceipt #tblMain .clsChkInvoice').each(function(){
		   
		   if($(this).parent().parent().find('.clsReceiveAmt').val()=='' || $(this).parent().parent().find('.clsReceiveAmt').val()==0)
		   { 
				var tobeReceive = parseFloat($(this).parent().parent().find('.clsToBeReceive').html());
				$(this).parent().parent().find('.clsReceiveAmt').val(tobeReceive);	
		   }
		});
		
	}
	else
	{
		$('#frmOtherReceipt .clsAmount').val('');
		$('#frmOtherReceipt #totGLAlocAmount').html('');
		$('#frmOtherReceipt #totPayAmount').html('');
		$('#frmOtherReceipt .clsChkInvoice').attr("checked",false);
		$('#frmOtherReceipt .clsReceiveAmt').val(0);
	}
	CaculateTotalValue();
}
function saveData()
{
	if(!IsProcessMonthLocked_date($('#frmOtherReceipt #txtDate').val()))
		return;
		
	showWaiting();
	var receiveNo 		= $('#frmOtherReceipt #txtReceiveNo').val();
	var receiveYear 	= $('#frmOtherReceipt #txtReceiveYear ').val();
	var customerID 		= $('#frmOtherReceipt #cboCustomer').val();
	var receiveDate		= $('#frmOtherReceipt #txtDate').val();
	var currency 		= $('#frmOtherReceipt #cboCurrency').val();
	var Remarks 		= $('#frmOtherReceipt #txtRemarks').val();
	var recvMethod 		= $('#frmOtherReceipt #cboReceiveMethod').val();
	var bankRefNo 		= $('#frmOtherReceipt #txtBankRefNo').val();
	var accountId		= $('#frmOtherReceipt .clsAccounts').val();
	var glAmount		= $('#frmOtherReceipt .clsAmount').val();
	var memo			= $('#frmOtherReceipt .clsMemo').val();
	var costCenter		= $('#frmOtherReceipt .clsCostCener').val();
	
	if($('#frmOtherReceipt').validationEngine('validate'))
	{
		var totReceiveAmount	= parseFloat($('#frmOtherReceipt #totReceiveAmount').html());
		var totGLAlocAmount 	= parseFloat($('#frmOtherReceipt #totGLAlocAmount').html());
		
		if(totReceiveAmount!=totGLAlocAmount)
		{
			$('#frmOtherReceipt #totReceiveAmount').validationEngine('showPrompt','Total receive amount must equal to Total GL Alocated amonut.','fail');
			hideWaiting();
			return;
		}
				
		if(totReceiveAmount<=0 || totGLAlocAmount<=0)
		{
			$('#frmOtherReceipt #butSave').validationEngine('showPrompt','Total amount must greater than 0.','fail');
			hideWaiting();
			return;
		}
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"receiveNo":"'+receiveNo+'",' ;
							arrHeader += '"receiveYear":"'+receiveYear+'",' ;
							arrHeader += '"receiveDate":"'+receiveDate+'",' ;
							arrHeader += '"customerID":"'+customerID+'",' ;
							arrHeader += '"currency":"'+currency+'",' ;
							arrHeader += '"Remarks":'+URLEncode_json(Remarks)+',' ;
							arrHeader += '"recvMethod":"'+recvMethod+'",' ;
							arrHeader += '"bankRefNo":"'+bankRefNo+'",' ;
							arrHeader += '"totReceiveAmount":"'+totReceiveAmount+'",' ;
							arrHeader += '"totGLAlocAmount":"'+totGLAlocAmount+'",' ;
							arrHeader += '"accountId":"'+ accountId +'",' ;
							arrHeader += '"glAmount":"'+ glAmount +'",' ;
							arrHeader += '"memo":'+ URLEncode_json(memo) +',' ;
							arrHeader += '"costCenter":"'+ costCenter +'"' ;
			arrHeader += ' }';
		
		var chkStatus  	= false;
		var amtStatus  	= false;
		var arrDetails	= "";
		$('#frmOtherReceipt #tblMain .clsChkInvoice').each(function(){
			
			chkStatus  	= true;
			if($(this).attr('checked'))
			{
				var invoiceNoArr 	= $(this).parent().parent().find('.clsInvoice').attr('id');
				var receiveAmount 	= $(this).parent().parent().find('.clsReceiveAmt').val();
				
				if(parseFloat(receiveAmount)<=0)
				{
					$(this).parent().parent().find('.clsReceiveAmt').validationEngine('showPrompt','Amount must greater than 0.','fail');
					amtStatus = true;
					hideWaiting();
					return false;	
				}
				arrDetails += "{";
				arrDetails += '"invoiceNoArr":"'+ invoiceNoArr +'",' ;
				arrDetails += '"receiveAmount":"'+ receiveAmount +'"' ;
				arrDetails +=  '},';
			}
		});
		
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		var arrHeader	= arrHeader;
		var arrDetails	= '['+arrDetails+']';
		data+="&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		if(amtStatus)
			return;
		
		if(!chkStatus)
		{
			$(this).validationEngine('showPrompt','No records to save.','fail');
			hideWaiting();	
			return;
		}
			
		var url = "controller.php?q="+menuID;
			$.ajax({
					url:url,
					dataType:'json',
					type:'post',
					data:data,
					async:false,
					success:function(json){
						$('#frmOtherReceipt #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
						//	var t = setTimeout("alertx()",3000);
							$('#frmOtherReceipt #txtReceiveNo').val(json.receiveNo);
							$('#frmOtherReceipt #txtReceiveYear').val(json.receiveYear);
							if(json.app_perm)
								$('#frmOtherReceipt #butConfirm').show();
							if(json.cancel_perm)
								$('#frmOtherReceipt #butCancle').show();
							hideWaiting();
							return;
						}
						else
						{
							hideWaiting();
						}
					},
					error:function(xhr,status){
							
							$('#frmOtherReceipt #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							hideWaiting();
							return;
					}		
			});
	}
	else
	{
		hideWaiting();
	}
}
function setAmount()
{
	var tobeReceive 	= parseFloat($(this).parent().parent().find('.clsToBeReceive').html());
	if(parseFloat($(this).val())>tobeReceive)
	{
		$(this).val(tobeReceive);
	}
	if($(this).val() == '')
		$(this).val(0);
	$('#frmOtherReceipt .clsAmount').val('');
	CaculateTotalValue();
}
function CaculateTotalValue()
{
	var totalValue		= 0;
	var totGLValue		= 0;
	
	$('#frmOtherReceipt #tblMain .clsReceiveAmt').each(function(){
		if($(this).parent().parent().find('.clsChkInvoice').attr('checked'))
		{
			totalValue += parseFloat($(this).val());
		}
	});
	$('#frmOtherReceipt .clsAmount').val(RoundNumber(totalValue,2));	
	
	$('#frmOtherReceipt #tblGLMain .clsAmount').each(function(){
		
		totGLValue += parseFloat($(this).val()==''?0:$(this).val());	
		
	});
	totalValue	= RoundNumber(totalValue,2);
	totGLValue	= RoundNumber(totGLValue,2);
	
	$('#frmOtherReceipt #totReceiveAmount').html(totalValue);	
	$('#frmOtherReceipt #totGLAlocAmount').html(totGLValue);
}

function setCheckAmount()
{
	$('#frmOtherReceipt .clsAmount').val('');
	$('#frmOtherReceipt #totGLAlocAmount').html('');
	$('#frmOtherReceipt #totPayAmount').html('');
	
	var tobeReceive = parseFloat($(this).parent().parent().find('.clsToBeReceive').html());	
	$(this).parent().parent().find('.clsReceiveAmt').val(tobeReceive);
	
	$('#frmOtherReceipt .clsChkInvoice').each(function(){
        
		if($(this).attr('checked'))
		{
			$(this).parent().parent().find('.clsReceiveAmt').addClass('validate[required]');
		}
		else
		{
			$(this).parent().parent().find('.clsReceiveAmt').removeClass('validate[required]');
			$(this).parent().parent().find('.clsReceiveAmt').val(0);
			$('#frmOtherReceipt #chkAll').attr('checked' , false);
		}
    });
	CaculateTotalValue();
}

function setGLAmount()
{
	var obj			 	= this;
	var totGLAmount  	= 0;
	var totAmount 	 	= parseFloat($('#totPayAmount').html());
	$('#frmOtherReceipt .clsAmount').each(function(){

		if($(this).val()!="")
			totGLAmount += parseFloat($(this).val());
	});
	if(totGLAmount>totAmount)
	{
		obj.value 		= (parseFloat(obj.value)-parseFloat(parseFloat(totGLAmount)-parseFloat(totAmount)));
		totLedAmount 	= totAmount;
	}
	CaculateTotalValue();
}

function urlApprove()
	{
		var url  = "?q=1093&receiveNo="+$('#frmOtherReceipt #txtReceiveNo').val();
			url += "&receiveYear="+$('#frmOtherReceipt #txtReceiveYear').val();
			url += "&mode=Confirm";
		window.open(url,'other_receipt_report.php');
	}
function urlCancel()
	{
		var url  = "?q=1093&receiveNo="+$('#frmOtherReceipt #txtReceiveNo').val();
			url += "&receiveYear="+$('#frmOtherReceipt #txtReceiveYear').val();
			url += "&mode=Cancel";
		window.open(url,'other_receipt_report.php');
	}
function urlReport()
	{
		var url  = "?q=1093&receiveNo="+$('#frmOtherReceipt #txtReceiveNo').val();
			url += "&receiveYear="+$('#frmOtherReceipt #txtReceiveYear').val();
		window.open(url,'other_receipt_report.php');
	}


function approve()
{
	var val = $.prompt('Are you sure you want to approve this Receipt ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
			showWaiting();
			var url = "controller.php?q=1093"+window.location.search+'&requestType=approve';
			var obj = $.ajax({
				url:url,
				type:'post',
				dataType: "json",  
				data:'',
				async:false,						
				success:function(json){
					$('#frmOtherReceiptReport #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//var t=setTimeout("alertx1()",1000);
						window.location.href = window.location.href;
						window.opener.location.reload();
						return;
					}
					 hideWaiting();
				},
				error:function(xhr,status){						
						$('#frmOtherReceiptReport #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						//var t=setTimeout("alertx1()",3000);
						 hideWaiting();
				}		
			});	
		}			  
	}});		
}
function reject()
{
	var val = $.prompt('Are you sure you want to reject this receipt?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "controller.php?q=1093"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,						
						success:function(json){
								$('#frmOtherReceiptReport #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									//var t=setTimeout("alertxR2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){								
								$('#frmOtherReceiptReport #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								//var t=setTimeout("alertxR2()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});		
}
function cancel()
{

	var val = $.prompt('Are you sure you want to Cancel this receipt ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url 	 ="controller.php?q=1093"+window.location.search+'&requestType=cancel';
					
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType:'json',  
						async:false,
						success:function(json){
					$('#frmOtherReceiptReport #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//var t=setTimeout("alertx1()",1000);
						window.location.href = window.location.href;
						window.opener.location.reload();
						return;
					}
					 hideWaiting();
					},
					error:function(xhr,status){						
							$('#frmOtherReceiptReport #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							//var t=setTimeout("alertx1()",3000);
							 hideWaiting();
					}		
						});
					
					}
				
			}});
}

function clearData()
{
	$("#frmOtherReceipt #tblMain tr:gt(2)").remove();
	$('#frmOtherReceipt #chkAll').attr("checked",false);
	$('#frmOtherReceipt #tblGLMain').find('#txtAmmount').val('');
	$('#frmOtherReceipt #tblGLMain').find('#cboAccounts').val('');
	$('#frmOtherReceipt #tblGLMain').find('#txtMemo').val('');
	$('#frmOtherReceipt #tblGLMain').find('#cboCostCenter').val('');
	$('#frmOtherReceipt #totReceiveAmount').html('');
	$('#frmOtherReceipt #totGLAlocAmount').html('');
	$('#frmOtherReceipt #cboReceiveMethod').val('');
	$('#frmOtherReceipt #txtBankRefNo').val('');
	$('#frmOtherReceipt #txtRemarks').val('');
	return ;
	
}


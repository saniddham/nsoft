<?php
session_start();
ini_set('display_errors',0);

//BEGIN - INCLUDE FILES {
$backwardseperator 	= "../../../../";
include_once ("../../../../dataAccess/DBManager2.php");
include_once ("../../../../libraries/fpdf/fpdf.php");
include_once ("../../../../class/masterData/companies/cls_companies_get.php");
include_once ("../../../../class/tables/finance_other_receivable_receipt_header.php");
include_once ("../../../../class/tables/finance_other_receivable_receipt_details.php");
include_once ("../../../../class/finance/cls_convert_amount_to_word.php");
include_once ("../../../../class/sessions.php");
include_once ("../../../../class/finance/cls_common_get.php");
include_once ("../../../../class/tables/menupermision.php"); 									

//END	- INCLUDE FILES }
$db							= new DBManager2();
$db->connect();
//BEGIN	- CREATE OBJECTS {
$finance_other_receivable_receipt_header		= new finance_other_receivable_receipt_header($db);
$finance_other_receivable_receipt_details		= new finance_other_receivable_receipt_details($db);
$obj_company_get								= new cls_companies_get($db);
$obj_convert_amount_to_word						= new Cls_Convert_Amount_To_Word($db);
$obj_fin_com									= new Cls_Common_Get($db);
$sessions										= new sessions($db);
$menupermision									= new menupermision($db);
//END	- CREATE OBJECTS }
$session_companyId	= $sessions->getCompanyId();
$userId  			= $sessions->getUserId();

//BEGIN - PARAMETERS {

$menupermision->set('P1092',$userId);
$pdfPermission			= $menupermision->getintExportToPDF();
if(!$pdfPermission)
{	
	echo "No Permission to View PDF Report";
	return;
}

$receiveNo		= $_REQUEST["receiveNo"];
$receiveYear	= $_REQUEST["receiveYear"];

$title			= "OTHER RECEIVABLE RECEIPT";
$font			= 'Times';

//END	- PARAMETERS }
class PDF extends FPDF
{
	function Header1()
	{
		global $result_reportHeader;
		global $title;
		global $session_companyId;
		global $font;
		$h = 5;
		
		$this->Image('../../../../images/screenline.jpg',8,9);
		$this->SetFont($font,'B',14);$this->Cell(0,$h,$result_reportHeader["COMPANY_NAME"],'0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,$result_reportHeader["LOCATION_ADDRESS"].' '.$result_reportHeader["LOCATION_STREET"].' '.$result_reportHeader["LOCATION_CITY"].' '.$result_reportHeader["COMPANY_COUNTRY"].'.','0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,"Tel : ".$result_reportHeader["LOCATION_PHONE"].'  '."Fax : ".$result_reportHeader["LOCATION_FAX"],'0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,"E-mail : ".$result_reportHeader["LOCATION_EMAIL"].'  '."Web : ".$result_reportHeader["LOCATION_WEB"],'0',1,'C');
		$this->SetFont($font,'B',12);$this->Cell(0,10,$title,'0',1,'C');		
	}
	
	function SetPageWaterMark($status,$printStatus)
	{
		//Put the watermark
		switch($status)
		{
			case 1:
				$msg	= ($printStatus==1?"DUPLICATE":"");
				break;
			case '-2':
				$msg	= "CANCELED RECEIPT";
				break;
			default:
				$msg	= "NOT A VALID RECEIPT";
				break;
		}
		$this->SetFont('Times','B',50);
		$this->SetTextColor(255,192,203);
		$this->RotatedText(35,190,$msg,45);
		$this->SetTextColor(0,0,0);
	}
	
	function RotatedText($x, $y, $txt, $angle)
	{
		//Text rotated around its origin
		$this->Rotate($angle,$x,$y);
		$this->Text($x,$y,$txt);
		$this->Rotate(0);
	}
		
	function ReportHeader($result_header,$result_reportHeader,$receiptNo)
	{
		$h = 5;
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Receipt No','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$receiptNo,'0',1,'L');
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Receipt Date','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["RECEIPT_DATE"],'0',1,'L');
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Payment Method','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["PAYMENT_MODE"],'0',1,'L');
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Currency','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["CURRENCY_CODE"],'0',1,'L');
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Customer Information','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["CUSTOMER_NAME"],'0',1,'L');
		$this->SetX(52);
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["ADDRESS"],'0',1,'L');
		$this->SetX(52);
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["CITY"],'0',1,'L');
		$this->SetX(52);
		$this->SetFont($font,'',11);$this->Cell(80,$h,$result_header["COUNTRY_NAME"].'.','0',1,'L');		
		$this->SetFont($font,'',11);$this->Cell(40,$h,'Remarks','0',0,'L');
		$this->SetFont($font,'',11);$this->Cell(2,$h,':','0',0,'L');
		$this->SetFont($font,'',11);$this->MultiCell(0,5,$result_header["REMARKS"],'0','',0);
	}
	
	function CreateTable($result_header,$result_inv_tot,$obj_convert_amount_to_word)
	{
		$count 			= 1;
		
		$this->Table_header();
			
		while($rowAmt = mysqli_fetch_array($result_inv_tot))
		{
				$this->Table_Body($rowAmt);
			
			if($count == 12)
			{
				$this->AddPage();
				$this->Table_header();
				$count = 0;	
			}
			$count++;
		}
		$this->Table_Body_Validate($count);	
		$this->Table_footer($result_header,$obj_convert_amount_to_word);
	}
	
	function Table_header()
	{
		$this->SetXY($this->GetX(),$this->GetY()+2);
		$h = 10;
		$this->SetFont('Times','B',10);
		$this->Cell(50,$h,'Invoice No','1',0,'C');
		$this->Cell(120,$h,'Items ','1',0,'C');
		$this->Cell(30,$h,'Total Value','1',0,'R');
		$this->Ln();		
	}
		
	function Table_Body($rowAmt)
	{	
		global $finance_other_receivable_receipt_details;
		
		$h	= 4;
		$this->SetFont('Times','',8);
		$this->Cell(50,$h,$rowAmt['CON_INVOICE'],'LTR',0,'L');
		$result_details	= $finance_other_receivable_receipt_details->getPdfReceiptDetail_result($rowAmt['RECEIPT_NO'],$rowAmt['RECEIPT_YEAR'],$rowAmt['BILL_INVOICE_NO'],$rowAmt['BILL_INVOICE_YEAR']);
		$row = mysqli_fetch_array($result_details);
		
		$this->Cell(120,$h,$row["GL_NAME"].'['.$row["ITEM_DESCRIPTION"].']','LTR',0,'L');
		$this->Cell(30,$h,number_format($rowAmt['AMOUNT'],2),'LTR',0,'R');
		$this->Ln();
		if(mysqli_num_rows($result_details)>1)
		{
			while($row = mysqli_fetch_array($result_details))
			{
				$this->Cell(50,$h,'','LR',0,'L');
				$this->Cell(120,$h,$row["GL_NAME"].'['.$row["ITEM_DESCRIPTION"].']','LR',0,'L');
				$this->Cell(30,$h,'','LR',0,'R');
				$this->Ln();
			}
		}

	}

	function Table_Body_Validate($count)
	{
		$h1	= 0;
		for($i=$count;$i<=14;$i++)
		{
			$h1 += 4;
		}
		$this->SetFont('Times','',7);
		$this->Cell(170,$h1,"",'1',0,'C');
		$this->Cell(30,$h1,"",'1',0,'C');
		$this->Ln();
	}
	
	function Table_footer($result_header,$obj_convert_amount_to_word)
	{
		$this->SetY($this->GetY());
		$totDamage = 0;
		$this->SetFont('Times','',10);
		$this->Cell(4,10,"",'LBT',0,'L');
		$this->Cell(166,10,$result_header["MEMO"],'TB',0,'L');
		$this->Cell(30,10,"(".number_format($result_header["AMOUNT"],2).")",'1',0,'R');
		$this->Ln();
		
		$this->SetFont('Times','B',10);$this->Cell(26,10,"Through",'LBT',0,'L');
		$this->Cell(4,10,":",'TB',0,'L');
		$this->SetFont('Times','',10);$this->Cell(140,10,"",'BTR',0,'C');
		$this->Cell(30,10,"",'1',0,'C');
		$this->Ln();
		
		$this->Cell(4,10,"",'LBT',0,'L');
		$this->Cell(166,10,$result_header["BANK_GL_NAME"],'TB',0,'L');
		$this->Cell(30,10,"",'1',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','B',10);$this->Cell(26,10,"Amount(In Words)",'LBT',0,'L');
		$this->Cell(4,10,":",'TB',0,'L');
		$this->SetFont('Times','',10);$this->Cell(140,10,"",'BTR',0,'C');
		$this->Cell(30,10,"",'1',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','B',10);
		$this->Cell(170,10,$obj_convert_amount_to_word->Convert_Amount($result_header["AMOUNT"],$result_header["CURRENCY_CODE"]),'1',0,'L');
		$this->Cell(30,10,"",'1',0,'R');
		$this->Ln();
		
		$this->SetFont('Times','B',10);
		$this->Cell(170,10,"Total",'1',0,'R');
		$this->Cell(30,10,number_format($result_header["VALUE"],2),'1',0,'R');
		$this->Ln();
		
	}	
	function Footer1()
	{
		global $session_companyId;
		global $result_header;	
			
		$this->SetFont('Times','',8);
		
		$this->SetXY(20,$this->GetY()+10);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetX(140);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetXY(20,$this->GetY()+3);
		$this->Cell(50,5,'Authorized By',0,0,'C');
		
		$this->SetX(140);		
		$this->Cell(50,5,'Checked By',0,0,'C');	
		
		$this->SetXY(20,$this->GetY()+10);
		$this->Cell(50,5,$result_header["CREATED_BY_NAME"],0,0,'C');
		
		$this->SetXY(20,$this->GetY()+2);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetX(140);
		$this->Cell(50,5,'..........................................',0,0,'C');

		$this->SetXY(140,$this->GetY()+3);
		$this->Cell(50,5,'Received By',0,0,'C');

		$this->SetX(20);
		$this->Cell(50,5,'Prepared By',0,0,'C');	
	}
	
	function Footer()
	{
		$this->SetY(-5);
		$this->SetFont('Times','I',8);
		$this->Cell(0,2,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf 					= new PDF('P','mm','Letter');

$result_header 			= $finance_other_receivable_receipt_header->getPDFHeader_result($receiveNo,$receiveYear);
$result_reportHeader	= $obj_company_get->GetCompanyReportHeader($result_header["LOCATION_ID"]);
$receiptNo				= $obj_fin_com->getSerialNo($result_header["SERIAL_NO"],$result_header["SERIAL_DATE"],$session_companyId,'RunQuery');
$result_inv_tot			= $finance_other_receivable_receipt_details->getReceiptAmount($receiveNo,$receiveYear);
//$result_details			= $finance_other_receivable_receipt_details->getPdfReceiptDetail_result($receiveNo,$receiveYear,$result_inv_tot);

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetPageWaterMark($result_header["STATUS"],$result_header["PRINT_STATUS"]);
$pdf->Header1();
$pdf->ReportHeader($result_header,$result_reportHeader,$receiptNo);
$pdf->CreateTable($result_header,$result_inv_tot,$obj_convert_amount_to_word);
$pdf->Footer1();

$finance_other_receivable_receipt_header->set($receiveNo,$receiveYear);
$status		= $finance_other_receivable_receipt_header->getSTATUS();
if($status == 1)
{
$finance_other_receivable_receipt_header->setPRINT_STATUS(1);

$result_arr	= $finance_other_receivable_receipt_header->commit();	
$db->commit();		
if(!$result_arr['status'])
	throw new Exception($result_arr['msg']);
}

$pdf->Output('rpt_other_receivable_receipt_pdf.pdf','I');
$db->disconnect()
?>
<?php 

include_once "class/finance/cls_get_gldetails.php";							$obj_get_GLCombo								= new Cls_Get_GLDetails($db);
include_once "class/tables/mst_customer.php";								$mst_customer									= new mst_customer($db);
include_once "class/tables/mst_financecurrency.php";						$mst_financecurrency							= new mst_financecurrency($db);
include_once "class/tables/mst_financepaymentsmethods.php";					$mst_financepaymentsmethods						= new mst_financepaymentsmethods($db);
include_once "class/tables/mst_financedimension.php";						$mst_financedimension							= new mst_financedimension($db);
include_once "class/tables/mst_financeexchangerate.php";					$mst_financeexchangerate						= new mst_financeexchangerate($db);
include_once "class/tables/mst_customer.php";								$mst_customer									= new mst_customer($db);
include_once "class/tables/finance_other_receivable_receipt_details.php";	$finance_other_receivable_receipt_details		= new finance_other_receivable_receipt_details($db);	
include_once "class/tables/finance_other_receivable_invoice_header.php";	$finance_other_receivable_invoice_header		= new finance_other_receivable_invoice_header($db);
include_once "class/tables/finance_other_receivable_transaction.php";		$finance_other_receivable_transaction			= new finance_other_receivable_transaction($db);
include_once "class/tables/finance_other_receivable_receipt_header.php";	$finance_other_receivable_receipt_header		= new finance_other_receivable_receipt_header($db);
include_once "class/dateTime.php";											$dateTimes										= new dateTimes($db);
include_once "class/tables/finance_other_receivable_receipt_gl.php";		$finance_other_receivable_receipt_gl			= new finance_other_receivable_receipt_gl($db);
require_once "class/tables/menus_special.php";            					$menus_special               					= new menus_special($db);

$locationId		= $sessions->getLocationId();
$userId			= $sessions->getUserId();
$companyId		= $sessions->getCompanyId();

$dateSP			= $menus_special->loadSpecialMenuPermission(67,$userId);

$receiveNo		= $_REQUEST['receiveNo'];
$receiveYear	= $_REQUEST['receiveYear'];

if($receiveNo != '' && $receiveYear != '')
{
$finance_other_receivable_receipt_header->set($receiveNo,$receiveYear);
$customerID		= $finance_other_receivable_receipt_header->getCUSTOMER_ID();
$currencyID		= $finance_other_receivable_receipt_header->getCURRENCY_ID();
$recvDate		= $finance_other_receivable_receipt_header->getRECEIPT_DATE();
$recvMode		= $finance_other_receivable_receipt_header->getPAYMENT_MODE_ID();
$remark			= $finance_other_receivable_receipt_header->getREMARKS();
$bankRefNo		= $finance_other_receivable_receipt_header->getBANK_REFERENCE_NO();
$status			= $finance_other_receivable_receipt_header->getSTATUS();
$appLevel		= $finance_other_receivable_receipt_header->getAPPROVE_LEVELS();

$GL_result		= $finance_other_receivable_receipt_gl->select('*',NULL,"RECEIPT_NO = '$receiveNo' AND RECEIPT_YEAR = '$receiveYear'",NULL,NULL);
$rowGL			= mysqli_fetch_array($GL_result);
$glID			= $rowGL['CHART_OF_ACCOUNT_ID'];
$glAmount		= $rowGL['AMOUNT'];
$memo			= $rowGL['REMARKS'];
$costCenter		= $rowGL['COST_CENTER'];

$save_perm		= $menupermision->checkMenuPermision('Edit',$status,$appLevel);
$appr_perm		= $menupermision->checkMenuPermision('Approve',$status,$appLevel);
$cancel_perm	= $menupermision->checkMenuPermision('Cancel',$status,$appLevel);
}
else
{
$save_perm		= $menupermision->checkMenuPermision('Edit','','');
}


?>
<title>Other Receivable Receipt</title>

<!--<script type="text/javascript" src="presentation/finance_new/otherPayble/otherBillPayment/otherBillPayment-js.js"></script>-->

<form id="frmOtherReceipt" name="frmOtherReceipt" method="post">
<div align="center">
<div class="trans_layoutL">
<div class="trans_text">Other Receivable Receipt</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
                <tr><td><table width="100%">
                <tr>
                	<td width="17%" class="normalfnt">Receipt No</td>
                	<td width="44%" class="normalfnt"><input name="txtReceiveNo" type="text" disabled="disabled" id="txtReceiveNo" style="width:60px" value="<?php echo $receiveNo?>" />&nbsp;<input name="txtReceiveYear" type="text" disabled="disabled" id="txtReceiveYear" style="width:35px" value="<?php echo $receiveYear?>" /></td>
                	<td width="24%" class="normalfnt">&nbsp;</td>
                	<td width="15%" class="normalfnt"></td>
                </tr>
                <tr>
                    <td class="normalfnt">Customer<span class="compulsoryRed">*</span></td>
                    <td class="normalfnt"><select name="cboCustomer" id="cboCustomer"  style="width:224px" class="validate[required]" >
               <?php
			   	if($receiveNo != '' && $receiveYear != '')
					echo $mst_customer->getCombo($customerID,"intStatus = 1 AND intTypeId = 13");
				else	
					echo $mst_customer->getCombo(NULL,"intStatus = 1 AND intTypeId = 13");
				
			    ?>
                </select></td>
                    <td class="normalfnt">Date</td>
                    <td class="normalfnt"><input name="txtDate" type="text" value="<?php echo ($receiveNo != '' && $receiveYear != ''?$recvDate:date("Y-m-d"));?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" <?php echo($dateSP!=1?'disabled="disabled"':''); ?> /></td>
                </tr>
                <tr>
                    <td class="normalfnt">Currency <span class="compulsoryRed">*</span></td>
                    <td class="normalfnt"><select name="cboCurrency" id="cboCurrency"  style="width:106px" class="validate[required] CurrencyCalExchangeRate" >
               <?php 
			   	if($receiveNo != '' && $receiveYear != '')
			  		echo $mst_financecurrency->getCombo($currencyID,"intStatus = 1");
				else
					echo $mst_financecurrency->getCombo(NULL,"intStatus = 1");
			   ?>
                </select></td>
                    <td class="normalfnt">Rate</td>
                    <td class="normalfnt"><input type="text" name="txtCurrencyRate" id="txtCurrencyRate" style="width:100px" disabled="disabled" value="0.0000" class="cls_txt_exchangeRate"/></td>
                </tr>
                <tr>
                    <td class="normalfnt" valign="top">Remarks</td>
                    <td rowspan="3" class="normalfnt" valign="top"><textarea name="txtRemarks" id="txtRemarks" style="width:220px;resize:none" rows="2"><?php echo $remark; ?></textarea></td>
                    <td class="normalfnt" valign="top">Receivable Method <span class="compulsoryRed">*</span></td>
                    <td class="normalfnt" valign="top"><select name="cboReceiveMethod" id="cboReceiveMethod"  style="width:104px" class="validate[required]" >
               <?php 
			   if($receiveNo != '' && $receiveYear != '')
			  		echo $mst_financepaymentsmethods->getCombo($recvMode,"intStatus = 1");
				else
					echo $mst_financepaymentsmethods->getCombo(NULL,"intStatus = 1");
			   ?>
                </select></td>
                </tr>
                <tr>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt" valign="top">Reference Number</td>
                  <td class="normalfnt" valign="top"><input type="text" name="txtBankRefNo" id="txtBankRefNo" style="width:100px" value="<?php echo $bankRefNo;?>" maxlength="100" /></td>
                </tr>
                <tr>
                    <td class="normalfnt">&nbsp;</td>
                    <td class="normalfnt" valign="top">&nbsp;</td>
                    <td class="normalfnt" valign="top"></td>
                </tr>
                </table></td></tr>
                <tr><td><table width="100%">
                <tr>
                	<td colspan="4" class="normalfnt"><div style="overflow:scroll;overflow-x:hidden;width:auto;height:250px;">
                	<table width="100%" border="0" class="bordered" id="tblMain">
                        <thead>
                            <tr>
                                <th width="4%"><input type="checkbox" id="chkAll" name="chkAll" class="clsChkAll" <?php echo($paymentNo!=''?'checked="checked"':''); ?> ></th>
                                <th width="11%">Invoice No</th>
                                <th width="16%">Reference No</th>
                                <th width="12%">Invoiced Date</th>
                                <th width="15%">Invoiced Amount</th>
                                <th width="14%">Received Amount</th>
                                <th width="14%">To Be Received</th>
                                <th width="14%">Receiving Amount <span class="compulsoryRed">*</span></th>
                            </tr>
                        </thead>        
               			<tbody id="tblBody">
						<?php 
							$totReceiveAmt	= 0;
							$detailResult	= $finance_other_receivable_receipt_details->getReceiptDetail_result($receiveNo,$receiveYear);
							while($row		= mysqli_fetch_array($detailResult))
							{
								$invoiceNo	= $row['BILL_INVOICE_NO'];
								$invoiceYear= $row['BILL_INVOICE_YEAR'];
								
								$finance_other_receivable_invoice_header->set($invoiceNo,$invoiceYear);
								$refNo		= $finance_other_receivable_invoice_header->getREFERENCE_NO();
								$invDate	= $finance_other_receivable_invoice_header->getBILL_DATE();
								
								$grandTotal	= $finance_other_receivable_transaction->getTransactionAMount($invoiceNo,$invoiceYear,'invoice');
								$paidAmt	= $finance_other_receivable_transaction->getTransactionAMount($invoiceNo,$invoiceYear,'payment');
								$tobeRecv	= $finance_other_receivable_transaction->getTransactionAMount($invoiceNo,$invoiceYear,NULL);

								
								$totReceiveAmt = $totReceiveAmt + $row['AMOUNT']; 
								
						?>
                            <tr class="normalfnt" >
                                <td style="text-align:center"><input type="checkbox" id="chkInvoice" name="chkInvoice" class="clsChkInvoice validate[minCheckbox[1]]" checked="checked"></td>
                                <td class="clsInvoice" id="<?php echo $invoiceNo.'/'.$invoiceYear?>" style="text-align:left"><a href="?q=1087&invoiceNo=<?php echo $invoiceNo?>&invoiceYear=<?php echo $invoiceYear ?>" target="_blank"><?php echo $invoiceNo.'/'.$invoiceYear?></a></td>
                                <td class="clsReference" style="text-align:left"><?php echo $refNo?></td>
                                <td class="clsInvoiceDate" style="text-align:center"><?php echo $invDate;?></td>
                                <td class="clsInvoiceAmt" style="text-align:right"><?php echo number_format($grandTotal, 2, '.', '');?></td>
                                <td class="clsPaidAmt" style="text-align:right"><?php echo number_format($paidAmt, 2, '.', '');?></td>
                                <td class="clsToBeReceive" style="text-align:right"><?php echo number_format($tobeRecv, 2, '.', '')?></td>
                                <td class="clsPayAmount" style="text-align:center"><input type="textbox" id="txtReceiveAmt" name="txtReceiveAmt" style="width:110px;text-align:right" value="<?php echo $row['AMOUNT']?>" class="clsReceiveAmt validate[required,decimal[2],custom[number]] cls_input_number_validation"></td>
                            </tr>
					<?php } ?>
                		</tbody>
                	</table>
                    
                    </div>
                    </td>
                </tr></table></td></tr>
                <tr>
                <td class="normalfnt"><div style="width:auto;" id="divGrid">
                <table width="100%" class="bordered" id="tblGLMain" >
                    <thead>
                        <tr style="display:none">
                            <th colspan="5" >GL Allocation
                            <div style="float:right"><a class="button white small" id="butInsertRow">Add New GL</a></div></th>
                        </tr>
                        <tr>
                            <th width="29%" >Account <span class="compulsoryRed">*</span></th>
                            <th width="13%" >Amount <span class="compulsoryRed">*</span></th>
                            <th width="41%" >Remarks </th>
                            <th width="14%" >Cost Center <span class="compulsoryRed">*</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="29%" style="text-align:center" ><select name="cboAccounts" id="cboAccounts" style="width:100%" class="clsAccounts validate[required]">
                            <?php
                            echo $obj_get_GLCombo->getBankGLCombo($glID);
                            ?>
                            </select></td>
                            <td width="13%" style="text-align:center" ><input name="txtAmmount" type="text" id="txtAmmount" style="width:100%;text-align:right" class="validate[required,decimal[2],custom[number]] clsAmount cls_input_number_validation" value="<?php echo $glAmount?>" /></td>
                            <td width="41%" style="text-align:center" ><input type="text" name="txtMemo" id="txtMemo" style="width:100%" class="clsMemo" value="<?php echo $memo?>" /></td>
                            <td width="14%" style="text-align:center" >
                            <select name="cboCostCenter" id="cboCostCenter" class="clsCostCener validate[required]" style="width:100%">
							<?php 
							if($receiveNo != '' && $receiveYear != '')
								echo $mst_financedimension->getCombo($costCenter,"intStatus = 1");
							else
                                echo $mst_financedimension->getCombo(NULL,"intStatus=1");
                            ?>
                            </select></td>
                        </tr>
                    </tbody>
                </table>
                </div>
                </td>
                </tr>
                <tr><td><table width="100%">
                <tr>
                	<td colspan="2" class="normalfnt" valign="top">&nbsp;</td>
                	<td colspan="2" class="normalfnt" style="text-align:right"><table width="301" border="0" align="right" class="normalfnt">
                <tr>
                	<td width="166">Total Receiving Amount</td>
                	<td width="11" style="text-align:center">:</td>
                	<td width="91" style="text-align:right;" id="totReceiveAmount"><?php echo number_format($totReceiveAmt, 2, '.', '');?></td>
                </tr>
                <tr>
                	<td>Total GL Allocated Amount</td>
                	<td style="text-align:center"><b>:</b></td>
                	<td style="text-align:right" id="totGLAlocAmount"><?php echo number_format($glAmount, 2, '.', '');?></td>
                </tr>
                </table></td></tr>
                </table>
              </td>
            </tr> 
         </table>
        </td>
    </tr>
    <tr>
        <td height="32" >
            <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
            	<td align="center" class="tableBorder_allRound"><a href="?q=1092" class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" name="butSave" style=" <?php if($save_perm['type']){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>">Save</a><a class="button white medium" id="butConfirm" name="butConfirm" style=" <?php if($appr_perm['type']){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>">Approve</a><a class="button white medium" id="butCancle" name="butCancle" style=" <?php if($cancel_perm['type']){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>">Cancel</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>
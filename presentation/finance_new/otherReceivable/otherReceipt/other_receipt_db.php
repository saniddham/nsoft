<?php


/*
SAVE
	1.	check whether month end is locked	
	2. check permissions.
	3. check exchange rate available.
	4. check bank exchange rate available.
	5. display error msg if details not available.
	6. check saved location when edit.
	7. check invoice status (invoice status should be approved).
	8. check invoice balance.

APPROVE
	1. check whether month end is locked.
	2. check permissions
	3. check saved location when approve.
	4. check invoice status (invoice status should be approved).
	5. check invoice balance	
	6. insert into `finance_other_receivable_transaction` table - when final approval. [Document Type 'BILLPAYMENT' , Amount '-'] 
	7. insert into `finance_transaction` table - when final approval [Document Type = 'BILLPAYMENT',TRANSACTION_CATEGORY 'OR',TRANSACTION_CATEGORY_ID 'GL_ID'] [Customer - C , Bank - D]

CANCEL
	1. check whether month end is locked.
	2. check permissions.
	3. check saved location when cancel.
	4. update header status as '-2'.
	5. rollback 'finance_other_receivable_transaction'
	6. rollback 'finance_transaction'
*/
try{
	$userId 			= $sessions->getUserId();
	$locationId	  		= $sessions->getLocationId();
	$companyId			= $sessions->getCompanyId();
	$menuID				= '1092';
	$programCode		= 'P1092';
	
	include_once "class/tables/mst_financeexchangerate.php";						$mst_financeexchangerate						= new mst_financeexchangerate($db);
	include_once "class/tables/mst_customer.php";									$mst_customer									= new mst_customer($db);
	include_once "class/tables/finance_other_receivable_invoice_header.php";		$finance_other_receivable_invoice_header		= new finance_other_receivable_invoice_header($db);
	include_once "class/tables/finance_other_receivable_invoice_details.php";		$finance_other_receivable_invoice_details		= new finance_other_receivable_invoice_details($db);
	include_once "class/tables/finance_other_receivable_receipt_details.php";		$finance_other_receivable_receipt_details		= new finance_other_receivable_receipt_details($db);	
	include_once "class/tables/finance_other_receivable_transaction.php";			$finance_other_receivable_transaction			= new finance_other_receivable_transaction($db);
	include_once "class/tables/menupermision.php";									$menupermision									= new menupermision($db);
	include_once "class/tables/sys_approvelevels.php";								$sys_approvelevels								= new sys_approvelevels($db);
	include_once "class/tables/sys_no.php";											$sys_no											= new sys_no($db);
	include_once "class/tables/mst_financeexchangerate.php";						$mst_financeexchangerate						= new mst_financeexchangerate($db);
	include_once "class/tables/finance_other_receivable_receipt_header.php";		$finance_other_receivable_receipt_header		= new finance_other_receivable_receipt_header($db);
	include_once "class/dateTime.php";												$dateTimes										= new dateTimes($db);
	include_once "class/tables/finance_other_receivable_receipt_gl.php";			$finance_other_receivable_receipt_gl			= new finance_other_receivable_receipt_gl($db);
	include_once "class/tables/finance_other_receivable_receipt_approve_by.php";	$finance_other_receivable_receipt_approve_by	= new finance_other_receivable_receipt_approve_by($db);
	include_once "class/tables/finance_mst_chartofaccount.php";						$finance_mst_chartofaccount						= new finance_mst_chartofaccount($db);
	include_once "class/tables/finance_mst_chartofaccount_bank.php";				$finance_mst_chartofaccount_bank				= new finance_mst_chartofaccount_bank($db);
	include_once "class/tables/finance_transaction.php";							$finance_transaction							= new finance_transaction($db);
	include_once "class/tables/finance_month_end_process.php";						$finance_month_end_process						= new finance_month_end_process($db);
	include_once "class/tables/mst_financeexchangerate_bankwise.php";				$mst_financeexchangerate_bankwise				= new mst_financeexchangerate_bankwise($db);
	include_once "class/masterData/exchange_rate/cls_exchange_rate_get.php";		$cls_exchange_rate_get							= new cls_exchange_rate_get($db);	
	
	$requestType	= $_REQUEST['requestType'];
	
	if($requestType == 'loadExchangeRate')
	{
		$currency				= $_REQUEST['currencyId'];
		$date	  				= $_REQUEST['date'];
		
		$mst_financeexchangerate->set($currency,$date,$companyId);
		$exchangeRate			= $mst_financeexchangerate->getdblExcAvgRate();
		$response['exchgRate']	= $exchangeRate;
			
	}
	else if($requestType == 'loadDetails')
	{
		$customerID		= $_REQUEST['customerID'];
		$currency		= $_REQUEST['currency'];
		$content		= '';
	
		$result			= $finance_other_receivable_invoice_header->getCustomerWiseHeaderDetail($customerID,$currency,$locationId);
		while($row		= mysqli_fetch_array($result))
		{
			$grandTotal	= $finance_other_receivable_transaction->getTransactionAMount($row['BILL_INVOICE_NO'],$row['BILL_INVOICE_YEAR'],'invoice');
			$paidAmt	= $finance_other_receivable_transaction->getTransactionAMount($row['BILL_INVOICE_NO'],$row['BILL_INVOICE_YEAR'],'payment');
			$tobeRecv	= $finance_other_receivable_transaction->getTransactionAMount($row['BILL_INVOICE_NO'],$row['BILL_INVOICE_YEAR'],NULL);
			if($tobeRecv > 0)
			{
				$content	.= "<tr class=\"normalfnt\" >";
				$content	.= "<td style=\"text-align:center\"><input type=\"checkbox\" id=\"chkInvoice\" name=\"chkInvoice\" class=\"clsChkInvoice validate[minCheckbox[1]]\"></td>";
				$content	.= "<td class=\"clsInvoice\" id=\"".$row['BILL_INVOICE_NO'].'/'.$row['BILL_INVOICE_YEAR']."\" style=\"text-align:center\"><a href=\"?q=1087&invoiceNo=".$row['BILL_INVOICE_NO']."&invoiceYear=".$row['BILL_INVOICE_YEAR']."\" target=\"other_invoice.php\">".$row['BILL_INVOICE_NO'].'/'.$row['BILL_INVOICE_YEAR']."</a></td>";
				$content	.= "<td class=\"clsReference\" style=\"text-align:left\"><span class=\"clsReference\" style=\"text-align:left\">".$row['REFERENCE_NO']."</span></td>";
				$content	.= "<td class=\"clsInvoiceDate\" style=\"text-align:center\">".$row['BILL_DATE']."</td>";
				$content	.= "<td class=\"clsInvoiceAmt\" style=\"text-align:right\">".number_format($grandTotal, 2, '.', '')."</td>";
				$content	.= "<td class=\"clsPaidAmt\" style=\"text-align:right\">".number_format($paidAmt, 2, '.', '')."</td>";
				$content	.= "<td class=\"clsToBeReceive\" style=\"text-align:right\">".number_format($tobeRecv,2, '.', '')."</td>";
				$content	.= "<td class=\"clsPayAmount\" style=\"text-align:center\"><input type=\"textbox\" id=\"txtReceiveAmt\" name=\"txtReceiveAmt\" style=\"width:110px;text-align:right\" value=\"0.00\" class=\"clsReceiveAmt validate[decimal[2],custom[number]] cls_input_number_validation\"></td></tr>";	
			}
			
		}
		$response['gridDetail']	= $content;
	}
	else if($requestType == 'saveData')
	{
		$arrHeader 		= json_decode($_REQUEST['arrHeader'],true);
		$arrDetails 	= json_decode($_REQUEST['arrDetails'],true);
		$editMode		= 0;
		
		$receiveNo		= $arrHeader["receiveNo"];
		$receiveYear	= $arrHeader["receiveYear"];
		$receiveDate	= $arrHeader["receiveDate"];
		$customerID		= $arrHeader["customerID"];
		$currency		= $arrHeader["currency"];
		$Remarks		= $arrHeader["Remarks"];
		$recvMethod		= $arrHeader["recvMethod"];
		$bankRefNo		= $arrHeader["bankRefNo"];
		$totRecvAmount	= $arrHeader["totReceiveAmount"];
		$totGLAlcAmount = $arrHeader["totGLAlocAmount"];
		$accountId 		= $arrHeader["accountId"];
		$glAmount 		= $arrHeader["glAmount"];
		$memo 			= $arrHeader["memo"];
		$costCenter 	= $arrHeader["costCenter"];
		
		####################################################
		## 1.check whether month end is locked			  ##
		####################################################
		$mep_status			= $finance_month_end_process->getIsMonthEndProcessLocked($receiveDate,$companyId);
		
		if($mep_status)
				throw new Exception("Month end Process locked for the date '".$receiveDate."' .");
	
		## set menupermisssion		 			
		$menupermision->set($programCode,$userId);
		
		## get approve levels and status				
		$sys_approvelevels->set($programCode);
		$approveLevel		= $sys_approvelevels->getintApprovalLevel();
		$status				= $approveLevel+1; 
		
		####################################################
		## 2.check permission							  ##
		####################################################
		$permissionArr	= $menupermision->checkMenuPermision('Edit',NULL,NULL);
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
			
		####################################################
		## 3.check exchange rate for current date		  ##
		####################################################
		$exchResult				= $mst_financeexchangerate->checkCurrencyAvailableForSelectedDate($receiveDate,$companyId);
		if(!$exchResult)
			throw new Exception("Please enter exchange rates for ".$receiveDate);
		// check bank for selected gl
		//$finance_mst_chartofaccount_bank->checkBankForGL($glID);
			
		####################################################
		## 4.check bank exchange rate for current date	  ##
		####################################################
		$chkBankRate	= checkBankCurrency($accountId,$currency,$receiveDate);
		if(!$chkBankRate)
				throw new Exception("Please enter bank exchange rates for ".$receiveDate);
				## insert data to header 				
		if($receiveNo == '' && $receiveYear == '')
		{
		####################################################
		## 5. display error msg if details not available  ##
		####################################################
			
			if(!$arrDetails)
			{
				throw new Exception("No details to save");
			}
			
			//get serial no	
			$receiveNo			= $sys_no->getSerialNoAndUpdateSysNo('OTHER_RECEIVABLE_RECEIPT_NO',$locationId);
			$receiveYear		= $dateTimes->getCurruntYear();

			// save header data
			$result_arr		= $finance_other_receivable_receipt_header->insertRec($receiveNo,$receiveYear,$customerID,$currency,$receiveDate,$recvMethod,$bankRefNo,$Remarks,$status,$approveLevel,0,$companyId,$locationId,$userId,$dateTimes->getCurruntDateTime(),'NULL',$dateTimes->getCurruntDateTime());
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
			
			$insert_gl		= $finance_other_receivable_receipt_gl->insertRec($receiveNo,$receiveYear,$accountId,$glAmount,$memo,$costCenter);
			if(!$insert_gl['status'])
				throw new Exception($insert_gl['msg']);
		}
		## update data and delete datail data		
		else
		{
			$finance_other_receivable_receipt_header->set($receiveNo,$receiveYear);
			$status_saved			= $finance_other_receivable_receipt_header->getSTATUS();
			$approveLevel_saved		= $finance_other_receivable_receipt_header->getAPPROVE_LEVELS();
			
			$permissionArr	= $menupermision->checkMenuPermision('Edit',$status_saved,$approveLevel_saved);
			if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
 		
		####################################################
		## 6.check whether saved location			  	  ##
		####################################################
		$location	= $finance_other_receivable_receipt_header->getLOCATION_ID();
		if($location != $locationId)
				throw new Exception("This is not saved location");
				
		## update header data					
			$finance_other_receivable_receipt_header->set($receiveNo,$receiveYear);

			$finance_other_receivable_receipt_header->setCUSTOMER_ID($customerID);
			$finance_other_receivable_receipt_header->setCURRENCY_ID($currency);
			$finance_other_receivable_receipt_header->setRECEIPT_DATE($receiveDate);
			$finance_other_receivable_receipt_header->setPAYMENT_MODE_ID($recvMethod);
			$finance_other_receivable_receipt_header->setBANK_REFERENCE_NO($bankRefNo);
			$finance_other_receivable_receipt_header->setSTATUS($status);
			$finance_other_receivable_receipt_header->setAPPROVE_LEVELS($approveLevel);
			$finance_other_receivable_receipt_header->setLAST_MODIFY_BY($userId);
			$finance_other_receivable_receipt_header->setREMARKS($Remarks);
			$finance_other_receivable_receipt_header->setBANK_REFERENCE_NO($bankRefNo);
			$finance_other_receivable_receipt_header->commit();
		
		## update gl data		
			$data		= array('CHART_OF_ACCOUNT_ID'=>$accountId,
								'AMOUNT'=>$glAmount,
								'REMARKS'=>$memo,
								'COST_CENTER'=>$costCenter,);		
			$updateGL	= $finance_other_receivable_receipt_gl->update($data," RECEIPT_NO = '$receiveNo' AND RECEIPT_YEAR = '$receiveYear'");
			if(!$updateGL['status'])	
				throw new Exception($updateGL['msg']);		
		## update max status in approve by table	
			$finance_other_receivable_receipt_approve_by->updateMaxStatus($receiveNo,$receiveYear);

		## delete issue to production detail data	
			$resultDel		= $finance_other_receivable_receipt_details->delete(" RECEIPT_NO = '$receiveNo' AND RECEIPT_YEAR = '$receiveYear'");
			
			if(!$resultDel['status'])	
				throw new Exception("Delete error!");	
					
			$editMode		= 1;	
		}
		## Save Detail Data					
		$totReceive	= 0;
		foreach($arrDetails as $detail)
		{
			$invoiceNoArr 		= explode('/',trim($detail['invoiceNoArr']));
			$invoiceNo 			= $invoiceNoArr[0];
			$invoicYear			= $invoiceNoArr[1];
			$receiveAmt			= $detail['receiveAmount'];
			$totReceive			+= $receiveAmt;
			$finance_other_receivable_invoice_header->set($invoiceNo,$invoicYear);
		####################################################
		## 7.check invoice status					  	  ##
		####################################################
			if($finance_other_receivable_invoice_header->getSTATUS() != 1)
				throw new Exception('Can not save payment. '.$invoiceNo.'/'.$invoicYear.' Invoice is not approved');
		
		####################################################
		## 8.check invoice balance					  	  ##
		####################################################
			$tobeRecv	= $finance_other_receivable_transaction->getTransactionAMount($invoiceNo,$invoicYear,NULL);

			if($receiveAmt > $tobeRecv)
				throw new Exception("Receive amount exceeds to be receive amount in :".$detail['invoiceNoArr']."</br> Receive Amount:".$receiveAmt."</br> To Be Receive:".$tobeRecv);

						
			$saveResult		= $finance_other_receivable_receipt_details->insertRec($receiveNo,$receiveYear,$invoiceNo,$invoicYear,$receiveAmt);
			if(!$saveResult['status'])
				throw new Exception("Detail saving error");	
			
		}
		if($glAmount != $totReceive)
			throw new Exception('Total receive amount must equal to total GL alocated amonut.');
		if($status == 1)
			insertToTransactionTables($receiveNo,$receiveYear);
		
		$approve_perm	= $menupermision->checkMenuPermision('Approve',$status,$approveLevel);
		$cancel_perm	= $menupermision->checkMenuPermision('Cancel',$status,$approveLevel);
			
		$sys_no->getSerialNoAndUpdateSysNo('OTHER_RECEIVABLE_RECEIPT_NO',$locationId);
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode == 1)
			$response['msg'] 		= "Updated Successfully.";
		else
			$response['msg'] 		= "Saved Successfully.";
		
		$response['receiveNo']		= $receiveNo;
		$response['receiveYear']	= $receiveYear;
		$response['app_perm']		= $approve_perm['type'];
		$response['cancel_perm']	= $cancel_perm['type'];
		$db->disconnect();
	}
	else if($requestType == 'approve')
	{
		$db->connect();$db->begin();	
		$receiveNo			= $_REQUEST['receiveNo'];
		$receiveYear		= $_REQUEST['receiveYear'];

		## get header status and approve level		  
		$finance_other_receivable_receipt_header->set($receiveNo,$receiveYear);
		$status				= $finance_other_receivable_receipt_header->getSTATUS(); 
		$approveLevels		= $finance_other_receivable_receipt_header->getAPPROVE_LEVELS();
		$currencyID			= $finance_other_receivable_receipt_header->getCURRENCY_ID();
		$recvDate			= $finance_other_receivable_receipt_header->getRECEIPT_DATE();
		
		####################################################
		## 1.check whether month end is locked			  ##
		####################################################
		$mep_status			= $finance_month_end_process->getIsMonthEndProcessLocked($recvDate,$companyId);
		
		if($mep_status)
				throw new Exception("Month end process locked for the date '".$recvDate."' .");

		## set menupermisssion		 				  
		$menupermision->set($programCode,$userId);
		
		####################################################
		## 2.check Approve permission					  ##
		####################################################
		$permissionArr	= $menupermision->checkMenuPermision('Approve',$status,$approveLevels);
		if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
		
		####################################################
		## 3.check whether saved location			  	  ##
		####################################################
		$location	= $finance_other_receivable_receipt_header->getLOCATION_ID();
		if($location != $locationId)
				throw new Exception("This is not saved location");
				
		$exchResult				= $mst_financeexchangerate->checkCurrencyAvailableForSelectedDate($recvDate,$companyId);
		if(!$exchResult)
			throw new Exception("Please enter exchange rates for ".$recvDate);
		
		$rowGL					= $finance_other_receivable_receipt_gl->getGLDetails_result($receiveNo,$receiveYear);
		$glAmnt					= $rowGL['AMOUNT']	;
		
		//check bank exchange rates are available
		$chkBankRate	= checkBankCurrency($rowGL['CHART_OF_ACCOUNT_ID'],$currencyID,$recvDate);
		if(!$chkBankRate)
				throw new Exception("Please enter bank exchange rates for ".$recvDate);
				
		####################################################
		## 4.check invoice status					  	  ##
		####################################################
		$result		= $finance_other_receivable_receipt_details->getReceiptDetail_result($receiveNo,$receiveYear);
		$totRcv		= 0;
		while($row	= mysqli_fetch_array($result))
		{	
			$receiveAmt		= $row['AMOUNT'];
			$totRcv			+= $receiveAmt;
			if($row['STATUS'] != 1)
				throw new Exception('Can not save payment. '.$row['BILL_INVOICE_NO'].'/'.$row['BILL_INVOICE_YEAR'].' Invoice is not approved');
			
		####################################################
		## 5.check invoice balance					  	  ##
		####################################################
			$tobeRecv	= $finance_other_receivable_transaction->getTransactionAMount($row['BILL_INVOICE_NO'],$row['BILL_INVOICE_YEAR'],NULL);

			if($receiveAmt > $tobeRecv)
				throw new Exception("Receive amount exceeds to be receive amount in :".$row['BILL_INVOICE_NO'].'/'.$row['BILL_INVOICE_YEAR']."</br> Receive Amount:".$receiveAmt."</br> To Be Receive:".$tobeRecv);
		}
		///check tottal amount with gl amount
		if($glAmnt != $totRcv)
			throw new Exception('Total receive amount should equal to GL amount');
		
		
		## Update header status					  
		$where = "RECEIPT_NO = '$receiveNo' AND RECEIPT_YEAR= '$receiveYear'";
		
		$data  =array(	'STATUS'=> -1
		);
		$result_update_h		= $finance_other_receivable_receipt_header->upgrade($data,$where);
		if(!$result_update_h['status']) 
			throw new Exception($result_update_h['msg']);
		
		## insert to approve by table				  	  
		$finance_other_receivable_receipt_header->set($receiveNo,$receiveYear);
		$hstatus		= $finance_other_receivable_receipt_header->getSTATUS();
		$aplevel		= $finance_other_receivable_receipt_header->getAPPROVE_LEVELS();
		$level			= (int)$aplevel+1- (int)$hstatus; 

		$inserToApprovedBy	= $finance_other_receivable_receipt_approve_by->insertRec($receiveNo,$receiveYear,$level,$userId,$dateTimes->getCurruntDateTime(),0);
		if(!$inserToApprovedBy['status']) 
			throw new Exception($inserToApprovedBy['msg']);
		
		if($hstatus == 1)
		{
			insertToTransactionTables($receiveNo,$receiveYear);
		}
		
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Approved Successfully.";	
		$db->disconnect();
	}
		else if($requestType == 'cancel')
	{
		$db->connect();$db->begin();
		$receiveNo			= $_REQUEST['receiveNo'];
		$receiveYear		= $_REQUEST['receiveYear'];
		
		## get header status and approve level	
		$finance_other_receivable_receipt_header->set($receiveNo,$receiveYear);
		$status				= $finance_other_receivable_receipt_header->getSTATUS(); 
		$approveLevels		= $finance_other_receivable_receipt_header->getAPPROVE_LEVELS();
		$receiveDate		= $finance_other_receivable_receipt_header->getRECEIPT_DATE();
		
		####################################################
		## 1.check whether month end is locked.			  ##
		####################################################
		$mep_status			= $finance_month_end_process->getIsMonthEndProcessLocked($receiveDate,$companyId);
		
		if($mep_status)
				throw new Exception("Month end Process locked for the date '".$receiveDate."' .");

		## set menupermisssion		 				 
		$menupermision->set($programCode,$userId);
		
		####################################################
		## 2.check Cancel permission					  ##
		####################################################
		$permissionArr	= $menupermision->checkMenuPermision('Cancel',$status,$approveLevels);
		if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
		
		####################################################
		## 3.check whether saved location			  	  ##
		####################################################
		$location	= $finance_other_receivable_receipt_header->getLOCATION_ID();
		if($location != $locationId)
				throw new Exception("This is not saved location");	
	
		####################################################
		## 4.update header status					  	  ##
		####################################################	 	
		$finance_other_receivable_receipt_header->setSTATUS(-2);
		$finance_other_receivable_receipt_header->commit();
		
		## update approved by table	 		
		$inserToApprovedBy	= $finance_other_receivable_receipt_approve_by->insertRec($receiveNo,$receiveYear,-2,$userId,$dateTimes->getCurruntDateTime(),0);
		if(!$inserToApprovedBy['status']) 
			throw new Exception($inserToApprovedBy['msg']);
			
		####################################################
		## 5.delete other receivable tansactions		  ##
		####################################################

		$deleteORTrans	= $finance_other_receivable_transaction->delete("DOCUMENT_NO = '$receiveNo' AND DOCUMENT_YEAR = '$receiveYear' AND DOCUMENT_TYPE = 'BILLPAYMENT'");
		if(!$deleteORTrans['status']) 
			throw new Exception($deleteORTrans['msg']);
			
		####################################################
		## 6.delete finance tansactions					  ##
		####################################################

		$deleteFinTrans	= $finance_transaction->delete("DOCUMENT_NO = '$receiveNo' AND DOCUMENT_YEAR = '$receiveYear' AND DOCUMENT_TYPE = 'BILLPAYMENT' AND TRANSACTION_CATEGORY = 'OR'");
		if(!$deleteFinTrans['status']) 
			throw new Exception($deleteFinTrans['msg']);
				
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "cancel Successfully.";
		$db->disconnect();	
			
	}
	else if($requestType == 'reject')
	{
		$db->connect();$db->begin();
		$receiveNo			= $_REQUEST['receiveNo'];
		$receiveYear		= $_REQUEST['receiveYear'];
		
		####################################################
		## 1.get header status and approve level		  ##
		####################################################
		$finance_other_receivable_receipt_header->set($receiveNo,$receiveYear);
		$status				= $finance_other_receivable_receipt_header->getSTATUS(); 
		$approveLevels		= $finance_other_receivable_receipt_header->getAPPROVE_LEVELS();

		####################################################
		## 2.set menupermisssion		 				  ##
		####################################################
		$menupermision->set($programCode,$userId);
		
		####################################################
		## 3.check Reject permission					  ##
		####################################################
		$permissionArr	= $menupermision->checkMenuPermision('Reject',$status,$approveLevels);
		if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
		
		####################################################
		## 4.check whether saved location			  	  ##
		####################################################
		$location	= $finance_other_receivable_receipt_header->getLOCATION_ID();
		if($location != $locationId)
				throw new Exception("This is not saved location");
						
		####################################################
		## 5.update header status		 				  ##
		####################################################
		$finance_other_receivable_receipt_header->setSTATUS(0);
		$finance_other_receivable_receipt_header->commit();
		
		####################################################
		## 6.update approved by table	 				  ##
		####################################################
		$inserToApprovedBy	= $finance_other_receivable_receipt_approve_by->insertRec($receiveNo,$receiveYear,0,$userId,$dateTimes->getCurruntDateTime(),0);
		if(!$inserToApprovedBy['status']) 
			throw new Exception($inserToApprovedBy['msg']);
				
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Rejected Successfully.";
		$db->disconnect();	
	}
}
catch(Exception $e)
{
	$db->rollback();
	$response['msg'] 	=  $e->getMessage();
	$response['error'] 	=  $error_handler->jTraceEx($e);
	$response['type']	=  'fail';
	$response['sql']	=  $db->getSql();
}
$db->disconnect();		
echo json_encode($response);

function insertToTransactionTables($receiveNo,$receiveYear)
{
	global $finance_other_receivable_invoice_header;
	global $mst_customer;
	global $finance_mst_chartofaccount;
	global $finance_other_receivable_invoice_details;
	global $finance_other_receivable_receipt_details;
	global $finance_other_receivable_receipt_header;
	global $finance_other_receivable_transaction;
	global $finance_other_receivable_receipt_gl;
	global $finance_transaction;
	global $finance_mst_chartofaccount_tax;
	global $mst_financetaxgroup;
	global $dateTimes;
	global $finance_mst_chartofaccount_bank;
	global $cls_exchange_rate_get;
	global $companyId;
	global $userId;
	
	$finance_other_receivable_receipt_header->set($receiveNo,$receiveYear);
	$customerID		= $finance_other_receivable_receipt_header->getCUSTOMER_ID();
	$currencyID		= $finance_other_receivable_receipt_header->getCURRENCY_ID();
	$recvDate		= $finance_other_receivable_receipt_header->getRECEIPT_DATE();
	$remark			= $finance_other_receivable_receipt_header->getREMARKS();
	$bankRefNo		= $finance_other_receivable_receipt_header->getBANK_REFERENCE_NO();
	$creteCompanyId	= $finance_other_receivable_receipt_header->getCOMPANY_ID();
	$location		= $finance_other_receivable_receipt_header->getLOCATION_ID();
	$createdBy		= $finance_other_receivable_receipt_header->getCREATED_BY();
	
	$rowGL		= $finance_other_receivable_receipt_gl->getGLDetails_result($receiveNo,$receiveYear);
	$glID			= $rowGL['CHART_OF_ACCOUNT_ID'];
	$remark			= $rowGL['REMARKS'];

	$customerGL		= $finance_mst_chartofaccount->getCatogoryTypeWiseGL('C',$customerID);
	$amtResult		= $finance_other_receivable_receipt_details->getReceiptDetail_result($receiveNo,$receiveYear);
	$totAmount 		= 0;
	while($rowDetail = mysqli_fetch_array($amtResult))
	{
		$amount 		= $rowDetail['AMOUNT'];
		$invoiceYear	= $rowDetail['BILL_INVOICE_YEAR'];
		$invoiceNo		= $rowDetail['BILL_INVOICE_NO'];
		$totAmount 		= $totAmount+$amount;
		$ORTransaction	= $finance_other_receivable_transaction->insertRec($customerID,$currencyID,$receiveYear,$receiveNo,'BILLPAYMENT',$invoiceYear,$invoiceNo,'NULL',-($amount),$creteCompanyId,$location,$userId,$recvDate);
		if(!$ORTransaction['status']) 
			throw new Exception($ORTransaction['msg']);
	}
	$finCusTrans	= $finance_transaction->insertRec($customerGL,$totAmount,$receiveNo,$receiveYear,'BILLPAYMENT',$invoiceNo,$invoiceYear,'C','OR',$customerID,$currencyID,NULL,NULL,0,$location,$creteCompanyId,$userId,$recvDate,$dateTimes->getCurruntDateTime());	
	if(!$finCusTrans['status']) 
			throw new Exception($finCusTrans['msg']);
	$finance_mst_chartofaccount_bank->set($glID);
	$bankCurrency			= $finance_mst_chartofaccount_bank->getCURRENCY_ID();
	$bankID					= $finance_mst_chartofaccount_bank->getBANK_ID();
	if($bankCurrency != $currencyID)
	{
		$rateTarget		= $cls_exchange_rate_get->GetAllBankValues($bankCurrency,$bankID,2,$recvDate,$companyId,'RunQuery');
		$rateSource		= $cls_exchange_rate_get->GetAllBankValues($currencyID,$bankID,2,$recvDate,$companyId,'RunQuery');
		$currencyID		= $bankCurrency;
		$totAmount	= round((($totAmount/$rateTarget["AVERAGE_RATE"])*$rateSource["AVERAGE_RATE"]),2);
		if($totRcvAmount=='0')
			throw new Exception("Bank exchange rate not available in the system.");
			
		$finTransaction	= $finance_transaction->insertRec($glID,$totAmount,$receiveNo,$receiveYear,'BILLPAYMENT',$invoiceNo,$invoiceYear,'D','OR',$customerID,$currencyID,$bankRefNo,$remark,0,$location,$creteCompanyId,$userId,$recvDate,$dateTimes->getCurruntDateTime());
	}
	else{
		$finTransaction	= $finance_transaction->insertRec($glID,$totAmount,$receiveNo,$receiveYear,'BILLPAYMENT',$invoiceNo,$invoiceYear,'D','OR',$customerID,$currencyID,$bankRefNo,$remark,0,$location,$creteCompanyId,$userId,$recvDate,$dateTimes->getCurruntDateTime());
	}
	if(!$finTransaction['status']) 
			throw new Exception($finTransaction['msg']);
	
	
}	
function checkBankCurrency($accountId,$currencyID,$receiveDate)
{
	global $finance_mst_chartofaccount_bank;
	global $mst_financeexchangerate_bankwise;
	global $companyId;
	
	$chkStatus	= 1;
	$error_msg	= '';
	// check bank for selected gl
	$bankChkArr	= $finance_mst_chartofaccount_bank->checkBankForGL($accountId);
	if(!$bankChkArr['type'])
		throw new Exception($bankChkArr['msg']);
		
	$finance_mst_chartofaccount_bank->set($accountId);
	$bankCurrency			= $finance_mst_chartofaccount_bank->getCURRENCY_ID();
	if($bankCurrency != $currencyID)
	{
		$bankExchResult			= $mst_financeexchangerate_bankwise->checkBankCurrencyAvailableForSelectedDate($receiveDate,$accountId,$companyId);
		if($bankExchResult != 1)
			$chkStatus			= 0;	
	}
			
	if($chkStatus==0)
		return false;

	else 
		return true;
	
}
?>
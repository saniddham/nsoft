<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once ("class/finance/cls_get_gldetails.php");
include_once ("class/finance/cls_common_get.php");
include_once ("class/finance/bank/bankToBankTransfer/cls_bankToBankTransfer_get.php");
include_once ("class/cls_commonErrorHandeling_get.php");
//include 		"include/javascript.html";

$progrmCode			= 'P0764';
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$session_userId 	= $_SESSION["userId"];

$serialNo			= (!isset($_REQUEST['SerialNo'])?'':$_REQUEST['SerialNo']);
$serialYear			= (!isset($_REQUEST['SerialYear'])?'':$_REQUEST['SerialYear']);
/*$serialNo			= 100009;
$serialYear			= 2013;
*/ 
 
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_GLDetails_get	= new Cls_Get_GLDetails($db);
$obj_fin_com_get	= new Cls_Common_Get($db);
$obj_fin_transfer_get= new Cls_BankToBankTransfer_Get($db);

$header_array 		= $obj_fin_transfer_get->get_header_array($serialYear,$serialNo);
$detail_result		= $obj_fin_transfer_get->get_details_result($serialYear,$serialNo);

$backDatePermision	= $obj_commonErr->Load_special_menupermision('17',$session_userId,'RunQuery');

$permition_arr		= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_save		= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_cancel($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_cancel	= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_reject	= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_confirm	= $permition_arr['permision'];

 if(!isset($header_array["TRANSFER_NO"]))
	$journalEntDate	= date("Y-m-d");
else
	$journalEntDate	= $header_array["TRANSFER_DATE"];
	
$exchangeRate		= $obj_fin_com_get->get_exchane_rate($header_array["CURRENCY_ID"],$journalEntDate,$session_companyId,'RunQuery');
?>
<title>Bank To Bank Transfer</title>

<!--<script type="text/javascript" src="presentation/finance_new/bank/bankToBankTransfer/bankToBankTransfer.js"></script>-->

<form id="frmBankTransfer" name="frmBankTransfer" method="post">
  <div align="center">
    <div class="trans_layoutS" style="width:950px">
      <div class="trans_text">Fund Transfer</div>
      <table width="950">
        <tr>
          <td colspan="2"><table width="100%" border="0" class="normalfnt" cellpadding="1" cellspacing="1">
              <tr>
                <td width="10%">Transfer No</td>
                <td width="24%"><input type="text" name="txtSerialNo" id="txtSerialNo" style="width:85px" disabled="disabled" value="<?php echo $header_array["TRANSFER_NO"]?>"/>&nbsp;<input type="text" name="txtSerialYear" id="txtSerialYear" style="width:60px" disabled="disabled" value="<?php echo $header_array["TRANSFER_YEAR"]?>"/></td>
                <td width="36%">&nbsp;</td>
                <td width="12%">Date</td>
                <td width="18%"><input name="txtTransferDate" <?php if($backDatePermision != 1){ ?> disabled="disabled"<?php } ?> type="text" value="<?php echo $journalEntDate ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtTransferDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
              <tr>
                <td>Currency <span class="compulsoryRed">*</span></td>
                <td><select name="cboCurrency" id="cboCurrency" style="width:85px"  class="validate[required] cls_cbo_CurrencyCalExchangeRate">
                  <?php 
 				  echo $obj_fin_com_get->getCurrencyCombo($header_array["CURRENCY_ID"]); ?>
                </select>
                <input type="text" name="txtExchangeRate" id="txtExchangeRate" style="width:60px; text-align:right" disabled="disabled" value="<?php echo $exchangeRate;?>"  class="validate[required] cls_txt_exchangeRate "  /></td>
                <td>&nbsp;</td>
                <td>Reference No</td>
                <td><input type="text" maxlength="50" name="txtRefNo" id="txtRefNo" style="width:167px"  value="<?php echo $header_array["REFERENCE_NO"];?>"/></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td colspan="2" align="left" ><div style="overflow:scroll;width:950px;height:250px;">
              <table width="1000" border="0" class="bordered" id="tblMain">
                <thead>
<tr>
                    <th colspan="6" style="text-align:right"><a id="butInsertRow" class="button white small">Add New</a></th>
                  </tr>                  <tr>
                    <th width="6%">Del</th>
                    <th width="32%"><strong>Bank Account <span class="compulsoryRed">*</span></strong></th>
                    <th width="10%"><strong>Debit</strong></th>
                    <th width="11%"><strong>Credit</strong></th>
                    <th width="22%">Remarks</th>
                    <th width="19%">Cost Center <strong><span class="compulsoryRed">*</span></strong></th>
                    </tr>
                </thead>
                <tbody>
                  <?php
$loop	= 0;
$cTot	=0;
$dTot	=0;
while($row = mysqli_fetch_array($detail_result))
{ 
	$booAvailable	= true;
	if($row["TRANSACTION_TYPE"]=='C'){
		$cTot	+=	$row["AMOUNT"];
	}
	else{
		$dTot	+=	$row["AMOUNT"];
	}
?>
                  <tr>
                    <td style="text-align:center"><img class="delImg mouseover" width="15" height="15" src="images/del.png"/></td>
                    <td>
                    <select name="cboAccount" id="cboAccount" class="clsLedgerAc validate[required]"  style="width:100%" >
                      <?php
					  		echo $obj_GLDetails_get->getBankGLCombo($row["CHART_OF_ACCOUNT_ID"]);
							//echo Get_GL_HTML($row["GL_ACCOUNT"]);
                      ?>
                    </select>
                    </td>
                    <?php
					
					?>
                    <td style="text-align:center"><input id="txtDebit" type="textbox" style="width:80px;text-align:right" value="<?php if($row["TRANSACTION_TYPE"] == 'D'){ echo $row["AMOUNT"];} else { echo 0; }?>" class="debit validate[custom[number]]"></td>
                    <td style="text-align:center"><input id="txtCredit" type="textbox" style="width:80px;text-align:right" value="<?php if($row["TRANSACTION_TYPE"] == 'C'){ echo $row["AMOUNT"];} else { echo 0; }?>" class="credit validate[custom[number]]"></td>
                    <td style="text-align:center"><textarea name="txtRemarks" id="txtRemarks" cols="30"  style="height:20px" class="remarks"><?php echo $row["REMARKS"] ?></textarea></td>
                    <td style="text-align:center"><span class="cls_td_salesOrderNo">
                      <select name="cboCostCenter" id="cboCostCenter" class="clsCostCenter validate[required]"  style="width:100%" >
                        <?php
					  		echo $obj_fin_com_get->getCostCenterCombo($row["COST_CENTER_ID"]);
							//echo Get_GL_HTML($row["GL_ACCOUNT"]);
                      ?>
                      </select>
                    </span></td>
                  </tr>
                  <?php
}
if(!$booAvailable)
{
?>
                  <tr>
                    <td style="text-align:center"><img class="delImg mouseover" width="15" height="15" src="images/del.png"/></td>
                    <td><select name="cboAccount" id="cboAccount" class="clsLedgerAc validate[required]"  style="width:100%" >
                      <?php
					  		echo $obj_GLDetails_get->getBankGLCombo('');
							//echo Get_GL_HTML($row["GL_ACCOUNT"]);
                      ?>
                    </select></td>
                    <td style="text-align:center"><input id="txtDebit" type="textbox" style="width:80px;text-align:right" value="0" class="debit validate[custom[number]]" />
                    </td>
                    <td style="text-align:center"><input id="txtCredit" type="textbox" style="width:80px;text-align:right" value="0" class="credit validate[custom[number]]" />
                    </td>
                    <td style="text-align:center"><textarea name="txtRemarks" id="txtRemarks" cols="30" style="height:20px" class="remarks"></textarea>
                    </td>
                    <td style="text-align:center"> 
                      <select name="cboCostCenter" id="cboCostCenter" class="clsCostCenter validate[required]"  style="width:100%" >
                        <?php
					  		echo $obj_fin_com_get->getCostCenterCombo('');
							//echo Get_GL_HTML($row["GL_ACCOUNT"]);
                      ?>
                      </select>
                    </td>
                  </tr>
                <?php
}

	  $glCombo = $obj_GLDetails_get->getBankGLCombo('');
	  $costCenter = $obj_fin_com_get->getCostCenterCombo('');
?>
 
                 </tbody>
              </table>
          </div></td>
        </tr>
        <tr>
          <td width="472" >&nbsp;</td>
          <td width="512" valign="top"><table width="265" border="0" align="right" class="normalfnt" cellpadding="0" cellspacing="2">
              <tr>
                <td width="105">Credit Amount</td>
                <td width="12">:</td>
                <td width="140" class="normalfnt"><input name="txtCreditTotal" type="text" disabled="disabled" id="txtCreditTotal" value="<?php echo number_format($cTot,4); ?>" style="width:100%;text-align:right;" /></td>
              </tr>
              <tr>
                <td>Debit Amount</td>
                <td>:</td>
                <td class="normalfnt"><input name="txtDebitTotal" type="text" disabled="disabled" id="txtDebitTotal" value="<?php echo number_format($dTot,4); ?>" style="width:100%;text-align:right" /></td>
              </tr>
              <tr>
                <td>Difference</td>
                <td>:</td>
                <td class="normalfnt"><input name="txtDifference" type="text" disabled="disabled" id="txtDifference" value="<?php echo number_format($cTot-$dTot,4); ?>" style="width:100%;text-align:right" /></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2" align="center">
                <a class="button white medium" id="butNew">New</a>
                <a class="button white medium" id="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a>
                <a class="button white medium" id="butConfirm"  <?php if($permision_confirm!=1){ ?> style="display:none"<?php } ?>>Approve</a>
                <a class="button white medium" id="butReject" <?php if($permision_reject!=1){ ?>style="display:none" <?php } ?>>Reject</a>
                <a class="button white medium" id="butCancel" <?php if($permision_cancel!=1){ ?>  style="display:none"<?php } ?>>Cancel</a>
                <a class="button white medium" id="butReport">Report</a>
                <a href="main.php" class="button white medium" id="butClose">Close</a>
          </td>
        </tr>
      </table>
    </div>
  </div>
</form>
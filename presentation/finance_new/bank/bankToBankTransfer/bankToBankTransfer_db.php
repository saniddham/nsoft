<?php

session_start();
//ini_set('display_errors',1);

$session_companyId 	= $_SESSION["headCompanyId"];
//BEGIN - INCLUDE FILES {
include_once  ("../../../../dataAccess/Connector.php");
include_once ("../../../../class/finance/cls_common_get.php");
include_once  ("../../../../class/cls_commonFunctions_get.php");
include_once ("../../../../class/finance/cls_get_gldetails.php");
include_once ("../../../../class/finance/bank/bankToBankTransfer/cls_bankToBankTransfer_get.php");
include_once ("../../../../class/finance/bank/bankToBankTransfer/cls_bankToBankTransfer_set.php");
 
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {

$obj_fin_com_get		= new Cls_Common_Get($db);
$obj_fin_comfunc_get	= new cls_commonFunctions_get($db);
$obj_GLDetails_get		= new Cls_Get_GLDetails($db);
$obj_fin_transfer_get	= new Cls_BankToBankTransfer_Get($db);
$obj_fin_transfer_set	= new Cls_BankToBankTransfer_Set($db);

//END 	- CREATE OBJECTS }
$requestType	= $_REQUEST["RequestType"];

if($requestType=="getGLAccountOptions")
{
	$glCombo 				= $obj_GLDetails_get->getBankGLCombo('');
	$response['combOptions']=$glCombo;
 	echo json_encode($response);
} 
if($requestType=="getCostCenterOptions")
{
	$costCenter 			= $obj_fin_com_get->getCostCenterCombo('');
	$response['combOptions']=$costCenter;
 	echo json_encode($response);
} 
if($requestType=="getJEOptions")
{
	$account				=$_REQUEST["account"];
	$costCenter 			= $obj_fin_com_get->getJECombo('','','',$account);
	$response['combOptions']=$costCenter;
 	echo json_encode($response);
} 
if($requestType=="loadExchangeRate")
{
	$date					=$_REQUEST["date"];
	$currency				=$_REQUEST["currency"];
	$val		 			= $obj_fin_com_get->get_exchane_rate($currency,$date,$session_companyId,'RunQuery');
	$response['exchaRate']	=$val;
 	echo json_encode($response);
} 
 

elseif($requestType=="URLSave")
{
   
   	$Header				= json_decode($_REQUEST["Header"],true);
  	$Grid				= json_decode($_REQUEST["Grid"],true);
	
	
	foreach($Header as $array_loop)
	{
 		$serialYear		= $array_loop["serialYear"];
		$serialNo		= $array_loop["serialNo"];
		$date			= $array_loop["date"];
 		$currency		= $array_loop["currency"];
		$refferenceNo	= $obj_fin_comfunc_get->replace($array_loop["refferenceNo"]);
 	}
	$response			= $obj_fin_transfer_get->ValidateBeforeSave($serialYear,$serialNo);

	if($response["type"]=='fail')
	{
 		echo json_encode($response);
		return;
	}
 		echo $obj_fin_transfer_set->Save($serialYear,$serialNo,$date,$currency,$refferenceNo,$Grid);
 } 
elseif($requestType=="approve")
{
	$serialNo			= $_REQUEST["SerialNo"];
	$serialYear			= $_REQUEST["SerialYear"];
	
	$response			= $obj_fin_transfer_get->ValidateBeforeApprove($serialYear,$serialNo);
	
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
 	echo $obj_fin_transfer_set->Approve($serialYear,$serialNo);
}
elseif($requestType=="reject")
{
	$serialNo			= $_REQUEST["SerialNo"];
	$serialYear			= $_REQUEST["SerialYear"];
	
	$response			= $obj_fin_transfer_get->ValidateBeforeReject($serialYear,$serialNo);
	
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	
	echo $obj_fin_transfer_set->Reject($serialYear,$serialNo);
}
elseif($requestType=="cancel")
{
	$serialNo			= $_REQUEST["SerialNo"];
	$serialYear			= $_REQUEST["SerialYear"];
	
	$response			= $obj_fin_transfer_get->ValidateBeforeCancel($serialYear,$serialNo);
	
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	
	echo $obj_fin_transfer_set->Cancel($serialYear,$serialNo);
}


?>
var glCombo;
var costCenter;
var balAmount 	= 0;
var basePath	= "presentation/finance_new/bank/bankToBankTransfer/";

var reportId	= 980;
var menuId		= 764;

$(document).ready(function() {
	
 	$('#frmBankTransfer').validationEngine();
	
	$('#frmBankTransfer #butInsertRow').live('click',addNew);
	$('#frmBankTransfer #butNew').die('click').live('click',New);
	$('#frmBankTransfer #butSave').die('click').live('click',Save);
	$('#frmBankTransfer #butConfirm').die('click').live('click',Confirm);
	$('#frmBankTransfer #butReject').die('click').live('click',Reject);
	$('#frmBankTransfer #butCancel').die('click').live('click',Cancel);
	$('#frmBankTransfer #butReport').die('click').live('click',Report);
  	
	$('#frmRptBankTransfer #butRptConfirm').die('click').live('click',ConfirmRpt);
	$('#frmRptBankTransfer #butRptReject').die('click').live('click',RejectRpt);
	$('#frmRptBankTransfer #butRptCancel').die('click').live('click',CancelRpt);
	
	

	$('#frmBankTransfer #tblMain input').die('keyup').live('keyup',function(e){
			if($(this).val()=="" || isNaN($(this).val())){
				$(this).val(0);
				$(this).select();
			}
			if($(this).val() > 0){
				if($(this).attr('id')=='txtCredit'){
					$(this).parent().parent().find('.debit').val(0)
				}
				else{
					$(this).parent().parent().find('.credit').val(0)
				}
			}
			CaculateTotalValue();
	});
	
 	
	$('#frmBankTransfer #tblMain .delImg').die('click').live('click',function(){
 		 	$(this).parent().parent().remove();
			CaculateTotalValue();
	});
	
 
	
 });

glCombo 	= getGLAccountCombo();
costCenter	= getCostCenterCombo();
JE 			= getJECombo('');
 
function addNew(){
  	
	if(!$('#frmBankTransfer').validationEngine('validate')){
		setTimeout("alertx('')",2000);	
		return false;
	}
	
 	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);		
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML  = "<img class=\"delImg mouseover\" width=\"15\" height=\"15\" src=\"images/del.png\">";
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:center');
  	cell.innerHTML 	= "<select id=\"cboAccount\" class=\"clsLedgerAc validate[required]\" style=\"width:100%\" name=\"cboAccount\">"+glCombo+"</select>";
	
	var cell		= row.insertCell(2);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML  = "<input id=\"txtDebit\" class=\"debit validate[custom[number]]\" type=\"textbox\" value=\"0\" style=\"width:80px;text-align:right\">";
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML 	= "<input id=\"txtCredit\" class=\"credit validate[custom[number]]\" type=\"textbox\" value=\"0\" style=\"width:80px;text-align:right\">";
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<textarea id=\"txtRemarks\" class=\"remarks\" rows=\"1\" cols=\"30\"  style=\"height:20px\" name=\"txtRemarks\"></textarea>";

	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<select id=\"cboCostCenter\" class=\"clsCostCenter validate[required]\" style=\"width:100%\" name=\"cboCostCenter\">"+costCenter+"</select>";

 }

function Report()
{
	if($('#frmBankTransfer #txtSerialNo').val()=="")
	{
		alert("No details available to view report.");
		return;
	}
 	var url  = "?q="+reportId+"&SerialNo="+$('#frmBankTransfer #txtSerialNo').val();
	    url += "&SerialYear="+$('#frmBankTransfer #txtSerialYear').val();
	window.open(url,'rptBankToBankTransfer.php');
}
function Cancel()
{
  	var url  = "?q="+reportId+"&SerialNo="+$('#frmBankTransfer #txtSerialNo').val();
	    url += "&SerialYear="+$('#frmBankTransfer #txtSerialYear').val();
	    url += "&mode=Cancel";
	window.open(url,'rptBankToBankTransfer.php');
}
function Reject()
{
  	var url  = "?q="+reportId+"&SerialNo="+$('#frmBankTransfer #txtSerialNo').val();
	    url += "&SerialYear="+$('#frmBankTransfer #txtSerialYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rptBankToBankTransfer.php');
}
function Confirm()
{
  	var url  = "?q="+reportId+"&SerialNo="+$('#frmBankTransfer #txtSerialNo').val();
	    url += "&SerialYear="+$('#frmBankTransfer #txtSerialYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rptBankToBankTransfer.php');
}


function CaculateTotalValue()
{
	var difference		= 0;
	var creditTotal		= 0;
	var debitTotal		= 0;
	
	$('#frmBankTransfer #tblMain .credit').each(function(){
			creditTotal += parseFloat($(this).val());
			debitTotal   += parseFloat($(this).parent().parent().find('.debit').val());
 	});
	creditTotal	= parseFloat(RoundNumber(creditTotal,4));
	debitTotal	= parseFloat(RoundNumber(debitTotal,4));
	difference	= creditTotal - debitTotal;
	
	$('#frmBankTransfer #txtCreditTotal').val(RoundNumber(creditTotal,4));	
	$('#frmBankTransfer #txtDebitTotal').val(RoundNumber(debitTotal,4));	
	$('#frmBankTransfer #txtDifference').val(RoundNumber(difference,4));
}

 
 
function Save()
{	
	showWaiting();	
	if(!Validate_save()){
		hideWaiting();
		setTimeout("alertx('')",2000);	
		return;
	}
 
	var errFlag = 0;
	
	var arrh	  = "[";
 			arrh += "{";
			arrh += '"currency":"'+$('#frmBankTransfer #cboCurrency').val()+'",' ;
			arrh += '"date":"'+$('#frmBankTransfer #txtTransferDate').val()+'",';
			arrh += '"refferenceNo":'+URLEncode_json($('#frmBankTransfer #txtRefNo').val())+',';
			arrh += '"serialNo":"'+$('#frmBankTransfer #txtSerialNo').val()+'",' ;
			arrh += '"serialYear":"'+$('#frmBankTransfer #txtSerialYear').val()+'"';
  			arrh +=  '},';
 
 	arrh 		 = arrh.substr(0,arrh.length-1);
	arrh 		+= " ]";
 
	var arr		= "[";
	
	var i=0;
	$('#frmBankTransfer #tblMain .credit').each(function(){
		var obj= this;
		i++;
 		if(($(this).parent().parent().find('.debit').val()!=0) || ($(this).val()!=0))
		{
			arr += "{";
			arr += '"account":"'+$(this).parent().parent().find('.clsLedgerAc').val()+'",' ;
			arr += '"credit":"'+$(this).val()+'",';
			arr += '"debit":"'+$(this).parent().parent().find('.debit').val()+'",';
			arr += '"remarks":'+URLEncode_json($(this).parent().parent().find('.remarks').val())+',';
 			arr += '"costCenter":"'+$(this).parent().parent().find('.clsCostCenter').val()+'"';
  			arr +=  '},';
		}
	});
	
	if(errFlag==1){
		alert(msg);
		return;
	}

	arr 		 = arr.substr(0,arr.length-1);
	arr 		+= " ]";
 
 	var url 	= basePath+"bankToBankTransfer_db.php?RequestType=URLSave";
	var data 	= "Header="+arrh;
 		data   += "&Grid="+arr;
 		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
		{
			$('#frmBankTransfer #butSave').validationEngine('showPrompt', json.msg,json.type );
			if(json.type=='pass')
			{
				$('#frmBankTransfer #txtSerialNo').val(json.serialNo);
				$('#frmBankTransfer #txtSerialYear').val(json.serialYear);
				if(json.permision_confirm==1){
 					$('#frmBankTransfer #butConfirm').css('display', 'inline'); 
				}
			}
		},
	error:function(xhr,status)
		{
			$('#frmBankTransfer #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
		}
	});
	var t = setTimeout("alertx('#butSave')",4000);
	hideWaiting();
}

function alertx(but)
{
	$('#frmBankTransfer '+but).validationEngine('hide')	;
}

function New()
{
	window.location.href = '?q='+menuId;
}

  
function LoadAutoData(customerPO)
{
	 $('#frmBankTransfer #txtCustomerPONo').val(customerPO);
	 $('#frmBankTransfer #btnSearch').click();
}

function AddNewGL()
{	
	$('#frmBankTransfer #tblGL tbody tr:last').after("<tr>"+$('#frmBankTransfer #tblGL .cls_tr_firstRow').html()+"</tr>");
}

function CalculateGLTotals()
{
	var total	= 0;
	$('#frmBankTransfer #tblGL .cls_td_GLAmount').each(function(){
    	 total	+= isNaN(parseFloat($(this).children().val()))? 0:parseFloat($(this).children().val());
    });
	total	= RoundNumber(total,4,true);
	$('#frmBankTransfer #tblGL .td_totalGLAlloAmount').val(total);
}

function Validate_save()
{
 	var diff=$('#frmBankTransfer #txtDifference').val();
	
	if(!$('#frmBankTransfer').validationEngine('validate')){
		return false;
	}
/*	else if(hasDuplicates(arr_accounts)==true){
		alert('Duplicate Accounts exists');
		return false;
	}
*/	
/*	if($('#frmBankTransfer #txtSerialNo').val()!="")
	{
		$('#frmBankTransfer #butSave').validationEngine('showPrompt','Already this Bank To Bank Transfer Note No saved.Please refresh the page','fail');
		var t = setTimeout("alertx()",2000);
		return false;
	}
*/	else if(diff != 0){
		alert('Credit total not tally with Debit total');
		return false;
	}
	else{
 	return true;
	}
}

function CheckAll()
{
	if($(this).is(':checked'))
		$('#frmBankTransfer #tblMain .cls_check').attr('checked','checked');
	else
		$('#frmBankTransfer #tblMain .cls_check').removeAttr('checked');
		
	$('#frmBankTransfer #tblMain .cls_check').each(function(){
		ValidateRowWhenCheck($(this));
	});
}

function ValidateRowWhenCheck(obj)
{
	if(obj.is(':checked'))
	{
		obj.parent().parent().find('.cls_td_invoice').children().attr('disabled','');
	}
	else
	{
		obj.parent().parent().find('.cls_td_invoice').children().attr('disabled','disabled');
	}
	CaculateTotalValue();
}


function getGLAccountCombo(){
	
	var optionss = '';
 	var url 	= basePath+"bankToBankTransfer_db.php?RequestType=getGLAccountOptions";
	
	var httpobj = $.ajax({
	url:url,
	data:'',
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		optionss = json.combOptions;
 	}
	});		
	return optionss;
}

function getCostCenterCombo(){
	
	var optionss = '';
 	var url 	= basePath+"bankToBankTransfer_db.php?RequestType=getCostCenterOptions";
	
	var httpobj = $.ajax({
	url:url,
	data:'',
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		optionss = json.combOptions;
 	}
	});		
	return optionss;
	
}

function getJECombo(account){
	
	var optionss = '';
 	var url 	= basePath+"bankToBankTransfer_db.php?RequestType=getJEOptions";
	var data 	= "account="+account;
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		optionss = json.combOptions;
 	}
	});		
	return optionss;
	
}

function hasDuplicates(array) {
	var valuesSoFar = [];
	for (var i = 0; i < array.length; ++i) {
		var value = array[i];
		if (valuesSoFar.indexOf(value) !== -1) {
			return true;
		}
 	}
	return false;
}	



 function ConfirmRpt(){
 	 
	var val = $.prompt('Are you sure you want to approve this Bank To Bank Transfer Note ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"bankToBankTransfer_db.php"+window.location.search+'&RequestType=approve';
					var obj = $.ajax({
						url:url,
						type:'POST',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptBankTransfer #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptBankTransfer #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
					   hideWaiting();
				}});
  }
  
  
  function RejectRpt(){
	  
	var val = $.prompt('Are you sure you want to reject this Bank To Bank Transfer Note ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				showWaiting();
				var url = basePath+"bankToBankTransfer_db.php"+window.location.search+'&RequestType=reject';
				var obj = $.ajax({
					url:url,
					type:'POST',
					dataType: "json",  
					data:'',
					async:false,
					
					success:function(json){
							$('#frmRptBankTransfer #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
								window.location.href = window.location.href;
								window.opener.location.reload();//reload listing page
								return;
							}
						},
					error:function(xhr,status){
							
							$('#frmRptBankTransfer #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);
						}		
					});
				////////////
					}
					hideWaiting();
			}});
	  
  }

 
function CancelRpt(){

	var val = $.prompt('Are you sure you want to cancel this Bank To Bank Transfer Note ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
				if(v)
				{
					showWaiting();
					var url = basePath+"bankToBankTransfer_db.php"+window.location.search+'&RequestType=cancel';
					var obj = $.ajax({
						url:url,
						type:'POST',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptBankTransfer #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptBankTransfer #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
						hideWaiting();
 				}});
	
}
   
 function loadExchangeRate(){
	 
 	var url 		= basePath+"bankToBankTransfer_db.php?RequestType=loadExchangeRate";
	var date 		= $('#frmBankTransfer #txtTransferDate').val();
	var currency 	= $('#frmBankTransfer #cboCurrency').val();
	
 	var data 		= "date="+date;
 	   data	 		+= "&currency="+currency;
	
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		$('#frmBankTransfer #txtExchangeRate').val(json.exchaRate);
  	}
	});		
	 
 }
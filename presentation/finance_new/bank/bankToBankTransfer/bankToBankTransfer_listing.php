<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$intUser			= $_SESSION["userId"];

$programCode		= 'P0764';
$reportId			= 980;
$menuId				= 764;

require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

$approveLevel = (int)getMaxApproveLevel();

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'tb1.STATUS',
				'SERIAL'=>"CONCAT(tb1.TRANSFER_NO,'/',tb1.TRANSFER_YEAR)",
				'CURRENCY'=>'FCOA.CHART_OF_ACCOUNT_NAME',
				'CURRENCY'=>'mst_financecurrency.strCode',
				'REFERENCE_NO'=>'tb1.REFERENCE_NO',
				'TRANSFER_DATE'=>'tb1.TRANSFER_DATE'
				);
				
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2','Cancelled'=>'-2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND tb1.TRANSFER_DATE = '".date('Y-m-d')."'";
	
// }

$sql = "SELECT SUB_1.* FROM
			(SELECT
				tb1.TRANSFER_NO,
				tb1.TRANSFER_YEAR,
				CONCAT(tb1.TRANSFER_NO,'/',tb1.TRANSFER_YEAR) AS SERIAL, 
				tb1.CURRENCY_ID,
				tb1.REFERENCE_NO,
				tb1.TRANSFER_DATE,
				tb1.COMPANY_ID,
				tb1.LOCATION_ID,
				tb1.CREATED_BY,
				tb1.CREATED_DATE,
				tb1.MODIFIED_BY,
				tb1.MODIFIED_DATE,
				tb1.SAVE_LEVELS,
				if(tb1.STATUS=1,'Approved',if(tb1.STATUS=0,'Rejected',if(tb1.STATUS=-10,'Completed',if(tb1.STATUS=-2,'Cancelled','Pending')))) as Status,
				mst_financecurrency.strCode as CURRENCY ,
				mst_locations.strName AS LOCATION,
				mst_companies.strName AS COMPANY,
				sys_users.strUserName AS CREATOR,
				
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(finance_bank_to_bank_transfer_header_approvedby.dtApprovedDate),')' )
								FROM
								finance_bank_to_bank_transfer_header_approvedby
								Inner Join sys_users ON finance_bank_to_bank_transfer_header_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								finance_bank_to_bank_transfer_header_approvedby.TRANSFER_NO  = tb1.TRANSFER_NO AND
								finance_bank_to_bank_transfer_header_approvedby.TRANSFER_YEAR =  tb1.TRANSFER_YEAR AND
								finance_bank_to_bank_transfer_header_approvedby.intApproveLevelNo =  '1' AND
								finance_bank_to_bank_transfer_header_approvedby.intStatus =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(finance_bank_to_bank_transfer_header_approvedby.dtApprovedDate),')' )
								FROM
								finance_bank_to_bank_transfer_header_approvedby
								Inner Join sys_users ON finance_bank_to_bank_transfer_header_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								finance_bank_to_bank_transfer_header_approvedby.TRANSFER_NO  = tb1.TRANSFER_NO AND
								finance_bank_to_bank_transfer_header_approvedby.TRANSFER_YEAR =  tb1.TRANSFER_YEAR AND
								finance_bank_to_bank_transfer_header_approvedby.intApproveLevelNo =  '$i' AND
								finance_bank_to_bank_transfer_header_approvedby.intStatus =  '0' 
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.STATUS>1) AND (tb1.STATUS<=tb1.SAVE_LEVELS) AND ((SELECT
								concat(sys_users.strUserName )
								FROM
								finance_bank_to_bank_transfer_header_approvedby
								Inner Join sys_users ON finance_bank_to_bank_transfer_header_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								finance_bank_to_bank_transfer_header_approvedby.TRANSFER_NO  = tb1.TRANSFER_NO AND
								finance_bank_to_bank_transfer_header_approvedby.TRANSFER_YEAR =  tb1.TRANSFER_YEAR AND
								finance_bank_to_bank_transfer_header_approvedby.intApproveLevelNo =  ($i-1) AND 
								finance_bank_to_bank_transfer_header_approvedby.intStatus='0' )<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.SAVE_LEVELS,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
								
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(finance_bank_to_bank_transfer_header_approvedby.dtApprovedDate),')' )
								FROM
								finance_bank_to_bank_transfer_header_approvedby
								Inner Join sys_users ON finance_bank_to_bank_transfer_header_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								finance_bank_to_bank_transfer_header_approvedby.TRANSFER_NO  = tb1.TRANSFER_NO AND
								finance_bank_to_bank_transfer_header_approvedby.TRANSFER_YEAR =  tb1.TRANSFER_YEAR AND
								finance_bank_to_bank_transfer_header_approvedby.intApproveLevelNo =  '0' AND
								finance_bank_to_bank_transfer_header_approvedby.intStatus =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS<=tb1.SAVE_LEVELS AND 
								tb1.STATUS>1),'Reject', '')) as `Reject`,";
								
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(finance_bank_to_bank_transfer_header_approvedby.dtApprovedDate),')' )
								FROM
								finance_bank_to_bank_transfer_header_approvedby
								Inner Join sys_users ON finance_bank_to_bank_transfer_header_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								finance_bank_to_bank_transfer_header_approvedby.TRANSFER_NO  = tb1.TRANSFER_NO AND
								finance_bank_to_bank_transfer_header_approvedby.TRANSFER_YEAR =  tb1.TRANSFER_YEAR AND
								finance_bank_to_bank_transfer_header_approvedby.intApproveLevelNo =  '-2' AND
								finance_bank_to_bank_transfer_header_approvedby.intStatus =  '0'
							),IF(((SELECT
								menupermision.intCancel 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND 
								tb1.STATUS=1),'Cancel', '')) as `Cancel`,";
								
							$sql .= "'View' as `View`   
				
FROM `finance_bank_to_bank_transfer_header` as tb1 
				INNER JOIN mst_financecurrency ON tb1.CURRENCY_ID = mst_financecurrency.intId
				INNER JOIN mst_locations ON tb1.LOCATION_ID = mst_locations.intId
				INNER JOIN mst_companies ON tb1.COMPANY_ID = mst_companies.intId
				INNER JOIN sys_users ON tb1.CREATED_BY = sys_users.intUserId
				where 1=1
				$where_string
 
		)  
		AS SUB_1 WHERE 1=1";
		
		

$jq 	= new jqgrid('',$db);	
$col 	= array();
$cols 	= array();

//BEGIN - ACTIVE/INACTIVE {
$col["title"] 			= "Status";
$col["name"] 			= "Status";
$col["width"] 			= "2"; 						// not specifying width will expand to fill space
$col["align"] 			= "center";
$col["sortable"] 		= true; 						// this column is not sortable
$col["search"] 			= true; 						// this column is not searchable
$col["editable"] 		= false;
$col["stype"] 			= "select";
$str 					= "Pending:Pending;Approved:Approved;Rejected:Rejected;Cancelled:Cancelled;:All" ;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;
//END 	- ACTIVE/INACTIVE }

$col["title"] 			= "Serial No";
$col["name"] 			= "SERIAL";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col['link']			= '?q='.$menuId.'&SerialNo={TRANSFER_NO}&SerialYear={TRANSFER_YEAR}';
$col["linkoptions"] 	= "target='_blank'";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Serial No";
$col["name"] 			= "TRANSFER_NO";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"] 			= true; 					
$col['link']			= '?q='.$menuId.'&SerialNo={TRANSFER_NO}&SerialYear={TRANSFER_YEAR}';
$col["linkoptions"] 	= "target='_blank'";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Serial Year";
$col["name"] 			= "TRANSFER_YEAR";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"] 			= true; 					
$col['link']			= '?q='.$menuId.'&SerialNo={TRANSFER_NO}&SerialYear={TRANSFER_YEAR}';
$col["linkoptions"] 	= "target='_blank'";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "CURRENCY";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Reference No";
$col["name"] 			= "REFERENCE_NO";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

//DATE
$col["title"]			= "Date"; // caption of column
$col["name"] 			= "TRANSFER_DATE"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "2";
$col["align"] 			= "center";
$cols[] 				= $col;	
$col					=NULL;
 
//FIRST APPROVAL
$col["title"] 			= "1st Approval"; // caption of column
$col["name"] 			= "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportId.'&SerialNo={TRANSFER_NO}&SerialYear={TRANSFER_YEAR}&mode=Confirm';
$col["linkoptions"] 	= "target='_blank'";
$col['linkName']		= 'Approve';
$cols[] 				= $col;	
$col					=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap		="2nd Approval";
	$ap1	="2nd_Approval";
	}
	else if($i==3){
	$ap		="3rd Approval";
	$ap1	="3rd_Approval";
	}
	else {
	$ap		=$i."th Approval";
	$ap1	=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] 			= $ap; // caption of column
$col["name"] 			= $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportId.'&SerialNo={TRANSFER_NO}&SerialYear={TRANSFER_YEAR}&mode=Confirm';
$col["linkoptions"] 	= "target='_blank'";
$col['linkName']		= 'Approve';
$cols[] 				= $col;	
$col					=NULL;
}
/*
$col["title"] 			= 'Reject'; // caption of column
$col["name"] 			= 'Reject'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= 'rptBankToBankTransfer.php?SerialNo={TRANSFER_NO}&SerialYear={TRANSFER_YEAR}&mode=Reject';
$col["linkoptions"] 	= "target='rptBankToBankTransfer.php'";
$col['linkName']		= 'Reject';
$cols[] 				= $col;	
$col					=NULL;*/

$col["title"] 			= 'Cancel'; // caption of column
$col["name"] 			= 'Cancel'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportId.'&SerialNo={TRANSFER_NO}&SerialYear={TRANSFER_YEAR}&mode=Cancel';
$col["linkoptions"] 	= "target='_blank'";
$col['linkName']		= 'Cancel';
$cols[] 				= $col;	
$col					=NULL;


//VIEW
$col["title"] 			= "Report"; // caption of column
$col["name"] 			= "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "2";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportId.'&SerialNo={TRANSFER_NO}&SerialYear={TRANSFER_YEAR}';
$col["linkoptions"]		= "target='_blank'";
$cols[] 				= $col;	
$col					=NULL;
 
 
$grid["caption"] 		= "Bank To Bank Transfer Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'TRANSFER_YEAR,TRANSFER_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "ASC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;

//$grid["postData"] = array("filters" => $sarr ); 

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Bank To Bank Transfer Listing</title>
<?php
echo $out;

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(finance_bank_to_bank_transfer_header.SAVE_LEVELS) AS appLevel
			FROM finance_bank_to_bank_transfer_header";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>

<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once $_SESSION['ROOT_PATH']."class/finance/cls_get_gldetails.php";
require_once $_SESSION['ROOT_PATH']."class/finance/cls_common_get.php";
require_once $_SESSION['ROOT_PATH']."class/finance/bank/bank_payment/cls_bank_payment_get.php";
require_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
//include 	 "include/javascript.html";
 
$progrmCode			= 'P0748';
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$session_userId 	= $_SESSION["userId"];

$serialNo			= (!isset($_REQUEST['SerialNo'])?'':$_REQUEST['SerialNo']);
$serialYear			= (!isset($_REQUEST['SerialYear'])?'':$_REQUEST['SerialYear']);
/*$serialNo			= 100009;
$serialYear			= 2013;
*/ 
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_GLDetails_get	= new Cls_Get_GLDetails($db);
$obj_fin_com_get	= new Cls_Common_Get($db);
$obj_bank_payment_get= new Cls_bank_payment_Get($db);


$header_array 		= $obj_bank_payment_get->get_header_array($serialYear,$serialNo);
$detail_result		= $obj_bank_payment_get->get_details_result($serialYear,$serialNo);

$totAmount			= $obj_bank_payment_get->get_tot_amount($serialYear,$serialNo);
$backDatePermision	= $obj_commonErr->Load_special_menupermision('19',$session_userId,'RunQuery');

$permition_arr		= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_save		= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_cancel($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_cancel	= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_reject	= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_confirm	= $permition_arr['permision'];

 if(!isset($header_array["BANK_PAYMENT_NO"]))
	$date	= date("Y-m-d");
else
	$date	= $header_array["PAYMENT_DATE"];
	
$exchangeRate		= $obj_fin_com_get->get_exchane_rate($header_array["CURRENCY_ID"],$date,$session_companyId,'RunQuery');
?>
<title>Bank Payment</title>

<!--<script type="text/javascript" src="presentation/finance_new/bank/bankPayment/bank_payment.js"></script>-->

<form id="frmBankPayment" name="frmBankPayment" method="post">
  <div align="center">
    <div class="trans_layoutS" style="width:1100px">
      <div class="trans_text">Bank Payment</div>
      <table width="1100">
        <tr>
          <td colspan="2"><table width="100%%" border="0" class="normalfnt">
              <tr>
                <td width="16%">Voucher No</td>
                <td width="36%"><input type="text" name="txtSerialNo" id="txtSerialNo" style="width:85px; text-align:right" disabled="disabled" value="<?php echo $serialNo?>"/>&nbsp;<input type="text" name="txtSerialYear" id="txtSerialYear" style="width:50px; text-align:right" disabled="disabled" value="<?php echo $serialYear?>"/></td>
                <td width="18%">Date <span class="compulsoryRed">*</span></td>
                <td width="30%"><input name="txtDate" type="text" <?php if($backDatePermision != 1){ ?> disabled="disabled"<?php } ?> value="<?php echo $date ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
              <tr>
                <td>Bank Account <span class="compulsoryRed">*</span></td>
                <td><select id="cboBank" name="cboBank" style="width:280px;height:20px" class="validate[required]">
                  <?php   echo $obj_GLDetails_get->getBankGLCombo($header_array["BANK_CHART_OF_ACCOUNT_ID"]); ?>
              
                  </select></td>
                <td>Payment Method <span class="compulsoryRed">*</span></td>
                <td id="td_order"><select id="cboPaymentMethod" name="cboPaymentMethod" style="width:280px;height:20px" class="validate[required]">
                  <?php   echo $obj_fin_com_get->getPayMethodCombo($header_array["PAYMENT_METHOD"]); ?>
                  </select></td>
              </tr>
              <tr>
                <td valign="">Currency <span class="compulsoryRed">*</span></td>
                <td><select id="cboCurrency" name="cboCurrency" style="width:280px;height:20px"  class="validate[required] cls_cbo_CurrencyCalExchangeRate" >
                  <?php   echo $obj_fin_com_get->getCurrencyCombo($header_array["CURRENCY_ID"]); ?>
                  </select>
                  <input type="text" name="txtExchangeRate" id="txtExchangeRate" style="width:60px;text-align:right" disabled="disabled" value="<?php echo $exchangeRate; ?>" title="Exchange Rate" class="cls_txt_exchangeRate validate[required]"/></td>
                <td>Bank Reference No</td>
                <td><input type="text" name="txtRefNo" id="txtRefNo" style="width:280px" value="<?php echo $header_array["BANK_REFERENCE_NO"]; ?>" maxlength="50"/></td>
              </tr>
              <tr>
                <td valign="top">Remarks</td>
                <td><textarea name="txtRemarksH" id="txtRemarksH" style="width:280px;height:50px" ><?php echo $header_array["REMARKS"]?></textarea></td>
                <td valign="top">&nbsp;</td>
                <td valign="top">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td colspan="2" ><div style="overflow:scroll;width:1100px;height:250px;">
              <table width="100%%" border="0" class="bordered" id="tblMain">
                <thead>
                <tr>
                    <th colspan="7" align="right"><div style="float:right"><a id="butInsertRow" class="button white small">Add New</a></div></th>
                  </tr>                  <tr>
                    <th width="4%">Del</th>
                    <th width="16%">Account <span class="compulsoryRed">*</span></th>
                    <th width="13%">Tax Code</th>
                    <th width="14%">Amount <span class="compulsoryRed">*</span></th>
                    <th width="24%">Remarks</th>
                    <th width="13%">Cost Center <span class="compulsoryRed">*</span></th>
                    <th width="16%">Settlement <span class="compulsoryRed">*</span></th>
                  </tr>
                </thead>
                <tbody>
                  <?php
$loop	= 0;
$cTot	=0;
$dTot	=0;
while($row = mysqli_fetch_array($detail_result))
{ 
	$booAvailable	= true;
 ?>
                  <tr>
                    <td class="cls_td_check" style="text-align:center"><img src="images/del.png" width="15" height="15" alt="del" class="delImg" /></td>
                    <td class="cls_td_salesOrderNo"><select style="width:100%;height:20px"  id="cboAccount" class="claAccount validate[required]">
					<?php echo  $glCombo = $obj_GLDetails_get->getGLCombo('BANK_PAYMENT',$row['CHART_OF_ACCOUNT_ID']); ?>
                      </select></td>
                    <td width="13%"><span class="cls_td_salesOrderNo">
                      <select class="clsTax"  id="cboTax" style="width:100%;height:20px">
                    <?php echo $obj_fin_com_get->getTaxCombo($row['TAX_ID']); 
					 ?>
                      </select>
                      </span></td>
                    <td width="14%"><input type="text" style="width:100%; text-align:right" value="<?php echo $row['AMOUNT']; ?>"  id="txtAmount" class="clsAmount validate[required,custom[number]]"/></td>
                    <td class="cls_td_qty" style="text-align:right"><textarea style="width:100%;height:20px" class="clsRemarks" ><?php echo $row['REMARKS']; ?></textarea></td>
                    <td class="cls_td_PD cls_Subtract" style="text-align:center"><span class="cls_td_salesOrderNo">
                      <select name="cboCostCenter" class="clsCostCenter validate[required]" id="cboCostCenter" style="width:100%;height:20px" >
                    <?php echo $obj_fin_com_get->getCostCenterCombo($row['COST_CENTER_ID']);  ?>
                      </select>
                      </span></td>
                    <td class="cls_td_PD cls_Subtract" style="text-align:center"><span class="cls_td_salesOrderNo">
                      <select name="cboJECombo" id="cboJECombo" class="clsJE validate[required]"  style="width:100%" >
                        <?php
						
					  		echo $obj_fin_com_get->getJECombo($row["SETTELEMENT_YEAR"],$row["SETTELEMENT_NO"],$row["SETTELEMENT_ORDER"],$row["CHART_OF_ACCOUNT_ID"]);
							//echo Get_GL_HTML($row["GL_ACCOUNT"]);
                      ?>
                      </select>
                    </span></td>
                  </tr>
                  <?php
}
if(!$booAvailable)
{
?>
                  <tr>
                    <td class="cls_td_check" style="text-align:center"><img src="images/del.png" width="15" height="15" alt="del" class="delImg" /></td>
                    <td class="cls_td_salesOrderNo"><select style="width:100%;height:20px"  id="cboAccount" class="claAccount validate[required]">
					<?php   echo $glCombo = $obj_GLDetails_get->getGLCombo('BANK_PAYMENT',$row['ACCOUNT_ID']); ?>
                      </select></td>
                    <td width="13%"><span class="cls_td_salesOrderNo">
                      <select class="clsTax"  id="cboTax" style="width:100%;height:20px">
                    <?php echo $obj_fin_com_get->getTaxCombo($row['TAX_ID']);  ?>
                      </select>
                      </span></td>
                    <td width="14%"><input type="text" style="width:100%;text-align:right" value="0"  id="txtAmount" class="clsAmount validate[required,custom[number]]"/></td>
                    <td class="cls_td_qty" style="text-align:right"><textarea style="width:100%;height:20px" class="clsRemarks" ></textarea></td>
                    <td class="cls_td_PD cls_Subtract" style="text-align:center"><span class="cls_td_salesOrderNo">
                      <select name="cboCostCenter" class="clsCostCenter validate[required]" id="cboCostCenter" style="width:100%;height:20px" >
                    <?php echo $obj_fin_com_get->getCostCenterCombo($row['COST_CENTER']);  ?>
                      </select>
                      </span></td>
                    <td class="cls_td_PD cls_Subtract" style="text-align:center"><span class="cls_td_salesOrderNo">
                      <select name="cboJECombo2" id="cboJECombo2" class="clsJE validate[required]"  style="width:100%" >
                        <?php
					  		echo $obj_fin_com_get->getJECombo('','','','');
							//echo Get_GL_HTML($row["GL_ACCOUNT"]);
                      ?>
                      </select>
                    </span></td>
                  </tr>
<?php
	}
 	  $glCombo = $obj_GLDetails_get->getGLCombo('BANK_PAYMENT','');
	  $costCenter = $obj_fin_com_get->getCostCenterCombo('');
?>
                  
                </tbody>
              </table>
          </div></td>
        </tr>
        <tr>
          <td width="510" >&nbsp;</td>
          <td width="586" valign="top"><table width="266" border="0" align="right" class="normalfnt" cellpadding="0" cellspacing="2">
              <tr>
                <td width="80">Sub Total</td>
                <td width="10">&nbsp;</td>
                <td width="96"><input name="txtSubTotal" type="text" disabled="disabled" id="txtSubTotal" value="0.0000" style="width:100%;text-align:right" /></td>
              </tr>
              <tr>
                <td>Tax Total</td>
                <td>&nbsp;</td>
                <td><input name="txtTotTax" type="text" disabled="disabled" id="txtTotTax" value="0.0000" style="width:100%;text-align:right" /></td>
              </tr>
              <tr>
                <td>Grand Total</td>
                <td>&nbsp;</td>
                <td><input name="txtGrandTotal" type="text" disabled="disabled" id="txtGrandTotal" value="0.0000" style="width:100%;text-align:right" /></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2" align="center">
                <a class="button white medium" id="butNew">New</a>
                <a class="button white medium" id="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a>
                <a class="button white medium" id="butConfirm"  <?php if($permision_confirm!=1){ ?> style="display:none"<?php } ?>>Approve</a>
                <a class="button white medium" id="butCancel" <?php if($permision_cancel!=1){ ?>  style="display:none"<?php } ?>>Cancel</a>
                <a class="button white medium" id="butReport">Report</a>
                <a href="main.php" class="button white medium" id="butClose">Close</a>
          </td>
        </tr>
      </table>
    </div>
  </div>
</form>
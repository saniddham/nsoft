<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once $_SESSION['ROOT_PATH']."class/finance/cls_get_gldetails.php";
require_once $_SESSION['ROOT_PATH']."class/finance/cls_common_get.php";
require_once $_SESSION['ROOT_PATH']."class/finance/bank/bank_payment/cls_bank_payment_get.php";
require_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
require_once $_SESSION['ROOT_PATH']."class/finance/cls_common_get.php";
 
$progrmCode			= 'P0748';
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$session_userId 	= $_SESSION["userId"];

$jurnalReportId		= 986;

$serialNo			= $_REQUEST["SerialNo"];
$serialYear			= $_REQUEST["SerialYear"];
$mode				= (!isset($_REQUEST["mode"])?'':$_REQUEST["mode"]);
/*$serialNo			= 100009;
$serialYear			= 2013;
*/ 
$obj_fin_com		= new Cls_Common_Get($db); 
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_GLDetails_get	= new Cls_Get_GLDetails($db);
$obj_fin_com_get	= new Cls_Common_Get($db);
$obj_bank_payment_get= new Cls_bank_payment_Get($db);

$header_array 		= $obj_bank_payment_get->get_Report_header_array($serialYear,$serialNo);
$detail_result		= $obj_bank_payment_get->get_Report_details_result($serialYear,$serialNo);
$totAmount			=$obj_bank_payment_get->get_tot_amount($serialYear,$serialNo);

$permition_arr		= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_save		= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_cancel($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_cancel	= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_reject	= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_confirm	= $permition_arr['permision'];

$locationId			= $header_array["LOCATION_ID"];

if(!isset($_REQUEST["SerialNo"]))
	$jernalDate	= date("Y-m-d");
else
	$jernalDate	= $header_array["PAYMENT_DATE"];
	
$company_Id			= $header_array["COMPANY_ID"];
?>
<head>
<title>Bank Payment Report</title>

<script type="text/javascript" src="presentation/finance_new/bank/bankPayment/bank_payment.js"></script>

 </head>
<body>
<style type="text/css">
.apDiv1 {
	position:absolute;
	left:380px;
	top:100px;
	width:auto;
	height:auto;
	z-index:0;
	opacity:0.1;
}
</style>
 
 <form id="frmRptBank_payments" name="frmRptBank_payments" method="post" action="rptBank_payments.php">
  <table width="900" align="center">
    <tr>
      <td><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
      <td class="reportHeader" align="center">Bank Payment</td>
    </tr>
	<?php
		include "presentation/report_approve_status_and_buttons.php"
     ?>

   <tr>
      <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td width="14%">Bank Payment No</td>
            <td width="1%">:</td>
            <td width="43%"><?php echo $obj_fin_com->getSerialNo($header_array["SERIAL_NO"],$header_array["SERIAL_DATE"],$company_Id,'RunQuery')//echo $header_array["ADVANCE_NO"]; ?></td>
            <td width="14%">Payment Date</td>
            <td width="2%">:</td>
            <td width="26%"><?php echo $header_array["PAYMENT_DATE"]?></td>
          </tr>
<tr>
            <td>Bank</td>
            <td>:</td>
            <td><?php echo $header_array["CHART_OF_ACCOUNT_NAME"]?></td>
            <td>Pay Method</td>
            <td>:</td>
            <td><?php echo $header_array["PAY_TO"]?></td>
          </tr>          <tr>
            <td>Reference No</td>
            <td>:</td>
            <td><?php echo $header_array["BANK_REFERENCE_NO"]?></td>
            <td>Currency</td>
            <td>:</td>
            <td><?php echo $header_array["CURRENCY"]?></td>
          </tr>
<tr>
            <td>Company</td>
            <td>:</td>
            <td><?php echo $header_array["COMPANY"]?></td>
            <td>Location</td>
            <td>:</td>
            <td><?php echo $header_array["LOCATION"]?></td>
          </tr>  
<tr>
            <td>Remarks</td>
            <td>:</td>
            <td><?php echo $header_array["REMARKS"]?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>                </table></td>
    </tr>
    <tr>
      <td align="right"><a class="button green small" target="rpt_payment_voucher_pdf.php" href="presentation/finance_new/reports/rpt_payment_voucher_pdf.php?SerialNo=<?php echo $serialNo?>&SerialYear=<?php echo $serialYear?>&Type=BANK_PAYMENT">Click here to print voucher</a></td>
    </tr>
    <tr>
      <td ><table width="100%" border="0" class="rptBordered" id="tblMain">
          <thead>
            <tr>
              <th width="34%">Account</th>
               <th width="18%">Tax Code</th>
              <th width="12%">Amount</th>
               <th width="19%">Remarks</th>
              <th width="17%">Cost Center</th>
              <th width="16%">Settele No</th>
            </tr>
          </thead>
          <tbody>
            <?php
$creditTot	= 0;
$debitTot	= 0;
$diff		= 0;
$tot		= 0;
while($row = mysqli_fetch_array($detail_result))
{
$no			=$row["SETTELEMENT_NO"];
$year		=$row["SETTELEMENT_YEAR"];
$link		= "?q=".$jurnalReportId."&SerialNo=$no&SerialYear=$year";
  ?>
        <tr>
          <td class="cls_td_salesOrderNo" nowrap="nowrap"><?php echo $row["CHART_OF_ACCOUNT_NAME"].' - '.$row["accountCode"]?>&nbsp;</td>
          <td align="left"><?php echo $row["TAX"]?>&nbsp;</td>
          <td class="cls_td_invoice cls_Subtract" style="text-align:right" nowrap="nowrap"><?php echo number_format($row["AMOUNT"],2)?>&nbsp;</td>
          <td align="left"><?php  echo $row["REMARKS"];  ?>&nbsp;</td>
           <td align="left"><?php  echo $row["COST_CENTER"];  ?>&nbsp;</td>
         <?php
            if($no!=''){
          ?>
          <td align="center"><a href="<?php echo $link; ?>" target="_blank" ><?php echo $no.'/'.$year; ?></a></td>
         <?php
            }
            else{
          ?>
          <td align="center"></td>
         <?php
            }
          ?>
         </tr>
        <?php
             $tot +=$row["AMOUNT"];
   }
 ?>
<tr bgcolor="#EAEAEA">
          <td colspan="2" class="cls_td_salesOrderNo" align="center" ><b>Total</b></td>
          <td align="right"><b><?php echo number_format($tot,2); ?></b></td>
          <td align="right" colspan="3">&nbsp;</td>
          
         </tr>          
</tbody>
        </table></td>
    </tr>
     <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
    <td>
    <?php
			$creator		= $header_array['CREATOR'];
			$createdDate	= $header_array['CREATED_DATE'];
 			$resultA 		= $obj_bank_payment_get->get_Report_approval_details_result($serialYear,$serialNo);
			include "presentation/report_approvedBy_details.php"
 	?>
</td>
    </tr>
  </table>
</form>
</body>
<?php
 ?>
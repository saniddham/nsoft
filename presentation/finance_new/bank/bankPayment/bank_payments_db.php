<?php
session_start();
//BEGIN - INCLUDE FILES {
include_once $_SESSION['ROOT_PATH']."dataAccess/Connector.php";
require_once $_SESSION['ROOT_PATH']."class/finance/cls_common_get.php";
require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
require_once $_SESSION['ROOT_PATH']."class/finance/cls_get_gldetails.php";
require_once $_SESSION['ROOT_PATH']."class/finance/bank/bank_payment/cls_bank_payment_set.php";
require_once $_SESSION['ROOT_PATH']."class/finance/bank/bank_payment/cls_bank_payment_get.php";
include_once $_SESSION['ROOT_PATH']."class/finance/accountant/journalEntry/cls_journalEntry_get.php";
 
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {

$obj_fin_com_get		= new Cls_Common_Get($db);
$obj_fin_comfunc_get	= new cls_commonFunctions_get($db);
$obj_GLDetails_get		= new Cls_Get_GLDetails($db);
$obj_bank_payment_set	= new Cls_bank_payment_Set($db);
$obj_bank_payment_get	= new Cls_bank_payment_Get($db);
$obj_fin_journal_get	= new Cls_JournalEntry_Get($db);

//END 	- CREATE OBJECTS }
$requestType	= $_REQUEST["RequestType"];

if($requestType=="getGLAccountOptions")
{
	$glCombo 				= $obj_GLDetails_get->getGLCombo('BANK_PAYMENT','');
	$response['combOptions']=$glCombo;
 	echo json_encode($response);
} 
if($requestType=="getCostCenterOptions")
{
	$costCenter 			= $obj_fin_com_get->getCostCenterCombo('');
	$response['combOptions']=$costCenter;
 	echo json_encode($response);
} 
if($requestType=="getTaxOptions")
{
	$tax 			= $obj_fin_com_get->getTaxCombo('');
	$response['combOptions']=$tax;
 	echo json_encode($response);
} 
if($requestType=="getPayToOptions")
{
	$payTo 			= $obj_fin_com_get->getTaxCombo('');
	$response['combOptions']=$payTo;
 	echo json_encode($response);
} 
if($requestType=="getJEOptions")
{
	$account				=$_REQUEST["account"];
	$costCenter 			= $obj_fin_com_get->getJECombo('','','',$account);
	$response['combOptions']=$costCenter;
 	echo json_encode($response);
} 
if($requestType=="loadExchangeRate")
{
	$date					=$_REQUEST["date"];
	$currency				=$_REQUEST["currency"];
	$val		 			= $obj_fin_com_get->get_exchane_rate($currency,$date,$session_companyId,'RunQuery');
	$response['exchaRate']	=$val;
 	echo json_encode($response);
} 
if($requestType=="loadBalAmount")
{
   	$Header				= json_decode($_REQUEST["Header"],true);
 	
	foreach($Header as $array_loop)
	{
		$settelement	= $obj_fin_comfunc_get->replace($array_loop["settelement"]);
	}
	$settelement 		= explode("/", $settelement);
	$settelementYear	= $settelement[0];  
	$settelementNo		= $settelement[1];  
	$settelementOrder	= $settelement[2]; 
	
	$balAmount 		= $obj_fin_journal_get->get_balanceToSetele_jernal_amount($settelementYear,$settelementNo,$settelementOrder,'RunQuery');
	$response['balAmount']	=$balAmount;
 	echo json_encode($response);
} 

elseif($requestType=="URLSave")
{
   
   	$Header				= json_decode($_REQUEST["Header"],true);
  	$Grid				= json_decode($_REQUEST["Grid"],true);
	
	
	foreach($Header as $array_loop)
	{
 		$serialYear		= $array_loop["serialYear"];
		$serialNo		= $array_loop["serialNo"];
		$date			= $array_loop["date"];
 		$currency		= $array_loop["currency"];
 		$bank			= $array_loop["bank"];
 		$paymentMethod	= $array_loop["paymentMethod"];
 		$referenceNo	= $obj_fin_comfunc_get->replace($array_loop["referenceNo"]);
 		$remarks		= $obj_fin_comfunc_get->replace($array_loop["remarks"]);
 	}
	$response			= $obj_bank_payment_get->ValidateBeforeSave($serialYear,$serialNo);
	
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_bank_payment_set->Save($serialYear,$serialNo,$date,$currency,$bank,$paymentMethod,$referenceNo,$remarks,$Grid);
} 
elseif($requestType=="approve")
{
	$serialNo			= $_REQUEST["SerialNo"];
	$serialYear			= $_REQUEST["SerialYear"];
	
	$response			= $obj_bank_payment_get->ValidateBeforeApprove($serialYear,$serialNo);
	
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	
	echo $obj_bank_payment_set->Approve($serialYear,$serialNo);
}
elseif($requestType=="reject")
{
	$serialNo			= $_REQUEST["SerialNo"];
	$serialYear			= $_REQUEST["SerialYear"];
	
	$response			= $obj_bank_payment_get->ValidateBeforeReject($serialYear,$serialNo);
	
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	
	echo $obj_bank_payment_set->Reject($serialYear,$serialNo);
}
elseif($requestType=="cancel")
{
	$serialNo			= $_REQUEST["SerialNo"];
	$serialYear			= $_REQUEST["SerialYear"];
	
	$response			= $obj_bank_payment_get->ValidateBeforeCancel($serialYear,$serialNo);
	
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	
	echo $obj_bank_payment_set->Cancel($serialYear,$serialNo);
}


?>
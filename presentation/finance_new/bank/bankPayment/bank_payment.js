var glCombo;
var costCenter;
var taxCombo;
var payToCombo;
var basePath		= "presentation/finance_new/bank/bankPayment/";
var voucherRptPath	= "presentation/finance_new/reports/";
var reportId		= 987;
var menuId			= 748;

$(document).ready(function() {
	
 	$('#frmBankPayment').validationEngine();
	
	$('#frmBankPayment #butInsertRow').die('click').live('click',addNew);
	$('#frmBankPayment .clsTax').die('change').live('change',CalculateTax);
	$('#frmBankPayment #butNew').die('click').live('click',New);
	$('#frmBankPayment #butSave').die('click').live('click',Save);
	$('#frmBankPayment #butConfirm').die('click').live('click',Confirm);
	$('#frmBankPayment #butReject').die('click').live('click',Reject);
	$('#frmBankPayment #butCancel').die('click').live('click',Cancel);
	$('#frmBankPayment #butReport').die('click').live('click',Report);
 	
	$('#frmRptBank_payments #butRptConfirm').die('click').live('click',ConfirmRpt);
	$('#frmRptBank_payments #butRptReject').die('click').live('click',RejectRpt);
	$('#frmRptBank_payments #butRptCancel').die('click').live('click',CancelRpt);

	$('#frmBankPayment .claAccount').die('change').live('change',function(){
 			var combo	=	getJECombo($(this).parent().parent().find('.claAccount').val())
			$(this).parent().parent().find('.clsJE').html(combo);
 	});	
	$('#frmBankPayment .clsJE').die('change').live('change',function(){
 			loadBalAmount(this);
	});	

	$('#frmBankPayment #tblMain input').die('keyup').live('keyup',function(e){
			if($(this).val()=="" || isNaN($(this).val())){
				$(this).val(0);
				$(this).select();
			}
			CaculateTotalValue();
	});
 	
	$('#frmBankPayment #tblMain .delImg').die('click').live('click',function(){
 		 	$(this).parent().parent().remove();
			CaculateTotalValue();
	});
	
 
	
 });

glCombo 	= getGLAccountCombo();
costCenter	= getCostCenterCombo();
taxCombo	= getTaxCombo();
payToCombo	= getPayToCombo();
JE 			= getJECombo('');
function addNew()
{  	
	if(!$('#frmBankPayment').validationEngine('validate')){
		setTimeout("alertx('')",2000);
		return;
	}
 	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);		
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML  = "<img class=\"delImg mouseover\" width=\"15\" height=\"15\" src=\"images/del.png\">";
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:center');
  	cell.innerHTML 	= "<select style=\"width:100%;height:20px\"  id=\"cboAccount\" class=\"claAccount validate[required]\">"+glCombo+"</select>";
	
	var cell		= row.insertCell(2);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML  = "<select class=\"clsTax validate[required]\"  id=\"cboTax\" style=\"width:100%;height:20px\">"+taxCombo+"</select>";
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML 	= "<input type=\"text\" style=\"width:100%;  text-align:right \" value=\"0\"  id=\"txtAmount\" class=\"clsAmount validate[required,custom[number]]\"/>";
	
 	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<textarea style=\"width:100%;height:20px\" class=\"clsRemarks\" ></textarea>";

	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<select name=\"cboCostCenter\" class=\"clsCostCenter validate[required]\" id=\"cboCostCenter\" style=\"width:100%;height:20px\" >"+costCenter+"</select>";

	var cell 		= row.insertCell(6);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<select id=\"cboJECombo\" class=\"clsJE\" style=\"width:100%\" name=\"cboJECombo\">"+JE+"</select>";

}

function Report()
{
	if($('#frmBankPayment #txtSerialNo').val()=="")
	{
		alert("No details available to view report.");
		return;
	}
 	var url  = voucherRptPath+"rpt_payment_voucher_pdf.php?SerialNo="+$('#frmBankPayment #txtSerialNo').val();
	    url += "&SerialYear="+$('#frmBankPayment #txtSerialYear').val();
	    url += "&Type=BANK_PAYMENT";
 	window.open(url,'rpt_payment_voucher_pdf.php');
}
function Cancel()
{
  	var url  = "?q="+reportId+"&SerialNo="+$('#frmBankPayment #txtSerialNo').val();
	    url += "&SerialYear="+$('#frmBankPayment #txtSerialYear').val();
	    url += "&mode=Cancel";
	window.open(url,'rptBank_payments.php');
}
function Reject()
{
  	var url  = "?q="+reportId+"&SerialNo="+$('#frmBankPayment #txtSerialNo').val();
	    url += "&SerialYear="+$('#frmBankPayment #txtSerialYear').val();
	    url += "&mode=Reject";
	window.open(url,'rptBank_payments.php');
}
function Confirm()
{
  	var url  = "?q="+reportId+"&SerialNo="+$('#frmBankPayment #txtSerialNo').val();
	    url += "&SerialYear="+$('#frmBankPayment #txtSerialYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rptBank_payments.php');
}


function CaculateTotalValue()
{
 	var total		= 0;
	var tax_tot		=0;
	var grand_tot	=0;
	
	$('#frmBankPayment #tblMain .clsAmount').each(function(){
			total += parseFloat($(this).val());
  	});
	total	= parseFloat(RoundNumber(total,4));
	
	grand_tot =total+tax_tot;
 	
	$('#frmBankPayment #txtSubTotal').val(RoundNumber(total,4));	
	$('#frmBankPayment #txtTotTax').val(RoundNumber(tax_tot,4));	
	$('#frmBankPayment #txtGrandTotal').val(RoundNumber(grand_tot,4));
}

 
 
function Save()
{
	if(!IsProcessMonthLocked_date($('#frmBankPayment #txtDate').val()))
		return;
			
	showWaiting();
	if(!Validate_save()){
		hideWaiting();
		setTimeout("alertx('')",4000);
		return;
	}
	
	var arrh	  = "[";
 			arrh += "{";
			arrh += '"currency":"'+$('#frmBankPayment #cboCurrency').val()+'",' ;
			arrh += '"date":"'+$('#frmBankPayment #txtDate').val()+'",';
			arrh += '"bank":"'+$('#frmBankPayment #cboBank').val()+'",';
			arrh += '"referenceNo":'+URLEncode_json($('#frmBankPayment #txtRefNo').val())+',';
			arrh += '"remarks":'+URLEncode_json($('#frmBankPayment #txtRemarksH').val())+',';
			arrh += '"paymentMethod":"'+$('#frmBankPayment #cboPaymentMethod').val()+'",' ;
			arrh += '"serialNo":"'+$('#frmBankPayment #txtSerialNo').val()+'",' ;
			arrh += '"serialYear":"'+$('#frmBankPayment #txtSerialYear').val()+'"';
  			arrh +=  '},';
 
 	arrh 		 = arrh.substr(0,arrh.length-1);
	arrh 		+= " ]";
 
	var arr		= "[";
	$('#tblMain .clsAmount').each(function(){
		if($(this).val()!=0)
		{
			arr += "{";
			arr += '"account":"'+$(this).parent().parent().find('.claAccount').val()+'",' ;
			arr += '"taxCode":"'+$(this).parent().parent().find('.clsTax').val()+'",';
			arr += '"amount":"'+$(this).parent().parent().find('.clsAmount').val()+'",';
 			arr += '"remarks":'+URLEncode_json($(this).parent().parent().find('.clsRemarks').val())+',';
			arr += '"settelement":'+URLEncode_json($(this).parent().parent().find('.clsJE').val())+',';
			arr += '"costCenter":"'+$(this).parent().parent().find('.clsCostCenter').val()+'"';
 
  			arr +=  '},';
		}
	});

	arr 		 = arr.substr(0,arr.length-1);
	arr 		+= " ]"; 
 
 	var url 	= basePath+"bank_payments_db.php?RequestType=URLSave";
	var data 	= "Header="+arrh;
 		data   += "&Grid="+arr;
 		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
		{
			$('#frmBankPayment #butSave').validationEngine('showPrompt', json.msg,json.type );
			if(json.type=='pass')
			{
				$('#frmBankPayment #txtSerialNo').val(json.serialNo);
				$('#frmBankPayment #txtSerialYear').val(json.serialYear);
				if(json.permision_confirm==1){
 					$('#frmBankPayment #butConfirm').css('display', 'inline'); 
				}
				$('#frmBankPayment #butReport').css('display','inline'); 
			}
		},
	error:function(xhr,status)
		{
			$('#frmBankPayment #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
		}
	});
	var t = setTimeout("alertx('#butSave')",4000);
	hideWaiting();
}

function alertx(but)
{
	$('#frmBankPayment '+but).validationEngine('hide')	;
}

function New()
{
	window.location.href = '?q='+menuId;
}

function CalculateTax()
{
	var obj		= $(this);
	var url 	= basePath+"bank_payments_db.php?RequestType=URLCalculateTax";
	var data 	= "TaxId="+$(this).val();
		data   += "&Value="+parseFloat($(this).parent().parent().find('.cls_td_value').html());
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		obj.parent().attr('id',json.TaxValue);
		CaculateTotalValue();
	}
	});		
}

 
function LoadAutoData(customerPO)
{
	 $('#frmBankPayment #txtCustomerPONo').val(customerPO);
	 $('#frmBankPayment #btnSearch').click();
}

function AddNewGL()
{	
	$('#frmBankPayment #tblGL tbody tr:last').after("<tr>"+$('#frmBankPayment #tblGL .cls_tr_firstRow').html()+"</tr>");
}

function CalculateGLTotals()
{
	var total	= 0;
	$('#frmBankPayment #tblGL .cls_td_GLAmount').each(function(){
    	 total	+= isNaN(parseFloat($(this).children().val()))? 0:parseFloat($(this).children().val());
    });
	total	= RoundNumber(total,4,true);
	$('#frmBankPayment #tblGL .td_totalGLAlloAmount').val(total);
}

function Validate_save()
{
 	
	if(!$('#frmBankPayment').validationEngine('validate')){
		return false;
	}
/*	else if(hasDuplicates(arr_accounts)==true){
		alert('Duplicate Accounts exists');
		return false;
	}
*/	
/*	if($('#frmBankPayment #txtSerialNo').val()!="")
	{
		$('#frmBankPayment #butSave').validationEngine('showPrompt','Already this Bank Payments No saved.Please refresh the page','fail');
		var t = setTimeout("alertx()",2000);
		return false;
	}*/
 	else{
 	return true;
	}
}

function CheckAll()
{
	if($(this).is(':checked'))
		$('#frmBankPayment #tblMain .cls_check').attr('checked','checked');
	else
		$('#frmBankPayment #tblMain .cls_check').removeAttr('checked');
		
	$('#frmBankPayment #tblMain .cls_check').each(function(){
		ValidateRowWhenCheck($(this));
	});
}

function ValidateRowWhenCheck(obj)
{
	if(obj.is(':checked'))
	{
		obj.parent().parent().find('.cls_td_invoice').children().attr('disabled','');
	}
	else
	{
		obj.parent().parent().find('.cls_td_invoice').children().attr('disabled','disabled');
	}
	CaculateTotalValue();
}


function getGLAccountCombo(){
	
	var optionss = '';
 	var url 	= basePath+"bank_payments_db.php?RequestType=getGLAccountOptions";
	
	var httpobj = $.ajax({
	url:url,
	data:'',
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		optionss = json.combOptions;
 	}
	});		
	return optionss;
}

function getCostCenterCombo(){
	
	var optionss = '';
 	var url 	= basePath+"bank_payments_db.php?RequestType=getCostCenterOptions";
	
	var httpobj = $.ajax({
	url:url,
	data:'',
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		optionss = json.combOptions;
 	}
	});		
	return optionss;
	
}

function getTaxCombo(){
	
	var optionss = '';
 	var url 	= basePath+"bank_payments_db.php?RequestType=getTaxOptions";
	
	var httpobj = $.ajax({
	url:url,
	data:'',
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		optionss = json.combOptions;
 	}
	});		
	return optionss;
	
}
function getPayToCombo(){
	
	var optionss = '';
 	var url 	= basePath+"bank_payments_db.php?RequestType=getPayToOptions";
	
	var httpobj = $.ajax({
	url:url,
	data:'',
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		optionss = json.combOptions;
 	}
	});		
	return optionss;
	
}
function getJECombo(account){
	
	var optionss = '';
 	var url 	= basePath+"bank_payments_db.php?RequestType=getJEOptions";
	var data 	= "account="+account;
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		optionss = json.combOptions;
 	}
	});		
	return optionss;
	
}


function hasDuplicates(array) {
	var valuesSoFar = [];
	for (var i = 0; i < array.length; ++i) {
		var value = array[i];
		if (valuesSoFar.indexOf(value) !== -1) {
			return true;
		}
 	}
	return false;
}	



 function ConfirmRpt(){
	 
	var val = $.prompt('Are you sure you want to approve this Bank Payments ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"bank_payments_db.php"+window.location.search+'&RequestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptBank_payments #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptBank_payments #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
					   hideWaiting();
				}});
  }
  
  
  function RejectRpt(){
	  
	var val = $.prompt('Are you sure you want to reject this Bank Payments ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				showWaiting();
				var url = basePath+"bank_payments_db.php"+window.location.search+'&RequestType=reject';
				var obj = $.ajax({
					url:url,
					type:'post',
					dataType: "json",  
					data:'',
					async:false,
					
					success:function(json){
							$('#frmRptBank_payments #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
								window.location.href = window.location.href;
								window.opener.location.reload();//reload listing page
								return;
							}
						},
					error:function(xhr,status){
							
							$('#frmRptBank_payments #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);
						}		
					});
				////////////
					}
					hideWaiting();
			}});
	  
  }

 
function CancelRpt(){

	var val = $.prompt('Are you sure you want to cancel this Bank Payments ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
				if(v)
				{
					showWaiting();
					var url = basePath+"bank_payments_db.php"+window.location.search+'&RequestType=cancel';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptBank_payments #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptBank_payments #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
						hideWaiting();
 				}});
	
}

function loadBalAmount(obj)
{
 	var url 	= basePath+"bank_payments_db.php?RequestType=loadBalAmount";
	
 	var arrh	  = "[";
 			arrh += "{";
			arrh += '"settelement":'+URLEncode_json($(obj).parent().parent().find('.clsJE').val());
  			arrh +=  '},';
	
  
 	arrh 		 = arrh.substr(0,arrh.length-1);
	arrh 		+= " ]";
	var data 	= "Header="+arrh;
	
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		balAmount = json.balAmount;
 		$(obj).parent().parent().find('.debit').removeClass('debit validate[custom[number]').addClass('debit validate[custom[number],max['+balAmount+']]');
		$(obj).parent().parent().find('.credit').removeClass('credit validate[custom[number]').addClass('credit validate[custom[number],max['+balAmount+']]');
 	}
	});		
 }
// JavaScript Document
var i = 0;
var basePath	= "presentation/finance_new/bank/bankReconciliations/";
var reportId	= 984 ;
var menuId		= 753 ;
$(document).ready(function(){
	
	$("#frmBankReconciliations").validationEngine();
	
	/*if(intAddx)
	{
		$('#frmBankReconciliations #butNew').show();
		$('#frmBankReconciliations #butSave').show();
	}
	//permision for edit 
	if(intEditx)
	{
		$('#frmBankReconciliations #butNew').show();
		$('#frmBankReconciliations #butSave').show();
	}*/
	
	//$('#frmBankReconciliations #cboAccounts').die('change').live('change',loadData);
	//$('#frmBankReconciliations #cboCurrency').die('change').live('change',loadData);
	$('#frmBankReconciliations #butSearch').die('click').live('click',loadData);
	$('#frmBankReconciliations .clsChkRec').die('click').live('click',setAmonut);
	$('#frmBankReconciliations #butSave').die('click').live('click',saveData);
	
	$('#frmBankReconciliations #butNew').die('click').live('click',clearAll);
	$('#frmBankReconciliations #butConfirm').die('click').live('click',Confirm);
	$('#frmBankReconciliations #butReject').die('click').live('click',Reject);
	$('#frmBankReconciliations #butCancel').die('click').live('click',Cancel);
	$('#frmBankReconciliations #butReport').die('click').live('click',Report);
	
});
function loadData()
{
	clearData();
	
	if($('#cboAccounts').val()=='')
	{	
		return;
	}
	if($('#cboCurrency').val()=='')
	{
		return;
	}
	
	var accountId 	= $('#frmBankReconciliations #cboAccounts').val();
	var currencyId 	= $('#frmBankReconciliations #cboCurrency').val();
	var toDate  	= $('#frmBankReconciliations #txtToDate').val();
	
	var url 	 = basePath+"bankReconciliations-db.php";
	var data	 = "requestType=loadData&accountId="+accountId+"&currencyId="+currencyId+"&toDate="+toDate;
	var obj = $.ajax({
						url:url,
						type:'POST',
						dataType:'json',  
						data:data,
						async:false,
						success:function(json){
							
							$('#frmBankReconciliations #txtLedgerBalance').val(json.ledgerBalance);
							$('#frmBankReconciliations #txtStatementBalance').val(json.statementBal);
							$('#frmBankReconciliations #txtSavedStatementBal').val(json.statementBal);
							
							if(json.arrDepositData!=null)
							{
								var lengthDepDetail   	= json.arrDepositData.length;
								var arrDepositData  	= json.arrDepositData;	
								
								for(var i=0;i<lengthDepDetail;i++)
								{
									var serialId		= arrDepositData[i]['serialId'];
									var dtDate			= arrDepositData[i]['dtDate'];
									var docNo			= arrDepositData[i]['docNo'];	
									var amount			= RoundNumber(arrDepositData[i]['AMOUNT'],2);
									var transacTypeNm	= arrDepositData[i]['transacTypeName'];
									var chartOfAccName	= arrDepositData[i]['chartOfAccName'];
									var bankRefNo		= arrDepositData[i]['bankRefNo'];
									var remarks			= arrDepositData[i]['remarks'];
									var trType			= arrDepositData[i]['transacType'];
								
									createGrid('tblDeposit',serialId,dtDate,docNo,amount,transacTypeNm,chartOfAccName,bankRefNo,remarks,trType);
								}
							}
							if(json.arrPaymentData!=null)
							{
								var lengthPayDetail   	= json.arrPaymentData.length;
								var arrPaymentData  	= json.arrPaymentData;	
								
								for(var i=0;i<lengthPayDetail;i++)
								{
									var serialId		= arrPaymentData[i]['serialId'];
									var dtDate			= arrPaymentData[i]['dtDate'];
									var docNo			= arrPaymentData[i]['docNo'];	
									var amount			= RoundNumber(arrPaymentData[i]['AMOUNT'],2);
									var transacTypeNm	= arrPaymentData[i]['transacTypeName'];
									var chartOfAccName	= arrPaymentData[i]['chartOfAccName'];
									var bankRefNo		= arrPaymentData[i]['bankRefNo'];
									var remarks			= arrPaymentData[i]['remarks'];
									var trType			= arrPaymentData[i]['transacType'];
								
									createGrid('tblPayment',serialId,dtDate,docNo,amount,transacTypeNm,chartOfAccName,bankRefNo,remarks,trType);
								}
							}
						
						},
						error:function(xhr,status){
								
						}		
					});

}
function createGrid(tableName,serialId,dtDate,docNo,amount,transacTypeName,chartOfAccName,bankRefNo,remarks,trType)
{
	var tbl 		= document.getElementById(tableName);
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);
	row.id			= serialId;
	
	var cell 		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML  = "<input type=\"checkbox\" id=\"chkReconciliation\" name=\"chkReconciliation\" class=\"clsChkRec validate[minCheckbox[1]]\">";
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'clsDate';
	cell.innerHTML 	= dtDate;
	
	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsTransacType';
	cell.id			= trType;
	cell.innerHTML 	= (transacTypeName==''?'&nbsp;':transacTypeName);
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'clsDocNo';
	cell.innerHTML 	= docNo;
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<input type=\"textbox\" id=\"txtAmount\" class=\"clsAmount\" style=\"width:100%;text-align:right\" value=\""+amount+"\" disabled=\"disabled\">";
	
	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsAccount';
	cell.innerHTML 	= (chartOfAccName==''?'&nbsp;':chartOfAccName);
	
	var cell 		= row.insertCell(6);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsBnkRefNo';
	cell.innerHTML 	= (bankRefNo==''?'&nbsp;':bankRefNo);
	
	var cell 		= row.insertCell(7);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'clsPresentedDate';
	cell.innerHTML 	= "<input name=\"txtPrDate"+i+"\" type=\"text\" value=\"\" class=\"clsPRDate\" id=\"txtPrDate"+i+"\" style=\"width:100px;\" onKeyPress=\"return ControlableKeyAccess(event);\"  onclick=\"return showCalendar(this.id, '%Y-%m-%d');\" /><input type=\"reset\" value=\"\"  class=\"txtbox\" style=\"visibility:hidden;\"  onclick=\"return showCalendar(this.id, '%Y-%m-%');\" />";
	
	var cell 		= row.insertCell(8);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsRemarks';
	cell.innerHTML 	= (remarks==''?'&nbsp;':remarks);
	
	i++;
}
function clearData()
{
	$("#frmBankReconciliations #tblDeposit tr:gt(1)").remove();
	$("#frmBankReconciliations #tblPayment tr:gt(1)").remove();
	$("#frmBankReconciliations #txtLedgerBalance").val('');
	$("#frmBankReconciliations #txtStatementBalance").val('');
}
function setAmonut()
{
	if($(this).attr('checked'))
	{
		$(this).parent().parent().find('.clsPRDate').addClass('validate[required]');
	}
	else
	{
		$(this).parent().parent().find('.clsPRDate').removeClass('validate[required]');
	}
	
	CaculateTotalValue();
}
function CaculateTotalValue()
{
	var totalDepValue		= 0;
	var totalPayValue		= 0;
	var total				= 0;
	var statementBal		= parseFloat(($('#frmBankReconciliations #txtSavedStatementBal').val()==''?0:$('#frmBankReconciliations #txtSavedStatementBal').val()));
	
	$('#frmBankReconciliations #tblDeposit .clsAmount').each(function(){
		if($(this).parent().parent().find('.clsChkRec').attr('checked'))
		{
			totalDepValue += parseFloat($(this).val());
		}	
	});
	$('#frmBankReconciliations #tblPayment .clsAmount').each(function(){
		if($(this).parent().parent().find('.clsChkRec').attr('checked'))
		{
			totalPayValue += parseFloat($(this).val());
		}	
	});
	
	total = statementBal+(totalDepValue-totalPayValue);
	
	$('#frmBankReconciliations #txtStatementBalance').val(RoundNumber(total,2));
}
function saveData()
{
	if(!IsProcessMonthLocked_date($('#txtDate').val()))
		return;
		
	showWaiting();
	var recNo 			= $('#frmBankReconciliations #txtReconciliationNo').val();
	var recYear 		= $('#frmBankReconciliations #txtReconciliationYear').val();
	var accountId 		= $('#frmBankReconciliations #cboAccounts').val();
	var currencyId 		= $('#frmBankReconciliations #cboCurrency').val();
	var fromDate 		= $('#frmBankReconciliations #txtFromDate').val();
	var toDate 			= $('#frmBankReconciliations #txtToDate').val();
	var statementDate 	= $('#frmBankReconciliations #txtDate').val();
	var ledgerBal 		= $('#frmBankReconciliations #txtLedgerBalance').val();
	var statementBal 	= $('#frmBankReconciliations #txtStatementBalance').val();
	
	if($('#frmBankReconciliations').validationEngine('validate'))
	{
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"recNo":"'+recNo+'",' ;
							arrHeader += '"recYear":"'+recYear+'",' ;
							arrHeader += '"accountId":"'+accountId+'",' ;
							arrHeader += '"currencyId":"'+currencyId+'",' ;
							arrHeader += '"fromDate":"'+fromDate+'",' ;
							arrHeader += '"toDate":"'+toDate+'",' ;
							arrHeader += '"statementDate":"'+statementDate+'",' ;
							arrHeader += '"ledgerBal":"'+ledgerBal+'",' ;
							arrHeader += '"statementBal":"'+statementBal+'"' ;
			arrHeader += "}";
		
		var chkStatus	= false;
		var arrDetails	= "";
		
		$('#frmBankReconciliations input:checkbox:checked').each(function(){
			
			var serialNo 	= $(this).parent().parent().attr('id');	
			var trType	 	= $(this).parent().parent().find('.clsTransacType').attr('id');	
			var prDate	 	= $(this).parent().parent().find('.clsPRDate').val();
			
			arrDetails += "{";
			arrDetails += '"serialNo":"'+ serialNo +'",' ;
			arrDetails += '"trType":"'+ trType +'",' ;
			arrDetails += '"prDate":"'+ prDate +'",' ;
			
			if($(this).parent().parent().find('.clsChkRec').attr('checked'))
			{			
				arrDetails += '"saveFlag":"1"' ;
			}
			else
			{
				arrDetails += '"saveFlag":"0"' ;
			}
			arrDetails += "},";
		});
		
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		
		var arrHeader	= arrHeader;
		var arrDetails	= '['+arrDetails+']';
		data+="&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		var url = basePath+"bankReconciliations-db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'POST',
				data:data,
				async:false,
				success:function(json){
					$('#frmBankReconciliations #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						$('#txtReconciliationNo').val(json.recNo);
						$('#txtReconciliationYear').val(json.recYear);
						$('#frmBankReconciliations #butConfirm').show();
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
						$('#frmBankReconciliations #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
	}
	else
	{
		hideWaiting();
		var t = setTimeout("alertx2()",3000);
	}	
}
function clearAll()
{
	window.location.href = '?q='+menuId;
}
function Confirm()
{
	var url  = "?q="+reportId+"&recNo="+$('#frmBankReconciliations #txtReconciliationNo').val();
	    url += "&recYear="+$('#frmBankReconciliations #txtReconciliationYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rptBankReconciliations.php');
}
function Reject()
{
	var url  = "?q="+reportId+"&recNo="+$('#frmBankReconciliations #txtReconciliationNo').val();
	    url += "&recYear="+$('#frmBankReconciliations #txtReconciliationYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rptBankReconciliations.php');
}
function Cancel()
{
	var url  = "?q="+reportId+"&recNo="+$('#frmBankReconciliations #txtReconciliationNo').val();
	    url += "&recYear="+$('#frmBankReconciliations #txtReconciliationYear').val();
	    url += "&mode=Cancel";
	window.open(url,'rptBankReconciliations.php');
}
function Report()
{
	if($('#frmBankReconciliations #txtReconciliationNo').val()=='')
	{
		$('#frmBankReconciliations #butReport').validationEngine('showPrompt','No reconciliation no to view Report','fail');
		return;	
	}
	
	var url  = "?q="+reportId+"&recNo="+$('#frmBankReconciliations #txtReconciliationNo').val();
	    url += "&recYear="+$('#frmBankReconciliations #txtReconciliationYear').val();
	window.open(url,'rptBankReconciliations.php');
}

function alertx()
{
	$('#frmBankReconciliations #butSave').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmBankReconciliations').validationEngine('hide')	;
}
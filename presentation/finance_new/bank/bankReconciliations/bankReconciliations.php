<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId					= $_SESSION["CompanyID"];
$companyId					= $_SESSION["headCompanyId"];
$userId 					= $_SESSION['userId'];

include_once "class/finance/cls_get_gldetails.php";
include_once "class/finance/cls_common_get.php";
include_once "class/finance/bank/bankReconciliation/cls_reconciliation_get.php";
include_once "class/cls_commonErrorHandeling_get.php";
//include 		"include/javascript.html";

$obj_get_GLCombo			= new Cls_Get_GLDetails($db);
$obj_common_get				= new Cls_Common_Get($db);
$obj_reconciliation_get		= new Cls_Reconciliation_Get($db);
$obj_commonErr				= new cls_commonErrorHandeling_get($db);

$recNo 						= (!isset($_REQUEST['recNo'])?'':$_REQUEST['recNo']);
$recYear 					= (!isset($_REQUEST['recYear'])?'':$_REQUEST['recYear']);
$programCode				= 'P0753';

$header_arr					= $obj_reconciliation_get->loadHeaderData($recNo,$recYear);
$dep_result					= $obj_reconciliation_get->getDetails($recNo,$recYear,'D');
$pay_result					= $obj_reconciliation_get->getDetails($recNo,$recYear,'C');

$intStatus					= $header_arr['STATUS'];
$levels						= $header_arr['APPROVE_LEVELS'];

$permition_arr				= $obj_commonErr->get_permision_withApproval_save($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_save				= $permition_arr['permision'];

$permition_arr				= $obj_commonErr->get_permision_withApproval_cancel($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_cancel			= $permition_arr['permision'];

$permition_arr				= $obj_commonErr->get_permision_withApproval_reject($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_reject			= $permition_arr['permision'];

$permition_arr				= $obj_commonErr->get_permision_withApproval_confirm($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_confirm			= $permition_arr['permision'];

$result_chek				= $obj_reconciliation_get->checkPendingConfirm($recNo,$recYear);

if(mysqli_num_rows($result_chek)>0)
{
	$str 		= "System not allow to keep more than one pending Reconciliation .<br>Please confirm the pending Reconciliation and try again .";
	$maskClass	= "maskShow";
}
else
{
	$maskClass	= "maskHide";
}
?>

<title>Bank Reconciliations</title>

<!--<script type="text/javascript" src="presentation/finance_new/bank/bankReconciliations/bankReconciliations-js.js"></script>-->

<form id="frmBankReconciliations" name="frmBankReconciliations" method="post">
<div align="center">
<div class="trans_layoutXL">
<div class="trans_text">Bank Reconciliations</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
            <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
                <tr class="normalfnt">
                    <td width="18%">Reconciliation No</td>
                    <td width="42%"><input name="txtReconciliationNo" type="text" disabled="disabled" id="txtReconciliationNo" style="width:60px" value="<?php echo $recNo; ?>" />&nbsp;<input name="txtReconciliationYear" type="text" disabled="disabled" id="txtReconciliationYear" style="width:35px" value="<?php echo $recYear; ?>" /></td>
                    <td width="20%">Bank Statement Date</td>
                    <td width="20%"><input name="txtDate" type="text" value="<?php echo($header_arr['STATEMENT_DATE']==''?date("Y-m-d"):$header_arr['STATEMENT_DATE']); ?>" class="validate[required]" id="txtDate" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                </tr>
                <tr class="normalfnt">
                    <td>Accounts <span class="compulsoryRed">*</span></td>
                    <td><select name="cboAccounts" id="cboAccounts" style="width:300px" class="clsAccounts">
					<?php
                    	echo $obj_get_GLCombo->getBankGLCombo($header_arr['CHART_OF_ACCOUNT_ID']);
                    ?>
                    </select></td>
                    <td>From</td>
                    <td><input name="txtFromDate" type="text" value="<?php echo($header_arr['FROM_DATE']==''?date("Y-m-d"):$header_arr['FROM_DATE']); ?>" class="validate[required]" id="txtFromDate" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                </tr>
                <tr class="normalfnt">
                    <td>Currency <span class="compulsoryRed">*</span></td>
                    <td><select name="cboCurrency" id="cboCurrency"  style="width:100px" class="validate[required]" >
                      <?php
                    	echo $obj_common_get->getCurrencyCombo($header_arr['CURRENCY_ID']);
                    ?>
                    </select></td>
                    <td>To</td>
                    <td><input name="txtToDate" type="text" value="<?php echo($header_arr['TO_DATE']==''?date("Y-m-d"):$header_arr['TO_DATE']); ?>" class="validate[required]" id="txtToDate" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /><a class="button green small" id="butSearch">Search</a></td>
                </tr>
                <tr class="normalfnt">
                    <td>Ledger Balance</td>
                    <td><input type="text" name="txtLedgerBalance" id="txtLedgerBalance" style="width:100px;text-align:right" disabled="disabled" value="<?php echo $header_arr['LEDGER_BALANCE']; ?>"/></td>
                    <td>Statement Balance</td>
                    <td><input type="text" name="txtStatementBalance" id="txtStatementBalance" style="width:100px;text-align:right" disabled="disabled" value="<?php echo $header_arr['STATEMENT_BALANCE']; ?>"/><input type="hidden" id="txtSavedStatementBal" value="<?php echo $header_arr['STATEMENT_BALANCE']; ?>" /></td>
                </tr>
                <tr>
                	<td colspan="4"><div style="overflow:scroll;overflow-x:hidden;width:auto;height:200px;">
                    	<table width="100%" border="0" class="bordered" id="tblDeposit">
                        <thead>
                        <tr>
                        	<th colspan="9" >Deposit</th>
                        </tr>
                        <tr class="normalfnt">
                        <th width="3%">&nbsp;</th>
                        <th width="7%">Date</th>
                        <th width="13%">Transaction Type</th>
                        <th width="8%">Document No</th>
                        <th width="7%">Amount</th>
                        <th width="21%">Account</th>
                        <th width="11%">Bank Reference No</th>
                        <th width="11%">Presented Date</th>
                        <th width="19%">Remarks</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
							while($rowDep=mysqli_fetch_array($dep_result))
							{
						?>
                        		<tr class="normalfnt" id="<?php echo $rowDep['SERIAL_ID']; ?>">
                                    <td style="text-align:center"><input type="checkbox" id="chkReconciliation" name="chkReconciliation" class="clsChkRec validate[minCheckbox[1]]" <?php echo($rowDep['SAVE_FLAG']==1?'checked="checked"':''); ?>></td>
                                    <td style="text-align:center" class="clsDate"><?php echo $rowDep['dtDate']; ?></td>
                                    <td style="text-align:left" class="clsTransacType" id="<?php echo $rowDep['TRANSACTION_TYPE']; ?>"><?php echo ($rowDep['transacType']==''?'&nbsp;':$rowDep['transacType']); ?></td>
                                    <td style="text-align:center" class="clsDocNo"><?php echo $rowDep['docNo']; ?></td>
                                    <td style="text-align:center" ><input type="textbox" id="txtAmount" class="clsAmount" style="width:100%;text-align:right" value="<?php echo $rowDep['AMOUNT'];  ?>" disabled="disabled"></td>
                                    <td style="text-align:left" class="clsAccount"><?php echo ($rowDep['chartOfAccName']==''?'&nbsp;':$rowDep['chartOfAccName']); ?></td>
                                    <td style="text-align:left" class="clsBnkRefNo"><?php echo ($rowDep['bankRefNo']==''?'&nbsp;':$rowDep['bankRefNo']); ?></td>
                                    <td style="text-align:center" class="clsPresentedDate"><input name="txtPrDate<?php echo $i++; ?>" type="text" value="<?php echo $rowDep['PRESENTED_DATE']; ?>" class="clsPRDate" id="txtPrDate<?php echo $i++; ?>" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                    <td style="text-align:left" class="clsRemarks"><?php echo ($rowDep['remarks']==''?'&nbsp;':$rowDep['remarks']); ?></td>
                                </tr>
                        <?php
							}
						?>
                        </tbody>
                        </table>
                    </div>
                    </td>
                </tr>
                <tr>
                	<td colspan="4"><div style="overflow:scroll;overflow-x:hidden;width:auto;height:200px;">
                    	<table width="100%" border="0" class="bordered" id="tblPayment">
                        <thead>
                        <tr>
                        	<th colspan="9" >Payment</th>
                        </tr>
                        <tr class="normalfnt">
                        <th width="3%">&nbsp;</th>
                        <th width="7%">Date</th>
                        <th width="13%">Transaction Type</th>
                        <th width="8%">Document No</th>
                        <th width="7%">Amount</th>
                        <th width="21%">Account</th>
                        <th width="11%">Bank Reference No</th>
                        <th width="11%">Presented Date</th>
                        <th width="19%">Remarks</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
							while($rowPay=mysqli_fetch_array($pay_result))
							{
						?>
                        		<tr class="normalfnt" id="<?php echo $rowPay['SERIAL_ID']; ?>">
                                    <td style="text-align:center"><input type="checkbox" id="chkReconciliation" name="chkReconciliation" class="clsChkRec validate[minCheckbox[1]]" <?php echo($rowPay['SAVE_FLAG']==1?'checked="checked"':''); ?>></td>
                                    <td style="text-align:center" class="clsDate"><?php echo $rowPay['dtDate']; ?></td>
                                    <td style="text-align:left" class="clsTransacType" id="<?php echo $rowPay['TRANSACTION_TYPE']; ?>"><?php echo ($rowPay['transacType']==''?'&nbsp;':$rowPay['transacType']); ?></td>
                                    <td style="text-align:center" class="clsDocNo"><?php echo $rowPay['docNo']; ?></td>
                                    <td style="text-align:center" ><input type="textbox" id="txtAmount" class="clsAmount" style="width:100%;text-align:right" value="<?php echo $rowPay['AMOUNT'];  ?>" disabled="disabled"></td>
                                    <td style="text-align:left" class="clsAccount"><?php echo ($rowPay['chartOfAccName']==''?'&nbsp;':$rowPay['chartOfAccName']); ?></td>
                                    <td style="text-align:left" class="clsBnkRefNo"><?php echo ($rowPay['bankRefNo']==''?'&nbsp;':$rowPay['bankRefNo']); ?></td>
                                    <td style="text-align:center" class="clsPresentedDate"><input name="txtPrDate<?php echo $i++; ?>" type="text" value="<?php echo $rowPay['PRESENTED_DATE']; ?>" class="clsPRDate" id="txtPrDate<?php echo $i++; ?>" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                    <td style="text-align:left" class="clsRemarks"><?php echo ($rowPay['remarks']==''?'&nbsp;':$rowPay['remarks']); ?></td>
                                </tr>
                        <?php
							}
						?>
                        </tbody>
                        </table>
                    </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="32" >
            <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
                	<td align="center" class="tableBorder_allRound"><a class="button white medium" id="butNew">New</a><a class="button white medium" id="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a><a class="button white medium" id="butConfirm"  <?php if($permision_confirm!=1){ ?> style="display:none"<?php } ?>>Approve</a><a class="button white medium" id="butReject" <?php if($permision_reject!=1){ ?>style="display:none" <?php } ?>>Reject</a><a class="button white medium" id="butCancel" <?php if($permision_cancel!=1){ ?>  style="display:none"<?php } ?>>Cancel</a><a class="button white medium" id="butReport">Report</a><a href="main.php" class="button white medium" id="butSave">Close</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    
</table>
</div>
</div>
</form>
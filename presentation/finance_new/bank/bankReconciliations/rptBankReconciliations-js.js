// JavaScript Document
var basePath	= "presentation/finance_new/bank/bankReconciliations/";
$(document).ready(function(){
	
	$("#frmRptReconciliation").validationEngine();
	
	$('#frmRptReconciliation #butRptConfirm').die('click').live('click',ConfirmRpt);
	$('#frmRptReconciliation #butRptReject').die('click').live('click',RejectRpt);
	$('#frmRptReconciliation #butRptCancel').die('click').live('click',CancelRpt);
});
function ConfirmRpt()
{
	var val = $.prompt('Are you sure you want to Approve this Reconciliation ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"bankReconciliations-db.php"+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'POST',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptReconciliation #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx1()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptReconciliation #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx1()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function RejectRpt()
{
	var val = $.prompt('Are you sure you want to Reject this Reconciliation ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"bankReconciliations-db.php"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'POST',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptReconciliation #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptReconciliation #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx2()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function CancelRpt()
{
	var val = $.prompt('Are you sure you want to Cancel this Reconciliation ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"bankReconciliations-db.php"+window.location.search+'&requestType=cancel';
					var obj = $.ajax({
						url:url,
						type:'POST',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptReconciliation #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx3()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptReconciliation #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx3()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function alertx1()
{
	$('#frmRptReconciliation #butRptConfirm').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmRptReconciliation #butRptReject').validationEngine('hide')	;
}
function alertx3()
{
	$('#frmRptReconciliation #butRptCancel').validationEngine('hide')	;
}
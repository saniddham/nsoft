<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once  "class/finance/bank/bankReconciliation/cls_reconciliation_get.php";
include_once  "class/cls_commonErrorHandeling_get.php";

$obj_reconciliations_get	= new Cls_Reconciliation_Get($db);
$obj_commonErrHandle		= new cls_commonErrorHandeling_get($db);

$locationId					= $_SESSION["CompanyID"];
$companyId					= $_SESSION["headCompanyId"];
$session_userId 			= $_SESSION["userId"];
$thisFilePath 				= $_SERVER['PHP_SELF'];
$programCode				= 'P0753';
$recNo						= $_REQUEST["recNo"];
$recYear					= $_REQUEST["recYear"];
$mode						= (!isset($_REQUEST['mode'])?'':$_REQUEST['mode']);

$header_array 				= $obj_reconciliations_get->getRptHeaderData($recNo,$recYear);
$detail_result 				= $obj_reconciliations_get->getRptDetailData($recNo,$recYear,'detail');
$detail_Rec_result 			= $obj_reconciliations_get->getRptDetailData($recNo,$recYear,'reconcile');
$approve_details 			= $obj_reconciliations_get->getRptApproveDetails($recNo,$recYear);

$intStatus					= $header_array['STATUS'];
$levels						= $header_array['APPROVE_LEVELS'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_cancel($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_cancel			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_reject($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_reject			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_confirm			= $permition_arr['permision'];

?>
<head>
<title>Bank Reconciliation Report</title>

<script type="text/javascript" src="presentation/finance_new/bank/bankReconciliations/rptBankReconciliations-js.js"></script>

<style>
#apDiv1 {
	position:absolute;
	left:301px;
	top:175px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 16px;
	font-weight:bold;
	font-family: "Arial Black", Gadget, sans-serif;
	color: #36F;
}
</style>
</head>

<body>
<form id="frmRptReconciliation" name="frmRptReconciliation" method="post">
<table width="1100" align="center">
	<tr>
    	<td colspan="2"><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align:center">&nbsp;</td>
    </tr>
    <tr>
    	<td width="508" style="text-align:center"><strong>BANK RECONCILIATIONS REPORT</strong></td>
    </tr>
    <tr>
        <td align="center" bgcolor="#EAEAFF">
        <?php 
		if(($mode=='Confirm')&&($permision_confirm==1))
		{
		?>
        	<a class="button white medium" id="butRptConfirm">Approve</a>
        <?php
        }
        if(($mode=='Confirm')&&($permision_reject==1))
		{
		?>
        	<a class="button white medium" id="butRptReject">Reject</a>
        <?php
        }
        if(($mode=='Cancel')&&($permision_cancel==1))
		{
		?>
        	<a class="button white medium" id="butRptCancel">Cancel</a>
        <?php
        }
        ?>
        </td>
    </tr>
    <tr>
		<?php
        if($header_array["STATUS"]==1)
        {
        ?>	
            <td class="APPROVE" style="color:#6C6">CONFIRMED</td>
        <?PHP 
        }
        else if($header_array["STATUS"]==0)
		{
		?>
        	<td width="95" class="APPROVE" style="color:#FF8040">REJECTED</td>
		<?php 
		}
        else if($header_array["STATUS"]==-2)	
		{
		?>
        	<td width="156" class="APPROVE" style="color:#FF0000">CANCELLED</td>
		<?php
        }
        else 
		{  
		?> 
        	<td width="121" class="APPROVE">PENDING</td>  
		<?php
        }
        ?>
    </tr>
    <tr>
    	<td>
            <table width="100%" border="0">
                <tr class="normalfnt">
                    <td width="20%">Reconciliation No</td>
                    <td width="1%">:</td>
                    <td width="36%"><?php echo $recNo.'/'.$recYear?></td>
                    <td width="19%">Statement Date</td>
                    <td width="1%">:</td>
                    <td width="23%"><?php echo $header_array["STATEMENT_DATE"]?></td>
                </tr>
                <tr class="normalfnt">
                    <td>Account</td>
                    <td>:</td>
                    <td><?php echo $header_array["CHART_OF_ACCOUNT_NAME"]?></td>
                    <td>Currency</td>
                    <td>:</td>
                    <td><?php echo $header_array["currency"]?></td>
                </tr>
                <tr class="normalfnt">
                    <td>From Date</td>
                    <td>:</td>
                    <td><?php echo $header_array["FROM_DATE"]?></td>
                    <td>To Date</td>
                    <td>:</td>
                    <td><?php echo $header_array["TO_DATE"]?></td>
                </tr>
                <tr class="normalfnt">
                    <td>Ledger Balance</td>
                    <td>:</td>
                    <td><?php echo number_format($header_array["LEDGER_BALANCE"],2)?></td>
                    <td>Statement Balance</td>
                    <td>:</td>
                    <td><?php echo number_format($header_array["STATEMENT_BALANCE"],2)?></td>
                </tr>
                <tr class="normalfnt">
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>Difference</td>
                  <td>:</td>
                  <td><?php echo number_format(($header_array["LEDGER_BALANCE"]-$header_array["STATEMENT_BALANCE"]),2); ?></td>
                </tr>        
            </table>
        </td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" class="bordered" id="tblDeposit">
            <thead> 
                <tr class="normalfnt">
                    <th width="7%">Date</th>
                    <th width="13%">Transaction Type</th>
                    <th width="8%">Document No</th>
                    <th width="9%">Deposit</th>
                    <th width="9%">Payment</th>
                    <th width="21%">Account</th>
                    <th width="11%">Bank Reference No</th>
                    <th width="11%">Presented Date</th>
                    <th width="19%">Remarks</th>
                </tr>
            </thead>
            <tbody>
            <?php
			$totPayment = 0;
			$totDeposit = 0;
			while($row=mysqli_fetch_array($detail_result))
			{
				if($row['TRANSACTION_TYPE']=='D')
					$totDeposit	+= $row['AMOUNT'];
				else
					$totPayment	+= $row['AMOUNT'];
			?>
            	<tr class="normalfnt">
                	<td style="text-align:center"><?php echo $row['dtDate'];?></td>
                    <td style="text-align:left"><?php echo ($row['transacType']==''?'&nbsp;':$row['transacType']);?></td>
                    <td style="text-align:center"><?php echo $row['docNo'];?></td>
                    <td style="text-align:right"><?php echo ($row['TRANSACTION_TYPE']=='D'?number_format($row['AMOUNT'],2):'&nbsp;');?></td>
                    <td style="text-align:right"><?php echo ($row['TRANSACTION_TYPE']=='C'?number_format($row['AMOUNT'],2):'&nbsp;');?></td>
                    <td style="text-align:left"><?php echo ($row['chartOfAccName']==''?'&nbsp;':$row['chartOfAccName']);?></td>
                    <td style="text-align:left"><?php echo ($row['bankRefNo']==''?'&nbsp;':$row['bankRefNo']);?></td>
                    <td style="text-align:center"><?php echo ($row['PRESENTED_DATE']==''?'&nbsp;':$row['PRESENTED_DATE']);?></td>
                    <td style="text-align:left"><?php echo ($row['remarks']==''?'&nbsp;':$row['remarks']);?></td>
                </tr>
			<?php
			}
			?>
                <tr class="normalfnt">
                    <td colspan="3" style="text-align:left"><b>Total :</b></td>
                    <td style="text-align:right"><b><?php echo number_format($totDeposit,2);?></b></td>
                    <td style="text-align:right"><b><?php echo number_format($totPayment,2);?></b></td>
                    <td colspan="4" style="text-align:left">&nbsp;</td>
                </tr>
                <tr class="normalfnt">
                  <td colspan="3" style="text-align:left"><b>Difference :</b></td>
                  <td style="text-align:right"><b><?php echo number_format(($totDeposit-$totPayment),2);?></b></td>
                  <td style="text-align:right">&nbsp;</td>
                  <td colspan="4" style="text-align:left">&nbsp;</td>
                </tr>
            </tbody>
            </table>
        </td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <?php
	if($header_array["STATUS"]!=1)
	{
	?>
        <tr>
            <td><?php include 'presentation/finance_new/bank/bankReconciliations/rptReconciled.php'?></td>
        </tr>
    <?php
	}
	?>
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr>
    	<td>
        	<table width="100%" border="0">
            	<tr>
                	<td class="normalfnt"><b>Entered By</b> - <?php echo $header_array['strUserName'];?></td>
                </tr>
                <?php
				$finalPrintedApproval = 0;
				while($rowAP = mysqli_fetch_array($approve_details))
				{
					switch($rowAP['APPROVE_LEVEL_NO'])
					{
						case -2:
						$desc	= "Cancelled By ";
						$col 	= "#FF0000";
						break;
						
						case -1:
						$desc	= "Revised By ";
						$col 	= "#FF8040";
						break;
						
						case 0:
						$desc	= "Rejected By ";
						$col 	= "#FF8040";
						break;
						
						case 1:
						$desc	= "1st Approved By ";
						$col 	= "#000000";
						break;
						
						case 2:
						$desc	= "2nd Approved By ";
						$col 	= "#000000";
						break;
						
						case 3:
						$desc	= "3rd Approved By ";
						$col 	= "#000000";
						break;
						
						default:
						$desc	= $rowAP['APPROVE_LEVEL_NO']."th Approved By ";
						$col 	= "#000000";
						break;
					}
					$desc2		= $rowAP['UserName']."(".$rowAP['APPROVED_DATE'].")";
					if($rowAP['UserName']=='')
						$desc2	= '---------------------------------';
					?>
                        <tr>
                        	<td class="normalfnt" style="color:<?php echo $col; ?>"><strong><?php echo $desc; ?>- </strong><?php echo $desc2;?></td>
                        </tr>
                <?php
					$finalPrintedApproval	= $rowAP['APPROVE_LEVEL_NO'];
				}
				
				$intStatus	= $header_array["STATUS"];
				$appLevels	= $header_array['APPROVE_LEVELS'];
				
				if(($finalPrintedApproval<$appLevels) && ($finalPrintedApproval>=0) && ($intStatus>0))
				{
					for($j=$finalPrintedApproval+1;$j<=$appLevels;$j++)
					{ 
						switch($j)
						{
							case 1:
							$desc	= "1st Approved By ";
							break;
							
							case 2:
							$desc	= "2nd Approved By ";
							break;
							
							case 3:
							$desc	= "3rd Approved By ";
							break;
							
							default:
							$desc	= $j."th Approved By ";
							break;
						}
						
						$desc2		= '---------------------------------';	
				?>
                        <tr>
                        <td class="normalfnt"><strong><?php echo $desc;?>- </strong><?php echo $desc2;?></td>
                        </tr>
				<?php
					}
				}
				?>
            </table>
        </td>
    </tr>
    <tr>
        <td height="40" ></td>
    </tr>
    <tr>
    	<td align="center" class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></td>
    </tr> 
</table>
</form>
</body>
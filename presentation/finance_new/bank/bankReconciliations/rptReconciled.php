<table width="100%" border="0" class="bordered">
    <thead>
        <tr>
        	<th colspan="9" >Reconciled</th>
        </tr>
        <tr class="normalfnt">
            <th width="7%">Date</th>
            <th width="13%">Transaction Type</th>
            <th width="8%">Document No</th>
            <th width="9%">Deposit</th>
            <th width="9%">Payment</th>
            <th width="21%">Account</th>
            <th width="11%">Bank Reference No</th>
            <th width="11%">Presented Date</th>
            <th width="19%">Remarks</th>
        </tr>
        <?php
			$totRDeposit = 0;
			$totRPayment = 0;
			while($row=mysqli_fetch_array($detail_Rec_result))
			{
				if($row['TRANSACTION_TYPE']=='D')
					$totRDeposit	+= $row['AMOUNT'];
				else
					$totRPayment	+= $row['AMOUNT'];
			?>
            	<tr class="normalfnt">
                	<td style="text-align:center"><?php echo $row['dtDate'];?></td>
                    <td style="text-align:left"><?php echo ($row['transacType']==''?'&nbsp;':$row['transacType']);?></td>
                    <td style="text-align:center"><?php echo $row['docNo'];?></td>
                    <td style="text-align:right"><?php echo ($row['TRANSACTION_TYPE']=='D'?number_format($row['AMOUNT'],2):'&nbsp;');?></td>
                    <td style="text-align:right"><?php echo ($row['TRANSACTION_TYPE']=='C'?number_format($row['AMOUNT'],2):'&nbsp;');?></td>
                    <td style="text-align:left"><?php echo ($row['chartOfAccName']==''?'&nbsp;':$row['chartOfAccName']);?></td>
                    <td style="text-align:left"><?php echo ($row['bankRefNo']==''?'&nbsp;':$row['bankRefNo']);?></td>
                    <td style="text-align:center"><?php echo ($row['PRESENTED_DATE']==''?'&nbsp;':$row['PRESENTED_DATE']);?></td>
                    <td style="text-align:left"><?php echo ($row['remarks']==''?'&nbsp;':$row['remarks']);?></td>
                </tr>
			<?php
			}
			?>
            <tr class="normalfnt">
                    <td colspan="3" style="text-align:left"><b>Total :</b></td>
                    <td style="text-align:right"><b><?php echo number_format($totRDeposit,2);?></b></td>
                    <td style="text-align:right"><b><?php echo number_format($totRPayment,2);?></b></td>
                    <td colspan="4" style="text-align:left">&nbsp;</td>
                </tr>
    </thead>
</table>
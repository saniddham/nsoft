<?php 
 session_start();
$backwardseperator 			= "../../../../";
$userId 					= $_SESSION['userId'];
$locationId	  				= $_SESSION["CompanyID"];
$companyId					= $_SESSION["headCompanyId"];

$requestType 				= $_REQUEST['requestType'];
$programName				= 'Bank Reconciliations Approval';
$programCode				= 'P0753';

include_once "../../../../dataAccess/Connector.php";
include_once  "../../../../class/finance/bank/bankReconciliation/cls_reconciliation_set.php";
include_once  "../../../../class/finance/bank/bankReconciliation/cls_reconciliation_get.php";

$obj_reconciliation_set		= new Cls_Reconciliation_Set($db);
$obj_reconciliation_get		= new Cls_Reconciliation_Get($db);

if($requestType=='loadData')
{
	$accountId 		= $_REQUEST['accountId'];
	$currencyId 	= $_REQUEST['currencyId'];
	$toDate 		= $_REQUEST['toDate'];
	
	$response['arrDepositData']	= $obj_reconciliation_get->loadData($accountId,$currencyId,$toDate,'D');
	$response['arrPaymentData']	= $obj_reconciliation_get->loadData($accountId,$currencyId,$toDate,'C');
	$response['ledgerBalance']	= $obj_reconciliation_get->loadLedgerBalance($accountId,$currencyId,$toDate);
	$response['statementBal']	= $obj_reconciliation_get->loadStatementBalance($accountId,$currencyId,$toDate);
	
	echo json_encode($response);
}
else if($requestType=='saveData')
{
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	$arrDetails 		= json_decode($_REQUEST['arrDetails'],true);
	
	echo $obj_reconciliation_set->save($arrHeader,$arrDetails,$programCode,$programName);
}
else if($requestType=='approve')
{
	$recNo				= $_REQUEST["recNo"];
	$recYear			= $_REQUEST["recYear"];
	
	$response			= $obj_reconciliation_get->validateBeforeApprove($recNo,$recYear,$programCode);
	
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_reconciliation_set->approve($recNo,$recYear);
}
else if($requestType=='reject')
{
	$recNo				= $_REQUEST["recNo"];
	$recYear			= $_REQUEST["recYear"];
	
	$response			= $obj_reconciliation_get->validateBeforeReject($recNo,$recYear,$programCode);
	 
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_reconciliation_set->reject($recNo,$recYear);
}
else if($requestType=='cancel')
{
	$recNo				= $_REQUEST["recNo"];
	$recYear			= $_REQUEST["recYear"];
	
	$response			= $obj_reconciliation_get->validateBeforeCancel($recNo,$recYear,$programCode);
	 
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_reconciliation_set->cancel($recNo,$recYear);
}

?>
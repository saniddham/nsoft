<?php
session_start();
$backwardseperator = "../../../../";
$companyId 			= $_SESSION['headCompanyId'];

//BEGIN - INCLUDE FILES {
include  	"{$backwardseperator}dataAccess/Connector.php";
include_once ("../../../../class/finance/cash_flow_forecast/cls_cash_flow_forecast_get.php");
include_once ("../../../../class/finance/cash_flow_forecast/cls_cash_flow_forecast_set.php");
//END	}

//BEGIN - CREATE OBJECTS {
$objCashFlowForecastGet		= new cls_cash_flow_forecast_get($db);
$objCashFlowForecastSet		= new cls_cash_flow_forecast_set($db);
//END	}

//BEGIN - ASSIGN PARAMETERS {
$requestType	= $_REQUEST["RequestType"];
//END	}

if($requestType=="URLTransfer")
{
	$fromDate		= $_REQUEST["FromDate"];
	$toDate			= $_REQUEST["ToDate"];
	$tblType		= $_REQUEST["Type"]=='CUSTOMER'?'finance_forecast_transaction_customer':'finance_forecast_transaction_supplier';
	$array			= json_decode($_REQUEST["Array"],true);
	
	$booSavedStatus	= true;
	$db->begin();
	
	foreach($array as $array)
	{				
		$response = $objCashFlowForecastSet->UpdateFromAmount($fromDate,$array["Id"],$array["Amount"],$companyId,$tblType);
		
		$count = $objCashFlowForecastGet->IsRowAvailable($toDate,$array["Id"],$companyId,$tblType);
		
		if($count>0)
		{
			if($response["type"])
				$response = $objCashFlowForecastSet->UpdateCustomerForcast($toDate,$array["Id"],$array["Amount"],$companyId,$tblType);
		}
		else
		{
			if($response["type"])
				$response = $objCashFlowForecastSet->InsertCustomerForcast($toDate,$array["Id"],$array["Amount"],$companyId,$tblType);			
		}	
	}	
	if($response["type"])
	{
		$db->commit();
		$response1['type']	= 'pass';
		$response1['Msg']	= 'Saved';
	}
	else
	{
		$db->rollback();
		$response1['type']	= 'fail';
		$response1['Msg']	= $response['msg'];
		$response1['Sql']	= $response['sql'];
	}
	echo json_encode($response1);
}
?>
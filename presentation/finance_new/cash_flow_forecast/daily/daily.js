$(document).ready(function(e) {
    $('.cls_td_amount').live('dblclick',OpenAmountTransferPopup);
	$('.cls_but_transfer').live('click',Transfer);
	$('#tblPopupCustomer thead .cls_th_check').live('click',CheckAll);
	$('#tblPopupCustomer tbody .cls_check').live('click',ValidateCheckbox);
	$('#tblPopupCustomer tbody .cls_amount').live('keyup',ValidateAmount);
});

function OpenAmountTransferPopup()
{
	if(parseFloat($(this).html()) <= 0)
		return;
		
	var transFromDate	= $(this).attr('id');
	var type			= $(this).closest('tr').find('td:first').attr('id');
	$('body').append("<div style=\"width:800px; position: absolute;display:none;z-index:100\" id=\"popupContact1\"></div>");
	$('body').append("<div style=\"height: 0px; opacity: 0.7; display: none;\" id=\"backgroundPopup\"></div>");
	popupWindow3('1');
	$('#popupContact1').html("");	
	$('#popupContact1').load('transfer_amount_popup.php?TransFromDate='+transFromDate+'&Type='+type,function(){
		$('#popupContact1 .butClose1').click(disablePopup);
	});
}

function Transfer()
{	
	if(!ValidateTransfer())
		return;
		
	var array 		= "[";
	$('#tblPopupCustomer tbody .cls_check').each(function() {
		if($(this).is(':checked'))
		{
			array += "{";
			array += '"Id":"'+$(this).parent().parent().find('.cls_td_customer').attr('id')+'",';
			array += '"Amount":"'+$(this).parent().parent().find('.cls_td_amount').children().val()+'"';
			array +=  '},';			
		}
    });
		array = array.substr(0,array.length-1);
		array += "]";
	
	var url 	= "daily_db.php?RequestType=URLTransfer";
	var data 	= "FromDate="+$('#frmAmountTransferPopup #txtFromDate_popup').val();
		data   += "&ToDate="+$('#frmAmountTransferPopup #txtToDate_popup').val();
		data   += "&Type="+$('#frmAmountTransferPopup .cls_td_reportHeader').attr('id');
		data   += "&Array="+array;
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		if(json.type=='pass')
		{
			document.frmDailyForecasting.submit();	
		}
	}
	});	
}

function CheckAll()
{
	if($(this).is(':checked'))
	{
		$('#tblPopupCustomer tbody .cls_check').attr('checked',true);
		$('#tblPopupCustomer tbody .cls_amount').removeAttr('disabled');
			
	}
	else
	{
		$('#tblPopupCustomer tbody .cls_check').attr('checked',false);
		$('#tblPopupCustomer tbody .cls_amount').attr('disabled','disabled');		
	}
	CalculateTransferAmount();		
}

function CalculateTransferAmount()
{
	var total = 0;
	$('#tblPopupCustomer tbody .cls_check').each(function() {
        if($(this).is(':checked'))
		{
			total += parseFloat($(this).parent().parent().find('.cls_td_amount').children().val());
		}
    });
	
	$('#frmAmountTransferPopup #txtTransferToAmount').val(total);
}

function ValidateCheckbox()
{
	if($(this).is(':checked'))
	{
		$(this).parent().parent().find('.cls_td_amount').children().removeAttr('disabled');
	}
	else
	{
		$(this).parent().parent().find('.cls_td_amount').children().attr('disabled','disabled');
	}
	CalculateTransferAmount();
}

function ValidateAmount()
{
	if($(this).val()=="" || isNaN($(this).val())){
		$(this).val(parseFloat($(this).attr('id')));
		$(this).select();
	}
	
	if(parseFloat($(this).attr('id')) < parseFloat($(this).val()))
	{
		$(this).val(parseFloat($(this).attr('id')));
	}
	CalculateTransferAmount();
}

function ValidateTransfer()
{
	if(!$('#frmAmountTransferPopup').validationEngine('validate'))
		return false;
		
	return true;
}
<?php
session_start();
//BEGIN - SET PARAMETERS	{
$backwardseperator = "../../../../";
$companyId 			= $_SESSION['headCompanyId'];
$location 			= $_SESSION['CompanyID'];
$intUser 			= $_SESSION["userId"];
//END 	- SET PARAMETERS	}

//BEGIN - INCLUDE FILES {
include  	"{$backwardseperator}dataAccess/Connector.php";
include_once("../../../../class/masterData/companies/cls_companies_get.php");
//END 	- INCLUDE FILES }

$obj_companies_get	= new cls_companies_get($db);

$target_currencyId	= 1;
$comp_array		= $obj_companies_get->GetCompanyReportHeader($location);
if(isset($_REQUEST["txtFromDate"])){
	$fromDate	= $_REQUEST["txtFromDate"];
	//die('T');
}else{
	$fromDate	= date('Y-m-01');
	//die('F');
}
	
if(isset($_REQUEST["txtToDate"]))
	$toDate	= $_REQUEST["txtToDate"];
else
	$toDate	=	date('Y-m-'.date('t',strtotime(date('Y-m-d'))));
//BEGIN - 
$startDate 	= strtotime($fromDate);
$endDate   	= strtotime($toDate);

$currentDate = $endDate;
$i	= 0;
$array	= array();
while ($currentDate >= $startDate) {
	$array[$i][0]	= date('Y M d',$currentDate);
	$array[$i][1]	= date('Y-m-d',$currentDate);
	$array[$i][2]	= date('Y',$currentDate);
	$array[$i][3]	= date('m',$currentDate);
	$array[$i][4]	= date('Y-m-d',$currentDate)==date('Y-m-d')? '#64F47E':'';
    $currentDate = strtotime(date('Y/m/d',$currentDate).' -1 day');
	$i++;
	
}
krsort($array);

//END	-
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Daily Cash Flow Forecast</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">


<script src="../../../../libraries/jquery/jquery.js" type="application/javascript"></script>
<script src="../../../../libraries/jquery/jquery-ui.js" type="application/javascript"></script>
<script src="../../../../libraries/javascript/script.js" type="application/javascript"></script>
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
<script type="text/javascript" src="daily.js"></script>
<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script> 
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script> 
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script> 
<script type="application/javascript">
function Reload()
{
	document.frmDailyForecasting.submit();	
}
</script>
</head>
<body>
<form id="frmDailyForecasting" name="frmDailyForecasting" method="post" action="daily.php">
  <div align="center">  
   <div style="background-color:#FFF" class="reportHeader_16"><strong>Daily Cash Flow Forecast - <?php echo $comp_array["COMPANY_NAME"]?></strong></div>
   <div style="background-color:#FFF" class="reportHeader_16" ><strong>From <?php echo $fromDate?> to <?php echo $toDate?></strong></div>
<table width="1100" border="0" align="center" bgcolor="#FFFFFF">
 <tr>
   <td><table width="500" border="0" class="normalfnt">
     <tr>
       <td width="15%">Date From</td>
       <td width="2%">:</td>
       <td width="26%"><input name="txtFromDate" type="text" value="<?php echo $fromDate?>" class="validate[required]" id="txtFromDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
       <td width="10%">Date To</td>
       <td width="2%">:</td>
       <td width="33%"><input name="txtToDate" type="text" value="<?php echo $toDate?>" class="validate[required]" id="txtToDate" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
       <td width="12%"><a class="button green small" onclick="Reload();">Search</a></td>
     </tr>
   </table></td>
 </tr>
 <tr>
  <td><table  border="0" cellpadding="0" cellspacing="0" class="bordered" width="100%">
    <thead>
      <tr valign="bottom">
        <th width="21%">&nbsp;</th>
        <?php 
		foreach ($array as $key => $val)
		{
		?>
        <th nowrap="nowrap"><?php echo $val[0];?></th>
        <?php 
		}
		?>
        </tr>
    </thead>
     <tbody>
      <tr>
        <td nowrap="nowrap" title="Customer Name" ><u><b>Add - Receipts</b></u></td>
         <?php 
		foreach ($array as $key => $val)
		{
		?>
       <td nowrap="nowrap" title="<?php echo $val[0];?>" bgcolor="<?php echo $val[4];?>"><?php echo $row["PAYMENT_TERM"]?></td>
        <?php 
		}
		?>        
       </tr>
      <tr>
        <td nowrap="nowrap" title="Customer Name" id="CUSTOMER">&nbsp;&nbsp;&nbsp;&nbsp;Monthly collection from customers</td>
         <?php 
		foreach ($array as $key => $val)
		{
			$array_total[$val[1]][0] = round(GetCustomerAmount($val[1]),2);
		?>
        <td nowrap="nowrap" title="<?php echo "Monthly collection from customers -> ".$val[0];?>" style="text-align:right" bgcolor="<?php echo $val[4];?>" class="cls_td_amount" id="<?php echo $val[1]?>"><?php echo number_format(GetCustomerAmount($val[1]))?></td>
        <?php 
		}
		?>       
      </tr>
      
      <tr>
        <td nowrap="nowrap"><u><b>Deduct - Payment</b></u></td>
         <?php 
		foreach ($array as $key => $val)
		{
		?>
       <td nowrap="nowrap" title="<?php echo $val[0];?>" bgcolor="<?php echo $val[4];?>"><?php echo $row["PAYMENT_TERM"]?></td>
        <?php 
		}
		?>        
       </tr>
      <tr>
        <td nowrap="nowrap" id="SUPPLIER">&nbsp;&nbsp;&nbsp;&nbsp;Monthly payment to supplier</td>
         <?php 
		foreach ($array as $key => $val)
		{
			$supplierAmount	= round(GetSupplierAmount($val[1]));
			$array_total[$val[1]][1] = $supplierAmount;
		?>
        <td nowrap="nowrap" title="<?php echo "Monthly payment to supplier -> ".$val[0];?>" style="text-align:right" bgcolor="<?php echo $val[4];?>" class="cls_td_amount" id="<?php echo $val[1]?>"><?php echo $supplierAmount=='0'?0:'('.number_format($supplierAmount).')'?></td>
        <?php 
		}
		?>       
      </tr>
      
      <tr>
        <td nowrap="nowrap">&nbsp;</td>
         <?php 
		foreach ($array as $key => $val)
		{
		?>
       <td nowrap="nowrap" title="<?php echo $val[0];?>" bgcolor="<?php echo $val[4];?>">&nbsp;</td>
        <?php 
		}
		?>        
       </tr>
      <tr>
        <td nowrap="nowrap" title="Customer Name" ><b>Total</b></td>
         <?php 
		foreach ($array as $key => $val)
		{
			$total = $array_total[$val[1]][0] - $array_total[$val[1]][1];
		?>
        <td nowrap="nowrap" title="<?php echo "Total -> ".$val[0];?>" style="text-align:right" bgcolor="<?php echo $val[4];?>" class="cls_td_amount" id="<?php echo $val[1]?>"><b><?php echo ($total<0?'('.number_format(abs($total)).')':number_format(abs($total)))?></b></td>
        <?php 
		}
		?>       
      </tr>
     </tbody>
  </table></td>
</tr>
            <tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>
</form>
</body>
<script type="text/javascript">
$(document).ready(function()
{
	$('.bordered tbody tr').click(function(){
		if($(this).attr('style')!='background-color:#82FF82')
		{
			$('.bordered tbody tr').attr('style','background-color:#FFFFFF');  
			   $(this).attr('style','background-color:#82FF82');     
		}                                             
	})
});
</script>
</html>
<?php
function GetCustomerAmount($date)
{
	global $db;
	global $companyId;
	global $target_currencyId;

	$sql = "SELECT 
				SUM(FT.BALANCE_AMOUNT) AS AMOUNT		
			FROM finance_forecast_transaction_customer FT
			WHERE FT.DATE = '$date'
				AND FT.COMPANY_ID = $companyId ";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}

function GetSupplierAmount($date)
{
	global $db;
	global $companyId;
	global $target_currencyId;

	$sql = "SELECT 
				SUM(FT.BALANCE_AMOUNT) AS AMOUNT		
			FROM finance_forecast_transaction_supplier FT
			WHERE FT.DATE = '$date'
				AND FT.COMPANY_ID = $companyId ";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}
?>
<?php
session_start();
//BEGIN - SET PARAMETERS	{
$backwardseperator = "../../../../";
$companyId 			= $_SESSION['headCompanyId'];
$location 			= $_SESSION['CompanyID'];
$intUser 			= $_SESSION["userId"];
$transFromDate		= $_REQUEST["TransFromDate"];
$type				= $_REQUEST["Type"];
//END 	- SET PARAMETERS	}

//BEGIN - INCLUDE FILES {
include  	"{$backwardseperator}dataAccess/Connector.php";
//END 	- INCLUDE FILES }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Daily Cash Flow Forecast</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

</head>
<body>
<form id="frmAmountTransferPopup" name="frmAmountTransferPopup" method="post" action="daily.php">
  <div align="center">
    <table width="500" border="0" align="center" bgcolor="#FFFFFF">
      <tr>
        <td class="reportHeader cls_td_reportHeader" align="center" id="<?php echo $type?>"><?php echo ($type=="CUSTOMER" ? 'Customer Details':'Supplier Details')?></td>
      </tr>
      <tr>
        <td align="center"><table width="100%" border="0" class="normalfnt">
          <tr>
            <td>Transfer From Date</td>
            <td><input name="txtFromDate_popup" type="text" value="<?php echo $transFromDate?>" class="validate[required]" id="txtFromDate_popup" style="width:100px;" disabled="disabled" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
          <tr>
            <td colspan="2"><table width="100%" border="0" class="bordered" id="tblPopupCustomer">
              <thead>
                <tr>
                  <th width="6%"><input type="checkbox" class="cls_th_check"/></th>
                  <th width="68%" height="17"><?php echo ($type=="CUSTOMER" ? 'Customer':'Supplier')?></th>
                  <th width="26%">Amount</th>
                  </tr>
                </thead>
              <tbody>
                <?php 
			  $result = GetDetails($transFromDate,$type);
			  while($row = mysqli_fetch_array($result))
			  {
			  ?>
                <tr>
                  <td style="text-align:center" class="cls_td_check"><input type="checkbox" class="cls_check"/></td>
                  <td style="text-align:left" class="cls_td_customer" id="<?php echo $row["CUSTOMER_ID"]?>"><?php echo $row["CUSTOMER_NAME"]?></td>
                  <td style="text-align:right" class="cls_td_amount"><input type="text" id="<?php echo $row["BALANCE_AMOUNT"]?>" style="text-align:right;width:100%" value="<?php echo $row["BALANCE_AMOUNT"]?>" class="cls_amount" disabled="disabled"/></td>
                  </tr>
                <?php 
			  }
			  ?>
                </tbody>
              </table></td>
          </tr>
          <tr>
            <td width="74%">Transfer To Date</td>
            <td width="26%"><input name="txtToDate_popup" type="text" value="" class="validate[required]" id="txtToDate_popup" style="width:100px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;width:1px"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
          <tr>
            <td>Transfer To Amount</td>
            <td><input type="text" id="txtTransferToAmount" name="txtTransferToAmount" style="text-align:right;width:100px" value="0"/></td>
          </tr>
        </table></td>
      </tr>
      <tr>
      <tr height="40">
        <td align="center" class="normalfntMid"><a class="button green small cls_but_transfer">Transfer</a><a class="button green small butClose1">Close</a></td>
      </tr>
    </table>
  </div>
</form>
</body>
</html>
<?php 
function GetDetails($transFromDate,$type)
{
	global $db;
	
	if($type=='CUSTOMER')
	{
		$sql = "SELECT
				  FFTC.CATEGORY_ID 		AS CUSTOMER_ID,
				  CU.strName 			AS CUSTOMER_NAME,
				  SUM(BALANCE_AMOUNT) 	AS BALANCE_AMOUNT
				FROM finance_forecast_transaction_customer FFTC
				  INNER JOIN mst_customer CU
					ON CU.intId = FFTC.CATEGORY_ID
				WHERE FFTC.DATE = '$transFromDate'
				GROUP BY FFTC.CATEGORY_ID
				HAVING BALANCE_AMOUNT >0
				ORDER BY CU.strName";
	}
	else
	{
		$sql = "SELECT
				  FFTS.CATEGORY_ID 		AS CUSTOMER_ID,
				  SU.strName 			AS CUSTOMER_NAME,
				  SUM(FFTS.BALANCE_AMOUNT) 	AS BALANCE_AMOUNT
				FROM finance_forecast_transaction_supplier FFTS
				  INNER JOIN mst_supplier SU
					ON SU.intId = FFTS.CATEGORY_ID
				WHERE FFTS.DATE = '$transFromDate'
				GROUP BY FFTS.CATEGORY_ID
				HAVING BALANCE_AMOUNT >0
				ORDER BY SU.strName";
	}
	return $db->RunQuery($sql);
}
?>
<?php
session_start();
//BEGIN - SET PARAMETERS	{
$backwardseperator = "../../../../";
$companyId 			= $_SESSION['headCompanyId'];
$location 			= $_SESSION['CompanyID'];
$intUser 			= $_SESSION["userId"];
//END 	- SET PARAMETERS	}

//BEGIN - INCLUDE FILES {
include  	"{$backwardseperator}dataAccess/Connector.php";
//END 	- INCLUDE FILES }

//BEGIN - 
$startDate = strtotime('2013-04-01');
$endDate   = strtotime('2014-03-31');

$currentDate = $endDate;
$i	= 0;
$array	= array();
while ($currentDate >= $startDate) {
	$array[$i][0]	= date('Y | M',$currentDate);
	$array[$i][1]	= date('Y-m',$currentDate);
	$array[$i][2]	= date('Y',$currentDate);
	$array[$i][3]	= date('m',$currentDate);
    $currentDate = strtotime( date('Y/m/01/',$currentDate).' -1 day');
	$i++;
	
}
krsort($array);

//END	-
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Monthly Cash Flow Forecast</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<script type="application/javascript">
function Reload()
{
	document.frmCustomerInvoiceAgingReport.submit();	
}
</script>
</head>
<style type="text/css">
#progress {
 width: 100px;   
 border: 1px solid black;
 position: relative;
 padding: 1px;
}

#percent {
 position: absolute;   
 left: 50%;
}

#bar {
 height: 15px;
 background-color: #0C0;
}
</style>
<body>
<form id="frmCustomerInvoiceAgingReport" name="frmCustomerInvoiceAgingReport" method="post" action="rptCustomer_invoice_aging.php">
  <div align="center">
  <div style="background-color:#FFF" class="reportHeader" ><strong>Expected Monthly Cash Flow - (From August 2013 to March 2014)</strong></div>
   <div style="background-color:#FFF" class="reportHeader"><strong>Monthly Cash Flow Forecast - Screenline group</strong></div>
<table width="1100" border="0" align="center" bgcolor="#FFFFFF">
 <tr>
   <td>&nbsp;</td>
 </tr>
 <tr>
  <td><table  border="0" cellpadding="0" cellspacing="0" class="bordered" width="100%">
    <thead>
      <tr valign="bottom">
        <th width="21%">&nbsp;</th>
        <?php 
		foreach ($array as $key => $val)
		{
		?>
        <th nowrap="nowrap"><?php echo $val[0];?></th>
        <?php 
		}
		?>
        </tr>
    </thead>
     <tbody>
      <tr>
        <td nowrap="nowrap" title="Customer Name" ><u><b>Add - Receipts</b></u></td>
         <?php 
		foreach ($array as $key => $val)
		{
		?>
       <td nowrap="nowrap" title="<?php echo $val[0];?>"><?php echo $row["PAYMENT_TERM"]?></td>
        <?php 
		}
		?>        
       </tr>
      <tr>
        <td nowrap="nowrap" title="Customer Name" >&nbsp;&nbsp;&nbsp;&nbsp;Monthly collection from customers</td>
         <?php 
		foreach ($array as $key => $val)
		{
		?>
        <td nowrap="nowrap" style="text-align:right" <?php echo $val[0];?>><?php echo number_format(GetCustomerAmount($val[2],$val[3]),2)?></td>
        <?php 
		}
		?>
       
      </tr>
     </tbody>
  </table></td>
</tr>
            <tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>
</form>
</body>
<script type="text/javascript">
$(document).ready(function()
{
	$('.bordered tbody tr').click(function(){
		if($(this).attr('style')!='background-color:#82FF82')
		{
			$('.bordered tbody tr').attr('style','background-color:#FFFFFF');  
			   $(this).attr('style','background-color:#82FF82');     
		}                                             
	})
});
</script>
</html>
<?php
function GetCustomerAmount($year,$month)
{
	global $db;
	
/*	$sql = "SELECT SUM(T1.INVOICED_AMOUNT - PAYMENT_RECEIVED_AMOUNT) AS AMOUNT 
	FROM (SELECT
  ROUND(COALESCE((SELECT SUM(VALUE) FROM finance_customer_transaction FCT WHERE FCT.ORDER_NO = OH.intOrderNo AND FCT.ORDER_YEAR = OH.intOrderYear AND FCT.DOCUMENT_TYPE = 'INVOICE'),0),2) AS INVOICED_AMOUNT,
  ROUND(COALESCE((SELECT SUM(VALUE) * -1 FROM finance_customer_transaction FCT WHERE FCT.ORDER_NO = OH.intOrderNo AND FCT.ORDER_YEAR = OH.intOrderYear AND FCT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2) AS PAYMENT_RECEIVED_AMOUNT,
  (SELECT
     DATE(FDH.dtmdate)
   FROM ware_fabricdispatchdetails FDD
     INNER JOIN ware_fabricdispatchheader FDH
       ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
         AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
   WHERE FDH.intOrderNo = OH.intOrderNo
       AND FDH.intOrderYear = OH.intOrderYear
       AND FDH.intStatus = 1
   ORDER BY DATE(FDH.dtmdate)DESC
   LIMIT 1) AS LAST_DISPATCH_DATE,
  DATE_ADD((SELECT DATE(FDH.dtmdate) FROM ware_fabricdispatchdetails FDD INNER JOIN ware_fabricdispatchheader FDH ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear WHERE FDH.intOrderNo = OH.intOrderNo AND FDH.intOrderYear = OH.intOrderYear AND FDH.intStatus = 1 ORDER BY DATE(FDH.dtmdate) DESC LIMIT 1), INTERVAL FPT.strName DAY) AS PAYMENT_DATE,
  FPT.strName AS PAYMENT_TERM
FROM finance_customer_invoice_header IH
  INNER JOIN trn_orderheader OH
    ON OH.intOrderNo = IH.ORDER_NO
      AND OH.intOrderYear = IH.ORDER_YEAR
  INNER JOIN mst_financepaymentsterms FPT
    ON FPT.intId = OH.intPaymentTerm
HAVING YEAR(PAYMENT_DATE) = $year
    AND MONTH(PAYMENT_DATE) = $month) AS T1";*/
	
	$sql = "SELECT SUM(FT.BALANCE_AMOUNT) AS AMOUNT
		FROM finance_forecast_transaction FT
		WHERE YEAR(DATE) = '$year'
		AND MONTH(DATE) = '$month'
		AND CATEGORY_CODE = 'CUSTOMER_RECEIVING'";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}
?>
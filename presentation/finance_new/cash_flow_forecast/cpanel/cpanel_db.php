<?PHP
include '../../../../dataAccess/Connector.php';

$requestType		= $_REQUEST["RequestType"];

if($requestType=="URLSave")
{
	$savedStatus	= true;
	$savedMasseged	= "";
	$error_sql 		= "";
	$detailArray1	= json_decode($_REQUEST["DetailArray1"],true);
	$detailArray2	= json_decode($_REQUEST["DetailArray2"],true);
	$detailArray3	= json_decode($_REQUEST["DetailArray3"],true);
	$db->begin();
	
	foreach($detailArray1 as $row)
	{
		if($savedStatus)
			$result = check($row,'OC');
			
		if(!$result["SavedStatus"])
			$savedStatus	= false;
	}
	
	
	foreach($detailArray2 as $row)
	{
		if($savedStatus)
			$result = check($row,'CC');
			
		if(!$result["SavedStatus"])
			$savedStatus	= false;
	}
	
	foreach($detailArray3 as $row)
	{
		if($savedStatus)
			$result = check($row,'D');
			
		if(!$result["SavedStatus"])
			$savedStatus	= false;
	}
	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Updated successfully.";
	}else{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql']		= $error_sql;
	}
	echo json_encode($response);
}

function check($row,$mainCode)
{
	global $db;
	
	$sql = "SELECT COUNT(*) AS COUNT 
			FROM finance_cash_flow_cpanel
			WHERE 
				ORDER_BY = '".$row["ID"]."'
				AND MAIN_CODE = '$mainCode';";
	$result = $db->RunQuery2($sql);
	$row1	= mysqli_fetch_array($result);
	if($row1["COUNT"]>0){
		$a = update($row,$mainCode);
		return $a;
	}else{
		$b = insert($row,$mainCode);
		return $b;
	}
}

function insert($row,$mainCode)
{
	global $db;
	$subCategory	= $row["SubCategory"]=='null'?'':$row["SubCategory"];
	$value			= $row["Value"]=='null'?'':$row["Value"];
	
	$sql = "INSERT INTO finance_cash_flow_cpanel 
			(MAIN_CODE, 
			MAIN_DISPLAY_NAME, 
			SUB_NAME, 
			VALUE, 
			ORDER_BY)
			VALUES
			('$mainCode', 
			'MAIN_DISPLAY_NAME', 
			'$subCategory', 
			'$value', 
			'".$row["ID"]."');";
	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$data["SavedStatus"]	= false;
		$data["SavedMasseged"]	= $db->errormsg;
		$data["ErrorSql"] 		= $sql;
	}
	else
		$data["SavedStatus"]	= true;
	
	return $data;
}

function update($row,$mainCode)
{
	global $db;
	$subCategory	= $row["SubCategory"]=='null'?'':$row["SubCategory"];
	$value			= $row["Value"]=='null'?'':$row["Value"];
	
	$sql = "UPDATE finance_cash_flow_cpanel
			SET SUB_NAME = '$subCategory',
				VALUE = '$value'
			WHERE ORDER_BY = '".$row["ID"]."'
				AND MAIN_CODE = '$mainCode';";
	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$data["SavedStatus"]	= false;
		$data["SavedMasseged"]	= $db->errormsg;
		$data["ErrorSql"] 		= $sql;
	}
	else
		$data["SavedStatus"]	= true;
	
	return $data;
}
?>
<?php
session_start();
$backwardseperator 		= "../../../../";
include_once "{$backwardseperator}dataAccess/permisionCheck.inc";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Finance Cash Flow CPanel</title>
<link rel="stylesheet" type="text/css" href="../../../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../../../css/button.css"/>
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">
<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="cpanel.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>
<body>
<form id="frmCPanel" name="frmCPanel" autocomplete="off" action="loan_schedule.php" method="post">
  <table width="100%" border="0" align="center">
    <tr>
      <td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
    </tr>
  </table>
  <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script> 
  <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
  <link rel="stylesheet" href="../../../../libraries/chosen/chosen/chosen.css" />
  <script src="../../../../libraries/chosen/chosen/prototype.js" type="text/javascript"></script> 
  <script src="../../../../libraries/chosen/chosen/chosen.proto.js" type="text/javascript"></script> 
  <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script> 
  <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script> 
  <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script> 
  <script>	
jQuery.noConflict();
</script>
  <div align="center">
    <div class="trans_layoutXL">
      <div class="trans_text">Finance Cash Flow Configuration Panel</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" class="bordered" id="tblMain">
              <thead>
                <tr>
                  <th colspan="4">Configuration Panel
                    <div style="float:right"><a class="button white small" id="butInsertRow">Add New Row</a></div></th>
                </tr>
              </thead>
              <tbody>
                <tr <?php echo($count==2?'class="cls_tr_firstRow"':''); ?>>
                  <td width="2%" class="clsDelete"  style="text-align:center">1.</td>
                  <td width="18%">Operational Costs</td>
                  <td width="80%"  style="text-align:center"><table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblSub1">
                      <tbody>
                        <tr id="1" class="cls_tr">
                          <td width="25%" class="cls_td_subCategory"><input type="text" name="textfield" id="textfield" style="width:100%;height:30px" value="Staff salaries and EPF / ETF"/></td>
                          <td width="75%" class="cls_td_value"><select name="select" multiple="multiple" class="txtCalled normalfnt msCombo" style="width:600px;height:50px" tabindex="4" data-placeholder="">
                              <?php
		$savedArray = getSavedSubType('OC',1);
		$savedArray	= explode(',',$savedArray);	
		$sql = userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected = 0;
   				foreach($savedArray as $x)
				{
					if($x==$row['CHART_OF_ACCOUNT_ID'])
					  $selected = 1;	
				}
					if($selected==1)
 						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";
					else
						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";					
 		}
		echo $html;
						?>
                            </select></td>
                        </tr>
                        <tr id="2" class="cls_tr">
                          <td class="cls_td_subCategory"><input type="text" name="textfield2" id="textfield2" style="width:100%;height:30px" value="Management Salary"/></td>
                          <td class="cls_td_value"><select name="select2" multiple="multiple" class="txtCalled normalfnt msCombo" style="width:600px;height:50px" tabindex="4" data-placeholder="">
                            <?php
		$savedArray = getSavedSubType('OC',2);
		$savedArray	= explode(',',$savedArray);
		$sql = userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected = 0;
   				foreach($savedArray as $x)
				{
					if($x==$row['CHART_OF_ACCOUNT_ID'])
					  $selected = 1;	
				}
					if($selected==1)
 						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";
					else
						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";					
 		}
		echo $html;
						?>
                          </select></td>
                        </tr>
                        <tr id="3" class="cls_tr">
                          <td class="cls_td_subCategory"><input type="text" name="textfield4" id="textfield4" style="width:100%;height:30px" value="Factory salaries"/></td>
                          <td class="cls_td_value"><select name="select13" multiple="multiple" class="txtCalled normalfnt msCombo" style="width:600px;height:50px" tabindex="4" data-placeholder="">
                            <?php
		$savedArray = getSavedSubType('OC',3);
		$savedArray	= explode(',',$savedArray);
		$sql = userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected = 0;
   				foreach($savedArray as $x)
				{
					if($x==$row['CHART_OF_ACCOUNT_ID'])
					  $selected = 1;	
				}
					if($selected==1)
 						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";
					else
						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";					
 		}
		echo $html;
						?>
                          </select></td>
                        </tr>
                        <tr id="4" class="cls_tr">
                          <td class="cls_td_subCategory"><input type="text" name="textfield6" id="textfield5" style="width:100%;height:30px" value="Factory - EPF/ETF"/></td>
                          <td class="cls_td_value"><select name="select14" multiple="multiple" class="txtCalled normalfnt msCombo" style="width:600px;height:50px" tabindex="4" data-placeholder="">
                            <?php
		$savedArray = getSavedSubType('OC',4);
		$savedArray	= explode(',',$savedArray);
		$sql = userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected = 0;
   				foreach($savedArray as $x)
				{
					if($x==$row['CHART_OF_ACCOUNT_ID'])
					  $selected = 1;	
				}
					if($selected==1)
 						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";
					else
						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";					
 		}
		echo $html;
						?>
                          </select></td>
                        </tr>
                        <tr id="5" class="cls_tr">
                          <td class="cls_td_subCategory"><input type="text" name="textfield7" id="textfield7" style="width:100%;height:30px" value="EPF arrears payments"/></td>
                          <td class="cls_td_value"><select name="select15" multiple="multiple" class="txtCalled normalfnt msCombo" style="width:600px;height:50px" tabindex="4" data-placeholder="">
                            <?php
		$savedArray = getSavedSubType('OC',5);
		$savedArray	= explode(',',$savedArray);
		$sql = userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected = 0;
   				foreach($savedArray as $x)
				{
					if($x==$row['CHART_OF_ACCOUNT_ID'])
					  $selected = 1;	
				}
					if($selected==1)
 						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";
					else
						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";					
 		}
		echo $html;
						?>
                          </select></td>
                        </tr>
                        <tr id="6" class="cls_tr">
                          <td class="cls_td_subCategory"><input type="text" name="textfield8" id="textfield8" style="width:100%;height:30px" value="Bonus Payments - Staff"/></td>
                          <td class="cls_td_value"><select name="select16" multiple="multiple" class="txtCalled normalfnt msCombo" style="width:600px;height:50px" tabindex="4" data-placeholder="">
                            <?php
		$savedArray = getSavedSubType('OC',6);
		$savedArray	= explode(',',$savedArray);
		$sql = userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected = 0;
   				foreach($savedArray as $x)
				{
					if($x==$row['CHART_OF_ACCOUNT_ID'])
					  $selected = 1;	
				}
					if($selected==1)
 						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";
					else
						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";					
 		}
		echo $html;
						?>
                          </select></td>
                        </tr>
                        
                        <tr id="7" class="cls_tr">
                          <td class="cls_td_subCategory"><input type="text" name="textfield8" id="textfield8" style="width:100%;height:30px" value="Bonus Payments - Workers"/></td>
                          <td class="cls_td_value"><select name="select16" multiple="multiple" class="txtCalled normalfnt msCombo" style="width:600px;height:50px" tabindex="4" data-placeholder="">
                            <?php
		$savedArray = getSavedSubType('OC',7);
		$savedArray	= explode(',',$savedArray);
		$sql = userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected = 0;
   				foreach($savedArray as $x)
				{
					if($x==$row['CHART_OF_ACCOUNT_ID'])
					  $selected = 1;	
				}
					if($selected==1)
 						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";
					else
						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";					
 		}
		echo $html;
						?>
                          </select></td>
                        </tr>
                        
                        <tr id="8" class="cls_tr">
                          <td class="cls_td_subCategory"><input type="text" name="textfield9" id="textfield9" style="width:100%;height:30px" value="Gratuity payments"/></td>
                          <td class="cls_td_value"><select name="select17" multiple="multiple" class="txtCalled normalfnt msCombo" style="width:600px;height:50px" tabindex="4" data-placeholder="">
                            <?php
		$savedArray = getSavedSubType('OC',8);
		$savedArray	= explode(',',$savedArray);
		$sql = userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected = 0;
   				foreach($savedArray as $x)
				{
					if($x==$row['CHART_OF_ACCOUNT_ID'])
					  $selected = 1;	
				}
					if($selected==1)
 						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";
					else
						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";					
 		}
		echo $html;
						?>
                          </select></td>
                        </tr>
                        <tr id="9" class="cls_tr">
                          <td class="cls_td_subCategory"><input type="text" name="textfield10" id="textfield10" style="width:100%;height:30px" value="Taxes payments"/></td>
                          <td class="cls_td_value"><select name="select18" multiple="multiple" class="txtCalled normalfnt msCombo" style="width:600px;height:50px" tabindex="4" data-placeholder="">
                            <?php
		$savedArray = getSavedSubType('OC',9);
		$savedArray	= explode(',',$savedArray);
		$sql = userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected = 0;
   				foreach($savedArray as $x)
				{
					if($x==$row['CHART_OF_ACCOUNT_ID'])
					  $selected = 1;	
				}
					if($selected==1)
 						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";
					else
						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";					
 		}
		echo $html;
						?>
                          </select></td>
                        </tr>
                        <tr id="10" class="cls_tr">
                          <td class="cls_td_subCategory"><input type="text" name="textfield11" id="textfield11" style="width:100%;height:30px" value="Other OH payments"/></td>
                          <td class="cls_td_value"><select name="select19" multiple="multiple" class="txtCalled normalfnt msCombo" style="width:600px;height:50px" tabindex="4" data-placeholder="">
                            <?php
		$savedArray = getSavedSubType('OC',10);
		$savedArray	= explode(',',$savedArray);
		$sql = userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected = 0;
   				foreach($savedArray as $x)
				{
					if($x==$row['CHART_OF_ACCOUNT_ID'])
					  $selected = 1;	
				}
					if($selected==1)
 						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";
					else
						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";					
 		}
		echo $html;
						?>
                          </select></td>
                        </tr>
                        
                      </tbody>
                      <tfoot>
                        <tr>
                          <td colspan="2" style="text-align:center"><a class="button white medium" style="width:96%" id="butAddNew">Add Row</a></td>
                        </tr>
                      </tfoot>
                    </table></td>
                </tr>
                <tr>
                  <td  style="text-align:center" class="clsDelete">2.</td>
                  <td>Current Commitments</td>
                  <td  style="text-align:center"><table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblSub2">
                    <tbody>
                      <tr id="1" class="cls_tr">
                        <td width="25%" class="cls_td_subCategory"><input type="text" name="textfield3" id="textfield3" style="width:100%;height:30px" value="<?php echo GetCCDetails('SUB_NAME',1);?>"/></td>
                        <td width="75%" class="cls_td_value"><input type="text" name="textfield5" id="textfield6" style="width:100%;height:30px" value="<?php echo GetCCDetails('VALUE',1);?>"/></td>
                      </tr>
                      <tr id="2" class="cls_tr">
                        <td class="cls_td_subCategory"><select name="select12" id="select7" style="width:100%;height:30px">
                          <option value="WHERE STATUS IN ">STATUS</option>
                        </select></td>
                        <td class="cls_td_value"><select name="select11" id="select6" style="width:100%;height:30px">
                          <option value="1" <?php echo GetCCDetails('VALUE',2)==1?"selected='selected'":""?>>ACTIVE</option>
                          <option value="0" <?php echo GetCCDetails('VALUE',2)==0?"selected='selected'":""?>>IN ACTIVE</option>
                          <option value="1,0" <?php echo GetCCDetails('VALUE',2)=='1,0'?"selected='selected'":""?>>BOTH</option>
                        </select></td>
                      </tr>
                      <tr id="3">
                        <td class="cls_td_subCategory"><select name="select3" id="select" style="width:100%;height:30px">
                          <option value="AND SUB_TYPE_ID NOT IN" <?php echo GetCCDetails('SUB_NAME',3)=='AND SUB_TYPE_ID NOT IN'?"selected='selected'":""?>>SUB_TYPE_ID NOT IN</option>
                          <option value="AND SUB_TYPE_ID IN" <?php echo GetCCDetails('SUB_NAME',3)=='AND SUB_TYPE_ID IN'?"selected='selected'":""?>>SUB_TYPE_ID IN</option>
                        </select></td>
                        <td class="cls_td_value"><select name="select4" multiple="multiple" class="txtCalled normalfnt msCombo" style="width:600px;height:50px" tabindex="4" data-placeholder="">
                          <?php
					$savedArray = getSavedSubType('CC',3);
					$savedArray	= explode(',',$savedArray);	
					$sql = loadSubTypeId();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected = 0;
   				foreach($savedArray as $x)
				{
					if($x==$row['SUB_TYPE_ID'])
					  $selected = 1;	
				}
					if($selected==1)
 						$html .= "<option value=\"".$row['SUB_TYPE_ID']."\" selected=\"selected\">".$row['SUB_TYPE_NAME']."</option>";
					else
						$html .= "<option value=\"".$row['SUB_TYPE_ID']."\">".$row['SUB_TYPE_NAME']."</option>";					
 		}
		echo $html;
						?>
                        </select></td>
                      </tr>
                      <tr id="4">
                        <td class="cls_td_subCategory"><select name="select5" id="select2" style="width:100%;height:30px">
                          <option value="AND BANK IN">BANK</option>
                        </select></td>
                        <td class="cls_td_value"><select name="select6" id="select3" style="width:100%;height:30px">
                          <option value="1" <?php echo GetCCDetails('VALUE',4)=='1'?"selected='selected'":""?>>ALLOWED</option>
                          <option value="0" <?php echo GetCCDetails('VALUE',4)=='0'?"selected='selected'":""?>>NOT ALLOWED</option>
                        </select></td>
                      </tr>
                      <tr id="5">
                        <td class="cls_td_subCategory"><select name="select9" id="select5" style="width:100%;height:30px">
                          <option value="AND CHART_OF_ACCOUNT_ID NOT IN" <?php echo GetCCDetails('SUB_NAME',5)=='AND CHART_OF_ACCOUNT_ID NOT IN'?"selected='selected'":""?>>CHART_OF_ACCOUNT_ID NOT IN</option>
                          <option value="AND CHART_OF_ACCOUNT_ID IN" <?php echo GetCCDetails('SUB_NAME',5)=='AND CHART_OF_ACCOUNT_ID IN'?"selected='selected'":""?>>CHART_OF_ACCOUNT_ID IN</option>
                        </select></td>
                        <td class="cls_td_value"><select name="select10" multiple="multiple" class="txtCalled normalfnt msCombo" style="width:600px;height:50px" tabindex="4" data-placeholder="">
                          <?php
		$savedArray = getSavedSubType('CC',5);
		$savedArray	= explode(',',$savedArray);	
		$sql = userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected = 0;
   				foreach($savedArray as $x)
				{
					if($x==$row['CHART_OF_ACCOUNT_ID'])
					  $selected = 1;	
				}
					if($selected==1)
 						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";
					else
						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";					
 		}
		echo $html;
						?>
                        </select></td>
                      </tr>
                    </tbody>
                    <tfoot>
                    </tfoot>
                  </table></td>
                </tr>
                <tr>
                <td style="text-align:center" class="clsDelete">3.</td>
                <td>Depriciate Bank Transfer</td>
                <td style="text-align:center"><table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblSub3">
                    <tbody>
                      <tr id="1">
                        <td width="25%" class="cls_td_subCategory"><select name="select8" id="select4" style="width:100%;height:30px">
                          <option value="WHERE CHART_OF_ACCOUNT_ID IN" <?php echo GetCCDetails('SUB_NAME',1)=='WHERE CHART_OF_ACCOUNT_ID IN'?"selected='selected'":""?>>CHART_OF_ACCOUNT_ID IN</option>
                          <option value="WHERE CHART_OF_ACCOUNT_ID NOT IN" <?php echo GetCCDetails('SUB_NAME',1)=='WHERE CHART_OF_ACCOUNT_ID NOT IN'?"selected='selected'":""?>>CHART_OF_ACCOUNT_ID NOT IN</option>
                         
                        </select></td>
                        <td width="75%" class="cls_td_value"><select name="select7" multiple="multiple" class="txtCalled normalfnt msCombo" style="width:600px;height:50px" tabindex="4" data-placeholder="">
                          <?php
		$savedArray = getSavedSubType('D',1);
		$savedArray	= explode(',',$savedArray);	
		$sql = userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected = 0;
   				foreach($savedArray as $x)
				{
					if($x==$row['CHART_OF_ACCOUNT_ID'])
					  $selected = 1;	
				}
					if($selected==1)
 						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";
					else
						$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']."</option>";					
 		}
		echo $html;
						?>
                        </select></td>
                      </tr>
                      </tbody>
                    <tfoot>
                    </tfoot>
                  </table></td>
                </tr>
                </tbody>
            </table></td>
        </tr>
        <tr>
          <td height="32" style="text-align:center"><a class="button green" id="butSave">SAVE</a></td>
        </tr>
      </table>
    </div>
  </div>
</form>
</body>
</html>
<?php
function userList()
{	
	$sql = "SELECT
				CHART_OF_ACCOUNT_ID,
				CHART_OF_ACCOUNT_NAME
			FROM finance_mst_chartofaccount
			WHERE STATUS = 1
			AND CATEGORY_TYPE IN ('N')
			ORDER BY CHART_OF_ACCOUNT_NAME";		
	return $sql;
}

function loadSubTypeId()
{
	$sql = "SELECT SUB_TYPE_ID,SUB_TYPE_NAME FROM finance_mst_account_sub_type WHERE STATUS = 1 ORDER BY SUB_TYPE_NAME";
	return $sql;
}

function GetCCDetails($subName,$orderId)
{
	global $db;
	$sql = "SELECT $subName
			FROM finance_cash_flow_cpanel
			WHERE MAIN_CODE = 'CC'
			AND ORDER_BY = $orderId";
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	return $row[$subName];
}

function getSavedSubType($mainCode,$orderById)
{
	global $db;
	
	$sql = "SELECT VALUE FROM finance_cash_flow_cpanel WHERE MAIN_CODE = '$mainCode' AND ORDER_BY = $orderById";
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	return $row["VALUE"];
}
?>
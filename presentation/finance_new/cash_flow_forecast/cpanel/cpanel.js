var para;
var objName; 
// JavaScript Document
 
jQuery(document).ready(function() {
  	jQuery("#frmMeeting").validationEngine();
	
	//-------------------------
     var select, selects, _i, _len, _results;
    if (Prototype.Browser.IE && (Prototype.BrowserFeatures['Version'] === 6 || Prototype.BrowserFeatures['Version'] === 7)) {
      return;
    }

	var a1=".msCombo";
	var a2=".msCombo-deselect";
	//alert(a1);
	
    selects = $$(a1);
    _results = [];
    for (_i = 0, _len = selects.length; _i < _len; _i++) {
      select = selects[_i];
      _results.push(new Chosen(select));
    }
    deselects = $$(a2);
    for (_i = 0, _len = deselects.length; _i < _len; _i++) {
      select = deselects[_i];
      _results.push(new Chosen(select,{allow_single_deselect:true}));
    }
   // return _results;	
	//---------------------------------
	
	jQuery('#butAddNew').live('click',addNewRow);
	jQuery('#butSave').live('click',save);
	
jQuery('#butSave1').live('click',function(){
 		showWaiting_c();
		
		if(!validateData())
			return;
		
		var data = "requestType=save";
 		var arrHeader = "{";
 					arrHeader += '"minuteId":"'+ jQuery('#txtSerialNo').val()+'",' ;
					arrHeader += '"subject":'+ URLEncode_json(jQuery('#txtSubject').val()) +',' ;
					arrHeader += '"meetingPlace":'+ URLEncode_json(jQuery('#txtMeetinPlace').val()) +',' ;
					arrHeader += '"arrCalledBy":"'+ jQuery('.txtCalled').val() +'",' ;
					arrHeader += '"arrAttendance":"'+ jQuery('.txtAttendees').val() +'",' ;
					arrHeader += '"arrDistribution":"'+ jQuery('.txtDistribution').val() +'",' ;
					arrHeader += '"arrOtherDistribution":"'+ jQuery('#txtOtherDistribution').val() +'",' ;
					arrHeader += '"date":"'+ jQuery('#dtDate').val() +'"' ;
					
		arrHeader += ' }';
		
		var i=0;
		var rcds=0;
		var errorDescFlag=0;
		var errorActionByFlag=0;
		var arrDetails="";
		
		
		jQuery('.concern').each(function(){
			i++;
			var no 				=	jQuery(this).parent().parent().find('.no').attr('id');
			var completed		=	jQuery(this).parent().parent().find('.delC').attr('id');
			var concern 		=	jQuery(this).val();
 			var recommendation 	=	jQuery(this).parent().parent().find('.recommendation').val();
			var actionPlan 		=	jQuery(this).parent().parent().find('.actionPlan').val();
			var actionBy 		=	jQuery(this).parent().parent().find('.txtActionBy'+i).val();
			var concernBy 		=	jQuery(this).parent().parent().find('.txtConcernRaisedBy'+i).val();
			
			if(actionBy==null){actionBy='';}
			if(concernBy==null){concernBy='';}
			
			var dueDate =jQuery(this).parent().parent().find('.date').val();

			if((concern!='') &&(completed !=1))
			{
				rcds++;
				arrDetails += "{";
							arrDetails += '"concern":'+ URLEncode_json( concern )+',' ;
							arrDetails += '"recommendation":'+ URLEncode_json(recommendation) +',' ;
							arrDetails += '"actionPlan":'+ URLEncode_json(actionPlan) +',' ;
							arrDetails += '"actionBy":"'+ actionBy +'",' ;
							arrDetails += '"concernBy":"'+ concernBy +'",' ;
							arrDetails += '"dueDate":"'+ dueDate +'",' ;
							arrDetails += '"no":"'+ no +'"' ;
							arrDetails +=  '},';
			}
		})
		
		if(rcds==0){
			alert("There is no none-completed 'Concerns' to save");
			hideWaiting_c();
			return false;
		}
		
		arrDetails = arrDetails.substr(0,arrDetails.length-1);
		
		var arrHeader=arrHeader;
		var arrDetails='['+arrDetails+']';
 		data+="&arrHeader="	+	arrHeader+"&arrDetails="	+	arrDetails;
 		
		var url = "meetingMinutes-db-set.php";
		jQuery.ajax({
				url:url,
				async:false,
				dataType:'json',
				type:'post',
				data:data,
		success:function(json){
				jQuery('#frmMeeting #butSave').validationEngine('showPrompt', json.msg,json.type );
				if(json.type=='pass')
				{
					jQuery('#txtSerialNo').val(json.serialNo);
 					showCompleteButton();
					var t=setTimeout("alertx()",1000);
					hideWaiting_c();
					jQuery("#butReport").show();  
					return;
				}
				hideWaiting_c();
			},
		error:function(xhr,status){
				jQuery('#frmMeeting #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t=setTimeout("alertx()",1000);
				hideWaiting_c();
				return;
			}
			
		});

 	});



 //-----------------------------------
jQuery('#butReport').live('click',function(){	

	if(jQuery('#txtSerialNo').val()!=''){
		window.open('../listing/rptMeetingMinutes.php?serialNo='+jQuery('#txtSerialNo').val());	
	}
	else{
		alert("There is no Meeting Minute to view");
	}
});
//----------------------------------	
});//end of ready

function alertx()
{
	jQuery('#frmMeeting #butSave').validationEngine('hide')	;
	jQuery('#frmMeeting #butComplete').validationEngine('hide')	;
	jQuery('#frmMeeting #butDelete').validationEngine('hide')	;
	
}
function pageSubmitOnChange()
{
	var date 		= jQuery('#dtDate').val();
	var location 		= jQuery('#cboLocation').val();
	
	if(date==''){
		alert('Please select the date');
		return false;
	}
	else if(location==''){
		alert('Please select the location');
		return false;
	}
	
	if(date!='' && location!='')
	{
		document.getElementById('frmMeeting').submit();
	}
}
 
function deleteRow(obj)
{
 		jQuery(obj).parent().parent().remove();
} 

function clearGrid()
{
	var rowCount = document.getElementById('tblPopup').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblPopup').deleteRow(1);
	}
}

function addNewRow()
{
	jQuery('#tblMain #tblSub tbody tr:last').after("<tr>"+jQuery('#tblMain #tblSub tbody').children('tr:first').html()+"</tr>");
	comboAttr1(2);
}

function add_new_row(table,rowcontent){
        if (jQuery(table).length>0){
            if (jQuery(table+' > tbody').length==0) jQuery(table).append('<tbody />');
            (jQuery(table+' > tr').length>0)?jQuery(table).children('tbody:last').children('tr:last').append(rowcontent):jQuery(table).children('tbody:last').append(rowcontent);
        }
}

function showWaiting_c()
{
		var popupbox = document.createElement("div");
		var windowWidth = document.documentElement.clientWidth;
		var windowHeight = document.documentElement.clientHeight;
		var scrollH = (document.body.scrollHeight);
		//alert(windowHeight);
   popupbox.id = "divBackGroundBalck";
   popupbox.style.position = 'absolute';
   popupbox.style.zIndex = 100;
   popupbox.style.textAlign = 'center';
   popupbox.style.left = 0 + 'px';
   popupbox.style.top = 0 + 'px'; 
   popupbox.style.background="#000000"; 
   popupbox.style.width = screen.width + 'px';
   popupbox.style.height =  (scrollH)+ 'px';
   popupbox.style.opacity = 0.5;
   popupbox.style.color = "#FFFFFF";
	document.body.appendChild(popupbox);
	//document.getElementById('divBackGroundBalck').innerHTML = "this is text code";
	var popupbox1 = document.createElement("div");
	 popupbox1.id = "divBackgroundImg";
   popupbox1.style.position = 'absolute';
   popupbox1.style.zIndex = 101;
   popupbox1.style.verticalAlign = 'center';
   popupbox1.style.left =  windowWidth/2-100/2 +'px';
   popupbox1.style.top = (jQuery(window).scrollTop()+200) + 'px'; 
   popupbox1.style.width = '100px';
   popupbox1.style.height =  '100px';
   popupbox1.style.opacity = 1;
   popupbox1.style.color = "#FFFFFF";
	document.body.appendChild(popupbox1);
	
	//alert(mainPath);
	document.getElementById('divBackgroundImg').innerHTML = "<img src=\""+projectName+"/images/loading_go.gif\" /><span class=\"normalfnt\" style=\"color:white;\">Please Wait...</span>";
	
}

function hideWaiting_c()
{
	try
	{
		var box = document.getElementById('divBackGroundBalck');
		box.parentNode.removeChild(box);
		
		var box1 = document.getElementById('divBackgroundImg');
		box1.parentNode.removeChild(box1);
		
	}
	catch(err)
	{        
	}	
}

function validateData()
{
  	if((jQuery('#txtSubject').val()=='') || (jQuery('#txtSubject').val()==null)){
			alert("Please Enter 'Subject'");
			jQuery('#txtSubject').focus();
			hideWaiting_c();
			return false;
		}
 	else if((jQuery('#txtMeetinPlace').val()=='') || (jQuery('#txtMeetinPlace').val()==null)){
			alert("Please Enter 'Meeting Place'");
			hideWaiting_c();
			return false;
		}
/* 	else if((jQuery('.txtCalled').val()=='') || (jQuery('.txtCalled').val()==null)){
			alert("Please Enter 'Called By'");
			hideWaiting_c();
			return false;
		}
 	else if((jQuery('.txtAttendees ').val()=='') || (jQuery('.txtAttendees').val()==null)){
			alert("Please Enter 'Attendees'");
			hideWaiting_c();
			return false;
		}
*/		
		var i=0;
		jQuery('.concern').each(function(){
 			var concern 		=	jQuery(this).val();
 			if((concern!=''))
			{
				i++;
			}
		});
		
		if(i==0){
			alert("Please enter atleast one 'Concern' to save");
			hideWaiting_c();
			return false;
		}
		
	
	return true;
}

function disableFields(obje){
}

function comboAttr1(i){
 // document.observe('dom:loaded', function(evt2) {
	  
     var select, selects, _i, _len, _results;
    if (Prototype.Browser.IE && (Prototype.BrowserFeatures['Version'] === 6 || Prototype.BrowserFeatures['Version'] === 7)) {
      return;
    }
	var a1=".txtCalled"+i;
	var a2=".txtCalled"+i+"-deselect";
	
 	//-----------------------
    selects = $$(a1);
    _results = [];
    for (_i = 0, _len = selects.length; _i < _len; _i++) {
      select = selects[_i];
      _results.push(new Chosen(select));
    }
    deselects = $$(a2);
    for (_i = 0, _len = deselects.length; _i < _len; _i++) {
      select = deselects[_i];
      _results.push(new Chosen(select,{allow_single_deselect:true}));
    }
    return _results;
 	//-----------------------
}

function comboAttr2(i){
	//----------------------------------------
     var select, selects, _i, _len, _results;
    if (Prototype.Browser.IE && (Prototype.BrowserFeatures['Version'] === 6 || Prototype.BrowserFeatures['Version'] === 7)) {
      return;
    }
	var a1=".txtConcernRaisedBy"+i;
	var a2=".txtConcernRaisedBy"+i+"-deselect";
	
  
    selects = $$(a1);
    _results = [];
    for (_i = 0, _len = selects.length; _i < _len; _i++) {
      select = selects[_i];
      _results.push(new Chosen(select));
    }
    deselects = $$(a2);
    for (_i = 0, _len = deselects.length; _i < _len; _i++) {
      select = deselects[_i];
      _results.push(new Chosen(select,{allow_single_deselect:true}));
    }
    return _results;
 	//-----------------------
}

function showCompleteButton(){
 		jQuery('.concern').each(function(){
 			if((jQuery(this).parent().parent().find('.delC').attr('id')!=1) && (jQuery(this).val() !='')){
			 jQuery(this).parent().parent().find('.complete').html('<a id="butComplete" class="button green medium" style="" name="butComplete"> Complete </a>');
			}
 		});
	
}

function quote(string)
{
	string = string.replace(/'/gi,"\\'");
	string = string.replace(/"/gi,'\\"');
	return string;	
}

function save()
{
	showWaiting_c();
	
	var url = "cpanel_db.php?RequestType=URLSave";
	var data = "";
	
	var detailArray	= "";
	jQuery('#tblMain #tblSub1 tbody tr').each(function(){
		detailArray += "{";
		detailArray += '"ID":"'+jQuery(this).attr('id')+'",';
		detailArray += '"SubCategory":"'+jQuery(this).find('.cls_td_subCategory').children().val()+'",';
		detailArray += '"Value":"'+jQuery(this).find('.cls_td_value').children().val()+'"';
		detailArray += "},";
    });
	detailArray = detailArray.substr(0,detailArray.length-1);
	data += "DetailArray1="+"["+detailArray+"]";
	
	var detailArray	= "";
	jQuery('#tblMain #tblSub2 tbody tr').each(function(){
		detailArray += "{";
		detailArray += '"ID":"'+jQuery(this).attr('id')+'",';
		detailArray += '"SubCategory":"'+jQuery(this).find('.cls_td_subCategory').children().val()+'",';
		detailArray += '"Value":"'+jQuery(this).find('.cls_td_value').children().val()+'"';
		detailArray += "},";
    });
	detailArray = detailArray.substr(0,detailArray.length-1);
	data += "&DetailArray2="+"["+detailArray+"]";
	
	var detailArray	= "";
	jQuery('#tblMain #tblSub3 tbody tr').each(function(){
		detailArray += "{";
		detailArray += '"ID":"'+jQuery(this).attr('id')+'",';
		detailArray += '"SubCategory":"'+jQuery(this).find('.cls_td_subCategory').children().val()+'",';
		detailArray += '"Value":"'+jQuery(this).find('.cls_td_value').children().val()+'"';
		detailArray += "},";
    });
	detailArray = detailArray.substr(0,detailArray.length-1);
	data += "&DetailArray3="+"["+detailArray+"]";
	
	jQuery.ajax({
				url:url,
				async:false,
				dataType:'json',
				type:'post',
				data:data,
				success:function(json){
						jQuery('#frmCPanel #butSave').validationEngine('showPrompt', json.msg,json.type );
						if(json.type=='pass')
						{
							var t = setTimeout("alertx()",1000);
							hideWaiting_c();
							return;
						}
					},
				error:function(xhr,status){
						jQuery('#frmCPanel #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t = setTimeout("alertx()",1000);
						hideWaiting_c();
						return;				
					}
	});
}

function alertx()
{
	jQuery('#frmCPanel #butSave').validationEngine('hide');	
}
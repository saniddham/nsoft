<?Php
include_once "../../../../dataAccess/LoginDBManager.php";
include_once "../../../../libraries/htmlDom/simple_html_dom.php";

ini_set('display_errors',1);

$db 				= new LoginDBManager();
$currDate 			= date('Y-m-d');
$NewDate 			= date ("Y-m-d", strtotime("-1 day", strtotime($currDate)));
$day 				= date("d", strtotime($NewDate));
$month 				= date("m", strtotime($NewDate));
$year 				= date("Y", strtotime($NewDate));
$currDate			= $NewDate;
$chkCount			= 0;
$chkCountInd		= 0;

//$url				= ('http://www.cbsl.gov.lk/htm/scripts/get_mon_top_info.asp?d='.$year.'&m='.$month.'&dy='.$day);
$html 				= file_get_html('http://www.cbsl.gov.lk/htm/scripts/get_mon_top_info.asp?d='.$year.'&m='.$month.'&dy='.$day);
$rowCount 			= count($html->find('tr'));
do
{
	$chkCount++;	
	$NewDate 		= date ("Y-m-d", strtotime("-1 day", strtotime($currDate)));
	$day 			= date("d", strtotime($NewDate));
	$month 			= date("m", strtotime($NewDate));
	$year 			= date("Y", strtotime($NewDate));
	$currDate		= $NewDate;
	//$url			= ('http://www.cbsl.gov.lk/htm/scripts/get_mon_top_info.asp?d='.$year.'&m='.$month.'&dy='.$day);
	$html 			= file_get_html('http://www.cbsl.gov.lk/htm/scripts/get_mon_top_info.asp?d='.$year.'&m='.$month.'&dy='.$day);
	$rowCount 		= count($html->find('tr'));
}
while ($rowCount<=3 && $chkCount<10);

if($chkCount>=10)
{
	die('server Error');
}

foreach($html->find('tr') as $row)
{
	$rowData = array();
	$firstTime = true;
	foreach($row->find('td') as $cell) 
	{
		if($cell->getAttribute('class')=='table_txt')
		{
			if($firstTime)
				$rowData[] = $cell->innertext;
			
			foreach($cell->find('div') as $div) 
			{
				$rowData[] = $div->innertext;
			}
		$firstTime=false;
		}
	}
	if(count($rowData)>0)
		$arrData[] = $rowData;
}
$rowData[0] = 'LKR';
$rowData[1] = '1.0000';
$rowData[2] = '1.0000';
$arrData[]  = $rowData;

$currDateInd 			= date('Y-m-d');
$NewDateInd 			= date ("Y-m-d", strtotime("-1 day", strtotime($currDateInd)));
$dayInd 				= date("d", strtotime($NewDateInd));
$monthInd 				= date("m", strtotime($NewDateInd));
$yearInd 				= date("Y", strtotime($NewDateInd));
$currDateInd			= $NewDateInd;

//$urlInd					= ('http://www.cbsl.gov.lk/htm/scripts/get_mon_gulf_info.asp?d='.$yearInd.'&m='.$monthInd.'&dy='.$dayInd);
$htmlInd 				= file_get_html('http://www.cbsl.gov.lk/htm/scripts/get_mon_gulf_info.asp?d='.$yearInd.'&m='.$monthInd.'&dy='.$dayInd);
$rowCountInd 			= count($htmlInd->find('tr'));
do
{
	$chkCountInd++;	
	$NewDateInd 		= date ("Y-m-d", strtotime("-1 day", strtotime($currDateInd)));
	$dayInd 			= date("d", strtotime($NewDateInd));
	$monthInd 			= date("m", strtotime($NewDateInd));
	$yearInd 			= date("Y", strtotime($NewDateInd));
	$currDateInd		= $NewDateInd;
	//$urlInd				= ('http://www.cbsl.gov.lk/htm/scripts/get_mon_gulf_info.asp?d='.$yearInd.'&m='.$monthInd.'&dy='.$dayInd);
	$htmlInd 			= file_get_html('http://www.cbsl.gov.lk/htm/scripts/get_mon_gulf_info.asp?d='.$yearInd.'&m='.$monthInd.'&dy='.$dayInd);

	$rowCountInd 		= count($htmlInd->find('tr'));
}
while ($rowCountInd<=3 && $chkCountInd<10);

if($chkCountInd>=10)
{
	die('server Error');
}

$tdCount = 3;

foreach($htmlInd->find('tr') as $row)
{
	$rowData = array();
	$found	 = false;
	$k       = 0;
	foreach($row->find('td') as $cell) 
	{
		if($cell->getAttribute('class')=='table_txt')
		{
			$k = $k+1;
			if($cell->innertext=='India')
			{
				$found = true;
			}
			if($found)
			{
				foreach($cell->find('div') as $div) 
				{
					$rowData[] = $div->innertext;
					if($tdCount==$k)
					{
						$rowData[] = $div->innertext;	
					}
				}
			}
			else
			{
				continue 2;
			}
		}
	}
	if(count($rowData)>0)
		$arrData[] = $rowData;
}
$savedFlag	= true;

setOldDateExchangeRates();

$sql = "SELECT
		mst_companies.intId,
		mst_companies.intBaseCurrencyId
		FROM
		mst_companies
		WHERE
		mst_companies.intStatus=1 ";

$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$saved = saveCurrency($row['intId'],$row['intBaseCurrencyId']);
	if(!$saved)
	{
		$savedFlag = false;
	}
}
if(!$savedFlag)
	echo "ERROR";
else
	echo "DONE";

function saveCurrency($company,$baseCurr)
{
	global $db;
	global $arrData;
	$savedStatus		  = true;
	
	$baseCurrCBSLDis      = getCBSLDiscription($baseCurr);
	$DataArr['BCArrData'] = getBaseCurrencyRates($baseCurrCBSLDis);
	
	for($i=0;$i<count($arrData);$i++)
	{
		$currencyId       = getCurrencyId($arrData[$i][0]);

		if($currencyId!='')
		{
			$buyingRate       = round(($arrData[$i][1]/$DataArr['BCArrData']['buyingRateBC']),4);
			$sellingRate      = round(($arrData[$i][2]/$DataArr['BCArrData']['sellingRateBC']),4);
			
			$saved			  = saveData($currencyId,$buyingRate,$sellingRate,$company);
			if(!$saved)
			{
				$savedStatus  = false;
			}
		}
	}
	return $savedStatus;
	
}
function getCBSLDiscription($baseCurr)
{
	global $db;
	
	$sql = "SELECT CBSLCURRENCY
			FROM mst_financecurrency
			WHERE intId='$baseCurr' ";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	
	return $row['CBSLCURRENCY'];
}
function getBaseCurrencyRates($baseCurrCBSLDis)
{
	global $db;
	global $arrData;
	
	for($i=0;$i<count($arrData);$i++)
	{
		if($arrData[$i][0]==$baseCurrCBSLDis)
		{
			$data['buyingRateBC']  = $arrData[$i][1];
			$data['sellingRateBC'] = $arrData[$i][2];
		
			return $data;
		}
	}
		
}
function getCurrencyId($CBSLCurrency)
{
	global $db;
	
	$sql = "SELECT intId
			FROM mst_financecurrency
			WHERE CBSLCURRENCY='$CBSLCurrency' ";
			
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['intId'];
}
function saveData($currencyId,$buyingRate,$sellingRate,$company)
{
	global $db;
	
	$avgRate = round((($buyingRate+$sellingRate)/2),2);
	$sql = "INSERT INTO mst_financeexchangerate 
			(
			intCurrencyId, 
			dtmDate, 
			intCompanyId, 
			dblSellingRate, 
			dblBuying,
			dblExcAvgRate, 
			intCreator, 
			dtmCreateDate
			)
			VALUES
			(
			'$currencyId', 
			CURDATE(), 
			'$company', 
			'$sellingRate', 
			'$buyingRate',
			'$avgRate', 
			'2', 
			now()
			);";
	
	$result = $db->RunQuery($sql);
	return $result;
}
function setOldDateExchangeRates()
{
	global $db;

	$currDate		= date('Y-m-d');	
	$sqlMaxDt 		= "SELECT MAX(dtmDate) as maxDate FROM mst_financeexchangerate"; 
	$resultMxDt 	= $db->RunQuery($sqlMaxDt);
	$rowMxDt 		= mysqli_fetch_array($resultMxDt);
	$maxDate 		= $rowMxDt['maxDate'];
	$start 			= strtotime($currDate);
	$end 			= strtotime($maxDate);
	$days_between 	= ceil(abs($end - $start) / 86400);
	$newFrmDate 	= $maxDate;
	
	if($days_between>1)
	{
		saveOldDateExchange($days_between,$newFrmDate,$maxDate);	
	}	
}
function saveOldDateExchange($days_between,$newFrmDate,$maxDate)
{
	global $db;
	
	for($t=1;$t<$days_between;$t++)
	{
		$date = date_create($newFrmDate);
		date_add($date, date_interval_create_from_date_string('1 days'));
		$nextDt = date_format($date, 'Y-m-d');
		
		$sqlIns = "INSERT INTO mst_financeexchangerate
					(
					intCurrencyId,dtmDate,
					intCompanyId,dblSellingRate,
					dblBuying,dblExcAvgRate,intCreator,
					dtmCreateDate
					)
					(SELECT intCurrencyId, '".$nextDt."', 
					intCompanyId, dblSellingRate, 
					dblBuying,ROUND(((dblSellingRate+dblBuying)/2),4),2, 
					NOW()
					FROM 
					mst_financeexchangerate 
					WHERE dtmDate='$maxDate'
					)";
	
		$result = $db->RunQuery($sqlIns);
		$newFrmDate = $nextDt;	
	}
}
?>
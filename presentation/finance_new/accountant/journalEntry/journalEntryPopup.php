<?php
session_start();
$backwardseperator 		= "../../../../";
$userId 				= $_SESSION['userId'];

include $backwardseperator."dataAccess/Connector.php";
include_once "../../../../class/finance/accountant/journalEntry/cls_journalEntry_get.php";

$obj_fin_journal_get	= new Cls_JournalEntry_Get($db);
$currDate				= date("Y-m-d");
$monthBeforDate		    = date("Y-m-d", strtotime("-2 month"));

$result					= $obj_fin_journal_get->getPopupDetails($currDate,$monthBeforDate);
$juReportId				= 986;

?>
<title>Journal Entry Details</title>

<script src="presentation/finance_new/accountant/journalEntry/journalEntry.js"></script>

</head>
<form id="frmJournalEntryPopup" name="frmJournalEntryPopup" autocomplete="off" method="post">
<div align="center">
<div class="trans_layoutS" style="width:700px">
<div class="trans_text">Journal Entry Details</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr class="normalfnt">
    	<td width="11%">Entry No</td>
    	<td width="29%"><input name="txtPopEntryNo" id="txtPopEntryNo" type="text" style="width:150px" /></td>
    	<td width="15%">Reference No</td>
    	<td width="30%"><input name="txtPopReferenceNo" id="txtPopReferenceNo" type="text" style="width:200px" /></td>
    	<td width="15%" style="text-align:right"><a class="button white small" id="butJESearch" name="butJESearch">Search</a></td>
    </tr>
    <tr class="normalfnt">
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td style="text-align:right">&nbsp;</td>
    </tr>
	<tr class="normalfnt">
	  <td colspan="5"><div style="overflow:scroll;height:400px;" id="divGrid">
	    <table style="100%" class="bordered" id="tblJEPopup" >
	      <thead>
	        <tr>
	          <th width="17%" >Entry No</th>
	          <th width="16%" >Entry Year</th>
	          <th width="22%" >Date</th>
	          <th width="29%" >Reference No</th>
              <th width="9%" >Report</th>
              <th width="7%" >Add</th>
	          </tr>
	        </thead>
	      <tbody>
          <?php
		  while($row = mysqli_fetch_array($result))
		  {
		  ?>
			 <tr id="<?php echo $row['JOURNAL_ENTRY_NO'].'/'.$row['JOURNAL_ENTRY_YEAR']; ?>">
	          <td style="text-align:center"><?php echo $row['JOURNAL_ENTRY_NO']; ?></td>
	          <td style="text-align:center"><?php echo $row['JOURNAL_ENTRY_YEAR']; ?></td>
	          <td style="text-align:center"><?php echo $row['JOURNAL_ENTRY_DATE']; ?></td>
	          <td style="text-align:left"><?php echo ($row['REFERENCE_NO']==''?'&nbsp;':$row['REFERENCE_NO']); ?></td>
              <td style="text-align:center"><a href="?q=<?php echo $juReportId; ?>&SerialNo=<?php echo $row['JOURNAL_ENTRY_NO']; ?>&SerialYear=<?php echo $row['JOURNAL_ENTRY_YEAR']; ?>" target="rptJournalEntry.php">View</a></td>
              <td style="text-align:center"><a class="button green small clsJEAdd" id="butJEAdd" name="butJEAdd">Add</a></td>
	          </tr> 
          <?php
		  }
		  ?>
	      </tbody>
	      </table>
	    </div>
	    </td>	  
	  </tr>
      <tr>
    	<td height="32" colspan="5" >
    		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td align="center" class="tableBorder_allRound"><a class="button white medium" id="butJEClose" name="butJEClose">Close</a></td>
              </tr>
            </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>
</body>
</html>
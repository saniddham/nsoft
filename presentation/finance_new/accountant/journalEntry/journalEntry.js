var basePath	= "presentation/finance_new/accountant/journalEntry/";
var reportId	= 986;
var menuId		= 745;

$(document).ready(function() {
	
 	$('#frmJernalEntry').validationEngine();
	
	$('#frmJernalEntry #butInsertRow').die('click').live('click',addNew);
	$('#frmJernalEntry #butNew').die('click').live('click',New);
	$('#frmJernalEntry #butSave').die('click').live('click',Save);
	$('#frmJernalEntry #butConfirm').die('click').live('click',Confirm);
	$('#frmJernalEntry #butReject').die('click').live('click',Reject);
	$('#frmJernalEntry #butCancel').die('click').live('click',Cancel);
	$('#frmJernalEntry #butReport').die('click').live('click',Report);

	$('#frmJernalEntry #cboCurrency').die('change').live('change',loadExchangeRate);	
	
	$('#frmRptJournalEntry #butRptConfirm').die('click').live('click',ConfirmRpt);
	$('#frmRptJournalEntry #butRptReject').die('click').live('click',RejectRpt);
	$('#frmRptJournalEntry #butRptCancel').die('click').live('click',CancelRpt);
	
	$('#frmJernalEntry #butCopy').die('click').live('click',detailPopup);
	$('#frmJournalEntryPopup #butJESearch').die('click').live('click',searchData);
	$('#frmJournalEntryPopup .clsJEAdd').die('click').live('click',addJournalEntry);
	
	$('#frmJernalEntry #tblMain .delImg').die('click').live('click' ,function(){
		
		$(this).parent().parent().remove();
		CaculateTotalValue();
	});
	
	$('#frmJernalEntry .clsLedgerAc').die('change').live('change',function(){
		
 		var combo	=	getJECombo($(this).parent().parent().find('.clsLedgerAc').val())
		$(this).parent().parent().find('.clsJE').html(combo);
 	});	
	
	$('#frmJernalEntry .clsJE').die('change').live('change',function(){
 			loadBalAmount(this);
	});	

	$('#frmJernalEntry #tblMain input').die('keyup').live('keyup',function(e){
			if($(this).val()=="" || isNaN($(this).val())){
				$(this).val(0);
				$(this).select();
			}
			if($(this).val() > 0){
				if($(this).attr('id')=='txtCredit'){
					$(this).parent().parent().find('.debit').val(0)
				}
				else{
					$(this).parent().parent().find('.credit').val(0)
				}
			}
			CaculateTotalValue();
	});
	

 });

glCombo 	= getGLAccountCombo();
costCenter	= getCostCenterCombo();
JE 			= getJECombo('');

 function loadExchangeRate(){
	 
 	var url 		= basePath+"journalEntry_db.php?RequestType=loadExchangeRate";
	var date 		= $('#frmJernalEntry #txtJournalDate').val();
	var currency 	= $('#frmJernalEntry #cboCurrency').val();
	
 	var data 		= "date="+date;
 	   data	 		+= "&currency="+currency;
	
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		$('#frmJernalEntry #txtExchangeRate').val(json.exchaRate);
  	}
	});		
	 
 }
 
 
function addNew(){
  
	if(!$('#frmJernalEntry').validationEngine('validate')){
		setTimeout("alertx('')",2000);	
		return false;
	}
	
 	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);		
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML  = "<img class=\"delImg mouseover\" width=\"15\" height=\"15\" src=\"images/del.png\">";
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:center');
  	cell.innerHTML 	= "<select id=\"cboAccount\" class=\"clsLedgerAc validate[required]\" style=\"width:100%\" name=\"cboAccount\">"+glCombo+"</select>";
	
	var cell		= row.insertCell(2);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML  = "<input id=\"txtDebit\" class=\"debit validate[custom[number]]\" type=\"textbox\" value=\"0\" style=\"width:80px;text-align:right\">";
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML 	= "<input id=\"txtCredit\" class=\"credit validate[custom[number]]\" type=\"textbox\" value=\"0\" style=\"width:80px;text-align:right\">";
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<textarea id=\"txtRemarks\" class=\"remarks\" rows=\"1\" cols=\"30\"  style=\"height:20px\" name=\"txtRemarks\"></textarea>";

	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<select id=\"cboCostCenter\" class=\"clsCostCenter validate[required]\" style=\"width:100%\" name=\"cboCostCenter\">"+costCenter+"</select>";

	var cell 		= row.insertCell(6);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<select id=\"cboJECombo\" class=\"clsJE\" style=\"width:100%\" name=\"cboJECombo\">"+JE+"</select>";
	
	//deleteRow();
	//loadSettleGL();
	//loadSettleGLFunc();
}

function Report()
{
	if($('#frmJernalEntry #txtSerialNo').val()=="")
	{
		alert("No details available to view report.");
		return;
	}
 	var url  = "?q="+reportId+"&SerialNo="+$('#frmJernalEntry #txtSerialNo').val();
	    url += "&SerialYear="+$('#frmJernalEntry #txtSerialYear').val();
	window.open(url,'rptJournalEntry.php');
}
function Cancel()
{
  	var url  = "?q="+reportId+"&SerialNo="+$('#frmJernalEntry #txtSerialNo').val();
	    url += "&SerialYear="+$('#frmJernalEntry #txtSerialYear').val();
	    url += "&mode=Cancel";
	window.open(url,'rptJournalEntry.php');
}
function Reject()
{
  	var url  = "?q="+reportId+"&SerialNo="+$('#frmJernalEntry #txtSerialNo').val();
	    url += "&SerialYear="+$('#frmJernalEntry #txtSerialYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rptJournalEntry.php');
}
function Confirm()
{
  	var url  = "?q="+reportId+"&SerialNo="+$('#frmJernalEntry #txtSerialNo').val();
	    url += "&SerialYear="+$('#frmJernalEntry #txtSerialYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rptJournalEntry.php');
}


function CaculateTotalValue()
{
	var difference		= 0;
	var creditTotal		= 0;
	var debitTotal		= 0;
	
	$('#frmJernalEntry #tblMain .credit').each(function(){
			creditTotal += parseFloat($(this).val());
			debitTotal   += parseFloat($(this).parent().parent().find('.debit').val());
 	});
	creditTotal	= parseFloat(RoundNumber(creditTotal,4));
	debitTotal	= parseFloat(RoundNumber(debitTotal,4));
	difference	= creditTotal - debitTotal;
	
	$('#frmJernalEntry #txtCreditTotal').val(RoundNumber(creditTotal,4));	
	$('#frmJernalEntry #txtDebitTotal').val(RoundNumber(debitTotal,4));	
	$('#frmJernalEntry #txtDifference').val(RoundNumber(difference,4));
}

 
 
function Save()
{
	if(!IsProcessMonthLocked_date($('#frmJernalEntry #txtJournalDate').val()))
		return;
		
	showWaiting();	
	if(!Validate_save()){
		hideWaiting();
		setTimeout("alertx('')",2000);	
		return;
	}
 
	var errFlag = 0;
	
	var arrh	  = "[";
 			arrh += "{";
			arrh += '"currency":"'+$('#frmJernalEntry #cboCurrency').val()+'",' ;
			arrh += '"date":"'+$('#frmJernalEntry #txtJournalDate').val()+'",';
			arrh += '"refferenceNo":'+URLEncode_json($('#frmJernalEntry #txtRefNo').val())+',';
			arrh += '"serialNo":"'+$('#frmJernalEntry #txtSerialNo').val()+'",' ;
			arrh += '"serialYear":"'+$('#frmJernalEntry #txtSerialYear').val()+'"';
  			arrh +=  '},';
 
 	arrh 		 = arrh.substr(0,arrh.length-1);
	arrh 		+= " ]";
 
	var arr		= "[";
	
	var i=0;
	$('#frmJernalEntry #tblMain .credit').each(function(){
		var obj= this;
		i++;
 		if(($(this).parent().parent().find('.debit').val()!=0) || ($(this).val()!=0))
		{
			arr += "{";
			arr += '"account":"'+$(this).parent().parent().find('.clsLedgerAc').val()+'",' ;
			arr += '"credit":"'+$(this).val()+'",';
			arr += '"debit":"'+$(this).parent().parent().find('.debit').val()+'",';
			arr += '"remarks":'+URLEncode_json($(this).parent().parent().find('.remarks').val())+',';
			arr += '"settelement":'+URLEncode_json($(this).parent().parent().find('.clsJE').val())+',';
			arr += '"costCenter":"'+$(this).parent().parent().find('.clsCostCenter').val()+'"';
  			arr +=  '},';
		}
	});
	
	if(errFlag==1){
		alert(msg);
		return;
	}

	arr 		 = arr.substr(0,arr.length-1);
	arr 		+= " ]";
 
 	var url 	= basePath+"journalEntry_db.php?RequestType=URLSave";
	var data 	= "Header="+arrh;
 		data   += "&Grid="+arr;
 		
	var httpobj = $.ajax({
	url:url,
	data:data,
	type:"post",
	dataType:'json',
	async:false,
	success:function(json)
		{
			$('#frmJernalEntry #butSave').validationEngine('showPrompt', json.msg,json.type );
			if(json.type=='pass')
			{
				$('#frmJernalEntry #txtSerialNo').val(json.serialNo);
				$('#frmJernalEntry #txtSerialYear').val(json.serialYear);
				if(json.permision_confirm==1){
 					$('#frmJernalEntry #butConfirm').css('display', 'inline'); 
				}
			}
		},
	error:function(xhr,status)
		{
			$('#frmJernalEntry #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
		}
	});
	var t = setTimeout("alertx('#butSave')",4000);
	hideWaiting();
}

function alertx(but)
{
	$('#frmJernalEntry '+but).validationEngine('hide')	;
}

function New()
{
	window.location.href = "?q="+menuId;
}

/*function CalculateTax()
{
	var obj		= $(this);
	var url 	= basePath+"invoice_db.php?RequestType=URLCalculateTax";
	var data 	= "TaxId="+$(this).val();
		data   += "&Value="+parseFloat($(this).parent().parent().find('.cls_td_value').html());
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	async:false,
	success:function(json)
	{
		obj.parent().attr('id',json.TaxValue);
		CaculateTotalValue();
	}
	});		
}*/

 
function LoadAutoData(customerPO)
{
	 $('#frmJernalEntry #txtCustomerPONo').val(customerPO);
	 $('#frmJernalEntry #btnSearch').click();
}

function AddNewGL()
{	
	$('#frmJernalEntry #tblGL tbody tr:last').after("<tr>"+$('#frmJernalEntry #tblGL .cls_tr_firstRow').html()+"</tr>");
}

function CalculateGLTotals()
{
	var total	= 0;
	$('#frmJernalEntry #tblGL .cls_td_GLAmount').each(function(){
    	 total	+= isNaN(parseFloat($(this).children().val()))? 0:parseFloat($(this).children().val());
    });
	total	= RoundNumber(total,4,true);
	$('#frmJernalEntry #tblGL .td_totalGLAlloAmount').val(total);
}

function Validate_save()
{
 	var diff=$('#frmJernalEntry #txtDifference').val();
	
	if(!$('#frmJernalEntry').validationEngine('validate')){
		return false;
	}
/*	else if(hasDuplicates(arr_accounts)==true){
		alert('Duplicate Accounts exists');
		return false;
	}
*/	
/*	if($('#frmJernalEntry #txtSerialNo').val()!="")
	{
		$('#frmJernalEntry #butSave').validationEngine('showPrompt','Already this Journal Entry No saved.Please refresh the page','fail');
		var t = setTimeout("alertx()",2000);
		return false;
	}
*/	else if(diff != 0){
		alert('Credit total not tally with Debit total');
		return false;
	}
	else{
 	return true;
	}
}

function CheckAll()
{
	if($(this).is(':checked'))
		$('#frmJernalEntry #tblMain .cls_check').attr('checked','checked');
	else
		$('#frmJernalEntry #tblMain .cls_check').removeAttr('checked');
		
	$('#frmJernalEntry #tblMain .cls_check').each(function(){
		ValidateRowWhenCheck($(this));
	});
}

function ValidateRowWhenCheck(obj)
{
	if(obj.is(':checked'))
	{
		obj.parent().parent().find('.cls_td_invoice').children().attr('disabled','');
	}
	else
	{
		obj.parent().parent().find('.cls_td_invoice').children().attr('disabled','disabled');
	}
	CaculateTotalValue();
}


function getGLAccountCombo(){
	
	var optionss = '';
 	var url 	= basePath+"journalEntry_db.php?RequestType=getGLAccountOptions";
	
	var httpobj = $.ajax({
	url:url,
	data:'',
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		optionss = json.combOptions;
 	}
	});		
	return optionss;
}

function getCostCenterCombo(){
	
	var optionss = '';
 	var url 	= basePath+"journalEntry_db.php?RequestType=getCostCenterOptions";
	
	var httpobj = $.ajax({
	url:url,
	data:'',
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		optionss = json.combOptions;
 	}
	});		
	return optionss;
	
}

function getJECombo(account){
	
	var optionss = '';
 	var url 	= basePath+"journalEntry_db.php?RequestType=getJEOptions";
	var data 	= "account="+account;
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		optionss = json.combOptions;
 	}
	});		
	return optionss;
	
}

function hasDuplicates(array) {
	var valuesSoFar = [];
	for (var i = 0; i < array.length; ++i) {
		var value = array[i];
		if (valuesSoFar.indexOf(value) !== -1) {
			return true;
		}
 	}
	return false;
}	



 function ConfirmRpt(){
	 
	var val = $.prompt('Are you sure you want to approve this Journal Entry ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"journalEntry_db.php"+window.location.search+'&RequestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptJournalEntry #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptJournalEntry #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
					   hideWaiting();
				}});
  }
  
  
  function RejectRpt(){
	  
	var val = $.prompt('Are you sure you want to reject this Journal Entry ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				showWaiting();
				var url = basePath+"journalEntry_db.php"+window.location.search+'&RequestType=reject';
				var obj = $.ajax({
					url:url,
					type:'post',
					dataType: "json",  
					data:'',
					async:false,
					
					success:function(json){
							$('#frmRptJournalEntry #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
								window.location.href = window.location.href;
								window.opener.location.reload();//reload listing page
								return;
							}
						},
					error:function(xhr,status){
							
							$('#frmRptJournalEntry #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);
						}		
					});
				////////////
					}
					hideWaiting();
			}});
	  
  }

 
function CancelRpt(){

	var val = $.prompt('Are you sure you want to cancel this Journal Entry ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
				if(v)
				{
					showWaiting();
					var url = basePath+"journalEntry_db.php"+window.location.search+'&RequestType=cancel';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptJournalEntry #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptJournalEntry #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
						hideWaiting();
 				}});
	
}

function loadBalAmount(obj)
{
 	var url 	= basePath+"journalEntry_db.php?RequestType=loadBalAmount";
	
 	var arrh	  = "[";
 			arrh += "{";
			arrh += '"settelement":'+URLEncode_json($(obj).parent().parent().find('.clsJE').val());
  			arrh +=  '},';
	
  
 	arrh 		 = arrh.substr(0,arrh.length-1);
	arrh 		+= " ]";
	var data 	= "Header="+arrh;
	
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
		balAmount = json.balAmount;
		//obj.parent().parent().find('.debit').attr('class', 'debit validate[custom[number],max['+balAmount+']]');
		//obj.parent().parent().find('.credit').attr('class', 'credit validate[custom[number],max['+balAmount+']]');
 		$(obj).parent().parent().find('.debit').attr('class','debit validate[custom[number],max['+balAmount+']]');
		$(obj).parent().parent().find('.credit').attr('class','credit validate[custom[number],max['+balAmount+']]');
		if(json.trnType=='C'){
 			$(obj).parent().parent().find('.credit').val(0);
			$(obj).parent().parent().find('.credit').attr("disabled", "disabled");
			$(obj).parent().parent().find('.debit').removeAttr("disabled");
		}
		else{
 			$(obj).parent().parent().find('.debit').val(0);
			$(obj).parent().parent().find('.debit').attr("disabled", "disabled");
			$(obj).parent().parent().find('.credit').removeAttr("disabled");
		}
 	}
	});		
 }
 

function detailPopup()
{
 	popupWindow3('1');
	$('#popupContact1').load(basePath+'journalEntryPopup.php',function(){
		
		
		$('#butJEClose').die('click').live('click',disablePopup);
	});
}
function addJournalEntry()
{
	var entryNoArr 	= $(this).parent().parent().attr('id');
	
	var url 		= basePath+"journalEntry_db.php?RequestType=addJournalEntry";
	var data 		= "entryNoArr="+entryNoArr;
	
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				if(json.arrJEData!=null)
				{
					$("#frmJernalEntry #tblMain tr:gt(1)").remove();
					var lengthDetail   	= json.arrJEData.length;
					var arrJEData   	= json.arrJEData;
					
					for(var i=0;i<lengthDetail;i++)
					{
						var glHtml			= arrJEData[i]['GLCombo'];
						var costCenterHtml	= arrJEData[i]['costCenterCombo'];	
						
						createGrid(glHtml,costCenterHtml);
					}
				}
			}
	});
	disablePopup();

}
function createGrid(glHtml,costCenterHtml)
{
	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);		
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML  = "<img class=\"delImg mouseover\" width=\"15\" height=\"15\" src=\"images/del.png\">";
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<select id=\"cboAccount\" class=\"clsLedgerAc validate[required]\" style=\"width:100%\" name=\"cboAccount\">"+glHtml+"</select>";

	
	var cell		= row.insertCell(2);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML  = "<input id=\"txtDebit\" class=\"debit validate[custom[number]]\" type=\"textbox\" value=\"0\" style=\"width:80px;text-align:right\">";
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML 	= "<input id=\"txtCredit\" class=\"credit validate[custom[number]]\" type=\"textbox\" value=\"0\" style=\"width:80px;text-align:right\">";
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<textarea id=\"txtRemarks\" class=\"remarks\" rows=\"1\" cols=\"30\"  style=\"height:20px\" name=\"txtRemarks\"></textarea>";

	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<select id=\"cboCostCenter\" class=\"clsCostCenter validate[required]\" style=\"width:100%\" name=\"cboCostCenter\">"+costCenterHtml+"</select>";

	var cell 		= row.insertCell(6);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<select id=\"cboJECombo\" class=\"clsJE\" style=\"width:100%\" name=\"cboJECombo\"></select>";
	
	$('.clsLedgerAc').change();
}
function searchData()
{
	var entryNo 	= $('#txtPopEntryNo').val();
	var referenceNo = $('#txtPopReferenceNo').val();
	
	var url 		= basePath+"journalEntry_db.php?RequestType=searchData";
	var data 		= "entryNo="+entryNo;
		data	   += "&referenceNo="+referenceNo;
	
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				if(json.arrJESearchData!=null)
				{
					$("#tblJEPopup tr:gt(0)").remove();
					var lengthDetail   	= json.arrJESearchData.length;
					var arrJESearchData   	= json.arrJESearchData;
					
					for(var i=0;i<lengthDetail;i++)
					{
						var entryNo			= arrJESearchData[i]['entryNo'];
						var entryYear		= arrJESearchData[i]['entryYear'];
						var referenceNo		= arrJESearchData[i]['referenceNo'];
						var entryDate		= arrJESearchData[i]['entryDate'];		
						
						createPopuoGrid(entryNo,entryYear,referenceNo,entryDate);
					}
				}
			}
	});
	
}
function createPopuoGrid(entryNo,entryYear,referenceNo,entryDate)
{
	var tbl 		= document.getElementById('tblJEPopup');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);
	row.id			= entryNo+'/'+entryYear;
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML  = entryNo;
	
	var cell		= row.insertCell(1);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML  = entryYear;
	
	var cell		= row.insertCell(2);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML  = entryDate;
	
	var cell		= row.insertCell(3);
	cell.setAttribute("style",'text-align:left');
 	cell.innerHTML  = (referenceNo==''?'&nbsp;':referenceNo);
	
	var cell		= row.insertCell(4);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML  = "<a href='rptJournalEntry.php?SerialNo="+entryNo+"&SerialYear="+entryYear+"' target='rptJournalEntry.php'>View</a>";
	
	var cell		= row.insertCell(5);
	cell.setAttribute("style",'text-align:center');
 	cell.innerHTML  = "<a class='button green small clsJEAdd' id='butJEAdd' name='butJEAdd'>Add</a>";
}
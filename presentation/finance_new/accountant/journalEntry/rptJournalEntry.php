<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once ("class/finance/cls_convert_amount_to_word.php");
include_once ("class/finance/accountant/journalEntry/cls_journalEntry_get.php");
include_once ("class/cls_commonErrorHandeling_get.php");
require_once $_SESSION['ROOT_PATH']."class/finance/cls_common_get.php";
include 	  "include/javascript.html";

$programCode				='P0726';
$session_userId 			= $_SESSION["userId"];
$serialNo					= $_REQUEST["SerialNo"];
$serialYear					= $_REQUEST["SerialYear"];
$mode						= (!isset($_REQUEST['mode'])?'':$_REQUEST['mode']);
$reportId					= 986;

$obj_fin_com				= new Cls_Common_Get($db); 
$obj_commonErr				= new cls_commonErrorHandeling_get($db);
$obj_convert_amount_to_word	= new Cls_Convert_Amount_To_Word($db);
$obj_fin_journal_get		= new Cls_JournalEntry_Get($db);

$header_array 				= $obj_fin_journal_get->get_report_header_array($serialYear,$serialNo);
$detail_result 				= $obj_fin_journal_get->get_report_details_array($serialYear,$serialNo);

$permition_arr				= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_save				= $permition_arr['permision'];
$permition_arr				= $obj_commonErr->get_permision_withApproval_cancel($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_cancel			= $permition_arr['permision'];
$permition_arr				= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_reject			= $permition_arr['permision'];
$permition_arr				= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_confirm			= $permition_arr['permision'];

$locationId					= $header_array["LOCATION_ID"];

if(!isset($_REQUEST["SerialNo"]))
	$jernalDate	= date("Y-m-d");
else
	$jernalDate	= $header_array["JOURNAL_ENTRY_DATE"];
	
$company_Id			= $header_array["COMPANY_ID"];
?>
<head>
<title>Journal Entry Report</title>

<script type="text/javascript" src="presentation/finance_new/accountant/journalEntry/journalEntry.js"></script>

</head>
<body>
<style type="text/css">
.apDiv1 {
	position:absolute;
	left:380px;
	top:100px;
	width:auto;
	height:auto;
	z-index:0;
	opacity:0.1;
}
</style>
<?php
//$header_array["STATUS"]=3;

?>

 <form id="frmRptJournalEntry" name="frmRptJournalEntry" method="post">
  <table width="900" align="center">
    <tr>
      <td><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
      <td class="reportHeader" align="center">Journal Entry</td>
    </tr>
	<?php
		include "presentation/report_approve_status_and_buttons.php"
     ?>
<tr>
      <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td width="14%">Journal No</td>
            <td width="1%">:</td>
            <td width="43%"><?php echo $obj_fin_com->getSerialNo($header_array["SERIAL_NO"],$header_array["SERIAL_DATE"],$company_Id,'RunQuery')//echo $header_array["ADVANCE_NO"]; ?></td>
            <td width="14%">Journal Date</td>
            <td width="2%">:</td>
            <td width="26%"><?php echo $header_array["JOURNAL_ENTRY_DATE"]?></td>
          </tr>
          <tr>
            <td>Reference No</td>
            <td>:</td>
            <td><?php echo $header_array["REFERENCE_NO"]?></td>
            <td>Currency</td>
            <td>:</td>
            <td><?php echo $header_array["CURRENCY"]?></td>
          </tr>
<tr>
            <td>Company</td>
            <td>:</td>
            <td><?php echo $header_array["COMPANY"]?></td>
            <td>Location</td>
            <td>:</td>
            <td><?php echo $header_array["LOCATION"]?></td>
          </tr>        </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td ><table width="100%%" border="0" class="rptBordered" id="tblMain">
          <thead>
            <tr>
              <th width="33%">Account</th>
               <th width="17%">Remarks</th>
              <th width="17%">Cost Center</th>
              <th width="8%">Debit</th>
              <th width="9%">Credit</th>
              <th width="16%">Settlement No</th>
            </tr>
          </thead>
          <tbody>
            <?php
$creditTot	= 0;
$debitTot	= 0;
$diff		= 0;
while($row = mysqli_fetch_array($detail_result))
{ 
$no			=$row["SETTELEMENT_NO"];
$year		=$row["SETTELEMENT_YEAR"];
$link		= "?q=".$reportId."&SerialNo=$no&SerialYear=$year";
?>
        <tr>
          <td class="cls_td_salesOrderNo" nowrap="nowrap"><?php echo $row["CHART_OF_ACCOUNT_NAME"],' - '.$row["accountCode"] ?></td>
          <td><?php echo ($row["REMARKS"]==''?'&nbsp;':$row["REMARKS"]);?></td>
          <td class="cls_td_invoice cls_Subtract" style="text-align:right" nowrap="nowrap"><?php echo $row["COST_CENTER"]?></td>
          <td align="right"><?php if($row['TRANSACTION_TYPE']=='D'){ echo number_format($row["AMOUNT"],2);}else{ echo '&nbsp;';}?></td>
          <td align="right"><?php if($row['TRANSACTION_TYPE']=='C'){ echo number_format($row["AMOUNT"],2);}else{ echo '&nbsp;';}?></td>
         <?php
            if($no!=''){
          ?>
          <td align="center"><a href="<?php echo $link; ?>" target="_blank" ><?php echo $no.'/'.$year; ?></a></td>
         <?php
            }
            else{
          ?>
          <td align="center">&nbsp;</td>
         <?php
            }
          ?>
        </tr>
        <?php
        if($row['TRANSACTION_TYPE']=='C'){
            $creditTot +=$row["AMOUNT"];
        } 
        else{
            $debitTot +=$row["AMOUNT"];
        }
  }
 ?>
<tr bgcolor="#EAEAEA">
          <td colspan="3" class="cls_td_salesOrderNo" align="center" ><b>Total</b></td>
          <td align="right"><b><?php echo number_format($debitTot,2); ?></b></td>
          <td align="right"><b><?php echo number_format($creditTot,2); ?></b></td>
          <td align="right">&nbsp;</td>
        </tr>          
</tbody>
        </table></td>
    </tr>
     <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
    <td>
    <?php
			$creator		= $header_array['CREATOR'];
			$createdDate	= $header_array['CREATED_DATE'];
 			$resultA 		= $obj_fin_journal_get->get_Report_approval_details_result($serialYear,$serialNo);
			include "presentation/report_approvedBy_details.php"
 	?>
    </td>
    </tr>
  </table>
</form>
</body>
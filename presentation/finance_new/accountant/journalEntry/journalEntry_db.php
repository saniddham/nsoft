<?php

session_start();

$session_companyId 	= $_SESSION["headCompanyId"];
//BEGIN - INCLUDE FILES {
include_once  ("../../../../dataAccess/Connector.php");
include_once ("../../../../class/finance/cls_common_get.php");
include_once  ("../../../../class/cls_commonFunctions_get.php");
include_once ("../../../../class/finance/cls_get_gldetails.php");
include_once ("../../../../class/finance/accountant/journalEntry/cls_journalEntry_set.php");
include_once ("../../../../class/finance/accountant/journalEntry/cls_journalEntry_get.php");
 
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {

$obj_fin_com_get		= new Cls_Common_Get($db);
$obj_fin_comfunc_get	= new cls_commonFunctions_get($db);
$obj_GLDetails_get		= new Cls_Get_GLDetails($db);
$obj_fin_journal_set	= new Cls_JournalEntry_Set($db);
$obj_fin_journal_get	= new Cls_JournalEntry_Get($db);

//END 	- CREATE OBJECTS }
$requestType	= $_REQUEST["RequestType"];

if($requestType=="getGLAccountOptions")
{
	$glCombo 				= $obj_GLDetails_get->getGLCombo('JOURNAL_ENTRY','');
	$response['combOptions']=$glCombo;
 	echo json_encode($response);
} 
if($requestType=="getCostCenterOptions")
{
	$costCenter 			= $obj_fin_com_get->getCostCenterCombo('');
	$response['combOptions']=$costCenter;
 	echo json_encode($response);
} 
if($requestType=="getJEOptions")
{
	$account				=$_REQUEST["account"];
	$costCenter 			= $obj_fin_com_get->getJECombo('','','',$account);
	$response['combOptions']=$costCenter;
 	echo json_encode($response);
} 
if($requestType=="loadExchangeRate")
{
	$date					=$_REQUEST["date"];
	$currency				=$_REQUEST["currency"];
	$val		 			= $obj_fin_com_get->get_exchane_rate($currency,$date,$session_companyId,'RunQuery');
	$response['exchaRate']	=$val;
 	echo json_encode($response);
} 
if($requestType=="loadBalAmount")
{
   	$Header				= json_decode($_REQUEST["Header"],true);
 	
	foreach($Header as $array_loop)
	{
		$settelement	= $obj_fin_comfunc_get->replace($array_loop["settelement"]);
	}
	$settelement 		= explode("/", $settelement);
	$settelementYear	= $settelement[0];  
	$settelementNo		= $settelement[1];  
	$settelementOrder	= $settelement[2]; 
	
	$balAmount 		= $obj_fin_journal_get->get_balanceToSetele_jernal_amount($settelementYear,$settelementNo,$settelementOrder,'RunQuery');
	$type 			= $obj_fin_journal_get->get_settle_jernal_type($settelementYear,$settelementNo,$settelementOrder,'RunQuery');
	
	$response['balAmount']	=$balAmount;
	$response['trnType']	=$type;
	
 	echo json_encode($response);
} 


elseif($requestType=="URLSave")
{
   
   	$Header				= json_decode($_REQUEST["Header"],true);
  	$Grid				= json_decode($_REQUEST["Grid"],true);
	
	
	foreach($Header as $array_loop)
	{
 		$serialYear		= $array_loop["serialYear"];
		$serialNo		= $array_loop["serialNo"];
		$date			= $array_loop["date"];
 		$currency		= $array_loop["currency"];
		$refferenceNo	= $obj_fin_comfunc_get->replace($array_loop["refferenceNo"]);
 	}
	$response			= $obj_fin_journal_get->ValidateBeforeSave($serialYear,$serialNo);

	if($response["type"]=='fail')
	{
 		echo json_encode($response);
		return;
	}
 		echo $obj_fin_journal_set->Save($serialYear,$serialNo,$date,$currency,$refferenceNo,$Grid);
 } 
elseif($requestType=="approve")
{
	$serialNo			= $_REQUEST["SerialNo"];
	$serialYear			= $_REQUEST["SerialYear"];
	
	$response			= $obj_fin_journal_get->ValidateBeforeApprove($serialYear,$serialNo);
	
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
 	echo $obj_fin_journal_set->Approve($serialYear,$serialNo);
}
elseif($requestType=="reject")
{
	$serialNo			= $_REQUEST["SerialNo"];
	$serialYear			= $_REQUEST["SerialYear"];
	
	$response			= $obj_fin_journal_get->ValidateBeforeReject($serialYear,$serialNo);
	
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	
	echo $obj_fin_journal_set->Reject($serialYear,$serialNo);
}
elseif($requestType=="cancel")
{
	$serialNo			= $_REQUEST["SerialNo"];
	$serialYear			= $_REQUEST["SerialYear"];
	
	$response			= $obj_fin_journal_get->ValidateBeforeCancel($serialYear,$serialNo);
	
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	
	echo $obj_fin_journal_set->Cancel($serialYear,$serialNo);
}
else if($requestType=='addJournalEntry')
{
	$entryNoArr	= explode('/',$_REQUEST['entryNoArr']);
	$entryNo	= $entryNoArr[0];
	$entryYear	= $entryNoArr[1];
	
	$response['arrJEData']	= $obj_fin_journal_get->loadJournalEntry($entryNo,$entryYear);
	
	echo json_encode($response);
}
else if($requestType=="searchData")
{
	$entryNo		= $_REQUEST['entryNo'];
	$referenceNo	= $_REQUEST['referenceNo'];
	$currDate		= date("Y-m-d");
	$monthBeforDate	= date("Y-m-d", strtotime("-1 month"));
	
	$response['arrJESearchData']	= $obj_fin_journal_get->loadSearchData($entryNo,$referenceNo,$currDate,$monthBeforDate);
	
	echo json_encode($response);
}

?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
try
{
 	$requestType 		= $_REQUEST['requestType']; // request parameter
	$programCode		= 'P0773'; // program code
									
	include_once "class/tables/menupermision.php";			$menupermision 							= new menupermision($db);
	include_once "class/tables/sys_no.php";					$sys_no 								= new sys_no($db);
	include_once "class/cls_dateTime.php";					$cls_dateTime 							= new cls_dateTime($db);
	include_once "class/tables/sys_approvelevels.php";		$sys_approvelevels 						= new sys_approvelevels($db);
	include_once "class/tables/mst_subcategory.php";		$mst_subcategory 						= new mst_subcategory($db);
	include_once "class/tables/mst_item.php";				$mst_item 								= new mst_item($db);
	include_once "class/tables/mst_locations.php";			$mst_locations 							= new mst_locations($db);
	include_once "class/tables/fixed_assets_registry.php";	$fixed_assets_registry 					= new fixed_assets_registry($db);
	include_once "class/tables/fixed_assets_process_log.php";	$fixed_assets_process_log 					= new fixed_assets_process_log($db);
	include_once "class/tables/mst_financeaccountingperiod.php";	$mst_financeaccountingperiod	= new mst_financeaccountingperiod($db);
 
 	
	if($requestType=='process')//load sub category combo
	{
		$db->connect();
		
		$currentDate		= $cls_dateTime->getCurruntDate();
		$currentDateTime	= $cls_dateTime->getCurruntDateTime();
		
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Edit','','');
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
		
		$processed	=0;
			 $arrRow_res	= $fixed_assets_registry->select($cols = 'ID', $join = null, $where = " (LAST_PROCESSED_DATE IS NULL || DATEDIFF(DATE('$currentDate'),DATE(LAST_PROCESSED_DATE)) >0) AND NET_AMOUNT  > 0 AND DATEDIFF(DATE('$currentDate'),DATE(GRN_DATE)) >0 ", $order = null, $limit = null);
		while($row_np=mysqli_fetch_array($arrRow_res)){
			$processed++;
			$serialNo 		= $row_np['ID'];
			$fixed_assets_registry->set($serialNo);
			$type			= $fixed_assets_registry->getMANUAL_FLAG();
			$type_desc		=($fixed_assets_registry->getMANUAL_FLAG()==1?'Manual Fixed Asset':'System GRN Fixed Asset');
			$grnNo			= $fixed_assets_registry->getGRN_NO();
			$grnYear		= $fixed_assets_registry->getGRN_YEAR();
			$item			= $fixed_assets_registry->getITEM_ID();
			$itemSerial		= $fixed_assets_registry->getSERIAL_NO();
			$location		= $fixed_assets_registry->getLOCATION_ID();
			$department		= $fixed_assets_registry->getDEPARTMENT_ID();
			$supplier		= $fixed_assets_registry->getSUPPLIER_ID();
			$purchaseDate	= $fixed_assets_registry->getPURCHASE_DATE();
			$depreciation	= $fixed_assets_registry->getMODIFIED_DEPRECIATE_RATE();
			$itemPrice		= $fixed_assets_registry->getITEM_PRICE();
			$price			= $fixed_assets_registry->getNET_AMOUNT();
			$currency		= $fixed_assets_registry->getCURRENCY();
			$grnDate		= $fixed_assets_registry->getGRN_DATE();
			$warrentyDate	= $fixed_assets_registry->getWARRANTY_END_DATE();
			$status			= $fixed_assets_registry->getSTATUS();
			$lpDate			= $fixed_assets_registry->getLAST_PROCESSED_DATE();
			
			if($grnNo=='')
			$grnNo	= 'NULL';
			if($grnYear=='')
			$grnYear	= 'NULL';
			if($grnDate=='')
			$grnDate	= 'NULL';
			if($lpDate=='' || $lpDate=='NULL')
			$lpDate	= $grnDate;
			

			$res_acc_Period	=$mst_financeaccountingperiod->select($cols = " if(DATEDIFF(DATE('$currentDate'),DATE(dtmStartingDate)) >0,'$currentDate' , DATE(dtmStartingDate)) as startDate, dtmStartingDate ,dtmClosingDate ", $join = null, $where = " DATEDIFF(DATE('$currentDate'),DATE(dtmStartingDate)) >0 AND DATEDIFF(DATE(dtmClosingDate),DATE('$currentDate')) >0 ", $order = null, $limit = null);
 			$res_acc_Period=mysqli_fetch_array($res_acc_Period);
			$days_from_bgn_finyear 	= $cls_dateTime->getDifferenceBetweenTwoDates($res_acc_Period['startDate'],$currentDate);
			$total_days_for_finYear = $cls_dateTime->getDifferenceBetweenTwoDates($res_acc_Period['dtmStartingDate'],$res_acc_Period['dtmClosingDate']);  

			
			if(strtotime($lpDate) < strtotime($res_acc_Period['dtmStartingDate']))
			$lpDate	= $res_acc_Period['dtmStartingDate'];
			
 			$days_from_last_process_date	= $cls_dateTime->getDifferenceBetweenTwoDates($lpDate,$currentDate);

 			$DepAmount		= $price*$depreciation/100*($days_from_last_process_date/$total_days_for_finYear);
			
			$fixed_assets_registry->setLAST_PROCESSED_DATE($currentDateTime);
			$result_arr	= $fixed_assets_registry->commit();
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
			
			//ACCUMILATE_DEPRECIATE_AMOUNT
			$where = " ID = '$serialNo'";
			$data  =array( 'ACCUMILATE_DEPRECIATE_AMOUNT'=> '+'.$DepAmount 
			);
			$result_update_h		= $fixed_assets_registry->upgrade($data,$where);
			if(!$result_update_h['status']) 
				throw new Exception($result_update_h['msg']);
			
			//NET_AMOUNT
			$where = "ID = '$serialNo'";
			$data  =array(	'NET_AMOUNT'=>  '-'.$DepAmount
			);
			$result_update_h		= $fixed_assets_registry->upgrade($data,$where);
			if(!$result_update_h['status']) 
				throw new Exception($result_update_h['msg']);
				$result_arr2	= $fixed_assets_process_log->insertRec($serialNo,$grnNo,$grnYear,$grnDate,$item,$itemSerial,$currency,$itemPrice,$depreciation,$price,$DepAmount,($price-$DepAmount),$currentDateTime,'PROCESS');
				if(!$result_arr2['status'])
					throw new Exception($result_arr2['msg']);
		}
		
		if($processed==0){
			throw new Exception("No records to process");
		}
 
		$db->commit();
		$response['type'] 			= "pass";
		$response['msg'] 			= "Processed Successfully.";
		$response['serialNo']		= $serialNo;
	}
	
}
 catch(Exception $e)
{
	$db->rollback();
	$response['msg'] 	=  $e->getMessage();;
	$response['error'] 	=  $error_handler->jTraceEx($e);
	$response['type'] 	=  'fail';
	$response['sql']	=  $db->getSql();
	$db->disconnect();
}
$db->disconnect();
echo json_encode($response);


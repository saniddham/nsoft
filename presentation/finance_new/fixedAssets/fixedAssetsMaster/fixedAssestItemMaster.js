// JavaScript Document
// JavaScript Document
var basePath	= "presentation/finance_new/fixedAssetsMaster/";
$(document).ready(function(){
	
	try
	{
		//$('#frmFixedAsset').validationEngine();
 	}
	catch(e)
	{
		
	}
 	
	$('#frmFixedAsset').on("keyup keypress", function(e) {
	  var code = e.keyCode || e.which; 
	  if (code  == 13) {  
	    $(document.activeElement).next().focus();             
		//e.preventDefault();
		return false;
	  }
	});
	
	$('#frmFixedAsset #cboMainCategory').die('change').live('change',loadSubCategoryCombo);
	$('#frmFixedAsset #cboSubCategory').die('change').live('change',loadItemsCombo);
	$('#frmFixedAsset #cboItems').die('change').live('change',loadItemDetails);
	
	$('#frmFixedAsset #butSave').die('click').live('click',saveDetails);
	
	$('#frmFixedAsset #butDelete').die('click').live('click',deleteData);
	
	$('#frmFixedAsset #butDownload').die('click').live('click',downlad_excel_format);

 });

  
function loadSubCategoryCombo(){

	var mainCategory = $('#cboMainCategory').val();
	var url 		 = "controller.php?q=771&requestType=loadSubCategoryCombo";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		data:"mainCategory="+mainCategory,
		async:false,
		success:function(json){
				$('#cboSubCategory').html(json.subCategoryCombo);
				$('#cboItems').html('');
		}
	});

}

function loadItemsCombo(){
	
	var subCategory = $('#cboSubCategory').val();
	var url 		= "controller.php?q=771&requestType=loadItemCombo";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		data:"subCategory="+subCategory,
		async:false,
		success:function(json){
				$('#cboItems').html(json.itemsCombo);
 		}
	});
	
}

function loadItemDetails(){

	var items	= $('#cboItems').val();
	var url 	= "controller.php?q=771&requestType=loadItemDetails";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		data:"items="+items,
		async:false,
		success:function(json){
				$('#txtDepriciation').val(json.depriciation);
				$('#txtItemPrice').val(json.price);
				$('#cboCurrency').val(json.currency);
		}
	});
	
}

function saveDetails(){
	
	if(!$('#frmFixedAsset').validationEngine('validate'))
		return false;
 	
	showWaiting();
	
	var data = "requestType=saveData";
	var arrData = "{";
						arrData += '"type":"'+$('#type').html()+'",' ;
						arrData += '"serialNo":"'+$('#serial').html()+'",' ;
						if($('#type').html()==1){//manual entries
						arrData += '"grnNo":"'+$('#txtGrnNo').val()+'",' ;
						arrData += '"grnYear":"'+$('#txtGrnYear').val()+'",' ;
						arrData += '"mainCategory":"'+$('#cboMainCategory').val()+'",' ;
						arrData += '"subCategory":"'+$('#cboSubCategory').val()+'",' ;
						arrData += '"item":"'+$('#cboItems').val()+'",' ;
						//arrData += '"itemSerial":"'+URLEncode_json($('#txtItemSerial').val())+'",' ;
						arrData += '"itemSerial":'+URLEncode_json($('#txtItemSerial').val())+',';
						arrData += '"currency":"'+$('#cboCurrency').val()+'",' ;
						arrData += '"depreciation":"'+$('#txtDepriciation').val()+'",' ;
						arrData += '"itemPrice":"'+$('#txtItemPrice').val()+'",' ;
						arrData += '"warentyEnd":"'+$('#txtwarrentyEnd').val()+'",' ;
						arrData += '"location":"'+$('#cboLocation').val()+'",' ;
						arrData += '"department":"'+$('#cboDepartment').val()+'",' ;
						arrData += '"supplier":"'+$('#cboSupplier').val()+'",' ;
						arrData += '"purchaseDate":"'+$('#txtPurchaseDate').val()+'",' ;
						arrData += '"grnDate":"'+$('#txtGRNDate').val()+'",' ;	
						}
						arrData += '"depreciation":"'+$('#txtDepriciation').val()+'"' ;	
		arrData  += "}";
		data	   	   += "&arrData="+arrData;
		
		var url = "controller.php?q=771";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmFixedAsset #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmFixedAsset #serial').html(json.serialNo);
						$('#frmFixedAsset #serial').html('');
 						hideWaiting();
						var t = setTimeout("alertx("+butSave+")",9999000);
						window.location.href = '?q=771';
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
						$('#frmFixedAsset #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
	
	
	hideWaiting();
}

function deleteData(){
	
 	if($('#serial').html()<=0){
		$('#frmFixedAsset #butDelete').validationEngine('showPrompt', 'No saved data to delete','fail');
		return false;
	}
 	
	var val = $.prompt('Are you sure you want to delete this ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{

			showWaiting();
			
			var data = "requestType=delete";
			var arrData = "{";
								arrData += '"serialNo":"'+$('#serial').html()+'"' ;
				arrData  += "}";
				data	   	   += "&arrData="+arrData;
				
				var url = "controller.php?q=771";
				$.ajax({
						url:url,
						dataType:'json',
						type:'post',
						data:data,
						async:false,
						success:function(json){
							$('#frmFixedAsset #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								var t = setTimeout("alertx("+butDelete+")",3000);
								window.location.href = '?q=771';						//return;
								hideWaiting();
								return;
							}
							else
							{
								hideWaiting();
							}
						},
						error:function(xhr,status){
								
								$('#frmFixedAsset #butDelete').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								hideWaiting();
								return;
						}		
				});
	
	
	hideWaiting();
			}
		}
	});
 }
 
function downlad_excel_format()
{
 	//var url = "presentation/finance_new/fixedAssets/fixedAssetsMaster/fixedAssestBulkTemplate.php";
 	var url = "presentation/finance_new/fixedAssets/fixedAssetsMaster/Fixed_Asset_Upload_format.xls";
	window.open(url)
 
}

  
 
 function alertx(buttom)
{
	$('#frmFixedAsset #'+buttom).validationEngine('hide');
}

function uploadExcelFile(fileName){
	
	alert(fileName);
}
<?php
	if(!is_dir("documents/fixedAssetBulkItem"))
	{
		mkdir("documents/fixedAssetBulkItem", 0777);
	}
	$upload_path = 'documents/fixedAssetBulkItem/';
 	

if($_FILES["file"]["name"]==''){
	$response_upload['type']='fail';
	$response_upload['msg']='Please select a file to upload';
	
}
else if (($_FILES["file"]["size"] < 1000000))//1000KB
  {
  if ($_FILES["file"]["error"] > 0)
    {
   //echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    }
  else
    {
    chmod($_FILES["file"]["tmp_name"], 0777);
 	chmod("documents/fixedAssetBulkItem", 0777 );  
  
    if (file_exists("documents/fixedAssetBulkItem/" . $_FILES["file"]["name"]))
      {
		  if($_FILES["file"]["name"]!=''){
		    unlink("documents/fixedAssetBulkItem/" . $_FILES["file"]["name"]);
      		move_uploaded_file($_FILES["file"]["tmp_name"],"documents/fixedAssetBulkItem/" . $_FILES["file"]["name"]);
			chmod("documents/fixedAssetBulkItem/".$_FILES["file"]["name"], 0777 ); 
			//$response_upload['type']='fail';
			//$response_upload['msg']=$_FILES["file"]["name"] . " already exists. ";
		  }
	  }
    else
      {
      move_uploaded_file($_FILES["file"]["tmp_name"],"documents/fixedAssetBulkItem/" . $_FILES["file"]["name"]);
		
		//file_put_contents($_FILES["file"]["tmp_name"], file_get_contents("documents/fixedAssetBulkItem/" . $_FILES["file"]["name"]));
		//@unlink("documents/fixedAssetBulkItem/" . $_FILES["file"]["name"]);
		
		chmod("documents/fixedAssetBulkItem/".$_FILES["file"]["name"], 0777 ); 
		//chmod("documents/fixedAssetBulkItem/" . $_FILES["file"]["name"], 0666);
      //echo "uploaded successfully.";
      }
    }
	
	//----------------
	$name= $_FILES["file"]["name"];
	if($response_upload['type'] != 'fail')
	$response_upload	=readExcelFile($name);
 	//include "upload_item_write.php";
	//header("Location:upload_item_write.php?name=".$_FILES['file']['name']);	
 	//header("Location:presentation/finance_new/fixedAssets/fixedAssetsMaster/upload_item_write.php?name=".$_FILES['file']['name']);	
  }
else
  {//

	$response_upload['type']='fail';
	$response_upload['msg']='Invalid Size';
  
  }
	
 

function readExcelFile($file){
		global $db;
		global $fixed_assets_registry;
		global $mst_item;
		global $mst_financecurrency;
		global $mst_locations;
		global $mst_department;
		global $mst_supplier;
	
 		require_once("reader.php");
 		$upload_path 	= 'documents/fixedAssetBulkItem/'.$file; //same directory
		$folder			= "fixedAssetBulkItem";
		
		$booSaveStatus	= true;
		$message		= "";
		$arrValus		= NULL;
		$toSaveRecords	=0;
		
		$excel = new Spreadsheet_Excel_Reader();
		$response=$excel->read($upload_path,$file); 
  		if($response['type']=='fail'){ 
			$booSaveStatus	= false;
			$message		= $response['msg'];
		}
 		
 		if($booSaveStatus==true){
		$db->connect();
 		
 		for ($i=2;$i<$excel->sheets[0]['numRows']+1;$i++)
		{
			
			if(trim($excel->sheets[0]['cells'][$i][3]) !='' && $booSaveStatus == true){
				$toSaveRecords++;

				$grnYear	= trim($excel->sheets[0]['cells'][$i][1]);
				if($grnYear=='')
					$grnYear	 = 0; 
			
				$grnNo	= trim($excel->sheets[0]['cells'][$i][2]);
				if($grnNo=='')
					$grnNo		 = 0; 
				
				$item	= trim($excel->sheets[0]['cells'][$i][3]);
				try {
				$item	= $mst_item->setCodeToGetData($item);
				$item	= $mst_item->getintId($item);
				} catch (Exception $e) {
					$message		.= $e->getMessage()." Please check the 'Line No :$i' for Item code.</br>";
					$booSaveStatus  = false; 
				}
				if($item=='')
					$booSaveStatus = false; 
				
				$serial	= trim($excel->sheets[0]['cells'][$i][4]);
				if($serial=='')
					$booSaveStatus = false; 
				
				$price	= trim($excel->sheets[0]['cells'][$i][5]);
				if($price=='')
					$booSaveStatus = false; 
				
				$currency	= trim($excel->sheets[0]['cells'][$i][6]);
				try {
					$currency	= $mst_financecurrency->setCodeToGetData($currency);
					$currency	= $mst_financecurrency->getintId($currency);
				} catch (Exception $e) {
					$message		.= $e->getMessage()." Please check the 'Line No :$i' for Currency code.</br>";
					$booSaveStatus  = false; 
				}
				if($currency=='')
					$booSaveStatus = false; 
				
				$depRate	= trim($excel->sheets[0]['cells'][$i][7]);
				if($depRate=='')
					$booSaveStatus = false; 
				
 				$poDate	= trim($excel->sheets[0]['cells'][$i][8]);
 				$old_date_timestamp = strtotime($poDate);
				$poDate = date('Y-m-d', $old_date_timestamp); 
				if($poDate=='')
					$booSaveStatus = false; 
				
				$grnDate	= trim($excel->sheets[0]['cells'][$i][9]);
 				$old_date_timestamp = strtotime($grnDate);
				$grnDate = date('Y-m-d', $old_date_timestamp); 
				if($grnDate=='')
					$booSaveStatus = false; 
				
				$warrentEndDate	= trim($excel->sheets[0]['cells'][$i][10]);
 				$old_date_timestamp = strtotime($warrentEndDate);
				$warrentEndDate = date('Y-m-d', $old_date_timestamp); 
				if($warrentEndDate=='')
					$booSaveStatus = false; 
				
				$locationId	= trim($excel->sheets[0]['cells'][$i][11]);
				try {
				$locationId	= $mst_locations->setCodeToGetData($locationId);
				$locationId	= $mst_locations->getintId($locationId);
				} catch (Exception $e) {
					$message		.= $e->getMessage()." Please check the 'Line No :$i' for Location code.</br>";
					$booSaveStatus  = false; 
				}
				if($locationId=='')
					$booSaveStatus = false; 
				
				$departmentId	= trim($excel->sheets[0]['cells'][$i][12]);
				try {
				$departmentId	= $mst_department->setCodeToGetData($departmentId);
				$departmentId	= $mst_department->getintId($departmentId);
				} catch (Exception $e) {
					$message		.= $e->getMessage()." Please check the 'Line No :$i' for Department code.</br>";
					$booSaveStatus  = false; 
				}
				if($departmentId=='')
					$booSaveStatus = false; 
				
				$supplierId		= trim($excel->sheets[0]['cells'][$i][13]);
				try {
				$supplierId		= $mst_supplier->setCodeToGetData($supplierId);
				$supplierId		= $mst_supplier->getintId($supplierId);
				} catch (Exception $e) {
					$message		.= $e->getMessage()." Please check the 'Line No :$i' for Supplier code.</br>";
					$booSaveStatus  = false; 
				}
				if($supplierId=='')
					$booSaveStatus = false; 
				
 				
				//$supplierId	= GetSupplierId($excel->sheets[0]['cells'][$i][1]);		
				//$itemId		= GetItemId($excel->sheets[0]['cells'][$i][2]);
				//$price		= $excel->sheets[0]['cells'][$i][3];
	//echo $grnYear.",".$grnNo.",".$item.",".$serial.",".$price.",".$currency.",".$depRate.",".$poDate.",".$grnDate.",".$warrentEndDate.",".$locationId.",".$departmentId.",".$supplierId;
				if($booSaveStatus==false){
					if($message=='')
 					$message		= "Codes do not match.<br/>Please Check the Line No :$i'<br/><br/>";
					
					//continue;
				}
				else
				{
 					$result_arr	=Insert_to_fixed_assets($grnYear,$grnNo,$item,$serial,$price,$currency,$depRate,$poDate,$grnDate,$warrentEndDate,$locationId,$departmentId,$supplierId);
					if(!$result_arr['status']){
						$booSaveStatus	= false;
						$message		= $result_arr['msg']."<br/>Please check the 'Line No :$i'<br/><br/>";;
					}

				}
			}
		}
		}
		
		if($toSaveRecords==0 && $booSaveStatus){
			$booSaveStatus	=false;
			$message		="No records to upload";
		}
		
		if($booSaveStatus==true){
			$db->commit();
			$message	= "Bulk file '".$file."' uploaded successfully";
			$response['type']='pass';
			$response['msg']=$message;
		}
		else{
			$response['type']='fail';
			$response['msg']=$message;
		}
		
		return $response;
}



function Insert_to_fixed_assets($grnYear,$grnNo,$item,$itemSerial,$itemPrice,$currency,$depreciation,$purchaseDate,$grnDate,$warentyEnd,$location,$department,$supplier) {
	
	global $fixed_assets_registry;
	global $cls_dateTime;
	global $sessions;
	global $db;
	
	$result_arr		= $fixed_assets_registry->insertRec($grnNo,$grnYear,$supplier,$item,$itemSerial,"NULL",$warentyEnd,"NULL",$location,$department,"NULL",$purchaseDate,$grnDate,$itemPrice,$depreciation,$depreciation,0,$itemPrice,$currency,1,1,$cls_dateTime->getCurruntDateTime(),$sessions->getUserId()); 
	return $result_arr;
	
}
?>



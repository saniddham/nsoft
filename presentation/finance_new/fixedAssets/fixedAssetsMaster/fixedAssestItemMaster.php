<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
 include_once("class/tables/fixed_assets_registry.php");	$fixed_assets_registry	= new fixed_assets_registry($db);
include_once("class/tables/mst_maincategory.php");			$mst_maincategory		= new mst_maincategory($db);
include_once("class/tables/mst_subcategory.php");			$mst_subcategory		= new mst_subcategory($db);
include_once("class/tables/mst_item.php");					$mst_item				= new mst_item($db);
include_once("class/tables/mst_locations.php");				$mst_locations			= new mst_locations($db);
include_once("class/tables/mst_department.php");			$mst_department			= new mst_department($db);
include_once("class/tables/mst_supplier.php");				$mst_supplier			= new mst_supplier($db);
include_once("class/tables/mst_financecurrency.php");		$mst_financecurrency	= new mst_financecurrency($db);
include_once("class/cls_dateTime.php");						$cls_dateTime			= new cls_dateTime($db);

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	include("fixedAssetBulkUpload.php");
	//uploadFile($_FILES);
}

$type			= 1; //manual
$type_desc		='Manual Fixed Asset';
$disableDesc	='';
$status			='';
$status_desc 	='';

$serialNo		= (!isset($_REQUEST['serialNo'])?'':$_REQUEST['serialNo']);

if($serialNo != '') {
	
	$fixed_assets_registry->set($serialNo);
	$type			=$fixed_assets_registry->getMANUAL_FLAG();
 	$type_desc		=($fixed_assets_registry->getMANUAL_FLAG()==1?'Manual Fixed Asset':'System GRN Fixed Asset');
	$grnNo			= $fixed_assets_registry->getGRN_NO();
	$grnYear		= $fixed_assets_registry->getGRN_YEAR();
	$item			= $fixed_assets_registry->getITEM_ID();
	$mst_item->set($item);
	$itemSerial		= $fixed_assets_registry->getSERIAL_NO();
	$mainCategory	= $mst_item->getintMainCategory();
	$subCategory	= $mst_item->getintSubCategory();
	$location		= $fixed_assets_registry->getLOCATION_ID();
	$department		= $fixed_assets_registry->getDEPARTMENT_ID();
	$supplier		= $fixed_assets_registry->getSUPPLIER_ID();
	$purchaseDate	= $fixed_assets_registry->getPURCHASE_DATE();
	$depreciation	= $fixed_assets_registry->getMODIFIED_DEPRECIATE_RATE();
	//$price			= $fixed_assets_registry->getITEM_PRICE();
	$price			= $fixed_assets_registry->getNET_AMOUNT();
	$currency		= $fixed_assets_registry->getCURRENCY();
	$grnDate		= $fixed_assets_registry->getGRN_DATE();
	$warrentyDate	= $fixed_assets_registry->getWARRANTY_END_DATE();
	$status			= $fixed_assets_registry->getSTATUS();
	
 }
 
	if($type==1)//manual
		$disable_desc	="";
	else//system grn
		$disable_desc	= 'disabled="disabled"';
	
	if($status=='0') 
		$status_desc = '<font color="#FF8080">Deleted Entry</>';
?>

<title>Fixed Asset Master Form</title>

<form id="frmFixedAsset" name="frmFixedAsset" method="post" enctype="multipart/form-data">

<div align="center">
    <div class="trans_layoutD" style="width:800px">
      <div class="trans_text">Fixed Asset Items Master</div>
   			 <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
                 <tr class="normalfnt">
                  <td colspan="7" height="30" class="normalfnt"><div id="assetType" style="background-color:#D9D9FF; font-size:12px; border-radius:5px; padding:2px; " ><?php echo $type_desc; ?></div></td>
                 <td width="1%">&nbsp;</td>
                 </tr>
	           <tr class="normalfnt">
                 <td>&nbsp;</td>
                 <td width="13%">GRN No</td>
                 <td colspan="3"><input type="text" name="txtGrnYear" class="txtbox validate[custom[integer]]" id="txtGrnYear"  style="width:50px;" tabindex="1" <?php echo $disable_desc ; ?> />
                   <input type="text" name="txtGrnNo" class="txtbox validate[custom[integer]]" id="txtGrnNo"  style="width:90px;" tabindex="1" <?php echo $disable_desc ; ?>/></td>
                 <td width="15%"><div id="type" style="display:none"><?php echo $type; ?></div><div id="serial" style="display:none"><?php echo $serialNo; ?></div></td>
                 <td width="34%" align="right"><?php echo $status_desc; ?></td>
                 <td>&nbsp;</td>
                 </tr>
                       <tr class="normalfnt">
                 <td>&nbsp;</td>
                 <td nowrap="nowrap">Main Category <span class="compulsoryRed">*</span></td>
                 <td colspan="3"><select name="cboMainCategory" class="txtbox validate[required]" id="cboMainCategory"  style="width:250px;" tabindex="1" <?php echo $disable_desc ; ?> >
                   <?php
				 $where = " intStatus=1 AND FIXED_ASSET = 1 " ;
				 echo $mst_maincategory->getCombo($mainCategory,$where);
				   ?>
                  </select></td>
                 <td nowrap="nowrap">Location <span class="compulsoryRed">*</span></td>
                 <td><select name="cboLocation" class="txtbox validate[required]" id="cboLocation"  style="width:250px;" tabindex="1" <?php echo $disable_desc ; ?>>
                   <?php
				 $where = " intStatus=1" ;
				 echo $mst_locations->getCombo($location,$where);
				   ?>
                 </select></td>
                 <td>&nbsp;</td>
                 </tr>
                 <tr class="normalfnt">
                 <td>&nbsp;</td>
                 <td nowrap="nowrap">Sub Category <span class="compulsoryRed">*</span></td>
                 <td colspan="3"><select name="cboSubCategory" class="txtbox validate[required]" id="cboSubCategory"  style="width:250px;" tabindex="1" <?php echo $disable_desc ; ?>>
                   <?php
				 $where = " intStatus=1" ;
					if($mainCategory!='')
						$where .= " AND intMainCategory = $mainCategory  ";
					if($serialNo!='')
						echo $mst_subcategory->getCombo($subCategory,$where);
				   ?>
                 </select></td>
                 <td nowrap="nowrap">Department <span class="compulsoryRed">*</span></td>
                 <td><select name="cboDepartment" class="txtbox validate[required]" id="cboDepartment"  style="width:250px;" tabindex="1"  <?php echo $disable_desc ; ?>>
                   <?php
				 $where = " intStatus=1" ;
				 echo $mst_department->getCombo($department,$where);
				   ?>
                 </select></td>
                 <td>&nbsp;</td>
                 </tr>
                 <tr class="normalfnt">
                 <td>&nbsp;</td>
                 <td nowrap="nowrap">Item <span class="compulsoryRed">*</span></td>
                 <td colspan="3"><select name="cboItems" class="txtbox validate[required]" id="cboItems"  style="width:250px;" tabindex="1" <?php echo $disable_desc ; ?> >
                   <?php 
				   $where = " intStatus=1" ;
			 	   if($mainCategory!='')
				   $where .= " AND intMainCategory = $mainCategory  ";
				   if($intSubCategory!='')
				   $where .= " AND intSubCategory = $subCategory  ";
				   
				   if($serialNo!='')
 				   echo $mst_item->getCombo($item,$where);
				   ?>
                 </select></td>
                 <td nowrap="nowrap">Supplier <span class="compulsoryRed">*</span></td>
                 <td><select name="cboSupplier" class="txtbox validate[required]" id="cboSupplier"  style="width:250px;" tabindex="1" <?php echo $disable_desc ; ?>>
                   <?php
				 $where = " intStatus=1" ;
				 echo $mst_supplier->getCombo($supplier,$where);
				   ?>
                 </select></td>
                 <td>&nbsp;</td>
                 </tr>
                <tr class="normalfnt">
                 <td>&nbsp;</td>
                 <td  nowrap="nowrap">Item Serial No <span class="compulsoryRed">*</span></td>
                 <td colspan="3"><input type="text" name="txtItemSerial" class="txtbox validate[required, maxSize[50]]" id="txtItemSerial" style="width:250px;" tabindex="1" value="<?php echo htmlentities($itemSerial); ?>" <?php echo $disable_desc ; ?>/></td>
                 <td  nowrap="nowrap">Purchase Date <span class="compulsoryRed">*</span></td>
                 <td><input name="txtPurchaseDate" type="text" value="<?php if($purchaseDate){ echo substr($purchaseDate,0,10); }?>" class="txtbox validate[required]" id="txtPurchaseDate" style="width:105px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo $disable_desc ; ?> /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                 <td>&nbsp;</td>
                 </tr>
                 <tr class="normalfnt">
                 <td>&nbsp;</td>
                 <td nowrap="nowrap">Depreciation % <span class="compulsoryRed">*</span></td>
                 <td colspan="3"><input type="text" name="txtDepriciation" class="txtbox validate[required, custom[number]]" id="txtDepriciation"  style="width:250px; text-align:right" tabindex="1" value="<?php echo $depreciation; ?>" /></td>
                 <td nowrap="nowrap">GRN Date <span class="compulsoryRed">*</span></td>
                 <td><input name="txtGRNDate" type="text" value="<?php if($grnDate){ echo substr($grnDate,0,10); }?>" class="txtbox validate[required]" id="txtGRNDate" style="width:105px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo $disable_desc ; ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                 <td>&nbsp;</td>
                 </tr>
               <tr class="normalfnt">
                 <td>&nbsp;</td>
                 <td nowrap="nowrap">Item Price <span class="compulsoryRed">*</span></td>
                 <td width="13%"><input type="text" name="txtItemPrice" class="txtbox validate[required, min[0], custom[number]]" id="txtItemPrice"  style="width:105px; text-align:right" tabindex="1"  value="<?php echo round($price,2); ?>" <?php echo $disable_desc ; ?>/></td>
                 <td width="7%"  nowrap="nowrap" >Currency  <span class="compulsoryRed">*</span></td>
                 <td width="18%" align="left"><select name="cboCurrency" class="txtbox validate[required]" id="cboCurrency"  style="width:75px;" tabindex="1" <?php echo $disable_desc ; ?>  >
                   <?php
				 $where = " intStatus=1" ;
				 echo $mst_financecurrency->getCombo($currency,$where);
				   ?>
                 </select></td>
                 <td nowrap="nowrap">Warrant end date <span class="compulsoryRed">*</span></td>
                 <td ><input name="txtwarrentyEnd" type="text" value="<?php if($warrentyDate){ echo substr($warrentyDate,0,10); }?>" class="txtbox validate[required]" id="txtwarrentyEnd" style="width:105px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo $disable_desc ; ?> /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                 <td>&nbsp;</td>
                 </tr>
                <tr class="normalfnt">
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 </tr>
                  <tr class="normalfnt">
                   <td>&nbsp;</td>
                   <td colspan="7" rowspan="2"><fieldset>
                   <legend>Bulk Upload (Xls format only)</legend>
                   <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                     <td width="25%"><a class="button white medium" id="butDownload" name="butDownload">Download Sample Format</a></td>
                     <td width="19%"></td>
                     <td width="8%">&nbsp;</td>
                     <td width="45%" class="normalfnt"><input type="file" name="file" id="file" /><input type="submit" name="button" class="button white medium" id="button" value="Upload Bulk" />
      <?php /*?><a class="button white medium" id="butUpload" name="butUpload">Upload Bulk</a><?php */?>
      <input style="visibility:hidden" name="txtFolder" type="text" id="txtFolder" value="<?php echo $folderName; ?>" /></td>
                     <td width="1%">&nbsp;</td>
                     <td width="1%">&nbsp;</td>
                     <td width="1%">&nbsp;</td>
                   </tr>
                    <tr>
                      <td colspan="4" align="right"><b><?php if($response_upload['type']=='pass'){?><font color="#00BB00"><?php } else{?><font color="#FF8080"><?php }?><?php echo $response_upload['msg']; ?></font></b></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                   </table>
                   </fieldset></td>
                 </tr>
                 <tr>
                 <td height="25" colspan="8"></td>
                 </tr>
               <tr>
                 <td colspan="8">
                 <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            <tr>
            <td align="center" class="tableBorder_allRound">
            <a href="?q=771" class="button white medium" id="butNew" name="butNew">New</a>																		                <a class="button white medium" id="butSave" name="butSave" style="">Save</a>
             <?php if($type==1 && $status==1){ ?><a class="button red medium" id="butDelete" name="butDelete">Delete</a><?php } ?><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
            </tr>
            </table>
                 </td>
                 </tr>
              </table>
     </div>
</div>
</form>
 

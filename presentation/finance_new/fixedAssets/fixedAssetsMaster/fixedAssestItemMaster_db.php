<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
try
{
 	$requestType 		= $_REQUEST['requestType']; // request parameter
	$programCode		= 'P0771'; // program code
									
	include_once "class/tables/menupermision.php";			$menupermision 						= new menupermision($db);
	include_once "class/tables/sys_no.php";					$sys_no 							= new sys_no($db);
	include_once "class/cls_dateTime.php";					$cls_dateTime 						= new cls_dateTime($db);
	include_once "class/tables/sys_approvelevels.php";		$sys_approvelevels 					= new sys_approvelevels($db);
	include_once "class/tables/mst_subcategory.php";		$mst_subcategory 					= new mst_subcategory($db);
	include_once "class/tables/mst_item.php";				$mst_item 							= new mst_item($db);
	include_once "class/tables/mst_locations.php";			$mst_locations 						= new mst_locations($db);
	include_once "class/tables/fixed_assets_registry.php";	$fixed_assets_registry 				= new fixed_assets_registry($db);
	include_once "class/tables/fixed_assets_process_log.php";	$fixed_assets_process_log		= new fixed_assets_process_log($db);
 	
	
	if($requestType=='loadSubCategoryCombo')//load sub category combo
	{
		$db->connect();
		
		$mainCategory				= $_REQUEST['mainCategory'];
		$where 						= " intStatus=1" ;
		if($mainCategory!='')
		$where 						.= " AND intMainCategory = $mainCategory  ";
 		$html						= $mst_subcategory->getCombo(NULL,$where);
		
		$response['subCategoryCombo']	= $html;
	}
	else if($requestType=='loadItemCombo') //load item combo
	{
		$db->connect();
		
		$subCategory				= $_REQUEST['subCategory'];
		$where 						= " intStatus=1" ;
		if($subCategory!='')
		$where 						.= " AND intSubCategory = $subCategory  ";
 		$html						= $mst_item->getCombo(NULL,$where);
		
		$response['itemsCombo']	= $html;
	}
	else if($requestType=='loadItemDetails') //load item's details
	{
		$db->connect();
		
		$item						= $_REQUEST['items'];
  		$mst_item->set($item);
		
		$response['depriciation']	= $mst_item->getDEPRECIATION_RATE();
		$response['price']			= $mst_item->getdblLastPrice();
		$response['currency']		= $mst_item->getintCurrency();
	}
	
	
	
	else if($requestType=='saveData')
	{
		$arrData 		= json_decode($_REQUEST['arrData'],true);
  		
		$type			= $arrData['type'];//this always should be equal to 1
		$serialNo		= $arrData['serialNo'];
/*		$grnNo			= ($arrData['grnNo']==''?"NULL":$arrData['grnNo']);
		$grnYear		= ($arrData['grnYear']==''?"NULL":$arrData['grnYear']);
*/		$grnNo			= ($arrData['grnNo']==''?0:$arrData['grnNo']);
		$grnYear		= ($arrData['grnYear']==''?0:$arrData['grnYear']);
		$mainCategory	= $arrData['mainCategory'];
		$subCategory	= $arrData['subCategory'];
		$item			= $arrData['item'];
		$itemSerial		= $arrData['itemSerial'];
		$currency		= $arrData['currency'];
		$depreciation	= $arrData['depreciation'];
		$itemPrice		= $arrData['itemPrice'];
		$warentyEnd		= $arrData['warentyEnd'];
		$location		= $arrData['location'];
		$department		= $arrData['department'];
		$supplier		= $arrData['supplier'];
		$purchaseDate	= $arrData['purchaseDate'];
		$grnDate		= $arrData['grnDate'];
		
		$editMode		= false;
		$detailStatus	= false;
		
 		$db->connect(); 
 		
 		//check save or update
		if($serialNo=='')
		{
			//check save permission	
			$menupermision->set($programCode,$sessions->getUserId());
			$permissionArr	= $menupermision->checkMenuPermision('Edit','','');
			
			if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
				
   		
			$status			= 1; 
			//check for duplicate entry(Because GRN NO is NULL, MYSQL don't recognize it when occure dupplicate)
		/*	$where			= " fixed_assets_registry.GRN_NO = '$grnNo' AND fixed_assets_registry.GRN_YEAR = '$grnYear' AND fixed_assets_registry.ITEM_ID = '$item' AND fixed_assets_registry.SERIAL_NO = '$itemSerial'
";
			$result_dup		= $fixed_assets_registry->select($cols = '*', $join = null, $where, $order = null, $limit = null);
			$row			= mysqli_fetch_array($result_dup);

			if($row['GRN_NO'] != '')
				throw new Exception("Duplicate Entry.");*/
			
			// save header data
			$result_arr		= $fixed_assets_registry->insertRec($grnNo,$grnYear,$supplier,$item,$itemSerial,"NULL",$warentyEnd,"NULL",$location,$department,"NULL",$purchaseDate,$grnDate,$itemPrice,$depreciation,$depreciation,0,$itemPrice,$currency,$type,$status,$cls_dateTime->getCurruntDateTime(),$sessions->getUserId());
			$serialNo		= $result_arr['insertId'];
 			
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
			else{
 				$result_arr2=$fixed_assets_process_log->insertRec($serialNo,$grnNo,$grnYear,$grnDate,$item,$itemSerial,$currency,$itemPrice,$depreciation,$itemPrice,0,$itemPrice,'NULL','MANUAL_FIXED_ASSET');
				if(!$result_arr2['status'])
					throw new Exception($result_arr2['msg']);
				}
		}
		else
		{
			// set  values
			$fixed_assets_registry->set($serialNo);
			//get current status
  			
			//check location with saved location when edit
			
			//if($fixed_assets_registry->getLOCATION_ID()!=$sessions->getLocationId())
				//throw new Exception("This is not saved location");
			
			$menupermision->set($programCode,$sessions->getUserId());
			$permissionArr	= $menupermision->checkMenuPermision('Edit','','');
			
			//if($fixed_assets_registry->getLAST_PROCESSED_DATE())
				//throw new Exception("Process already raised.Can't edit");
			
			if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
				
 			$status				= 1;
			
			/*$where			= " fixed_assets_registry.GRN_NO = '$grnNo' AND fixed_assets_registry.GRN_YEAR = '$grnYear' AND fixed_assets_registry.ITEM_ID = '$item' AND fixed_assets_registry.SERIAL_NO = '$itemSerial' AND fixed_assets_registry.ID <> '$serialNo'
";
			$result_dup		= $db->select($cols = '*', $join = null, $where = null, $order = null, $limit = null);
			if($result_dup)
				throw new Exception("Duplicate Entry.");*/
			
 			if($type==1){
				$fixed_assets_registry->set($serialNo);
				$fixed_assets_registry->setGRN_NO($grnNo);
				$fixed_assets_registry->setGRN_YEAR($grnYear);
				$fixed_assets_registry->setSUPPLIER_ID($supplier);
				$fixed_assets_registry->setITEM_ID($item);
				$fixed_assets_registry->setSERIAL_NO($itemSerial);
				$fixed_assets_registry->setWARRANTY_MONTHS("NULL");
				$fixed_assets_registry->setWARRANTY_END_DATE($warentyEnd);
				$fixed_assets_registry->setASSET_CODE("NULL");
				$fixed_assets_registry->setLOCATION_ID($location);
				$fixed_assets_registry->setDEPARTMENT_ID($department);
				$fixed_assets_registry->setSUB_DEPARTMENT_ID("NULL");
				$fixed_assets_registry->setPURCHASE_DATE($purchaseDate);
				$fixed_assets_registry->setGRN_DATE($grnDate);
				$fixed_assets_registry->setITEM_PRICE($itemPrice);
				//$fixed_assets_registry->setACCUMILATE_DEPRECIATE_AMOUNT();
				//$fixed_assets_registry->setNET_AMOUNT($printType);
				$fixed_assets_registry->setCURRENCY($currency);
				$fixed_assets_registry->setMANUAL_FLAG($type);
				$fixed_assets_registry->setSTATUS($status);
				//$fixed_assets_registry->setDEPRECIATE_RATE($depreciation);
				$fixed_assets_registry->setMODIFIED_DEPRECIATE_RATE($depreciation);
			
				$result_arr	= $fixed_assets_registry->commit();
				if(!$result_arr['status'])
					throw new Exception($result_arr['msg']);
					
				$editMode			= true;
			
				$result_arr2	= $fixed_assets_process_log->insertRec($serialNo,$grnNo,$grnYear,$grnDate,$item,$itemSerial,$currency,$itemPrice,$depreciation,$itemPrice,0,$itemPrice,'NULL','MANUAL_FIXED_ASSET');
				if(!$result_arr2['status'])
					throw new Exception($result_arr2['msg']);
				
			}
			
			
		}
		
		

		
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode)
			$response['msg'] 		= "Updated Successfully.";
		else
			$response['msg'] 		= "Saved Successfully.";
			
		$response['serialNo']		= $serialNo;
 	}
	else if($requestType=='delete'){
		
		$arrData 		= json_decode($_REQUEST['arrData'],true);
  		$serialNo		= $arrData['serialNo'];
 		
		$db->connect(); //open connection.
 			
		//$result_arr = $fixed_assets_registry->delete(" ID = $serialNo ");
		//if(!$result_arr['status'])
		//	throw new Exception($result_arr['msg']);
		$fixed_assets_registry->set($serialNo);
		$fixed_assets_registry->setSTATUS(0);
		$result_arr	= $fixed_assets_registry->commit();
		
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
	
		$db->commit();
		$response['type'] 			= "pass";
		$response['msg'] 			= "Deleted Successfully.";
	}
	
	else if($requestType=='upload'){

		//print_r($_FILES["file"]);
		
		require_once("reader.php");

 		//if (!empty($_FILES['file1'])) 
		if($_FILES["file"]["size"]!=0)
		{	
		
			if(!is_dir("documents/fixedAssetBulkItem"))
			{
				mkdir("documents/fixedAssetBulkItem", 0700);
			}
			$upload_path = 'documents/fixedAssetBulkItem/';
			if ($_FILES['file']['error'] == 0) 
			{
				$file = explode(".", $_FILES['file']['name']);
				move_uploaded_file($_FILES['file']['tmp_name'], $upload_path.'/'.$_FILES['file']['name']);
			} 
			header("Location:presentation/finance_new/fixedAssets/fixedAssetsMaster/upload_item_write.php?name=".$_FILES['file']['name']."&Type=S");	
		}
	}
	
 	else if($requestType=='upload1'){
			$data = array();
			 
			if(isset($_GET['files']))
			{
				$error = false;
				$files = array();
				 
				$uploaddir = '../../../../documents/fixedAssetBulkItem/';
				foreach($_FILES as $file)
				{
					if(move_uploaded_file($file['tmp_name'], $uploaddir .basename($file['name'])))
					{
						$files[] = $uploaddir .$file['name'];
					}
					else
					{
						$error = true;
					}
				}
				$data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $files);
			}
			else
			{
				$data = array('success' => 'Form was submitted', 'formData' => $_POST);
			}
			 
			echo json_encode($data);		
	}
		
	
 }
catch(Exception $e)
{
	$db->rollback();
	$response['msg'] 	=  $e->getMessage();;
	$response['error'] 	=  $error_handler->jTraceEx($e);
	$response['type'] 	=  'fail';
	$response['sql']	=  $db->getSql();
	$db->disconnect();
}
$db->disconnect();
echo json_encode($response);


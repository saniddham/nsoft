<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$session_locationId = $sessions->getLocationId();
$session_companyId 	= $sessions->getCompanyId();
$intUser  			= $sessions->getUserId();
$hrDb				= $sessions->getHrDatabase();

$programCode		= 'P0771';

include_once 		"class/tables/ink_actual_production_header.php";	$ink_actual_production_header = new ink_actual_production_header($db);
include_once 		"libraries/jqgrid2/inc/jqgrid_dist.php";

$select				= "MAX(APPROVE_LEVELS) AS MAX_LEVEL";
$header_result		= $ink_actual_production_header->select($select,NULL,NULL,NULL,NULL);
$header_array		= mysqli_fetch_array($header_result);
$approveLevel 		= $header_array['MAX_LEVEL'];

///BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
$arr =  json_decode($_REQUEST['filters'],true);

$arr = $arr['rules'];

$where_string = '';
$where_array = array(
					'Status'=>'APH.STATUS',
					'CONCAT_SERIAL_NO'=>"CONCAT(APH.SERIAL_NO,' / ',APH.SERIAL_YEAR)",
					'printType'=>"APH.RE_PRINT",
					'GROUP_NAME'=>"APH.GROUP_ID",
					'RAISED_BY'=>'sys_users.strUserName',
					'CREATED_DATE'=>'DATE(CREATED_DATE)'
					);
$arr_status 	= array('Approved'=>'1','Rejected'=>'0','Revised'=>'-1','Cancelled'=>'-2','Pending'=>'2');
$arr_printType  = array('Re Print'=>'1','Normal'=>'0');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($v['field']=='printType')
	{
		$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_printType[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0)					 
		$where_string .= "AND DATE(CREATED_DATE) = '".date('Y-m-d')."'";
//END }

$sql = "select * from(SELECT 
		fixed_assets_registry.ID									AS ID,
		IF(fixed_assets_registry.STATUS=1,'Approved',if(fixed_assets_registry.STATUS=0,'Deleted','')) AS STATUS,
		IF(fixed_assets_registry.GRN_NO >0 ,CONCAT(fixed_assets_registry.GRN_NO,'/',fixed_assets_registry.GRN_YEAR), '') AS GRN_NO,
		IF(fixed_assets_registry.MANUAL_FLAG=1,'Manual','System') 	AS TYPE,
		fixed_assets_registry.MANUAL_FLAG							AS MANUAL_FLAG,
		mst_supplier.strName 										AS SUPPLIER,
		mst_supplier.intId 											AS SUPPLIER_ID,
		mst_item.strName											AS ITEM,
		fixed_assets_registry.SERIAL_NO								AS SERIAL_NO,
		fixed_assets_registry.WARRANTY_MONTHS 						AS WARRANTY_MONTHS,
		fixed_assets_registry.WARRANTY_END_DATE						AS WARRANTY_END_DATE,
		fixed_assets_registry.ASSET_CODE							AS ASSET_CODE,
		mst_locations.strName										AS LOCATION,
		mst_locations.intId											AS LOCATION_ID,
		mst_department.strName										AS DEPARTMENT,
		mst_department.intId										AS DEPARTMENT_ID,
		fixed_assets_registry.PURCHASE_DATE							AS PURCHASE_DATE,
		fixed_assets_registry.GRN_DATE								AS GRN_DATE,
		ROUND(fixed_assets_registry.ITEM_PRICE,2)					AS ITEM_PRICE,
		fixed_assets_registry.DEPRECIATE_RATE						AS DEPRECIATE_RATE,
		fixed_assets_registry.MODIFIED_DEPRECIATE_RATE				AS MODIFIED_DEPRECIATE_RATE,
		fixed_assets_registry.LAST_PROCESSED_DATE					AS LAST_PROCESSED_DATE,
		ROUND(fixed_assets_registry.ACCUMILATE_DEPRECIATE_AMOUNT,2)			AS ACCUMILATE_DEPRECIATE_AMOUNT,
		ROUND(fixed_assets_registry.NET_AMOUNT,2)							AS NET_AMOUNT,
		mst_financecurrency.strDescription							AS CURRENCY,
 		fixed_assets_registry.CREATED_DATE							AS CREATED_DATE,
		fixed_assets_registry.CREATED_BY							AS CREATED_BY 
		FROM
		fixed_assets_registry
		INNER JOIN mst_supplier ON fixed_assets_registry.SUPPLIER_ID = mst_supplier.intId
		INNER JOIN mst_locations ON fixed_assets_registry.LOCATION_ID = mst_locations.intId
		INNER JOIN mst_department ON fixed_assets_registry.DEPARTMENT_ID = mst_department.intId
		INNER JOIN mst_financecurrency ON fixed_assets_registry.CURRENCY = mst_financecurrency.intId
		INNER JOIN mst_item ON fixed_assets_registry.ITEM_ID = mst_item.intId

				$where_string
		)  
		AS SUB_1 WHERE 1=1";
//echo $sql;
$jq = new jqgrid('',$db);	

$cols	= array();
$col	= array();

$formLink					= "?q=771&serialNo={ID}";	 
$col["title"] 				= "ID";
$col["name"] 				= "ID";	
$col["width"] 				= "2"; 						
$col["align"] 				= "left";
$col['link']	= $formLink;	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$col["hidden"]  			= false;				
$cols[] 					= $col;	
$col						= NULL;


$col["title"] 				= "Status";
$col["name"] 				= "STATUS";
$col["width"] 				= "2"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["stype"] 				= "select";
$str 						= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;	
$col						= NULL;



$col["title"] 				= "GRN No";
$col["name"] 				= "GRN_NO";	
$col["width"] 				= "3"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= false;				
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Item";
$col["name"] 				= "ITEM";
$col["width"] 				= "3"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= false;					
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Item Serial";
$col["name"] 				= "SERIAL_NO";
$col["width"] 				= "3"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= false;					
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Price";
$col["name"] 				= "ITEM_PRICE";
$col["width"] 				= "2"; 						
$col["align"] 				= "right";
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= false;					
$cols[] 					= $col;	
$col						= NULL;


$col["title"] 				= "Currency";
$col["name"] 				= "CURRENCY";
$col["width"] 				= "2"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$cols[] 					= $col;	
$col						= NULL;
 
$col["title"] 				= "Supplier";
$col["name"] 				= "SUPPLIER";
$col["width"] 				= "3"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "SUPPLIER_ID";
$client_lookup 				= $jq->get_dropdown_values("SELECT S.intId AS k,S.strName AS v
														FROM mst_supplier as S
 														WHERE
														S.intStatus = 1 ORDER BY S.strName ASC");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;
 
$col["title"] 				= "Department";
$col["name"] 				= "DEPARTMENT";
$col["width"] 				= "3"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "DEPARTMENT_ID";
$client_lookup 				= $jq->get_dropdown_values("SELECT S.intId AS k,S.strName AS v
														FROM mst_department as S
 														WHERE
														S.intStatus = 1 ORDER BY S.strName ASC");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;
 
$col["title"] 				= "Location";
$col["name"] 				= "LOCATION";
$col["width"] 				= "3"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "LOCATION_ID";
$client_lookup 				= $jq->get_dropdown_values("SELECT S.intId AS k,S.strName AS v
														FROM mst_locations as S
 														WHERE
														S.intStatus = 1 and S.intCompanyId=$session_companyId ORDER BY S.strName ASC");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;

 
$col["title"] 				= "PO Date";
$col["name"] 				= "PURCHASE_DATE";
$col["width"] 				= "2"; 						
$col["align"] 				= "center";
$col["sortable"] 			= true; 						
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "GRN Date";
$col["name"] 				= "GRN_DATE";
$col["width"] 				= "2"; 						
$col["align"] 				= "center";
$col["sortable"] 			= true; 						
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Depreciation %";
$col["name"] 				= "DEPRECIATE_RATE";
$col["width"] 				= "2"; 						
$col["align"] 				= "right";
$col["sortable"] 			= true; 						
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Modified Dep %";
$col["name"] 				= "MODIFIED_DEPRECIATE_RATE";
$col["width"] 				= "2"; 						
$col["align"] 				= "right";
$col["sortable"] 			= true; 						
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Tot Depreciate Amount";
$col["name"] 				= "ACCUMILATE_DEPRECIATE_AMOUNT";
$col["width"] 				= "2"; 						
$col["align"] 				= "right";
$col["sortable"] 			= true; 						
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Amount";
$col["name"] 				= "NET_AMOUNT";
$col["width"] 				= "2"; 						
$col["align"] 				= "right";
$col["sortable"] 			= true; 						
$cols[] 					= $col;	
$col						= NULL;



$col["title"] 				= "Type";
$col["name"] 				= "TYPE";
$col["width"] 				= "2"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$cols[] 					= $col;	
$col						= NULL;


$grid["caption"] 			= "Fixed asset items Listing";
$grid["multiselect"] 		= false;
$grid["rowNum"] 			= 20; 
$grid["sortname"] 			= 'ID'; 
$grid["sortorder"] 			= "DESC"; 
$grid["autowidth"] 			= true; 
$grid["multiselect"] 		= false; 
$grid["search"] 			= true; 
$grid["postData"] 			= array("filters" => $sarr ); 
$grid["export"] 			= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Fixed asset items Listing</title>
<?php
echo $out;

?>
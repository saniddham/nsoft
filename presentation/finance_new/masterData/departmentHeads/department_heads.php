<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
#BEGIN 	- INCLUDE FILES {
include_once "class/tables/mst_department_heads.php";				$mst_department_heads				= new mst_department_heads($db);
require_once 'class/tables/mst_locations.php';						$mst_locations						= new mst_locations($db);
require_once 'class/tables/mst_department.php';						$mst_department						= new mst_department($db);
require_once 'class/tables/sys_users.php';							$sys_users							= new sys_users($db);
#END 	- INCLUDE FILES }
?>
<title>Department Heads Approvals</title>

<form id="frmDepartmentHeads" name="frmDepartmentHeads" method="post">
  <div align="center">
    <div class="trans_layoutS" style="width:600px">
      <div class="trans_text">Department Heads Approvals</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr class="normalfnt">
          <td width="69%">&nbsp;</td>
        </tr>
        <tr class="normalfnt">
          <td height="16"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="22%">Location <span class="compulsoryRed">*</span></td>
                <td width="62%"><select name="cboLocation" id="cboLocation" style="width:350px">
                    <?php
                echo $mst_locations->getCombo($_REQUEST['cboLocation'],'intCompanyId='.$sessions->getCompanyId());
				?>
                  </select></td>
                <td width="16%"><a class="button green small" id="butSearch">Search</a></td>
              </tr>
            </table></td>
        </tr>
        <tr class="normalfnt">
          <td height="16">&nbsp;</td>
        </tr>
        <tr class="normalfnt">
          <td height="16"><table width="100%" border="0" class="bordered" id="tblMain">
              <tr class="normalfnt">
                <th width="50%">Department</th>
                <th width="50%">Department Head</th>
              </tr>
              <?php
			  $result = $mst_department->getDepartments();
			  while($row = mysqli_fetch_array($result))
			  {
				 	if($mst_department_heads->checkAvailable($_REQUEST['cboLocation'],$row['ID']))
					{
				  		$mst_department_heads->set($_REQUEST['cboLocation'],$row['ID']);
						$u = $mst_department_heads->getDEP_HEAD_ID();
				  	}
					else
						$u = '';	
			  ?>
              <tr>
                <td class="cls_td_dept" id="<?php echo $row['ID']?>"><?php echo $row['NAME']?></td>
                <td class="cls_td_user"><select name="cboUser" id="cboUser" style="width:100%" class="cls_user" >
                  <?php
                echo $sys_users->getLocationWiseCombo($u,$_REQUEST['cboLocation']);
				?>
                </select></td>
              </tr>
              <?php
			  }
			  ?>
            </table></td>
        </tr>
        <tr class="normalfnt">
          <td>&nbsp;</td>
        </tr>
        <tr class="normalfnt">
          <td >&nbsp;</td>
        </tr>
      </table>
    </div>
  </div>
</form>

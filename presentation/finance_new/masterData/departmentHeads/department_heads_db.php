<?php
try{
	
#BEGIN 	- INCLUDE FILES {
include_once "class/tables/mst_department_heads.php";							$mst_department_heads							= new mst_department_heads($db);
#END 	- INCLUDE FILES }

	$requestType 	= $_REQUEST['requestType'];
	
	if($requestType=='URLUpdate')
	{
		$locId		= $_REQUEST['LocationId'];
		$deptId		= $_REQUEST['DepId'];
		$userId		= ($_REQUEST['UserId']==''?'null':$_REQUEST['UserId']);
		
		if($mst_department_heads->checkAvailable($locId,$deptId))
		{
			$mst_department_heads->set($locId,$deptId);
			$mst_department_heads->setDEP_HEAD_ID($userId);
			$result = $mst_department_heads->commit();
			if(!$result['status'])
				throw new Exception($resulte['msg']);
		}
		else
		{
			$result = $mst_department_heads->insertRec($locId,$deptId,$userId);
			if(!$result['status'])
				throw new Exception($resulte['msg']);
		}
		
		$response['type'] 		= 'pass';
		$response['msg']		= "Updated successfully.";
		echo json_encode($response);
		$db->commit();

#######################################	
## CLOSE CONNECTION      			 ##
#######################################	
		$db->disconnect();
	}
}
catch(Exception $e)
{
	switch ($e->getCode()){
		case 0:
			$db->rollback();		
			$response['msg'] 	=  $e->getMessage();
			$response['error'] 	=  $error_handler->jTraceEx($e);
			$response['type'] 	=  'fail';
			$response['sql']	=  $db->getSql();
			break;		
	}
	echo json_encode($response);
}
?>
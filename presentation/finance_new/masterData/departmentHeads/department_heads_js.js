$(document).ready(function(e) {
	$('#frmDepartmentHeads #cboLocation').die('change').live('change',pageSubmit);
	$('#frmDepartmentHeads #butSearch').die('click').live('click',pageSubmit);
  	$('#frmDepartmentHeads #tblMain .cls_user').die('change').live('change',update);
});

function pageSubmit()
{
	$('#frmDepartmentHeads').submit();
}

function update()
{
	var url   = "controller.php?q=1074";

	var data  = 'requestType=URLUpdate';
		data += '&LocationId='+$('#frmDepartmentHeads #cboLocation').val();
	    data += '&DepId='+$(this).parent().parent().find('.cls_td_dept').attr('id');
		data += '&UserId='+$(this).val();
	
	$.ajax({
	url:url,
	data:data,
	dataType:'json',
	type:'POST',
	async:false,
	success:function(json)
	{
	}
	});	
}
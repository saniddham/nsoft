<?php 
 session_start();
$backwardseperator 		= "../../../../";
$userId 				= $_SESSION['userId'];
$locationId	  			= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];

$requestType 			= $_REQUEST['requestType'];
$programCode			= 'P0781';
$savedMasseged			= "";
$error_sql				= "";
$savedStatus			= true;
$editMode				= false;

include_once "../../../../dataAccess/Connector.php";
include_once "../../../../class/finance/masterData/bank/cls_bank_set.php";
include_once "../../../../class/finance/masterData/bank/cls_bank_get.php";
include_once "../../../../class/cls_commonFunctions_get.php";

$obj_bank_set			= new Cls_Bank_Set($db);
$obj_bank_get			= new Cls_Bank_Get($db);
$obj_common				= new cls_commonFunctions_get($db);

if($requestType=='saveData')
{
	$arrHeader 		= json_decode($_REQUEST['arrHeader'],true);
	
	$searchId		= $arrHeader['searchId'];
	$bankCode		= $obj_common->replace($arrHeader["bankCode"]);
	$bankName		= $obj_common->replace($arrHeader["bankName"]);
	$country		= $arrHeader['country'];
	$status			= $arrHeader['status'];
	
	$db->begin();
	
	$savePermission = $obj_common->Load_menupermision2($programCode,$userId,'intAdd');
	if($savePermission==0 && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged  = 'No permission to save.';
	}
	if($searchId=='')
	{
		$dataArr		= $obj_bank_set->save($bankCode,$bankName,$country,$status,$userId);
		if($dataArr['savedStatus']=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= $dataArr['savedMassege'];
			$error_sql		= $dataArr['error_sql'];
		}
	}
	else
	{
		$editMode		= true;
		$dataArr		= $obj_bank_set->update($searchId,$bankCode,$bankName,$country,$status,$userId);
		if($dataArr['savedStatus']=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= $dataArr['savedMassege'];
			$error_sql		= $dataArr['error_sql'];
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		if($editMode)
			$response['msg']	= "Updated Successfully.";
		else
			$response['msg']	= "Saved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
	
}
else if($requestType=='loadCombo')
{
	$result = $obj_bank_get->getSaveBanks();
	
	$html = "<option value=\"\"></option>";
	while($row = mysqli_fetch_array($result))
	{
		$html .= "<option value=\"".$row["BANK_ID"]."\">".$row["BANK_NAME"]."</option>";
	}
	echo $html;
}
else if($requestType=='searchData')
{
	$bankId	 = $_REQUEST['searchId'];
	
	$searchData = $obj_bank_get->getSearchData($bankId);
	
	$response['bankCode'] 	= $searchData['BANK_CODE'];
	$response['bankName'] 	= $searchData['BANK_NAME'];
	$response['status'] 	= ($searchData['STATUS']==1?true:false);
	$response['country'] 	= $searchData['COUNTRY_ID'];
	
	echo json_encode($response);
}
else if($requestType=='delete')
{
	$bankId	 = $_REQUEST['bankId'];
	
	$db->begin();
	$deletePermission = $obj_common->Load_menupermision2($programCode,$userId,'intDelete');
	if($deletePermission==0 && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged  = 'No permission to delete.';
	}
	
	$dataArr		= $obj_bank_set->delete($bankId);
	if($dataArr['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= $dataArr['savedMassege'];
		$error_sql		= $dataArr['error_sql'];
	}
	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg']		= "Deleted Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
	
}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once  "class/finance/masterData/bank/cls_bank_get.php";
include_once  "class/cls_commonFunctions_get.php";

$obj_bank_get		= new Cls_Bank_Get($db);
$obj_comfunc_get	= new cls_commonFunctions_get($db);

$country_result		= $obj_comfunc_get->getCountry('RunQuery');
$search_result		= $obj_bank_get->getSaveBanks();
?>
<title>Bank</title>
<?php
	//include 		"include/javascript.html";
?>
<!--<script type="text/javascript" src="presentation/finance_new/masterData/bank/bank_js.js"></script>  -->

<form id="frmBank" name="frmBank" method="post">
<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text">Bank</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td width="46" class="normalfnt">&nbsp;</td>
                <td width="158" class="normalfnt">Bank</td>
                <td colspan="2">
                <select name="cboSearch" class="txtbox" id="cboSearch"  style="width:275px;"  >
                <option value=""></option>
                <?php
					while($row = mysqli_fetch_array($search_result))
					{
						echo "<option value=\"".$row["BANK_ID"]."\">".$row["BANK_NAME"]."</option>";
					}
				?>
                </select>				
                </td>                
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Bank Code&nbsp;<span class="compulsoryRed">*</span></td>
                <td width="142"><input  name="txtBankCode" type="text" class="validate[required,maxSize[10]]" id="txtBankCode" style="width:140px"  tabindex="1"/></td>
                <td width="209" class="normalfnt">&nbsp;</td>
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Bank Name&nbsp;<span class="compulsoryRed">*</span></td>
                <td colspan="2"><input name="txtBankName" type="text" class="validate[required,maxSize[100]]" id="txtBankName" style="width:275px" tabindex="2"/></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Country&nbsp;<span class="compulsoryRed">*</span></td>
                <td colspan="2"><select name="cboCountry" class="txtbox validate[required]" id="cboCountry"  style="width:275px;"  >
                <option value=""></option>
                <?php
               		while($row = mysqli_fetch_array($country_result))
					{
						echo "<option value=\"".$row["intCountryID"]."\">".$row["strCountryName"]."</option>";
					}
                ?>
                </select></td>
              </tr>
              <tr>
                <td rowspan="2" class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Active</td>
                <td><input type="checkbox" name="chkActive" id="chkActive" checked="checked" tabindex="4"/></td>
                <td class="normalfnt">&nbsp;</td>
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td></td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($form_permision['edit']==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');echo($form_permision['delete']==1?'<a class="button white medium" id="butDelete" name="butDelete">Delete</a>':'');?><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>

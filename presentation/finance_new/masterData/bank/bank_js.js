// JavaScript Document
var basePath	= "presentation/finance_new/masterData/bank/";
$(document).ready(function(){
	
	$("#frmBank").validationEngine();
	
	/*if(intAddx)
	{
		$('#frmBank #butNew').show();
		$('#frmBank #butSave').show();
	}
	if(intEditx)
	{
		$('#frmBank #butSave').show();
	}
	if(intDeletex)
	{
		$('#frmBank #butDelete').show();
	}*/
	
	$('#frmBank #butSave').die('click').live('click',saveData);
	$('#frmBank #cboSearch').die('change').live('change',searchData);
	$('#frmBank #butNew').die('click').live('click',clearAll);
	$('#frmBank #butDelete').die('click').live('click',function(){deleteData()});

});
function saveData()
{
	var searchId	= $('#frmBank #cboSearch').val();
	var bankCode	= $('#frmBank #txtBankCode').val();
	var bankName	= $('#frmBank #txtBankName').val();
	var country		= $('#frmBank #cboCountry').val();
	var status		= ($('#frmBank #chkActive').attr('checked')?1:0);
	
	if($('#frmBank').validationEngine('validate'))
	{
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"searchId":"'+searchId+'",' ;
							arrHeader += '"bankCode":'+URLEncode_json(bankCode)+',';
							arrHeader += '"bankName":'+URLEncode_json(bankName)+',';
							arrHeader += '"country":"'+country+'",' ;
							arrHeader += '"status":"'+status+'"' ;
		
			arrHeader += "}";
		
		data+="&arrHeader="+arrHeader;
		var url = basePath+"bank_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmBank #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmBank').get(0).reset();
						loadSearchCombo();
						var t = setTimeout("alertx()",1000);
						return;
					}
					else
					{
						
					}
				},
				error:function(xhr,status){
						
						$('#frmBank #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						return;
				}		
		});
	}
}
function searchData()
{
	var searchId		= $('#frmBank #cboSearch').val();
	if(searchId=='')
	{
		clearAll();
		return;
	}
	
	var data = "requestType=searchData";
	data	+= "&searchId="+searchId;
	
	var url  = basePath+"bank_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					
					$('#frmBank #txtBankCode').val(json.bankCode);
					$('#frmBank #txtBankName').val(json.bankName);
					$('#frmBank #cboCountry').val(json.country);
					$('#frmBank #chkActive').attr('checked',json.status);
				}
		});
}
function deleteData()
{
	var bankId	 = $('#cboSearch').val();
	if(bankId=='')
	{
		$('#frmBank #butDelete').validationEngine('showPrompt', 'Please select Bank.', 'fail');
		return;
	}
	var val = $.prompt('Are you sure you want to delete "'+$('#frmBank #cboSearch option:selected').text()+'" ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						var url = basePath+"bank_db.php";
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							type: 'post',
							data:'requestType=delete&bankId='+bankId,
							async:false,
							success:function(json){
								
								$('#frmBank #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									$('#frmBank').get(0).reset();
									loadSearchCombo();
									var t=setTimeout("alertDelete()",1000);
									return;
								}	
							}	 
						});
					}
					}
	});	
}
function loadSearchCombo()
{
	var url 	= basePath+"bank_db.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmBank #cboSearch').html(httpobj.responseText);
}
function clearAll()
{
	$('#frmBank').get(0).reset();
	loadSearchCombo();
}
function alertx()
{
	$('#frmBank #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmBank #butDelete').validationEngine('hide')	;
}
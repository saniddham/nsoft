// JavaScript Document
var basePath		= "presentation/finance_new/masterData/chartOfAccounts/";
var chartOfAccCode 	= '';
$(document).ready(function(){
	
	$("#frmChartOfAccount").validationEngine();
	
	/*if(intAddx)
	{
		$('#frmChartOfAccount #butNew').show();
		$('#frmChartOfAccount #butSave').show();
	}
	//permision for edit 
	if(intEditx)
	{
		$('#frmChartOfAccount #butSave').show();
	}
	
	//permision for delete
	if(intDeletex)
	{
		$('#frmChartOfAccount #butDelete').show();
	}
	
	//permision for view
	if(intViewx)
	{
		$('#frmChartOfAccount #cboSearch').removeAttr('disabled');
	}*/
	
	$('#frmChartOfAccount #cboFinanceType').die('change').live('change',loadMainType);
	$('#frmChartOfAccount #cboMainType').die('change').live('change',loadSubType);
	$('#frmChartOfAccount #cboSubType').die('change').live('change',loadChartOfAccCode);
	$('#frmChartOfAccount #chkAll').die('click').live('click',checkAll);
	$('#frmChartOfAccount #butSave').die('click').live('click',saveData);
	$('#frmChartOfAccount #butNew').die('click').live('click',clearAll);
	$('#frmChartOfAccount #cboSearch').die('change').live('change',loadData);
	$('#frmChartOfAccount #chkBank').die('click').live('click',showBankDetail);

});
function loadMainType()
{
	clearCombo('financeType');
	if($(this).val()=='')
	{
		return;
	}
	var url 	= basePath+"chartOfAccount-db.php?requestType=loadMainType";
	var data 	= "financeType="+$(this).val();
	var obj 	= $.ajax({
						url:url,
						dataType:'json',  
						data:data,
						async:false,
						success:function(json){
								
								$('#frmChartOfAccount #cboMainType').html(json.mainTypeCombo);
						},
						error:function(xhr,status){
							
						}		
					});
	
}
function loadSubType()
{
	clearCombo('mainType');
	if($(this).val()=='')
	{
		return;
	}
	var url 	= basePath+"chartOfAccount-db.php?requestType=loadSubType";
	var data 	= "mainType="+$(this).val();
	var obj 	= $.ajax({
						url:url,
						dataType:'json',  
						data:data,
						async:false,
						success:function(json){
								
								$('#frmChartOfAccount #cboSubType').html(json.subTypeCombo);
						},
						error:function(xhr,status){
							
						}		
					});
}
function loadChartOfAccCode()
{
	clearCombo('subType');
	if($(this).val()=='')
	{
		return;
	}
	var url 	= basePath+"chartOfAccount-db.php?requestType=loadChartOfAccCode";
	var data 	= "subType="+$(this).val();
	var obj 	= $.ajax({
						url:url,
						dataType:'json',  
						data:data,
						async:false,
						success:function(json){

								$('#frmChartOfAccount #txtChartOfAccountCode').val(json.newAccountCode);
						},
						error:function(xhr,status){
							
						}		
					});
}
function clearCombo(type)
{
	switch(type)
	{
		case 'financeType' :
			$('#frmChartOfAccount #cboMainType').html('');
			$('#frmChartOfAccount #cboSubType').html('');
			$('#frmChartOfAccount #txtChartOfAccountCode').val('');	
		break;
		
		case 'mainType' :
			$('#frmChartOfAccount #cboSubType').html('');
			$('#frmChartOfAccount #txtChartOfAccountCode').val('');	
		break;
		
		case 'subType' :
			$('#frmChartOfAccount #txtChartOfAccountCode').val('');	
		break;
	}
}
function checkAll()
{
	if($(this).attr('checked'))
	{
		$('#frmChartOfAccount .clsChkCompany').attr("checked",true);
	}
	else
	{
		$('#frmChartOfAccount .clsChkCompany').attr("checked",false);
	}
}
function saveData()
{
	showWaiting();
	var searchId 		= $('#frmChartOfAccount #cboSearch').val();
	var financeType 	= $('#frmChartOfAccount #cboFinanceType').val();
	var mainType 		= $('#frmChartOfAccount #cboMainType').val();
	var subType 		= $('#frmChartOfAccount #cboSubType').val();
	var accountCode 	= $('#frmChartOfAccount #txtChartOfAccountCode').val();
	var accountName 	= $('#frmChartOfAccount #txtChartOfAccountName').val();
	var bank	 		= ($('#frmChartOfAccount #chkBank').attr('checked')?1:0);
	var bankId 			= $('#frmChartOfAccount #cboBank').val();
	var currency 		= $('#frmChartOfAccount #cboCurrency').val();
	var active	 		= ($('#frmChartOfAccount #chkActive').attr('checked')?1:0);
	
	if($('#frmChartOfAccount').validationEngine('validate'))	
	{
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"searchId":"'+searchId+'",' ;
							arrHeader += '"financeType":"'+financeType+'",' ;
							arrHeader += '"mainType":"'+mainType+'",' ;
							arrHeader += '"subType":"'+subType+'",' ;
							arrHeader += '"accountCode":"'+accountCode+'",' ;
							arrHeader += '"accountName":'+URLEncode_json(accountName)+',' ;
							arrHeader += '"bank":"'+bank+'",' ;
							arrHeader += '"bankId":"'+bankId+'",' ;
							arrHeader += '"currency":"'+currency+'",' ;
							arrHeader += '"active":"'+ active +'"' ;
			arrHeader += ' }';
		
		var arrTransacDetails	= "";
		$('.clsChkTransacType').each(function(){

				var chartOfAccField = $(this).parent().parent().attr('id');
				var chkStatus		= ($(this).attr('checked')?1:0);
				
				if(chartOfAccField!='')
				{
					arrTransacDetails += "{";
					arrTransacDetails += '"chartOfAccField":"'+ chartOfAccField +'",' ;
					arrTransacDetails += '"chkStatus":"'+ chkStatus +'"' ;
					arrTransacDetails +=  '},';
				}
		});
		
		var arrCompanyDetails	= "";
		$('#frmChartOfAccount #tblCompany .clsChkCompany').each(function(){
			
			if($(this).attr('checked'))
			{
				var companyId = $(this).parent().parent().attr('id');
				
				arrCompanyDetails += "{";
				arrCompanyDetails += '"companyId":"'+ companyId +'"' ;
				arrCompanyDetails +=  '},';
			}
		});
		
		arrTransacDetails 		= arrTransacDetails.substr(0,arrTransacDetails.length-1);
		arrCompanyDetails 		= arrCompanyDetails.substr(0,arrCompanyDetails.length-1);
		
		var arrHeader			= arrHeader;
		var arrTransacDetails	= '['+arrTransacDetails+']';
		var arrCompanyDetails	= '['+arrCompanyDetails+']';
		
		data+="&arrHeader="+arrHeader+"&arrTransacDetails="+arrTransacDetails+"&arrCompanyDetails="+arrCompanyDetails;
		
		var url = basePath+"chartOfAccount-db.php";
			$.ajax({
					url:url,
					dataType:'json',
					type:'post',
					data:data,
					async:false,
					success:function(json){
						$('#frmChartOfAccount #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							var t = setTimeout("alertx()",3000);
							loadSearchCombo();
							clearAll();
							hideWaiting();
							return;
						}
						else
						{
							hideWaiting();
						}
					},
					error:function(xhr,status){
							
							$('#frmChartOfAccount #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							hideWaiting();
							return;
					}		
			});
	}
	else
	{
		hideWaiting();
	}
}
function loadData()
{
	
	var searchId = $(this).val();
	clearAll();
	
	if(searchId=='')
	{
		return;
		hideWaiting();
	}
	showWaiting();
	$(this).val(searchId);
	var url 	= basePath+"chartOfAccount-db.php?requestType=loadData";
	var data 	= "searchId="+searchId;
	var obj 	= $.ajax({
						url:url,
						dataType:'json',  
						data:data,
						async:false,
						success:function(json){

								var length  = json.DETAILS.length;
								var details = json.DETAILS;
								var header 	= json.HEADER;
								var bank 	= json.BANK_DETAILS;
	
								$('#frmChartOfAccount #cboFinanceType').val(header['financeType']);
								$('#frmChartOfAccount #cboFinanceType').change();
								
								$('#frmChartOfAccount #cboMainType').val(header['mainType']);
								$('#frmChartOfAccount #cboMainType').change();
								
								$('#frmChartOfAccount #cboSubType').val(header['subType']);
								$('#frmChartOfAccount #txtChartOfAccountCode').val(header['accountCode']);
								$('#frmChartOfAccount #txtChartOfAccountName').val(header['accountName']);
								
								if(header["bank"]=='1')
								{
									$('#frmChartOfAccount #chkBank').attr('checked',true);
									$('#frmChartOfAccount .tr_bankDetail').show();
									$('#frmChartOfAccount #cboBank').val(bank['bank_id']);
									$('#frmChartOfAccount #cboCurrency').val(bank['currency_id']);
									$('#frmChartOfAccount #cboBank').addClass('validate[required]');
									$('#frmChartOfAccount #cboCurrency').addClass('validate[required]');
									
								}
								else
								{
									$('#frmChartOfAccount #chkBank').attr('checked',false);
									$('#frmChartOfAccount .tr_bankDetail').hide();
									$('#frmChartOfAccount #cboBank').removeClass('validate[required]');
									$('#frmChartOfAccount #cboCurrency').removeClass('validate[required]');
								}
								
								if(header["active"]=='1')
									$('#frmChartOfAccount #chkActive').attr('checked',true);
								else
									$('#frmChartOfAccount #chkActive').attr('checked',false);
									
								
								for(var i=0;i<length;i++)
								{
									if(details[i]["STATUS"]=='1')
										$('.'+details[i]["FIELD_NAME"]).attr('checked',true);
									else
										$('.'+details[i]["FIELD_NAME"]).attr('checked',false);
								}
								
								var lengthCompany  = json.COMPANY.length;
								var detailsCompany = json.COMPANY;
								
								for(var j=0;j<lengthCompany;j++)
								{
									$('#frmChartOfAccount #tblCompany .clsChkCompany').each(function(){
										
										var companyId = $(this).parent().parent().attr('id');
										
										if(companyId==detailsCompany[j]['companyId'])
											$(this).attr('checked',true);
									});
								}
								hideWaiting();
						},
						error:function(xhr,status){
							
							hideWaiting();
							
						}		
					});
	
}
function loadSearchCombo()
{
	var url 	= basePath+"chartOfAccount-db.php";
	var data 	= "requestType=loadSearchCombo";
	var obj 	= $.ajax({
						url:url,
						dataType:'json',  
						data:data,
						async:false,
						success:function(json){

								$('#cboSearch').html(json.searchCombo);
						},
						error:function(xhr,status){
							
						}		
					});
}
function showBankDetail()
{
	if($(this).attr('checked'))
	{
		$('#frmChartOfAccount .tr_bankDetail').show();
		$('#frmChartOfAccount #cboBank').addClass('validate[required]');
		$('#frmChartOfAccount #cboCurrency').addClass('validate[required]');
	}
	else
	{
		$('#frmChartOfAccount .tr_bankDetail').hide();
		$('#frmChartOfAccount #cboBank').removeClass('validate[required]');
		$('#frmChartOfAccount #cboCurrency').removeClass('validate[required]');
	}
}
function clearAll()
{
	$('#frmChartOfAccount')[0].reset();
	$('#frmChartOfAccount #cboMainType').html('');
	$('#frmChartOfAccount #cboSubType').html('');
	$('#frmChartOfAccount .clsChkCompany').attr('checked',false);
	$('#frmChartOfAccount .clsChkTransacType').attr('checked',false);
	if(!$('#frmChartOfAccount #chkBank').attr('checked'))
		$('#frmChartOfAccount .tr_bankDetail').hide();
}
function alertx()
{
	$('#frmChartOfAccount #butSave').validationEngine('hide')	;
}
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId				= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$userId 				= $_SESSION['userId'];

include  	 "class/finance/masterData/chartOfAccount/cls_chartofaccount_get.php";
include_once "class/finance/cls_common_get.php";

$obj_chartofaccount_get	= new Cls_ChartOfAccount_Get($db);
$obj_common_get			= new Cls_Common_Get($db);
?>

<title>Chart of Accounts</title>
<?php
	//include 		"include/javascript.html";
?>
<!--<script type="text/javascript" src="presentation/finance_new/masterData/chartOfAccounts/chartOfAccount-js.js"></script>-->

<form id="frmChartOfAccount" name="frmChartOfAccount" method="post">
<div align="center">
<div class="trans_layoutS" style="width:600px">
<div class="trans_text">Chart of Accounts</div>
	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    	<tr>
        	<td>
            	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
                	<tr class="normalfnt">
                	  <td width="4%">&nbsp;</td>
                	  <td width="29%">Chart of Account Name</td>
                	  <td><select name="cboSearch" id="cboSearch" style="width:100%">
                     	<?php
							echo $obj_chartofaccount_get->loadSearchCombo('');
						?>
              	      </select></td>
                	  <td width="3%">&nbsp;</td>
           	      </tr>
                	<tr class="normalfnt">
                	  <td height="16">&nbsp;</td>
                	  <td>&nbsp;</td>
                	  <td>&nbsp;</td>
                	  <td>&nbsp;</td>
              	  </tr>
                	<tr class="normalfnt">
                	  <td height="16">&nbsp;</td>
                	  <td colspan="2"><table width="100%" border="0" class="tableBorder">
                	    <tr class="normalfnt">
                	      <td><table width="100%" border="0">
                	        <tr class="normalfnt">
                	          <td width="31%">Finance Type <span class="compulsoryRed">*</span></td>
                	          <td colspan="2"><select name="cboFinanceType" id="cboFinanceType" style="width:120px" class="validate[required]">
                	            <?php
							echo $obj_chartofaccount_get->loadFinanceTypeCombo();
						?>
              	            </select></td>
              	          </tr>
                	        <tr class="normalfnt">
                	          <td>Main Type <span class="compulsoryRed">*</span></td>
                	          <td colspan="2"><select name="cboMainType" id="cboMainType" style="width:80%" class="validate[required]">
              	            </select></td>
              	          </tr>
                	        <tr class="normalfnt">
                	          <td >Sub Type <span class="compulsoryRed">*</span></td>
                	          <td colspan="2"><select name="cboSubType" id="cboSubType" style="width:80%" class="validate[required]">
              	            </select></td>
              	          </tr class="normalfnt">
                	        <tr class="normalfnt">
                	          <td>Chart of Account Code <span class="compulsoryRed">*</span></td>
                	          <td colspan="2"><input type="text" name="txtChartOfAccountCode" id="txtChartOfAccountCode" class="validate[required]" style="width:120px" disabled="disabled" /></td>
              	          </tr>
                	        <tr class="normalfnt">
                	          <td>Chart of Account Name <span class="compulsoryRed">*</span></td>
                	          <td colspan="2"><input type="text" name="txtChartOfAccountName" id="txtChartOfAccountName" class="validate[required]" style="width:80%" maxlength="255"/></td>
              	          </tr>
                          <?php
								$rowCount 		= $obj_chartofaccount_get->getTransacTypeCount();
								$halfRwCount	= round(($rowCount/2),0);
						  ?>
                	        <tr class="normalfnt">
                	          <td valign="top">&nbsp;</td>
                	          <td valign="top">&nbsp;</td>
                	          <td valign="top">&nbsp;</td>
              	          </tr>
                	        <tr class="normalfnt">
                	          <td valign="top">Transaction Type</td>
                	          <td width="34%" valign="top"><table width="100%" border="0">
                      <?php
						$result	= $obj_chartofaccount_get->getTransacTypeDetail(0,$halfRwCount);
						while($row = mysqli_fetch_array($result))
						{
					  ?>
                	    <tr class="normalfnt" id="<?php echo $row['CHART_OF_ACCOUNT_FIELD']; ?>">
                	      <td width="11%" style="text-align:left"><input type="checkbox" name="chkTransacType" id="chkTransacType" class="clsChkTransacType <?php echo $row['CHART_OF_ACCOUNT_FIELD']; ?>" /></td>
                	      <td width="89%"><?php echo $row['TYPE_NAME']; ?></td>
               	        </tr>
                       <?php
						}
					   ?>
              	    </table></td>
                	  <td width="35%" valign="top"><table width="100%" border="0">
                      <?php
						$result			= $obj_chartofaccount_get->getTransacTypeDetail($halfRwCount,$rowCount);
						while($row = mysqli_fetch_array($result))
						{
					  ?>
                	    <tr class="normalfnt" id="<?php echo $row['CHART_OF_ACCOUNT_FIELD']; ?>">
                	      <td width="11%" style="text-align:left"><input type="checkbox" name="chkTransacType" id="chkTransacType" class="clsChkTransacType <?php echo $row['CHART_OF_ACCOUNT_FIELD']; ?>" /></td>
                	      <td width="89%"><?php echo $row['TYPE_NAME']; ?></td>
               	        </tr>
                        <?php
						}
						?>
              	    </table></td>
              	          </tr>
                	        <tr class="normalfnt">
                	          <td >Bank</td>
                	          <td ><table width="100%" border="0">
                	            <tr>
                	              <td><input type="checkbox" name="chkBank" id="chkBank" /></td>
              	              </tr>
              	            </table></td>
                	          <td valign="top">&nbsp;</td>
              	          </tr>
                	        <tr class="normalfnt tr_bankDetail" style="display:none">
                	          <td >Bank Name <span class="compulsoryRed">*</span></td>
                	          <td colspan="2" ><select name="cboBank" id="cboBank" style="width:80%">
                              <?php
                                	echo $obj_common_get->getBankCombo('');
                                ?>
              	           	</select></td>
               	            </tr>
                	        <tr class="normalfnt tr_bankDetail" style="display:none">
                                <td >Currency <span class="compulsoryRed">*</span></td>
                                <td ><select name="cboCurrency" id="cboCurrency" style="width:120px">
                                <?php
                                	echo $obj_common_get->getCurrencyCombo('');
                                ?>
                                </select></td>
                	          <td valign="top">&nbsp;</td>
              	          </tr>
                	        <tr class="normalfnt">
                	          <td>Active</td>
                	          <td ><table width="100%" border="0">
                	            <tr>
                	              <td><input type="checkbox" name="chkActive" id="chkActive" checked="checked" /></td>
              	              </tr>
              	            </table></td>
                	          <td valign="top">&nbsp;</td>
              	          </tr>
              	          </table></td>
              	      </tr>
              	    </table></td>
                	  <td>&nbsp;</td>
           	      </tr>
                    <tr class="normalfnt">
                        <td>&nbsp;</td>
                        <td colspan="2">
                            <table width="100%" id="tblCompany" class="bordered">
                                <tr class="normalfnt">
                                    <th width="6%"><input name="chkAll" type="checkbox" id="chkAll"/></th>
                                    <th width="23%">Company Code</th>
                                    <th width="71%">Company Name</th>
                                </tr>
                                <?php
								$result_Company = $obj_chartofaccount_get->getCompanyDetails();
								while($row = mysqli_fetch_array($result_Company))
								{
								?>
                                    <tr class="normalfnt" id="<?php echo $row['intId']; ?>">
                                        <td width="6%"  style="text-align:center"><input name="chkCompany" type="checkbox" id="chkCompany" class="clsChkCompany validate[minCheckbox[1]]"/></d>
                                        <td width="23%" style="text-align:center"><?php echo $row['strCode']; ?></td>
                                        <td width="71%" style="text-align:left"><?php echo $row['strName']; ?></td>
                                    </tr>
								<?php
                                }
                                ?>
                            </table>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                        <td>&nbsp;</td>
                        <td colspan="2" height="32">
                            <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                <tr>
                                	<td align="center" class="tableBorder_allRound"><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($form_permision['edit']==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');?><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                                </tr>
                            </table>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    
              </table>
        </td>
    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</div>
</form>
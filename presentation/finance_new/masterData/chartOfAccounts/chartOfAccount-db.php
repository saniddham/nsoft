<?php 
session_start();
$backwardseperator 		= "../../../../";
$userId 				= $_SESSION['userId'];
$locationId	  			= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];

$requestType 			= $_REQUEST['requestType'];
$programCode			= 'P0739';

include  "../../../../dataAccess/Connector.php";
include  "../../../../class/finance/masterData/chartOfAccount/cls_chartofaccount_get.php";
include  "../../../../class/finance/masterData/chartOfAccount/cls_chartofaccount_set.php";

$obj_chartofaccount_get	= new Cls_ChartOfAccount_Get($db);
$obj_chartofaccount_set	= new Cls_ChartOfAccount_Set($db);

if($requestType=='loadMainType')
{
	$financeType	= $_REQUEST['financeType'];
	
	$response['mainTypeCombo']	= $obj_chartofaccount_get->loadMainTypeCombo($financeType);
	
	echo json_encode($response);
}
else if($requestType=='loadSubType')
{
	$mainType	= $_REQUEST['mainType'];
	
	$response['subTypeCombo']	= $obj_chartofaccount_get->loadSubTypeCombo($mainType);
	
	echo json_encode($response);
}
else if($requestType=='loadSearchCombo')
{	
	$response['searchCombo']	= $obj_chartofaccount_get->loadSearchCombo('');
	
	echo json_encode($response);
}
else if($requestType=='loadChartOfAccCode')
{
	$subType	= $_REQUEST['subType'];
	
	$response['newAccountCode']	= $obj_chartofaccount_get->getMaxChartOfAccountCode($subType);
	
	echo json_encode($response);
}
else if($requestType=='loadData')
{
	$searchId = $_REQUEST['searchId'];
	
	echo json_encode($obj_chartofaccount_get->loadData($searchId));
}
else if($requestType=='saveData')
{
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	$arrTransacDetails 	= json_decode($_REQUEST['arrTransacDetails'],true);
	$arrCompanyDetails 	= json_decode($_REQUEST['arrCompanyDetails'],true);
	
	echo $obj_chartofaccount_set->save($arrHeader,$arrTransacDetails,$arrCompanyDetails,$programCode);
}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId				= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$userId 				= $_SESSION['userId'];

include_once("class/cls_commonFunctions_get.php");
//include 	 "include/javascript.html";

$obj_common	=	new cls_commonFunctions_get($db);	
?>
<title>Exchange Gain/Loss</title>

<!--<script type="text/javascript" src="presentation/finance_new/exchange_gain_loss/exchange_gain_loss-js.js"></script>-->

<form id="frmExchangeGainLoss" name="frmExchangeGainLoss" method="post">
<div align="center">
<div class="trans_layoutS" style="width:600px">
<div class="trans_text">Exchange Gain/Loss</div>
	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    	<tr>
        	<td>
            	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
                	<tr class="normalfnt">
                	  <td width="4%" height="16">&nbsp;</td>
                	  <td width="29%">&nbsp;</td>
                	  <td>&nbsp;</td>
                	  <td width="3%">&nbsp;</td>
              	  </tr>
                	<tr class="normalfnt">
                	  <td height="16">&nbsp;</td>
                	  <td colspan="2"><table width="100%" border="0" class="tableBorder">
                	    <tr class="normalfnt">
                	      <td><table width="100%" border="0">
                	        <tr class="normalfnt">
                	          <td width="31%">Year <span class="compulsoryRed">*</span></td>
                	          <td width="69%">
                              	<select name="cboYear" id="cboYear" style="width:120px" class="validate[required]">
                                <option value="<?php echo date('Y')-1; ?>"><?php echo date('Y')-1; ?></option>
                                <option value="<?php echo date('Y'); ?>" selected="selected"><?php echo date('Y'); ?></option>
                                </select>
                                </td>
              	          </tr>
                	        <tr class="normalfnt">
                	          <td>Month <span class="compulsoryRed">*</span></td>
                	          <td><select name="cboMonth" id="cboMonth" style="width:120px" class="validate[required]">
                              <?php
							  $month = date('m');
							  for($i==1; $i<13; $i++){ ?>
              	            <option <?php if($month-1==$i){?> selected="selected" <?php } ?> value="<?php echo $i; ?>"><?php echo $obj_common->getMonthName($i); ?></option>
							 <?php }
 							  ?>
                            </select></td>
              	          </tr>
               	          </table></td>
              	      </tr>
              	    </table></td>
                	  <td>&nbsp;</td>
           	      </tr>
                     <tr class="normalfnt">
                        <td>&nbsp;</td>
                        <td colspan="2" height="32">
                            <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                <tr>
                                	<td align="center" class="tableBorder_allRound"><a class="button white medium" id="butGenerate" name="butGenerate"  <?php /*?>style="display:none"<?php */?>>Generate</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                                </tr>
                            </table>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    
              </table>
        </td>
    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</div>
</form>
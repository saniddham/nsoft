// JavaScript Document
var basePath		= "presentation/finance_new/exchange_gain_loss/";
var eglBGReportId	= 68;
var reportId		= 69;

 $(document).ready(function(){
 	
	try
	{
		$("#frmExchangeGainLoss").validationEngine();
	}
	catch(err)
	{
		
	}
 	$('#frmExchangeGainLoss #butGenerate').die('click').live('click',gainlossReportBeforeGen);
	$('#frmExchangeGainLoss #butReport').die('click').live('click',gainlossReport);
	$('#frmExchange_gain_loss #butRptGenerate').die('click').live('click',generateGainloss);

});
function gainlossReportBeforeGen()
{
	var year	= $('#frmExchangeGainLoss #cboYear').val();	
	var month	= $('#frmExchangeGainLoss #cboMonth').val();
	
	if(validate(year,month))
	{
		var url  	= "?q="+eglBGReportId+"&year="+year;
		url 	   += "&month="+month;
	
		window.open(url,'rpt_exchange_gain_loss_beforeProcess.php');
	}
}
function gainlossReport()
{
	var year	= $('#frmExchangeGainLoss #cboYear').val();	
	var month	= $('#frmExchangeGainLoss #cboMonth').val();
	
	if(validateReportView(year,month))
	{
		var url  	= "?q="+reportId+"&year="+year;
		url 	   += "&month="+month;
	
		window.open(url,'report_exchange_gain_loss.php');
	}
}
function generateGainloss()
{
	showWaiting();
		var year	= $('#frmExchange_gain_loss #hidYear').val();	
		var month	= $('#frmExchange_gain_loss #hidMonth').val();
		
		if(validate(year,month))
		{	
			var url 	= basePath+"exchange_gain_loss_set.php"+window.location.search+"&requestType=generate";
			var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
			}
			});		
				alert('Generated Successfully !');	
				hideWaiting();	
		}
		else
		{
			hideWaiting();	
		}
 }
function alertx()
{
	$('#frmExchangeGainLoss #butGenerate').validationEngine('hide')	;
}
function validate(year,month){
	
	var day		=	32 - new Date(year, month, 32).getDate();
	var flag	=0;
	
	var today = new Date();
	date	=	today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate(); 
	//alert(date+'---'+(year+'-'+month+'-'+day));	
	if((year>today.getFullYear()) || (month>(today.getMonth()+1)) || ((date<(year+'-'+month+'-'+day)) && (year==today.getFullYear()) && (month==(today.getMonth()+1)))){
		alert('Invalid selection');
		return false;
	}
	
		var url 	= basePath+"exchange_gain_loss_set.php?requestType=validate";
  		var data 	= "year="+year+"&month="+month;
		var httpobj = $.ajax({
		url:url,
		data:data,
		dataType:'json',
		type:'POST',
		async:false,
		success:function(json)
		{
			type = json.type;
			if(type=='fail'){
				flag	=	1;
				alert(json.msg);
 			}
 		}
	});	
	if(flag==0)
	return true;
	else
	return false;
}
function validateReportView(year,month)
{
	var day		=	32 - new Date(year, month, 32).getDate();
	var flag	=0;
	
	var today = new Date();
	date	=	today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate(); 
	//alert(date+'---'+(year+'-'+month+'-'+day));	
	if((year>today.getFullYear()) || (month>(today.getMonth()+1)) || ((date<(year+'-'+month+'-'+day)) && (year==today.getFullYear()) && (month==(today.getMonth()+1)))){
		alert('Invalid selection');
		return false;
	}
	
		var url 	= basePath+"exchange_gain_loss_set.php?requestType=validateReportView";
  		var data 	= "year="+year+"&month="+month;
		var httpobj = $.ajax({
		url:url,
		data:data,
		dataType:'json',
		type:'POST',
		async:false,
		success:function(json)
		{
			type = json.type;
			if(type=='fail'){
				flag	=	1;
				alert(json.msg);
 			}
 		}
	});	
	if(flag==0)
	return true;
	else
	return false;
}
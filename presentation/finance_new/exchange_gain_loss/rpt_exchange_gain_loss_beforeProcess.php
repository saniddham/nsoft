<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once("class/finance/cls_common_get.php");
include_once("class/cls_commonFunctions_get.php");

include_once ("class/finance/cls_gain_loss_get.php");
include_once ("class/finance/cls_gain_loss_set.php");

$db->disconnect();
include_once "dataAccess/Connector.php";

$obj_common			= new cls_commonFunctions_get($db);
$obj_commfin_get	= new Cls_Common_Get($db);
$obj_gainloss_get	= new cls_gain_loss_get($db);
$obj_gainloss_set	= new cls_gain_loss_set($db);

$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$year				= $_REQUEST["year"];
$month				= $_REQUEST["month"];

if($month==1){
	$yearP		=$year-1;	
	$monthP		=12;
}
else{
	$yearP		=$year;	
	$monthP		=$month-1;	
}

$endOfDayInThisMonth	= date('t',$month);
$startDateThisMonth		= date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
$runDate 				= date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
$lastMonthEnd			= date('Y-m-t', mktime(0, 0, 0, $monthP, 1, $yearP));
$positive_type			= 'GAIN';
$negative_type			= 'LOSS';

$base_curr_array		= $obj_commfin_get->get_Base_Currency_arr($companyId);

?>
<head>  
<title>Exchange Gain Loss</title>

<script type="text/javascript" src="presentation/finance_new/exchange_gain_loss/exchange_gain_loss-js.js"></script>

</head>
<body>
<form id="frmExchange_gain_loss" name="frmExchange_gain_loss" method="post">
  <table width="750" align="center">
    <tr>
      <td colspan="3"><?php include 'reportHeader.php'; ?></td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3" class="reportHeader" style="text-align:center"><strong>Exchange Gain / Loss To Date - <?php echo $runDate; ?> </strong></td>
    </tr>
    <tr>
      <td width="134" class="reportHeader" style="text-align:left"><input type="hidden" id="hidYear" value="<?php echo $year; ?>" /></td>
      <td width="436" class="reportHeader" style="text-align:left"><input type="hidden" id="hidMonth" value="<?php echo $month; ?>" /></td>
      <td width="114" class="reportHeader" style="text-align:right"><a class="button green small" id="butRptGenerate" name="butRptGenerate">Generate</a></td>
    </tr>
     <tr>
        <td colspan="3" class="reportHeader" style="text-align:center">
        <table width="100%" class="rptBordered" id="tblMain" border="0">
        <thead>
            <tr>
            <th width="50%">GL Account</th>
            <th width="16%">Invoice No</th>
            <th width="16%">Gain/Loss</th>
            <th width="18%">Amount (<?php echo $base_curr_array['strCode']; ?>)</th>
            </tr>
         </thead>
		<?php
		$db->begin();
		$catTot_gainLoss	= 0;
		//--------------supplier gain/loss-----------------------------------
			$resulth = $obj_gainloss_get->get_suppliers_results($year,$month); 
			while($header_arr = mysqli_fetch_array($resulth))
			{
				$supId 			= $header_arr['intId'];
				$supId 			= $header_arr['intId'];
				$categoryGl		= $obj_commfin_get->GetSupplierGL($supId);
				$chartOfAccName	= $obj_gainloss_get->getChartOfAccName($categoryGl);
				$tot_gainLoss	= 0;
				$result			= $obj_gainloss_get->get_supplier_gainLoss_results($header_arr['intId'],$lastMonthEnd,$runDate);
				
				while($details_arr = mysqli_fetch_array($result))
				{
					$invoiceNo		 = $details_arr['SERIAL_NO'];
                	$invoiceYear	 = $details_arr['SERIAL_YEAR'];
					$gainLoss		 = $details_arr['GAINLOSS'];
				
					if($gainLoss>0)
						$type 		= $positive_type;
					else
						$type 		= $negative_type;
					
					$catTot_gainLoss += $gainLoss;
					$gainLoss	  	  = abs($gainLoss);
					
					if($gainLoss!=0)
					{
					?>
						<tr>
							<td class="normalfnt" style="text-align:left"><?php echo $chartOfAccName; ?></td>
							<td class="normalfnt" style="text-align:right"><?php echo $invoiceNo.' - '.$invoiceYear; ?></td>
							<td class="normalfnt" style="text-align:center"><?php echo $type; ?></td>
							<td class="normalfnt" style="text-align:right"><?php echo number_format($gainLoss,4); ?></td>
						 </tr>
					<?php
					}
				}
			}
		//--------------customer gain/loss-----------------------------------
			$resulth = $obj_gainloss_get->get_customers_results($year,$month);
			while($header_arr = mysqli_fetch_array($resulth))
			{
				$custId			= $header_arr['intId'];
				$categoryGl		= $obj_commfin_get->GetCustomerGL($custId);	
				$chartOfAccName	= $obj_gainloss_get->getChartOfAccName($categoryGl);
				$tot_gainLoss	= 0;
				
				$result			= $obj_gainloss_get->get_customer_gainLoss_results($header_arr['intId'],$lastMonthEnd,$runDate);
				while($details_arr = mysqli_fetch_array($result))
				{
					$invoiceNo		 = $details_arr['SERIAL_NO'];
                	$invoiceYear	 = $details_arr['SERIAL_YEAR'];
					$gainLoss		 = $details_arr['GAINLOSS'];
				
					if($gainLoss>0)
						$type 		= $positive_type;
					else
						$type 		= $negative_type;
					
					$catTot_gainLoss += $gainLoss;
					$gainLoss	  = abs($gainLoss);
					
					if($gainLoss!=0)
					{
					?>
						<tr>
							<td class="normalfnt" style="text-align:left"><?php echo $chartOfAccName; ?></td>
							<td class="normalfnt" style="text-align:right"><?php echo $invoiceNo.' - '.$invoiceYear; ?></td>
							<td class="normalfnt" style="text-align:center"><?php echo $type; ?></td>
							<td class="normalfnt" style="text-align:right"><?php echo number_format($gainLoss,4); ?></td>
						 </tr>
					<?php
					}
				}
			}
		//--------------other paybal gain/loss-----------------------------------
			$resulth = $obj_gainloss_get->get_other_payble_results();
			while($header_arr = mysqli_fetch_array($resulth))
			{
				$supId			= $header_arr['intId'];
				$categoryGl		= $obj_commfin_get->GetSupplierGL($supId);
				$chartOfAccName	= $obj_gainloss_get->getChartOfAccName($categoryGl);
				$tot_gainLoss	= 0;
				
				$result			= $obj_gainloss_get->get_other_payble_gainLoss_results($header_arr['intId'],$lastMonthEnd,$runDate);
				while($details_arr = mysqli_fetch_array($result))
				{
					$invoiceNo		 = $details_arr['SERIAL_NO'];
                	$invoiceYear	 = $details_arr['SERIAL_YEAR'];
					$gainLoss		 = $details_arr['GAINLOSS'];
				
					if($gainLoss>0)
						$type 		= $positive_type;
					else
						$type 		= $negative_type;
					
					$catTot_gainLoss += $gainLoss;
					$gainLoss	  = abs($gainLoss);
					
					if($gainLoss!=0)
					{
					?>
						<tr>
							<td class="normalfnt" style="text-align:left"><?php echo $chartOfAccName; ?></td>
							<td class="normalfnt" style="text-align:right"><?php echo $invoiceNo.' - '.$invoiceYear; ?></td>
							<td class="normalfnt" style="text-align:center"><?php echo $type; ?></td>
							<td class="normalfnt" style="text-align:right"><?php echo number_format($gainLoss,4); ?></td>
						 </tr>
					<?php
					}
				}
			}
		//-------------- Process 1 - Fixed Deposit,Bank Account-----------------------------------
			$resultP1 = $obj_gainloss_get->get_process1_gain_loss_results($startDateThisMonth,$runDate);
			while($rowP1 = mysqli_fetch_array($resultP1))
			{
				$categoryGl		= $rowP1['CHART_OF_ACCOUNT_ID'];
				$chartOfAccName	= $obj_gainloss_get->getChartOfAccName($categoryGl);
				$amount			= $rowP1['lastAmount']-$rowP1['actualAmount'];
				
				if($amount>0)
					$type 		= $positive_type;
				else
					$type 		= $negative_type;
				
				$catTot_gainLoss += $amount;
				$amount			  = abs($amount);
				
				if($amount!=0)
				{
				?>
                    <tr>
                        <td class="normalfnt" style="text-align:left"><?php echo $chartOfAccName; ?></td>
                        <td class="normalfnt" style="text-align:left">&nbsp;</td>
                        <td class="normalfnt" style="text-align:center"><?php echo $type; ?></td>
                        <td class="normalfnt" style="text-align:right"><?php echo number_format($amount,4); ?></td>
                     </tr>
            	<?php
				}
			}
		//-------------- Process 2 Bank Loans - Non Current Liabilities (USD) -----------------------------------
			$resultP2 = $obj_gainloss_get->get_process2_gain_loss_results($startDateThisMonth,$runDate);
			while($rowP2 = mysqli_fetch_array($resultP2))
			{
				$categoryGl		= $rowP2['CHART_OF_ACCOUNT_ID'];
				$chartOfAccName	= $obj_gainloss_get->getChartOfAccName($categoryGl);
				$amount			= $rowP2['lastAmount']-$rowP2['actualAmount'];
				
				if($amount>0)
					$type 		= $positive_type;
				else
					$type 		= $negative_type;
			
				$catTot_gainLoss += $amount;
				$amount			  = abs($amount);
				
				if($amount!=0)
				{
				?>
                    <tr>
                        <td class="normalfnt" style="text-align:left"><?php echo $chartOfAccName; ?></td>
                        <td class="normalfnt" style="text-align:left">&nbsp;</td>
                        <td class="normalfnt" style="text-align:center"><?php echo $type; ?></td>
                        <td class="normalfnt" style="text-align:right"><?php echo number_format($amount,4); ?></td>
                     </tr>
            	<?php
				}
			}
		//-------------- Process 3 Bank Loans - Current Liabilities (USD) -----------------------------------
			$resultP3 = $obj_gainloss_get->get_process3_gain_loss_results($startDateThisMonth,$runDate);
			while($rowP3 = mysqli_fetch_array($resultP3))
			{
				$categoryGl		= $rowP3['CHART_OF_ACCOUNT_ID'];
				$chartOfAccName	= $obj_gainloss_get->getChartOfAccName($categoryGl);
				$amount			= $rowP3['lastAmount']-$rowP3['actualAmount'];
				
				if($amount>0)
					$type 		= $positive_type;
				else
					$type 		= $negative_type;
				
				$catTot_gainLoss += $amount;
				$amount			  = abs($amount);
				
				if($amount!=0)
				{
				?>
                    <tr>
                        <td class="normalfnt" style="text-align:left"><?php echo $chartOfAccName; ?></td>
                        <td class="normalfnt" style="text-align:left">&nbsp;</td>
                        <td class="normalfnt" style="text-align:center"><?php echo $type; ?></td>
                        <td class="normalfnt" style="text-align:right"><?php echo number_format($amount,4); ?></td>
                     </tr>
            	<?php
				}
			}
		//-------------- Process 4 - Interest Receivable,Advance Against Import Purchase --------------------------
			$resultP4 = $obj_gainloss_get->get_process4_gain_loss_results($startDateThisMonth,$runDate);
			while($rowP4 = mysqli_fetch_array($resultP4))
			{
				$categoryGl		= $rowP4['CHART_OF_ACCOUNT_ID'];
				$chartOfAccName	= $obj_gainloss_get->getChartOfAccName($categoryGl);
				$amount			= $rowP4['lastAmount']-$rowP4['actualAmount'];
				
				if($amount>0)
					$type 		= $positive_type;
				else
					$type 		= $negative_type;
				
				$catTot_gainLoss += $amount;
				$amount			  = abs($amount);
				
				if($amount!=0)
				{
				?>
                    <tr>
                        <td class="normalfnt" style="text-align:left"><?php echo $chartOfAccName; ?></td>
                        <td class="normalfnt" style="text-align:left">&nbsp;</td>
                        <td class="normalfnt" style="text-align:center"><?php echo $type; ?></td>
                        <td class="normalfnt" style="text-align:right"><?php echo number_format($amount,4); ?></td>
                     </tr>
            	<?php
				}
			}
				if($catTot_gainLoss>0)
					$totType	 = $positive_type;
				else
					$totType	 = $negative_type;
				
				$catTot_gainLoss = abs($catTot_gainLoss);
			?>
                <tr bgcolor="#F4F4F4">
                    <td colspan="2" class="normalfnt" style="text-align:right" ><b>Total</b></td>
                    <td class="normalfnt" style="text-align:center"><b><?php echo $totType; ?></b></td>
                    <td class="normalfnt" style="text-align:right"><b><?php echo number_format($catTot_gainLoss,4); ?></b></td>
                </tr>
            <?php
  			$db->commit();
       ?>
        </table>
      </td>
    </tr>
    <tr height="40">
      <td colspan="3" align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
    </tr>		 
    </table>
</form>
</body>
<?php
include_once "dataAccess/DBManager2.php";
$db	= new DBManager2();

$db->connect();
?>
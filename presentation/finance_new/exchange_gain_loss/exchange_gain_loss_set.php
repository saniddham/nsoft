<?php
	session_start();
	$backwardseperator 	= "../../../";
	include_once  "{$backwardseperator}dataAccess/Connector.php";
	include_once("../../../class/finance/cls_common_get.php");
	include_once("../../../class/cls_commonFunctions_get.php");
 
 	include_once ("../../../class/finance/cls_gain_loss_get.php");
	include_once ("../../../class/finance/cls_gain_loss_set.php");

	$obj_common			= new cls_commonFunctions_get($db);
	$obj_commfin_get	= new Cls_Common_Get($db);
	$obj_gainloss_get	= new cls_gain_loss_get($db);
	$obj_gainloss_set	= new cls_gain_loss_set($db);
 	
	$requestType 		= $_REQUEST['requestType'];
	$locationId			= $_SESSION["CompanyID"];
	$companyId			= $_SESSION["headCompanyId"];
	$year				= $_REQUEST["year"];
	$month				= $_REQUEST["month"];
 	
	if($month==1){
		$yearP		=$year-1;	
		$monthP		=12;
	}
	else{
		$yearP		=$year;	
		$monthP		=$month-1;	
	}
 	
 	$endOfDayInThisMonth	= date('t',$month);
	$startDateThisMonth		= date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
 	$runDate 				= date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
 	$lastMonthEnd			= date('Y-m-t', mktime(0, 0, 0, $monthP, 1, $yearP));
	
	$base_curr_array		= $obj_commfin_get->get_Base_Currency_arr($companyId);
 
 
 	if($requestType=='generate'){	
 	$db->begin();
	//--------------supplier gain/loss-----------------------------------
	$resulth= $obj_gainloss_get->get_suppliers_results($year,$month);
	while($header_arr = mysqli_fetch_array($resulth))
	{
		$supId				= $header_arr['intId'];
		$currency			= $base_curr_array['intId']; 
		$gainLossGl			= '535';
		$categoryGl			= $obj_commfin_get->GetSupplierGL($supId);			
		$tot_gainLoss		= 0;
		$result				= $obj_gainloss_get->get_supplier_gainLoss_results($header_arr['intId'],$lastMonthEnd,$runDate);
				while($details_arr = mysqli_fetch_array($result))
				{
 					$gainLoss			= $details_arr['GAINLOSS'];
 					$tot_gainLoss		+= $gainLoss;
				}//
				if($tot_gainLoss!=0){
					$data_array		=	$obj_common->GetSystemMaxNo('GAIN_LOSS_NO',$locationId);
					$serialNo		=	$data_array["max_no"];
					$serialYear		=	date('Y');
					if($tot_gainLoss>0){
						$trnsType_gainLoss	='D';
						$trnsType_cat		='C';
 					}
					else{
						$trnsType_gainLoss	='C';
						$trnsType_cat		='D';
					}
					$tot_gainLoss = abs($tot_gainLoss);
 					$obj_commfin_get->Save_Jurnal_Transaction_bkp_date($gainLossGl,$tot_gainLoss,$serialNo,$serialYear,'GAIN_LOSS',0,0,$trnsType_gainLoss,'SU',$supId,$currency,$runDate);
					$obj_commfin_get->Save_Jurnal_Transaction_bkp_date($categoryGl,$tot_gainLoss,$serialNo,$serialYear,'GAIN_LOSS',0,0,$trnsType_cat,'SU',$supId,$currency,$runDate);
					$obj_gainloss_set->Save_Gain_Loss_header($serialNo,$serialYear,$supId,'SU',$year,$month,$endOfDayInThisMonth,$runDate);
				}
 	}
	//--------------customer gain/loss-----------------------------------
	$resulth= $obj_gainloss_get->get_customers_results($year,$month);
	while($header_arr = mysqli_fetch_array($resulth))
	{
		$custId				= $header_arr['intId'];
		$currency			= $base_curr_array['intId']; 
		$gainLossGl			= '535';
		$categoryGl			= $obj_commfin_get->GetCustomerGL($custId);			
		$tot_gainLoss		= 0;
		$result				= $obj_gainloss_get->get_customer_gainLoss_results($header_arr['intId'],$lastMonthEnd,$runDate);
				while($details_arr = mysqli_fetch_array($result))
				{
 					$gainLoss			= $details_arr['GAINLOSS'];
 					$tot_gainLoss		+= $gainLoss;
				}//
				if($tot_gainLoss!=0){
					$data_array		=	$obj_common->GetSystemMaxNo('GAIN_LOSS_NO',$locationId);
					$serialNo		=	$data_array["max_no"];
					$serialYear		=	date('Y');
					if($tot_gainLoss>0){
						$trnsType_gainLoss	='C';
						$trnsType_cat		='D';
 					}
					else{
						$trnsType_gainLoss	='D';
						$trnsType_cat		='C';
					}
					$tot_gainLoss = abs($tot_gainLoss);
 					$obj_commfin_get->Save_Jurnal_Transaction_bkp_date($gainLossGl,$tot_gainLoss,$serialNo,$serialYear,'GAIN_LOSS',0,0,$trnsType_gainLoss,'CU',$custId,$currency,$runDate);
					$obj_commfin_get->Save_Jurnal_Transaction_bkp_date($categoryGl,$tot_gainLoss,$serialNo,$serialYear,'GAIN_LOSS',0,0,$trnsType_cat,'CU',$custId,$currency,$runDate);
					$obj_gainloss_set->Save_Gain_Loss_header($serialNo,$serialYear,$custId,'CU',$year,$month,$endOfDayInThisMonth,$runDate);
				}
 	}//
	//--------------other paybal gain/loss-----------------------------------
	$resulth= $obj_gainloss_get->get_other_payble_results();
	while($header_arr = mysqli_fetch_array($resulth))
	{
		$supId				= $header_arr['intId'];
		$currency			= $base_curr_array['intId']; 
		$gainLossGl			= '535';
		$categoryGl			= $obj_commfin_get->GetSupplierGL($supId);			
		$tot_gainLoss		= 0;
		$result				= $obj_gainloss_get->get_other_payble_gainLoss_results($header_arr['intId'],$lastMonthEnd,$runDate);
				while($details_arr = mysqli_fetch_array($result))
				{
 					$gainLoss			= $details_arr['GAINLOSS'];
 					$tot_gainLoss		+= $gainLoss;
				}//
				if($tot_gainLoss!=0){
					$data_array		=	$obj_common->GetSystemMaxNo('GAIN_LOSS_NO',$locationId);
					$serialNo		=	$data_array["max_no"];
					$serialYear		=	date('Y');
					if($tot_gainLoss>0){
						$trnsType_gainLoss	='D';
						$trnsType_cat		='C';
 					}
					else{
						$trnsType_gainLoss	='C';
						$trnsType_cat		='D';
					}
					$tot_gainLoss = abs($tot_gainLoss);
 					$obj_commfin_get->Save_Jurnal_Transaction_bkp_date($gainLossGl,$tot_gainLoss,$serialNo,$serialYear,'GAIN_LOSS',0,0,$trnsType_gainLoss,'OP',$supId,$currency,$runDate);
					$obj_commfin_get->Save_Jurnal_Transaction_bkp_date($categoryGl,$tot_gainLoss,$serialNo,$serialYear,'GAIN_LOSS',0,0,$trnsType_cat,'OP',$supId,$currency,$runDate);
					$obj_gainloss_set->Save_Gain_Loss_header($serialNo,$serialYear,$supId,'OP',$year,$month,$endOfDayInThisMonth,$runDate);
				}
 	}
	//-------------- Process 1 - Fixed Deposit,Bank Account-----------------------------------
	
		$resultP1 = $obj_gainloss_get->get_process1_gain_loss_results($startDateThisMonth,$runDate);
		
		while($rowP1 = mysqli_fetch_array($resultP1))
		{
			$currency			= $rowP1['CURRENCY_ID']; 
			$gainLossGl			= '535';
			$categoryGl			= $rowP1['CHART_OF_ACCOUNT_ID'];
			
			$trnsType_gainLoss	= '';
			$trnsType_cat		= '';
			$amount				= $rowP1['lastAmount']-$rowP1['actualAmount'];
			
			if($amount>0) 
			{
				$trnsType_gainLoss	='C';
				$trnsType_cat		='D';
			}
			else
			{
				$trnsType_gainLoss	='D';
				$trnsType_cat		='C';
			}
			if($amount!=0)
			{
				$data_array		=	$obj_common->GetSystemMaxNo('GAIN_LOSS_NO',$locationId);
				$serialNo		=	$data_array["max_no"];
				$serialYear		=	date('Y');
				$tot_gainLoss 	= 	abs($amount);
					
				$resultArr = $obj_commfin_get->Save_Jurnal_Transaction_bkp_date($gainLossGl,$tot_gainLoss,$serialNo,$serialYear,'GAIN_LOSS',0,0,$trnsType_gainLoss,'GL','null',$currency,$runDate);
				$obj_commfin_get->Save_Jurnal_Transaction_bkp_date($categoryGl,$tot_gainLoss,$serialNo,$serialYear,'GAIN_LOSS',0,0,$trnsType_cat,'P1','null',$currency,$runDate);
				$obj_gainloss_set->Save_Gain_Loss_header($serialNo,$serialYear,'null','P1',$year,$month,$endOfDayInThisMonth,$runDate);
			}
			
		}
	//-------------------------------------------------------------------------------------------------------
	
	//-------------- Process 2 Bank Loans - Non Current Liabilities (USD) -----------------------------------
	
		$resultP2 = $obj_gainloss_get->get_process2_gain_loss_results($startDateThisMonth,$runDate);
		
		while($rowP2 = mysqli_fetch_array($resultP2))
		{
			$currency			= $rowP2['CURRENCY_ID']; 
			$gainLossGl			= '535';
			$categoryGl			= $rowP2['CHART_OF_ACCOUNT_ID'];
			
			$trnsType_gainLoss	= '';
			$trnsType_cat		= '';
			$amount				= $rowP2['lastAmount']-$rowP2['actualAmount'];
			
			if($amount>0) 
			{
				$trnsType_gainLoss	='D';
				$trnsType_cat		='C';
			}
			else
			{
				$trnsType_gainLoss	='C';
				$trnsType_cat		='D';
			}
			if($amount!=0)
			{
				$data_array		=	$obj_common->GetSystemMaxNo('GAIN_LOSS_NO',$locationId);
				$serialNo		=	$data_array["max_no"];
				$serialYear		=	date('Y');
				$tot_gainLoss 	= 	abs($amount);
					
				$resultArr = $obj_commfin_get->Save_Jurnal_Transaction_bkp_date($gainLossGl,$tot_gainLoss,$serialNo,$serialYear,'GAIN_LOSS',0,0,$trnsType_gainLoss,'GL','null',$currency,$runDate);
				$obj_commfin_get->Save_Jurnal_Transaction_bkp_date($categoryGl,$tot_gainLoss,$serialNo,$serialYear,'GAIN_LOSS',0,0,$trnsType_cat,'P2','null',$currency,$runDate);
				$obj_gainloss_set->Save_Gain_Loss_header($serialNo,$serialYear,'null','P2',$year,$month,$endOfDayInThisMonth,$runDate);
			}	
		}
	//---------------------------------------------------------------------------------------------------
	
	//-------------- Process 3 Bank Loans - Current Liabilities (USD) -----------------------------------
	
		$resultP3 = $obj_gainloss_get->get_process3_gain_loss_results($startDateThisMonth,$runDate);
		
		while($rowP3 = mysqli_fetch_array($resultP3))
		{
			$currency			= $rowP3['CURRENCY_ID']; 
			$gainLossGl			= '535';
			$categoryGl			= $rowP3['CHART_OF_ACCOUNT_ID'];
			
			$trnsType_gainLoss	= '';
			$trnsType_cat		= '';
			$amount				= $rowP3['gainLossAmount'];
			
			if($amount>0) 
			{
				$trnsType_gainLoss	='D';
				$trnsType_cat		='C';
			}
			else
			{
				$trnsType_gainLoss	='C';
				$trnsType_cat		='D';
			}
			if($amount!=0)
			{
				$data_array		=	$obj_common->GetSystemMaxNo('GAIN_LOSS_NO',$locationId);
				$serialNo		=	$data_array["max_no"];
				$serialYear		=	date('Y');
				$tot_gainLoss 	= 	abs($amount);
					
				$resultArr = $obj_commfin_get->Save_Jurnal_Transaction_bkp_date($gainLossGl,$tot_gainLoss,$serialNo,$serialYear,'GAIN_LOSS',0,0,$trnsType_gainLoss,'GL','null',$currency,$runDate);
				$obj_commfin_get->Save_Jurnal_Transaction_bkp_date($categoryGl,$tot_gainLoss,$serialNo,$serialYear,'GAIN_LOSS',0,0,$trnsType_cat,'P3','null',$currency,$runDate);
				$obj_gainloss_set->Save_Gain_Loss_header($serialNo,$serialYear,'null','P3',$year,$month,$endOfDayInThisMonth,$runDate);
			}
			
		}
	//-------------------------------------------------------------------------------------------------------
	
	//-------------- Process 4 - Interest Receivable,Advance Against Import Purchase --------------------------
	
		$resultP4 = $obj_gainloss_get->get_process4_gain_loss_results($startDateThisMonth,$runDate);
		
		while($rowP4 = mysqli_fetch_array($resultP4))
		{
			$currency			= $rowP4['CURRENCY_ID']; 
			$gainLossGl			= '535';
			$categoryGl			= $rowP4['CHART_OF_ACCOUNT_ID'];
			
			$trnsType_gainLoss	= '';
			$trnsType_cat		= '';
			$amount				= $rowP4['gainLossAmount'];
			
			if($amount>0) 
			{
				$trnsType_gainLoss	='C';
				$trnsType_cat		='D';
			}
			else
			{
				$trnsType_gainLoss	='D';
				$trnsType_cat		='C';
			}
			if($amount!=0)
			{
				$data_array		=	$obj_common->GetSystemMaxNo('GAIN_LOSS_NO',$locationId);
				$serialNo		=	$data_array["max_no"];
				$serialYear		=	date('Y');
				$tot_gainLoss 	= 	abs($amount);
					
				$resultArr = $obj_commfin_get->Save_Jurnal_Transaction_bkp_date($gainLossGl,$tot_gainLoss,$serialNo,$serialYear,'GAIN_LOSS',0,0,$trnsType_gainLoss,'GL','null',$currency,$runDate);
				$obj_commfin_get->Save_Jurnal_Transaction_bkp_date($categoryGl,$tot_gainLoss,$serialNo,$serialYear,'GAIN_LOSS',0,0,$trnsType_cat,'P4','null',$currency,$runDate);
				$obj_gainloss_set->Save_Gain_Loss_header($serialNo,$serialYear,'null','P4',$year,$month,$endOfDayInThisMonth,$runDate);
			}
			
		}
	//-------------------------------------------------------------------------------------------------------
		$db->commit();
	}
	else if($requestType=='validate'){
		
		$db->begin();
	
		$year	= $_REQUEST['year'];
		$month	= $_REQUEST['month'];
		
		 $sql = "SELECT
				finance_gain_loss_header.ID
				FROM `finance_gain_loss_header`
				WHERE
				finance_gain_loss_header.`YEAR` = '$year' AND
				finance_gain_loss_header.`MONTH` = '$month'";
		$result = $db->RunQuery2($sql);
		$r=0;
		while($row=mysqli_fetch_array($result))
		{
			$r++;
 		}
		
		$excRate	=	$obj_commfin_get->get_exchane_rate($base_curr_array['intId'],$runDate,$companyId,'RunQuery2');
		
		
		if($r>0){
		$response['type'] = 'fail';
		$response['msg'] = 'Gain/Loss already generated';
		}
		else if($excRate==''){
		$response['type'] = 'fail';
		$response['msg'] = 'Please enter exchange rate for month end of selected month';
		}		
		else
		$response['type'] = 'pass';

		echo json_encode($response);
		$db->commit();
	}
	else if($requestType=='validateReportView')
	{
		$db->begin();
	
		$year	= $_REQUEST['year'];
		$month	= $_REQUEST['month'];
		
		 $sql = "SELECT
				finance_gain_loss_header.ID
				FROM `finance_gain_loss_header`
				WHERE
				finance_gain_loss_header.`YEAR` = '$year' AND
				finance_gain_loss_header.`MONTH` = '$month'";
		
		$result = $db->RunQuery2($sql);
		if(mysqli_num_rows($result)>0)
		{
			$response['type'] = 'pass';
		}
		else
		{
			$response['type'] = 'fail';
			$response['msg']  = 'No Gain/Loss generated for selected month to view report ';
		}
		echo json_encode($response);
		$db->commit();
	}

	?>    
 
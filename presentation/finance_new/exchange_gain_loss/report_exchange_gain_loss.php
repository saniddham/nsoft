<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once("class/finance/cls_common_get.php");
include_once("class/cls_commonFunctions_get.php");
include_once ("class/finance/cls_gain_loss_get.php");

$db->disconnect();
include_once "dataAccess/Connector.php";

$obj_common			= new cls_commonFunctions_get($db);
$obj_commfin_get	= new Cls_Common_Get($db);
$obj_gainloss_get	= new cls_gain_loss_get($db);

$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];

$baseCurruncy_arr 	= $obj_commfin_get->get_Base_Currency_arr($companyId);
$year				= $_REQUEST["year"];
$month				= $_REQUEST["month"];

if($month==1)
{
	$yearP		= $year-1;	
	$monthP		= 12;
}
else
{
	$yearP		= $year;	
	$monthP		= $month-1;	
}

$endOfDayInThisMonth	= date('t',$month);
$startDateThisMonth		= date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
$runDate 				= date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
$lastMonthEnd			= date('Y-m-t', mktime(0, 0, 0, $monthP, 1, $yearP));

?>
<head>
<title>Exchange Gain Loss</title>
</head>
<body>
<form id="frmExchange_gain_loss" name="frmExchange_gain_loss" method="post">
  <table width="750" align="center">
    <tr>
      <td><?php include 'reportHeader.php'; ?></td>
    </tr>
    <?php 
	$db->begin();
	
	?> 
		<tr>
		  <td>&nbsp;</td>
		</tr>
		<tr>
		  <td class="reportHeader" style="text-align:center"><strong>Exchange Gain / Loss To Date - <?php echo $runDate; ?> </strong></td>
		</tr>
		<tr>
		  <td class="reportHeader" style="text-align:center">&nbsp;</td>
		</tr>
        <tr>
            <td class="reportHeader" style="text-align:center">
            <table width="100%" class="rptBordered" id="tblMain" border="0">
                <thead>
                    <tr>
                        <th width="51%">Invoice No</th>
                        <th width="24%">Gain/Loss</th>
                        <th width="25%">Amount (<?php echo $baseCurruncy_arr['strCode']; ?>)</th>
                    </tr>
            	</thead>
               	<tbody>
		 <?php
		$catTot_gainLoss  = 0;
		$resulth 		  = $obj_gainloss_get->get_header_report_results($year,$month);
		while($header_arr = mysqli_fetch_array($resulth))
		{
			$catId					= $header_arr['CATEGORY_ID'];
			$rptType				= '';
		
			if($header_arr['CATEGORY_TYPE']=='SU'){
				$headerField		='Supplier';
				$categoryField		='Supplier';
				$category_lower		='supplier';
				$category_upper		='SUPPLIER';
				$positive_Gainloss	='D';
				$negative_Gainloss	='C';
				$positive_Categ		='C';
				$negative_Categ		='D';
				$positive_type		='GAIN';
				$negative_type		='LOSS';
				$tot_positive_type	='GAIN';
				$tot_negative_type	='LOSS';
				
				$records			= $obj_gainloss_get->get_supplier_gainLoss_report_records($catId,$runDate,$lastMonthEnd);
				$categoryName		= $obj_commfin_get->GetPublic_values('strName','mst_supplier','intId',$catId);
				$resultD			= $obj_gainloss_get->get_supplier_gainLoss_report_results($catId,$runDate,$lastMonthEnd);
			}
			else if($header_arr['CATEGORY_TYPE']=='CU'){
				$headerField		='Customer';
				$categoryField		='Customer';
				$category_lower		='customer';
				$category_upper		='CUSTOMER';
				$positive_Gainloss	='C';
				$negative_Gainloss	='D';
				$positive_Categ		='D';
				$negative_Categ		='C';
				$positive_type		='GAIN';
				$negative_type		='LOSS';
				$tot_positive_type	='GAIN';
				$tot_negative_type	='LOSS';
				
				$records			= $obj_gainloss_get->get_customer_gainLoss_report_records($catId,$runDate,$lastMonthEnd);
				$categoryName		= $obj_commfin_get->GetPublic_values('strName','mst_customer','intId',$catId);
				$resultD			= $obj_gainloss_get->get_customer_gainLoss_report_results($catId,$runDate,$lastMonthEnd);
			}
			else if($header_arr['CATEGORY_TYPE']=='OP'){
				$headerField		='Other Payble';
				$categoryField		='Supplier';
				$category_lower		='supplier';
				$category_upper		='SUPPLIER';
				$positive_Gainloss	='D';
				$negative_Gainloss	='C';
				$positive_Categ		='C';
				$negative_Categ		='D';
				$positive_type		='GAIN';
				$negative_type		='LOSS';
				$tot_positive_type	='GAIN';
				$tot_negative_type	='LOSS';
				
				$records			= $obj_gainloss_get->get_other_payble_gainLoss_report_records($catId,$runDate,$lastMonthEnd);
				$categoryName		= $obj_commfin_get->GetPublic_values('strName','mst_supplier','intId',$catId);
				$resultD			= $obj_gainloss_get->get_other_payble_gainLoss_report_results($catId,$runDate,$lastMonthEnd);
			}
			else if($header_arr['CATEGORY_TYPE']=='P1'){
				$headerField		='Fixed Deposit,Bank Account';
				$categoryField		='Process 1';
				$category_lower		='Fixed Deposit,Bank Account';
				$category_upper		='&nbsp;';
				$positive_Gainloss	='D';
				$negative_Gainloss	='C';
				$positive_Categ		='C';
				$negative_Categ		='D';
				$positive_type		='GAIN';
				$negative_type		='LOSS';
				$tot_positive_type	='GAIN';
				$tot_negative_type	='LOSS';
				$rptType			='PROCESS';
				
				$categoryName		= 'Fixed Deposit,Bank Account';
				$resultD			= $obj_gainloss_get->get_process1_gain_loss_results($startDateThisMonth,$runDate);
				$records			= mysqli_num_rows($resultD);
			}
			else if($header_arr['CATEGORY_TYPE']=='P2'){
				$headerField		='Bank Loans';
				$categoryField		='Process 2';
				$category_lower		='Bank Loans';
				$category_upper		='&nbsp;';
				$positive_Gainloss	='D';
				$negative_Gainloss	='C';
				$positive_Categ		='C';
				$negative_Categ		='D';
				$positive_type		='GAIN';
				$negative_type		='LOSS';
				$tot_positive_type	='GAIN';
				$tot_negative_type	='LOSS';
				$rptType			='PROCESS';
				
				$categoryName		= 'Bank Loans - Non Current Liabilities';
				$resultD			= $obj_gainloss_get->get_process2_gain_loss_results($startDateThisMonth,$runDate);
				$records			= mysqli_num_rows($resultD);
			}
			else if($header_arr['CATEGORY_TYPE']=='P3'){
				$headerField		='Bank Loans';
				$categoryField		='Process 2';
				$category_lower		='Bank Loans';
				$category_upper		='&nbsp;';
				$positive_Gainloss	='D';
				$negative_Gainloss	='C';
				$positive_Categ		='C';
				$negative_Categ		='D';
				$positive_type		='GAIN';
				$negative_type		='LOSS';
				$tot_positive_type	='GAIN';
				$tot_negative_type	='LOSS';
				$rptType			='PROCESS';
				
				$categoryName		= 'Bank Loans - Current Liabilities';
				$resultD			= $obj_gainloss_get->get_process3_gain_loss_results($startDateThisMonth,$runDate);
				$records			= mysqli_num_rows($resultD);
			}
			else if($header_arr['CATEGORY_TYPE']=='P4'){
				$headerField		='Interest Receivable,Advance Against Import Purchase';
				$categoryField		='Process 3';
				$category_lower		='Bank Loans';
				$category_upper		='&nbsp;';
				$positive_Gainloss	='D';
				$negative_Gainloss	='C';
				$positive_Categ		='C';
				$negative_Categ		='D';
				$positive_type		='GAIN';
				$negative_type		='LOSS';
				$tot_positive_type	='GAIN';
				$tot_negative_type	='LOSS';
				$rptType			='PROCESS';
				
				$categoryName		= 'Interest Receivable,Advance Against Import Purchase';
				$resultD			= $obj_gainloss_get->get_process4_gain_loss_results($startDateThisMonth,$runDate);
				$records			= mysqli_num_rows($resultD);
			}
			if($records>0){

				while($details_arr = mysqli_fetch_array($resultD))
				{
					if($rptType=='PROCESS')
					{
						switch($header_arr['CATEGORY_TYPE'])
						{
							case 'P1':
							$gainLoss = $details_arr['lastAmount']-$details_arr['actualAmount'];
							break;
							
							case 'P2':
							$gainLoss = $details_arr['lastAmount']-$details_arr['actualAmount'];
							break;
							
							case 'P3':
							$gainLoss = $details_arr['gainLossAmount'];
							break;
							
							case 'P4':
							$gainLoss = $details_arr['gainLossAmount'];
							break;	
						}	
						$conCatInvNo	= '&nbsp;';
					}
					else
					{
						$invoiceNo		= $details_arr['SERIAL_NO'];
						$invoiceYear	= $details_arr['SERIAL_YEAR'];
						$conCatInvNo	= $invoiceNo.'-'.$invoiceYear;
						$gainLoss		= $details_arr['GAINLOSS'];
					}
				   
					if($gainLoss>0){
						$type = $positive_type;
					}
					else{
						$type = $negative_type;
					}
					$gainLossP=abs($gainLoss);
			?>
					<tr>
						<td class="normalfnt" style="text-align:center"><?php echo $conCatInvNo; ?></td>
						<td class="normalfnt" style="text-align:center"><?php echo $type; ?></td>
						<td class="normalfnt" style="text-align:right"><?php echo number_format($gainLossP,4); ?></td>
					 </tr>
			<?php
					$catTot_gainLoss += $gainLoss;
				}
			}
		}
		if($catTot_gainLoss!=0)
		{
			if($catTot_gainLoss>0)
			{
				$trnsType_gainLoss	= $positive_Gainloss;
				$trnsType_cat		= $positive_Categ;
				$type				= $tot_positive_type;
			}
			else
			{
				$trnsType_gainLoss	= $negative_Gainloss;
				$trnsType_cat		= $negative_Categ;
				$type				= $tot_negative_type;
			}
		}
		$catTot_gainLossP = abs($catTot_gainLoss);
	?>
                <tr bgcolor="#F4F4F4">
                    <td class="normalfnt" style="text-align:right" ><b>Total</b></td>
                    <td class="normalfnt" style="text-align:center"><b><?php echo $type; ?></b></td>
                    <td class="normalfnt" style="text-align:right"><b><?php echo number_format($catTot_gainLossP,4); ?></b></td>
                </tr>
                </tbody>
            </table>
          </td>
        </tr> 
        <tr height="40">
          <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
        </tr>
			 <?php
  		$db->commit();
 	?>    
    </table>
</form>
</body>
</html>
<?php
include_once "dataAccess/DBManager2.php";
$db	= new DBManager2();

$db->connect();
?>
 
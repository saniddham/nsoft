<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once  "class/finance/loan_schedule/cls_loan_schedule_get.php";
include_once  "class/cls_commonErrorHandeling_get.php";
include 	  "include/javascript.html";

$obj_schedule_get			= new Cls_Schedule_Get($db);
$obj_commonErrHandle		= new cls_commonErrorHandeling_get($db);

$locationId					= $_SESSION["CompanyID"];
$companyId					= $_SESSION["headCompanyId"];
$session_userId 			= $_SESSION["userId"];
$programCode				= 'P0775';
$scheduleNo					= $_REQUEST["scheduleNo"];
$scheduleYear				= $_REQUEST["scheduleYear"];
$mode						= (!isset($_REQUEST['mode'])?'':$_REQUEST['mode']);

$header_array 				= $obj_schedule_get->getRptHeaderData($scheduleNo,$scheduleYear);
$detail_result 				= $obj_schedule_get->getRptDetailData($scheduleNo,$scheduleYear);

$intStatus					= $header_array['STATUS'];
$levels						= $header_array['APPROVE_LEVELS'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_cancel($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_cancel			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_reject($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_reject			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_confirm			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_revise($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_revise			= $permition_arr['permision'];

?>
<head>
<title>Loan Schedule Report</title>

<script type="text/javascript" src="presentation/finance_new/loan_schedule/rptLoanSchedule-js.js"></script>

<style>
#apDiv1 {
	position: absolute;
	left: 468px;
	top: 181px;
	width: 650px;
	height: 322px;
	z-index: 1;
	visibility: hidden;
}
.APPROVE {
	font-size: 16px;
	font-weight:bold;
	font-family: "Arial Black", Gadget, sans-serif;
	color: #36F;
}
</style>
</head>
<body>
<?php
if($intStatus==-2)
{
?>
	<div id="apDiv1"><img src="images/cancelled.png" style="opacity:0.3" /></div>
<?php
}
?>
<form id="frmRptLoanSchedule" name="frmRptLoanSchedule" method="post" action="rpt_loan_schedule.php" autocomplete="off">
<table width="700" align="center">
	<tr>
    	<td colspan="3"><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
    	<td colspan="3" style="text-align:center">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="3" style="text-align:center"><strong>LOAN SCHEDULE REPORT</strong></td>
    </tr>
    <?php
		include "presentation/report_approve_status_and_buttons.php"
     ?>
    <tr>
    	<td colspan="3">
            <table width="100%" border="0">
            	<tr class="normalfnt">
                	<td width="16%">Schedule No</td>
                    <td width="2%">:</td>
                    <td width="33%"><?php echo $scheduleNo.' / '.$scheduleYear?></td>
                    <td width="16%">Schedule Date</td>
                    <td width="3%">:</td>
                    <td width="30%"><?php echo $header_array["SCHEDULE_DATE"]?></td>
                </tr>
            	<tr class="normalfnt">
            	  <td>Currency</td>
            	  <td>:</td>
            	  <td><?php echo $header_array["strCode"]?></td>
            	  <td>Debit Account</td>
            	  <td>:</td>
            	  <td><?php echo $header_array["debitAccount"]?></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>Remarks</td>
            	  <td>:</td>
            	  <td valign="top"><?php echo $header_array["REMARKS"]?></td>
            	  <td valign="top">Credit Account</td>
            	  <td>:</td>
            	  <td valign="top"><?php echo $header_array["creditAccount"]?></td>
          	  </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="3">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="3">
        	<table width="100%" border="0" class="rptBordered" id="tblMain">
                <thead> 
                <tr class="normalfnt">
                    <th width="8%">No.</th>
                    <th width="18%">Date</th>
                    <th width="25%">Amount</th>
                    <th width="24%">Status</th>
                    <th width="25%">Processed Date</th>
                </tr>
                </thead>
                <tbody>
				<?php
                $total 	= 0;
				$i		= 0;
                while($row = mysqli_fetch_array($detail_result))
                {
					$total += $row['AMOUNT'];
                ?>
                <tr class="normalfnt">
                	<td style="text-align:center"><?php echo ++$i; ?></td>
                    <td style="text-align:center"><?php echo $row['PAY_DATE']; ?></td>
                    <td style="text-align:right"><?php echo number_format($row['AMOUNT'],2); ?></td>
                    <td style="text-align:center"><?php echo ($row['STATUS']==1?'<img src="images/accept.png">':'&nbsp;'); ?></td>
                    <td style="text-align:center"><?php echo ($row['PROCESS_DATE']==''?'&nbsp;':$row['PROCESS_DATE']); ?></td>
                <?php
				}
				?>
                <tr class="normalfnt">
                    <td colspan="2" style="text-align:center"><b>Total :</b></td>
                    <td style="text-align:right"><b><?php echo number_format($total,2); ?></b></td>
                    <td colspan="2" style="text-align:center">&nbsp;</td>
                </tr>
               </tbody>
            </table>
        </td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr>
        <td>
			<?php
            $creator		= $header_array['CREATOR'];
            $createdDate	= $header_array['CREATED_DATE'];
            $resultA 		= $obj_schedule_get->getRptApproveDetails($scheduleNo,$scheduleYear);
            include "presentation/report_approvedBy_details.php"
            ?>
        </td>
    </tr>
    <tr height="40">
      <td align="center" class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></td>
    </tr>
</table>
</form>
</body>
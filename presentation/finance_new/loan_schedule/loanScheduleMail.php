<?php

$thisFilePath = $_SERVER['PHP_SELF'];
include_once '../../../dataAccess/LoginDBManager.php';
include_once "../../../libraries/mail/mail_bcc.php";		
$db =  new LoginDBManager();

$diffDays	= getDiffrentDays();
//$diffDays		= 5;
$haveRecords	= false;

ob_start();
?>
<style>

 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
</style>
<table width="900" cellpadding="0" cellspacing="0" align="center" class="bordered">
	<thead>
    <tr>
    	<th colspan="5">PAYMENT NOTIFICATION AS AT ( <?php echo date('Y-m-d'); ?> )</th>
    </tr>
	<tr>
    	<th width="454">Remarks</th>
        <th width="89">Currency</th>
        <th width="131">Process Plan Date</th>
        <th width="153">Process Plan Amount</th>
        <th width="71">Due Days</th>	
    </tr>
    </thead>
    <tbody>
    <?php 
		$sql = "SELECT date(LSD.PAY_DATE) as planDate,
				DATEDIFF(CURRENT_DATE(),PAY_DATE) AS dueDays,
				LSH.REMARKS,
				FC.strCode AS currency,
				LSD.AMOUNT
				FROM finance_loan_schedule_details LSD
				INNER JOIN finance_loan_schedule_header LSH ON LSH.SCHEDULE_NO=LSD.SCHEDULE_NO AND
				LSH.SCHEDULE_YEAR=LSD.SCHEDULE_YEAR
				INNER JOIN mst_financecurrency FC ON FC.intId=LSH.CURRENCY_ID
				WHERE LSD.PAY_DATE<CURRENT_DATE() AND
				LSH.STATUS = 1 AND
				LSD.STATUS = 0
				ORDER BY LSD.PAY_DATE";
		$result = $db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$haveRecords	= true; 
		?>
			<tr style="color:#ff0000">
				<td style="text-align:left"><?php echo $row['REMARKS']; ?></td>
				<td style="text-align:left"><?php echo $row['currency']; ?></td>
				<td style="text-align:center"><?php echo $row['planDate']; ?></td>
				<td style="text-align:right"><?php echo number_format($row['AMOUNT'],2); ?></td>
				<td style="text-align:center"><?php echo $row['dueDays']; ?></td>	
			</tr>
        <?php
		}
		$sql = "SELECT date(LSD.PAY_DATE) as planDate,
				DATEDIFF(PAY_DATE,CURRENT_DATE()) AS dueDays,
				LSH.REMARKS,
				FC.strCode AS currency,
				LSD.AMOUNT
				FROM finance_loan_schedule_details LSD
				INNER JOIN finance_loan_schedule_header LSH ON LSH.SCHEDULE_NO=LSD.SCHEDULE_NO AND
				LSH.SCHEDULE_YEAR=LSD.SCHEDULE_YEAR
				INNER JOIN mst_financecurrency FC ON FC.intId=LSH.CURRENCY_ID
				WHERE PAY_DATE BETWEEN (CURRENT_DATE()) AND (DATE_ADD(CURRENT_DATE(),INTERVAL $diffDays DAY)) AND
				LSH.STATUS = 1 AND
				LSD.STATUS = 0
				ORDER BY LSD.PAY_DATE";
		$result = $db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$haveRecords	= true; 
		?>
			<tr style="color:#000000">
				<td style="text-align:left"><?php echo $row['REMARKS']; ?></td>
				<td style="text-align:left"><?php echo $row['currency']; ?></td>
				<td style="text-align:center"><?php echo $row['planDate']; ?></td>
				<td style="text-align:right"><?php echo number_format($row['AMOUNT'],2); ?></td>
				<td style="text-align:center"><?php echo ($row['dueDays']>0?'&nbsp;':$row['dueDays']); ?></td>	
			</tr>
        <?php
		}
	?>
    </tbody>
</table>
<?php
$body = ob_get_clean();
if($haveRecords)
{
	$mailHeader= "PAYMENT NOTIFICATION";
	
	$sql = "SELECT
			sys_users.strEmail,sys_users.strFullName
			FROM
			sys_mail_eventusers
			Inner Join sys_users ON sys_users.intUserId = sys_mail_eventusers.intUserId
			WHERE
			sys_mail_eventusers.intMailEventId =  '1017' ";
	$result = $db->RunQuery($sql);
	$toEmails = '';
	
	while($row=mysqli_fetch_array($result))
	{
		$toEmails .= $row['strEmail'].',';
	}
	
	sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.',$toEmails,$mailHeader,$body,'','');
}
echo $body;
 
function getDiffrentDays()
{
	global $db;
	
	$sql 	= "SELECT PAYMENT_NOTIFICATION_DAYS FROM sys_config";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	
	return $row['PAYMENT_NOTIFICATION_DAYS'];
}
	 
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$intUser  			= $_SESSION["userId"];

$programCode		= 'P0775';

$menuId				= 775;
$reportId			= 974;

require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

$approveLevel = (int)getMaxApproveLevel();

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'tb1.STATUS',
				'CONCAT_SCHEDULE_NO'=>"CONCAT(tb1.SCHEDULE_NO,'/',tb1.SCHEDULE_YEAR)",
				'CREDITACCNAME'=>'FCOA1.CHART_OF_ACCOUNT_NAME',
				'DEBITACCNAME'=>'FCOA2.CHART_OF_ACCOUNT_NAME',
				'CURRENCY'=>'mst_financecurrency.strCode',
				'SCHEDULE_DATE'=>'tb1.SCHEDULE_DATE'
				);
				
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2','Cancelled'=>'-2','Revised'=>'-1');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND tb1.SCHEDULE_DATE = '".date('Y-m-d')."'";
	
// }

$sql = "SELECT SUB_1.* FROM
			(SELECT
				CONCAT(tb1.SCHEDULE_NO,'/',tb1.SCHEDULE_YEAR)	AS CONCAT_SCHEDULE_NO,
				tb1.SCHEDULE_NO,
				tb1.SCHEDULE_YEAR,
				tb1.CREDIT_ACCOUNT_ID,
				tb1.DEBIT_ACCOUNT_ID,
				FCOA1.CHART_OF_ACCOUNT_NAME AS CREDITACCNAME,
				FCOA2.CHART_OF_ACCOUNT_NAME AS DEBITACCNAME,
				tb1.SCHEDULE_DATE,
				tb1.COMPANY_ID,
				tb1.CREATED_BY,
				tb1.CREATED_DATE,
				tb1.APPROVE_LEVELS,
				if(tb1.STATUS=1,'Approved',if(tb1.STATUS=0,'Rejected',if(tb1.STATUS=-1,'Revised',if(tb1.STATUS=-2,'Cancelled','Pending')))) as Status,
				mst_financecurrency.strCode as CURRENCY ,
				mst_companies.strName AS COMPANY,
				sys_users.strUserName AS CREATOR,
				
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(finance_loan_schedule_approveby.APPROVED_DATE),')' )
								FROM
								finance_loan_schedule_approveby
								Inner Join sys_users ON finance_loan_schedule_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								finance_loan_schedule_approveby.SCHEDULE_NO  = tb1.SCHEDULE_NO AND
								finance_loan_schedule_approveby.SCHEDULE_YEAR =  tb1.SCHEDULE_YEAR AND
								finance_loan_schedule_approveby.APPROVE_LEVEL_NO = '1' AND
								finance_loan_schedule_approveby.STATUS =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
								$approval	= "2nd_Approval";
							}
							else if($i==3){
								$approval	= "3rd_Approval";
							}
							else {
								$approval	= $i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(finance_loan_schedule_approveby.APPROVED_DATE),')' )
								FROM
								finance_loan_schedule_approveby
								Inner Join sys_users ON finance_loan_schedule_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								finance_loan_schedule_approveby.SCHEDULE_NO  = tb1.SCHEDULE_NO AND
								finance_loan_schedule_approveby.SCHEDULE_YEAR =  tb1.SCHEDULE_YEAR AND
								finance_loan_schedule_approveby.APPROVE_LEVEL_NO =  '$i' AND
								finance_loan_schedule_approveby.STATUS =  '0' 
							),
							IF(
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.STATUS>1) AND (tb1.STATUS<=tb1.APPROVE_LEVELS) AND ((SELECT
								concat(sys_users.strUserName )
								FROM
								finance_loan_schedule_approveby
								Inner Join sys_users ON finance_loan_schedule_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								finance_loan_schedule_approveby.SCHEDULE_NO  = tb1.SCHEDULE_NO AND
								finance_loan_schedule_approveby.SCHEDULE_YEAR =  tb1.SCHEDULE_YEAR AND
								finance_loan_schedule_approveby.APPROVE_LEVEL_NO =  ($i-1) AND 
								finance_loan_schedule_approveby.STATUS='0' )<>'')),
								
								'Approve',
								 if($i>tb1.APPROVE_LEVELS,'-----',''))
								
								) as `".$approval."`, "; 
									
								}
								
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(finance_loan_schedule_approveby.APPROVED_DATE),')' )
								FROM
								finance_loan_schedule_approveby
								Inner Join sys_users ON finance_loan_schedule_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								finance_loan_schedule_approveby.SCHEDULE_NO  = tb1.SCHEDULE_NO AND
								finance_loan_schedule_approveby.SCHEDULE_YEAR =  tb1.SCHEDULE_YEAR AND
								finance_loan_schedule_approveby.APPROVE_LEVEL_NO =  '0' AND
								finance_loan_schedule_approveby.STATUS =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS<=tb1.APPROVE_LEVELS AND 
								tb1.STATUS>1),'Reject', '')) as `Reject`,";
								
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(finance_loan_schedule_approveby.APPROVED_DATE),')' )
								FROM
								finance_loan_schedule_approveby
								Inner Join sys_users ON finance_loan_schedule_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								finance_loan_schedule_approveby.SCHEDULE_NO  = tb1.SCHEDULE_NO AND
								finance_loan_schedule_approveby.SCHEDULE_YEAR =  tb1.SCHEDULE_YEAR AND
								finance_loan_schedule_approveby.APPROVE_LEVEL_NO =  '-2' AND
								finance_loan_schedule_approveby.STATUS =  '0'
							),IF(((SELECT
								menupermision.intCancel 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND 
								tb1.STATUS=1),'Cancel', '')) as `Cancel`,";
							
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(finance_loan_schedule_approveby.APPROVED_DATE),')' )
								FROM
								finance_loan_schedule_approveby
								Inner Join sys_users ON finance_loan_schedule_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								finance_loan_schedule_approveby.SCHEDULE_NO  = tb1.SCHEDULE_NO AND
								finance_loan_schedule_approveby.SCHEDULE_YEAR =  tb1.SCHEDULE_YEAR AND
								finance_loan_schedule_approveby.APPROVE_LEVEL_NO =  '-1' AND
								finance_loan_schedule_approveby.STATUS =  '0'
							),IF(((SELECT
								menupermision.intRevise 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND 
								tb1.STATUS=1),'Revise', '')) as `Revise`,";
								
							$sql .= "'View' as `View`   
				
FROM finance_loan_schedule_header as tb1 
				INNER JOIN mst_financecurrency ON tb1.CURRENCY_ID = mst_financecurrency.intId
				INNER JOIN mst_companies ON tb1.COMPANY_ID = mst_companies.intId
				INNER JOIN sys_users ON tb1.CREATED_BY = sys_users.intUserId
				LEFT JOIN finance_mst_chartofaccount FCOA1 ON FCOA1.CHART_OF_ACCOUNT_ID = tb1.CREDIT_ACCOUNT_ID
				LEFT JOIN finance_mst_chartofaccount FCOA2 ON FCOA2.CHART_OF_ACCOUNT_ID = tb1.DEBIT_ACCOUNT_ID
				WHERE tb1.COMPANY_ID='$session_companyId'
				$where_string
 
		)  
		AS SUB_1 WHERE 1=1";
				
$jq 	= new jqgrid('',$db);	
$g 		= new jqgrid();
$col 	= array();
$cols 	= array();

//BEGIN - ACTIVE/INACTIVE {
$col["title"] 				= "Status";
$col["name"] 				= "Status";
$col["width"] 				= "3"; 						// not specifying width will expand to fill space
$col["align"] 				= "center";
$col["sortable"] 			= true; 						// this column is not sortable
$col["search"] 				= true; 						// this column is not searchable
$col["editable"] 			= false;
$col["stype"] 				= "select";
$str 						= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Revised:Revised;Cancelled:Cancelled" ;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;	
$col						= NULL;
//END 	- ACTIVE/INACTIVE }

//BEGIN	- BANK PAYMENT NO - HIDDEN {
$col["title"] 				= "scheduleNo";
$col["name"] 				= "SCHEDULE_NO";
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;				
$cols[] 					= $col;	
$col						= NULL;

//END 	- BANK PAYMENT NO - HIDDEN }

//BEGIN	- BANK PAYMENT YEAR - HIDDEN {
$col["title"] 				= "scheduleYear";
$col["name"] 				= "SCHEDULE_YEAR";
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;					
$cols[] 					= $col;	
$col						= NULL;
//END 	- BANK PAYMENT YEAR - HIDDEN }

//BEGIN	- BANK PAYMENT NO {
$col["title"] 				= "Serial No";
$col["name"] 				= "CONCAT_SCHEDULE_NO";
$col["width"] 				= "2";
$col["align"] 				= "center";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col['link']				= '?q='.$menuId.'&scheduleNo={SCHEDULE_NO}&scheduleYear={SCHEDULE_YEAR}';
$col["linkoptions"] 		= "target='loan_schedule.php'";
$cols[] 					= $col;	
$col						= NULL;
//END 	- BANK PAYMENT NO }

//BEGIN - BANK GL {
$col["title"] 				= "Credit Account";
$col["name"] 				= "CREDITACCNAME";
$col["width"] 				= "6"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "CREDIT_ACCOUNT_ID";
$client_lookup 				= $jq->get_dropdown_values("SELECT DISTINCT 
															FCOA.CHART_OF_ACCOUNT_ID AS k,
															CONCAT(FCOA.CHART_OF_ACCOUNT_NAME,' | ',FMT.FINANCE_TYPE_CODE,FMMT.MAIN_TYPE_CODE,FMST.SUB_TYPE_CODE,FCOA.CHART_OF_ACCOUNT_CODE) AS v
														
														FROM finance_mst_chartofaccount FCOA
														INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
														INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
														INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
														INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
														WHERE FCOA.PAYMENT_SCHEDULE='1' AND FCOAC.COMPANY_ID='$session_companyId' AND FCOA.CATEGORY_TYPE='N'
														ORDER BY CHART_OF_ACCOUNT_NAME");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;
//END 	- BANK GL }
$col["title"] 				= "Debit Account";
$col["name"] 				= "DEBITACCNAME";
$col["width"] 				= "6"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "DEBIT_ACCOUNT_ID";
$client_lookup 				= $jq->get_dropdown_values("SELECT DISTINCT 
															FCOA.CHART_OF_ACCOUNT_ID AS k,
															CONCAT(FCOA.CHART_OF_ACCOUNT_NAME,' | ',FMT.FINANCE_TYPE_CODE,FMMT.MAIN_TYPE_CODE,FMST.SUB_TYPE_CODE,FCOA.CHART_OF_ACCOUNT_CODE) AS v
														
														FROM finance_mst_chartofaccount FCOA
														INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
														INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
														INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
														INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
														WHERE FCOA.PAYMENT_SCHEDULE='1' AND FCOAC.COMPANY_ID='$session_companyId' AND FCOA.CATEGORY_TYPE='N'
														ORDER BY CHART_OF_ACCOUNT_NAME");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;
//BEGIN - CURRENCY {
$col["title"] 				= "Currency";
$col["name"] 				= "CURRENCY";
$col["width"] 				= "2";
$col["align"] 				= "center";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$cols[] 					= $col;	
$col						= NULL;
//END	- CURRENCY	}

//BEGIN - REFERENCE NO {
$col["title"] 				= "Schedule Date";
$col["name"] 				= "SCHEDULE_DATE";
$col["width"] 				= "3";
$col["align"] 				= "center";
$col["align"] 				= "left";
$cols[] 					= $col;	
$col						= NULL;
//END 	- REFERENCE NO }

//BEGIN - FIRST APPROVAL {
$col["title"] 				= "1st Approval";
$col["name"] 				= "1st_Approval";
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q='.$reportId.'&scheduleNo={SCHEDULE_NO}&scheduleYear={SCHEDULE_YEAR}&mode=Confirm';
$col["linkoptions"] 		= "target='_blank'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;
//END	- FIRST APPROVEL }

//BEGIN - SECOND & MORE APPROVAL {
for($i=2; $i<=$approveLevel; $i++)
{
	if($i==2){
		$ap		= "2nd Approval";
		$ap1	= "2nd_Approval";
	}
	else if($i==3){
		$ap		= "3rd Approval";
		$ap1	= "3rd_Approval";
	}
	else {
		$ap		= $i."th Approval";
		$ap1	= $i."th_Approval";
	}

$col["title"] 				= $ap; // caption of column
$col["name"] 				= $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q='.$reportId.'&scheduleNo={SCHEDULE_NO}&scheduleYear={SCHEDULE_YEAR}&mode=Confirm';
$col["linkoptions"] 		= "target='_blank'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;
}
//END 	- SECOND & MORE APPROVAL {

/*//BEGIN - REJECT {
$col["title"] 				= 'Reject'; // caption of column
$col["name"] 				= 'Reject'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= 'rptBankReconciliations.php?recNo={RECONCILIATION_NO}&recYear={RECONCILIATION_YEAR}&mode=Reject';
$col["linkoptions"] 		= "target='rptBankReconciliations.php'";
$col['linkName']			= 'Reject';
$cols[] 					= $col;	
$col						= NULL;
//END 	- REJECT }*/
$col["title"] 				= 'Revise'; // caption of column
$col["name"] 				= 'Revise'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q='.$reportId.'&scheduleNo={SCHEDULE_NO}&scheduleYear={SCHEDULE_YEAR}&mode=Revise';
$col["linkoptions"] 		= "target='_blank'";
$col['linkName']			= 'Revise';
$cols[] 					= $col;	
$col						= NULL;
//BEGIN - CANCEL {
$col["title"] 				= 'Cancel'; // caption of column
$col["name"] 				= 'Cancel'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q='.$reportId.'&scheduleNo={SCHEDULE_NO}&scheduleYear={SCHEDULE_YEAR}&mode=Cancel';
$col["linkoptions"] 		= "target='_blank'";
$col['linkName']			= 'Cancel';
$cols[] 					= $col;	
$col						= NULL;
//END 	- CANCEL }

//BEGIN - REPORT {
$col["title"] 				= "Report"; // caption of column
$col["name"] 				= "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "2";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q='.$reportId.'&scheduleNo={SCHEDULE_NO}&scheduleYear={SCHEDULE_YEAR}';
$col["linkoptions"] 		= "target='_blank'";
$cols[] 					= $col;	
$col						= NULL;
//END 	- REPORT }
 
$grid["caption"] 			= "Loan Schedule Listing";
$grid["multiselect"] 		= false;
$grid["rowNum"] 			= 20; // by default 20
$grid["sortname"] 			= 'SCHEDULE_YEAR,SCHEDULE_NO'; // by default sort grid by this field
$grid["sortorder"] 			= "DESC"; // ASC or DESC
$grid["autowidth"] 			= true; // expand grid to screen width
$grid["multiselect"] 		= false; // allow you to multi-select through checkboxes
$grid["search"] 			= true; 
$grid["postData"] 			= array("filters" => $sarr ); 
$grid["export"] 			= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;

//$grid["postData"] = array("filters" => $sarr ); 

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Loan Schedule Listing</title>
<?php
echo $out;

function getMaxApproveLevel()
{
	global $db;
	$appLevel = 0;
	
	$sqlp = "SELECT
			Max(finance_loan_schedule_header.APPROVE_LEVELS) AS appLevel
			FROM finance_loan_schedule_header";					
	$resultp 	= $db->RunQuery($sqlp);
	$rowp		= mysqli_fetch_array($resultp);			 
	return $rowp['appLevel'];
}
?>

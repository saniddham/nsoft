<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId				= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$session_userId 		= $_SESSION['userId'];

include_once  "class/finance/cls_get_gldetails.php";
include_once  "class/cls_commonFunctions_get.php";
include_once  "class/finance/loan_schedule/cls_loan_schedule_get.php";
include_once  "class/cls_commonErrorHandeling_get.php";
include_once  "class/finance/cls_common_get.php";
//include 	  "include/javascript.html";


$obj_get_GLCombo		= new Cls_Get_GLDetails($db);
$obj_schedule_get		= new Cls_Schedule_Get($db);
$obj_commonErrHandle	= new cls_commonErrorHandeling_get($db);
$obj_common				= new cls_commonFunctions_get($db);
$obj_common_get			= new Cls_Common_Get($db);

$programCode			= 'P0775';
$scheduleNo				= (!isset($_REQUEST['scheduleNo'])?'':$_REQUEST['scheduleNo']);
$scheduleYear			= (!isset($_REQUEST['scheduleYear'])?'':$_REQUEST['scheduleYear']);

$header_arr				= $obj_schedule_get->loadHeaderData($scheduleNo,$scheduleYear);
$detail_result			= $obj_schedule_get->loadDetailData($scheduleNo,$scheduleYear);

$intStatus				= $header_arr['STATUS'];
$levels					= $header_arr['APPROVE_LEVELS'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_save($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_save			= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_cancel($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_cancel		= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_reject($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_reject		= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_confirm		= $permition_arr['permision'];

$dateChangeMode 		= $obj_common->ValidateSpecialPermission('33',$session_userId,'RunQuery');

?>
<script type="text/javascript" >
	var curDate			= '<?php echo date('Y-m-d'); ?>';
</script>

<title>Loan Schedule</title>

<!--<script type="text/javascript" src="presentation/finance_new/loan_schedule/loan_schedule_js.js"></script>-->

<form id="frmLoanSchedule" name="frmLoanSchedule" autocomplete="off" action="loan_schedule.php" method="post">
<div align="center">
<div class="trans_layoutS" style="width:800px">
<div class="trans_text">Loan Schedule</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr class="normalfnt">
                <td width="14%" class="normalfnt">Schedule No</td>
                <td width="40%" class="normalfnt"><input name="txtScheduleNo" type="text" disabled="disabled" id="txtScheduleNo" style="width:80px" value="<?php echo $scheduleNo; ?>" />&nbsp;<input name="txtScheduleYear" type="text" disabled="disabled" id="txtScheduleYear" style="width:50px" value="<?php echo $scheduleYear; ?>" /></td>
                <td width="18%" class="normalfnt">Date</td>
                <td width="28%" class="normalfnt"><input name="txtDate" type="text" value="<?php echo($header_arr['SCHEDULE_DATE']==''?date("Y-m-d"):$header_arr['SCHEDULE_DATE']); ?>" class="validate[required] cls_txt_DateCalExchangeRate" id="txtDate" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($dateChangeMode!=1?'disabled="disabled"':''); ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
            </tr>
            <tr class="normalfnt">
                <td>Currency <span class="compulsoryRed">*</span></td>
                <td class="normalfnt"><select name="cboCurrency" id="cboCurrency"  style="width:80px" class="validate[required] cls_cbo_CurrencyCalExchangeRate" >
                <?php
                echo $obj_common_get->getCurrencyCombo($header_arr['CURRENCY_ID']);
                ?>
                </select></td>
                <td class="normalfnt">Rate</td>
                <td class="normalfnt"><input type="text" name="txtCurrencyRate" id="txtCurrencyRate" style="width:100px;text-align:right" disabled="disabled" class="cls_txt_exchangeRate" /></td>
            </tr>
            <tr class="normalfnt">
              <td valign="top">Remarks <span class="compulsoryRed">*</span></td>
              <td class="normalfnt"><textarea name="txtRemarks" id="txtRemarks" style="width:200px" rows="2" class="validate[required]"><?php echo $header_arr['REMARKS']; ?></textarea></td>
              <td valign="top" class="normalfnt">Loan Amount <span class="compulsoryRed">*</span></td>
              <td valign="top" class="normalfnt"><input type="text" name="txtLoanAmount" id="txtLoanAmount" style="width:100px;text-align:right" /></td>
            </tr>
            <tr>
            	<td colspan="4">
                <table width="100%" border="0" class="bordered" id="tblMain">
              <thead>
                <tr>
                  <th colspan="6">Schedule<div style="float:right"><a class="button white small" id="butInsertRow">Add New Row</a></div></th>
                </tr>
                <tr>
                  <th width="6%">Del</th>
                  <th width="23%">Date</th>
                  <th width="29%">Amount</th>
                  <th width="18%">Status</th>
                  <th width="24%">Processed Date</th>
                </tr>
              </thead>
              <tbody>
              <?php
				$totAmount 	= 0;
				$count		= 2;
				if($scheduleNo!='' && $scheduleYear!='')
				{
					while($row = mysqli_fetch_array($detail_result))
					{
						$totAmount	+= $row['AMOUNT'];
					?>
                        <tr <?php echo($count==2?'class="cls_tr_firstRow"':''); ?> id="<?php echo $row['ORDER_BY']; ?>">
                            <td  style="text-align:center" class="clsDelete"><?php echo($row['STATUS']==1?'&nbsp;':'<img src="images/del.png" class="removeRow mouseover"/>'); ?></td>
                            <td  style="text-align:center"><input name="txtPayDate" type="text" value="<?php echo $row['PAY_DATE'] ?>" class="clsPayDate" id="txtPayDate~<?php echo $count; ?>" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($row['STATUS']==1?'disabled="disabled"':''); ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                            <td  style="text-align:center"><input name="txtAmount" type="text" id="txtAmount" style="width:150px;text-align:right" class="clsAmount" value="<?php echo $row['AMOUNT']; ?>" <?php echo($row['STATUS']==1?'disabled="disabled"':''); ?>/></td>
                            <td style="text-align:center" class="clsStatus" id="<?php echo $row['STATUS']; ?>"><?php echo ($row['STATUS']==1?'<img src="images/accept.png">':($intStatus==1?'<a class="button green small clsProcess" id="butProcess">Process</a>':'&nbsp;')); ?></td>
                            <td style="text-align:center" class="clsProcessedDate"><?php echo ($row['PROCESS_DATE']==NULL?'&nbsp;':$row['PROCESS_DATE']); ?></td>
                        </tr>
                    <?php
					$count++;
					}
					for($i=$count;$i<12;$i++)
					{
					?> 
                        <tr <?php echo($i==2?'class="cls_tr_firstRow"':''); ?> id="">
                            <td  style="text-align:center" class="clsDelete"><img src="images/del.png" class="removeRow mouseover"/></td>
                            <td  style="text-align:center"><input name="txtPayDate" type="text" value="" class="clsPayDate" id="txtPayDate~<?php echo $i; ?>" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                            <td  style="text-align:center"><input name="txtAmount" type="text" id="txtAmount" style="width:150px;text-align:right" class="clsAmount"/></td>
                            <td class="clsStatus" id="0">&nbsp;</td>
                            <td class="clsProcessedDate">&nbsp;</td>
                        </tr>
					<?php
					}
				}
			else
			{ 
			  for($i=2;$i<12;$i++)
			  {
			  ?> 
                <tr <?php echo($i==2?'class="cls_tr_firstRow"':''); ?> id="">
                  <td  style="text-align:center" class="clsDelete"><img src="images/del.png" class="removeRow mouseover"/></td>
                  <td  style="text-align:center"><input name="txtPayDate" type="text" value="" class="clsPayDate" id="txtPayDate~<?php echo $i; ?>" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                  <td  style="text-align:center"><input name="txtAmount" type="text" id="txtAmount" style="width:150px;text-align:right" class="clsAmount"/></td>
                  <td class="clsStatus" id="0">&nbsp;</td>
                  <td class="clsProcessedDate">&nbsp;</td>
                </tr>
             <?php
			  }
			}
			 ?>  
              </tbody>
              <tfoot>
              <tr style="font-weight:bold">
                  <td>&nbsp;</td>
                  <td style="text-align:center">TOTAL</td>
                  <td class="cls_td_totAmount" style="text-align:right"><?php echo number_format($totAmount); ?></td>
                  <td style="text-align:right">&nbsp;</td>
                  <td style="text-align:right">&nbsp;</td>
                </tr>
              </tfoot>
            </table>
                </td>
            </tr>
            </table>
       	</td>
    </tr>
    <tr>
                <td height="32" >
                    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                        <tr>
                            <td align="center" class="tableBorder_allRound"><a class="button white medium" id="butNew">New</a><a class="button white medium" id="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a><a class="button white medium" id="butConfirm"  <?php if($permision_confirm!=1){ ?> style="display:none"<?php } ?>>Approve</a><a class="button white medium" id="butCancel" <?php if($permision_cancel!=1){ ?>  style="display:none"<?php } ?>>Cancel</a><a class="button white medium" id="butReport">Report</a><a href="main.php" class="button white medium" id="butClose">Close</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
</table>
</div>
</div>
</form>
<script>
GetCommonExchangeRate();
</script>

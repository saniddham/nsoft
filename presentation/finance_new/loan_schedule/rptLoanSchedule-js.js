// JavaScript Document
var basePath	= "presentation/finance_new/loan_schedule/";
$(document).ready(function(){
	
	$("#frmRptLoanSchedule").validationEngine();
	
	$('#frmRptLoanSchedule #butRptConfirm').die('click').live('click',ConfirmRpt);
	$('#frmRptLoanSchedule #butRptReject').die('click').live('click',RejectRpt);
	$('#frmRptLoanSchedule #butRptCancel').die('click').live('click',CancelRpt);
	$('#frmRptLoanSchedule #butRptRevise').die('click').live('click',ReviseRpt);
});
function ConfirmRpt()
{
	var val = $.prompt('Are you sure you want to Approve this Schedule ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"loan_schedule_db.php"+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptLoanSchedule #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx1()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptLoanSchedule #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx1()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function RejectRpt()
{
	var val = $.prompt('Are you sure you want to Reject this Schedule ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"loan_schedule_db.php"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptLoanSchedule #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptLoanSchedule #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx2()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function CancelRpt()
{
	var val = $.prompt('Are you sure you want to Cancel this Schedule ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"loan_schedule_db.php"+window.location.search+'&requestType=cancel';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptLoanSchedule #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx3()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptLoanSchedule #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx3()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function ReviseRpt()
{
	var val = $.prompt('Are you sure you want to Revise this Schedule ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"loan_schedule_db.php"+window.location.search+'&requestType=revise';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptLoanSchedule #butRptRevise').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx4()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptLoanSchedule #butRptRevise').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx4()",4000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function alertx1()
{
	$('#frmRptLoanSchedule #butRptConfirm').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmRptLoanSchedule #butRptReject').validationEngine('hide')	;
}
function alertx3()
{
	$('#frmRptLoanSchedule #butRptCancel').validationEngine('hide')	;
}
function alertx4()
{
	$('#frmRptLoanSchedule #butRptRevise').validationEngine('hide')	;
}
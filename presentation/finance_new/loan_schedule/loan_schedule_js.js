// JavaScript Document
var basePath	= "presentation/finance_new/loan_schedule/";
var reportId	= 974;
var menuId		= 775;
$(document).ready(function(){
	
	$("#frmLoanSchedule").validationEngine();
	
	/*if(intAddx)
	{
		$('#frmLoanSchedule #butNew').show();
		
		if(status==-2)
			$('#frmLoanSchedule #butSave').hide();
		else
			$('#frmLoanSchedule #butSave').show();
	}
	//permision for edit 
	if(intEditx)
	{
		if(status==-2)
			$('#frmLoanSchedule #butSave').hide();
		else
			$('#frmLoanSchedule #butSave').show();
	}*/
	
	$('#frmLoanSchedule #butInsertRow').die('click').live('click',addNewRow);
	$('#frmLoanSchedule .removeRow').die('click').live('click',deleteRow);
	$('#frmLoanSchedule .clsPayDate').die('blur').live('blur',function(){addValidateClass(this,'Date')});
	$('#frmLoanSchedule .clsAmount').die('blur').live('blur',function(){addValidateClass(this,'Amount')});
	$('#frmLoanSchedule #butSave').die('click').live('click',saveData);
	$('#frmLoanSchedule .clsAmount').die('keyup').live('keyup',calculateTotal);
	$('#frmLoanSchedule #butNew').die('click').live('click',clearAll);
	
	$('#frmLoanSchedule #butConfirm').die('click').live('click',Confirm);
	$('#frmLoanSchedule #butCancel').die('click').live('click',Cancel);
	$('#frmLoanSchedule #butReport').die('click').live('click',Report);
	$('#frmLoanSchedule .clsProcess').die('click').live('click',process);
	
	
});
function addNewRow()
{
	var lastDateIdArr 	= $('#frmLoanSchedule #tblMain tbody tr:last').find('.clsPayDate').attr('id').split('~');
	var lastDateNo		= parseFloat(lastDateIdArr[1]);
	$('#frmLoanSchedule #tblMain tbody tr:last').after("<tr>"+$('#frmLoanSchedule #tblMain .cls_tr_firstRow').html()+"</tr>");
	$('#frmLoanSchedule #tblMain tbody tr:last').find('.clsPayDate').attr('id','txtPayDate~'+(lastDateNo+1));
	$('#frmLoanSchedule #tblMain tbody tr:last').find('.clsPayDate').val('');
	$('#frmLoanSchedule #tblMain tbody tr:last').find('.clsPayDate').removeAttr('disabled');
	$('#frmLoanSchedule #tblMain tbody tr:last').find('.clsAmount').val('');
	$('#frmLoanSchedule #tblMain tbody tr:last').find('.clsAmount').removeAttr('disabled');
	$('#frmLoanSchedule #tblMain tbody tr:last').find('.clsStatus').html('&nbsp;');
	$('#frmLoanSchedule #tblMain tbody tr:last').find('.clsProcessedDate').html('&nbsp;');
	
	if($('#frmLoanSchedule #tblMain tbody tr:last').find('.clsStatus').attr('id')==1)
	{
		$('#frmLoanSchedule #tblMain tbody tr:last').find('.clsDelete').html('<img src="images/del.png" class="removeRow mouseover"/>');
	}
	$('#frmLoanSchedule #tblMain tbody tr:last').find('.clsStatus').attr('id',0);
}
function deleteRow()
{
	var delRowCount = parseInt(document.getElementById('tblMain').rows.length);
	
	if(delRowCount>4)
		$(this).parent().parent().remove();
	
	$('#frmLoanSchedule #tblMain tbody tr').removeClass('cls_tr_firstRow');
	$('#frmLoanSchedule #tblMain tbody tr:first').addClass('cls_tr_firstRow');
}
function addValidateClass(obj,type)
{
	switch(type)
	{
		case 'Date' :
			if($(obj).val()!='')
				$(obj).parent().parent().find('.clsAmount').addClass('validate[required,custom[number]]');
			else
				$(obj).parent().parent().find('.clsAmount').removeClass('validate[required,custom[number]]');
		case 'Amount' :
			if($(obj).val()!='')
				$(obj).parent().parent().find('.clsPayDate').addClass('validate[required]');
			else
				$(obj).parent().parent().find('.clsPayDate').removeClass('validate[required]');
	}
}
function calculateTotal()
{
	var totalValue		= 0;
	
	$('#frmLoanSchedule #tblMain .clsAmount').each(function(){
	
			totalValue += parseFloat(($(this).val()==''?0:$(this).val()));
	});
	
	totalValue	 = RoundNumber(totalValue,2);
	
	$('#frmLoanSchedule #tblMain .cls_td_totAmount').html(totalValue);	
}
function saveData()
{
	if(!IsProcessMonthLocked_date($('#txtDate').val()))
		return;
		
	showWaiting();
	var scheduleNo		= $('#frmLoanSchedule #txtScheduleNo').val();
	var scheduleYear	= $('#frmLoanSchedule #txtScheduleYear').val();
	var date			= $('#frmLoanSchedule #txtDate').val();
	var currency		= $('#frmLoanSchedule #cboCurrency').val();
	//var creditGL		= $('#cboCreditAccount').val();		//already bank payment journal entry will raise from finance
	//var debitGL		= $('#cboDebitAccount').val();		//already bank payment journal entry will raise from finance
	var remarks			= $('#frmLoanSchedule #txtRemarks').val();
	var totalAmonut		= parseFloat($('#frmLoanSchedule #tblMain .cls_td_totAmount').html());
	var enterdAmount	= parseFloat($('#frmLoanSchedule #txtLoanAmount').val());
	
	if(totalAmonut!=enterdAmount)
	{
		alert("Total loan amount and schedule loan amount should equal.");
		hideWaiting();
		return;
	}
	
	if($('#frmLoanSchedule').validationEngine('validate'))
	{
		
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"scheduleNo":"'+scheduleNo+'",' ;
							arrHeader += '"scheduleYear":"'+scheduleYear+'",' ;
							arrHeader += '"date":"'+date+'",' ;
							arrHeader += '"currency":"'+currency+'",' ;
							arrHeader += '"remarks":'+URLEncode_json(remarks)+',';
							arrHeader += '"totalAmonut":"'+totalAmonut+'"' ;

			arrHeader += "}";
		
		var chkStatus	= false;
		var chkDtStatus	= true;
		var arrDetails	= "";
		
		$('#frmLoanSchedule .clsPayDate').each(function(){
			
			var payDate 	= $(this).val();	
			var amount	 	= $(this).parent().parent().find('.clsAmount').val();
			var rowId		= $(this).parent().parent().attr('id');
			
			if(payDate!='' && amount!='')
			{
				if($(this).parent().parent().find('.clsStatus').attr('id')==0 && payDate<curDate)
				{
					$(this).validationEngine('showPrompt','Date must greater than current Date.','fail');
					chkDtStatus = false;
					hideWaiting();	
					return false;
				}
				chkStatus	= true;
				arrDetails += "{";
				arrDetails += '"payDate":"'+ payDate +'",' ;
				arrDetails += '"amount":"'+ amount +'",' ;
				arrDetails += '"rowId":"'+ rowId +'"' ;
				arrDetails += "},";
			}
		});
		if(!chkDtStatus)
			return;
		
		if(!chkStatus)
		{
			$(this).validationEngine('showPrompt','No records to save.','fail');
			hideWaiting();	
			return;
		}
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		
		var arrHeader	= arrHeader;
		var arrDetails	= '['+arrDetails+']';
		data+="&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		var url = basePath+"loan_schedule_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmLoanSchedule #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						$('#frmLoanSchedule #txtScheduleNo').val(json.scheduleNo);
						$('#frmLoanSchedule #txtScheduleYear').val(json.scheduleYear);
						$('#frmLoanSchedule #butConfirm').show();
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
						$('#frmLoanSchedule #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
	}
	else
	{
		hideWaiting();
	}
}

function Confirm()
{
	var url  = "?q="+reportId+"&scheduleNo="+$('#frmLoanSchedule #txtScheduleNo').val();
	    url += "&scheduleYear="+$('#frmLoanSchedule #txtScheduleYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rpt_loan_schedule.php');
}
function Cancel()
{
	var url  = "?q="+reportId+"&scheduleNo="+$('#frmLoanSchedule #txtScheduleNo').val();
	    url += "&scheduleYear="+$('#frmLoanSchedule #txtScheduleYear').val();
	    url += "&mode=Cancel";
	window.open(url,'rpt_loan_schedule.php');
}
function Report()
{
	if($('#frmLoanSchedule #txtScheduleNo').val()=='')
	{
		$('#frmLoanSchedule #butReport').validationEngine('showPrompt','No schedule no to view Report','fail');
		return;	
	}
	var url  = "?q="+reportId+"&scheduleNo="+$('#frmLoanSchedule #txtScheduleNo').val();
	    url += "&scheduleYear="+$('#frmLoanSchedule #txtScheduleYear').val();
	window.open(url,'rpt_loan_schedule.php');
}
function process()
{
	var rowId			= $(this).parent().parent().attr('id');
	var scheduleNo		= $('#frmLoanSchedule #txtScheduleNo').val();
	var scheduleYear	= $('#frmLoanSchedule #txtScheduleYear').val();
	var obj				= this;
	
	var val = $.prompt('Are you sure you want to Process this Schedule ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						showWaiting();
	
						var url 			= basePath+"loan_schedule_db.php";
						var data 			= "requestType=process&rowId="+rowId+"&scheduleNo="+scheduleNo+"&scheduleYear="+scheduleYear;
							
						var httpobj = $.ajax({
						url:url,
						data:data,
						dataType:'json',
						type:'POST',
						async:false,
						success:function(json){
									$(obj).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
									if(json.type=='pass')
									{
										var t = setTimeout("alertx2()",3000);
										
										$(obj).parent().parent().find('.clsDelete').html('&nbsp;');
										$(obj).parent().parent().find('.clsPayDate').attr("disabled", "disabled");
										$(obj).parent().parent().find('.clsAmount').attr("disabled", "disabled");
										$(obj).parent().parent().find('.clsStatus').attr('id',1);
										$(obj).parent().parent().find('.clsProcessedDate').html(json.processDate);
										$(obj).parent().parent().find('.clsStatus').html('<img src="images/accept.png">');
										hideWaiting();
										return;
									}
									else
									{
										hideWaiting();
										return;
									}
								},
								error:function(xhr,status){
											
										$(obj).validationEngine('showPrompt', errormsg(xhr.status),'fail');
										hideWaiting();
										return;
								}	
						});
					}
				}});
}
function clearAll()
{
	window.location.href = '?q='+menuId;
}
function alertx()
{
	$('#frmLoanSchedule #butSave').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmLoanSchedule #tblMain').validationEngine('hide')	;
}
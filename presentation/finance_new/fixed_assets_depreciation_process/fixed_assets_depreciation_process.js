// JavaScript Document
var basePath	= "presentation/finance_new/fixed_assets_depreciation_process/";
$(document).ready(function(){
	
	$("#frmFixedAssetsProcess").validationEngine();
	
	$('#frmFixedAssetsProcess #butProcess').die('click').live('click',fixedAssetsProcess);
});
function fixedAssetsProcess()
{
	showWaiting();
	if($('#frmFixedAssetsProcess').validationEngine('validate'))
	{
		var year 	= $('#frmFixedAssetsProcess #cboYear').val();
		var month 	= $('#frmFixedAssetsProcess #cboMonth').val();
		
		var url = basePath+"fixed_assets_depreciation_process_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:"requestType=process&year="+year+"&month="+month,
				async:false,
				success:function(json){
					$('#frmFixedAssetsProcess #butProcess').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
						return;
					}
				},
				error:function(xhr,status){
						
						$('#frmFixedAssetsProcess #butProcess').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
		
	}
	else
	{
		hideWaiting();
		return;
	}
}
function alertx()
{
	$('#frmFixedAssetsProcess #butProcess').validationEngine('hide')	;
}
<?php
session_start();
$backwardseperator 	= "../../../";
include_once "{$backwardseperator}dataAccess/Connector.php";

include_once "../../../class/finance/fixedAssets/fixedAssetsDepreciationProcess/cls_fixed_assets_depreciation_process_set.php";
include_once "../../../class/finance/fixedAssets/fixedAssetsDepreciationProcess/cls_fixed_assets_depreciation_process_get.php";
include_once "../../../class/cls_commonFunctions_get.php";

$obj_fixedAProcess_set	= new Cls_Fixed_Assets_Depreciation_Process_Set($db);
$obj_fixedAProcess_get	= new Cls_Fixed_Assets_Depreciation_Process_Get($db);
$obj_common				= new cls_commonFunctions_get($db);

$requestType 			= $_REQUEST['requestType'];
$locationId				= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$session_userId			= $_SESSION["userId"];

$savedStatus			= true;
$savedMasseged			= '';
$error_sql				= '';

if($requestType=='process')
{
	$year				= $_REQUEST["year"];
	$month				= $_REQUEST["month"];
	$db->begin();
	
	validateBeforeProcess($year,$month);
	
	$sysNo_arry 		= $obj_common->GetSystemMaxNo('intFixedAssetDepNo',$locationId);
	if($sysNo_arry["rollBackFlag"]==1)
	{
		if($savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $sysNo_arry["msg"];
			$error_sql		= $sysNo_arry["q"];
		}
		
	}
	$fixedAssetDepNo		= $sysNo_arry["max_no"];
	$fixedAssetDepYear		= date('Y');
	
	saveFinanceTransaction($fixedAssetDepNo,$fixedAssetDepYear,$year,$month);
	updateFixedAssetRegistry($year,$month);
	savedFixedAssetProcess($fixedAssetDepNo,$fixedAssetDepYear,$year,$month);
	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Saved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
}
function validateBeforeProcess($year,$month)
{
	global $savedStatus;
	global $savedMasseged;
	global $obj_fixedAProcess_get;
	global $companyId;
	
	if((date('m')<=$month && date('Y')==$year) && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= "System allow to process only the past month data.";
	}
	
	$result	 = $obj_fixedAProcess_get->validateSave($year,$month,$companyId,'RunQuery2');
	if(mysqli_num_rows($result)>0 && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= "Fixed assets depreciation process already processed for the selected month.";
	}
}
function saveFinanceTransaction($fixedAssetDepNo,$fixedAssetDepYear,$year,$month)
{
	global $savedStatus;
	global $savedMasseged;
	global $error_sql;
	global $obj_fixedAProcess_set;
	global $obj_fixedAProcess_get;
	global $companyId;
	global $locationId;
	global $session_userId;
	
	$lastDate = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
	$result	  = $obj_fixedAProcess_get->getDetails($lastDate,'RunQuery2');
	if(mysqli_num_rows($result)==0 && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= "No data to process for selected month";
	}
	while($row = mysqli_fetch_array($result))
	{
		if(($row['debitAccount']=='' || $row['creditAccount']=='') && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= "Chart of account not assign for Some sub categories.";
			return;
		}
		else
		{
			$resultArr = $obj_fixedAProcess_set->saveFinanceTransaction($fixedAssetDepNo,$fixedAssetDepYear,$row['debitAccount'],$row['depAmount'],'FIXED_ASSET_DEP','D','FA',$companyId,$locationId,$session_userId,$lastDate);
			
			if($resultArr['savedStatus']=='fail' && ($savedStatus))
			{
				$savedStatus	= false;
				$savedMasseged	= $resultArr['savedMassege'];
				$error_sql		= $resultArr['error_sql'];
			}
			
			$resultArr = $obj_fixedAProcess_set->saveFinanceTransaction($fixedAssetDepNo,$fixedAssetDepYear,$row['creditAccount'],$row['depAmount'],'FIXED_ASSET_DEP','C','FA',$companyId,$locationId,$session_userId,$lastDate);
			
			if($resultArr['savedStatus']=='fail' && ($savedStatus))
			{
				$savedStatus	= false;
				$savedMasseged	= $resultArr['savedMassege'];
				$error_sql		= $resultArr['error_sql'];
			}
		}
	}
}
function updateFixedAssetRegistry($year,$month)
{
	global $savedStatus;
	global $savedMasseged;
	global $error_sql;
	global $obj_fixedAProcess_set;
	global $obj_fixedAProcess_get;
	global $companyId;
	
	$lastDate = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
	$result	  = $obj_fixedAProcess_get->getFixedAssetDetails($lastDate,'RunQuery2');
	if(mysqli_num_rows($result)==0 && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= "No data to process for selected month";
		return;
	}
	while($row = mysqli_fetch_array($result))
	{
		$resultArr	= $obj_fixedAProcess_set->updateFixedAssetRegistry($row['ID'],$row['depAmount']);
		if($resultArr['savedStatus']=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= $resultArr['savedMassege'];
			$error_sql		= $resultArr['error_sql'];
		}
	}
}
function savedFixedAssetProcess($fixedAssetDepNo,$fixedAssetDepYear,$year,$month)
{
	global $savedStatus;
	global $savedMasseged;
	global $error_sql;
	global $obj_fixedAProcess_set;
	global $companyId;
	global $session_userId;
	
	$resultArr	= $obj_fixedAProcess_set->savedFixedAssetProcess($fixedAssetDepNo,$fixedAssetDepYear,$year,$month,$companyId,$session_userId);
	if($resultArr['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= $resultArr['savedMassege'];
		$error_sql		= $resultArr['error_sql'];
	}
}
?>
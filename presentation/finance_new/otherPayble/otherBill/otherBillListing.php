<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId			= $_SESSION["headCompanyId"];
$menuId				= 729;
$reportId			= 979;

require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'STATUS'=>'OPH.STATUS',
				'billInvNo'=>"CONCAT(OPH.BILL_INVOICE_NO,' / ',OPH.BILL_INVOICE_YEAR)",
				'supplier'=>'MS.strName',
				'BILL_DATE'=>'OPH.BILL_DATE',
				'strCode'=>'FC.strCode',
				'REMARKS'=>'OPH.REMARKS'
				);
				
$arr_status = array('Saved'=>'1','Cancelled'=>'-2');
foreach($arr as $k=>$v)
{
	if($v['field']=='STATUS')
	{
		$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND OPH.BILL_DATE = '".date('Y-m-d')."'";
	
// }

$sql = "SELECT SUB_1.* FROM
		(
		SELECT IF(OPH.STATUS=-2,'Cancelled','Saved') AS STATUS,
		CONCAT(OPH.BILL_INVOICE_NO,' / ',OPH.BILL_INVOICE_YEAR) AS billInvNo,
		OPH.BILL_INVOICE_NO,
		OPH.BILL_INVOICE_YEAR,
		OPH.BILL_DATE,
		OPH.REMARKS,
		FC.strCode,
		MS.strName AS supplier,
		'View' AS VIEW,
		(SELECT ROUND(SUM((OPD.QTY*OPD.UNIT_PRICE)*((100-OPD.DISCOUNT)/100)+OPD.TAX_AMOUNT),2) 
		FROM finance_other_payable_bill_details OPD
		WHERE OPD.BILL_INVOICE_NO=OPH.BILL_INVOICE_NO AND
		OPD.BILL_INVOICE_YEAR=OPH.BILL_INVOICE_YEAR) AS totAmount
		FROM finance_other_payable_bill_header OPH
		INNER JOIN mst_supplier MS ON MS.intId=OPH.SUPPLIER_ID
		LEFT JOIN mst_financecurrency FC ON FC.intId=OPH.CURRENCY_ID
		WHERE OPH.COMPANY_ID='$companyId'
		$where_string
		)  
		AS SUB_1 WHERE 1=1";

$jq 	= new jqgrid('',$db);	
$col 	= array();
$cols 	= array();

$col["title"] 			= "BillInvoice No";
$col["name"] 			= "BILL_INVOICE_NO";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "BillInvoice Year";
$col["name"] 			= "BILL_INVOICE_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Status"; 
$col["name"] 			= "STATUS";
$col["width"] 			= "3";
$col["stype"] 			= "select";
$str 					= ":All;Saved:Saved;Cancelled:Cancelled" ;
$col["editoptions"] 	=  array("value"=> $str);
$col["align"] 			= "center";
$cols[] 				= $col;
$col					= NULL;

$col["title"] 			= "Inv Serial No";
$col["name"] 			= "billInvNo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 
$col['link']			= '?q='.$menuId.'&billInvoiceNo={BILL_INVOICE_NO}&billInvoiceYear={BILL_INVOICE_YEAR}';
$col["linkoptions"] 	= "target='otherBill.php'";					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Supplier";
$col["name"] 			= "supplier";
$col["width"] 			= "6";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Date";
$col["name"] 			= "BILL_DATE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "strCode";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Remarks";
$col["name"] 			= "REMARKS";
$col["width"] 			= "7";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Amount";
$col["name"] 			= "totAmount";
$col["width"] 			= "3";
$col["align"] 			= "right";
$col["sortable"] 		= true; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "View";
$col["name"] 			= "VIEW";
$col["width"] 			= "2";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= '?q='.$reportId.'&billInvoiceNo={BILL_INVOICE_NO}&billInvoiceYear={BILL_INVOICE_YEAR}';
$col["linkoptions"] 	= "target='rptOtherBill.php'";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Other Bill Invoice Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'BILL_INVOICE_YEAR,BILL_INVOICE_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Other Bill Invoice Listing</title>
<?php
echo $out;
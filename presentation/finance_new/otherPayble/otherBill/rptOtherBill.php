<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once  "class/finance/cls_convert_amount_to_word.php";
include_once  "class/finance/otherPayable/otherBill/cls_invoice_get.php";
require_once  "class/finance/cls_common_get.php";

$obj_fin_com		= new Cls_Common_Get($db); 
$obj_AmtName	   	= new Cls_Convert_Amount_To_Word($db);
$obj_invoice_get	= new Cls_Invoice_Get($db);

$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$thisFilePath 		= $_SERVER['PHP_SELF'];

$billInvoiceNo		= $_REQUEST["billInvoiceNo"];
$billInvoiceYear	= $_REQUEST["billInvoiceYear"];

$header_array 		= $obj_invoice_get->getRptHeaderData($billInvoiceNo,$billInvoiceYear);
$detail_result 		= $obj_invoice_get->getRptDetaiData($billInvoiceNo,$billInvoiceYear);

$company_Id			= $header_array["COMPANY_ID"];
?>
<head>
<title>Other Payable - Bill Invoice Report</title>

<style>
#apDiv1 {
	position: absolute;
	left: 449px;
	top: 115px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
</style>

</head>
<body>
<?php
if($header_array['STATUS']==-2)//pending
{
?>
	<div id="apDiv1"><img src="images/cancelled.png" style="opacity:0.2" /></div>
<?php
}
?>
<form id="frmSalesInvoice" name="frmSalesInvoice" method="post" action="invoice.php" autocomplete="off">
<table width="900" align="center">
    <tr>
    	<td colspan="2"><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align:center">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align:center"><strong>OTHER PAYABLE BILL INVOICE REPORT</strong></td>
    </tr>
    <tr>
        <td colspan="2">
            <table width="100%" border="0" class="normalfnt">
                <tr>
                    <td width="14%">&nbsp;</td>
                    <td width="1%">&nbsp;</td>
                    <td width="44%">&nbsp;</td>
                    <td width="14%">&nbsp;</td>
                    <td width="1%">&nbsp;</td>
                    <td width="26%">&nbsp;</td>
                </tr>
                <tr>
                    <td>Invoice No</td>
                    <td>:</td>
            		<td><?php echo $obj_fin_com->getSerialNo($header_array["SERIAL_NO"],$header_array["SERIAL_DATE"],$company_Id,'RunQuery')//echo $header_array["ADVANCE_NO"]; ?></td>
                    <td>Invoiced Date</td>
                    <td>:</td>
                    <td><?php echo $header_array["BILL_DATE"]?></td>
                </tr>
                <tr>
                    <td>Supplier</td>
                    <td>:</td>
                    <td><?php echo $header_array["supplier"]; ?></td>
                    <td>Currency</td>
                    <td>:</td>
                    <td><?php echo $header_array["currency"]; ?></td>
                </tr>
                <tr>
                    <td valign="top">Remarks</td>
                    <td valign="top">:</td>
                    <td rowspan="2" valign="top"><?php echo $header_array["REMARKS"]; ?></td>
                    <td valign="top">Reference No</td>
                     <td valign="top">:</td>
                    <td valign="top"><?php echo $header_array["REFERENCE_NO"]; ?></td>
                </tr>
                <tr>
                    <td height="16">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" >
            <table width="100%" border="0" class="rptBordered" id="tblMain">
            <thead>
                <tr>
                    <th width="4%">&nbsp;</th>
                    <th width="23%">Item / Expenses</th>
                    <th width="23%">Item Desc.</th>
                    <th width="10%">UOM</th>
                    <th width="10%">Unit Price</th>
                    <th width="10%">Qty</th>
                    <th width="10%">Discount(%)</th>
                    <th width="10%">Value</th>
                </tr>
            </thead>        
            <tbody>
            <?php
            $totAmount 		= 0;
            $totTaxAmount 	= 0;
			$i				= 0;
            while($row = mysqli_fetch_array($detail_result))
            { 
				$amount 		= ($row['QTY']*$row['UNIT_PRICE'])*((100-$row['DISCOUNT'])/100);
				$finalAmount 	= $amount;
				?>
					<tr>
						<td class="cls_td_check" align="center"><?php echo ++$i;?>.</td>
						<td class="cls_td_salesOrderNo"><?php echo $row["CHART_OF_ACCOUNT_NAME"]?></td>
						<td width="23%"><?php echo $row["ITEM_DESCRIPTION"]?></td>
						<td width="10%"><?php echo $row["unit"]?></td>
						<td width="10%" style="text-align:right"><?php echo number_format($row["UNIT_PRICE"],2)?></td>
						<td class="cls_td_invoice cls_Subtract" style="text-align:right"><?php echo number_format($row["QTY"],2)?></td>
						<td style="text-align:right"><?php echo number_format($row["DISCOUNT"],2)?></td>
						<td class="cls_td_value" style="text-align:right"><?php echo number_format($finalAmount,2);?></td>
					</tr>
				<?php
				$totAmount 		= $totAmount+$finalAmount;
				$totTaxAmount 	= $totTaxAmount+$row['TAX_AMOUNT'];
            }
            $grandTotal 	= $totAmount+$totTaxAmount;
            ?>
            </tbody>
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="2" class="normalfnt"><b><?php echo $obj_AmtName->Convert_Amount($grandTotal,$header_array["currency"]); ?></b></td>
    </tr>
    <tr>
    	<td width="416" valign="top">&nbsp;</td>
        <td width="472" valign="top" >
            <table width="230" border="0" align="right" class="normalfnt">
                <tr>
                    <td width="100">Sub Total</td>
                    <td width="10">:</td>
                    <td width="106" align="right"><?php echo number_format($totAmount,2)?></td>
                </tr>
                <tr>
                    <td>Tax Total</td>
                    <td>:</td>
                    <td align="right"><?php echo number_format($totTaxAmount,2);?></td>
                </tr>
                <tr>
                    <td>Grand Total</td>
                    <td>:</td>
                    <td align="right"><?php echo number_format($grandTotal,2);?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="2" class="normalfnt">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="2">&nbsp;</td>
    </tr>
</table>
</form>
</body>
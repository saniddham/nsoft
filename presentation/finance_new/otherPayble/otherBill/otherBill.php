<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$userId 			= $_SESSION['userId'];

include  		"class/finance/cls_get_gldetails.php";
include  		"class/finance/cls_common_get.php";
include  		"class/finance/otherPayable/otherBill/cls_invoice_get.php";
include_once  	"class/cls_commonFunctions_get.php";
//include_once 		"include/javascript.html";
//include_once  	"class/masterData/exchange_rate/cls_exchange_rate_get.php";

$obj_get_GLCombo	= new Cls_Get_GLDetails($db);
$obj_common_get		= new Cls_Common_Get($db);
$obj_invoice_get	= new Cls_Invoice_Get($db);
$obj_common			= new cls_commonFunctions_get($db);
$obj_exchgRate_get	= new cls_exchange_rate_get($db);

$billInvoiceNo 		= (!isset($_REQUEST['billInvoiceNo'])?'':$_REQUEST['billInvoiceNo']);
$billInvoiceYear 	= (!isset($_REQUEST['billInvoiceYear'])?'':$_REQUEST['billInvoiceYear']);
$programCode		= 'P0729';

$header_arr			= $obj_invoice_get->getHeaderData($billInvoiceNo,$billInvoiceYear);
$detail_result		= $obj_invoice_get->getDetailData($billInvoiceNo,$billInvoiceYear);
$dateChangeMode 	= $obj_common->ValidateSpecialPermission('27',$userId,'RunQuery');
$cancleMode 		= $obj_common->Load_menupermision($programCode,$userId,'intCancel');
$row				= $obj_exchgRate_get->GetAllValues($header_arr['CURRENCY_ID'],4,$header_arr['BILL_DATE'],$companyId,'RunQuery');
$exchgRate			= $row['AVERAGE_RATE'];	

?>
<title>Other Payable - Bill Invoice</title>

<!--<script src="presentation/finance_new/otherPayble/otherBill/otherBill-js.js"></script>-->

<style>
#apDiv1 {
	position: absolute;
	left: 27px;
	top: 134px;
	width: auto;
	height: auto;
	z-index: 1;
}
</style>
<?php
if($header_arr['STATUS']==-2)
{
?>
	<div id="apDiv1" style="display:none"><img src="images/cancelled.png" /></div>
<?php
}
?>
<form id="frmOtherBill" name="frmOtherBill" method="post">
<div align="center">
<div class="trans_layoutXL">
<div class="trans_text">Other Payable - Bill Invoice</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td class="normalfnt">Reference No</td>
                <td class="normalfnt"><input name="txtBillInvNo" type="text" disabled="disabled" id="txtBillInvNo" style="width:60px" value="<?php echo $billInvoiceNo; ?>" />&nbsp;<input name="txtBillInvYear" type="text" disabled="disabled" id="txtBillInvYear" style="width:35px" value="<?php echo $billInvoiceYear; ?>" /></td>
                <td class="normalfnt">Date</td>
                <td class="normalfnt"><input name="txtDate" type="text" value="<?php echo($header_arr['BILL_DATE']==''?date("Y-m-d"):$header_arr['BILL_DATE']); ?>" class="validate[required]" id="txtDate" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($dateChangeMode!=1?'disabled="disabled"':''); ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
            </tr>
            <tr>
                <td width="16%" class="normalfnt">Supplier <span class="compulsoryRed">*</span></td>
                <td width="46%" class="normalfnt">
                <select name="cboSupplier" id="cboSupplier"  style="width:270px" class="validate[required]" >
                <?php
                echo $obj_invoice_get->loadSupplier($header_arr['SUPPLIER_ID']);
                ?>
                </select></td>
                <td width="15%" class="normalfnt">Currency <span class="compulsoryRed">*</span></td>
                <td width="23%" class="normalfnt"><select name="cboCurrency" id="cboCurrency"  style="width:100px" class="validate[required]" >
                <?php
                echo $obj_common_get->getCurrencyCombo($header_arr['CURRENCY_ID']);
                ?>
                </select></td>
            </tr>
            <tr>
                <td class="normalfnt" valign="top">Remark</td>
                <td rowspan="3" valign="top" class="normalfnt"><textarea name="txtRemarks" id="txtRemarks" style="width:270px" rows="2"><?php echo $header_arr['REMARKS']; ?></textarea></td>
                <td class="normalfnt" valign="top">Rate</td>
                <td class="normalfnt" valign="top"><input type="text" name="txtCurrencyRate" id="txtCurrencyRate" style="width:100px" disabled="disabled" value="<?php echo($billInvoiceNo==''?'0.0000':$exchgRate); ?>" /></td>
            </tr>
            <tr>
              <td class="normalfnt" valign="top">&nbsp;</td>
              <td class="normalfnt" valign="top">Invoice No  <span class="compulsoryRed">*</span></td>
              <td class="normalfnt" valign="top"><input type="text" name="txtReferenceNo" id="txtReferenceNo" width="250px" maxlength="50" class="validate[required]" value="<?php echo $header_arr['REFERENCE_NO']; ?>" /></td>
            </tr>
            <tr>
                <td class="normalfnt" valign="top">&nbsp;</td>
                <td class="normalfnt" valign="top">&nbsp;</td>
                <td class="normalfnt" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4" class="normalfnt"><div style="overflow:scroll;overflow-x:hidden;width:auto;height:350px;" id="divGrid">
                <table width="100%" class="bordered" id="tblMain" >
                <thead>
                    <tr>
                    	<th colspan="10" >Items<div style="float:right"><a class="button white small" id="butInsertRow">Add New Item</a></div></th>
                    </tr>
                    <tr>
                        <th width="15" height="30" >Del</th>
                        <th width="220" >Item / Expenses <span class="compulsoryRed">*</span></th>
                        <th width="250"  >Item Description <span class="compulsoryRed">*</span></th>
                        <th width="80" >UOM <span class="compulsoryRed">*</span></th>
                        <th width="80" >Qty <span class="compulsoryRed">*</span></th>
                        <th width="80" >Unit Price <span class="compulsoryRed">*</span></th>
                        <th width="80" >Discount %</th>
                        <th width="100" >Amount</th>
                        <th width="100" >Tax</th>
                        <th width="100" >Cost Center <span class="compulsoryRed">*</span></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $totAmount 		= 0;
                $totTaxAmount 	= 0;
                if($billInvoiceNo!='' && $billInvoiceYear!='')
                {
                while($row=mysqli_fetch_array($detail_result))
                {
                    $amount	   		 = $row['QTY']*$row['UNIT_PRICE'];	
                    $disamonut 		 = round((($amount*(100-$row['DISCOUNT']))/100),2);
                    
                    $totAmount 		+=$disamonut;
                    $totTaxAmount 	+=$row['TAX_AMOUNT'];
                ?>
                    <tr class="normalfnt cls_tr_firstRow">
                        <td style="text-align:center"><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>
                        <td style="text-align:left">
                        <select name="cboLedgerAc" id="cboLedgerAc" class="clsLedgerAc validate[required]"  style="width:100%" >
                        <?php
                        echo $obj_get_GLCombo->getGLCombo('OTHER_PAYABLE_INVOICE',$row['CHART_OF_ACCOUNT_ID']);
                        ?>
                        </select></td>
                        <td style="text-align:left"><textarea name="txtItemDisc" id="txtItemDisc" class="clsItemDesc validate[required]" style="width:100%;height:19px"><?php echo $row['ITEM_DESCRIPTION']; ?></textarea></td>
                        <td style="text-align:left">
                        <select name="cboUOM" id="cboUOM" class="clsUOM validate[required]" style="width:100%">
                        <?php
                        echo $obj_common_get->getUOMCombo($row['UNIT_OF_MEASURE']);
                        ?>
                        </select>
                        </td>
                        <td style="text-align:right">
                        <input type="textbox" id="txtQty" class="clsQty validate[required,custom[number]]" style="width:100%;text-align:right" value="<?php echo number_format($row['QTY'], 2, '.', ''); ?>" />
                        </td>
                        <td style="text-align:center"><input type="textbox" id="txtUnitPrice" class="clsUnitPrice validate[required,custom[number]]" style="width:100%;text-align:right" value="<?php echo number_format($row['UNIT_PRICE'], 2, '.', ''); ?>"></td>
                        <td style="text-align:center"><input type="textbox" id="txtDiscount" class="clsDiscount validate[custom[number]]" style="width:100%;text-align:right" value="<?php echo number_format($row['DISCOUNT'], 2, '.', ''); ?>"></td>
                        <td style="text-align:center"><input type="textbox" id="txtAmount" class="clsAmount" style="width:100%;text-align:right" value="<?php echo number_format($disamonut, 2, '.', ''); ?>" disabled="disabled" /></td>
                        <td style="text-align:center">
                        <select name="cboTax" id="cboTax" class="clsTax" style="width:100%">
                        <?php
                        echo $obj_common_get->getTaxCombo($row['TAX_CODE']);
                        ?>
                        </select><input type="hidden" id="hidTaxRate" class="clsTaxAmount" value="<?php echo number_format($row['TAX_AMOUNT'], 2, '.', ''); ?>"/></td>
                        <td style="text-align:center">
                        <select name="cboCostCenter" id="cboCostCenter" class="clsCostCener validate[required]" style="width:100%">                
                        <?php
                        echo $obj_common_get->getCostCenterCombo($row['COST_CENTER']);
                        ?>
                        </select></td>
                    </tr>
                <?php		
                }
                }
                if($billInvoiceNo=='' && $billInvoiceYear=='')
                {
                ?>
                    <tr class="normalfnt cls_tr_firstRow">
                        <td style="text-align:center"><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>
                        <td style="text-align:left"><select name="cboLedgerAc" id="cboLedgerAc" class="clsLedgerAc validate[required]"  style="width:100%" >
                        <?php
                        echo $obj_get_GLCombo->getGLCombo('OTHER_PAYABLE_INVOICE','');
                        ?>
                        </select></td>
                        <td style="text-align:left"><textarea name="txtItemDisc" id="txtItemDisc" class="clsItemDesc validate[required]" style="width:100%;height:19px"><?php echo $remarks; ?></textarea></td>
                        <td style="text-align:left">
                        <select name="cboUOM" id="cboUOM" class="clsUOM validate[required]" style="width:100%">
                        <?php
                        echo $obj_common_get->getUOMCombo("");
                        ?>
                        </select>
                        </td>
                        <td style="text-align:right">
                        <input type="textbox" id="txtQty" class="clsQty validate[required,custom[number]]" style="width:100%;text-align:right" />
                        </td>
                        <td style="text-align:center"><input type="textbox" id="txtUnitPrice" class="clsUnitPrice validate[required,custom[number]]" style="width:100%;text-align:right"></td>
                        <td style="text-align:center"><input type="textbox" id="txtDiscount" class="clsDiscount validate[custom[number]]" style="width:100%;text-align:right"></td>
                        <td style="text-align:center"><input type="textbox" id="txtAmount" class="clsAmount" style="width:100%;text-align:right" disabled="disabled" value="0" /></td>
                        <td style="text-align:center">
                        <select name="cboTax" id="cboTax" class="clsTax" style="width:100%">
                        <?php
                        echo $obj_common_get->getTaxCombo("");
                        ?>
                        </select><input type="hidden" id="hidTaxRate" class="clsTaxAmount" value="0"/></td>
                        <td style="text-align:center">
                        <select name="cboCostCenter" id="cboCostCenter" class="clsCostCener validate[required]" style="width:100%">
                        <?php
                        echo $obj_common_get->getCostCenterCombo("");
                        ?>
                        </select></td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
                </table>
                </div></td>
            </tr>
            <tr>
                <td colspan="2" class="normalfnt" valign="top">&nbsp;</td>
                <td class="normalfnt" style="text-align:right">&nbsp;</td>
                <td class="normalfnt" >
                <table width="100%" border="0">
                    <tr>
                        <td width="39%" class="normalfnt" style="text-align:right">Sub Total</td>
                        <td width="8%" class="normalfnt" style="text-align:right">:</td>
                        <td width="20%" class="normalfnt" style="text-align:right">&nbsp;</td>
                        <td width="33%" class="normalfnt"><span class="normalfnt" style="text-align:right">
                        <input name="txtSubTotal" type="text" disabled="disabled" id="txtSubTotal" style="width:100px;text-align:right" value="<?php echo number_format($totAmount, 2, '.', ''); ?>" />
                        </span></td>
                    </tr>
                    <tr>
                        <td class="normalfnt" style="text-align:right">Tax</td>
                        <td class="normalfnt" style="text-align:right">:</td>
                        <td class="normalfnt" style="text-align:right">&nbsp;</td>
                        <td class="normalfnt"><span class="normalfnt" style="text-align:right">
                        <input name="txtTaxTotal" type="text" disabled="disabled" id="txtTaxTotal" style="width:100px;text-align:right" value="<?php echo number_format($totTaxAmount, 2, '.', ''); ?>" />
                        </span></td>
                    </tr>
                    <tr>
                        <td class="normalfnt" style="text-align:right">Total</td>
                        <td class="normalfnt" style="text-align:right">:</td>
                        <td class="normalfnt" style="text-align:right">&nbsp;</td>
                        <td class="normalfnt"><span class="normalfnt" style="text-align:right">
                        <input name="txtTotal" type="text" disabled="disabled" id="txtTotal" style="width:100px;text-align:right" value="<?php echo number_format(($totAmount+$totTaxAmount), 2, '.', ''); ?>" />
                        </span></td>
                    </tr>
                </table>
                </td>
            </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="32" >
            <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            <tr>
            	<td align="center" class="tableBorder_allRound"><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($form_permision['edit']==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');?><a class="button white medium" id="butCancle" name="butCancle" <?php echo(($cancleMode!=1 || $header_arr['STATUS']==-2 || ($billInvoiceNo=='' && $billInvoiceYear==''))?'style="display:none"':''); ?> >Cancel</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
            </tr>
            </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>

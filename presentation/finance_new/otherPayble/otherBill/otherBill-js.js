// JavaScript Document
var basePath	= "presentation/finance_new/otherPayble/otherBill/";
var menuId		= 729;
$(document).ready(function(){
	
	$("#frmOtherBill").validationEngine();
	
	/*if(intAddx)
	{
		$('#frmOtherBill #butNew').show();
		
		if(status==-2)
			$('#frmOtherBill #butSave').hide();
		else
			$('#frmOtherBill #butSave').show();
	}
	//permision for edit 
	if(intEditx)
	{
		$('#frmOtherBill #butNew').show();
		
		if(status==-2)
			$('#frmOtherBill #butSave').hide();
		else
			$('#frmOtherBill #butSave').show();
	}
	
	//permision for delete
	if(intDeletex)
	{
		$('#frmOtherBill #butDelete').show();
	}
	
	//permision for view
	if(intViewx)
	{
		$('#frmOtherBill #cboSearch').removeAttr('disabled');
	}*/
	
	$('#frmOtherBill #butInsertRow').die('click').live('click',addNewRow);
	$('#frmOtherBill .clsDel').die('click').live('click',deleteRow);
	$('#frmOtherBill .clsQty').die('keyup').live('keyup',setAmount);
	$('#frmOtherBill .clsUnitPrice').die('keyup').live('keyup',setAmount);
	$('#frmOtherBill .clsDiscount').die('keyup').live('keyup',setAmount);
	$('#frmOtherBill .clsTax').die('change').live('change',function(){calculateTax(this)});
	$('#frmOtherBill .clsQty').die('blur').live('blur',function(){calculateTax(this)});
	$('#frmOtherBill .clsUnitPrice').die('blur').live('blur',function(){calculateTax(this)});
	$('#frmOtherBill .clsDiscount').die('blur').live('blur',function(){calculateTax(this)});
	$('#frmOtherBill #butSave').die('click').live('click',saveData);
	$('#frmOtherBill #butNew').die('click').live('click',clearAll);
	$('#frmOtherBill #butCancle').die('click').live('click',cancleInvoice);
	$('#frmOtherBill #cboSupplier').die('change').live('change',loadCurrency);
	$('#frmOtherBill #cboCurrency').die('change').live('change',loadExchangeRate);
	$('#frmOtherBill #txtDate').die('blur').live('blur',loadExchangeRate);
});
function addNewRow()
{
	if($('#frmOtherBill').validationEngine('validate'))
	{
		$('#frmOtherBill #tblMain tbody tr:last').after("<tr>"+$('#frmOtherBill #tblMain .cls_tr_firstRow').html()+"</tr>");
		$('#frmOtherBill #tblMain tbody tr:last #cboLedgerAc').val('');
		$('#frmOtherBill #tblMain tbody tr:last #txtItemDisc').val('');
		$('#frmOtherBill #tblMain tbody tr:last #cboUOM').val('');
		$('#frmOtherBill #tblMain tbody tr:last #txtQty').val('');
		$('#frmOtherBill #tblMain tbody tr:last #txtUnitPrice').val('');
		$('#frmOtherBill #tblMain tbody tr:last #txtDiscount').val('');	
		$('#frmOtherBill #tblMain tbody tr:last #txtAmount').val(0);	
		$('#frmOtherBill #tblMain tbody tr:last #cboTax').val('');
		$('#frmOtherBill #tblMain tbody tr:last #hidTaxRate').val(0);		
	}
}
function deleteRow()
{
	var delRowCount = parseInt(document.getElementById('tblMain').rows.length);
	
	if(delRowCount>3)
		$(this).parent().parent().remove();
	
	if(parseInt(document.getElementById('tblMain').rows.length)==3)
		$('#frmOtherBill #tblMain tbody tr:last').addClass('cls_tr_firstRow');
	
	CaculateTotalValue();
}
function setAmount()
{
	var qty 		= parseFloat($(this).parent().parent().find('.clsQty').val()==''?0:$(this).parent().parent().find('.clsQty').val());
	var unitPrice 	= parseFloat($(this).parent().parent().find('.clsUnitPrice').val()==''?0:$(this).parent().parent().find('.clsUnitPrice').val());
	var discount	= parseFloat($(this).parent().parent().find('.clsDiscount').val()==''?0:$(this).parent().parent().find('.clsDiscount').val());
	
	var amount		= (qty*unitPrice*(100-discount)/100);
	
	$(this).parent().parent().find('.clsAmount').val(RoundNumber(amount,2));
	
	CaculateTotalValue();
}
function calculateTax(obj)
{
	var url 	= basePath+"otherBill-db.php?requestType=URLCalculateTax";
	var data 	= "taxId="+$(obj).parent().parent().find('.clsTax').val()+"&amount="+$(obj).parent().parent().find('.clsAmount').val();
	var httpobj = $.ajax({
							url:url,
							data:data,
							dataType:'json',
							type:'POST',
							async:false,
							success:function(json)
							{
								$(obj).parent().parent().find('.clsTaxAmount').val(RoundNumber(json.TaxValue,2));
							}
						});	
	CaculateTotalValue();	
}
function CaculateTotalValue()
{
	var totalValue		= 0;
	var grandTotal		= 0;
	var taxValue		= 0;
	
	$('#frmOtherBill #tblMain .clsAmount').each(function(){
		
		totalValue += parseFloat($(this).val());
		taxValue   += parseFloat($(this).parent().parent().find('.clsTaxAmount').val());	
	});
	
	taxValue	 = RoundNumber(taxValue,2);
	totalValue	 = RoundNumber(totalValue,2);
	grandTotal	 = parseFloat(totalValue)+parseFloat(taxValue);
	
	$('#frmOtherBill #txtSubTotal').val(totalValue);
	$('#frmOtherBill #txtTaxTotal').val(taxValue);	
	$('#frmOtherBill #txtTotal').val(RoundNumber(grandTotal,2));
}
function saveData()
{
	showWaiting();
	var billInvNo 		= $('#frmOtherBill #txtBillInvNo').val();
	var billInvYear 	= $('#frmOtherBill #txtBillInvYear').val();
	var dtDate 			= $('#frmOtherBill #txtDate').val();
	var supplierId 		= $('#frmOtherBill #cboSupplier').val();
	var currency 		= $('#frmOtherBill #cboCurrency').val();
	var remarks 		= $('#frmOtherBill #txtRemarks').val();
	var grandTotal 		= $('#frmOtherBill #txtTotal').val();
	var referenceNo 	= $('#frmOtherBill #txtReferenceNo').val();
	
	if($('#frmOtherBill').validationEngine('validate'))
	{
		if(billInvNo!='' && billInvYear!='')
		{
			$('#frmOtherBill #butSave').validationEngine('showPrompt','You can not update this Invoice.','fail');
			hideWaiting();
			return;
		}
		
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"billInvNo":"'+billInvNo+'",' ;
							arrHeader += '"billInvYear":"'+billInvYear+'",' ;
							arrHeader += '"dtDate":"'+dtDate+'",' ;
							arrHeader += '"supplierId":"'+supplierId+'",' ;
							arrHeader += '"currency":"'+currency+'",' ;
							arrHeader += '"remarks":'+URLEncode_json(remarks)+',' ;
							arrHeader += '"referenceNo":'+URLEncode_json(referenceNo)+',' ;
							arrHeader += '"grandTotal":"'+grandTotal+'"' ;
			arrHeader += ' }';
			
		var chkStatus	= false;
		var arrDetails	= "";
		$('#tblMain .clsAmount').each(function(){
			
			var chartOfAccId	= $(this).parent().parent().find('.clsLedgerAc').val();
			var itemDisc		= $(this).parent().parent().find('.clsItemDesc').val();
 			var uom			 	= $(this).parent().parent().find('.clsUOM').val();
			var qty		 		= $(this).parent().parent().find('.clsQty').val();
			var amonut			= $(this).val();
			var unitPrice 		= $(this).parent().parent().find('.clsUnitPrice').val();
			var discount 		= $(this).parent().parent().find('.clsDiscount').val();
			var taxId 			= $(this).parent().parent().find('.clsTax').val();
			var costCenter 		= $(this).parent().parent().find('.clsCostCener').val();
			var taxAmount 		= $(this).parent().parent().find('.clsTaxAmount').val();
			
			if(parseFloat(amonut)>0)
			{
				chkStatus   = true;
				arrDetails += "{";
				arrDetails += '"chartOfAccId":"'+ chartOfAccId +'",' ;
				arrDetails += '"itemDisc":'+ URLEncode_json(itemDisc) +',' ;
				arrDetails += '"uom":"'+ uom +'",' ;
				arrDetails += '"qty":"'+ qty +'",' ;
				arrDetails += '"unitPrice":"'+ unitPrice +'",' ;
				arrDetails += '"discount":"'+ discount +'",' ;
				arrDetails += '"taxId":"'+ taxId +'",' ;
				arrDetails += '"costCenter":"'+ costCenter +'",' ;
				arrDetails += '"taxAmount":"'+ taxAmount +'"' ;
				arrDetails +=  '},';
			}
			
		});
		
		if(!chkStatus)
		{
			$('#frmOtherBill #butSave').validationEngine('showPrompt','No records to save.','fail');
			hideWaiting();
			return;
		}
		
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		
		var arrHeader	= arrHeader;
		var arrDetails	= '['+arrDetails+']';
		data+="&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		var url = basePath+"otherBill-db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmOtherBill #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						$('#frmOtherBill #txtBillInvNo').val(json.InvoiceNo);
						$('#frmOtherBill #txtBillInvYear').val(json.InvoiceYear);
						$('#frmOtherBill #butSave').hide();
						$('#frmOtherBill #butCancle').show();
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
						$('#frmOtherBill #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
	}
	else
	{
		hideWaiting();
	}	
}
function clearAll()
{
	window.location.href = '?q='+menuId;
}
function cancleInvoice()
{
	var billInvNo 		= $('#frmOtherBill #txtBillInvNo').val();
	var billInvYear 	= $('#frmOtherBill #txtBillInvYear').val();
	
	if(billInvNo=='')
	{
		$('#frmOtherBill #butCancle').validationEngine('showPrompt','No Bill Invoice No to Cancel.','fail');
		return;
	}
	var val = $.prompt('Are you sure you want to Cancel "'+billInvNo+'/'+billInvYear+'" ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url 	 = basePath+"otherBill-db.php?requestType=cancleInvoice";
					var data	 = "billInvNo="+billInvNo;
					data		+= "&billInvYear="+billInvYear;
					
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType:'json',  
						data:data,
						async:false,
						success:function(json){
								$('#frmOtherBill #butCancle').validationEngine('showPrompt', json.msg,json.type );
								if(json.type=='pass')
								{
									hideWaiting();
									var t=setTimeout("alertx1()",2000);
									var t=setTimeout("window.location.reload();",2000);
									return;
								}
								else
								{
									hideWaiting();
								}
							},
						error:function(xhr,status){
								
								$('#frmOtherBill #butCancle').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								hideWaiting();
								return;
							}		
						});
					
					}
				
			}});
}
function loadCurrency()
{
 	var url 	= basePath+"otherBill-db.php?requestType=loadCurrency";
	var data 	= "supplierId="+$(this).val();
	data 	   += "&date="+$('#txtDate').val();
	
	var obj 	= $.ajax({
						url:url,
						dataType:'json',
						type:'POST',  
						data:data,
						async:false,
						success:function(json){
								
								$('#frmOtherBill #cboCurrency').val(json.currencyId);
								$('#frmOtherBill #txtCurrencyRate').val(json.exchgRate);
						},
						error:function(xhr,status){
							
						}		
						});
 
}
function loadExchangeRate()
{
	if($('#frmOtherBill #cboCurrency').val()=='')
	{
		$('#frmOtherBill #txtCurrencyRate').val('0.0000');
		return;
	}
	var url 	= basePath+"otherBill-db.php?requestType=loadExchangeRate";
	var data 	= "currencyId="+$('#frmOtherBill #cboCurrency').val()+"&date="+$('#txtDate').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmOtherBill #txtCurrencyRate').val(json.exchgRate);
			}
	});
}
function alertx()
{
	$('#frmOtherBill #butSave').validationEngine('hide')	;
}
function alertx1()
{
	$('#frmOtherBill #butCancle').validationEngine('hide')	;
}
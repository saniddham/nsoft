// JavaScript Document
var basePath		= "presentation/finance_new/otherPayble/otherBillPayment/";
var voucherRptPath	= "presentation/finance_new/reports/";
var billReportId	= 979;
var menuId			= 735;
$(document).ready(function(){
	
	$("#frmOtherBillPayment").validationEngine();
	
	/*if(intAddx)
	{
		$('#frmOtherBillPayment #butNew').show();
		
		if(status==-2)
			$('#frmOtherBillPayment #butSave').hide();
		else
			$('#frmOtherBillPayment #butSave').show();
	}
	//permision for edit 
	if(intEditx)
	{
		$('#frmOtherBillPayment #butNew').show();
		
		if(status==-2)
			$('#frmOtherBillPayment #butSave').hide();
		else
			$('#frmOtherBillPayment #butSave').show();
	}
	//permision for delete
	if(intDeletex)
	{
		$('#frmOtherBillPayment #butDelete').show();
	}
	
	//permision for view
	if(intViewx)
	{
		$('#frmOtherBillPayment #cboSearch').removeAttr('disabled');
	}*/
	
	$('#frmOtherBillPayment #cboSupplier').die('change').live('change',loadData);
	$('#frmOtherBillPayment #cboCurrency').die('change').live('change',loadData);
	$('#frmOtherBillPayment .clsPayAmt').die('keyup').live('keyup',setAmount);
	$('#frmOtherBillPayment .clsChkInvoice').die('click').live('click',setCheckAmount);
	$('#frmOtherBillPayment .clsAmount').die('keyup').live('keyup',setGLAmount);
	$('#frmOtherBillPayment #chkAll').die('click').live('click',checkAll);
	$('#frmOtherBillPayment #butSave').die('click').live('click',saveData);
	$('#frmOtherBillPayment #butCancle').die('click').live('click',canclePayment);
	$('#frmOtherBillPayment #butNew').die('click').live('click',clearAll);
	$('#frmOtherBillPayment #butVoucher').die('click').live('click',viewVoucherReport);
});
function loadData()
{
	loadExchangeRate();
	if($('#frmOtherBillPayment #cboSupplier').val()=='')
	{
		clearData('Supplier');
		return;
	}
	if($('#frmOtherBillPayment #cboCurrency').val()=='')
	{
		clearData('Currency');
		return;
	}
	clearData('Other');
	
	var currency 	= $('#cboCurrency').val();
	var supplier 	= $('#cboSupplier').val();
	
	if(currency=='' || supplier=='' )
	{
		return;
	}
	
	var url 		= basePath+"otherBillPayment-db.php?requestType=loadData";
	var data		= "supplier="+supplier;
		data	   += "&currency="+currency;
	
		var obj = 	$.ajax({
							url:url,
							dataType:"json",
							type:'POST',  
							data:data,
							async:false,
							success:function(json){
								
								$('#frmOtherBillPayment #cboPaymentMethod').val(json.paymentMethod);
								
								var lengthDetail   	= json.arrDetailData.length;
								var arrDetailData  	= json.arrDetailData;
								
								for(var i=0;i<lengthDetail;i++)
								{
									var invoiceNo		= arrDetailData[i]['invoiceNo'];
									var billInvoiceNo	= arrDetailData[i]['billInvoiceNo'];
									var billInvoiceYear	= arrDetailData[i]['billInvoiceYear'];
									var invoiceDate		= arrDetailData[i]['invoiceDate'];	
									var invoiceAmount	= RoundNumber(arrDetailData[i]['invoiceAmount'],2);
									var paidAmount		= RoundNumber(arrDetailData[i]['paidAmount'],2);
									var toBePaidAmount	= RoundNumber(arrDetailData[i]['toBePaidAmount'],2);
									var referenceNo		= arrDetailData[i]['referenceNo'];	
									
									if(invoiceNo!=null)
										createGrid(invoiceNo,billInvoiceNo,billInvoiceYear,invoiceDate,invoiceAmount,paidAmount,toBePaidAmount,referenceNo);
								}
							},
							error:function(xhr,status){
							
							}		
					});	
}
function createGrid(invoiceNo,billInvoiceNo,billInvoiceYear,invoiceDate,invoiceAmount,paidAmount,toBePaidAmount,referenceNo)
{
	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);
	row.id			= invoiceNo;
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML  = "<input type=\"checkbox\" id=\"chkInvoice\" name=\"chkInvoice\" class=\"clsChkInvoice validate[minCheckbox[1]]\">";
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'clsInvoice';
	cell.innerHTML 	= "<a href='?q="+billReportId+"&billInvoiceNo="+billInvoiceNo+"&billInvoiceYear="+billInvoiceYear+"' target=\"rptOtherBill.php\">"+invoiceNo+"</a>";
	
	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsInvoiceDate';
	cell.innerHTML 	= (referenceNo==''?'&nbsp;':referenceNo);
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'clsInvoiceDate';
	cell.innerHTML 	= invoiceDate;
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsInvoiceAmt';
	cell.innerHTML 	= invoiceAmount;
	
	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsPaidAmt';
	cell.innerHTML 	= paidAmount;
	
	var cell 		= row.insertCell(6);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsToBePaid';
	cell.innerHTML 	= toBePaidAmount;
	
	var cell 		= row.insertCell(7);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'clsPayAmount';
	cell.innerHTML 	= "<input type=\"textbox\" id=\"txtPayAmt\" name=\"txtPayAmt\" style=\"width:110px;text-align:right\" value=\""+0+"\" class=\"clsPayAmt validate[custom[number]]\" >";
	
}
function setAmount()
{
	var tobePaid 	= parseFloat($(this).parent().parent().find('.clsToBePaid').html());
	if(parseFloat($(this).val())>tobePaid)
	{
		$(this).val(tobePaid);
	}
	
	$('#frmOtherBillPayment .clsAmount').val('');
	CaculateTotalValue();
}
function CaculateTotalValue()
{
	var totalValue		= 0;
	var totGLValue		= 0;
	
	$('#frmOtherBillPayment #tblMain .clsPayAmt').each(function(){
		if($(this).parent().parent().find('.clsChkInvoice').attr('checked'))
		{
			totalValue += parseFloat($(this).val());
		}
	});
	$('#frmOtherBillPayment .clsAmount').val(RoundNumber(totalValue,2));	
	
	$('#frmOtherBillPayment #tblGLMain .clsAmount').each(function(){
		
		totGLValue += parseFloat($(this).val()==''?0:$(this).val());	
		
	});
	totalValue	= RoundNumber(totalValue,2);
	totGLValue	= RoundNumber(totGLValue,2);
	
	$('#frmOtherBillPayment #totPayAmount').html(totalValue);	
	$('#frmOtherBillPayment #totGLAlocAmount').html(totGLValue);
}
function setCheckAmount()
{
	$('#frmOtherBillPayment .clsAmount').val('');
	$('#frmOtherBillPayment #totGLAlocAmount').html('');
	$('#frmOtherBillPayment #totPayAmount').html('');
	
	var tobePaid = parseFloat($(this).parent().parent().find('.clsToBePaid').html());		
	$(this).parent().parent().find('.clsPayAmt').val(tobePaid);
	
	$('#frmOtherBillPayment .clsChkInvoice').each(function(){
        
		if($(this).attr('checked'))
		{
			$(this).parent().parent().find('.clsPayAmt').addClass('validate[required]');
		}
		else
		{
			$(this).parent().parent().find('.clsPayAmt').removeClass('validate[required]');
			$(this).parent().parent().find('.clsPayAmt').val(0);
			$('#frmOtherBillPayment #chkAll').attr('checked' , false);
		}
    });
	CaculateTotalValue();
}
function setGLAmount()
{
	var obj			 	= this;
	var totGLAmount  	= 0;
	var totAmount 	 	= parseFloat($('#totPayAmount').html());
	$('#frmOtherBillPayment .clsAmount').each(function(){

		if($(this).val()!="")
			totGLAmount += parseFloat($(this).val());
	});
	if(totGLAmount>totAmount)
	{
		obj.value 		= (parseFloat(obj.value)-parseFloat(parseFloat(totGLAmount)-parseFloat(totAmount)));
		totLedAmount 	= totAmount;
	}
	CaculateTotalValue();
}
function checkAll()
{
	if($(this).attr('checked'))
	{
		$('#frmOtherBillPayment .clsChkInvoice').attr("checked",true);
		
		$('#frmOtherBillPayment #tblMain .clsChkInvoice').each(function(){
           
		   if($(this).parent().parent().find('.clsPayAmt').val()=='' || $(this).parent().parent().find('.clsPayAmt').val()==0)
		   { 
				var tobePaid = parseFloat($(this).parent().parent().find('.clsToBePaid').html());
				$(this).parent().parent().find('.clsPayAmt').val(tobePaid);	
		   }
        });
		
	}
	else
	{
		$('#frmOtherBillPayment .clsAmount').val('');
		$('#frmOtherBillPayment #totGLAlocAmount').html('');
		$('#frmOtherBillPayment #totPayAmount').html('');
		$('#frmOtherBillPayment .clsChkInvoice').attr("checked",false);
		$('#frmOtherBillPayment .clsPayAmt').val(0);
	}
	CaculateTotalValue();
}
function saveData()
{
	showWaiting();
	var paymentNo 		= $('#frmOtherBillPayment #txtPaymentNo').val();
	var paymentYear 	= $('#frmOtherBillPayment #txtPaymentYear').val();
	var supplierId 		= $('#frmOtherBillPayment #cboSupplier').val();
	var payDate 		= $('#frmOtherBillPayment #txtDate').val();
	var currency 		= $('#frmOtherBillPayment #cboCurrency').val();
	var Remarks 		= $('#frmOtherBillPayment #txtRemarks').val();
	var payMethod 		= $('#frmOtherBillPayment #cboPaymentMethod').val();
	var bankRefNo 		= $('#frmOtherBillPayment #txtBankRefNo').val();
	var accountId		= $('#frmOtherBillPayment .clsAccounts').val();
	var glAmount		= $('#frmOtherBillPayment .clsAmount').val();
	var memo			= $('#frmOtherBillPayment .clsMemo').val();
	var costCenter		= $('#frmOtherBillPayment .clsCostCener').val();
	
	if($('#frmOtherBillPayment').validationEngine('validate'))
	{
		var totPayAmount 		= parseFloat($('#frmOtherBillPayment #totPayAmount').html());
		var totGLAlocAmount 	= parseFloat($('#frmOtherBillPayment #totGLAlocAmount').html());
		
		if(totPayAmount!=totGLAlocAmount)
		{
			$('#frmOtherBillPayment #totPayAmount').validationEngine('showPrompt','Total Pay amount must equal to Total GL Alocated amonut.','fail');
			hideWaiting();
			return;
		}
		if(paymentNo!='' && paymentYear!='')
		{
			$('#frmOtherBillPayment #butSave').validationEngine('showPrompt','You can not update this Payment.','fail');
			hideWaiting();
			return;
		}
		
		if(totPayAmount<=0 || totGLAlocAmount<=0)
		{
			$('#frmOtherBillPayment #butSave').validationEngine('showPrompt','Totals must greater than 0.','fail');
			hideWaiting();
			return;
		}
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"paymentNo":"'+paymentNo+'",' ;
							arrHeader += '"paymentYear":"'+paymentYear+'",' ;
							arrHeader += '"payDate":"'+payDate+'",' ;
							arrHeader += '"supplierId":"'+supplierId+'",' ;
							arrHeader += '"currency":"'+currency+'",' ;
							arrHeader += '"Remarks":'+URLEncode_json(Remarks)+',' ;
							arrHeader += '"payMethod":"'+payMethod+'",' ;
							arrHeader += '"bankRefNo":"'+bankRefNo+'",' ;
							arrHeader += '"totPayAmount":"'+totPayAmount+'",' ;
							arrHeader += '"totGLAlocAmount":"'+totGLAlocAmount+'",' ;
							arrHeader += '"accountId":"'+ accountId +'",' ;
							arrHeader += '"glAmount":"'+ glAmount +'",' ;
							arrHeader += '"memo":'+ URLEncode_json(memo) +',' ;
							arrHeader += '"costCenter":"'+ costCenter +'"' ;
			arrHeader += ' }';
		
		var chkStatus  	= false;
		var amtStatus  	= false;
		var arrDetails	= "";
		$('#frmOtherBillPayment #tblMain .clsChkInvoice').each(function(){
			
			chkStatus  	= true;
			if($(this).attr('checked'))
			{
				var invoiceNoArr = $(this).parent().parent().attr('id');
				var payAmount 	 = $(this).parent().parent().find('.clsPayAmt').val();
				
				if(parseFloat(payAmount)<=0)
				{
					$(this).parent().parent().find('.clsPayAmt').validationEngine('showPrompt','Amount must greater than 0.','fail');
					amtStatus = true;
					hideWaiting();
					return false;	
				}
				arrDetails += "{";
				arrDetails += '"invoiceNoArr":"'+ invoiceNoArr +'",' ;
				arrDetails += '"payAmount":"'+ payAmount +'"' ;
				arrDetails +=  '},';
			}
		});
		
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		var arrHeader	= arrHeader;
		var arrDetails	= '['+arrDetails+']';
		data+="&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		if(amtStatus)
			return;
		
		if(!chkStatus)
		{
			$(this).validationEngine('showPrompt','No records to save.','fail');
			hideWaiting();	
			return;
		}
		
		/*var arrGLDetails = "";
		$('#tblGLMain .clsAmount').each(function(){
			
			var accountId 	= $(this).parent().parent().find('.clsAccounts').val();
			var amount 		= $(this).val();
			var memo 		= $(this).parent().parent().find('.clsMemo').val();
			var costCenter 	= $(this).parent().parent().find('.clsCostCener').val();
			
			arrGLDetails += "{";
			arrGLDetails += '"accountId":"'+ accountId +'",' ;
			arrGLDetails += '"amount":"'+ amount +'",' ;
			arrGLDetails += '"memo":'+ URLEncode_json(memo) +',' ;
			arrGLDetails += '"costCenter":"'+ costCenter +'"' ;
			arrGLDetails += '},';
			
		});
		arrGLDetails 		= arrGLDetails.substr(0,arrGLDetails.length-1);
		var arrGLDetails	= '['+arrGLDetails+']';
		data+="&arrGLDetails="+arrGLDetails;*/
		
		var url = basePath+"otherBillPayment-db.php";
			$.ajax({
					url:url,
					dataType:'json',
					type:'post',
					data:data,
					async:false,
					success:function(json){
						$('#frmOtherBillPayment #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							var t = setTimeout("alertx()",3000);
							$('#frmOtherBillPayment #txtPaymentNo').val(json.paymentNo);
							$('#frmOtherBillPayment #txtPaymentYear').val(json.paymentYear);
							$('#frmOtherBillPayment #butSave').hide();
							$('#frmOtherBillPayment #butCancle').show();
							$('#frmOtherBillPayment #butVoucher').show();
							hideWaiting();
							return;
						}
						else
						{
							hideWaiting();
						}
					},
					error:function(xhr,status){
							
							$('#frmOtherBillPayment #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							hideWaiting();
							return;
					}		
			});
	}
	else
	{
		hideWaiting();
	}
}
function canclePayment()
{
	var paymentNo 		= $('#frmOtherBillPayment #txtPaymentNo').val();
	var paymentYear 	= $('#frmOtherBillPayment #txtPaymentYear').val();
	
	if(paymentNo=='')
	{
		$('#frmOtherBillPayment #butCancle').validationEngine('showPrompt','No PaymentNo to Cancel.','fail');
		return;
	}
	var val = $.prompt('Are you sure you want to Cancel "'+paymentNo+'/'+paymentYear+'" ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url 	 = basePath+"otherBillPayment-db.php?requestType=cancelPayment";
					var data	 = "paymentNo="+paymentNo;
					data		+= "&paymentYear="+paymentYear;
					
					var obj = $.ajax({
								url:url,
								type:'post',
								dataType:'json',  
								data:data,
								async:false,
								success:function(json){
										$('#frmOtherBillPayment #butCancle').validationEngine('showPrompt', json.msg,json.type );
										if(json.type=='pass')
										{
											hideWaiting();
											var t=setTimeout("alertx1()",2000);
											var t=setTimeout("window.location.reload();",2000);
											return;
										}
										else
										{
											hideWaiting();
										}
								},
								error:function(xhr,status){
										
										$('#frmOtherBillPayment #butCancle').validationEngine('showPrompt', errormsg(xhr.status),'fail');
										hideWaiting();
										return;
								}		
							});
					
					}
				
			}});
}
function clearData(type)
{
	$("#frmOtherBillPayment #tblMain tr:gt(0)").remove();
	$('#frmOtherBillPayment .clsAmount').val('');
	$('#frmOtherBillPayment #totPayAmount').html(0.00);
	$('#frmOtherBillPayment #totGLAlocAmount').html(0.00);
	
}
function loadExchangeRate()
{
	if($('#frmOtherBillPayment #cboCurrency').val()=='')
	{
		$('#frmOtherBillPayment #txtCurrencyRate').val('0.0000');
		return;
	}
	var url 	= basePath+"otherBillPayment-db.php?requestType=loadExchangeRate";
	var data 	= "currencyId="+$('#frmOtherBillPayment #cboCurrency').val()+"&date="+$('#txtDate').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmOtherBillPayment #txtCurrencyRate').val(json.exchgRate);
			}
	});
}
function viewVoucherReport()
{
	var url  = voucherRptPath+"rpt_payment_voucher_pdf.php?SerialNo="+$('#frmOtherBillPayment #txtPaymentNo').val();
		url += "&SerialYear="+$('#frmOtherBillPayment #txtPaymentYear').val();
		url += "&Type=BILLPAYMENT";
	
	window.open(url,'rpt_payment_voucher_pdf.php');
	
}
function clearAll()
{
	window.location.href = '?q='+menuId;
}
function alertx()
{
	$('#frmOtherBillPayment #butSave').validationEngine('hide')	;
}
function alertx1()
{
	$('#frmOtherBillPayment #butCancle').validationEngine('hide')	;
}
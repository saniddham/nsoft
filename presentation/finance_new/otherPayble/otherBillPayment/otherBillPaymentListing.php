<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId	= $_SESSION["headCompanyId"];
$menuId		= 735;

require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'STATUS'=>'OPPH.STATUS',
				'paymentNo'=>"CONCAT(OPPH.PAYMENT_NO,' - ',OPPH.PAYMENT_YEAR)",
				'supplier'=>'MS.strName',
				'PAY_DATE'=>'OPPH.PAY_DATE',
				'strCode'=>'FC.strCode',
				'REMARKS'=>'OPPH.REMARKS'
				);
				
$arr_status = array('Saved'=>'1','Cancelled'=>'-2');
foreach($arr as $k=>$v)
{
	if($v['field']=='STATUS')
	{
		$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND OPPH.PAY_DATE = '".date('Y-m-d')."'";
	
// }

$sql = "SELECT SUB_1.* FROM
		(
		SELECT IF(OPPH.STATUS=-2,'Cancelled','Saved') AS STATUS,
		OPPH.PAYMENT_NO,
		OPPH.PAYMENT_YEAR,
		CONCAT(OPPH.PAYMENT_NO,' - ',OPPH.PAYMENT_YEAR) AS paymentNo,
		OPPH.REMARKS,
		OPPH.PAY_DATE,
		FC.strCode,
		MS.strName AS supplier,
		'View' AS VIEW,
		'Voucher' AS VOUCHER,
		(SELECT ROUND(SUM(OPPD.AMOUNT),2) FROM finance_other_payable_payment_details OPPD
		WHERE OPPD.PAYMENT_NO=OPPH.PAYMENT_NO AND
		OPPD.PAYMENT_YEAR=OPPH.PAYMENT_YEAR) AS totAmount
		FROM finance_other_payable_payment_header OPPH
		INNER JOIN mst_supplier MS ON MS.intId=OPPH.SUPPLIER_ID
		LEFT JOIN mst_financecurrency FC ON FC.intId=OPPH.CURRENCY_ID
		WHERE OPPH.COMPANY_ID='$companyId'
		$where_string
		)  
		AS SUB_1 WHERE 1=1";

$jq 	= new jqgrid('',$db);	
$col 	= array();
$cols 	= array();

$col["title"] 			= "Payment No";
$col["name"] 			= "PAYMENT_NO";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Payment Year";
$col["name"] 			= "PAYMENT_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Status"; 
$col["name"] 			= "STATUS";
$col["width"] 			= "3";
$col["stype"] 			= "select";
$str 					= ":All;Saved:Saved;Cancelled:Cancelled" ;
$col["editoptions"] 	=  array("value"=> $str);
$col["align"] 			= "center";
$cols[] 				= $col;
$col					= NULL;

$col["title"] 			= "Payment No";
$col["name"] 			= "paymentNo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 
$col['link']			= '?q='.$menuId.'&paymentNo={PAYMENT_NO}&paymentYear={PAYMENT_YEAR}';
$col["linkoptions"] 	= "target='otherBillPayment.php'";					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Supplier";
$col["name"] 			= "supplier";
$col["width"] 			= "7";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Date";
$col["name"] 			= "PAY_DATE";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "strCode";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Remarks";
$col["name"] 			= "REMARKS";
$col["width"] 			= "7";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Amount";
$col["name"] 			= "totAmount";
$col["width"] 			= "3";
$col["align"] 			= "right";
$col["sortable"] 		= true; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Payment Voucher";
$col["name"] 			= "VOUCHER";
$col["width"] 			= "3";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= 'presentation/finance_new/reports/rpt_payment_voucher_pdf.php?SerialNo={PAYMENT_NO}&SerialYear={PAYMENT_YEAR}&Type=BILLPAYMENT';
$col["linkoptions"] 	= "target='rptPayment_voucher.php'";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Other Payable Payment Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'PAYMENT_YEAR,PAYMENT_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Other Payable Payment Listing</title>
<?php
echo $out;
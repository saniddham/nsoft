<?php 
session_start();
$backwardseperator 	= "../../../../";

$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$requestType 		= $_REQUEST['requestType'];
$programCode		= 'P0735';

include "../../../../dataAccess/Connector.php";
include "../../../../class/finance/otherPayable/otherBillPayment/cls_payment_set.php";
include "../../../../class/finance/otherPayable/otherBillPayment/cls_payment_get.php";
//include_once "../../../../class/masterData/exchange_rate/cls_exchange_rate_get.php";

$obj_exchgRate_get	= new cls_exchange_rate_get($db);
$obj_payment_set	= new Cls_Payment_Set($db,$obj_exchgRate_get);
$obj_payment_get	= new Cls_Payment_Get($db);


if($requestType=='loadData')
{
	$supplier		= $_REQUEST['supplier'];
	$currency		= $_REQUEST['currency'];
	
	$response['arrDetailData']	= $obj_payment_get->getDetailData($supplier,$currency);
	$response['paymentMethod']	= $obj_payment_get->getPaymentMethod($supplier);
	
	echo json_encode($response);
}
else if($requestType=='saveData')
{	
	$arrHeader 		= json_decode($_REQUEST['arrHeader'],true);
	$arrDetails 	= json_decode($_REQUEST['arrDetails'],true);
	//$arrGLDetails = json_decode($_REQUEST['arrGLDetails'],true);
	
	echo $obj_payment_set->Save($arrHeader,$arrDetails,$programCode);
}
else if($requestType=="cancelPayment")
{
	$paymentNo		= $_REQUEST['paymentNo'];
	$paymentYear	= $_REQUEST['paymentYear'];
	
	echo $obj_payment_set->Cancel($paymentNo,$paymentYear,$programCode);
}
else if($requestType=='loadExchangeRate')
{
	$currency				= $_REQUEST['currencyId'];
	$date	  				= $_REQUEST['date'];
	
	$row					= $obj_exchgRate_get->GetAllValues($currency,4,$date,$companyId,'RunQuery');
	$response['exchgRate']	= $row['AVERAGE_RATE'];
	
	echo json_encode($response);
}
?>
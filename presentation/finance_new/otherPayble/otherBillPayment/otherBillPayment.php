<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$userId 			= $_SESSION['userId'];
$billReportId		= 979;

include "class/finance/cls_get_gldetails.php";
include "class/finance/cls_common_get.php";
include "class/finance/otherPayable/otherBill/cls_invoice_get.php";
include "class/finance/otherPayable/otherBillPayment/cls_payment_get.php";
include "class/cls_commonFunctions_get.php";
//include "include/javascript.html";
//include_once "class/masterData/exchange_rate/cls_exchange_rate_get.php";

$obj_get_GLCombo	= new Cls_Get_GLDetails($db);
$obj_invoice_get	= new Cls_Invoice_Get($db);
$obj_payment_get	= new Cls_Payment_Get($db);
$obj_common_get		= new Cls_Common_Get($db);
$obj_common			= new cls_commonFunctions_get($db);
$obj_exchgRate_get	= new cls_exchange_rate_get($db);

$paymentNo 			= (!isset($_REQUEST['paymentNo'])?'':$_REQUEST['paymentNo']);
$paymentYear 		= (!isset($_REQUEST['paymentYear'])?'':$_REQUEST['paymentYear']);
$programCode		= 'P0735';

$header_arr			= $obj_payment_get->getSavedHeaderData($paymentNo,$paymentYear);
$detail_result		= $obj_payment_get->getSavedDetailData($paymentNo,$paymentYear);
$GL_result_arr		= $obj_payment_get->getChartOfAccountData($paymentNo,$paymentYear);
$cancleMode 		= $obj_common->Load_menupermision($programCode,$userId,'intCancel');
$dateChangeMode 	= $obj_common->ValidateSpecialPermission('28',$userId,'RunQuery');

$row				= $obj_exchgRate_get->GetAllValues($header_arr['CURRENCY_ID'],4,$header_arr['PAY_DATE'],$companyId,'RunQuery');
$exchgRate			= $row['AVERAGE_RATE'];

?>
<title>Other Payable - Bill Payment</title>

<!--<script type="text/javascript" src="presentation/finance_new/otherPayble/otherBillPayment/otherBillPayment-js.js"></script>-->

<form id="frmOtherBillPayment" name="frmOtherBillPayment" method="post">
<div align="center">
<div class="trans_layoutL">
<div class="trans_text">Other Payable - Bill Payment</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
                <tr>
                	<td width="17%" class="normalfnt">Payment No</td>
                	<td width="36%" class="normalfnt"><input name="txtPaymentNo" type="text" disabled="disabled" id="txtPaymentNo" style="width:60px" value="<?php echo $paymentNo; ?>" />&nbsp;<input name="txtPaymentYear" type="text" disabled="disabled" id="txtPaymentYear" style="width:35px" value="<?php echo $paymentYear; ?>" /></td>
                	<td width="18%" class="normalfnt">&nbsp;</td>
                	<td width="29%" class="normalfnt"></td>
                </tr>
                <tr>
                    <td class="normalfnt">Supplier <span class="compulsoryRed">*</span></td>
                    <td class="normalfnt"><select name="cboSupplier" id="cboSupplier"  style="width:220px" class="validate[required]" >
                    <?php
                    echo $obj_invoice_get->LoadSupplier($header_arr['SUPPLIER_ID']);
                    ?>
                    </select></td>
                    <td class="normalfnt">Date</td>
                    <td class="normalfnt"><input name="txtDate" type="text" value="<?php echo($header_arr['PAY_DATE']==''?date("Y-m-d"):$header_arr['PAY_DATE']); ?>" class="validate[required]" id="txtDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($dateChangeMode!=1?'disabled="disabled"':''); ?> /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                </tr>
                <tr>
                    <td class="normalfnt">Currency <span class="compulsoryRed">*</span></td>
                    <td class="normalfnt"><select name="cboCurrency" id="cboCurrency"  style="width:100px" class="validate[required]" >
                    <?php
                    echo $obj_common_get->getCurrencyCombo($header_arr['CURRENCY_ID']);
                    ?>
                    </select></td>
                    <td class="normalfnt">Rate</td>
                    <td class="normalfnt"><input type="text" name="txtCurrencyRate" id="txtCurrencyRate" style="width:100px" disabled="disabled" value="<?php echo($paymentNo==''?'0.0000':$exchgRate); ?>" /></td>
                </tr>
                <tr>
                    <td class="normalfnt" valign="top">Remarks</td>
                    <td rowspan="3" class="normalfnt" valign="top"><textarea name="txtRemarks" id="txtRemarks" style="width:220px" rows="2"><?php echo $header_arr['REMARKS']; ?></textarea></td>
                    <td class="normalfnt" valign="top">Payment Method <span class="compulsoryRed">*</span></td>
                    <td class="normalfnt" valign="top"><select name="cboPaymentMethod" id="cboPaymentMethod" style="width:220px" class="validate[required]" >
                      <?php
                    echo $obj_common_get->getPayMethodCombo($header_arr['PAY_METHOD']);
                    ?>
                    </select></td>
                </tr>
                <tr>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt" valign="top">Reference Number</td>
                  <td class="normalfnt" valign="top"><input type="text" name="txtBankRefNo" id="txtBankRefNo" style="width:220px" value="<?php echo $header_arr['BANK_REFERENCE_NO']; ?>" maxlength="100" /></td>
                </tr>
                <tr>
                    <td class="normalfnt">&nbsp;</td>
                    <td class="normalfnt" valign="top">&nbsp;</td>
                    <td class="normalfnt" valign="top"></td>
                </tr>
                <tr>
                	<td colspan="4" class="normalfnt"><div style="overflow:scroll;overflow-x:hidden;width:auto;height:250px;">
                	<table width="100%" border="0" class="bordered" id="tblMain">
                        <thead>
                            <tr>
                                <th width="4%"><input type="checkbox" id="chkAll" name="chkAll" class="clsChkAll" <?php echo($paymentNo!=''?'checked="checked"':''); ?> ></th>
                                <th width="11%">Invoice No</th>
                                <th width="16%">Reference No</th>
                                <th width="12%">Invoiced Date</th>
                                <th width="15%">Invoiced Amount</th>
                                <th width="14%">Paid Amount</th>
                                <th width="14%">To Be Paid</th>
                                <th width="14%">Paying Amount <span class="compulsoryRed">*</span></th>
                            </tr>
                        </thead>        
               			<tbody>
						<?php
						$totPaidAmount = 0.00;
                        while($row = mysqli_fetch_array($detail_result))
                        {
							$totPaidAmount+=$row['currPayAmount'];
                        ?>
                            <tr class="normalfnt" >
                                <td style="text-align:center"><input type="checkbox" id="chkInvoice" name="chkInvoice" class="clsChkInvoice validate[minCheckbox[1]]" checked="checked"></td>
                                <td class="clsInvoice" style="text-align:center"><a href="?q=<?php echo $billReportId; ?>&billInvoiceNo=<?php echo $row["BILL_INVOICE_NO"]?>&billInvoiceYear=<?php echo $row["BILL_INVOICE_YEAR"]?>" target="rptOtherBill.php"><?php echo $row["invoiceNo"]?></a></td>
                                <td class="clsReference" style="text-align:left"><?php echo ($row['REFERENCE_NO']==''?'&nbsp;':$row['REFERENCE_NO']); ?></td>
                                <td class="clsInvoiceDate" style="text-align:center"><?php echo $row["invoiceDate"]?></td>
                                <td class="clsInvoiceAmt" style="text-align:right"><?php echo number_format($row['invoiceAmount'],2)?></td>
                                <td class="clsPaidAmt" style="text-align:right"><?php echo number_format($row['paidAmount'],2);?></td>
                                <td class="clsToBePaid" style="text-align:right"><?php echo number_format($row['toBePaidAmount'],2)?></td>
                                <td class="clsPayAmount" style="text-align:center"><input type="textbox" id="txtPayAmt" name="txtPayAmt" style="width:110px;text-align:right" value="<?php echo number_format($row['currPayAmount'],2,'.','') ?>" class="clsPayAmt validate[custom[number]]"></td>
                            </tr>
						<?php
                        }
                        ?>
                		</tbody>
                	</table>
                    </div>
                    </td>
                </tr>
                <tr>
                <td colspan="4" class="normalfnt"><div style="width:auto;" id="divGrid">
                <table width="100%" class="bordered" id="tblGLMain" >
                    <thead>
                        <tr style="display:none">
                            <th colspan="5" >GL Allocation
                            <div style="float:right"><a class="button white small" id="butInsertRow">Add New GL</a></div></th>
                        </tr>
                        <tr>
                            <th width="3%" height="30" style="display:none">Del</th>
                            <th width="29%" >Account <span class="compulsoryRed">*</span></th>
                            <th width="13%" >Amount <span class="compulsoryRed">*</span></th>
                            <th width="41%" >Remarks </th>
                            <th width="14%" >Cost Center <span class="compulsoryRed">*</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="cls_tr_firstRow">
                            <td width="3%" style="text-align:center;display:none"><img src="images/del.png" name="butDel" class="mouseover clsDel" id="butDel" /></td>
                            <td width="29%" style="text-align:center" ><select name="cboAccounts" id="cboAccounts" style="width:100%" class="clsAccounts validate[required]">
                            <?php
                            echo $obj_get_GLCombo->getBankGLCombo($GL_result_arr['ACCOUNT_ID']);
                            ?>
                            </select></td>
                            <td width="13%" style="text-align:center" ><input name="txtAmmount" type="text" id="txtAmmount" style="width:100%;text-align:right" class="validate[required,custom[number]] clsAmount" value="<?php echo number_format($GL_result_arr['AMOUNT'], 2, '.', ''); ?>" /></td>
                            <td width="41%" style="text-align:center" ><input type="text" name="txtMemo" id="txtMemo" style="width:100%" class="clsMemo" value="<?php echo $GL_result_arr['MEMO']; ?>" /></td>
                            <td width="14%" style="text-align:center" >
                            <select name="cboCostCenter" id="cboCostCenter" class="clsCostCener validate[required]" style="width:100%">
                            <?php
                            echo $obj_common_get->getCostCenterCombo($GL_result_arr['COST_CENTER']);
                            ?>
                            </select></td>
                        </tr>
                    </tbody>
                </table>
                </div>
                </td>
                </tr>
                <tr>
                	<td colspan="2" class="normalfnt" valign="top">&nbsp;</td>
                	<td colspan="2" class="normalfnt" style="text-align:right"><table width="301" border="0" align="right" class="normalfnt">
                <tr>
                	<td width="166">Total Paying Amount</td>
                	<td width="11" style="text-align:center">:</td>
                	<td width="91" style="text-align:right;" id="totPayAmount"><?php echo number_format($totPaidAmount, 2, '.', ''); ?></td>
                </tr>
                <tr>
                	<td>Total GL Allocated Amount</td>
                	<td style="text-align:center"><b>:</b></td>
                	<td style="text-align:right" id="totGLAlocAmount"><?php echo number_format($GL_result_arr['AMOUNT'], 2, '.', ''); ?></td>
                </tr>
                </table>
              </td>
            </tr> 
         </table>
        </td>
    </tr>
    <tr>
        <td height="32" >
            <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
                	<td align="center" class="tableBorder_allRound"><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($form_permision['edit']==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');?><a class="button white medium" id="butCancle" name="butCancle" <?php echo(($cancleMode!=1 || $header_arr['STATUS']==-2 || ($paymentNo=='' && $paymentYear==''))?'style="display:none"':''); ?> >Cancel</a><a class="button white medium" id="butVoucher" name="butVoucher" <?php echo (($paymentNo!='' && $paymentYear!='')?'':'style="display:none"');?>>Voucher</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>
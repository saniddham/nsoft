<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once "class/finance/cls_convert_amount_to_word.php";
require_once "class/finance/otherPayable/otherBillPayment/cls_payment_get.php";

$obj_AmtName	   	= new Cls_Convert_Amount_To_Word($db);
$obj_payment_get	= new Cls_Payment_Get($db);

$mainPath 			= $_SESSION['mainPath'];
$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$thisFilePath 		= $_SERVER['PHP_SELF'];

$paymentNo 			= $_REQUEST['paymentNo'];
$paymentYear 		= $_REQUEST['paymentYear'];

$header_array 		= $obj_payment_get->getRptHeaderData($paymentNo,$paymentYear,$companyId);
$detail_result 		= $obj_payment_get->getSavedDetailData($paymentNo,$paymentYear);
$GL_result 			= $obj_payment_get->getRptGLDetailData($paymentNo,$paymentYear);

?>
<head>
<title>Other Bill Paymnet</title>
<style>
#apDiv1 {
	position: absolute;
	left: 468px;
	top: 117px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
</style>

</head>
<body>
<?php
if($header_array['STATUS']==-2)
{
?>
<div id="apDiv1" ><img src="images/cancelled.png" style="opacity:0.3" /></div>
<?php
}
?>
<form id="frmOtherBillPayment" name="frmOtherBillPayment" method="post">
    <table width="1000" align="center">
      <tr>
        <td width="888"><?php include 'reportHeader.php'?></td>
      </tr>
      <tr>
        <td style="text-align:center">&nbsp;</td>
      </tr>
      <tr>
        <td style="text-align:center"><strong>OTHER BILL PAYMENT REPORT</strong></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>Payment No</td>
            <td>:</td>
            <td><?php echo $paymentNo.' / '.$paymentYear; ?></td>
            <td>Payment Date</td>
            <td>:</td>
            <td><?php echo $header_array["PAY_DATE"]; ?></td>
          </tr>
          <tr>
            <td>Supplier</td>
            <td>:</td>
            <td><?php echo $header_array["supplier"]; ?></td>
            <td>Currency</td>
            <td>:</td>
            <td><?php echo $header_array["currency"]; ?></td>
          </tr>
          <tr>
            <td width="15%">Amount</td>
            <td width="1%">:</td>
            <td width="41%"><?php echo number_format($header_array["totPaidAmount"],2); ?></td>
            <td width="14%">Payment Method</td>
            <td width="1%">:</td>
            <td width="28%"><?php echo $header_array["paymentMethod"]; ?></td>
          </tr>
          <tr>
            <td>Remarks</td>
            <td>:</td>
            <td rowspan="2" valign="top"><?php echo $header_array["REMARKS"]; ?></td>
            <td>Bank Reference No</td>
            <td>:</td>
            <td><?php echo $header_array["BANK_REFERENCE_NO"]; ?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
       <tr>
         <td class="normalfnt"><table width="100%" border="0" class="rptBordered" id="tblMain2">
           <thead>
             <tr>
               <th width="3%">&nbsp;</th>
                <th width="15%">Invoice No</th>
                <th width="17%">Reference No</th>
                <th width="13%">Invoiced Date</th>
                <th width="13%">Invoiced Amount</th>
                 <th width="13%">Paid Amount</th>
                <th width="13%">To Be Paid</th>
                <th width="13%">Paying Amount</th>
             </tr>
           </thead>
           <tbody>
             <?php
$totAmount 		= 0;

while($row = mysqli_fetch_array($detail_result))
{ 
?>
            <tr>
                <td align="center"><?php echo ++$i;?>.</td>
                <td class="clsInvoice" style="text-align:left"><?php echo $row["invoiceNo"]?></td>
                <td class="clsInvoice" style="text-align:left"><?php echo $row["referenceNo"]?></td>
                <td class="clsInvoiceDate" style="text-align:center"><?php echo $row["invoiceDate"]?></td>
                <td class="clsInvoiceAmt" style="text-align:right"><?php echo $row["invoiceAmount"]?></td>
                <td class="clsPaidAmt" style="text-align:right"><?php echo ($row["paidAmount"]==null?'&nbsp;':$row["paidAmount"]);?></td>
                <td class="clsToBePaid" style="text-align:right"><?php echo $row["toBePaidAmount"]?></td>
                <td class="clsPayAmount" style="text-align:right"><?php echo number_format($row['currPayAmount'],2); ?></td>
            </tr>
             <?php
}
?>
             <tr>
               <td colspan="7" align="right"><b>Total :</b></td>
               <td width="13%" style="text-align:right"><b><?php echo number_format($header_array["totPaidAmount"],2); ?></b></td>
             </tr>
           </tbody>
         </table></td>
       </tr>
       <tr>
        <td class="normalfnt">&nbsp;</td>
      </tr>
      <tr>
        <td ><table width="100%" border="0" class="rptBordered" id="tblMain">
        <thead>
          <tr>
            <th width="3%">&nbsp;</th>
            <th width="29%">Account</th>
            <th width="42%">Memo</th>
            <th width="15%">Cost Center</th>
            <th width="11%">Amount</th>
          </tr>
        </thead>        
        <tbody>
<?php
$totAmount 		= 0;
while($row = mysqli_fetch_array($GL_result))
{ 
	$totAmount+=$row['AMOUNT'];
?>
		<tr>
        	<td class="cls_td_check" align="center"><?php echo ++$j;?>.</td>
        	<td class="cls_td_salesOrderNo"><?php echo $row["account"]?></td>
       	 	<td width="42%"><?php echo $row["MEMO"]?></td>
        	<td width="15%"><?php echo $row["costCenter"]?></td>
        	<td width="11%" style="text-align:right"><?php echo number_format($row["AMOUNT"],2);?></td>
        </tr>
<?php
}
?>
		<tr>
        	<td colspan="4" align="right"><b>Total :</b></td>
        	<td width="11%" style="text-align:right"><b><?php echo number_format($totAmount,2);?></b></td>
        </tr>
        </tbody>
        </table></td>
      </tr>
      <tr>
        <td class="normalfnt"><b><?php echo $obj_AmtName->Convert_Amount($header_array["totPaidAmount"],$header_array["currency"]); ?></b></td>
      </tr>
      <tr>
        <td class="normalfnt">&nbsp;</td>
      </tr>
      </table>
</form>
</body>
</html>
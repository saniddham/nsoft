<?php
//BEGIN - INCLUDE FILES {
include_once "../../../../dataAccess/LoginDBManager.php";
include_once "../../../../class/finance/budget/entering/cls_budget_get.php";
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {
$db						= new LoginDBManager;
$obj_budget_get			= new cls_budget_get($db);
//END 	- CREATE OBJECTS }

//BEGIN - SET PARAMETERS {
$company				= $_REQUEST["CompId"];
$location				= $_REQUEST["LocaId"];
$financePeriod			= $_REQUEST["FinancePeriod"];
$department				= $_REQUEST["DeptId"];
$financeMonth_arr		= $obj_budget_get->getFinanceMonths($financePeriod);
//END 	- SET PARAMETERS }

//BEGIN - QUERY PARAMETERS {
if($location!="")
	$para .=  "AND MAIN.LOCATION_ID = $location ";

if($department!="")
	$para .= "AND MAIN.DEPARTMENT_ID = $department ";
//END 	- QUERY PARAMETERS }

//BEGIN - MAIN QUERY {
    $sql = "SELECT 
				LOCATION_NAME,
				DEPARTMENT_NAME,
				MAIN_CAT_NAME,
				SUB_CAT_NAME,
				ITEM_NAME, ";	
for($i=$financeMonth_arr['startMonth'];$i<=12+$financeMonth_arr['endMonth'];$i++)
{			
	$m 	  = date('m', mktime(0,0,0,$i,1));
	$sql .= "budget_amount$m,
			budget_units$m,
			outflows_amount$m,
			outflows_units$m,			
			iF((IFNULL(budget_amount$m,0)- IFNULL(outflows_amount$m,0))=0,'',(IFNULL(budget_amount$m,0)-IFNULL(outflows_amount$m,0)))as balance_amount$m,			
			if((IFNULL(budget_units$m,0) - IFNULL(outflows_units$m,0))=0,'',(IFNULL(budget_units$m,0) - IFNULL(outflows_units$m,0))) as balance_units$m,
			 ";
} 
	
	$sql .= "'VIEW'	AS VIEW
			
			FROM(			
				SELECT
					LO.strName 						AS LOCATION_NAME,
					DE.strName 						AS DEPARTMENT_NAME,
					mst_maincategory.strName 		AS MAIN_CAT_NAME,
					mst_subcategory.strName 		AS SUB_CAT_NAME,
					mst_item.strName 				AS ITEM_NAME, ";
					
/*for($i=$financeMonth_arr['startMonth'];$i<=12+$financeMonth_arr['endMonth'];$i++)
{				
	$sql .= "(SELECT PD.INITIAL_BUDGET 
					FROM budget_plan_details PD
					WHERE 
						PD.LOCATION_ID = MAIN.LOCATION_ID AND
						PD.ITEM_ID = MAIN.ITEM_ID AND
						PD.month = $i limit 1)						AS budget_amount$i,
					
			(SELECT  budget_plan_details.INITIAL_BUDGET_UNITS 
				from budget_plan_details 
			WHERE 
				budget_plan_details.LOCATION_ID = MAIN.LOCATION_ID AND
				budget_plan_details.ITEM_ID = MAIN.ITEM_ID AND
				budget_plan_details.month = $i limit 1) 								AS budget_units$i,
				
			(SELECT  budget_plan_details.ISSUE_BUDGET 
				from budget_plan_details 
			WHERE 
				budget_plan_details.LOCATION_ID = MAIN.LOCATION_ID AND
				budget_plan_details.ITEM_ID = MAIN.ITEM_ID AND
				budget_plan_details.month = $i limit 1) 								AS outflows_amount$i,
			
			(SELECT budget_plan_details.ISSUE_BUDGET_UNITS 
				from budget_plan_details 
			WHERE 
				budget_plan_details.LOCATION_ID = MAIN.LOCATION_ID AND
				budget_plan_details.ITEM_ID = MAIN.ITEM_ID AND
				budget_plan_details.month = $i limit 1)								AS outflows_units$i, ";
}*/

for($i=$financeMonth_arr['startMonth'];$i<=12+$financeMonth_arr['endMonth'];$i++)
{
	$m 	  = date('m', mktime(0,0,0,$i,1));
	$sql .= "SUM(IF(MONTH=$m,IFNULL(INITIAL_BUDGET,0) + IFNULL(ADDITIONAL_BUDGET,0) + IFNULL(TRANSFER_BUDGET,0),'')) AS budget_amount$m,";
	$sql .= "SUM(IF(MONTH=$m,IFNULL(INITIAL_BUDGET_UNITS,0) + IFNULL(ADDITIONAL_BUDGET_UNITS,0) + IFNULL(TRANSFER_BUDGET_UNITS,0),'')) AS budget_units$m,";
	$sql .= "SUM(IF(MONTH=$m,ISSUE_BUDGET,''))					AS outflows_amount$m,";
	$sql .= "SUM(IF(MONTH=$m,ISSUE_BUDGET_UNITS,''))			AS outflows_units$m,";
}

	$sql .= "
			'VIEW'	AS VIEW
			FROM
			budget_plan_details AS MAIN
			INNER JOIN budget_plan_header PH
				ON PH.LOCATION_ID = MAIN.LOCATION_ID
				AND PH.DEPARTMENT_ID = MAIN.DEPARTMENT_ID
				AND PH.FINANCE_YEAR_ID = MAIN.FINANCE_YEAR_ID
			INNER JOIN mst_locations LO ON LO.intId = MAIN.LOCATION_ID
			INNER JOIN mst_department DE ON DE.intId = MAIN.DEPARTMENT_ID
			INNER JOIN mst_financeaccountingperiod ON mst_financeaccountingperiod.intId = MAIN.FINANCE_YEAR_ID
			INNER JOIN mst_subcategory ON mst_subcategory.intId = MAIN.SUB_CATEGORY_ID
			LEFT JOIN mst_item ON mst_item.intId = MAIN.ITEM_ID
			LEFT JOIN mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
			WHERE
				PH.STATUS = 1
				AND LO.intCompanyId = '$company'
				AND MAIN.FINANCE_YEAR_ID = $financePeriod
				$para		
			GROUP BY SUB_CATEGORY_ID,ITEM_ID) as main";
//END 	- MAIN QUERY }	
//echo $sql;				
	$result = $db->RunQuery($sql);
	$rowCount	= mysqli_num_rows($result);
	$arrayTotal["total"] = $rowCount;
	$data	= array();
    while($row = mysqli_fetch_array($result))
	{
			$data["locationName"] 			= $row['LOCATION_NAME'];
			$data["depName"] 				= $row['LOCATION_NAME'];
			$data["mainCat"] 				= $row['MAIN_CAT_NAME'];
			$data["subName"] 				= $row['SUB_CAT_NAME'];
			$data["itemName"] 				= $row['ITEM_NAME'];
		
		for($i=$financeMonth_arr['startMonth'];$i<=12+$financeMonth_arr['endMonth'];$i++)
		{
			$m 	  = date('m', mktime(0,0,0,$i,1));
			$data[$m."_budget_amount"] 		= ($row["budget_amount$m"]=='0'?'':$row["budget_amount$m"]);
			$data[$m."_budget_units"]		= ($row["budget_units$m"]=='0'?'':$row["budget_units$m"]);		
			$data[$m."_outflows_amount"] 	= ($row["outflows_amount$m"]=='0'?'':$row["outflows_amount$m"]);
			$data[$m."_outflows_units"] 	= ($row["outflows_units$m"]=='0'?'':$row["outflows_units$m"]);
			$data[$m."_balance_amount"] 	= $row["balance_amount$m"];//($row["balance_amount$m"]=='0'?'':$row["balance_amount$m"]);
			$data[$m."_balance_units"] 		= $row["balance_units$m"];//($row["balance_units$m"]=='0'?'':$row["balance_units$m"]);
		}
		$a[] = $data;
    }
		$arrayTotal["rows"]	= $a;
    echo json_encode($arrayTotal);
?>
<?php
session_start();
include_once "../../../../dataAccess/LoginDBManager.php";
include_once "../../../../class/finance/budget/entering/cls_budget_get.php";
include_once "../../../../class/masterData/companies/cls_companies_get.php";
include_once "../../../../class/finance/budget/cls_common_function_budget_get.php";
include_once "../../../../class/masterData/department/cls_department_get.php";

$db						= new LoginDBManager;
$obj_budget_get			= new cls_budget_get($db);
$obj_companies_get		= new cls_companies_get($db);
$obj_department_get		= new cls_department_get($db);
$obj_com_func_budget_get= new cls_common_function_budget_get($db);
//BEGIN -
if(!isset($_REQUEST["cboCompany"]))
	$company		= $_SESSION['headCompanyId'];
else
	$company		= $_REQUEST["cboCompany"];

$periodArray		= $obj_com_func_budget_get->getFinancePeriod($company,date('Y-m-d'),'RunQuery');	
$location			= $_REQUEST["cboLocation"];

if(!isset($_REQUEST["cboFinancePeriod"]))
	$financePeriod	= $periodArray["FINANCE_ID"];
else 
	$financePeriod	= $_REQUEST["cboFinancePeriod"];
	
$department			= $_REQUEST["cboDepartment"];
//END	- 

$financeMonth_arr		= $obj_budget_get->getFinanceMonths($financePeriod);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Budget Report</title>
	<link rel="stylesheet" type="text/css" href="../easyui/themes/default/easyui.css">
	<!--<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/icon.css">-->
	<link rel="stylesheet" type="text/css" href="../easyui/demo/demo.css">
    <link rel="stylesheet" type="text/css" href="../../../../css/mainstyle.css">
    <link rel="stylesheet" type="text/css" href="../../../../css/button.css">
	<script type="text/javascript" src="jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="../easyui/jquery.easyui.min.js"></script>
    <script>
		function getSelected(){
			var row = $('#tt').datagrid('getSelected');
			if (row){
				alert('Item ID:'+row.subName+"\nPrice:"+row.subName);
			}
		}
		function getSelections(){
			var ids = [];
			var rows = $('#tt').datagrid('getSelections');
			for(var i=0; i<rows.length; i++){
				ids.push(rows[i].subName);
			}
			alert(ids.join('\n'));
		}
	</script>
</head>
<body>
<form id="frmMain" name="frmMain" enctype="multipart/form-data" method="post">
    <table style="width:1300px;">
    <tr>
    	<td width="81">Company</td>
    	<td width="138"><select name="cboCompany" id="cboCompany" style="width:120px" class="cls_formLoad">
    	  <?php echo $obj_companies_get->getCompanySelection($company)?>
  	  </select></td>
    	<td width="80">Location</td>
    	<td width="140"><select name="cboLocation" id="cboLocation" style="width:120px" class="cls_formLoad">
        <option value=""></option>
        <?php echo $obj_companies_get->getLocationSelection($company,$location)?>
  	  </select></td>
    	<td width="114">Finance Period</td>
    	<td width="145"><select name="cboFinancePeriod" id="cboFinancePeriod" style="width:120px" class="cls_formLoad">
        <?php $result = $obj_com_func_budget_get->loadFinanceYear($company,'RunQuery');
			  while($row = mysqli_fetch_array($result))
			  {
				  if($financePeriod==$row["intId"])
				  	echo "<option value=\"".$row["intId"]."\" selected=\"selected\">".$row["financeYear"]."</option>";
				  else
				  	echo "<option value=\"".$row["intId"]."\">".$row["financeYear"]."</option>";
			  }?>
  	  </select></td>
    	<td width="97">Department</td>
        <td width="396"><select name="cboDepartment" id="cboDepartment" style="width:120px" class="cls_formLoad">
        <?php echo $obj_department_get->getDepartmentSelection($department)?>
        </select></td>
        <td width="69"><a class="button white medium" id="butSearch">Search</a></td>
      </tr>
    </table>
	<table id="tt" title="Budget Report" class="easyui-datagrid"  style="width:1300px;height:600px"
			url="data.php?CompId=<?php echo $company?>&LocaId=<?php echo $location?>&FinancePeriod=<?php echo $financePeriod?>&DeptId=<?php echo $department?>"
			rownumbers="true" pagination="true" singleSelect="false" iconCls="icon-save" showFooter="true">
           
		<thead frozen="true">
			<tr>
				<th rowspan="3" field="mainCat" sortable="true" width="200">Main Category</th>
				<th rowspan="3" field="subName" sortable="true" width="120">Sub Category</th>
                <th rowspan="3"  field="itemName" sortable="true" width="120">Item</th>
			</tr>
            <tr></tr>	
			<tr></tr>
	  </thead>
		<thead>
        	<tr>
            <?php for($i=$financeMonth_arr['startMonth'];$i<=12+$financeMonth_arr['endMonth'];$i++){?>
            	<th colspan="6" ><?php  echo date('Y-F', mktime(0,0,0,$i,1,$financeMonth_arr['startYear'],0)) ?></th>
            <?php } ?>             
          </tr>	
			<tr>
            <?php for($i=$financeMonth_arr['startMonth'];$i<=12+$financeMonth_arr['endMonth'];$i++){?>
           	  <th colspan="2" >BUDGETED</th>
                <th colspan="2" >OUTFLOWS</th>
                <th colspan="2" >BALANCE</th>
            <?php } ?>              	
            </tr>
			<tr>
            	<?php for($i=$financeMonth_arr['startMonth'];$i<=12+$financeMonth_arr['endMonth'];$i++){
					$m 	  = date('m', mktime(0,0,0,$i,1));?>
           	  <th field="<?php echo $m?>_budget_amount" width="80" sortable="true" align="right">Amount</th>
			  <th field="<?php echo $m?>_budget_units" width="80" sortable="true" align="right">Units</th>
              <th field="<?php echo $m?>_outflows_amount" width="80" sortable="true" align="right">Amount</th>
			  <th field="<?php echo $m?>_outflows_units" width="80" sortable="true" align="right">Units</th>
              <th field="<?php echo $m?>_balance_amount" width="80" sortable="true" align="right" formatter="formatBalanceColumnFont">Amount</th>
			  <th field="<?php echo $m?>_balance_units" width="80" sortable="true" align="right" formatter="formatBalanceColumnFont">Units</th>
            	<?php } ?> 				
			</tr>
		</thead>
        
  </table>
    </form>
    <script>
	function formatBalanceColumnFont(val,row){
			return '<span style="color:blue;">'+val+'</span>';
		}
		
	$(document).ready(function(e) {
        $('.cls_formLoad').live('change',loadForm);
		$('#butSearch').live('click',loadForm);
	
    });
	function loadForm()
	{
		$('#frmMain').submit();
	}
/*		$(function(){
			$('#tt').datagrid('loadData', getData());
		});
		function getData()
		{
			alert(2);	
		}*/
		
		/*$(function(){
			$('#tt').datagrid({
				url: 'data.php?q=1',
				method: 'get',
				singleSelect: true
			});
		});*/
	/*$(function(){
	$('#tt').datagrid('load', {
    	name: 'easyui',
    	address: 'ho'
    });
	});*/
	</script>
</body>
</html>
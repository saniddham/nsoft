<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<title>Budget Report</title>
</head>

<body>
<table width="988">
	<tr class="normalfnt">
	  <td width="120"><strong>Finance Year</strong></td>
	  <td width="224"><strong>Category</strong></td>
	  <td width="150"><strong>Company</strong></td>
	  <td width="150"><strong>Location</strong></td>
	  <td width="150"><strong>Department</strong></td>
	  <td width="79">&nbsp;</td>
	  <td width="83">&nbsp;</td>
  </tr>
	<tr>
	  <td>
      <select style="width:120px" name="select" id="select">
      </select></td>
	  <td><select style="width:200px" name="select2" id="select2">
      </select></td>
	  <td><select style="width:150px" name="select4" id="select4">
      </select></td>
	  <td><select style="width:150px" name="select5" id="select5">
      </select></td>
	  <td><select style="width:150px" name="select3" id="select3">
      </select></td>
	  <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
</table>
<table width="1112" border="0" cellspacing="0" cellpadding="0" class="bordered">
  <tr>
    <th width="31">&nbsp;</th>
    <th width="12">&nbsp;</th>
    <th width="199">&nbsp;</th>
    <th width="41">&nbsp;</th>
    <th colspan="6" style="font-size:14px;color:#C30">April-2104</th>
    <th colspan="6" style="font-size:14px;color:#C30">May-2104</th>
  </tr>
    <tr>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
    <th colspan="2">BUDGETED</th>
    <th colspan="2">OUTFLOWS </th>
    <th colspan="2">BALANCE</th>
    <th colspan="2">BUDGETED</th>
    <th colspan="2">OUTFLOWS </th>
    <th colspan="2">BALANCE</th>
  </tr>
    <tr>
    <th colspan="3" style="text-align:left">Item</th>
    <th>Unit</th>
    <th width="76">Amount</th>
    <th width="60">Units</th>
    <th width="75">Amount</th>
    <th width="60">Units</th>
    <th width="75">Amount</th>
    <th width="60">Units</th>
    <th width="76">Amount</th>
    <th width="60">Units</th>
    <th width="75">Amount</th>
    <th width="60">Units</th>
    <th width="75">Amount</th>
    <th width="75">Units</th>
  </tr>
  <tr>
    <td colspan="3">Stationary</td>
    <td>&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>A4 Sheet</td>
    <td>PCS</td>
    <td align="right" class="normalfntRight" style="color:blue">10,000</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight" style="color:green">5,000</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">5,000</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>A3 Sheet</td>
    <td>PCS</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Pen</td>
    <td>PCS</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">25</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">10</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">15</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
  </tr>
</table>
</body>
</html>
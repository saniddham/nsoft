var basePath = "presentation/finance_new/budget/department_item_allocation/";
$(document).ready(function() {
	
	$('#frmPopUp #chkCheckAll').die('click').live('click',function(){
		CheckAll_Approve($(this).is(':checked'));
	});
	
	$('#frmItemAllocation #butNew').die('click').live('click',clearForm);
	$('#frmItemAllocation #butSave').die('click').live('click',save);
	$('#frmItemAllocation #tblMain .removeRow').die('click').live('click',removeRow);
	$("#frmItemAllocation #butAdd").die('click').live('click',setToAddNew);
	$('#frmPopUp #cboMainCategory').die('change').live('change',loadSubCat);
	$('#frmPopUp #butSearch').die('click').live('click',loadItemGrid);
	$('#frmPopUp #butAdd').die('click').live('click',addItems);
	$('#frmItemAllocation #cboDepartment').die('change').live('change',searchItem);
	
});

function save()
{
	showWaiting();
	/*if(!save_validation()){
		setTimeout("autoHideFormValidations('#frmItemAllocation')",5000);
		setTimeout("autoHideFormValidations('#frmItemAllocation #butSave')",5000);
		hideWaiting();
		return;
	}*/
		
	var detailArray	 = "[";
	$('#frmItemAllocation #tblMain #tblBody tr').each(function(){
		detailArray += "{";
		detailArray += '"serialID":"'+$(this).find('.cls_td_del').attr('id')+'"' ;
		detailArray +=  '},';
	});
	detailArray 	 = detailArray.substr(0,detailArray.length-1);
	detailArray 	+= " ]";

	var url 	= basePath+"item_allocation_db.php?RequestType=Save";
	var data 	= "Department="+$('#frmItemAllocation #cboDepartment').val();
		data   += "&DetailArray="+detailArray;
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type: "POST",
	async:false,
	success:function(json)
		{
			if(json.type=='pass')
			{
				hideWaiting();
				$('#frmItemAllocation #butSave').validationEngine('showPrompt',json.msg,json.type);
				var t = setTimeout("autoHideFormValidations('#frmItemAllocation #butSave')",4000);
			}
			else
			{
				hideWaiting();
				$('#frmBudgetTransfer #butReport').hide();
				$('#frmBudgetTransfer #butSave').validationEngine('showPrompt',json.msg,json.type);
				var t = setTimeout("autoHideFormValidations('#frmBudgetTransfer #butSave')",5000);
			}
		},
	error:function(xhr,status)
		{
			hideWaiting();
			$('#frmBudgetTransfer #butSave').validationEngine('showPrompt',errormsg(xhr.status),'fail');
			var t = setTimeout("autoHideFormValidations('#frmBudgetTransfer #butSave')",4000);
		}
	});	
	
	hideWaiting();
}

function save_validation()
{
	if(!$('#frmBudgetTransfer').validationEngine('validate')){
		return false;
	}
	
	if($('#frmBudgetTransfer #tblMain tbody tr').length=='0')
	{
		$('#frmBudgetTransfer #butSave').validationEngine('showPrompt','No details available to save.','fail');
		return false;
	}
	return true;
}
function searchItem()
{
	var searchId	= $('#frmItemAllocation #cboDepartment').val();
	var locationId	= $('#frmItemAllocation #cboLocation').val();	
	var url 		= basePath+"item_allocation_db.php?RequestType=searchItem&searchId="+searchId;
	 	url		   += "&locationId="+locationId;
	var httpobj = $.ajax({
	url:url,
	dataType:'json',
	type: "POST",
	async:false,
	success:function(json)
		{
			$('#frmItemAllocation #tblMain #tblBody').html(json.gridDetail);	
		}
	});
	
}
function autoHideFormValidations(obj)
{
	$(obj).validationEngine('hide');
}
function loadSubCat()
{
	var mainCategory = $('#frmPopUp #cboMainCategory').val();
	var url 		= basePath+"item_allocation_db.php?RequestType=loadSubCategory&mainCategory="+mainCategory;
	var httpobj 	= $.ajax({url:url,type:'POST',async:false})
	$('#cboSubCategory').html(httpobj.responseText);	
}
function loadItemGrid()
{
	$("#frmPopUp #tblItemsPopup tr:gt(1)").remove();
			
	var mainCategory = $('#frmPopUp #cboMainCategory').val();
	var subCategory = $('#frmPopUp #cboSubCategory').val();
	var description = $('#frmPopUp #txtItem').val();
	var department	= $('#frmItemAllocation #cboDepartment').val();
								
	var url 		= basePath+"item_allocation_db.php?RequestType=loadItems";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		data:"mainCategory="+mainCategory+"&subCategory="+subCategory+"&department="+department+"&description="+description, 
		async:false,
		success:function(json){
	
			$('#frmPopUp #tblItemsPopup #tblContent').html(json.gridDetail);	
		}
	});	
}
function addItems()
{
	$('#frmPopUp .chkSubCat:checked').each(function(){
	var popObj			= $(this);
	var popSerial		= popObj.attr('id');
	var popSubId 		= popObj.parent().parent().find('.cls_td_subCategory').attr('id');
	var popItemId 		= popObj.parent().parent().find('.cls_td_item').attr('id');
	var serialNo		= popObj.attr('id');//alert(popSubId);

	var found 			= false;
	$('#frmItemAllocation #tblMain >tbody >tr').each(function(){
		var mainObj		= $(this);
		var mainSerial	= $(this).attr('id');
		var mainSubId   = mainObj.find('.cls_td_subCategory').attr('id');
		var mainItemId	= mainObj.find('.cls_td_item').attr('id');
		
		if(popSerial == mainSerial)
			found = true;
	});
	
	if(!found)
	{					
		var x = "<tr id=\""+popObj.attr('id')+"\">"+
		"<td align=\"center\" style=\"text-align:center\" id="+serialNo+" class=\"cls_td_del\"><img class=\"mouseover removeRow\" src=\"images/del.png\" /></td>"+
		"<td class=\"cls_td_subCategory\" id="+popSubId+">"+popObj.parent().parent().find('.cls_td_subCategory').html()+"</td>"+
		"<td class=\"cls_td_item\" id="+popItemId+">"+popObj.parent().parent().find('.cls_td_item').html()+"</td>"+
		"<td style=\"text-align:center\">"+popObj.parent().parent().find('.cls_td_unit').html()+"</td></tr>";
		
		$("#frmItemAllocation #tblMain > tbody:last").append(x); 
	}
});
disablePopup();
				
}
function setToAddNew(obj)
{	
	if(!$('#frmItemAllocation').validationEngine('validate')){
		return false;
	}
	
	var loop	= 0;
	popupWindow3('1');
	$('#popupContact1').load(basePath+'popUpitem.php?',function(){
			
		$('#frmPopUp #butClose').die('click').live('click',disablePopup);		
	});		
}

function CheckAll_Approve(obj,obj1)
{
	$('#frmPopUp .chkSubCat').each(function(){
			if(!$(this).is(':disabled')){
				if(obj)
					$(this).attr('checked','checked');
				else
					$(this).removeAttr('checked');
			}
		});
}
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
function clearForm()
{
	$('#frmItemAllocation')[0].reset();
	$("#frmItemAllocation #tblMain tr:gt(0)").remove();
	$('#frmItemAllocation #cboDepartment').val('');
}

function removeRow()
{
	$(this).parent().parent().remove();
}

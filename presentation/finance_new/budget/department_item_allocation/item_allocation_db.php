<?php
session_start();
ini_set('display_errors',0);
$backwardseperator 		= "../../../../";
$mainPath 				= $_SESSION['mainPath'];
$userId 				= $_SESSION['userId'];
$location 				= $_SESSION['CompanyID'];
$companyId 				= $_SESSION['headCompanyId'];

$savedMasseged			= "";
$error_sql				= "";
$savedStatus			= true;
$editMode				= false;

include_once "{$backwardseperator}dataAccess/Connector.php";
require_once "../../../../class/cls_commonFunctions_get.php";
require_once "../../../../class/cls_commonErrorHandeling_get.php";
require_once "../../../../class/finance/budget/cls_common_function_budget_get.php";
require_once "../../../../class/finance/budget/department_item_allocation/department_item_allocation_get.php";
require_once "../../../../class/finance/budget/department_item_allocation/department_item_allocation_set.php";

$obj_common					= new cls_commonFunctions_get($db);
$obj_commonErr				= new cls_commonErrorHandeling_get($db);
$obj_comm_budget_get		= new cls_common_function_budget_get($db);
$obj_item_allocation_get	= new cls_department_item_allocation_get($db);
$obj_item_allocation_set	= new cls_department_item_allocation_set($db); 
$requestType				= $_REQUEST['RequestType'];


if($requestType == 'Save')
{
	$db->begin();
	$detailArray			= json_decode($_REQUEST['DetailArray'],true);	
	$depatment			 	= $_REQUEST['Department'];
	$deleteArr				= $obj_item_allocation_set->deleteItem($depatment,'RunQuery2');
	
	if($saveArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $saveArr['savedMassege'];
		$error_sql		= $saveArr['error_sql'];	
	}
	foreach($detailArray as $detailArray)
	{
		$serialID			= $detailArray['serialID'];
		$saveArr			= $obj_item_allocation_set->saveItem($serialID,$depatment,'RunQuery2');
		
		if($saveArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $saveArr['savedMassege'];
			$error_sql		= $saveArr['error_sql'];	
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 	= "pass";
		$response['msg'] 	= "Saved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 	= "fail";
		$response['msg'] 	= $savedMasseged;
		$response['sql']	= $error_sql;
	}
	echo json_encode($response);
	
}
else if($requestType == 'loadSubCategory')
{
	$mainCategory  = $_REQUEST['mainCategory'];
	$subCat		   = $obj_comm_budget_get->loadSubCategory($mainCategory,'RunQuery');
	$html = "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($subCat))
	{
		$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
	}
	echo $html;
}
else if($requestType == 'loadItems')
{
	$mainCategory  	= $_REQUEST['mainCategory'];
	$subCategory  	= $_REQUEST['subCategory'];
	$description  	= $_REQUEST['description'];
	$department	  	= $_REQUEST['department'];	
	//echo $subCategory;
	$subcat 		= $obj_comm_budget_get->loadBudgetCategoryList($companyId,$location,$mainCategory,$subCategory,$description);
	$content 		= '';

while($row=mysqli_fetch_array($subcat))
{
		$subCatName 	= $row['SUB_CATEGORY_NAME'];
		$serialNo 		= $row['SERIAL_ID'];
		$itemName 		= $row['ITEM_NAME'];
		$subCatID 		= $row['SUB_CATEGORY_ID'];
		$uom 			= $row['UNIT'];
		//die($serialNo);
				
		$content .= "<tr class=\"normalfnt\" id=\"".$serialNo."\"><td align=\"center\" bgcolor=\"#FFFFFF\" id=\"".$mainCategory."\"><input type=\"checkbox\" class=\"chkSubCat\" id=\"".$serialNo."\" /></td>";
		$content .="<td align=\"center\" bgcolor=\"#FFFFFF\" id=\"".$subCatID."\" class=\"cls_td_subCategory\">".$subCatName."</td>";
									
		$content .="<td align=\"center\" bgcolor=\"#FFFFFF\" class=\"cls_td_item\">".$itemName."</td>";
		$content .="<td align=\"center\" bgcolor=\"#FFFFFF\" id=\"".$uom."\" class=\"cls_td_unit\">".$uom."</td>";
	
}
	$response['gridDetail'] = $content;
	echo json_encode($response);
	
}
else if($requestType == 'searchItem')
{
	$searchId	= $_REQUEST['searchId'];
	$locationId	= $_REQUEST['locationId'];
	$content	= '';
	$result		= $obj_item_allocation_get->getItem($locationId,$searchId,'RunQuery');
	while($row	= mysqli_fetch_array($result))
	{
		$subcat	  	= $row['SUB_CATEGORY_NAME'];
		$subCatID	= $row['SUB_CATEGORY_ID'];
		$serialNo	= $row['SERIAL_ID'];
		$itemName	= $row['ITEM_NAME'];
		$uom		= $row['UNIT'];
		
		$content .= "<tr id=\"".$serialNo."\"><td align=\"center\" style=\"text-align:center\" id=\"".$serialNo."\" class=\"cls_td_del\"><img class=\"mouseover removeRow\" src=\"images/del.png\" /></td>";
		$content .= "<td class=\"cls_td_subCategory\" id=\"".$subCatID."\">".$subcat."</td>";
		$content .=	"<td class=\"cls_td_item\">".$itemName."</td>";
		$content .= "<td style=\"text-align:center\">".$uom."</td></tr>";
		
	}
	$response['gridDetail'] = $content;
	echo json_encode($response);	
}
?>
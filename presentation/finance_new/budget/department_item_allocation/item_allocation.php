<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId						= $_SESSION["CompanyID"];
$companyId						= $_SESSION["headCompanyId"];
$userId							= $_SESSION["userId"];

include_once 	"class/cls_commonErrorHandeling_get.php";
include 		"class/cls_commonFunctions_get.php";

$obj_common_func				= new cls_commonFunctions_get($db);
$obj_commonErrHandle			= new cls_commonErrorHandeling_get($db);
$locationArr					= $obj_common_func->loadLocation($companyId,'RunQuery');

$depat							= $obj_common_func->loadDepartment('RunQuery');
$permision_arr					= $obj_commonErrHandle->get_permision_withApproval_save($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_save					= $permision_arr['permision'];

?>
<head>
<title>Budget Item Allocation</title>
<!--<script type="text/javascript" src="presentation/finance_new/budget/department_item_allocation/item_allocation_js.js"></script>-->
</head>
<body>
<form id="frmItemAllocation" name="frmItemAllocation" autocomplete="off" method="post">
<div align="center">
	<div class="trans_layoutD">
	<div class="trans_text">Budget Item Allocation</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr class="normalfnt">
    <td><table width="100%" border="0" class="normalfnt">
        <tr >
        	<td width="16%">Location</td>
            <td width="35%"><select id="cboLocation" name="cboLocation" style="width:200px">
            <option value=""></option>
            <?php
			while ($row = mysqli_fetch_array($locationArr))
			{
				if($locationId == $row['intId'])
					echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
				else
					echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
			}
			?>
            </select></td>
            <td width="15%">Department</td>
            <td width="29%">
           <select name="cboDepartment" id="cboDepartment" style="width:200px"  class="validate[required] txtText">
                 <option value=""></option>
                 <?php
					while($row=mysqli_fetch_array($depat))
					{
						if($header['DEPARTMENT_ID']==$row['intId'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
					?>
							
	        </select> </td>
        </tr>
        </table></td>
    </tr>
    <tr><td style="text-align:right"><a class="button green small" id="butAdd" name="butAdd">Add</a></td></tr>
     <tr>
        <td><table id="tblMain" width="100%" class="bordered">
        <thead>
        <tr>
        <th width="7%">Del</th>
        <th width="28%">Sub Category</th>
        <th width="25%">Item</th>
        <th width="21%">UOM</th>
        </tr>
        </thead>
       	<tbody id="tblBody">
        <?php
		while($row = mysqli_fetch_array($detail_arr))
		{
		?>
        <tr class = "td_invoiceNo" id="">
        <td align="center" style="text-align:center"><img class="mouseover removeRow" src="images/del.png" /></td>							        <td class="cls_td_subCategory" style="text-align:center" id=""></td>
		<td class="cls_td_item" id=""></td>
		<td class ="cls_td_unit" id="" style="text-align:right"></td>					        
        	
					
        </tr>
        <?php
		}?>
        </tbody>
        </table></td>
        </tr>
        
         <tr>
          <td width="100%" align="center" ><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" name="butSave">Save</a><a  class="button white medium" id="butClosePop" name="butClosePop" href="main.php">Close</a></td>
        </tr>
    </table>
    </div>
    </div>
    </form>
   </body>
   <div style="width:500px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$loginUserId		= $_SESSION['userId'];

?>
<title>Budget Item Selection</title>
<!--<script type="text/javascript" src="presentation/finance_new/budget/budgetItemSelection/itemSelection.js"></script>-->

<form id="frmItemSelection" name="frmItemSelection" method="post" >
        <?php
			include "class/tables/mst_maincategory.php";
			include "class/tables/mst_subcategory.php";
						
			$obj_maincategory			= new mst_maincategory($db);
			$obj_subcategory			= new mst_subcategory($db);
			$db->connect();
		?>
  <div align="center">
    <div class="trans_layoutD" style="width:650px">
      <div class="trans_text"> Budget Item Selection</div>
      <table width="100%" border="0">
        <tr class="normalfnt">
        <td>
        	<table width="100%" border="0" class="tableBorder">
                <tr>
                	<td></td>
            	  	<td></td>
            	  	<td></td>
            	  	<td></td>
                </tr>
            	<tr>
                <td width="14%">&nbsp;</td>
                <td width="21%">Main Category</td>
                <td width="41%"><select name="cboMainCategory" id="cboMainCategory"  style="width:200px">
                <option></option>
                <?php
				$mainCat	= $obj_maincategory->select('intId,strName',NULL,'intStatus=1',NULL,NULL);
				while($row = mysqli_fetch_array($mainCat))
				{
					echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
				}
				?>
                </select></td>
                <td width="24%">&nbsp;</td>
                </tr>
                <tr>
                <td width="14%">&nbsp;</td>
                <td width="21%">Sub Category<span class="compulsoryRed">*</span></td>
                <td width="41%"><select name="cboSubCategory" id="cboSubCategory" style="width:200px" class="validate[required]">
               	<option value=""></option>
				<?php
					$subCat		   = $obj_subcategory->select('intId,strName',NULL,'intStatus=1',NULL,NULL);
					while($row = mysqli_fetch_array($subCat))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
				?>
                </select></td>
                <td width="24%">&nbsp;</td>
                </tr>
                <tr>
                <td width="14%">&nbsp;</td>
                <td width="21%">Item Description</td>
                <td width="41%"><input name="txtItem" id="txtItem" style="width:200px" type="text" /></td>
                <td width="24%" style="text-align:left"><a class="button green small" id="butSearch" name="butSearch">Search</a></td>
                </tr>
                <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                </tr>
            </table>
        </td>
        </tr>
        <tr>
          <td><div style="width:650px;height:300px;overflow:scroll" >
              <table width="600" class="bordered" id="tblItems">
                <thead>
                  <tr>
                    <th width="8%" rowspan="2" ></th>
                    <th width="15%" rowspan="2" >Item Code</th>
                    <th width="34%" rowspan="2" >Item Name</th>
                    <th width="13%" rowspan="2" >UOM</th>
                    <th width="15%" rowspan="2" >Budget Type</th>
                    <th width="15%" rowspan="2" >Update</th>
                   
                  </tr>
                </thead>
                <tbody id="tblContent">
                </tbody>
              </table>
          </div></td>
        </tr>
        <tr>
        	<td>&nbsp;</td>	
        </tr>
      </table>
    </div>
  </div>
</form>
<?php
	$db->disconnect();	
?>
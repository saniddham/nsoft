// JavaScript Document
var basePath = "presentation/finance_new/budget/budgetItemSelection/";

$(document).ready(function(e) {
	$('#frmItemSelection').validationEngine();
    $('#frmItemSelection #cboMainCategory').die('change').live('change',loadSubcategory);
	$('#frmItemSelection #cboSubCategory').die('change').live('change',loadMaincategory);
	$('#frmItemSelection #butSearch').die('click').live('click',searchItem);
	$('#frmItemSelection .butUpdate').die('click').live('click',saveItem);
});
function loadSubcategory()
{
	var mainCategory 	= $('#frmItemSelection #cboMainCategory').val();
	var url 			= basePath+"itemSelection_db.php?RequestType=loadSubCategory&mainCategory="+mainCategory;
	var httpobj 		= $.ajax({url:url,type:'POST',async:false})
	$('#cboSubCategory').html(httpobj.responseText);	
}
function loadMaincategory()
{
	var subCategory 	= $('#frmItemSelection #cboSubCategory').val();
	var url 			= basePath+"itemSelection_db.php?RequestType=loadMainCategory&subCategory="+subCategory;
	$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				async:false,
				success:function(json){
						$('#frmItemSelection #cboMainCategory').val(json.mainCat);
				}
	});
}
function searchItem()
{
	showWaiting();
	$("#frmItemSelection #tblItemsPopup tr:gt(1)").remove();
			
	if($('#frmItemSelection #cboSubCategory').val() == '')
	{
		$('#frmItemSelection #cboSubCategory').validationEngine('showPrompt','Select Sub Category','fail');
	}
	var mainCategory 	= $('#frmItemSelection #cboMainCategory').val();
	var subCategory 	= $('#frmItemSelection #cboSubCategory').val();
	var description 	= $('#frmItemSelection #txtItem').val();
									
	var url 		= basePath+"itemSelection_db.php?RequestType=loadItems";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		data:"mainCategory="+mainCategory+"&subCategory="+subCategory+"&description="+description, 
		async:false,
		success:function(json){
	
			$('#frmItemSelection #tblItems #tblContent').html(json.gridDetail);	
			
		}
	});	
	hideWaiting();
}

function saveItem()
{
	//var mainCategory 	= $('#cboMainCategory').val();
	var btn				= $(this);		
	var subCategory 	= $('#frmItemSelection #cboSubCategory').val();
	var obj				= $(this).parent().parent();
	
	var itemID			= obj.attr('id');
	var type 			= obj.find('.cls_td_type').children().val();
	var check			= (obj.find('.td_chk_item').children().attr('checked')?1:0);
	
	var arrHeader = "{";
							arrHeader += '"subCategory":"'+subCategory+'",' ;
							arrHeader += '"itemID":"'+itemID+'",';
							arrHeader += '"type":"'+type+'",';
							arrHeader += '"check":"'+check+'"' ;
		
			arrHeader += "}";
	
	var RequestType 	= (check==1?'itemSave':'itemDelete');
	var url 	= basePath+"itemSelection_db.php?RequestType="+RequestType;
	
	var httpobj = $.ajax({
		url:url,
		data:"arrHeader="+arrHeader,
		dataType:'json',
		type:'POST',
		async:false,
		success:function(json)
		{
			btn.validationEngine('showPrompt',json.msg,json.type);
			if(json.type == 'pass')
			{
				var t=setTimeout(btn.validationEngine('hide'),10000);
			}
		},
		error:function(xhr,status)
		{
			btn.validationEngine('showPrompt',errormsg(xhr.status),'fail');
		}
	})
}
function alertX()
{
	$('#frmItemSelection #tblItems #tblContent').validationEngine('hide');	
}
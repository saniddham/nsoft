<?php 
	session_start();
	$loginUserId	= $_SESSION['userId'];
			
	//connection
	include "../../../../dataAccess/DBManager2.php";									$db							= new DBManager2();
	
	//class files
	include "../../../../class/error_handler.php";										$error_handler 				= new error_handler(); 
	include "../../../../class/sessions.php";											$obj_sessions				= new sessions();
	include "../../../../class/tables/mst_subcategory.php";								$obj_subcategory			= new mst_subcategory($db);
	include "../../../../class/tables/mst_maincategory.php";							$obj_maincategory			= new mst_maincategory($db);
	include "../../../../class/tables/mst_item.php";									$obj_mst_item				= new mst_item($db);
	include "../../../../class/tables/mst_budget_category_list.php";					$obj_budget_list			= new mst_budget_category_list($db);
	include "../../../../class/tables/mst_budget_category_list_department_wise.php";	$obj_dept_budget_items		= new mst_budget_category_list_department_wise($db);

	$requestType	= $_REQUEST['RequestType'];
	$editMode		= false;
	$location		= $obj_sessions->getLocationId();
	$company		= $obj_sessions->getCompanyId();
			
	if($requestType == 'loadSubCategory')
	{
		$db->connect();
		$mainCategory	= $_REQUEST['mainCategory'];
		
		$subCat			= $obj_subcategory->select('intId,strName',NULL,"intMainCategory='$mainCategory'",NULL,NULL);
		$html			= "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($subCat))
		{
			$html	   .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		$db->disconnect();
		echo $html;
		
	}
	else if($requestType == 'loadMainCategory')
	{
		$db->connect();
		$subCategory			= $_REQUEST['subCategory'];
		$cols					= ' mst_maincategory.intId AS intId,
									mst_maincategory.strName';
		$joins					= 'INNER JOIN mst_maincategory ON mst_subcategory.intMainCategory = mst_maincategory.intId';
		$wheres					= "mst_subcategory.intId = '$subCategory'";
		$mainCat				= $obj_subcategory->select($cols,$joins,$wheres,NULL,NULL);
		$row					= mysqli_fetch_array($mainCat);
		$mainCategory			= $row['intId'];
		$response['mainCat']	= $mainCategory;
		
		$db->disconnect();
		echo json_encode($response);
		
	}
	else if($requestType == 'loadItems')
	{
		$db->connect();
		$mainCategory	= $_REQUEST['mainCategory'];
		$subCategory	= $_REQUEST['subCategory'];
		$description	= $_REQUEST['description'];
		$content		= '';
		
		$col_r			= ' mst_item.intId,
							mst_item.strCode AS itemCode,
							mst_item.strName AS itemName,
							mst_units.strName AS UOM';
							
		$join_r			= 'INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId';
		$where_r		= "intMainCategory = '$mainCategory' AND 
							intSubCategory = '$subCategory'";
							
		if($description != '')
		{
			$where_r    .= "AND mst_item.strName like '%$description%'";	
		}
		
		$itemArr		= $obj_mst_item->select($col_r,$join_r,$where_r,NULL,NULL);
			
		while($row = mysqli_fetch_array($itemArr))
		{
			$item			= $row['intId'];
			$budgetListArr	= $obj_budget_list->select('ITEM_ID,BUDGET_TYPE',NULL,"COMPANY_ID = '$company' AND LOCATION_ID = '$location' AND SUB_CATEGORY_ID = '$subCategory' AND ITEM_ID='$item'",NULL,NULL);	
			$listRow		= mysqli_fetch_array($budgetListArr);
			$content		.= "<tr class=\"cls_itemID\" id=\"".$row['intId']."\">";
			if(mysqli_num_rows($budgetListArr)>0)
				$content	.= "<td align=\"center\" bgcolor=\"#FFFFFF\" class=\"td_chk_item\" id=\"".$row['intId']."\"><input type=\"checkbox\" id=\"chk_item\" class =\"chk_item\" checked=\"checked\"/></td>";
			else
				$content	.= "<td align=\"center\" bgcolor=\"#FFFFFF\" class=\"td_chk_item \" id=\"".$row['intId']."\"><input type=\"checkbox\" id=\"chk_item\" class =\"chk_item\"/></td>";
				
			$content 		.= "<td align=\"center\" bgcolor=\"#FFFFFF\" class=\"cls_td_item\">".$row['itemCode']."</td>";
			$content 		.= "<td align=\"left\" bgcolor=\"#FFFFFF\" class=\"cls_td_name\">".$row['itemName']."</td>";
			$content 		.= "<td align=\"center\" bgcolor=\"#FFFFFF\" class=\"cls_td_uom\">".$row['UOM']."</td>";
			if($listRow['BUDGET_TYPE'] == 'U' && $row['intId'] == $listRow['ITEM_ID'])
				$content 	.= "<td align=\"center\" bgcolor=\"#FFFFFF\" class=\"cls_td_type\"><select style=\"width:100%\" id = \"cboType\"><option value = \"\"></option><option value =\"U\" selected=\"selected\">Unit</option><option value =\"A\">Amount</option> </select></td>";
			else if($listRow['BUDGET_TYPE'] == 'A' && $row['intId'] == $listRow['ITEM_ID'])
				$content 	.= "<td align=\"center\" bgcolor=\"#FFFFFF\" class=\"cls_td_type\"><select style=\"width:100%\" id = \"cboType\"><option value = \"\"></option><option value =\"U\">Unit</option><option value =\"A\" selected=\"selected\">Amount</option> </select></td>";
			else
				$content 	.= "<td align=\"center\" bgcolor=\"#FFFFFF\" class=\"cls_td_type\"><select style=\"width:100%\" id = \"cboType\"><option value = \"\"></option><option value =\"U\" >Unit</option><option value =\"A\">Amount</option> </select></td>";	
			$content		.= "<td align=\"center\" bgcolor=\"#FFFFFF\" class=\"cls_td_update\"><a class=\"button green small butUpdate\" id=\"butUpdate\" name=\"butUpdate\">Update</a></td></tr>";
			
		}
		$response['gridDetail'] = $content;
		$db->disconnect();	
		echo json_encode($response);
	}
	if($requestType == 'itemSave')
	{
		//check item alredy in budget category list
		//if so item update item
		//else save to budget category list
		
		$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
		$subCategory		= $arrHeader['subCategory'];
		$itemID				= $arrHeader['itemID'];
		$type				= $arrHeader['type'];
		$response['type']	= true;
		
		try
		{
			$db->connect();
			//check item alredy in budget category list
			if($type == '')
			{
				throw new exception("Select Budget Type");	
			}
			$where_s		= "COMPANY_ID='$company' and LOCATION_ID='$location' and SUB_CATEGORY_ID='$subCategory' and ITEM_ID='$itemID'";
			$selectItem		= $obj_budget_list->select('*',NULL,$where_s,NULL,NULL);
			
			if(mysqli_num_rows($selectItem) > 0)
			{
				$obj_budget_list->set($company,$location,$subCategory,$itemID);
				$obj_budget_list->setBUDGET_TYPE($type);
				$obj_budget_list->commit();
				$editMode	= true;
			}
			else
			{
				$data =array(
				'COMPANY_ID'			=> $company,
				'LOCATION_ID'			=> $location,
				'SUB_CATEGORY_ID'		=> $subCategory,
				'ITEM_ID'				=> $itemID,
				'BUDGET_TYPE'			=> $type);
				
				$insertArr	= $obj_budget_list->insert($data);
				if(!$insertArr['status'])
				{
					throw new Exception("Save Error!");						
				}
									
			}
			$db->commit();
			$response['type'] 			= "pass";
			if($editMode)
				$response['msg'] 		= "Updated Successfully.";
			else
				$response['msg'] 		= "Saved Successfully.";
		}
		catch(Exception $e)
		{
			$db->rollback();
			$response['msg'] 		=  $e->getMessage();;
			$response['error'] 		=  $error_handler->jTraceEx($e);
			$response['type'] 		=  'fail';
			$response['sql']		=  $db->getSql();
			$response['mysql_error']=  $db->getMysqlError();
		}
		
			$db->disconnect();
			echo json_encode($response);
		
	}
	if($requestType == 'itemDelete')
	{
		//check whether item alredy allocated to department
		//if so, show error message
		//check item is in budget list table
		//if not, show error msg
		//else delete 	
		$arrHeader 		= json_decode($_REQUEST['arrHeader'],true);
		$subCategory	= $arrHeader['subCategory'];
		$itemID			= $arrHeader['itemID'];
		$type			= $arrHeader['type'];
	
		try
		{
			$db->connect();
		
			$where_s		= "COMPANY_ID='$company' and LOCATION_ID='$location' and SUB_CATEGORY_ID='$subCategory' and ITEM_ID='$itemID'";
			$selectItem		= $obj_budget_list->select('*',NULL,$where_s,NULL,NULL);
			$rowSelect		= mysqli_fetch_array($selectItem);
			
			if(mysqli_num_rows($selectItem) > 0)
			{
				$serialId		= $rowSelect['SERIAL_ID'];
				$deptItem		= $obj_dept_budget_items->select('*',NULL,"SERIAL_ID = '$serialId'",NULL,NULL);
				if(mysqli_num_rows($deptItem) > 0)
				{
					throw new Exception("Cannot delete!Item already allocated to department.");	
				}
				else
				{
					$deleteArr	= $obj_budget_list->delete($where_s);	
					if(!$deleteArr['status'])
					{
						throw new Exception("Delete Error!");	
					}
					
					$db->commit();
					$response['type'] 	= "pass";
					$response['msg'] 	= "Deleted Successfully.";	
				}
			}
			else
			{
				throw new Exception("Cannot delete!Item not allocated as a budget item.");	
			}
		}
		catch(Exception $e)
		{
			$db->rollback();
			$response['msg'] 		=  $e->getMessage();;
			$response['error'] 		=  $error_handler->jTraceEx($e);
			$response['type'] 		=  'fail';
			$response['sql']		=  $db->getSql();
			$response['mysql_error']=  $db->getMysqlError();
		}
		
			$db->disconnect();
			echo json_encode($response);
		
	}
	
?>
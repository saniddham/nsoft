<?php
date_default_timezone_set('Asia/Kolkata');
ini_set('display_errors',0);
session_start();
$backwardseperator = "../../../../";

include  "{$backwardseperator}dataAccess/Connector.php";
include "../../../../class/finance/budget/cls_common_function_budget_get.php";

$obj_common_function_budget_get	= new cls_common_function_budget_get($db);	

$companyId 		= $_SESSION["headCompanyId"];
$locationId		= $_SESSION["CompanyID"];
$dept			= $_REQUEST["Dept"];
$currentMonth	= date('Y-M');
$nextMonth		= date('Y-M',strtotime("+1 months", strtotime(date('Y-m-d'))));
$result			= $obj_common_function_budget_get->getFinancePeriod($companyId,date('Y-m-d'),'RunQuery');
$financeId		= $result["FINANCE_ID"];
$mainCat		= $obj_common_function_budget_get->loadMainCategory('RunQuery');
?>

<title>Sub category popup</title>

<script type="text/javascript" src="presentation/finance_new/budget/transfering/budgetTransfer_js.js"></script>

<form id="frmPopUp" name="frmPopUp" method="post">
  <div align="center">
    <div class="trans_layoutD" style="width:650px">
      <div class="trans_text"> Sub Categories</div>
      <table width="100%" border="0">
        <tr class="normalfnt">
        <td>
        	<table width="100%" border="0" class="tableBorder">
                <tr>
                	<td></td>
            	  	<td></td>
            	  	<td></td>
            	  	<td></td>
                </tr>
            	<tr>
                <td width="14%">&nbsp;</td>
                <td width="21%">Main Category</td>
                <td width="41%"><select name="cboMainCategory" id="cboMainCategory" style="width:250px">
                <option></option>
                <?php
				while($row = mysqli_fetch_array($mainCat))
				{
					echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
				}
				?>
                </select></td>
                <td width="24%">&nbsp;</td>
                </tr>
                <tr>
                <td width="14%">&nbsp;</td>
                <td width="21%">Sub Category</td>
                <td width="41%"><select name="cboSubCategory" id="cboSubCategory" style="width:250px">
                <option value=""></option>
                </select></td>
                <td width="24%">&nbsp;</td>
                </tr>
                <tr>
                <td width="14%">&nbsp;</td>
                <td width="21%">Item Description</td>
                <td width="41%"><input name="txtItem" id="txtItem" style="width:250px" type="text" /></td>
                <td width="24%" style="text-align:left"><a class="button green small" id="butSearch" name="butSearch">Search</a></td>
                </tr>
                <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                </tr>
            </table>
        </td>
        </tr>
        <tr>
          <td><div style="width:650px;height:400px;overflow:scroll" >
              <table width="432" class="bordered" id="tblItemsPopup">
                <thead>
                  <tr>
                    <th width="5%" rowspan="2" ><input type="checkbox" id="chkCheckAll" name="chkCheckAll" /></th>
                    <th width="20%" rowspan="2" >Sub Category</th>
                    <th width="30%" rowspan="2" >Item Description</th>
                    <th width="10%" rowspan="2" >Unit</th>
                    <th height="22" colspan="2" ><?php echo $currentMonth?></th>
                    <th colspan="2" ><?php echo $nextMonth?></th>
                  </tr>
                  <tr>
                    <th width="12%" height="22" >Amount</th>
                    <th width="11%" >Units</th>
                    <th width="11%" >Amount</th>
                    <th width="11%" >Units</th>
                  </tr>
                </thead>
                <tbody id="tblContent">
                </tbody>
              </table>
          </div></td>
        </tr>
        <tr>
          <td width="100%" align="center" ><a class="button white medium" id="butAdd" name="butAdd">Add</a><a  class="button white medium" id="butClose" name="butClose">Close</a></td>
        </tr>
      </table>
    </div>
  </div>
</form>
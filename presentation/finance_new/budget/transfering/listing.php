<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$userId				= $_SESSION["userId"];
$programCode		= 'P0842';

$menuId				= 842;
$reportId			= 911;

include_once "libraries/jqgrid2/inc/jqgrid_dist.php";
require_once "class/finance/budget/tranferring/cls_budgetTransfer_get.php";

$obj_budj_transf_get= new cls_budget_transfer_get($db);

$approveLevel 		= getMaxApproveLevel();
//$sql				= $obj_cls_lc_get->get_listing_sql($programCode,$approveLevel,$intUser,'RunQuery');

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'STATUS'=>'BTH.STATUS',
				'CONCAT_TRANSFER_NO'=>"CONCAT(BTH.TRANSFER_NO,'/',BTH.TRANSFER_YEAR)",
				'LOCATION_NAME'=>'LO.strName',
				'DEPARTMENT_NAME'=>'D.strName'
				);
				
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='STATUS')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND DATE(BTH.CREATED_DATE) = '".date('Y-m-d')."'";
	
// }

$sql = "SELECT SUB_1.* FROM
			(SELECT
			  CONCAT(BTH.TRANSFER_NO,'/',BTH.TRANSFER_YEAR )			AS CONCAT_TRANSFER_NO,
			  BTH.TRANSFER_NO											AS TRANSFER_NO,
			  BTH.TRANSFER_YEAR											AS TRANSFER_YEAR,
			  BTH.FINANCE_YEAR											AS FINANCE_YEAR,
			  FROM_YEAR													AS FROM_YEAR,
			  FROM_MONTH												AS FROM_MONTH,
			  TO_YEAR 													AS TO_YEAR,
			  TO_MONTH													AS TO_MONTH,
			  IF(BTH.STATUS=1,'Approved',IF(BTH.STATUS=0,'Rejected','Pending')) AS STATUS,
			  LO.intId 												AS LOCATION_ID,
			  LO.strName         									AS LOCATION_NAME,
			  D.intId												AS DEPARTMENT_ID,
			  D.strName          									AS DEPARTMENT_NAME,
			  (SELECT ROUND(SUM(BTD.AMOUNT),2)						 		
			   FROM budget_transfer_details BTD
				WHERE BTD.TRANSFER_NO = BTH.TRANSFER_NO
				  AND BTD.TRANSFER_YEAR = BTH.TRANSFER_YEAR) AS totAmount,
				 
				 (SELECT SUM(BTD.UNITS)						 		
			   FROM budget_transfer_details BTD
				WHERE BTD.TRANSFER_NO = BTH.TRANSFER_NO
				  AND BTD.TRANSFER_YEAR = BTH.TRANSFER_YEAR) AS totUnits, 
				   ";
				  
		$sql .= "IFNULL((
				SELECT
				CONCAT(U.strUserName,BTA.APPROVED_DATE) AS CONCT_APPROVE
				FROM
				budget_transfer_approve_by BTA
				INNER JOIN sys_users U ON BTA.APPROVED_BY = U.intUserId
				WHERE
				BTA.TRANSFER_NO  = BTH.TRANSFER_NO AND
				BTA.TRANSFER_YEAR =  BTH.TRANSFER_YEAR AND
				BTA.APPROVED_LEVEL_NO =  '1' AND
				BTA.STATUS =  '0'
				),IF(((SELECT
				MP.int1Approval 
				FROM menupermision MP
				Inner Join menus M ON MP.intMenuId = M.intId
				WHERE
				M.strCode = '$programCode' AND
				MP.intUserId =  '$intUser')=1 AND BTH.STATUS>1),'Approve', '')) as `1st_Approval`, ";
				
	for($i=2; $i<=$approveLevel; $i++){		
		if($i==2){
			$approval	= "2nd_Approval";
		}
		else if($i==3){
			$approval	= "3rd_Approval";
		}
		else {
			$approval	= $i."th_Approval";
		}
		
		
		$sql .= "IFNULL( ( SELECT CONCAT(U.strUserName,BTA.APPROVED_DATE) AS CONCT_APPROVE FROM budget_transfer_approve_by BTA 
				INNER JOIN sys_users U ON BTA.APPROVED_BY = U.intUserId 
				WHERE BTA.TRANSFER_NO = BTH.TRANSFER_NO AND BTA.TRANSFER_YEAR = BTH.TRANSFER_YEAR AND BTA.APPROVED_LEVEL_NO = '$i' 
				AND BTA.STATUS = '0' ), 
				
				IF( ((SELECT MP.int".$i."Approval FROM menupermision MP 
				INNER JOIN menus M ON MP.intMenuId = M.intId 
				WHERE M.strCode = '$programCode' AND MP.intUserId = '$userId')=1 AND (BTH.STATUS>1) 
				AND (BTH.STATUS<=BTH.APPROVE_LEVEL) AND ((SELECT CONCAT(U.strUserName,BTA.APPROVED_DATE) AS CONCT_APPROVE FROM 		budget_transfer_approve_by BTA 
				INNER JOIN sys_users U ON BTA.APPROVED_BY = U.intUserId 
				WHERE BTA.TRANSFER_NO = BTH.TRANSFER_NO AND BTA.TRANSFER_YEAR = BTH.TRANSFER_YEAR AND BTA.APPROVED_LEVEL_NO = ($i-1) 
				AND BTA.STATUS = '0' )<>'')), 'Approve', 
				if($i>BTH.APPROVE_LEVEL,'-----','')) ) as `".$approval."`,"; 
		
		}
	
	$sql .= " 'View'											AS VIEW,
			  'Report'											AS REPORT
			  FROM budget_transfer_header BTH
			  INNER JOIN mst_locations LO
			ON LO.intId = BTH.LOCATION_ID
		  INNER JOIN mst_department D
			ON D.intId = BTH.DEPARTMENT_ID
			where 1=1
			$where_string	  
			)  
		AS SUB_1 WHERE 1=1";

//die($sql);
$jq 	= new jqgrid('',$db);	
$col 	= array();
$cols 	= array();

//BEGIN - STATUS {
$col["title"] 			= "Status";
$col["name"] 			= "STATUS";
$col["width"] 			= "2"; 						// not specifying width will expand to fill space
$col["align"] 			= "left";
$col["sortable"] 		= true; 						// this column is not sortable
$col["search"] 			= true; 						// this column is not searchable
$col["editable"] 		= false;
$col["stype"] 			= "select";
$str 					= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected";  // all row, blank row, then all db values. ; is separator
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;
//END 	- STATUS }
$col["title"]			= "Transfer No";
$col["name"]			= "TRANSFER_NO";
$col["width"]			= "1";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$col["hidden"]			= true;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Transfer Year";
$col["name"]			= "TRANSFER_YEAR";
$col["width"]			= "1";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$col["hidden"]			= true;
$cols[]			    	= $col;
$col					= NULL;

$col["title"] 			= "Transfer No";
$col["name"] 			= "CONCAT_TRANSFER_NO";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col['link']			= "?q=".$menuId."&serialNo={TRANSFER_NO}&serialYear={TRANSFER_YEAR}";
$col["linkoptions"] 	= "target='budgetTransfer.php'";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "finance year";
$col["name"] 			= "FINANCE_YEAR";
$col["hidden"] 			= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "to month";
$col["name"] 			= "TO_MONTH";
$col["hidden"] 			= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "from month";
$col["name"] 			= "FROM_MONTH";
$col["hidden"] 			= true;
$cols[] 				= $col;	
$col					= NULL;


$col["title"] 			= "Location Id";
$col["name"] 			= "LOCATION_ID";
$col["hidden"] 			= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Location";
$col["name"] 			= "LOCATION_NAME";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Department Id";
$col["name"] 			= "DEPARTMENT_ID";
$col["hidden"] 			= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Department";
$col["name"] 			= "DEPARTMENT_NAME";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Amount";
$col["name"] 			= "totAmount";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Units";
$col["name"] 			= "totUnits";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

//BEGIN - FIRST APPROVAL {
$col["title"] 			= "1st Approval";
$col["name"] 			= "1st_Approval";
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportId.'&serialNo={TRANSFER_NO}&serialYear={TRANSFER_YEAR}&Mode=Confirm';
$col["linkoptions"] 	= "target='rpt_budgetTransfer.php'";
$col['linkName']		= 'Approve';
$cols[] 				= $col;	
$col					= NULL;
//END	- FIRST APPROVEL }

//BEGIN - OTHER APPROVALS {
for($i=2; $i<=$approveLevel; $i++)
{
	if($i==2){
		$ap		= "2nd Approval";
		$ap1	= "2nd_Approval";
	}
	else if($i==3){
		$ap		= "3rd Approval";
		$ap1	= "3rd_Approval";
	}
	else {
		$ap		= $i."th Approval";
		$ap1	= $i."th_Approval";
	}

$col["title"] 				= $ap;
$col["name"] 				= $ap1;
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q='.$reportId.'&serialNo={TRANSFER_NO}&serialYear={TRANSFER_YEAR}&Mode=Confirm';
$col["linkoptions"] 		= "target='rpt_budgetTransfer.php'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;
}	
//END 	- OTHER APPROVALS }

$col["title"] 			= "Report";
$col["name"] 			= "REPORT";
$col["width"] 			= "1";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= '?q='.$reportId.'&serialNo={TRANSFER_NO}&serialYear={TRANSFER_YEAR}';
$col["linkoptions"] 	= "target='rpt_budgetTransfer.php'";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Budget Transferring Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= ''; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"STATUS","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;
//$grid["postData"] = array("filters" => $sarr ); 


$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Budget Transferring Listing</title>
<?php
echo $out;

function getMaxApproveLevel()
{
	global $db;

	$sqlp = "SELECT
			  COALESCE(MAX(BTH.APPROVE_LEVEL),0) AS APPROVE_LEVEL
			FROM budget_transfer_header BTH";					
	$resultp 	= $db->RunQuery($sqlp);
	$rowp		= mysqli_fetch_array($resultp);			 
	return $rowp['APPROVE_LEVEL'];
}
?>
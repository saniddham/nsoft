var basePath	= "presentation/finance_new/budget/transfering/";
var reportId	= 911;
$(document).ready(function(){
	
	$('#frmBudgetTransfer').validationEngine();
	
	$('#frmBudgetTransfer #butNew').die('click').live('click',clearForm);
	$('#frmBudgetTransfer #butSave').die('click').live('click',save);
	$('#frmBudgetTransfer .removeRow').die('click').live('click',removeRow);
	$('#frmBudgetTransfer #butReport').die('click').live('click',report);
	$('#frmBudgetTransfer #butApprove').die('click').live('click',approve);
	$('#frmBudgetTransfer .butReportformError').die('click').live('click',hideReportError);
	
	$('#frmBudgetTransfer #btnAdd').die('click').live('click',loadPopUp);
	
	$('#frmPopUp #cboMainCategory').die('change').live('change',loadSubCategory);
	$('#frmPopUp #butSearch').die('click').live('click',searchData);
	$('#frmPopUp #butAdd').die('click').live('click',addData);
	
	$('#frmPopUp #chkCheckAll').die('click').live('click',function(){
		
		CheckAll_Approve($(this).is(':checked'));
	});
			
});
function addData()
{
	var loop = 0;
	$('#frmPopUp .chkSubCat:checked').each(function(){
		
		var popObj			= $(this);
		var popSubId 		= popObj.parent().parent().find('.cls_td_subCategory').attr('id');
		var popItemId 		= popObj.parent().parent().find('.cls_td_item').attr('id');
		
		var found 			= false;
		
		$('#frmBudgetTransfer #tblMain >tbody >tr').each(function(){
			
			var mainObj		= $(this);
			var mainSubId   = mainObj.find('.cls_td_subCategory').attr('id');
			var mainItemId	= mainObj.find('.cls_td_item').attr('id');
			
			if(popSubId == mainSubId && popItemId == mainItemId)
				found = true;
		});
	
		if(popObj.parent().parent().attr('id')=='U'){
			disableA	= 'disabled=disabled';
			disableU	= '';
			validateA	= "validate[custom[number],min[0]]";
			validateU	= "validate[required,custom[number],min[1]]";
		}
		else{
			disableU	= 'disabled=disabled';
			disableA	= '';
			validateU	= "validate[custom[number],min[0]]";
			validateA	= "validate[required,custom[number],min[1]]";
		}
	
		if(!found)
		{					
			var x = "<tr id=\""+$(this).val()+"\">"+
			"<td align=\"center\" style=\"text-align:center\"><img class=\"mouseover removeRow\" src=\"images/del.png\" /></td>"+
			"<td class=\"cls_td_subCategory\" id="+popSubId+">"+popObj.parent().parent().find('.cls_td_subCategory').html()+"</td>"+
			"<td class=\"cls_td_item\" id="+popItemId+">"+popObj.parent().parent().find('.cls_td_item').html()+"</td>"+
			"<td style=\"text-align:center\">"+popObj.parent().parent().find('.cls_td_unit').html()+"</td>"+
			"<td align=\"right\" id=\""+popObj.parent().parent().find('.cls_td_currentAmount').attr('id')+"\">"+popObj.parent().parent().find('.cls_td_currentAmount').html()+"</td>"+
			"<td align=\"right\" id=\""+popObj.parent().parent().find('.cls_td_currentUnit').attr('id')+"\">"+popObj.parent().parent().find('.cls_td_currentUnit').html()+"</td>"+
			"<td align=\"right\" id=\""+popObj.parent().parent().find('.cls_td_nextAmount').attr('id')+"\">"+popObj.parent().parent().find('.cls_td_nextAmount').html()+"</td>"+
			"<td align=\"right\" id=\""+popObj.parent().parent().find('.cls_td_nextUnit').attr('id')+"\">"+popObj.parent().parent().find('.cls_td_nextUnit').html()+"</td>"+
			"<td align=\"center\" class=\"cls_td_enteredAmount\"><input type=\"text\" style=\"width:80px;text-align:right\" class=\"clsAmount "+validateA+"\" value=\"0\" id=\""+loop+++"\" "+disableA+"/></td>"+
			"<td align=\"center\" class=\"cls_td_enteredUnit\"><input type=\"text\" style=\"width:80px;text-align:right\" class=\"clsUnit "+validateU+"\" value=\"0\" id=\""+loop+++"\" "+disableU+"/></td>"+
			"<td align=\"center\" class=\"cls_td_type\"><select id=\"cboType\" name=\"cboType\" style=\"width:100px\" class=\"clsType\"><option value=\"null\"></option><option value=\"0\">Transfer Budget</option><option value=\"1\">Additional Budget</option></select></td></tr>";
			
			$("#frmBudgetTransfer #tblMain > tbody:last").append(x); 
		}
	
	});
	disablePopup();
	
}
function searchData()
{
	$("#frmPopUp #tblItemsPopup tr:gt(1)").remove();
	
	var mainCategory 	= $('#frmPopUp #cboMainCategory').val();
	var subCategory 	= $('#frmPopUp #cboSubCategory').val();
	var description 	= $('#frmPopUp #txtItem').val();
	var department		= $('#frmBudgetTransfer #cboDepartment').val();
							
	var url 	= basePath+"budgetTransfer_db.php?RequestType=loadItems";
	var httpobj = $.ajax({
							url:url,
							dataType:'json',
							type:'POST',
							data:"mainCategory="+mainCategory+"&subCategory="+subCategory+"&department="+department+"&description="+description, 
							async:false,
							success:function(json){
							
							$('#frmPopUp #tblItemsPopup #tblContent').html(json.gridDetail);	
						}
	});
}
function loadPopUp()
{
	if(!$('#frmBudgetTransfer').validationEngine('validate'))
	{
		return false;
	}
	
	popupWindow3('1');
	$('#popupContact1').load(basePath+'subCategoryPopUp.php?Dept='+$('#frmBudgetTransfer #cboDepartment').val(),function(){
		
		$('#frmPopUp #butClose').die('click').live('click',disablePopup);		
	});
}
function loadSubCategory()
{
	var mainCategory 	= $('#frmPopUp #cboMainCategory').val();
	var url 			= basePath+"budgetTransfer_db.php?RequestType=loadSubCategory&mainCategory="+mainCategory;
	var httpobj 		= $.ajax({url:url,type:'POST',async:false})
	
	$('#frmPopUp #cboSubCategory').html(httpobj.responseText);
}
function setButtonEvents()
{
	$("#btnAdd").die('click').live('click',function(){
			setToAddNew(this);
	});
}

function save()
{
	showWaiting();
	if(!save_validation()){
		setTimeout("autoHideFormValidations('#frmBudgetTransfer')",5000);
		setTimeout("autoHideFormValidations('#frmBudgetTransfer #butSave')",5000);
		hideWaiting();
		return;
	}
	
	var	headerArray  = "{";
		headerArray += '"TransferNo":"'+$('#frmBudgetTransfer #txtTransfetNo').val()+'",' ;
		headerArray += '"TransferYear":"'+$('#frmBudgetTransfer #txtTransfetYear').val()+'",' ;		
		headerArray += '"Dept":"'+$('#frmBudgetTransfer #cboDepartment').val()+'",' ;
		headerArray += '"Current":"'+$('#frmBudgetTransfer .cls_th_current').attr('id')+'",';
		headerArray += '"Next":"'+$('#frmBudgetTransfer .cls_th_next').attr('id')+'"';
		headerArray +=  '}';
	
	var detailArray	 = "[";
	$('#frmBudgetTransfer #tblMain tbody tr').each(function(){
		detailArray += "{";
		detailArray += '"SubCatId":"'+$(this).find('.cls_td_subCategory').attr('id')+'",' ;
		detailArray += '"ItemId":"'+$(this).find('.cls_td_item').attr('id')+'",';
		detailArray += '"EnteredAmount":"'+$(this).find('.cls_td_enteredAmount').children().val()+'",';
		detailArray += '"EnteredUnit":"'+$(this).find('.cls_td_enteredUnit').children().val()+'",';
		detailArray += '"Type":"'+$(this).find('.cls_td_type').children().val()+'"';
		detailArray +=  '},';
	});
	detailArray 	 = detailArray.substr(0,detailArray.length-1);
	detailArray 	+= " ]";
	
	var url 	= basePath+"budgetTransfer_db.php?RequestType=URLSave";
	var data 	= "HeaderArray="+headerArray;
		data   += "&DetailArray="+detailArray;
		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	type: "POST",
	async:false,
	success:function(json)
		{
			if(json.type=='pass')
			{
				hideWaiting();
				$('#frmBudgetTransfer #butSave').validationEngine('showPrompt',json.msg,json.type);
				$('#frmBudgetTransfer #txtTransfetNo').val(json.no);
				$('#frmBudgetTransfer #txtTransfetYear').val(json.year);
				$('#frmBudgetTransfer #butReport').show();
				$('#frmBudgetTransfer #butApprove').show();
				var t = setTimeout("autoHideFormValidations('#frmBudgetTransfer #butSave')",4000);
			}
			else
			{
				hideWaiting();
				$('#frmBudgetTransfer #butReport').hide();
				$('#frmBudgetTransfer #butSave').validationEngine('showPrompt',json.msg,json.type);
				var t = setTimeout("autoHideFormValidations('#frmBudgetTransfer #butSave')",5000);
			}
		},
	error:function(xhr,status)
		{
			hideWaiting();
			$('#frmBudgetTransfer #butSave').validationEngine('showPrompt',errormsg(xhr.status),'fail');
			var t = setTimeout("autoHideFormValidations('#frmBudgetTransfer #butSave')",4000);
		}
	});	
	
	hideWaiting();
}

function save_validation()
{
	if(!$('#frmBudgetTransfer').validationEngine('validate')){
		return false;
	}
	
	if($('#frmBudgetTransfer #tblMain tbody tr').length=='0')
	{
		$('#frmBudgetTransfer #butSave').validationEngine('showPrompt','No details available to save.','fail');
		return false;
	}
	return true;
}

function autoHideFormValidations(obj)
{
	$(obj).validationEngine('hide');
}

function setToAddNew(obj)
{	
			
}

function CheckAll_Approve(obj,obj1)
{
	$('#frmPopUp .chkSubCat').each(function(){
			if(!$(this).is(':disabled')){
				if(obj)
					$(this).attr('checked','checked');
				else
					$(this).removeAttr('checked');
			}
		});
}
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
function clearForm()
{
	$('#frmBudgetTransfer')[0].reset();
	$("#frmBudgetTransfer #tblMain tr:gt(2)").remove();
	$('#frmBudgetTransfer #txtTransfetNo').val('');
	$('#frmBudgetTransfer #txtTransfetYear').val('');
	$('#frmBudgetTransfer #cboDepartment').val('');
}

function removeRow()
{
	$(this).parent().parent().remove();
}

function report()
{
	if($('#frmBudgetTransfer #txtTransfetNo').val()=='')
	{
		$('#frmBudgetTransfer #butReport').validationEngine('showPrompt','No saved Budget Transfer to view report.','fail');
		return;
	}
	var url  = "?q="+reportId+"&serialNo="+$('#frmBudgetTransfer #txtTransfetNo').val()+"&serialYear="+$('#frmBudgetTransfer #txtTransfetYear').val();
	window.open(url,'rpt_budgetTransfer.php');
}
function approve()
{
	var url  = "?q="+reportId+"&serialNo="+$('#frmBudgetTransfer #txtTransfetNo').val()+"&serialYear="+$('#frmBudgetTransfer #txtTransfetYear').val()+'&Mode=Confirm';
	window.open(url,'rpt_budgetTransfer.php');
}
function hideReportError()
{
	$('#frmBudgetTransfer #butReport').validationEngine('hide')	;
}
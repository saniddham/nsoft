// JavaScript Document
var basePath	= "presentation/finance_new/budget/transfering/";
$(document).ready(function() {
	
	$("#frmRptBudgetTransfer").validationEngine();
	$('#frmRptBudgetTransfer #butRptConfirm').die('click').live('click',urlApprove);
	$('#frmRptBudgetTransfer #butRptReject').die('click').live('click',urlReject);
});
function urlApprove()
{
	if($('#frmRptBudgetTransfer').validationEngine('validate'))
	{
		var arrDetails	= "";
		$('#frmRptBudgetTransfer .clsType').each(function(){
			
			var typeId		= $(this).val();
			var subCatId	= $(this).parent().parent().find('.clsSubCat').attr('id');
			var itemId		= $(this).parent().parent().find('.clsItem').attr('id');
		
			arrDetails += "{";
			arrDetails += '"typeId":"'+ typeId +'",' ;
			arrDetails += '"subCatId":"'+ subCatId +'",' ;
			arrDetails += '"itemId":"'+ itemId +'"' ;
			arrDetails += "},";
			
		});
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		var arrDetails	= '['+arrDetails+']';
		
		var val = $.prompt('Are you sure you want to approve this Budget Transfer ?',{
					buttons: { Ok: true, Cancel: false },
					callback: function(v,m,f){
					if(v)
					{
						showWaiting();
						var url = basePath+"budgetTransfer_db.php"+window.location.search+'&RequestType=approve';
						var obj = $.ajax({
							url:url,
							type:'post',
							dataType: "json",  
							data:'&arrDetails='+arrDetails,
							async:false,
							
							success:function(json){
									$('#frmRptBudgetTransfer #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
									if(json.type=='pass')
									{
										var t=setTimeout("alertx()",1000);
										window.location.href = window.location.href;
										window.opener.location.reload();//reload listing page
										return;
									}
								},
							error:function(xhr,status){
									
									$('#frmRptBudgetTransfer #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
									var t=setTimeout("alertx()",3000);
									return;
								}		
							});
						hideWaiting();
						}
					
				}});
	}
}
function urlReject()
{
	var val = $.prompt('Are you sure you want to reject this Budget Trasfer ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = basePath+"budgetTransfer_db.php"+window.location.search+'&RequestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptBudgetTransfer #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx1()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptBudgetTransfer #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx1()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function alertx()
{
	$('#frmRptBudgetTransfer #butRptConfirm').validationEngine('hide')	;
}
function alertx1()
{
	$('#frmRptBudgetTransfer #butRptReject').validationEngine('hide')	;
}
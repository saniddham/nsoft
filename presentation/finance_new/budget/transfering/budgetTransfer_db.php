<?php
session_start();
ini_set('display_errors',0);
$backwardseperator 		= "../../../../";
$mainPath 				= $_SESSION['mainPath'];
$userId 				= $_SESSION['userId'];
$location 				= $_SESSION['CompanyID'];
$companyId 				= $_SESSION['headCompanyId'];


include_once "{$backwardseperator}dataAccess/Connector.php";
require_once "../../../../class/cls_commonFunctions_get.php";
require_once "../../../../class/cls_commonErrorHandeling_get.php";
require_once "../../../../class/finance/budget/cls_common_function_budget_get.php";
require_once "../../../../class/finance/budget/tranferring/cls_budgetTransfer_get.php";
require_once "../../../../class/finance/budget/tranferring/cls_budgetTransfer_set.php";
require_once "../../../../class/finance/budget/entering/cls_budget_set.php";

$obj_common				= new cls_commonFunctions_get($db);
$obj_commonErr			= new cls_commonErrorHandeling_get($db);

$cls_transfer_get		= new cls_budget_transfer_get($db);
$cls_transfer_set		= new cls_budget_transfer_set($db);
$obj_comm_budget_get	= new cls_common_function_budget_get($db);
$obj_budgetPlan_set		= new cls_budget_set($db);

$requestType			= $_REQUEST['RequestType'];
$programCode			= 'P0842';

$currentMonth	= date('Y-M');
$nextMonth		= date('Y-M',strtotime("+1 months", strtotime(date('Y-m-d'))));
$result			= $obj_comm_budget_get->getFinancePeriod($companyId,date('Y-m-d'),'RunQuery');
$financeId		= $result["FINANCE_ID"];

if($requestType=='URLSave')
{
	$db->begin();
	$headerArray	= json_decode($_REQUEST['HeaderArray'],true);
	$detailArray	= json_decode($_REQUEST['DetailArray'],true);
	$transferNo		= $headerArray['TransferNo'];
	$transferYear	= $headerArray['TransferYear'];
	$result			= $obj_comm_budget_get->getFinancePeriod($companyId,$headerArray["Current"].'-01','RunQuery2');
	$financeId		= $result["FINANCE_ID"];
	$from			= explode('-',$headerArray["Next"]);	$fromYear = $from[0];	$fromMonth	= $from[1];
	$to				= explode('-',$headerArray["Current"]);	$toYear   = $to[0];		$toMonth 	= $to[1];	
	$savedStatus	= true;
	$message		= '';	
	
	if($transferNo=="" && $transferYear == "")
	{
		//BEGIN - SYSTEM NO GENERATION {
		$result = $obj_common->GetSystemMaxNo('BUDGET_TRANSFER_NO',$location);
		if($result['rollBackFlag']==0)
		{
			$transferNo 	= $result['max_no'];
			$transferYear 	= date('Y');
		}
		else
		{
			$savedStatus	= false;
			$message		= $result['msg'];
			$errorSql		= $result['q'];
		}
		//END 	- SYSTEM NO GENERATION }
		
		//BEGIN - GET APPROVE LEVELS {
		$result = $obj_common->getApproveLevels_new($programCode);
		if($result['type'] && $savedStatus)
		{
			$approveLevel	= $result['ApproLevel'];
			$status			= $approveLevel+1;
		}
		else
		{
			$savedStatus	= false;
			$message		= $result['msg'];
		}
		//END 	- GET APPROVE LEVELS }
		
		//BEGIN - HEADER SAVING PART {
		$result  = $cls_transfer_set->insertHeader('RunQuery2',$transferNo,$transferYear,$location,$headerArray["Dept"],$financeId,$fromYear,$fromMonth,$toYear,$toMonth,$status,$approveLevel,$userId);
		if(!$result['type'] && $savedStatus)
		{
			$savedStatus	= false;
			$message		= $result['msg'];
			$errorSql		= $result['sql'];
		}
		else
			$message		= $result['msg'];
		//END 	- HEADER SAVING PART }
	}
	else
	{
		//BEGIN - HEADER UPDATION PART {
			$result  = $cls_transfer_set->updateHeader('RunQuery2',$transferNo,$transferYear,$location,$headerArray["Dept"],$financeId,$userId);
			if(!$result['type'] && $savedStatus)
			{
				$savedStatus	= false;
				$message		= $result['msg'];
				$errorSql		= $result['sql'];
			}
			else
				$message		= $result['msg'];
		//END 	- HEADER UPDATION PART }
			$resultUMArr		= updateMaxStatus($transferNo,$transferYear);
			if($resultUMArr['savedStatus']=='fail' && $savedStatus)
			{
				$savedStatus	= false;
				$message 		= $resultUMArr['msg'];
				$errorSql		= $resultUMArr['sql'];
			}
		//BEGIN - DELETE TRANSFER DETAILS {
			$result  = $cls_transfer_set->deleteDetail('RunQuery2',$transferNo,$transferYear);
			if(!$result['type'] && $savedStatus)
			{
				$savedStatus	= false;
				$message		= $result['msg'];
				$errorSql		= $result['sql'];
			}
		//END	- DELETE TRANSFER DETAILS }
	}
	
	//BEGIN - DETAILS SAVING PART {
	foreach($detailArray as $row)
	{		
		$itemId	= $row["ItemId"]==""?0:$row["ItemId"];
		
		//BEGIN - VALIDATION {
		/*$result = $obj_comm_budget_get->load_budget_balance($location,$headerArray["Dept"],$financeId,$row["SubCatId"],$itemId,$fromMonth,'RunQuery2');
		if($result['balAmount']<$row["EnteredAmount"])
		{
			$savedStatus	= false;
			$message		= "Entered 'Amount' cannot exceed balance 'Amount'.";
		}
		if($result['balUnits']<$row["EnteredUnit"])
		{
			$savedStatus	= false;
			$message		= "Entered 'Unit Amount' cannot exceed balance 'Unit Amount'.";
		}*/
		//END 	- VALIDATION }		
		
		$result = $cls_transfer_set->insertDetail('RunQuery2',$transferNo,$transferYear,$row["SubCatId"],$itemId,$row["EnteredAmount"],$row["EnteredUnit"],$row["Type"]);
		if(!$result['type'] && $savedStatus)
		{
			$savedStatus	= false;
			$message		= $result['msg'];
			$errorSql		= $result['sql'];
		}
	}
	//END 	- DETAILS SAVING PART }
	$sysNoArr					= $obj_common->validateDuplicateSerialNoWithSysNo($transferNo,'BUDGET_TRANSFER_NO',$location);
	if($sysNoArr['type'] == 'fail' && $savedStatus)
	{
		$savedStatus	= false;
		$message		= $sysNoArr['msg'];
	}
	if($savedStatus)
	{
		$db->commit();
		$responce['type']	= 'pass';
		$responce['msg']	= $message;
		$responce['no']		= $transferNo;
		$responce['year']	= $transferYear;
	}
	else
	{
		$db->rollback();
		$responce['type']	= 'fail';
		$responce['msg']	= $message;
		$responce['sql']	= $errorSql;
	}
	echo json_encode($responce);
}
else if($requestType=='approve')
{
	$savedStatus	= true;
	$message		= '';
	$errorSql		= '';
	
	$arrDetails 	= json_decode($_REQUEST['arrDetails'],true);
	$transferNo		= $_REQUEST['serialNo'];
	$transferYear	= $_REQUEST['serialYear'];
	
	$db->begin();
	
	$validateArr		= validateBeforeApprove($transferNo,$transferYear,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$message 		= $validateArr["msg"];
	}
	$header_arr			= $cls_transfer_get->get_header($transferNo,$transferYear,'RunQuery2');
	if($header_arr['STATUS']>$header_arr['LEVELS'])
	{
		foreach($arrDetails as $array_loop)
		{
			$typeId			= $array_loop['typeId'];
			$subCatId		= $array_loop['subCatId'];
			$itemId			= $array_loop['itemId'];
			
			if($typeId=='')
			{
				$savedStatus	= false;
				$message 		= 'please select a Type.';
			}
			
			$resultDArr			= $cls_transfer_set->updateDetails($transferNo,$transferYear,$typeId,$subCatId,$itemId);
			if($resultDArr['savedStatus']=='fail' && $savedStatus)
			{
				$savedStatus	= false;
				$message 		= $resultDArr['savedMassege'];
				$errorSql		= $resultDArr['error_sql'];	
			}
		}	
	}
	$resultUHSArr		= updateHeaderStatus($transferNo,$transferYear,'');
	if($resultUHSArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$message 		= $resultUHSArr["savedMassege"];
		$errorSql		= $resultUHSArr['error_sql'];	
	}
	$resultAPArr		= approvedData($transferNo,$transferYear,$userId);
	if($resultAPArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$message 		= $resultAPArr["savedMassege"];
		$errorSql		= $resultAPArr['error_sql'];	
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Approved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $message;
		$response['sql']		= $errorSql;
	}
	echo json_encode($response);	
}
else if($requestType=='reject')
{
	$savedStatus	= true;
	$message		= '';
	$errorSql		= '';
	
	$transferNo		= $_REQUEST['serialNo'];
	$transferYear	= $_REQUEST['serialYear'];
	
	$db->begin();
	
	$validateArr		= validateBeforeReject($transferNo,$transferYear,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$message 		= $validateArr["msg"];
	}
	$resultUHSArr		= updateHeaderStatus($transferNo,$transferYear,'0');
	if($resultUHSArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$message 		= $resultUHSArr["savedMassege"];
		$errorSql		= $resultUHSArr['error_sql'];	
	}
	$resultAPArr		= $cls_transfer_set->approved_by_insert($transferNo,$transferYear,$userId,0);
	if($resultAPArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$message 		= $resultAPArr["savedMassege"];
		$errorSql		= $resultAPArr['error_sql'];	
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Rejected Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $message;
		$response['sql']		= $errorSql;
	}
	echo json_encode($response);
}
else if($requestType=='loadSubCategory')
{
	$mainCategory  = $_REQUEST['mainCategory'];
	$subCat		   = $obj_comm_budget_get->loadSubCategory($mainCategory,'RunQuery');
	$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($subCat))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		//echo json_encode($response); 	
}
else if($requestType=='loadItems')
{
	$mainCategory  	= $_REQUEST['mainCategory'];
	$subCategory  	= $_REQUEST['subCategory'];
	$description  	= $_REQUEST['description'];
	$department	  	= $_REQUEST['department'];	
	//echo $subCategory;
	$subcat 		= $obj_comm_budget_get->loadBudgetCategoryList($companyId,$location,$mainCategory,$subCategory,$description);
	$content 		= '';

while($row=mysqli_fetch_array($subcat))
{
	$resultArr	= $obj_comm_budget_get->load_budget_balance($location,$department,$financeId,$row['SUB_CATEGORY_ID'],$row['ITEM_ID'],date('m',strtotime($currentMonth)),'RunQuery');
		$resultArrnw	= $obj_comm_budget_get->load_budget_balance($locationId,$department,$financeId,$row['SUB_CATEGORY_ID'],$row['ITEM_ID'],date('m',strtotime($nextMonth)),'RunQuery');  

		$subCatName 	= $row['SUB_CATEGORY_NAME'];
		$itemID 		= $row['ITEM_ID'];
		$itemName 		= $row['ITEM_NAME'];
		$subCatID 		= $row['SUB_CATEGORY_ID'];
		$uom 			= $row['UNIT'];
		$type 			= $row['BUDGET_TYPE'];
		$currentBalAmt 	= $resultArr["balAmount"];
		$currentBalUnit = $resultArr["balUnits"];
		$nextBalAmt 	= $resultArrnw["balAmount"];
		$nextBalUnit 	= $resultArrnw["balUnits"];
		
		$content .= "<tr class=\"normalfnt\"><td align=\"center\" bgcolor=\"#FFFFFF\" id=\"\"><input type=\"checkbox\" class=\"chkSubCat\" id=\"chkSubCat\" /></td>";
		$content .="<td align=\"center\" bgcolor=\"#FFFFFF\" id=\"".$subCatID."\" class=\"cls_td_subCategory\">".$subCatName."</td>";
									
		$content .="<td align=\"center\" bgcolor=\"#FFFFFF\" id=\"".$itemID."\" class=\"cls_td_item\">".$itemName."</td>";
		$content .="<td align=\"center\" bgcolor=\"#FFFFFF\" id=\"".$uom."\" class=\"cls_td_unit\">".$uom."</td>";
		if($type == 'U')
		{
			$content .="<td align=\"right\" bgcolor=\"#FFFFFF\" id=\"".$currentBalAmt."\" class=\"cls_td_currentAmount\">&nbsp;</td>";
			$content .="<td align=\"right\" bgcolor=\"#FFFFFF\" id=\"".$currentBalUnit."\" class=\"cls_td_currentUnit\">".$currentBalUnit."</td>";
			$content .="<td align=\"right\" bgcolor=\"#FFFFFF\" id=\"".$nextBalAmt."\" class=\"cls_td_nextAmount\" >&nbsp;</td>";
			$content .="<td align=\"right\" bgcolor=\"#FFFFFF\" id=\"".$nextBalUnit."\" class=\"cls_td_nextUnit\">".$nextBalUnit."</td></tr>";
		}
		else
		{
		$content .="<td align=\"right\" bgcolor=\"#FFFFFF\" id=\"".$currentBalAmt."\" class=\"cls_td_currentAmount\">".$currentBalAmt."</td>";
			$content .="<td align=\"right\" bgcolor=\"#FFFFFF\" id=\"".$currentBalUnit."\" class=\"cls_td_currentUnit\">&nbsp;</td>";
			$content .="<td align=\"right\" bgcolor=\"#FFFFFF\" id=\"".$nextBalAmt."\" class=\"cls_td_nextAmount\" >".$nextBalAmt."</td>";
			$content .="<td align=\"right\" bgcolor=\"#FFFFFF\" id=\"".$nextBalUnit."\" class=\"cls_td_nextUnit\">&nbsp;</td></tr>";
		}
}
	$response['gridDetail'] = $content;
	echo json_encode($response);
	
}
function validateBeforeApprove($transferNo,$transferYear,$programCode,$userId)
{
	global $obj_commonErr;
	global $cls_transfer_get;
	
	$header_arr		= $cls_transfer_get->get_header($transferNo,$transferYear,'RunQuery2');
	$validateArr 	= $obj_commonErr->get_permision_withApproval_confirm($header_arr['STATUS'],$header_arr['LEVELS'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}
function updateHeaderStatus($transferNo,$transferYear,$status)
{
	global $cls_transfer_set;
	
	if($status=='') 
		$para 	= 'STATUS-1';
	else
		$para 	= $status;
		
	$resultArr	= $cls_transfer_set->updateHeaderStatus($transferNo,$transferYear,$para);
	
	return  $resultArr;
}
function approvedData($transferNo,$transferYear,$userId)
{
	global $cls_transfer_get;
	global $cls_transfer_set;
	global $obj_budgetPlan_set;
	$chkStatus		= true;
	
	$header_arr		= $cls_transfer_get->get_header($transferNo,$transferYear,'RunQuery2');
	$approval		= $header_arr['LEVELS']+1-$header_arr['STATUS'];
	
	$resultArr		= $cls_transfer_set->approved_by_insert($transferNo,$transferYear,$userId,$approval);
	if($resultArr['savedStatus']=='fail' && $chkStatus)
	{
		$chkStatus				= false;
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $resultArr['savedMassege'];
		$data['error_sql']		= $resultArr['error_sql'];
	}
	
	$resultDetail			= $cls_transfer_get->get_details_results($transferNo,$transferYear,'RunQuery2');
	while($row = mysqli_fetch_array($resultDetail))
	{
		if($row['TYPE']==0)
		{
			$validateBalAmountArr	= validateBalAmount($header_arr['LOCATION_ID'],$header_arr['DEPARTMENT_ID'],$header_arr['FINANCE_YEAR_ID'],$row['SUB_CATEGORY_ID'],$row['ITEM_ID'],$header_arr['FROM_MONTH'],$row['AMOUNT'],$row['UNITS'],$row['ITEM_NAME'],$row['SUB_CATEGORY_NAME']);
			if($validateBalAmountArr['savedStatus']=='fail' && $chkStatus)
			{
				$chkStatus				= false;
				$data['savedStatus']	= 'fail';
				$data['savedMassege']	= $validateBalAmountArr['savedMassege'];
			}
		}
	}
	
	if($header_arr['STATUS']==1)
	{
		$resultDetail			= $cls_transfer_get->get_details_results($transferNo,$transferYear,'RunQuery2');
		while($row = mysqli_fetch_array($resultDetail))
		{
			
			if($row['AMOUNT']>0)
				$fieldType = 'A';
			
			if($row['UNITS']>0)
				$fieldType = 'U';
					
			if($row['TYPE']==0)
			{
				$resultUBPFromArr 	= $obj_budgetPlan_set->updatePlanDetails_units_or_amount($header_arr['LOCATION_ID'],$header_arr['DEPARTMENT_ID'],$header_arr['FINANCE_YEAR_ID'],$row['SUB_CATEGORY_ID'],$row['ITEM_ID'],$header_arr['FROM_MONTH'],($row['AMOUNT']*-1),($row['UNITS']*-1),'TRANSFER',$fieldType,'RunQuery2');
				if($resultUBPFromArr['savedStatus']=='fail' && $chkStatus)
				{
					$chkStatus				= false;
					$data['savedStatus']	= 'fail';
					$data['savedMassege']	= $resultUBPFromArr['savedMassege'];
					$data['error_sql']		= $resultUBPFromArr['error_sql'];
				}
				
				$resultUBPToArr 	= $obj_budgetPlan_set->updatePlanDetails_units_or_amount($header_arr['LOCATION_ID'],$header_arr['DEPARTMENT_ID'],$header_arr['FINANCE_YEAR_ID'],$row['SUB_CATEGORY_ID'],$row['ITEM_ID'],$header_arr['TO_MONTH'],$row['AMOUNT'],$row['UNITS'],'TRANSFER',$fieldType,'RunQuery2');
				if($resultUBPToArr['savedStatus']=='fail' && $chkStatus)
				{
					$chkStatus				= false;
					$data['savedStatus']	= 'fail';
					$data['savedMassege']	= $resultUBPFromArr['savedMassege'];
					$data['error_sql']		= $resultUBPFromArr['error_sql'];
				}
			}
			else
			{
				$resultUBPArr 		= $obj_budgetPlan_set->updatePlanDetails_units_or_amount($header_arr['LOCATION_ID'],$header_arr['DEPARTMENT_ID'],$header_arr['FINANCE_YEAR_ID'],$row['SUB_CATEGORY_ID'],$row['ITEM_ID'],$header_arr['TO_MONTH'],$row['AMOUNT'],$row['UNITS'],'ADDITIONAL',$fieldType,'RunQuery2');
				if($resultUBPrr['savedStatus']=='fail' && $chkStatus)
				{
					$chkStatus				= false;
					$data['savedStatus']	= 'fail';
					$data['savedMassege']	= $resultUBPrr['savedMassege'];
					$data['error_sql']		= $resultUBPrr['error_sql'];
				}
			}
		}
	}
	
	return  $data;
}
function validateBeforeReject($transferNo,$transferYear,$programCode,$userId)
{
	global $obj_commonErr;
	global $cls_transfer_get;
	
	$header_arr		= $cls_transfer_get->get_header($transferNo,$transferYear,'RunQuery2');
	$validateArr 	= $obj_commonErr->get_permision_withApproval_reject($header_arr['STATUS'],$header_arr['LEVELS'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}
function validateBalAmount($location,$department,$financeId,$subCatId,$itemId,$fromMonth,$amount,$unitAmt,$item,$subcat)
{
	global $obj_comm_budget_get;
	$savedStatus	= true;
	
	$result 	= $obj_comm_budget_get->load_budget_balance($location,$department,$financeId,$subCatId,$itemId,$fromMonth,'RunQuery2');
	if($result['balAmount']<$amount && $savedStatus)
	{
		$savedStatus	= false;
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= "System not allow to exceed budget balance. <br> Sub Category : ".$subcat." <br> Item : ".$item." <br> Transfer Amount : ".$amount."<br> Budget Balance : ".$result['balAmount'];
	}
	if($result['balUnits']<$unitAmt && $savedStatus)
	{
		
		$savedStatus	= false;
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= "System not allow to exceed budget balance. <br> Sub Category : ".$subcat." <br> Item : ".$item." <br> Transfer Units : ".$unitAmt."<br> Budget Balance : ".$result['balUnits'];
	}
	return $data;
}
function updateMaxStatus($transferNo,$transferYear)
{
	global $cls_transfer_get;
	global $cls_transfer_set;
	
	$maxStatus	= $cls_transfer_get->getMaxStatus($transferNo,$transferYear,'RunQuery2');
	$resultArr	= $cls_transfer_set->updateApproveByStatus($transferNo,$transferYear,$maxStatus);
	
	return $resultArr;
}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId				= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$userId					= $_SESSION["userId"];

include "class/cls_commonFunctions_get.php";
require_once "class/finance/budget/tranferring/cls_budgetTransfer_get.php";
include "class/finance/budget/cls_common_function_budget_get.php";
include_once "class/cls_commonErrorHandeling_get.php";
//include 	 "include/javascript.html";

$obj_common				= new cls_commonFunctions_get($db);
$obj_commonErrHandle	= new cls_commonErrorHandeling_get($db);
$obj_common_function_budget_get	= new cls_common_function_budget_get($db);
$obj_transferGet		= new cls_budget_transfer_get($db);

$currentMonth			= date('Y-M');
$nextMonth				= date('Y-M',strtotime("+1 months", strtotime(date('Y-m-d'))));

$programCode			= 'P0842';
$serialNo				= (!isset($_REQUEST['serialNo'])?'':$_REQUEST['serialNo']);
$serialYear				= (!isset($_REQUEST['serialYear'])?'':$_REQUEST['serialYear']);

$dept					= $obj_common->loadDepartment('RunQuery');

$header					= $obj_transferGet->get_header($serialNo,$serialYear,'RunQuery');
$detail					= $obj_transferGet->get_details_results($serialNo,$serialYear,'RunQuery');
$dt						= $header['CREATED_DATE'];
$d						= strtotime($dt);

$intStatus				= $header['STATUS'];
$levels					= $header['LEVELS'];

$permision_arr			= $obj_commonErrHandle->get_permision_withApproval_save($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_save			= $permision_arr['permision'];

$permision_arr			= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_confirm		= $permision_arr['permision'];

?>
<title>Budget Transferring</title>

<!--<script type="text/javascript" src="presentation/finance_new/budget/transfering/budgetTransfer_js.js"></script>-->

<form id="frmBudgetTransfer" name="frmBudgetTransfer" method="post">
  <div align="center">
    <div class="trans_layoutL" style="width:890">
      <div class="trans_text" style="width:890">Budget Transfer Form</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        
          <td><table width="100%" height="31" border="0" align="center">
              <tr>
                <td class="normalfnt">Transfer No</td>
                <td><input type="text" name="txtTransfetNo" id="txtTransfetNo" style="width:100px" value="<?php echo $serialNo?>" disabled="disabled"/>
                  <input type="text" name="txtTransfetYear" id="txtTransfetYear" style="width:50px" value="<?php echo $serialYear ?>" disabled="disabled"/></td>
                <td class="normalfnt">Date</td>
                <td><input name="dtDate" type="text" value="<?php echo($header['CREATED_DATE']==''?date("Y-m-d"):date("Y-m-d",$d)); ?>" class="txtbox" id="dtDate" style="width:100px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled" />
                  <input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
              <tr>
                <td width="86" class="normalfnt">Department <span class="compulsoryRed">*</span></td>
                <td width="539"><select name="cboDepartment" id="cboDepartment" style="width:250px"  class="validate[required] txtText">
                    <option value=""></option>
                    <?php
					while($row=mysqli_fetch_array($dept))
					{
						if($header['DEPARTMENT_ID']==$row['intId'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
					?>
                  </select></td>
                <td width="54" class="normalfnt">&nbsp;</td>
                <td width="167">&nbsp;</td>
              </tr>
            </table></td>
        <tr>
          <td><table id="tblMain" width="100%" class="bordered">
              <thead>
                <tr>
                  <th colspan="11" style="text-align:right"><a class="button green small" id="btnAdd" name="btnAdd">Add Sub Category</a></th>
                </tr>
                <tr>
                  <th width="28" rowspan="2">Del</th>
                  <th width="129" rowspan="2">Sub Category</th>
                  <th width="194" rowspan="2">Item Description</th>
                  <th width="75" rowspan="2">Unit</th>
                  <th colspan="2" class="cls_th_current" id="<?php echo date('Y-m',strtotime($currentMonth))?>"><?php echo $currentMonth?></th>
                  <th colspan="2" class="cls_th_next" id="<?php echo date('Y-m',strtotime($nextMonth))?>"><?php echo $nextMonth?></th>
                  <th colspan="2">Amount</th>
                  <th>&nbsp;</th>
                </tr>
                <tr>
                  <th width="98" >Amount</th>
                  <th width="117">Units</th>
                  <th width="108">Amount</th>
                  <th width="116">Units</th>
                  <th width="74">Amount</th>
                  <th width="89">Units</th>
                  <th width="95">Type</th>
                </tr>
              </thead>
              <tbody>
                <?php
				$i	= 0;
				while($row	= mysqli_fetch_array($detail))
					{	
						if($row["BUDGET_TYPE"]=='U')
						{
							$disableA	= 'disabled=disabled';
							$disableU	= '';
							$validateA	= "validate[custom[number],min[0]]";
							$validateU	= "validate[required,custom[number],min[1]]";
						}
						else{
							$disableU	= 'disabled=disabled';
							$disableA	= '';
							$validateU	= "validate[custom[number],min[0]]";
							$validateA	= "validate[required,custom[number],min[1]]";
						}				
					?>
                <tr>
                  <td align="center" style="text-align:center"><img class="mouseover removeRow" src="images/del.png" /></td>
                  <td class="cls_td_subCategory" id="<?php echo $row['SUB_CATEGORY_ID'] ?>"><?php echo $row['SUB_CATEGORY_NAME'] ?></td>
                  <td class="cls_td_item" id="<?php echo $row['ITEM_ID'] ?>"><?php echo $row['ITEM_NAME'] ?></td>
                  <td style="text-align:center"><?php echo $row['UNIT']; ?></td>
                  <?php
						$toAmount	= $obj_common_function_budget_get->load_budget_balance($locationId,$department,$header['FINANCE_YEAR_ID'],$row['SUB_CATEGORY_ID'],$row['ITEM_ID'],$header['TO_MONTH'],'RunQuery')
						
						?>
                  <td align="right" id="<?php echo $toAmount['balAmount'] ?>"><?php echo $toAmount['balAmount'] ?></td>
                  <td align="right" id="<?php echo $toAmount['balUnits'] ?>"><?php echo $toAmount['balUnits'] ?></td>
                  <?php
                        $fromAmount	= $obj_common_function_budget_get->load_budget_balance($locationId,$department,$header['FINANCE_YEAR_ID'],$row['SUB_CATEGORY_ID'],$row['ITEM_ID'],$header['FROM_MONTH'],'RunQuery');
						?>
                  <td align="right" id="<?php echo $fromAmount['balAmount'] ?>"><?php echo $fromAmount['balAmount']?></td>
                  <td align="right" id="<?php echo $fromAmount['balUnits'] ?>"><?php echo $fromAmount['balUnits'] ?></td>
                  <td align="center" class="cls_td_enteredAmount"><input id="<?php echo $i?>" type="text" style="width:100px;text-align:right" class="clsAmount <?php echo $validateA?>" value="<?php echo $row['AMOUNT'] ?>" <?php echo $disableA?>/></td>
                  <td align="center" class="cls_td_enteredUnit"><input id="<?php echo $i?>" type="text" style="width:100px;text-align:right" class="clsAmount <?php echo $validateU?>" value="<?php echo $row['UNITS'] ?>" <?php echo $disableU?>/></td>
                  <td align="center" class="cls_td_type"><select name="cboType" id="cboType" class="clsType" style="width:100px">
                      <option  <?php if($row['TYPE']==''){ ?>selected="selected" <?php }?> value="null"></option>
                      <option  <?php if($row['TYPE']=='0'){ ?>selected="selected" <?php }?> value="0">Transfer</option>
                      <option  <?php if($row['TYPE']=='1'){ ?> selected="selected" <?php } ?> value="1">Additional</option>
                    </select></td>
                </tr>
                <?php
					}?>
              </tbody>
            </table></td>
        </tr>
        <tr>
          <td width="100%" align="center" bgcolor=""><p><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" name="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?> >Save</a><a class="button white medium" id="butApprove" name="butApprove" <?php if($permision_confirm!=1 || $serialNo==''){ ?>style="display:none" <?php } ?>>Approve</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></p></td>
        </tr>
      </table>
    </div>
  </div>
</form>
<div style="width:500px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>

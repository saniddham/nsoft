<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
	$companyId 			= $_SESSION['headCompanyId'];
	$locationId			= $_SESSION['CompanyID'];
	$session_userId		= $_SESSION["userId"];
	$intUser			= $_SESSION["userId"];

 	require_once "class/cls_commonFunctions_get.php";
	require_once "class/cls_commonErrorHandeling_get.php";
	require_once "class/finance/budget/tranferring/cls_budgetTransfer_get.php";
  	
	$obj_common			= new cls_commonFunctions_get($db);
	$obj_commonErr		= new cls_commonErrorHandeling_get($db);
	$obj_budg_transf_get= new cls_budget_transfer_get($db);
	
	$programName		='Transfer';
	$programCode		='P0842';
	 
	$serialNo			= $_REQUEST["serialNo"];
	$serialYear			= $_REQUEST["serialYear"];
	$approveMode		= (!isset($_REQUEST['approveMode'])?'':$_REQUEST['approveMode']);
	$mode				= (!isset($_REQUEST['Mode'])?'':$_REQUEST['Mode']);
	 
	$header_array 		= $obj_budg_transf_get->get_header($serialNo,$serialYear,'RunQuery');
 	$details_results	= $obj_budg_transf_get->get_details_results($serialNo,$serialYear,'RunQuery');
	
	$permition_arr		= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$intUser,$programCode,'RunQuery');
	$permision_save		= $permition_arr['permision'];
	
	$permition_arr		= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['LEVELS'],$intUser,$programCode,'RunQuery');
	$permision_reject	= $permition_arr['permision'];
	
	$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$intUser,$programCode,'RunQuery');
	$permision_confirm	= $permition_arr['permision'];
	
  ?>
<head>
<title>Budget Transfer/Addition Report</title>

<script type="text/javascript" src="presentation/finance_new/budget/transfering/rpt_budgetTransfer_js.js"></script>

 </head>
<body>
<style type="text/css">
.apDiv1 {
	position:absolute;
	left:380px;
	top:100px;
	width:auto;
	height:auto;
	z-index:0;
	opacity:0.1;
}
</style>
<?php
//$header_array["STATUS"]=3;

?>

 <form id="frmRptBudgetTransfer" name="frmRptBudgetTransfer" method="post">
  <table width="900" align="center">
    <tr>
      <td><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
      <td class="reportHeader" align="center">Budget Transfer | Additional Budget Report</td>
    </tr>
	<?php
		include "presentation/report_approve_status_and_buttons.php"
     ?>
<tr>
      <td><table width="100%" border="0" cellspacing="2" cellpadding="2" class="normalfnt">
                    	  <tr>
                    	    <td width="17%">Serial No</td>
                    	    <td width="2%">:</td>
                    	    <td width="31%"><?php echo $serialYear.'/'.$serialNo; ?></td>
                    	    <td width="15%">Date</td>
                    	    <td width="2%">:</td>
                    	    <td width="33%"><?php  echo $header_array['CREATED_DATE']?></td>
                  	    </tr>
                    	  <tr>
                    	    <td>Company</td>
                    	    <td>:</td>
                    	    <td><?php echo $header_array['COMPANY_NAME'];?>   
                  	        </td>
                    	    <td>Location</td>
                    	    <td>:</td>
                    	    <td><?php echo $header_array['LOCATION_NAME']; ?></td>
                  	    </tr>
                    	  <tr>
                    	    <td>Department</td>
                    	    <td>:</td>
                    	    <td><?php echo $header_array['DEPARTMENT_NAME']; ?></td>
                    	    <td>Financial Year</td>
                    	    <td>:</td>
                    	    <td><?php echo $header_array['CONCAT_PERIOD'];?>
                            </td>
                  	    </tr>
                        <tr>
                        	<td colspan="6">&nbsp;</td>
                        </tr>
                   	  </table></td>
    </tr>
    <tr>
    <td>
            <table width="100%" class="bordered">
              <tr>
                <th >Main Category</th>
                <th >Sub Category</th>
                <th >Item Description</th>
                 <th >Unit</th>
                <th >Type</th>
                <th >To Month</th>
                <th >From month</th>
                <th >Units</th>
                <th >Amount</th>
                </tr>
              <?php
				while($row=mysqli_fetch_array($details_results))
				{ 
					$subCatId	= $row['SUB_CATEGORY_ID'];
					$itemId		= $row['ITEM_ID'];
					$mainCat	=$row['MAIN_CATEGORY_NAME'];
					$subCat		=$row['SUB_CATEGORY_NAME'];
					$item		=$row['ITEM_NAME'];
					$type		=$row['TYPE_DESC'];
					$type_id	=$row['TYPE'];
					if($header_array['TO_YEAR'] !='' && $type_id ==0)
					$toMonths	=$header_array['TO_YEAR']."-".$obj_common->get_month_name($header_array['TO_MONTH'],'RunQuery');
					else
					$toMonths	='';
					if($header_array['FROM_YEAR'] !='' && $type_id ==0)
					$fromMonths	=$header_array['FROM_YEAR']."-".$obj_common->get_month_name($header_array['FROM_MONTH'],'RunQuery');
					else
					$fromMonths	='';
					$units		=$row['UNITS'];
					$amount		=$row['AMOUNT'];
        
                        ?>
              <tr class="normalfnt">
                <td align="center" bgcolor="#FFFFFF"> <?php echo $mainCat; ?></td>
                <td align="center" bgcolor="#FFFFFF" class="clsSubCat" id="<?php echo $subCatId; ?>"> <?php echo $subCat; ?></td>
                <td align="center" bgcolor="#FFFFFF" class="clsItem" id="<?php echo $itemId; ?>"> <?php echo $item; ?></td>
                <td align="center" bgcolor="#FFFFFF" class="clsUnit"> <?php echo $row['UNIT']; ?></td>
                <td align="center" bgcolor="#FFFFFF" style="width:150px">
                <?php
				if($type_id!='')
				{
					if($type_id==0)
						echo 'Transfer';
					else
						echo 'Additional';
				}
				else
				{
				?>
				<select name="cboTypes"  style="width:150px"  id="cboTypes" class="clsType validate[required]">
				<option value=""  <?php if($type_id==''){ ?>selected="selected" <?php }?> ></option>
				<option value="0" <?php if($type_id==0){ ?>selected="selected" <?php }?>>Transfer</option>
				<option value="1" <?php if($type_id==1){ ?>selected="selected" <?php }?>>Additional</option>
				</select>
				<?php
				}
				?></td>
                <td align="center" bgcolor="#FFFFFF"> <?php echo $toMonths; ?></td>
                <td align="center" bgcolor="#FFFFFF"> <?php echo $fromMonths; ?></td>
                <td bgcolor="#FFFFFF" align="right"> <?php echo $units; ?></td>
                <td bgcolor="#FFFFFF" align="right"> <?php echo $amount; ?></td>
                </tr>
              	<?php
				}
				?>
              </table>
         </td>
    </tr>
    <tr>
        <td colspan="6">&nbsp;</td>
    </tr>
    <tr>
    <td>
    <?php
			$creator		= $header_array['USER'];
			$createdDate	= $header_array['CREATED_DATE'];
 			$resultA 		= $obj_budg_transf_get->get_Report_approval_details_result($serialNo,$serialYear);
			include "presentation/report_approvedBy_details.php"
 	?>
    </td>
    </tr>
    <tr>
    <td align="center" class="normalfntMid"><strong>Printed Date : </strong><?php echo date('Y-m-d H:i:s');?></td>
    </tr>
  </table>
</form>
</body>
</html>
<?php
 ?>
<?php
session_start();
$backwardseperator = "../../../../";

include  "{$backwardseperator}dataAccess/Connector.php";
include  "../../../../class/finance/petty_cash/petty_cash_requisition/petty_cash_requisition_get.php";

$locationID						= $_REQUEST['locationID'];
$toDate							= $_REQUEST['toDate'];
$fromDate						= $_REQUEST['fromDate'];
$obj_petty_cash_requisition_get	= new cls_petty_cash_requisition_get($db);
//$invoiceResult					= $obj_petty_cash_requisition_get->getInvoice($toDate,$fromDate,$locationID,'RunQuery');

?>

<title>Invoice List</title>

<form id="frmPopUp" name="frmPopUp" method="post" action="">
  <div align="center">
    <div class="trans_layoutS" align="center" style="width:450px">
      <div class="trans_text"> Invoice List</div>
      <table width="100%" border="0"  >
      <tr class="normalfnt">
      <td><table width="100%" border="0"><tr class="normalfnt">
        	<td width="11%">From</td>
            <td width="40%"><input name="fromDate" type="text" value="" class="txtbox validate[required]" id="fromDate" style="width:100px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
            <td width="6%">To</td>
            <td width="34%"><input name="toDate" type="text" value="" class="txtbox validate[required]" id="toDate" style="width:100px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
            <td width="9%"><a class="button green small" id="butSearch" name="butSearch">Search</a></td>
        </tr></table>
      </td>
      </tr>
        <tr class="normalfnt">
        	<td><table width="100%" class="bordered" id="tblInvoicePopup">
                    <thead>
                      <tr>
                      <th width="9%"><input type="checkbox" id="chkCheckAll" name="chkCheckAll" /></th>
                      <th width="32%">Invoice No</th>
                      <th width="38%">Item</th>
                      <th width="21%">Amount</th>
                      <th width="21%" hidden="true">Path</th>
                      </tr>
                    </thead>
                    <tbody id="tblContent">
                    
                    </tbody>
              	</table></td>
            </tr>
            <tr>
          <td width="100%" align="center" ><a class="button white medium" id="butAdd" name="butAdd">Add</a><a  class="button white medium" id="butClosePop" name="butClosePop">Close</a></td>
        </tr>
        </table>
     </div>
   </div>  
</form>
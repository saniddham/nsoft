<?php
session_start();
ini_set('max_execution_time',60000);
ini_set('display_errors',0);
$company 				= $_SESSION['headCompanyId'];
//BEGIN - INCLUDE FILES {
//include_once "../../../../dataAccess/LoginDBManager.php";
include "../../../../dataAccess/Connector.php";
include_once "../../../../class/cls_commonFunctions_get.php";
require_once "../../../../class/finance/budget/entering/cls_budget_get.php";
//END 	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {
//$db								= new LoginDBManager;
$obj_commonFunctions_get		= new cls_commonFunctions_get($db);
$obj_budget_get					= new cls_budget_get($db);
//END 	- CREATE OBJECTS }

//BEGIN - SET PARAMETERS {
$department			= $_REQUEST["Department"];
$financeYear		= $_REQUEST["FinanceYear"];
$locationID			= $_REQUEST["Location"];

$financeMonth_arr	= $obj_budget_get->getFinanceMonths($financeYear);
$startMonth			= $financeMonth_arr['startMonth'];
$endMonth			= 12+$financeMonth_arr['endMonth'];	

if($financeMonth_arr['startYear']==$financeMonth_arr['endYear'])
	$endMonth		= $financeMonth_arr['endMonth'];
//END 	- SET PARAMETERS }

//BEGIN - MAIN QUERY {			
$result 			 = $obj_budget_get->getBudgetDetails($company,$locationID,$department);
$rowCount		 	 = mysqli_num_rows($result);
$arrayTotal["total"] = $rowCount;
//END 	- MAIN QUERY }	
	
//BEGIN - NORMAL ITEMS {
while($row1 = mysqli_fetch_array($result))
{
	//$data['subCategory1'] = $row1['subCatName'];
	$data['subCategory1'] 	= "<span id=".$row1['SUB_CATEGORY_ID'].">".$row1['SUB_CATEGORY_NAME']."</span>";
	$data['itemName'] 		= "<span id=".$row1['ITEM_ID'].">".$row1['ITEM_NAME']."</span>";
	$data['Unit'] 		= "<span>".$row1['UNIT']."</span>";
	//$html .="<td style=\"text-align:center\">&nbsp;</td>";
	
	for($i=$startMonth;$i<=$endMonth;$i++)
	{
		$month		= $i;
		if($i>12)
			$month 	= $i-12;
		$allocateBudgetArr 			= $obj_budget_get->getAllocatedBudget($locationID,$department,$financeYear,$month,$row1['SUB_CATEGORY_ID'],$row1['ITEM_ID']);
		
		$data[$i.'_Amount']			= "<span id=\"td_amount\"><input title=\"".$row1['SUB_CATEGORY_ID'].'~'.$row1['ITEM_ID']."\" type=\"textbox\" style=\"width:70px;text-align:right\" class=\"clsBudget clsAmount validate[custom[number]]\" id=\"".$month."\" value=\"".($allocateBudgetArr['INITIAL_BUDGET'])."\" ".($row1['BUDGET_TYPE']=='A'?'':'disabled="disabled"')."></span>";
		$data[$i.'_Unit']			= "<span id=\"td_unit\"><input title=\"".$row1['SUB_CATEGORY_ID'].'~'.$row1['ITEM_ID']."\" type=\"textbox\" style=\"width:70px;text-align:right\" class=\"clsBudget clsAmount validate[custom[number]]\" id=\"".$month."\" value=\"".($allocateBudgetArr['INITIAL_BUDGET_UNITS'])."\" ".($row1['BUDGET_TYPE']=='U'?'':'disabled="disabled"')."></span>";
	}
	$a[] 								= $data;
}
//END	- NORMAL ITEMS }
$arrayTotal["rows"]	= $a;
echo json_encode($arrayTotal);
?>
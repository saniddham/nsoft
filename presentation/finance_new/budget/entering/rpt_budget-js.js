// JavaScript Document
$(document).ready(function() {
	
	//$("#frmBudgetPlan").validationEngine();
	$('#frmBudgetPlan #butRptConfirm').die('click').live('click',urlApprove);
	$('#frmBudgetPlan #butRptReject').die('click').live('click',urlReject);
	$('#frmBudgetPlan #butRptRevise').die('click').live('click',urlRevise);
	
});
function urlApprove()
{

	var val = $.prompt('Are you sure you want to approve this Budget Plan ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "budgetPlan_db.php"+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmBudgetPlan #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmBudgetPlan #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function urlReject()
{
	var val = $.prompt('Are you sure you want to reject this Budget Plan ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "budgetPlan_db.php"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmBudgetPlan #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx1()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmBudgetPlan #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx1()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}

function urlRevise()
{
	var val = $.prompt('Are you sure you want to Revise this Budget Plan ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "budgetPlan_db.php"+window.location.search+'&requestType=revise';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmBudgetPlan #butRptRevise').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmBudgetPlan #butRptRevise').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx2()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}

function alertx()
{
	$('#frmBudgetPlan #butRptConfirm').validationEngine('hide')	;
}
function alertx1()
{
	$('#frmBudgetPlan #butRptReject').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmBudgetPlan #butRptRevise').validationEngine('hide')	;
}
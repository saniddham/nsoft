<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "libraries/jqgrid2/inc/jqgrid_dist.php";
$userId			= $_SESSION["userId"];
$reportMenuId	= 940;
$menuId			= 839;
$programCode	= 'P0839';
$approveLevel = (int)getMaxApproveLevel();

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'STATUS'=>'BH.STATUS',
				'CONCAT_PERIOD'=>"CONCAT(FP.dtmStartingDate,' | ',FP.dtmClosingDate)",
				'LOCATION_NAME'=>'LO.strName',
				'DEPARTMENT_NAME'=>'D.strName'
				);
				
$arr_status = array('Confirmed'=>'1','Rejected'=>'0','Pending'=>'2','Revised'=>'-1');
foreach($arr as $k=>$v)
{
	if($v['field']=='STATUS')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND DATE(BH.CREATED_DATE) = '".date('Y-m-d')."'";
	
// }

$sql = "SELECT SUB_1.* FROM
		(SELECT
		  if(BH.STATUS=1,'Confirmed',if(BH.STATUS=0,'Rejected',if(BH.STATUS=-1,'Revised',if(BH.STATUS=-2,'Cancelled','Pending')))) as STATUS,
		  FP.dtmStartingDate 									AS PERIOD_START,
		  FP.dtmClosingDate  									AS PERIOD_END,
		  CONCAT(FP.dtmStartingDate,' | ',FP.dtmClosingDate)	AS CONCAT_PERIOD,
		  LO.intId 												AS LOCATION_ID,
		  LO.strName         									AS LOCATION_NAME,
		  D.intId												AS DEPARTMENT_ID,
		  D.strName          									AS DEPARTMENT_NAME,
		  BH.FINANCE_YEAR_ID									AS FINANCE_YEAR_ID,
		  BH.CREATED_DATE    									AS CREATED_DATE,
		  U.strUserName      									AS CREATED_BY,
		  (SELECT
			 SUM(INITIAL_BUDGET_UNITS)
		   FROM budget_plan_details BD
		   WHERE BD.FINANCE_YEAR_ID = BH.FINANCE_YEAR_ID
		   AND BD.DEPARTMENT_ID = BH.DEPARTMENT_ID
		   AND BD.LOCATION_ID = BH.LOCATION_ID)					AS INITIAL_BUDGET_UNITS,
		   (SELECT
			 SUM(INITIAL_BUDGET)
		   FROM budget_plan_details BD
		   WHERE BD.FINANCE_YEAR_ID = BH.FINANCE_YEAR_ID
		   AND BD.DEPARTMENT_ID = BH.DEPARTMENT_ID
		   AND BD.LOCATION_ID = BH.LOCATION_ID)					AS INITIAL_BUDGET,
		  (SELECT
			 SUM(TRANSFER_BUDGET)
		   FROM budget_plan_details BD
		   WHERE BD.FINANCE_YEAR_ID = BH.FINANCE_YEAR_ID
		   AND BD.DEPARTMENT_ID = BH.DEPARTMENT_ID
		   AND BD.LOCATION_ID = BH.LOCATION_ID)					AS TRANSFER_BUDGET,
		   
		   IFNULL((
			SELECT
			CONCAT(U.strUserName,'(',max(BPAB.APPROVED_DATE),')' )
			FROM
				budget_plan_approve_by BPAB
			INNER JOIN sys_users U 
				ON BPAB.APPROVED_BY = U.intUserId
			WHERE
					BPAB.LOCATION_ID   = BH.LOCATION_ID
				AND BPAB.DEPARTMENT_ID =  BH.DEPARTMENT_ID
				AND BPAB.FINANCE_YEAR_ID =  BH.FINANCE_YEAR_ID
				AND BPAB.APPROVED_LEVEL_NO = '1'
				AND BPAB.STATUS =  '0'
			),IF(((SELECT
			MP.int1Approval 
			FROM menupermision MP
			Inner Join menus M
				ON MP.intMenuId = M.intId
			WHERE
				M.strCode = '$programCode' 
				AND MP.intUserId = '$userId')=1 
				AND BH.STATUS>1),'Approve', '')) 				AS `1st_Approval`,	";
				
		for($i=2; $i<=$approveLevel; $i++){							
			if($i==2){
				$approval	= "2nd_Approval";
			}
			else if($i==3){
				$approval	= "3rd_Approval";
			}
			else {
				$approval	= $i."th_Approval";
			}		
		
	$sql .= "IFNULL(
		(
		SELECT
			CONCAT(U.strUserName,'(',max(BPAB.APPROVED_DATE),')' )
			FROM
				budget_plan_approve_by BPAB
			INNER JOIN sys_users U 
				ON BPAB.APPROVED_BY = U.intUserId
			WHERE
					BPAB.LOCATION_ID   = BH.LOCATION_ID
				AND BPAB.DEPARTMENT_ID =  BH.DEPARTMENT_ID
				AND BPAB.FINANCE_YEAR_ID =  BH.FINANCE_YEAR_ID
				AND BPAB.APPROVED_LEVEL_NO = '$i'
				AND BPAB.STATUS =  '0'
		),
		IF(
		((SELECT
			 MP.int".$i."Approval 
		FROM menupermision MP
		INNER JOIN menus M 
			ON MP.intMenuId = M.intId
		WHERE
			M.strCode = '$programCode' 
			AND MP.intUserId =  '$userId')=1 
			AND (BH.STATUS>1) 
			AND (BH.STATUS<=BH.APPROVE_LEVELS)
			AND ((SELECT
				concat(U.strUserName )
				FROM
					budget_plan_approve_by BPAB
				INNER JOIN sys_users U ON BPAB.APPROVED_BY = U.intUserId
				WHERE
						BPAB.LOCATION_ID   = BH.LOCATION_ID
					AND BPAB.DEPARTMENT_ID =  BH.DEPARTMENT_ID
					AND BPAB.FINANCE_YEAR_ID =  BH.FINANCE_YEAR_ID
					AND BPAB.APPROVED_LEVEL_NO =  ($i-1)
					AND BPAB.STATUS='0' )<>'')),
		
		'Approve',
		if($i>BH.APPROVE_LEVELS,'-----',''))
		
		) as `".$approval."`, "; 
									
		}		
		
		$sql .= "IF(((SELECT
				MP.intRevise 
				FROM menupermision MP
				Inner Join menus M ON MP.intMenuId = M.intId
				WHERE
				M.strCode =  '$programCode' AND
				MP.intUserId =  '$userId') = 1),
				  if((SELECT DISTINCT 
					BH.STATUS
					FROM budget_plan_header SUB_BH
					WHERE 
						SUB_BH.STATUS = '1' 
						AND SUB_BH.LOCATION_ID = BH.LOCATION_ID
						AND SUB_BH.DEPARTMENT_ID = BH.DEPARTMENT_ID
						AND SUB_BH.FINANCE_YEAR_ID = BH.FINANCE_YEAR_ID)=1,'Revise',''),'')  as `Revise`, ";	
					  
		$sql .= "
			'View' AS VIEW,
			'Report' AS REPORT 
		  FROM budget_plan_header BH
		  INNER JOIN mst_financeaccountingperiod FP
			ON FP.intId = BH.FINANCE_YEAR_ID
		  INNER JOIN mst_locations LO
			ON LO.intId = BH.LOCATION_ID
		  INNER JOIN mst_department D
			ON D.intId = BH.DEPARTMENT_ID
		  INNER JOIN mst_financeaccountingperiod_companies FPC
			ON FPC.intPeriodId = BH.FINANCE_YEAR_ID
			  AND FPC.intCompanyId = LO.intCompanyId
		  INNER JOIN sys_users U
			ON U.intUserId = BH.CREATED_BY
			where 1=1
			$where_string
		  )  
		AS SUB_1 WHERE 1=1";
//die($sql);
$jq = new jqgrid('',$db);	
$col = array();

//BEGIN - STATUS {
$col["title"] 			= "Status";
$col["name"] 			= "STATUS";
$col["width"] 			= "2"; 						// not specifying width will expand to fill space
$col["align"] 			= "left";
$col["sortable"] 		= true; 						// this column is not sortable
$col["search"] 			= true; 						// this column is not searchable
$col["editable"] 		= false;
$col["stype"] 			= "select";
$str 					= ":All;Pending:Pending;Confirmed:Confirmed;Revised:Revised;Canceled:Canceled";  // all row, blank row, then all db values. ; is separator
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;
//END 	- STATUS }

$col["title"] 			= "Accounting Period";
$col["name"] 			= "CONCAT_PERIOD";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Location Id";
$col["name"] 			= "LOCATION_ID";
$col["hidden"] 			= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Location Name";
$col["name"] 			= "LOCATION_NAME";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Department Id";
$col["name"] 			= "DEPARTMENT_ID";
$col["hidden"] 			= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Department Id";
$col["name"] 			= "FINANCE_YEAR_ID";
$col["hidden"] 			= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Department";
$col["name"] 			= "DEPARTMENT_NAME";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Initial Budget Amount";
$col["name"] 			= "INITIAL_BUDGET";
$col["width"] 			= "5";
$col["align"] 			= "right";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Initial Budget Units";
$col["name"] 			= "INITIAL_BUDGET_UNITS";
$col["width"] 			= "5";
$col["align"] 			= "right";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Transfer Budget";
$col["name"] 			= "TRANSFER_BUDGET";
$col["width"] 			= "5";
$col["align"] 			= "right";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

//BEGIN - FIRST APPROVAL {
$col["title"] 			= "1st Approval";
$col["name"] 			= "1st_Approval";
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= "?q=".$reportMenuId."&LocId={LOCATION_ID}&DepId={DEPARTMENT_ID}&YearId={FINANCE_YEAR_ID}&Mode=Confirm";
$col["linkoptions"] 	= "target='rptReport.php'";
$col['linkName']		= 'Approve';
$cols[] 				= $col;	
$col					= NULL;
//END	- FIRST APPROVEL }

//BEGIN - OTHER APPROVALS {
for($i=2; $i<=$approveLevel; $i++)
{
	if($i==2){
		$ap		= "2nd Approval";
		$ap1	= "2nd_Approval";
	}
	else if($i==3){
		$ap		= "3rd Approval";
		$ap1	= "3rd_Approval";
	}
	else {
		$ap		= $i."th Approval";
		$ap1	= $i."th_Approval";
	}

$col["title"] 				= $ap;
$col["name"] 				= $ap1;
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= "rptReport.php?q=".$reportMenuId."&LocId={LOCATION_ID}&DepId={DEPARTMENT_ID}&YearId={FINANCE_YEAR_ID}&Mode=Confirm";
$col["linkoptions"] 		= "target='rptReport.php'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;
}	
//END 	- OTHER APPROVALS }

//BEGIN - REVISION {
$col["title"] 			= "Revise";
$col["name"] 			= "Revise";
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= "?q=".$reportMenuId."&LocId={LOCATION_ID}&DepId={DEPARTMENT_ID}&YearId={FINANCE_YEAR_ID}&Mode=Revise";
$col["linkoptions"] 	= "target='rptReport.php'";
$col['linkName']		= 'Revise';
$cols[] 				= $col;	
$col					= NULL;
//END 	- REVISION }

$col["title"] 			= "View";
$col["name"] 			= "VIEW";
$col["width"] 			= "1";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			 = "?q=".$menuId."&location={LOCATION_ID}&department={DEPARTMENT_ID}&financeYear={FINANCE_YEAR_ID}";
$col["linkoptions"] 	= "target='budgetPlan.php'";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Report";
$col["name"] 			= "REPORT";
$col["width"] 			= "1";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= "?q=".$reportMenuId."&LocId={LOCATION_ID}&DepId={DEPARTMENT_ID}&YearId={FINANCE_YEAR_ID}";
$col["linkoptions"] 	= "target='rptReport.php'";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Budget Planning Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= ''; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"STATUS","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;
//$grid["postData"] = array("filters" => $sarr ); 


$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Journal Entry Listing</title>
<?php
echo $out;

function getMaxApproveLevel()
{
	global $db;

	$sqlp = "SELECT
			  COALESCE(MAX(BH.APPROVE_LEVELS),0) AS APPROVE_LEVEL
			FROM budget_plan_header BH";					
	$resultp 	= $db->RunQuery($sqlp);
	$rowp		= mysqli_fetch_array($resultp);
	return $rowp['APPROVE_LEVEL'];
}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$userId				= $_SESSION["userId"];

include_once "class/finance/budget/entering/cls_budget_get.php";
include_once "class/cls_commonErrorHandeling_get.php";

//BEGIN - CREATE OBJECTS {
$obj_budget_get			= new cls_budget_get($db);
$obj_commonErrHandle	= new cls_commonErrorHandeling_get($db);
//END   - CREATE OBJECTS }

//BEGIN - ASSIGN VARIABLES {
$locationId				= $_REQUEST["LocId"];
$departmentId			= $_REQUEST["DepId"];
$finaceYearId			= $_REQUEST["YearId"];
$mode					= $_REQUEST["Mode"];
$programCode			= 'P0839';

$lastRevisionNo			= $obj_budget_get->getLastRevisionNo($locationId,$departmentId,$finaceYearId);
$header_array 			= $obj_budget_get->get_header('',$locationId,$departmentId,$finaceYearId,'RunQuery');
$detail_result 			= $obj_budget_get->getReportGridDetails($locationId,$departmentId,$finaceYearId);
$financeMonth_arr		= $obj_budget_get->getFinanceMonths($finaceYearId);
$status					= $header_array["STATUS"];
$approve_level			= $header_array["LEVELS"];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_reject($status,$approve_level,$userId,$programCode,'RunQuery');
$permision_reject		= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_confirm($status,$approve_level,$userId,$programCode,'RunQuery');
$permision_confirm		= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_revise($status,$approve_level,$userId,$programCode,'RunQuery');
$permision_revise		= $permition_arr['permision'];
//END 	- ASSIGN VARIABLES }
?>
<head>
<title>Budget Planing Report</title>
<script type="text/javascript" src="presentation/finance_new/budget/entering/rpt_budget-js.js"></script>
</head>
<body>
<style type="text/css">
.apDiv1 {
	position:absolute;
	left:380px;
	top:100px;
	width:auto;
	height:auto;
	z-index:0;
	opacity:0.1;
}

@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}
</style>
<div id="partPay" class="apDiv1 <?php echo $header_array["STATUS"]=='10'?'maskShow':'maskHide'?>"><img src="images/cancelled1.png"  /></div>
<form id="frmBudgetPlan" name="frmBudgetPlan" method="post" action="rptReport.php" autocomplete="off">
  <table width="100%" align="center">
    <tr>
      <td><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
      <td class="reportHeader" align="center">Budget Planing Report</td>
    </tr>
    <tr>
      <td style="text-align:center"><?php include "report_approve_status_and_buttons.php"?>
      </td>
    
      </tr>
    
    <tr>
      <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td width="14%">Location Name</td>
            <td width="1%">:</td>
            <td width="43%"><?php echo $header_array["LOCATION_NAME"]?></td>
            <td width="14%">Department Name</td>
            <td width="2%">:</td>
            <td width="26%"><?php echo $header_array["DEPARTMENT_NAME"]?></td>
          </tr>
          <tr>
            <td>Financial Period</td>
            <td>:</td>
            <td><?php echo $header_array["CONCAT_PERIOD"]?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td align="right">&nbsp;</td>
    </tr>
    <tr>
      <td ><table width="100%%" border="0" class="rptBordered" id="tblMain">
          <thead>
            <tr>
              <th width="7%" rowspan="2">No</th>
              <th width="32%" rowspan="2">Sub Category</th>
              <th width="41%" rowspan="2">Item Description</th>
              <th width="20%" rowspan="2">Unit</th>
              <?php for($i=$financeMonth_arr['startMonth'];$i<=12+$financeMonth_arr['endMonth'];$i++)
			  {?>
              <th nowrap="nowrap" colspan="2"><?php echo date('Y-M', mktime(0,0,0,$i,1)) ?></th>
              <?php } ?>
            </tr>
            <tr>
              <?php for($i=$financeMonth_arr['startMonth'];$i<=12+$financeMonth_arr['endMonth'];$i++)
			  {?>
              <th width="10%">Amount</th>
              <th width="10%">Units</th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <?php
$total_value	= 0;
$x	= 0;
while($row = mysqli_fetch_array($detail_result))
{ 
?>
            <tr>
              <td align="center"><?php echo ++$x;?>.</td>
              <td><?php echo $row["SUB_CATEGORY_NAME"]?></td>
              <td width="41%"><?php echo $row["ITEM_NAME"]?></td>
              <td style="text-align:center" width="20%"><?php echo $row['UNIT']; ?></td>
              <?php for($i=$financeMonth_arr['startMonth'];$i<=12+$financeMonth_arr['endMonth'];$i++)
			  {
				  $result = $obj_budget_get->getBudgetMonthlyAmount($locationId,$departmentId,$finaceYearId,$row["SUB_CATEGORY_ID"],$row["ITEM_ID"],date('m', mktime(0,0,0,$i,1)));
				  $c_array[$i][0]	= $result["INITIAL_BUDGET"];
				  $c_array[$i][1]	= $result["INITIAL_BUDGET_UNITS"];
				  $result1 = $obj_budget_get->getBudgetMonthlyAmountHistory($lastRevisionNo,$locationId,$departmentId,$finaceYearId,$row["SUB_CATEGORY_ID"],$row["ITEM_ID"],date('m', mktime(0,0,0,$i,1)));
				  $r_array[$i][0]	= $result1["INITIAL_BUDGET"];
				  $r_array[$i][1]	= $result1["INITIAL_BUDGET_UNITS"];				  
				  ?>
              <td nowrap="nowrap" style="text-align:right;background-color:<?php echo ($c_array[$i][0]!=$r_array[$i][0] && $lastRevisionNo>0 ? '#FFE8F3':'')?>" title="<?php echo $r_array[$i][0]?>"><?php echo ($result["INITIAL_BUDGET"]==0?'&nbsp;':number_format($result["INITIAL_BUDGET"],2))?></td>
              <td nowrap="nowrap" style="text-align:right;background-color:<?php echo ($c_array[$i][1]!=$r_array[$i][1] && $lastRevisionNo>0 ? '#FFE8F3':'')?>" title="<?php echo $r_array[$i][1]?>"><?php echo ($result["INITIAL_BUDGET_UNITS"]==0?'&nbsp;':number_format($result["INITIAL_BUDGET_UNITS"],2))?></td>
              <?php } ?>
            </tr>
            
            <?php /*if(isHistoryAvailabel($locationId,$departmentId,$finaceYearId)){
				?>
            <tr bgcolor="#FFE8F3">
              <td align="center" colspan="3"><?php echo "Revision[".$lastRevisionNo."]"?></td>              
              <?php for($i=$financeMonth_arr['startMonth'];$i<=12+$financeMonth_arr['endMonth'];$i++) //#F20E30 : #0BA333
			  {
				  $result = $obj_budget_get->getBudgetMonthlyAmountHistory($locationId,$departmentId,$finaceYearId,$row["SUB_CATEGORY_ID"],$row["ITEM_ID"],date('m', mktime(0,0,0,$i,1)));
				  $r_array[$i][0]	= $result["INITIAL_BUDGET"];
				  $r_array[$i][1]	= $result["INITIAL_BUDGET_UNITS"];
				  ?>
              <td nowrap="nowrap" style="text-align:right;color:" ><?php echo ($result["INITIAL_BUDGET"]==0?'':number_format($result["INITIAL_BUDGET"],2))?></td>
              <td nowrap="nowrap" style="text-align:right;color:<?php echo ($c_array[$i][1]>$r_array[$i][1] ? '#F20E30':($c_array[$i][1] < $r_array[$i][1] ? '#0BA333':''))?>"><?php echo ($result["INITIAL_BUDGET_UNITS"]==0?'':number_format($result["INITIAL_BUDGET_UNITS"],2))?></td>
              <?php } ?>
            </tr>
            <?php }*/ ?>
            
            <?php
}
?>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td ><table width="314" border="0" align="left" class="normalfnt">
          <tr>
            <td width="106"><?php
            $creator		= $header_array['USER'];
            $createdDate	= $header_array['CREATED_DATE'];
            $resultA 		= $obj_budget_get->get_Report_approval_details_result($locationId,$departmentId,$finaceYearId,'RunQuery');
            include "presentation/report_approvedBy_details.php"
            ?></td>
          </tr>
          
        </table></td>
    </tr>
    <?php
		if($mode=='Confirm')
		{
	?>
    <tr height="40">
    <?php
		$createCompanyId = $header_array['COMPANY_ID'];
		$url  = "{$backwardseperator}presentation/sendToApproval.php";
		$url .= "?status=$status";										// * set recent status
		$url .= "&approveLevels=$approve_level";								// * set approve levels in order header
		$url .= "&programCode=$programCode";								// * program code (ex:P10001)
		$url .= "&program=BUDGET PLAN";									// * program name (ex:Purchase Order)
		$url .= "&companyId=$createCompanyId";									// * created company id
		$url .= "&createUserId=".$header_array['CREATED_BY']."";
		
		$url .= "&subject=BUDGET PLAN FOR APPROVAL (".$header_array['CONCAT_PERIOD'].")";	
		
		$url .= "&statement1=Please Approve this";	
		$url .= "&statement2=to Approve this";	
								// * doc year
		$url .= "&link=".urlencode(base64_encode($mainPath."presentation/finance_new/budget/entering/rptReport.php?LocId=".$header_array['LOCATION_ID']."&DepId=".$header_array['DEPARTMENT_ID']."&YearId=".$header_array['FINANCE_YEAR_ID']."&Mode=Confirm"));
?>
    	<td align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"></iframe></td>
    </tr>
    <?php
		}
	?>
    <tr> </tr>
  </table>
</form>
</body>
</html>
<?php
function isHistoryAvailabel($locationId,$departmentId,$finaceYearId)
{
	global $db;
	
	$sql = "SELECT * 
			FROM budget_plan_details_history
			WHERE LOCATION_ID = '$locationId'
			AND DEPARTMENT_ID = '$departmentId'
			AND FINANCE_YEAR_ID = '$finaceYearId'";
	$result = $db->RunQuery($sql);
	$rowCount = mysqli_num_rows($result);
	if($rowCount>0)
		return true;
	else
		return false;
}
?>
// JavaScript Document
var basePath = "presentation/finance_new/budget/entering/";
var reportMenuId = 940;
$(document).ready(function(){
	
	$('#frmBudgetPlan').validationEngine();	
	$('#frmBudgetPlan #cboFinanceYear').die('change').live('change',clearGrid);
	$('#frmBudgetPlan #cboLocation').die('change').live('change',clearGrid);
	$('#frmBudgetPlan #cboDepartment').die('change').live('change',clearGrid);
	$('#frmBudgetPlan #butSearch').die('click').live('click', doSearch);
	$('#frmBudgetPlan #butSave').die('click').live('click',saveData);
	$('#frmBudgetPlan #butNew').die('click').live('click',clearAll);	
	$('#frmBudgetPlan #butConfirm').die('click').live('click',Confirm);
	$('#frmBudgetPlan #butRevise').die('click').live('click',Revise);
	$('#frmBudgetPlan #butReport').die('click').live('click',loadReport);
	

});

function doSearch(){
	
	if($('#frmBudgetPlan').validationEngine('validate'))
	{
		jq('#tt').datagrid('load',{
		Department: jq('#cboDepartment').val(),
		FinanceYear: jq('#cboFinanceYear').val(),
		Location: jq('#cboLocation').val()
		});
	}
}
	
function loadFinanceMonths()
{
	
	var url 	= basePath+"budgetPlan_db.php?requestType=loadFinanceMonths";
	var data 	= "financeYear="+$('#frmBudgetPlan #cboFinanceYear').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#tblMain thead').html(json.financeMonths);
			}
	});	
}
function clearGrid()
{
	jq('#tt').datagrid('loadData', {"total":0,"rows":[]});
}
function loadBudgetDetail()
{
	if($('#frmBudgetPlan').validationEngine('validate'))
	{
		showWaiting();
		$("#tblMain tr:gt(1)").remove();
		var url 	= basePath+"budgetPlan_db.php?requestType=loadBudgetDetail";
		var data 	= "location="+$('#frmBudgetPlan #cboLocation').val()+"&department="+$('#frmBudgetPlan #cboDepartment').val()+"&financeYear="+$('#frmBudgetPlan #cboFinanceYear').val();
		$.ajax({
				url:url,
				data:data,
				dataType:'json',
				type:'POST',
				async:false,
				success:function(json)
				{
					$('#tblMain tbody').html(json.gridDetail);
				}
		});
		hideWaiting();		
	}
	
}
function saveData()
{
	showWaiting();
	var location		= $('#frmBudgetPlan #cboLocation').val();
	var department		= $('#frmBudgetPlan #cboDepartment').val();
	var financeYear		= $('#frmBudgetPlan #cboFinanceYear').val();
	
	if($('#frmBudgetPlan').validationEngine('validate'))
	{
		var arrHeader = "{";
							arrHeader += '"location":"'+location+'",' ;
							arrHeader += '"department":"'+department+'",' ;
							arrHeader += '"financeYear":"'+financeYear+'"' ;
			arrHeader  += "}";
		
		var arrDetails	= "";
		var chkStatus	= false;
		$('#tblMain .clsBudget').each(function(){
			
			var budgetVal		= '';
			//var subcatArr		= $(this).parent().parent().attr('id');
			var subcatArr		= $(this).attr('title');
			var month			= $(this).attr('id');
			var type			= '';
			
			if($(this).parent().attr('id')=='td_amount')
			{
				budgetVal		= $(this).val();
				type			= 'A';
			}
		
			if($(this).parent().attr('id')=='td_unit')
			{
				budgetVal		= $(this).val();
				type			= 'U';
			}
			
			if(budgetVal!='')
				chkStatus		= true;
			
			arrDetails += "{";
			arrDetails += '"subcatArr":"'+ subcatArr +'",' ;
			arrDetails += '"month":"'+ month +'",' ;
			arrDetails += '"budgetVal":"'+ budgetVal +'",' ;
			arrDetails += '"type":"'+ type +'"' ;
			arrDetails +=  '},';
			
		});
		if(!chkStatus)
		{
			$(this).validationEngine('showPrompt','No data to save.','fail');
			hideWaiting();	
			return;
		}
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		
		var arrHeader	= arrHeader;
		var data		= "requestType=saveData";
		var arrDetails	= '['+arrDetails+']';
		data	   	   += "&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		var url 	= basePath+"budgetPlan_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmBudgetPlan #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						$('#frmBudgetPlan #butConfirm').show();
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
					$('#frmBudgetPlan #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					hideWaiting();
					return;
				}		
		});
	}
	else
	{
		hideWaiting();
	}
}
function clearAll()
{
	clearGrid();
	$('#frmBudgetPlan')[0].reset();
	$('#frmBudgetPlan #cboLocation').val('');
	$('#frmBudgetPlan #cboDepartment').val('');
	$('#frmBudgetPlan #butSave').show();
	$('#frmBudgetPlan #butRevise').hide();
	$('#frmBudgetPlan #butConfirm').hide();
	
	
}
function Confirm()
{
	var url  = "?q="+reportMenuId+"&LocId="+$('#frmBudgetPlan #cboLocation').val();
	    url += "&DepId="+$('#frmBudgetPlan #cboDepartment').val();
		 url += "&YearId="+$('#frmBudgetPlan #cboFinanceYear').val();
	    url += "&Mode=Confirm";
	window.open(url,'rptReport.php');
}
function Revise()
{
	var url  = "?q="+reportMenuId+"&LocId="+$('#frmBudgetPlan #cboLocation').val();
	    url += "&DepId="+$('#frmBudgetPlan #cboDepartment').val();
		 url += "&YearId="+$('#frmBudgetPlan #cboFinanceYear').val();
	    url += "&Mode=Revise";
	window.open(url,'rptReport.php');
}
function loadReport()
{
	if($('#frmBudgetPlan').validationEngine('validate'))
	{
		var url  = "?q="+reportMenuId+"&LocId="+$('#frmBudgetPlan #cboLocation').val();
	    	url += "&DepId="+$('#frmBudgetPlan #cboDepartment').val();
			url += "&YearId="+$('#frmBudgetPlan #cboFinanceYear').val();
		window.open(url,'rptReport.php');
	}
}
function alertx()
{
	$('#frmBudgetPlan #butSave').validationEngine('hide')	;
}
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId				= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$userId					= $_SESSION["userId"];

include_once "class/cls_commonFunctions_get.php";
include_once "class/finance/budget/cls_common_function_budget_get.php";
include_once "class/finance/budget/entering/cls_budget_get.php";
include_once "class/cls_commonErrorHandeling_get.php";
//include 	 "include/javascript.html"; 

$obj_common				= new cls_commonFunctions_get($db);
$obj_budget_common		= new cls_common_function_budget_get($db);
$obj_budget_get			= new cls_budget_get($db);
$obj_commonErrHandle	= new cls_commonErrorHandeling_get($db);

$location				= $_REQUEST['location'];
$department				= $_REQUEST['department'];
$financeYear			= $_REQUEST['financeYear'];

$programCode			= 'P0839';

$header_arr				= $obj_budget_get->get_header('',$location,$department,$financeYear,'RunQuery');
//detail_result			= $obj_requisition_get->loadDetailData($requisitionNo,$requisitionYear,'RunQuery');

$intStatus				= $header_arr['STATUS'];
$levels					= $header_arr['LEVELS'];

$permision_arr			= $obj_commonErrHandle->get_permision_withApproval_save($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_save			= $permision_arr['permision'];

$permision_arr			= $obj_commonErrHandle->get_permision_withApproval_revise($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_revise		= $permision_arr['permision'];

$permision_arr			= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_confirm		= $permision_arr['permision'];


	
?><head>
<title>Budget Plan</title>

<link rel="stylesheet" type="text/css" href="libraries/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="libraries/easyui/demo/demo.css">
<link rel="stylesheet" type="text/css" href="css/mainstyle.css">
<link rel="stylesheet" type="text/css" href="css/button.css">

<!--<script type="text/javascript" src="libraries/jquery/jquery-1.4.4.min.js"></script>-->
<!--<script type="text/javascript" src="libraries/easyui/jquery.easyui.min.js"></script>-->

<!--<script type="text/javascript" src="presentation/finance_new/budget/entering/budgetPlan_js.js"></script>-->


<form id="frmBudgetPlan" name="frmBudgetPlan" autocomplete="off" method="post">
  <div align="center">
    <div class="trans_layoutXL">
      <div class="trans_text">Budget Plan</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" >
              <tr class="normalfnt">
                <td width="8%">Location <span class="compulsoryRed">*</span></td>
                <td width="25%"><select name="cboLocation" id="cboLocation" class="validate[required]" style="width:250px">
                    <option value=""></option>
                    <?php
					$result = $obj_common->loadLocation($companyId,'RunQuery');
					while($row = mysqli_fetch_array($result))
					{
						if($header_arr['LOCATION_ID']==$row['intId'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
						else if($cboLocation==$row['intId'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
					?>
                  </select></td>
                <td width="9%">Department <span class="compulsoryRed">*</span></td>
                <td width="25%"><select name="cboDepartment" id="cboDepartment" class="validate[required]" style="width:250px">
                    <option value=""></option>
                    <?php
					$result = $obj_common->loadDepartment('RunQuery');
					while($row = mysqli_fetch_array($result))
					{
						if($header_arr['DEPARTMENT_ID']==$row['intId'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
						else if($cboDepartment==$row['intId'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
					?>
                  </select></td>
                <td width="9%">Financial Year <span class="compulsoryRed">*</span></td>
                <td width="16%"><select name="cboFinanceYear" id="cboFinanceYear" class="validate[required]" style="width:150px">
                    <?php
					$finaceYearId	= '';
					$result = $obj_budget_common->loadFinanceYear($companyId,'RunQuery');
					while($row = mysqli_fetch_array($result))
					{
						if($header_arr['FINANCE_YEAR_ID']==$row['intId'])
						{
							$finaceYearId = $row['intId'];
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['financeYear']."</option>";
						}
						else if($cboFinanceYear==$row['intId'])
						{
							$finaceYearId = $row['intId'];
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['financeYear']."</option>";
						}
						else if($cboFinanceYear=='' && date('Y')==$row['startYear'])
						{
							$finaceYearId = $row['intId'];
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['financeYear']."</option>";
						}
						else
							echo "<option value=\"".$row['intId']."\">".$row['financeYear']."</option>";
					}
					
					?>
                  </select></td>
                <td width="8%" style="text-align:right"><a class="button green small" id="butSearch" name="butSearch">Search</a></td>
              </tr>
              <tr class="normalfnt">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td style="text-align:right">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td height="32" ><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" id="tblMain">
              <tr>
                <td align="center"><table id="tt" title="Budget Plan" class="easyui-datagrid"  style="width:1250px;height:400px"
			url="presentation/finance_new/budget/entering/budgetPlan_data.php?FromDate=<?php echo $fromDate?>&amp;ToDate=<?php echo $toDate?>&amp;Location=<?php echo $location?>" rownumbers="true" pagination="true" singleselect="false" iconcls="icon-save" showfooter="true">
                    <thead frozen="true">
                      <tr>
                        <th rowspan="3" field="subCategory1" sortable="true" width="125">Sub Category</th>
                        <th rowspan="3"  field="itemName" sortable="true" width="200">Item Description</th>
                        <th rowspan="3"  field="Unit" sortable="true" width="60">Unit</th>
                      </tr>
                      <tr></tr>
                      <tr></tr>
                    </thead>
                    <thead>
                      <tr>
                        <?php
						$financeMonth_arr	= $obj_budget_get->getFinanceMonths($finaceYearId);
						$startMonth			= $financeMonth_arr['startMonth'];
						$endMonth			= 12+$financeMonth_arr['endMonth'];	
						
						if($financeMonth_arr['startYear']==$financeMonth_arr['endYear'])
							$endMonth		= $financeMonth_arr['endMonth'];
                            for($i=$startMonth;$i<=$endMonth;$i++)
                            {		
                        ?>
                        <th colspan="2" style="width:200px;"><?php echo date('M', mktime(0,0,0,$i,1)).' - '.($i>12?$financeMonth_arr['endYear']:$financeMonth_arr['startYear']) ?></th>
                        <?php
                            }
                        ?>
                      </tr>
                      <tr>
                        <?php
                            for($i=$startMonth;$i<=$endMonth;$i++)
                            {		
                        ?>
                        <th style="width:100px;"  field="<?php echo $i."_Amount"?>">Amount</th>
                        <th style="width:100px;"  field="<?php echo $i."_Unit"?>">Units</th>
                        <?php
                            }
                        ?>
                      </tr>
                    </thead>
                  </table></td>
              </tr>
              <tr>
                <td align="center">&nbsp;</td>
              </tr>
              <tr>
                <td align="center"><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a><a class="button white medium" id="butConfirm"  <?php if($permision_confirm!=1){ ?> style="display:none"<?php } ?>>Approve</a><a class="button white medium" id="butRevise" <?php if($permision_revise!=1){ ?>  style="display:none"<?php } ?>>Revise</a><a class="button white medium" id="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
              </tr>
              <tr>
                <td align="center">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table>
    </div>
  </div>
</form>

<div style="width:500px; position: absolute;display:none;z-index:100" id="popupContact1"></div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>

<script type="text/javascript" src="libraries/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="libraries/easyui/jquery.easyui.min.js"></script>

<script>
var jq = $.noConflict(true);
</script>

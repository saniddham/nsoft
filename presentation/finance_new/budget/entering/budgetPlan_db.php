<?php
	session_start();
	//ini_set('display_errors',1);
	$backwardseperator 		= "../../../../";
	$mainPath 				= $_SESSION['mainPath'];
	$userId 				= $_SESSION['userId'];
	$location 				= $_SESSION['CompanyID'];
	$company 				= $_SESSION['headCompanyId'];
	
	include_once  	"{$backwardseperator}dataAccess/Connector.php";
 	require_once "../../../../class/cls_commonFunctions_get.php";
	require_once "../../../../class/cls_commonErrorHandeling_get.php";
	require_once "../../../../class/finance/budget/cls_common_function_budget_get.php";
	require_once "../../../../class/finance/budget/entering/cls_budget_get.php";
	require_once "../../../../class/finance/budget/entering/cls_budget_set.php";
	
	$obj_common				= new cls_commonFunctions_get($db);
	$obj_commonErr			= new cls_commonErrorHandeling_get($db);
	$obj_budget_get			= new cls_budget_get($db);
	$obj_budget_set			= new cls_budget_set($db);
	$obj_comm_budget_get	= new cls_common_function_budget_get($db);

	$requestType			= $_REQUEST['requestType'];
 	
	$programName			= 'Budget Plan';
	$programCode			= 'P0839';
	
	$savedStatus			= true;
	$savedMasseged			= '';
	$error_sql				= '';

 	//---------------------------		
if($requestType=='approve'){
	
		$budg_location			= $_REQUEST['LocId'];
		$budg_department		= $_REQUEST['DepId'];
		$budg_year				= $_REQUEST['YearId'];
		
	
		$db->begin();
		
		$res_c					= $obj_common->loadCompany_for_location($_REQUEST['LocId'],'RunQuery2');
		$row_company			= mysqli_fetch_array($res_c);
		$budg_company			= $row_company['intCompanyId'];
	
		$rollBack_flag 			= 0;
		$rollBack_msg			= '';
		$rollBack_sql			= '';
	
		$result					= $obj_comm_budget_get->loadFinanceYear_with_selected_date($budg_company,date('Y-m-d'),'RunQuery2');
		$data					= mysqli_fetch_array($result);
		$curr_budget_year		= $data['intId'];
		$header_array			= $obj_budget_get->get_header($budg_company,$budg_location,$budg_department,$budg_year,'RunQuery2');
		$permition_arr			= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$userId,$programCode,'RunQuery2');
		$permision_confirm		= $permition_arr['permision']; 
		
		$next_budg_year			= $obj_comm_budget_get->check_for_approved_next_records($budg_location,$budg_department,$budg_year,'RunQuery2');
		
		//VALIDATION--------------------------------------
		//1. check for approval permision
 		if($permition_arr['type']  != 'pass'){
			$rollBack_flag		=	1;
			$rollBack_msg	 	=	$permition_arr['msg'];	
			$rollBack_sql		=	'';
 		}
		//2. check for financial year
 		else if($next_budg_year  != ''){
			$rollBack_flag		=	1;
			$rollBack_msg	 	=	"Can't approve.There is a approved budget for next finance year";	
			$rollBack_sql		=	'';
 		}
		//3. check for budget balance
		if($rollBack_flag != 1){
			$budget_details_result	= $obj_budget_get->getdetails($budg_company,$budg_location,$budg_department,$budg_year,'RunQuery2');
			while($row=mysqli_fetch_array($budget_details_result)){
				$month			= $row['MONTH'];
				$month_name		= $obj_common->get_month_name($month,'RunQuery2');
				$sub_cat		= $row['SUB_CATEGORY_ID'];
				$sub_cat_name	= $row['sub_cat_name'];
				$item_id		= $row['ITEM_ID'];
				$item_name		= $row['ITEM_NAME'];
				$budget_type	= $obj_comm_budget_get->load_budget_type($budg_location,$sub_cat,$item_id,'RunQuery2');
				$balance_arr	= $obj_comm_budget_get->load_budget_balance($budg_location,$budg_department,$budg_year,$sub_cat,$item_id,$month,'RunQuery2');
				//print_r($balance_arr);
				if($budget_type=='U')
					$budget_balance	= $balance_arr['balUnits'];
				else
					$budget_balance	= $balance_arr['balAmount']; 
				if($budget_balance  < 0 ){
					$rollBack_flag		=	1;
					if($item_id == '' || $item_id ==0)
					$rollBack_msg		.=	"'Used or transfered total budget' grater than the 'budget' in month '$month_name' and sub category '$sub_cat_name'"."</br>";	
					else
					$rollBack_msg		.=	"'Used or transfered total budget' grater than the 'budget' in month '$month_name' and sub category '$sub_cat_name' and item '$item_name'"."</br>";	
					$rollBack_sql		=	'';
				}
			}
		}
		//$rollBack_flag	=1;
		//SAVING--------------------------------------
		if($rollBack_flag != 1){
		//4. update header status
			$status				= $header_array['STATUS']-1;
			$response_1			= $obj_budget_set->update_header_status($budg_location,$budg_department,$budg_year,$status,'RunQuery2');
			if($response_1['type']	 != 'pass'){
				$rollBack_flag	=1;
				$rollBack_msg	=$response_1['msg'];
				$rollBack_sql	=$response_1['sql'];
			}
		}
		if($rollBack_flag != 1){
		//5. approve by insert
			if($rollBack_flag != 1){
				$header_array	= $obj_budget_get->get_header($budg_company,$budg_location,$budg_department,$budg_year,'RunQuery2');
				$levels			= $header_array['LEVELS']+1-$header_array['STATUS'];
				$response_2		= $obj_budget_set->approved_by_insert($budg_location,$budg_department,$budg_year,$userId,$levels,'RunQuery2');
				if($response_2['type']	 != 'pass'){
					$rollBack_flag		=1;
					$rollBack_msg		=$response_2['msg'];
					$rollBack_sql		=$response_2['sql'];
				}
			}
		}
   
		$header_array	= $obj_budget_get->get_header($budg_company,$budg_location,$budg_department,$budg_year,'RunQuery2');
	
	
		//$rollBack_flag = 1;

		if($rollBack_flag==1){
			$db->rollback();
			$response['type'] 				= 'fail';
			$response['msg'] 				= $rollBack_msg;
			$response['q'] 					= $rollBack_sql;
		}
		else if($header_array['STATUS'] ==1 ){
			$db->commit();
			$response['type'] 				= 'pass';
			$response['msg'] 				= 'Final Approval Raised successfully.';
		}
		else if($header_array['STATUS'] > 1  && $header_array['STATUS'] <= $header_array['LEVELS']){
			$db->commit();
			$response['type'] 				= 'pass';
			$response['msg'] 				= 'Approve level '.($header_array['LEVELS']+1-$header_array['STATUS']).' raised successfully.';
		}
		else{
			$db->rollback();
			$response['type'] 				= 'fail';
			$response['msg'] 				= $db->errormsg;
			$response['q'] 					= $sql;
		}
	
		$db->CloseConnection();		
		echo json_encode($response);
	
	}
	else if($requestType=='reject'){
		
		$rollBack_flag 			= 0;
		$rollBack_msg			= '';
		$rollBack_sql			= '';
		
		$budg_location			= $_REQUEST['LocId'];
		$budg_department		= $_REQUEST['DepId'];
		$budg_year				= $_REQUEST['YearId'];
		
		$db->begin();
	
		$res_c					= $obj_common->loadCompany_for_location($_REQUEST['LocId'],'RunQuery2');
		$row_company			= mysqli_fetch_array($res_c);
		$budg_company			= $row_company['intCompanyId'];
	
		$header_array			= $obj_budget_get->get_header($budg_company,$budg_location,$budg_department,$budg_year,'RunQuery2');
		$permition_arr			= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['LEVELS'],$userId,$programCode,'RunQuery2');
		$permision_confirm		= $permition_arr['permision']; 
		//--------------------------
		//$permition_arr['type'];
		if($permition_arr['type']  != 'pass'){
			$rollBack_flag		=	1;
			$rollBack_msg	 	=	$permition_arr['msg'];	
			$rollBack_sql		=	'';
 		}
		
		if($rollBack_flag != 1){
		//1. update header status
			$status				=0;
			$response_1			= $obj_budget_set->update_header_status($budg_location,$budg_department,$budg_year,$status,'RunQuery2');
			if($response_1['type']	 != 'pass'){
				$rollBack_flag	=1;
				$rollBack_msg	=$response_1['msg'];
				$rollBack_sql	=$response_1['sql'];
			}
		}
			
		if($rollBack_flag != 1){
		//2. approve by insert
			if($rollBack_flag != 1){
				$response_2		= $obj_budget_set->approved_by_insert($budg_location,$budg_department,$budg_year,$userId,0,'RunQuery2');
				if($response_2['type']	 != 'pass'){
					$rollBack_flag		=1;
					$rollBack_msg		.=$response_2['msg'];
					$rollBack_sql		.=$response_2['sql'];
				}
			}
		}
		
		if($rollBack_flag==1){
			$db->rollback();
			$response['type'] 				= 'fail';
			$response['msg'] 				= $rollBack_msg;
			$response['q'] 					= $rollBack_sql;
		}
		else if($toSave==$saved){
			$header_array					= $obj_budget_get->get_header($budg_company,$budg_location,$budg_department,$budg_year,'RunQuery2');
			$db->commit();
			$response['type'] 				= 'pass';
			if($header_array['STATUS']==0)
			$response['msg'] 				= 'Rejected successfully.';
			else
			$response['msg'] 				= 'Rejection Failed.';
		}
		else{
			$db->rollback();
			$response['type'] 				= 'fail';
			$response['msg'] 				= $db->errormsg;
			$response['q'] 					= $sql;
		}
	
		$db->CloseConnection();		
		echo json_encode($response);
	
	}
	else if($requestType=='revise'){
		
		$rollBack_flag 			= 0;
		$rollBack_msg			= '';
		$rollBack_sql			= '';
		
		$budg_location			= $_REQUEST['LocId'];
		$budg_department		= $_REQUEST['DepId'];
		$budg_year				= $_REQUEST['YearId'];
		
		$db->begin();
	
		$res_c					= $obj_common->loadCompany_for_location($_REQUEST['LocId'],'RunQuery2');
		$row_company			= mysqli_fetch_array($res_c);
		$budg_company			= $row_company['intCompanyId'];
	
		$header_array			= $obj_budget_get->get_header($budg_company,$budg_location,$budg_department,$budg_year,'RunQuery2');
		$permition_arr			= $obj_commonErr->get_permision_withApproval_revise($header_array['STATUS'],$header_array['LEVELS'],$userId,$programCode,'RunQuery2');
		$permision_confirm		= $permition_arr['permision']; 
		//--------------------------
		//$permition_arr['type'];
		if($permition_arr['type']  != 'pass'){
			$rollBack_flag		=	1;
			$rollBack_msg	 	=	$permition_arr['msg'];	
			$rollBack_sql		=	'';
 		}
		
		if($rollBack_flag != 1){
		//1. update header status
			$status				=-1;
			$response_1			= $obj_budget_set->update_header_status($budg_location,$budg_department,$budg_year,$status,'RunQuery2');
			if($response_1['type']	 != 'pass'){
				$rollBack_flag	=1;
				$rollBack_msg	=$response_1['msg'];
				$rollBack_sql	=$response_1['sql'];
			}
		}
			
		if($rollBack_flag != 1){
		//2. approve by insert
			if($rollBack_flag != 1){
				$response_2		= $obj_budget_set->approved_by_insert($budg_location,$budg_department,$budg_year,$userId,-1,'RunQuery2');
				if($response_2['type']	 != 'pass'){
					$rollBack_flag		=1;
					$rollBack_msg		.=$response_2['msg'];
					$rollBack_sql		.=$response_2['sql'];
				}
			}
		}
		//3. insert to history
		$next_history_id		= $obj_budget_get->get_next_history_id($budg_location,$budg_department,$budg_year);
		
		$budget_details_result	= $obj_budget_get->getdetails($budg_company,$budg_location,$budg_department,$budg_year,'RunQuery2');
		/*while($row=mysqli_fetch_array($budget_details_result)){
			$month			= $row['MONTH'];
			$sub_cat		= $row['SUB_CATEGORY_ID'];
			$sub_cat_name	= $row['sub_cat_name'];
			*/if($rollBack_flag != 1){
				$response_3		= $obj_budget_set->insertHistoryDetails($budg_location,$budg_department,$budg_year,$next_history_id,'RunQuery2');
				if($response_3['type']	 != 'pass'){
					$rollBack_flag		=1;
					$rollBack_msg		.=$response_3['msg'];
					$rollBack_sql		.=$response_3['sql'];
				}
			}
		//}
		
		if($rollBack_flag==1){
			$db->rollback();
			$response['type'] 				= 'fail';
			$response['msg'] 				= $rollBack_msg;
			$response['q'] 					= $rollBack_sql;
		}
		else if($toSave==$saved){
			$header_array					= $obj_budget_get->get_header($budg_company,$budg_location,$budg_department,$budg_year,'RunQuery2');
			$db->commit();
			$response['type'] 				= 'pass';
			if($header_array['STATUS']==-1)
			$response['msg'] 				= 'Revised successfully.';
			else
			$response['msg'] 				= 'Revision Failed.';
		}
		else{
			$db->rollback();
			$response['type'] 				= 'fail';
			$response['msg'] 				= $db->errormsg;
			$response['q'] 					= $sql;
		}
	
		$db->CloseConnection();		
		echo json_encode($response);
	
	}
else if($requestType=='loadFinanceMonths')
{
	$financeYearId		= $_REQUEST['financeYear'];
	
	$financeMonth_arr	= $obj_budget_get->getFinanceMonths($financeYearId);
	$startMonth			= $financeMonth_arr['startMonth'];
	$endMonth			= 12+$financeMonth_arr['endMonth'];	
	$html				= '';
	
	if($financeMonth_arr['startYear']==$financeMonth_arr['endYear'])
		$endMonth		= $financeMonth_arr['endMonth'];
	
	$html .="<tr>";
	$html .="<th style=\"width:250px;\" rowspan=\"2\">Sub Category / Item</th>";
	$html .="<th style=\"width:50px;\" rowspan=\"2\">Unit</th>";
	
	for($i=$startMonth;$i<=$endMonth;$i++)
	{
		$html .="<th colspan=\"2\" style=\"width:200px;\">".date('M', mktime(0,0,0,$i,1)).' - '.($i>12?$financeMonth_arr['endYear']:$financeMonth_arr['startYear'])."</th>";
	}
	$html .="</tr>";
	for($i=$startMonth;$i<=$endMonth;$i++)
	{
		$html .="<th style=\"width:100px;\">Amount</th>";
		$html .="<th style=\"width:100px;\">Units</th>";
	}
	$response['financeMonths'] 	= $html;
	echo json_encode($response);
}
/*else if($requestType=="loadDetail")
{
	$locationId		= $_REQUEST['location'];
	$department		= $_REQUEST['department'];
	$financeYear	= $_REQUEST['financeYear'];
	
	$subCat_result	= $obj_budget_get->getSubCategory_sql($company,$locationId);
	while($row = mysqli_fetch_array($subCat_result))
	{
		$data['subCatId'] 	 	= $row['subCatID'];
		$data['subCatName'] 	= $row['SubCategory'];
		$data['budgetDetail']	= getBudgetDetails($locationId,$department,$financeYear,$row['subCatID']);
		
		$arrDetailData[] 		= $data;	
	}
	return $arrDetailData;
}*/
else if($requestType=="saveData")
{
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	$arrDetails 		= json_decode($_REQUEST['arrDetails'],true);
	
	$db->begin();
		
	$locationID			= $arrHeader['location'];
	$department			= $arrHeader['department'];
	$financeYear		= $arrHeader['financeYear'];
	$editMode			= false;
	
	$validateArr		= validateBeforeSave($locationID,$department,$financeYear,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	
	$header_arr			= $obj_budget_get->get_header('',$locationID,$department,$financeYear,'RunQuery2');
	$status				= $header_arr['STATUS'];
	$approveLevels		= $header_arr['LEVELS'];
	
	if($header_arr['LOCATION_ID']=='')
	{
		$approveLevels 		= $obj_common->getApproveLevels($programName);
		$status				= $approveLevels+1;
		
		$resultHArr			= $obj_budget_set->saveHeader($locationID,$department,$financeYear,$userId,$status,$approveLevels);
		if($resultHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultHArr['savedMassege'];
			$error_sql		= $resultHArr['error_sql'];	
		}
	}
	else
	{
		$status				= $header_arr['STATUS'];
		$approveLevels		= $header_arr['LEVELS'];
		$approveLevels 		= $obj_common->getApproveLevels($programName);
		$status				= $approveLevels+1;
		
		$resultUHArr		= $obj_budget_set->updateHeader2($locationID,$department,$financeYear,$userId,$status,$approveLevels);
		if($resultUHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultUHArr['savedMassege'];
			$error_sql		= $resultUHArr['error_sql'];	
		}
		
		$resultUMArr		= updateMaxStatus($locationID,$department,$financeYear);
		if($resultUMArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultUMArr['savedMassege'];
			$error_sql		= $resultUMArr['error_sql'];	
		}
		$editMode			= true;
	}
	foreach($arrDetails as $array_loop)
	{
		$subcatArr			= explode('~',$array_loop['subcatArr']);
		$subCatId			= $subcatArr[0];
		$itemId				= $subcatArr[1];
		$month				= $array_loop['month'];
		$budgetVal			= ($array_loop['budgetVal']==''?0:$array_loop['budgetVal']);
		$type				= $array_loop['type'];
		
		$validateBalArr	= validateBalanceBudget($locationID,$department,$financeYear,$subCatId,$itemId,$month,$budgetVal,$type);
		if($validateBalArr['status']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $validateBalArr['msg'];
		}
		
		$checkBudPlan		= $obj_budget_get->checkBudgetPlanAvailable($locationID,$department,$financeYear,$subCatId,$itemId,$month);
		if($checkBudPlan)
		{
			$resultDUArr	= $obj_budget_set->updateDetails($locationID,$department,$financeYear,$subCatId,$itemId,$month,$budgetVal,$type);
			if($resultDUArr['savedStatus']=='fail' && $savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged 	= $resultDUArr['savedMassege'];
				$error_sql		= $resultDUArr['error_sql'];	
			}
		}
		else
		{
			if($budgetVal!=0)
			{
				$resultDArr		= $obj_budget_set->saveDetails($locationID,$department,$financeYear,$subCatId,$itemId,$month,$budgetVal,$type);
				if($resultDArr['savedStatus']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged 	= $resultDArr['savedMassege'];
					$error_sql		= $resultDArr['error_sql'];	
				}
			}
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode)
			$response['msg'] 		= "Updated Successfully.";
		else
			$response['msg'] 		= "Saved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= $savedMasseged;
		$response['sql']			= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=="loadBudgetDetail")
{
	$locationID			= $_REQUEST['location'];
	$department			= $_REQUEST['department'];
	$financeYear		= $_REQUEST['financeYear'];
	$html				= '';
	
	$financeMonth_arr	= $obj_budget_get->getFinanceMonths($financeYear);
	$startMonth			= $financeMonth_arr['startMonth'];
	$endMonth			= 12+$financeMonth_arr['endMonth'];	
	
	if($financeMonth_arr['startYear']==$financeMonth_arr['endYear'])
		$endMonth		= $financeMonth_arr['endMonth'];
	
	$subCat_result		= $obj_budget_get->getSubCategory($company,$locationID);
	while($row = mysqli_fetch_array($subCat_result))
	{
		$html .="<tr class=\"normalfnt\" id=\"".($row['itemCount']>0?0:$row['SUB_CATEGORY_ID'].'~0')."\">";
		$html .="<td style=\"text-align:left\" class=\"clsSubcatItem\">".$row['subCatName']."</td>";
		$html .="<td style=\"text-align:center\">&nbsp;</td>";
		
		for($i=$startMonth;$i<=$endMonth;$i++)
		{
			$month	= $i;
			if($i>12)
				$month = $i-12;
			$allocateBudgetArr = $obj_budget_get->getAllocatedBudget($locationID,$department,$financeYear,$month,$row['SUB_CATEGORY_ID'],0);
			
			$html .="<td id=\"td_amount\">".($row['itemCount']>0?"&nbsp;":"<input type=\"textbox\" name=\"txtAmount\" id=\"".$month."\" class=\"clsBudget clsAmount validate[custom[number]]\" style=\"width:100%;text-align:right\" value=\"".($allocateBudgetArr['INITIAL_BUDGET'])."\" ".($row['BUDGET_TYPE']=='A'?'':'disabled="disabled"').">")."</td>";
			$html .="<td id=\"td_unit\">".($row['itemCount']>0?"&nbsp;":"<input type=\"textbox\" name=\"txtUnit\" id=\"".$month."\" class=\"clsBudget clsUnit validate[custom[number]]\" style=\"width:100%;text-align:right\" value=\"".($allocateBudgetArr['INITIAL_BUDGET_UNITS'])."\" ".($row['BUDGET_TYPE']=='U'?'':'disabled="disabled"').">")."</td>";
		}
		$html .="</tr>";
		
		if($row['itemCount']>0)
		{
			$item_result = $obj_budget_get->getSubCatItem($company,$locationID,$row['SUB_CATEGORY_ID']);
			while($row2 = mysqli_fetch_array($item_result))
			{
				$html .="<tr class=\"normalfnt\" id=\"".$row['SUB_CATEGORY_ID'].'~'.$row2['ITEM_ID']."\">";
				$html .="<td style=\"text-align:right\" class=\"clsSubcatItem\">".$row2['itemName']."</td>";
				$html .="<td style=\"text-align:center\">".$row2['unit']."</td>";
				
				for($i=$startMonth;$i<=$endMonth;$i++)
				{
					$month	= $i;
					if($i>12)
						$month = $i-12;
					$allocateBudgetArr = $obj_budget_get->getAllocatedBudget($locationID,$department,$financeYear,$month,$row['SUB_CATEGORY_ID'],$row2['ITEM_ID']);
					$html .="<td id=\"td_amount\"><input type=\"textbox\" name=\"txtAmount\" id=\"".$month."\" class=\"clsBudget clsAmount validate[custom[number]]\" style=\"width:100%;text-align:right\" value=\"".$allocateBudgetArr['INITIAL_BUDGET']."\" ".($row2['BUDGET_TYPE']=='A'?'':'disabled="disabled"')." /></td>";
					$html .="<td id=\"td_unit\"><input type=\"textbox\" name=\"txtUnit\" id=\"".$month."\" class=\"clsBudget clsUnit validate[custom[number]]\" style=\"width:100%;text-align:right\" value=\"".$allocateBudgetArr['INITIAL_BUDGET_UNITS']."\" ".($row2['BUDGET_TYPE']=='U'?'':'disabled="disabled"')."/></td>";
				}
			}
		}
	}
	$response['gridDetail'] = $html;
	echo json_encode($response);
}
function getBudgetDetails($locationId,$department,$financeYearId,$subCatId)
{
	global $db;
	global $obj_budget_get;
	
	$financeMonth_arr	= $obj_budget_get->getFinanceMonths($financeYearId);
	$startMonth			= $financeMonth_arr['startMonth'];
	$endMonth			= 12+$financeMonth_arr['endMonth'];	
	$html				= '';
	
	if($financeMonth_arr['startYear']==$financeMonth_arr['endYear'])
		$endMonth		= $financeMonth_arr['endMonth'];
	
	for($i=$startMonth;$i<=$endMonth;$i++)
	{
		$allocateBudget 	= $obj_budget_get->getAllocatedBudget($locationId,$department,$financeYearId,$i,$subCatId);
		
		$html 				= "<input type=\"textbox\" id=\"txtAmount\" class=\"clsBudgetAmount validate[custom[number]]\" style=\"width:100%;text-align:right\" value=\"".$allocateBudget."\">"; 
		
		$data['monthNo'] 	= $i;
		$data['tdValue'] 	= $html;
		
		$arrBudgetDetailData[] 	= $data;
	}
	return $arrBudgetDetailData;
}
function validateBeforeSave($location,$department,$financeYear,$programCode,$userId)
{
	global $obj_commonErr;
	global $obj_budget_get;
	global $obj_comm_budget_get;
	global $obj_common;

	$header_arr		= $obj_budget_get->get_header('',$location,$department,$financeYear,'RunQuery2');
	$next_budg_year	= $obj_comm_budget_get->check_for_approved_next_records($location,$department,$financeYear,'RunQuery2');
	
	if($next_budg_year !='')
	{
		$validateArr['msg'] 		=	"Can't save.There is a approved budget for next finance year";	
		$validateArr['type'] 		=	'fail';
		$validateArr['permision'] 	=	0;
	}
	else
	{
		$validateArr 	= $obj_commonErr->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['LEVELS'],$userId,$programCode,'RunQuery2');
		if($validateArr['type']=='fail')
		{
			$perm = $obj_common->Load_menupermision2($programCode,$userId,'intEdit');
			if($perm !=1)
			{
				$validateArr['msg'] 	= "No Permision to Edit.";	
				$validateArr['type'] 	= 'fail';
			}
			else if(($header_arr['STATUS']<=$header_arr['LEVELS']) && ($header_arr['STATUS'] != 0) && ($header_arr['STATUS'] != ''))
			{
				$validateArr['msg'] 	= "You cannot edit either previous budget plans or the current approved budget plans.";	
				$validateArr['type'] 	= 'fail';
			}
		}	
	}
	return  $validateArr;
}
function updateMaxStatus($locationID,$department,$financeYear)
{
	global $obj_budget_get;
	global $obj_budget_set;
	
	$maxStatus	= $obj_budget_get->getMaxStatus($locationID,$department,$financeYear);
	$resultArr	= $obj_budget_set->updateApproveByStatus($locationID,$department,$financeYear,$maxStatus);
	
	return $resultArr;
}
function validateBalanceBudget($locationID,$department,$financeYear,$subCatId,$itemId,$month,$budgetVal,$type)
{
	global $obj_comm_budget_get;
	$chkStatus	= true;
	
	$balanceBudgetArr	= $obj_comm_budget_get->load_budget_balance($locationID,$department,$financeYear,$subCatId,$itemId,$month,'RunQuery2');
	
	if($type=='A')
	{
		if($budgetVal<$balanceBudgetArr['balAmount'] && $chkStatus)
		{
			$chkStatus 		= false;
			$data['status']	= 'fail';
			$data['msg']	= "Entered 'Amount' cannot exceed balance 'Amount'.";
		}
	}
	if($type=='U')
	{
		if($budgetVal<$balanceBudgetArr['balUnits'] && $chkStatus)
		{
			$chkStatus 		= false;
			$data['status']	= 'fail';
			$data['msg']	= "Entered 'Unit Amount' cannot exceed balance 'Unit Amount'.";
		}
	}
	
	return $data;
}
?>
$(document).ready(function(e) {
	$('#frmCategory #txtCategoryName').focus();
	$('#frmCategory #butSave').die('click').live('click',save);
	$('#frmCategory #cboSearch').die('change').live('change',searchData);
	$('#frmCategory #butNew').die('click').live('click',clearForm);
	$('#frmCategory #butDelete').die('click').live('click',deleteRecord);
});

function save()
{
	if(!$("#frmCategory").validationEngine('validate'))
		return;
	
	showWaiting();
		
	var url 	 	= 'controller.php?q=1116&RequestType=URLSave';
	
	var data	    = "{";
		data       += '"Search":"'+$('#frmCategory #cboSearch').val()+'",';
		data	   += '"CategoryName":'+URLEncode_json($('#frmCategory #txtCategoryName').val())+',';
		data       += '"OrderBy":"'+$('#frmCategory #cboOrderBy').val()+'",';
		data       += '"Active":"'+$('#frmCategory #chkActive').is(':checked')+'"';
		data	   += "}";
		
		
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',
		data:"HeaderArray="+data,
		async:false,	
		success:function(json){
			$('#frmCategory #butSave').validationEngine('showPrompt',json.msg,json.type);
			$('#frmCategory #cboSearch').html(json.html);
			clearForm();
			hideWaiting();
		},
		error:function(xhr,status){
			$('#frmCategory #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			hideWaiting();
		}		
	});
}

function searchData()
{
	var url 	= 'controller.php?q=1116&RequestType=URLSearch';
	
	var data	= 'Search='+$('#frmCategory #cboSearch').val();
	
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',
		data:data,
		async:false,	
		success:function(json){
			$('#frmCategory #txtCategoryName').val(json.CategoryName);
			$('#frmCategory #cboOrderBy').val(json.OrderBy);
			$('#frmCategory #chkActive').prop('checked',json.Status);
			hideWaiting();
		},
		error:function(xhr,status){
			$('#frmCategory #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			hideWaiting();
		}		
	});
}

function clearForm()
{	
	$('#frmCategory input').val('');
	$('#frmCategory select').val('');
	$('#frmCategory #txtCategoryName').focus();
	$('#frmCategory #chkActive').prop('checked',true);
}

function autoHideAlertBox(obj)
{
	$(obj).validationEngine('hide');
}

function deleteRecord()
{
	if($('#frmCategory #cboSearch').val().trim()=='')
	{
		$('#frmCategory #butDelete').validationEngine('showPrompt','Please select the category.','fail');
		$('#frmCategory #cboSearch').focus();
		var t = setTimeout("autoHideAlertBox('#frmCategory #butDelete')",4000);
		return;
	}
	var obj =	this;
	var val = $.prompt('Are you sure you want to delete this ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(!v)
				return;			
		
			showWaiting();
			var url 	= 'controller.php?q=1116&RequestType=URLDelete';
			
			var data	= 'Search='+$('#frmCategory #cboSearch').val();
			
			var obj = $.ajax({
				url:url,
				dataType: "json",
				type:'POST',
				data:data,
				async:false,	
				success:function(json){
					$('#frmCategory #butDelete').validationEngine('showPrompt',json.msg,json.type);
					var t = setTimeout("autoHideAlertBox('#frmCategory #butDelete')",2000);
					$('#frmCategory #cboSearch').html(json.html);
					clearForm();
					hideWaiting();
				},
				error:function(xhr,status){
					$('#frmCategory #butDelete').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					hideWaiting();
				}		
				});
		}
	});
}
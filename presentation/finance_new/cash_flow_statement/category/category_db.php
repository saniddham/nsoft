<?php 
try{
##########################################################################################################
## INCLUDE FILES AND CREATE OBJECTS 
require_once "class/tables/finance_cashflow_statement_cpanal_category.php"; 	$finance_cashflow_statement_cpanal_category		= new finance_cashflow_statement_cpanal_category($db);
require_once "class/tables/menupermision.php";									$menupermision 									= new menupermision($db);
require_once "class/dateTime.php";												$dateTimes										= new dateTimes($db);
##########################################################################################################

$requestType	= $_REQUEST['RequestType'];
$programCode	= 'P1116';

if($requestType == 'URLSave')
{
	$headerArray	= json_decode($_REQUEST['HeaderArray'],true);
	$status			= $headerArray['Active']=='true'?1:0;
	$db->begin();
	
##########################################################################################################
## 1. CHECK SAVE PERMISSION.          																	##
##########################################################################################################
	$menupermision->set($programCode,$sessions->getUserId());	
	$result	= $menupermision->checkMenuPermision('Edit','','');
	if(!$result['type'])
		throw new Exception($result['msg']);
##########################################################################################################

if($headerArray['Search']=="")
{
##########################################################################################################
## 2. INSERT INTO `finance_cashflow_statement_cpanal_category` TABLE 									##
##########################################################################################################
	$result = $finance_cashflow_statement_cpanal_category->insertRec($headerArray['CategoryName'],$headerArray['OrderBy'],$status,$sessions->getUserId(),$dateTimes->getCurruntDateTime(),'NULL','NULL');	
	if(!$result['status'])
		throw new Exception($result['msg']);
##########################################################################################################
}
else
{
##########################################################################################################
## 2. UPDATE `finance_cashflow_statement_cpanal_category` TABLE 									##
##########################################################################################################
	$finance_cashflow_statement_cpanal_category->set($headerArray['Search']);
	$finance_cashflow_statement_cpanal_category->setREPORT_CATEGORY_NAME($headerArray['CategoryName']);
	$finance_cashflow_statement_cpanal_category->setORDER_BY_ID($headerArray['OrderBy']);
	$finance_cashflow_statement_cpanal_category->setSTATUS($status);
	$finance_cashflow_statement_cpanal_category->commit();
##########################################################################################################
}
	$response['type'] 			= 'pass';
	$response['msg']			= "Successfully saved.";
	$response['html']			= $finance_cashflow_statement_cpanal_category->getCombo();
	
	$db->commit();$db->disconnect();
	echo json_encode($response);
}
elseif($requestType=='URLSearch')
{
	$db->connect();
	$categoryId		= $_REQUEST['Search'];
	$finance_cashflow_statement_cpanal_category->set($categoryId);
	
	$response['CategoryName']	= $finance_cashflow_statement_cpanal_category->getREPORT_CATEGORY_NAME();
	$response['OrderBy']		= $finance_cashflow_statement_cpanal_category->getORDER_BY_ID();
	$response['Status']			= $finance_cashflow_statement_cpanal_category->getSTATUS()==1?true:false;
	
	$db->disconnect();
	echo json_encode($response);
}
elseif($requestType=='URLDelete')
{
	$db->begin();
	$categoryId		= $_REQUEST['Search'];
	
	$where		= 'REPORT_CATEGORY_ID = "'.$categoryId.'"';	
	$result = $finance_cashflow_statement_cpanal_category->delete($where);
	if(!$result['status'])
		throw new Exception($result['msg']);
	
	$response['type'] 			= 'pass';
	$response['msg']			= "Successfully deleted.";
	$response['html']			= $finance_cashflow_statement_cpanal_category->getCombo();
	
	$db->commit();$db->disconnect();
	echo json_encode($response);
}
}catch(Exception $e){
	$db->rollback();$db->disconnect();
	$response['msg'] 	=  $e->getMessage();;
	$response['error'] 	=  $error_handler->jTraceEx($e);
	$response['type']	=  'fail';
	$response['sql']	=  $db->getSql();
	echo json_encode($response);
}
?>
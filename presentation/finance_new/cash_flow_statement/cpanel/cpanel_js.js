$(document).ready(function(e) {
	try
	{
		$("#frmCPanel .chosen-select").chosen({disable_search_threshold:25,allow_single_deselect: true , no_results_text: "Oops, nothing found!"});
	}
	catch(err){}
    $('#frmCPanel #cboCategory').die('change').live('change',submitPage);
	$('#frmCPanel #butInsertRow').die('click').live('click',addNewRow);
	$('#frmCPanel #butSave').die('click').live('click',save);
	$('#frmCPanel .cls_remove').die('click').live('click',remove);
});

function remove()
{
	var obj =	this;
	var val = $.prompt('Are you sure you want to delete this ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						$(obj).parent().parent().remove();
					}
				}
		});
}

function submitPage()
{
	$('#frmCPanel').submit();
}

function addNewRow()
{	
	var url 	 	= 'controller.php?q=1115&RequestType=URLAddNewRow';
	
	var data		= 'Category='+$('#frmCPanel #cboCategory').val();
		data       += '&RowCount='+$('#frmCPanel #tblMain tbody tr').length;

	var obj = $.ajax({
	url:url,
	dataType: "json",
	type:'POST',
	data:data,
	async:false,	
	success:function(json){
			$('#frmCPanel #tblMain tbody').append(json.html);
			$("#frmCPanel .chosen-select").chosen({allow_single_deselect: true , no_results_text: "Oops, nothing found!"});
		},
	error:function(xhr,status){
			$('#frmCPanel #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
		}		
	});
	
}

function save()
{
	if(!$("#frmCPanel").validationEngine('validate'))
		return;
	
	showWaiting();
	var booError = true;
	var msg		 = '';
	var detailArray = "["
	$('#frmCPanel #tblMain tbody tr').each(function(index, element) {
		if($(this).find('.cls_tr_gl').children().val()==null){
			booError	= false;
			msg			= 'Chart of account field is required.';
			return;
		}
		if($(this).find('.cls_tr_programType').children().val()==''){
			booError	= false;
			msg			= 'Program type field is required.';
			return;
		}
        detailArray += '{"OrderBy":"'+$(this).find('.cls_td_orderBy').children().val()+'",';
		detailArray += '"ReportName":'+URLEncode_json($(this).find('.cls_tr_reportName').children().val())+',';
		detailArray += '"GL":"'+$(this).find('.cls_tr_gl').children().val()+'",';
		detailArray += '"ProgramType":"'+$(this).find('.cls_tr_programType').children().val()+'"';
		detailArray += '},';
    });
	
	if(!booError){
		$('#frmCPanel #butSave').validationEngine('showPrompt',msg,'fail');
		hideWaiting();
		return false;
	}
	detailArray 		= detailArray.substr(0,detailArray.length-1);detailArray += "]";
	
	var url 	 	= 'controller.php?q=1115&RequestType=URLSave';
	
	var data		= "CategoryName="+$('#frmCPanel #cboCategory').val();
		data       += "&DetailArray="+detailArray;
		
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',
		data:data,
		async:false,	
		success:function(json){
			$('#frmCPanel #butSave').validationEngine('showPrompt',json.msg,json.type);
			if(json.type=='pass')
				submitPage();
			
			
			hideWaiting();
		},
		error:function(xhr,status){
			$('#frmCPanel #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			hideWaiting();
		}		
	});
}

function loadMain()
{
	$("#butViewCategory").die('click').live('click',popupCategory);
}

function popupCategory()
{
	popupWindow2('1','#cboSearch','#cboCategory','#frmCPanel');
}
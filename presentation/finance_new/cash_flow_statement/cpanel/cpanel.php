<?php
#########################################################################################################################
## INCLUDE FILES 
require_once "class/tables/finance_cashflow_statement_cpanal_category.php"; 	$finance_cashflow_statement_cpanal_category		= new finance_cashflow_statement_cpanal_category($db);
require_once "class/tables/finance_cashflow_statement_cpanal.php";				$finance_cashflow_statement_cpanal				= new finance_cashflow_statement_cpanal($db);
require_once "class/tables/finance_mst_chartofaccount.php";						$finance_mst_chartofaccount						= new finance_mst_chartofaccount($db);
require_once "class/tables/finance_mst_transaction_type.php";					$finance_mst_transaction_type					= new finance_mst_transaction_type($db);
#########################################################################################################################

$maxCount	= $finance_cashflow_statement_cpanal->getMaxRowCount($_REQUEST['cboCategory']);
?>
<title>Finance Cash Flow CPanel</title>
</head><body>
<form id="frmCPanel" name="frmCPanel" autocomplete="off" action="?q=1115" method="post">
  <div align="center">
    <div class="trans_layoutXL">
      <div class="trans_text">Finance Cash Flow Statement Configuration Panel</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="15%" class="normalfnt">Cash Flow Category</td>
                <td width="85%"><select name="cboCategory" id="cboCategory" style="width:400px;">
                    <?php echo $finance_cashflow_statement_cpanal_category->getCombo($_REQUEST['cboCategory'])?>
                  </select>
                  <img src="images/add_new.png" alt="addItem" name="butViewCategory" class="mouseover" id="butViewCategory"  /></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><table width="100%" border="0" class="bordered" id="tblMain">
              <thead>
                <tr>
                  <th colspan="5">Configuration Panel
                    <div style="float:right"><a class="button white small" id="butInsertRow">Add New Row</a></div></th>
                </tr>
                <tr>
                  <th>Remove</th>
                  <th>Report Name</th>
                  <th>Chart Of Account</th>
                  <th>Program Type</th>
                  <th>Order By</th>
                </tr>
              </thead>
              <tbody>
                <?php
              $result 		= $finance_cashflow_statement_cpanal->getDetails($_REQUEST['cboCategory']);
			  $rowCount		= mysqli_num_rows($result);
			  while($row = mysqli_fetch_array($result))
			  {
			  ?>
                <tr>
                  <td width="2%" style="text-align:center" class="cls_td_orderBy1" ><div class="cls_remove button red medium">Remove</div></td>
                  <td width="27%" class="cls_tr_reportName"><input type="text" id="txtReportName" name="txtReportName" value="<?php echo $row['REPORT_FIELD_NAME']?>" style="width:320px;height:30px" class="validate[required]"></td>
                  <td width="51%" class="cls_tr_gl"><select name="cboChartOfAccount" id="cboChartOfAccount" style="width:500px" class="chosen-select validate[required]" multiple>
                      <option value=""></option>
                      <?php
				$id_arr 	= explode(",",$row['CHART_OF_ACCOUNT_ID']);
				$result1 	= $finance_mst_chartofaccount->getFilterdAccountComboReturnResult(1,'N');				  
				$i			= 0;
				while($row1=mysqli_fetch_array($result1))
				{
					if(in_array($row1['CHART_OF_ACCOUNT_ID'], $id_arr))
						echo "<option value=\"".$row1['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row1['CHART_OF_ACCOUNT_NAME']."</option>";					
					else
						echo "<option value=\"".$row1['CHART_OF_ACCOUNT_ID']."\">".$row1['CHART_OF_ACCOUNT_NAME']."</option>";					
					
					$i++;
				}
				  ?>
                    </select></td>
                  <td width="20%" class="cls_tr_programType"><select name="cboProgramType" id="cboProgramType" style="width:250px" class="chosen-select validate[required]">
                      <option value="">&nbsp;</option>
                      <?php
				  echo $finance_mst_transaction_type->getCombo($row['TRANSACTION_PROGRAM_ID']);				  
				  ?>
                    </select></td>
                  <td width="2%" style="text-align:center" class="cls_td_orderBy" ><select name="cboOrderBy" id="cboOrderBy" style="width:80px" class="chosen-select  validate[required]">
                      <?php
				  for($r=1;$r<=$maxCount;$r++)
				  {
					  if($row['ORDER_BY_ID']==$r)
					  	echo "<option value=\"".$r."\" selected=\"selected\">".$r."</option>";
					  else
					  	echo "<option value=\"".$r."\">".$r."</option>";
				  }
				  ?>
                    </select></td>
                </tr>
                <?php
			  }
			  ?>
              </tbody>
            </table></td>
        </tr>
        <tr>
          <td style="text-align:center">&nbsp;</td>
        </tr>
        <tr>
          <td height="32" style="text-align:center"><a href="?q=1115" class="button white" id="butNew">New</a><a class="button white" id="butSave">Save</a><a href="main.php" class="button white" id="butClose">Close</a></td>
        </tr>
      </table>
    </div>
  </div>
</form>
</body>
</html><div  style="position: absolute;display:none;z-index:100"  id="popupContact1">
  <?php 
	
	 ?>
  <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="header_db.php?q=1116&iframe=1&clearCash=1" style="width:650px;height:260px;border:0;overflow:hidden"></iframe>
</div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
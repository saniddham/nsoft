<?PHP
try
{
$programCode = 'P1115';
##########################################################################################################
## INCLUDE FILES & CREATE CLASS OBJECTS 																##
##########################################################################################################
require_once "class/tables/finance_cashflow_statement_cpanal.php";		$finance_cashflow_statement_cpanal		= new finance_cashflow_statement_cpanal($db);
require_once "class/dateTime.php";										$dateTimes								= new dateTimes($db);
require_once "class/tables/menupermision.php";							$menupermision 							= new menupermision($db);
require_once "class/tables/finance_mst_chartofaccount.php";						$finance_mst_chartofaccount						= new finance_mst_chartofaccount($db);
require_once "class/tables/finance_mst_transaction_type.php";					$finance_mst_transaction_type					= new finance_mst_transaction_type($db);

##########################################################################################################
$requestType		= $_REQUEST["RequestType"];
	
if($requestType=="URLSave")
{
	$category			= $_REQUEST['CategoryName'];
	$detailArray		= json_decode($_REQUEST['DetailArray'],true);

	$db->begin();

##########################################################################################################
## 1. CHECK SAVE PERMISSION.          																	##
##########################################################################################################
$menupermision->set($programCode,$sessions->getUserId());	
$result	= $menupermision->checkMenuPermision('Edit','','');
if(!$result['type'])
	throw new Exception($result['msg']);
##########################################################################################################

##########################################################################################################
## 2. DELETE `finance_cashflow_statement_cpanal` TABLE.													##
##########################################################################################################
$where		= 'REPORT_CATEGORY_ID = "'.$category.'"';	
$result = $finance_cashflow_statement_cpanal->delete($where);
if(!$result['status'])
	throw new Exception($result['msg']);
##########################################################################################################

##########################################################################################################
## 3. INSERT INTO `finance_cashflow_statement_cpanal` TABLE.											##
##########################################################################################################
foreach($detailArray as $row)
{
	$booAvailable	= true;
	$result = $finance_cashflow_statement_cpanal->insertRec($category,$row['ReportName'],$row['GL'],$row['ProgramType'],$row['OrderBy'],1,$sessions->getUserId(),$dateTimes->getCurruntDateTime(),'NULL','NULL');
	if(!$result['status'])
		throw new Exception($result['msg']);	
}
##########################################################################################################

if(!$booAvailable)
	throw new Exception('Sorry!. No detail available to proceed.');

$response['type'] 			= 'pass';
$response['msg']			= "Successfully saved.";

$db->commit();$db->disconnect();
echo json_encode($response);
}
elseif($requestType=='URLAddNewRow')
{
	//$maxCount	= $finance_cashflow_statement_cpanal->getMaxRowCount($_REQUEST['Category']);
	$maxCount	= $_REQUEST['RowCount'];
	
	$html = 	"<tr>".
		"<td width=\"2%\" style=\"text-align:center\" class=\"cls_td_remove\" ><div class=\"cls_remove button red medium\">Remove</div></td>".
		"<td width=\"27%\" class=\"cls_tr_reportName\"><input type=\"text\" id=\"txtReportName\" name=\"txtReportName\" value=\"\" style=\"width:320px;height:30px\" class=\"validate[required]\"></td>".
		"<td width=\"51%\" class=\"cls_tr_gl\"><select name=\"cboChartOfAccount\" id=\"cboChartOfAccount\" style=\"width:500px\" class=\"chosen-select validate[required]\" multiple>".
		"<option value=\"\">&nbsp;</option>";
			$result1 	= $finance_mst_chartofaccount->getFilterdAccountComboReturnResult(1,'N');				  
			while($row1=mysqli_fetch_array($result1))
			{
				$html .= "<option value=\"".$row1['CHART_OF_ACCOUNT_ID']."\">".$row1['CHART_OF_ACCOUNT_NAME']."</option>";
			}
		$html .= "</select></td>".
		"<td width=\"20%\" class=\"cls_tr_programType\"><select name=\"cboProgramType\" id=\"cboProgramType\" style=\"width:250px\" class=\"chosen-select validate[required]\">".
		"<option value=\"\">&nbsp;</option>".$finance_mst_transaction_type->getCombo($row['TRANSACTION_PROGRAM_ID'])."</select>".
		"<td width=\"2%\" style=\"text-align:center\" class=\"cls_td_orderBy\" ><select name=\"cboOrderBy\" id=\"cboOrderBy\" style=\"width:80px\" class=\"class12 chosen-select validate[required]\">";
		for($r=1;$r<=$maxCount+1;$r++)
		{
			if($r==$maxCount+1)
				$html .= "<option value=\"".$r."\" selected=\"selected\">".$r."</option>";
			else
				$html .= "<option value=\"".$r."\">".$r."</option>";
		}
	$html .= "</select></td>".
		"</td>".
	"</tr>";
	$response['html']	= $html;
	echo json_encode($response);
}
}catch(Exception $e){	
	switch ($e->getCode()){
		case 0:
			$db->rollback();		
			$response['msg'] 	=  $e->getMessage();
			$response['error'] 	=  $error_handler->jTraceEx($e);
			$response['type'] 	=  'fail';
			$response['sql']	=  $db->getSql();
			break;		
	}
	echo json_encode($response);
}
?>
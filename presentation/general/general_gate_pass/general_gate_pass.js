var basePath	= "presentation/general/general_gate_pass/";
var reportCode	= 915;
var MenuId		= 767
$(document).ready(function(){
	
	$("#frmGeneralGatePass").validationEngine();
	
	/*if(intAddx)
	{
		$('#frmGeneralGatePass #butNew').show();
		
		if(status==-2)
			$('#frmGeneralGatePass #butSave').hide();
		else
			$('#frmGeneralGatePass #butSave').show();
	}
	//permision for edit 
	if(intEditx)
	{
		if(status==-2)
			$('#frmGeneralGatePass #butSave').hide();
		else
			$('#frmGeneralGatePass #butSave').show();
	}*/
	
	$('#frmGeneralGatePass #butInsertRow').die('click').live('click',addNewRow);
	$('#frmGeneralGatePass .removeRow').die('click').live('click',deleteRow);
	$('#frmGeneralGatePass .clsReturnable').die('click').live('click',function(){addValidateClass(this,'Date')});
	$('#frmGeneralGatePass .clsQty').die('keyup').live('keyup',calTotalAmount);
	$('#frmGeneralGatePass #butSave').die('click').live('click',saveData);
	$('#frmGeneralGatePass .clsDescription').die('keyup').live('keyup',function(){addValidateClass(this,'description')});
	$('#frmGeneralGatePass #butNew').die('click').live('click',clearAll);
	
	$('#frmGeneralGatePass #butConfirm').die('click').live('click',Confirm);
	$('#frmGeneralGatePass #butCancel').die('click').live('click',Cancel);
	$('#frmGeneralGatePass #butReport').die('click').live('click',Report);
	
});
function addNewRow()
{
	var lastDateId 	= parseFloat($('#frmGeneralGatePass #tblMain tbody tr:last').find('.clsReturnDate').attr('id'));
	var lastTrId 	= parseFloat($('#frmGeneralGatePass #tblMain tbody tr:last').attr('id'))+1;
	$('#frmGeneralGatePass #tblMain tbody tr:last').after("<tr>"+$('#frmGeneralGatePass #tblMain .cls_tr_firstRow').html()+"</tr>");
	$('#frmGeneralGatePass #tblMain tbody tr:last').find('.clsReturnDate').attr('id','txtReturnDate'+lastTrId);
	$('#frmGeneralGatePass #tblMain tbody tr:last').find('.clsReturnDate').val('');
	$('#frmGeneralGatePass #tblMain tbody tr:last').attr('id',lastTrId);
	
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsDescription').val('');
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsSize').val('');
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsSerialNo').val('');
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsUnit').val('');
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsUnit').removeClass('validate[required]');
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsQty').val('');
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsReturnable').attr('checked',false);
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsReturnDate').val('');
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsReturnDate').removeClass('validate[required]');
}
function deleteRow()
{
	var delRowCount = parseInt(document.getElementById('tblMain').rows.length);
	
	if(delRowCount>2)
		$(this).parent().parent().remove();
	
	$('#frmGeneralGatePass #tblMain tbody tr').removeClass('cls_tr_firstRow');
	$('#frmGeneralGatePass #tblMain tbody tr:first').addClass('cls_tr_firstRow');	
}
function addValidateClass(obj,type)
{
	switch(type)
	{
		case 'Date' :
			if($(obj).attr('checked'))
				$(obj).parent().parent().find('.clsReturnDate').addClass('validate[required]');
			else
				$(obj).parent().parent().find('.clsReturnDate').removeClass('validate[required]');
		case 'description' :
			if($(obj).val()!='')
			{
				$(obj).parent().parent().find('.clsUnit').addClass('validate[required]');
				$(obj).parent().parent().find('.clsQty').addClass('validate[required]');
			}
			else
			{
				$(obj).parent().parent().find('.clsUnit').removeClass('validate[required]');
				$(obj).parent().parent().find('.clsQty').removeClass('validate[required]');
			}
	}
}
function calTotalAmount()
{
	var totalValue		= 0;
	
	$('#tblMain .clsQty').each(function(){
	
			totalValue += parseFloat(($(this).val()==''?0:$(this).val()));
	});
	
	totalValue	 = RoundNumber(totalValue,2);
	
	$('#txtTotalQty').val(totalValue);
}
function saveData()
{
	showWaiting();
	var gatePassNo		= $('#txtGatePassNo').val();
	var gatePassYear	= $('#txtGatePassYear').val();
	var date			= $('#txtDate').val();
	var attentionTo		= $('#txtAttentionTo').val();
	var gatePassTo		= $('#txtGatePassTo').val();
	var remarks			= $('#txtRemarks').val();
	var through			= $('#cboThrough').val();
	
	if($('#frmGeneralGatePass').validationEngine('validate'))
	{
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"gatePassNo":"'+gatePassNo+'",' ;
							arrHeader += '"gatePassYear":"'+gatePassYear+'",' ;
							arrHeader += '"date":"'+date+'",' ;
							arrHeader += '"attentionTo":'+URLEncode_json(attentionTo)+',';
							arrHeader += '"gatePassTo":'+URLEncode_json(gatePassTo)+',';
							arrHeader += '"remarks":'+URLEncode_json(remarks)+',';
							arrHeader += '"through":"'+through+'"' ;
			arrHeader += "}";
		
		var chkStatus	= false;
		var arrDetails	= "";
		
		$('#frmGeneralGatePass .clsDescription').each(function(){
			
			var itemId			= $(this).parent().parent().attr('id');
			var description 	= $(this).val();	
			var size	 		= $(this).parent().parent().find('.clsSize').val();
			var serialNo	 	= $(this).parent().parent().find('.clsSerialNo').val();
			var unitId	 		= $(this).parent().parent().find('.clsUnit').val();
			var qty	 			= $(this).parent().parent().find('.clsQty').val();
			var returnable	 	= ($(this).parent().parent().find('.clsReturnable').attr('checked')?1:0);
			var returnableDate	= $(this).parent().parent().find('.clsReturnDate').val();
			
			if(description!='')
			{
				chkStatus	= true;
				arrDetails += "{";
				arrDetails += '"itemId":"'+ itemId +'",' ;
				arrDetails += '"description":'+URLEncode_json(description)+',';
				arrDetails += '"size":'+URLEncode_json(size)+',';
				arrDetails += '"serialNo":'+URLEncode_json(serialNo)+',';
				arrDetails += '"unit":"'+unitId+'",' ;
				arrDetails += '"qty":"'+qty+'",' ;
				arrDetails += '"returnable":"'+returnable+'",' ;
				arrDetails += '"returnableDate":"'+returnableDate+'"' ;
				arrDetails += "},";
			}
		});
		if(!chkStatus)
		{
			$(this).validationEngine('showPrompt','No records to save.','fail');
			hideWaiting();	
			return;
		}
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		
		var arrHeader	= arrHeader;
		var arrDetails	= '['+arrDetails+']';
		data+="&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		var url = basePath+"general_gate_pass_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmGeneralGatePass #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						$('#txtGatePassNo').val(json.gatePassNo);
						$('#txtGatePassYear').val(json.gatePassYear);
						$('#frmGeneralGatePass #butConfirm').show();
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
						$('#frmGeneralGatePass #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
	}
	else
	{
		hideWaiting();
		return;
	}
}
function clearAll()
{
	document.location.href = '?q='+MenuId;
}
function Confirm()
{
	var url  = "?q="+reportCode+"&gatePassNo="+$('#frmGeneralGatePass #txtGatePassNo').val();
	    url += "&gatePassYear="+$('#frmGeneralGatePass #txtGatePassYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rptGeneralGatePass.php');
}
function Cancel()
{
	var url  = basePath+"rptGeneralGatePass.php?gatePassNo="+$('#frmGeneralGatePass #txtGatePassNo').val();
	    url += "&gatePassYear="+$('#frmGeneralGatePass #txtGatePassYear').val();
	    url += "&mode=Cancel";
	window.open(url,'rptGeneralGatePass.php');
}
function Report()
{
	if($('#frmGeneralGatePass #txtGatePassNo').val()=='')
	{
		$('#frmGeneralGatePass #butReport').validationEngine('showPrompt','No GatePass no to view Report','fail');
		return;	
	}
	var url  = "?q="+reportCode+"&gatePassNo="+$('#frmGeneralGatePass #txtGatePassNo').val();
	    url += "&gatePassYear="+$('#frmGeneralGatePass #txtGatePassYear').val();
	window.open(url,'rptGeneralGatePass.php');
}
function alertx()
{
	$('#frmGeneralGatePass #butSave').validationEngine('hide')	;
}
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";
//include "include/javascript.html";
$intUser				=$_SESSION["userId"];
$session_locationId 	= $_SESSION["CompanyID"];
$programCode	= 'P0767';
$reportCode		= 915;
$approveLevel 	= (int)getMaxApproveLevel();

$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];

$where_string = '';
$where_array = array(
				'Status'=>'GH.STATUS',
				'CONCAT_GATEPASS_NO'=>'CONCAT(GH.GATEPASS_NO,'/',GH.GATEPASS_YEAR )',
				'GATEPASS_NO'=>'GH.GATEPASS_NO',
				'GATEPASS_YEAR'=>'GH.GATEPASS_YEAR',
				'DATE'=>'GH.DATE',
				'GATEPASS_TO'=>'GH.GATEPASS_TO',
				'ATTENSION_TO'=>'GH.ATTENSION_TO',
				'NAME'=>'GL.NAME'
					);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Revised'=>'-1','Canceled'=>'-2','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND GH.DATE = '".date('Y-m-d')."'";
$sql = "SELECT SUB_1.* FROM
			(SELECT
			  CONCAT(GH.GATEPASS_NO,'/',GH.GATEPASS_YEAR )		AS CONCAT_GATEPASS_NO,
			  GH.GATEPASS_NO									AS GATEPASS_NO,
			  GH.GATEPASS_YEAR									AS GATEPASS_YEAR,
			  GH.DATE											AS DATE,
			  GH.GATEPASS_TO 									AS GATEPASS_TO,
			  GH.ATTENSION_TO 									AS ATTENSION_TO,
			  GL.NAME   										AS NAME,
			  GH.REMARK											AS REMARK,		 
			 
			  IF(GH.STATUS=1,'Approved',IF(GH.STATUS=-2,'Canceled',IF(GH.STATUS=0,'Rejected',IF(GH.STATUS=-1,'Revised','Pending')))) AS Status,
			  (SELECT ROUND(SUM(GD.QTY),2)						 		
			   FROM general_gate_pass_details GD
				WHERE GD.GATEPASS_NO = GH.GATEPASS_NO
				  AND GD.GATEPASS_YEAR = GH.GATEPASS_YEAR)		AS QTY,
			  (SELECT MIN(GD.RETURNABLE_DATE)						 		
			   FROM general_gate_pass_details GD
				WHERE GD.GATEPASS_NO = GH.GATEPASS_NO
				  AND GD.GATEPASS_YEAR = GH.GATEPASS_YEAR)		AS RETURN_DATE, ";
				  
		$sql .= "IFNULL((
				SELECT
				concat(U.strUserName,'(',max(GA.APPROVED_DATE),')' )
				FROM
				general_gate_pass_approvedby GA
				Inner Join sys_users U
					ON GA.APPROVED_BY = U.intUserId
				WHERE
				GA.GATEPASS_NO  = GH.GATEPASS_NO AND
				GA.GATEPASS_YEAR =  GH.GATEPASS_YEAR AND
				GA.APPROVE_LEVEL_NO =  '1' AND
				GA.STATUS =  '0'
				),IF(((SELECT
				MP.int1Approval 
				FROM menupermision MP
				Inner Join menus M ON MP.intMenuId = M.intId
				WHERE
				M.strCode = '$programCode' AND
				MP.intUserId =  '$intUser')=1 AND GH.STATUS>1),'Approve', 'bb')) as `1st_Approval`, ";
				
	for($i=2; $i<=$approveLevel; $i++){		
		if($i==2){
			$approval	= "2nd_Approval";
		}
		else if($i==3){
			$approval	= "3rd_Approval";
		}
		else {
			$approval	= $i."th_Approval";
		}
		
		
		$sql .= "IFNULL(
		(
		SELECT
		concat(U.strUserName,'(',max(GA.APPROVED_DATE),')' )
		FROM
		general_gate_pass_approvedby GA
		Inner Join sys_users U ON GA.APPROVED_BY = U.intUserId
		WHERE
		GA.GATEPASS_NO  = GH.GATEPASS_NO AND
		GA.GATEPASS_YEAR = GH.GATEPASS_YEAR AND
		GA.APPROVE_LEVEL_NO =  '$i' AND
		GA.STATUS =  '0' 
		),
		IF(
		((SELECT
		MP.int".$i."Approval 
		FROM menupermision MP
		Inner Join menus M 
			ON MP.intMenuId = M.intId
		WHERE
			M.strCode = '$programCode' AND
			MP.intUserId =  '$intUser')=1 AND (GH.STATUS>1) AND (GH.STATUS<=GH.APPROVE_LEVEL) AND ((SELECT
		concat(U.strUserName )
		FROM
		general_gate_pass_approvedby GA
		Inner Join sys_users U ON GA.APPROVED_BY = U.intUserId
		WHERE
		GA.GATEPASS_NO = GH.GATEPASS_NO AND
		GA.GATEPASS_YEAR = GH.GATEPASS_YEAR AND
		GA.APPROVE_LEVEL_NO = ($i-1) AND 
		GA.STATUS = '0' )<>'')),		
		'Approve',
		if($i>GH.APPROVE_LEVEL,'-----',''))
		
		) as `".$approval."`, "; 
		
		}
	
	$sql .= "IFNULL((SELECT
			concat(sys_users.strUserName,'(',max(general_gate_pass_approvedby.APPROVED_DATE),')' )
			FROM
			general_gate_pass_approvedby
			Inner Join sys_users ON general_gate_pass_approvedby.APPROVED_BY = sys_users.intUserId
			WHERE
			general_gate_pass_approvedby.GATEPASS_NO  = GH.GATEPASS_NO AND
			general_gate_pass_approvedby.GATEPASS_YEAR =  GH.GATEPASS_YEAR AND
			general_gate_pass_approvedby.APPROVE_LEVEL_NO =  '-2' AND
			general_gate_pass_approvedby.STATUS =  '0'
			),IF(((SELECT
			menupermision.intCancel 
			FROM menupermision 
			Inner Join menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId =  '$intUser')=1 AND 
			GH.STATUS=1),'Cancel', '')) as `Cancel`,";	
	
	$sql .= "IFNULL((SELECT
			concat(sys_users.strUserName,'(',max(general_gate_pass_approvedby.APPROVED_DATE),')' )
			FROM
			general_gate_pass_approvedby
			Inner Join sys_users ON general_gate_pass_approvedby.APPROVED_BY = sys_users.intUserId
			WHERE
			general_gate_pass_approvedby.GATEPASS_NO  = GH.GATEPASS_NO AND
			general_gate_pass_approvedby.GATEPASS_YEAR =  GH.GATEPASS_YEAR AND
			general_gate_pass_approvedby.APPROVE_LEVEL_NO =  '-1' AND
			general_gate_pass_approvedby.STATUS =  '0'
			),IF(((SELECT
			menupermision.intRevise 
			FROM menupermision 
			Inner Join menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId =  '$intUser')=1 AND 
			GH.STATUS=1),'Revise', '')) as `Revise`,";
					
				  
				  
	$sql .= " 'View'											AS VIEW,
			  'Report'											AS REPORT
			  FROM general_gate_pass_header GH
			INNER JOIN mst_gatepass_through_list GL
			  	ON GH.THROUGH_ID = GL.ID
			WHERE GH.LOCATION_ID = $session_locationId
			$where_string
		)  
		AS SUB_1 WHERE 1=1";
//echo ($sql);
$jq = new jqgrid('',$db);	
$col = array();

//BEGIN - ACTIVE/INACTIVE {
$col["title"] 			= "Status";
$col["name"] 			= "Status";
$col["width"] 			= "2"; 						// not specifying width will expand to fill space
$col["align"] 			= "left";
$col["sortable"] 		= true; 						// this column is not sortable
$col["search"] 			= true; 						// this column is not searchable
$col["editable"] 		= false;
$col["stype"] 			= "select";
$str 					= "Pending:Pending;Approved:Approved;Rejected:Rejected;Revised:Revised;Canceled:Canceled;:All";  // all row, blank row, then all db values. ; is separator
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;
//END 	- ACTIVE/INACTIVE }

$col["title"] 			= "Serial No";
$col["name"] 			= "GATEPASS_NO";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Serial Year";
$col["name"] 			= "GATEPASS_YEAR";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Gate Pass No";
$col["name"] 			= "CONCAT_GATEPASS_NO";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col['link']			= '?q=767&gatePassNo={GATEPASS_NO}&gatePassYear={GATEPASS_YEAR}';
$col["linkoptions"] 	= "target='invoice.php'";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Date";
$col["name"] 			= "DATE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Gate Pass To";
$col["name"] 			= "GATEPASS_TO";
$col["width"] 			= "4";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Attention To";
$col["name"] 			= "ATTENSION_TO";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Through";
$col["name"] 			= "NAME";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Remarks";
$col["name"] 			= "REMARK";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Qty";
$col["name"] 			= "QTY";
$col["width"] 			= "2";
$col["align"] 			= "right";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "1st Return Date";
$col["name"] 			= "RETURN_DATE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

//BEGIN - FIRST APPROVAL {
$col["title"] 			= "1st Approval";
$col["name"] 			= "1st_Approval";
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportCode.'&gatePassNo={GATEPASS_NO}&gatePassYear={GATEPASS_YEAR}&mode=Confirm';
$col["linkoptions"] 	= "target='rptGeneralGatePass.php'";
$col['linkName']		= 'Approve';
$cols[] 				= $col;	
$col					= NULL;
//END	- FIRST APPROVEL }

//BEGIN - SECOND & MORE APPROVAL {
for($i=2; $i<=$approveLevel; $i++)
{
	if($i==2){
		$ap		= "2nd Approval";
		$ap1	= "2nd_Approval";
	}
	else if($i==3){
		$ap		= "3rd Approval";
		$ap1	= "3rd_Approval";
	}
	else {
		$ap		= $i."th Approval";
		$ap1	= $i."th_Approval";
	}

$col["title"] 			= $ap; // caption of column
$col["name"] 			= $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportCode.'&gatePassNo={GATEPASS_NO}&gatePassYear={GATEPASS_YEAR}&mode=Confirm';
$col["linkoptions"] 	= "target='rptGeneralGatePass.php'";
$col['linkName']		= 'Approve';
$cols[] 				= $col;	
$col					= NULL;
}
//END 	- SECOND & MORE APPROVAL }
$col["title"] 			= 'Revise'; // caption of column
$col["name"] 			= 'Revise'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportCode.'&gatePassNo={GATEPASS_NO}&gatePassYear={GATEPASS_YEAR}&mode=Revise';
$col["linkoptions"] 	= "target='rptGeneralGatePass.php'";
$col['linkName']		= 'Revise';
$cols[] 				= $col;	
$col					= NULL;
//BEGIN - CANCEL {
$col["title"] 			= 'Cancel'; // caption of column
$col["name"] 			= 'Cancel'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportCode.'&gatePassNo={GATEPASS_NO}&gatePassYear={GATEPASS_YEAR}&mode=Cancel';
$col["linkoptions"] 	= "target='rptGeneralGatePass.php'";
$col['linkName']		= 'Cancel';
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Report";
$col["name"] 			= "REPORT";
$col["width"] 			= "2";
$col["align"] 			= "center"; 
$col["sortable"]		= false;
$col["editable"] 		= false; 	
$col["search"] 			= false; 
$col['link']			= '?q='.$reportCode.'&gatePassNo={GATEPASS_NO}&gatePassYear={GATEPASS_YEAR}';
$col["linkoptions"] 	= "target='rptGeneralGatePass.php'";
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "General Gate Pass Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'GATEPASS_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");
$d	=date('Y-m-d');
$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"},{"field":"DATE","op":"eq","data":"$d"}
     ]
}
SEARCH_JSON;
$grid["postData"] = array("filters" => $sarr ); 

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<head>
<title>General Gate Pass Listing
</title>
<?php 
		include "include/listing.html";
?>
</head>
<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>
</body>

<?php
function getMaxApproveLevel()
{
	global $db;
	$appLevel = 0;
	
	$sqlp = "SELECT
			Max(general_gate_pass_header.APPROVE_LEVEL) AS appLevel
			FROM general_gate_pass_header";					
	$resultp 	= $db->RunQuery($sqlp);
	$rowp		= mysqli_fetch_array($resultp);			 
	return $rowp['appLevel'];
}
?>

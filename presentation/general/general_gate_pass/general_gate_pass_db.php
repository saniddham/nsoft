<?PHP
 session_start();
$backwardseperator 		= "../../../";
$userId 				= $_SESSION['userId'];
$locationId	  			= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];

$requestType 			= $_REQUEST['requestType'];
$programName			= 'General GatePass Approval';
$programCode			= 'P0767';

include_once "../../../dataAccess/Connector.php";
include_once "../../../class/general/generalGatePass/cls_generalGatePass_set.php";
include_once "../../../class/general/generalGatePass/cls_generalGatePass_get.php";

$obj_gatepass_set		= new Cls_GeneralGatePass_Set($db);
$obj_gatepass_get		= new Cls_GeneralGatePass_Get($db);

if($requestType=='saveData')
{
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	$arrDetails 		= json_decode($_REQUEST['arrDetails'],true);
	
	echo $obj_gatepass_set->save($arrHeader,$arrDetails,$programCode,$programName);
}
else if($requestType=='approve')
{
	$gatePassNo			= $_REQUEST["gatePassNo"];
	$gatePassYear		= $_REQUEST["gatePassYear"];
	
	$response			= $obj_gatepass_get->validateBeforeApprove($gatePassNo,$gatePassYear,$programCode);
	
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_gatepass_set->approve($gatePassNo,$gatePassYear);
}
else if($requestType=='reject')
{
	$gatePassNo			= $_REQUEST["gatePassNo"];
	$gatePassYear		= $_REQUEST["gatePassYear"];
	
	$response			= $obj_gatepass_get->validateBeforeReject($gatePassNo,$gatePassYear,$programCode);
	 
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_gatepass_set->reject($gatePassNo,$gatePassYear);
}
else if($requestType=='cancel')
{
	$gatePassNo			= $_REQUEST["gatePassNo"];
	$gatePassYear		= $_REQUEST["gatePassYear"];
	
	$response			= $obj_gatepass_get->validateBeforeCancel($gatePassNo,$gatePassYear,$programCode);
	 
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_gatepass_set->cancel($gatePassNo,$gatePassYear);
}
else if($requestType=='revise')
{
	$gatePassNo			= $_REQUEST["gatePassNo"];
	$gatePassYear		= $_REQUEST["gatePassYear"];
	
	$response			= $obj_gatepass_get->validateBeforeRevise($gatePassNo,$gatePassYear,$programCode);
	 
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_gatepass_set->revise($gatePassNo,$gatePassYear);
}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
//include "include/javascript.html";
include_once  "class/cls_commonFunctions_get.php";
include_once  "class/finance/cls_common_get.php";
include_once  "class/cls_commonErrorHandeling_get.php";
include_once  "class/general/generalGatePass/cls_generalGatePass_get.php";

$obj_gatepass_get		= new Cls_GeneralGatePass_Get($db);
$obj_commonErrHandle	= new cls_commonErrorHandeling_get($db);
$obj_common				= new cls_commonFunctions_get($db);
$obj_common_get			= new Cls_Common_Get($db);

$programCode			= 'P0767';
$gatePassNo				= $_REQUEST["gatePassNo"];
$gatePassYear			= $_REQUEST["gatePassYear"];

$header_arr				= $obj_gatepass_get->loadHeaderData($gatePassNo,$gatePassYear);
$detail_result			= $obj_gatepass_get->loadDetailData($gatePassNo,$gatePassYear);
$maxItemId				= $obj_gatepass_get->getMaxItemId($gatePassNo,$gatePassYear);

$intStatus				= $header_arr['STATUS'];
$levels					= $header_arr['APPROVE_LEVEL'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_save($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_save			= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_cancel($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_cancel		= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_confirm		= $permition_arr['permision'];

$dateChangeMode 		= $obj_common->ValidateSpecialPermission('32',$session_userId,'RunQuery');
?>
<script type="text/javascript" >
	var status 			= '<?php echo $intStatus; ?>' ;
</script>

<title>General Gata Pass</title>
<form id="frmGeneralGatePass" name="frmGeneralGatePass" autocomplete="off" method="post">
  <div align="center">
    <div class="trans_layoutS" style="width:1100px">
      <div class="trans_text">General Gate Pass</div>
      <table width="1100">
        <tr>
          <td width="1096" colspan="2"><table width="100%%" border="0" class="normalfnt">
              <tr>
                <td>Gate Pass No</td>
                <td><input type="text" name="txtGatePassNo" id="txtGatePassNo" style="width:100px" disabled="disabled" value="<?php echo $gatePassNo; ?>"/>
                  <input type="text" name="txtGatePassYear" id="txtGatePassYear" style="width:50px" disabled="disabled" value="<?php echo $gatePassYear; ?>" /></td>
                <td>Gate Pass Date</td>
                <td><input name="txtDate" type="text" value="<?php echo($header_arr['DATE']==''?date("Y-m-d"):$header_arr['DATE']); ?>" class="validate[required]" id="txtDate" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($dateChangeMode!=1?'disabled="disabled"':''); ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
              <tr>
                <td width="16%">Attention To</td>
                <td width="36%"><input type="text" name="txtAttentionTo" id="txtAttentionTo" style="width:280px" value="<?php echo $header_arr['ATTENSION_TO']; ?>" /></td>
                <td width="16%">Through <span class="compulsoryRed">*</span></td>
                <td width="32%"><select name="cboThrough" class="txtbox validate[required]" id="cboThrough" style="width:280px">
                <?php
               	 echo $obj_gatepass_get->getThroughCombo($header_arr['THROUGH_ID']);
                ?>
                </select></td>
              </tr>
              <tr>
                <td>Gate Pass To <span class="compulsoryRed">*</span></td>
                <td><textarea name="txtGatePassTo" id="txtGatePassTo" style="width:280px;height:50px" class="validate[required]" ><?php echo $header_arr["GATEPASS_TO"]?></textarea></td>
                <td valign="middle">Remarks</td>
                <td valign="middle"><textarea name="txtRemarks" id="txtRemarks" style="width:280px;height:50px" ><?php echo $header_arr["REMARK"]?></textarea></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td valign="middle">&nbsp;</td>
                <td valign="middle">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
        <tr class="bordered">
            <th colspan="8">Gate Pass
            <div style="float:right"><a class="button white small" id="butInsertRow">Add New Row</a></div></th>
            </tr>
        <tr>
          <td colspan="2" ><div style="overflow:scroll;width:1100px;height:250px;">
              <table width="100%%" border="0" class="bordered" id="tblMain">
                <thead>
                  <tr>
                    <th width="3%">Del</th>
                    <th width="34%">Description <span class="compulsoryRed">*</span></th>
                    <th width="14%">Size</th>
                    <th width="14%">Serial No</th>
                    <th width="8%">Unit <span class="compulsoryRed">*</span></th>
                    <th width="9%">Qty <span class="compulsoryRed">*</span></th>
                    <th width="7%">Returnable</th>
                    <th width="11%">Return Date</th>
                  </tr>
                </thead>
                <tbody>
                <?php
				$totAmount 	= 0;
				$count		= 1;
				if($gatePassNo!='' && $gatePassYear!='')
				{
					
					while($row = mysqli_fetch_array($detail_result))
					{
						$dateId 	 = $count+100;
						$totAmount	+= $row['QTY'];
					?>
                    	<tr id="<?php echo $row['ITEM_ID']; ?>" <?php echo($count==1?'class="cls_tr_firstRow"':''); ?>>
                            <td class="cls_td_del" style="text-align:center"><img src="images/del.png" width="15" height="15" alt="delete" class="removeRow mouseover" /></td>
                            <td class="cls_td_Description"><textarea name="txtDescription" id="txtDescription" style="width:100%;height:20px" class="clsDescription" ><?php echo $row['DESCRIPTION']; ?></textarea></td>
                            <td width="14%"><input type="text" name="txtSize" id="txtSize" style="width:100%" class="clsSize" value="<?php echo $row['SIZE']; ?>" maxlength="255" /></td>
                            <td width="14%"><input type="text" name="txtSerialNo" id="txtSerialNo" style="width:100%" class="clsSerialNo" value="<?php echo $row['SERIAL_NO']; ?>" maxlength="255"/></td>
                            <td width="8%"><select name="cboUnit" id="cboUnit" style="width:80px;" class="clsUnit validate[required]">
							<?php
                            	echo $obj_common_get->getUOMCombo($row['UNIT_ID']);
                            ?>
                            </select></td>
                            <td style="text-align:right"><input type="text" name="txtQty" id="txtQty" style="width:90px;text-align:right" class="clsQty validate[custom[number]]" value="<?php echo $row['QTY']; ?>"/></td>
                            <td style="text-align:center"><input type="checkbox" id="chkReturnable" class="clsReturnable" <?php echo($row['RETURNABLE_FLAG']=='1'?'checked="checked"':'') ?>/></td>
                            <td style="text-align:left"><input name="txtReturnDate" type="text" value="<?php echo $row['RETURNABLE_DATE']; ?>" class="clsReturnDate" id="txtReturnDate<?php echo $dateId; ?>" style="width:90px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                        </tr>
                    <?php
					$count++;
					}
					for($i=$count;$i<11;$i++)
					{
						$maxItemId = $maxItemId+1;
						$dateId 	 = $i+100;
					?> 
                        <tr id="<?php echo $maxItemId; ?>" <?php echo($i==1?'class="cls_tr_firstRow"':''); ?>>
                            <td class="cls_td_del" style="text-align:center"><img src="images/del.png" width="15" height="15" alt="delete" class="removeRow mouseover" /></td>
                            <td class="cls_td_Description"><textarea name="txtDescription" id="txtDescription" style="width:100%;height:20px" class="clsDescription" ></textarea></td>
                            <td width="14%"><input type="text" name="txtSize" id="txtSize" style="width:100%" class="clsSize" /></td>
                            <td width="14%"><input type="text" name="txtSerialNo" id="txtSerialNo" style="width:100%" class="clsSerialNo"/></td>
                            <td width="8%"><select name="cboUnit" id="cboUnit" style="width:80px;" class="clsUnit">
							<?php
                            	echo $obj_common_get->getUOMCombo("");
                            ?>
                            </select></td>
                            <td style="text-align:right"><input type="text" name="txtQty" id="txtQty" style="width:90px;text-align:right" class="clsQty validate[custom[number]]"/></td>
                            <td style="text-align:center"><input type="checkbox" id="chkReturnable" class="clsReturnable" /></td>
                            <td style="text-align:left"><input name="txtReturnDate" type="text" value="" class="clsReturnDate" id="txtReturnDate<?php echo $dateId; ?>" style="width:90px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                        </tr>
					<?php
					}
				}
				else
				{
					 for($i=1;$i<11;$i++)
					 {
						 $dateId = $i+100;
					  ?>
                        <tr id="<?php echo $i; ?>" <?php echo($i==1?'class="cls_tr_firstRow"':''); ?>>
                            <td class="cls_td_del" style="text-align:center"><img src="images/del.png" width="15" height="15" alt="delete" class="removeRow mouseover" /></td>
                            <td class="cls_td_Description"><textarea name="txtDescription" id="txtDescription" style="width:100%;height:20px" class="clsDescription" ></textarea></td>
                            <td width="14%"><input type="text" name="txtSize" id="txtSize" style="width:100%" class="clsSize" /></td>
                            <td width="14%"><input type="text" name="txtSerialNo" id="txtSerialNo" style="width:100%" class="clsSerialNo"/></td>
                            <td width="8%"><select name="cboUnit" id="cboUnit" style="width:80px;" class="clsUnit">
							<?php
                            	echo $obj_common_get->getUOMCombo("");
                            ?>
                            </select></td>
                            <td style="text-align:right"><input type="text" name="txtQty" id="txtQty" style="width:90px;text-align:right" class="clsQty validate[custom[number]]"/></td>
                            <td style="text-align:center"><input type="checkbox" id="chkReturnable" class="clsReturnable" /></td>
                            <td style="text-align:left"><input name="txtReturnDate" type="text" value="" class="clsReturnDate" id="txtReturnDate<?php echo $dateId; ?>" style="width:90px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                        </tr>
                      <?php 
					 }
				}
				?>
                </tbody>
              </table>
            </div></td>
        </tr>
        <tr>
          <td colspan="2" align="center"><table width="100%" border="0">
            <tr>
              <td width="25%">&nbsp;</td>
              <td width="48%">&nbsp;</td>
              <td width="13%" class="normalfnt">Total Qty</td>
              <td width="14%"><input type="text" name="txtTotalQty" id="txtTotalQty" style="width:100%;text-align:right" disabled="disabled" value="<?php echo number_format($totAmount,2,'.',''); ?> "/></td>
            </tr>
          </table></td>
        </tr>
        <tr>
            <td height="32" >
                <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                    <tr>
                       <td align="center" class="tableBorder_allRound">
        <a class="button white medium" id="butNew">New</a>
         <?php if(($form_permision['add']||$form_permision['edit']) and $permision_save == 1){ ?>
         <a id="butSave" name="butSave"  class="button white medium">Save</a>
		 <?php } ?>
         <a id="butConfirm" class="button white medium" <?php if($permision_confirm!=1){ ?> style="display:none"<?php }?>>Approve</a>
		 <a id="butCancel" class="button white medium" style="display:none">Cancel</a>
         <a id="butReport" name="butReport"  class="button white medium">Report</a>
         <a href="main.php"  class="button white medium">Close</a></td>
                        
                    </tr>
                </table>
            </td>
        </tr>
      </table>
    </div>
  </div>
</form>

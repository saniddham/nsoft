<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once  ("class/cls_commonErrorHandeling_get.php");
include_once "class/general/generalGatePass/cls_generalGatePass_get.php";


$obj_gatepass_get			= new Cls_GeneralGatePass_Get($db);
$obj_commonErrHandle		= new cls_commonErrorHandeling_get($db);

$locationId					= $_SESSION["CompanyID"];
$companyId					= $_SESSION["headCompanyId"];
$session_userId 			= $_SESSION["userId"];
$thisFilePath 				= $_SERVER['PHP_SELF'];

$programCode				= 'P0767';
$gatePassNo					= $_REQUEST["gatePassNo"];
$gatePassYear				= $_REQUEST["gatePassYear"];
$mode						= $_REQUEST["mode"];

$header_array 				= $obj_gatepass_get->getRptHeaderData($gatePassNo,$gatePassYear);
$detail_result 				= $obj_gatepass_get->getRptDetailData($gatePassNo,$gatePassYear);

$intStatus					= $header_array['STATUS'];
$levels						= $header_array['APPROVE_LEVEL'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_cancel($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_cancel			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_reject($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_reject			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_confirm			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_revise($intStatus,$levels,$session_userId,$programCode,'RunQuery');
$permision_revise			= $permition_arr['permision'];

?>
<head>
<title>General Gate Pass Report</title>
<link rel="stylesheet" type="text/css" href="css/button.css"/>
<link rel="stylesheet" type="text/css" href="css/promt.css"/>
<script type="application/javascript" src="presentation/general/general_gate_pass/rptGeneralGatePass-js.js"></script>
<style>
#apDiv1 {
	position: absolute;
	left: 468px;
	top: 181px;
	width: 650px;
	height: 322px;
	z-index: 1;
	visibility: hidden;
}
.APPROVE {
	font-size: 16px;
	font-weight:bold;
	font-family: "Arial Black", Gadget, sans-serif;
	color: #36F;
}
#apDiv2 {
	position: absolute;
	left: 407px;
	top: 240px;
	width: 294px;
	height: 113px;
	z-index: 1;
}
</style>
</head>
<body>
<?php
if($intStatus==-2)
{
?>
	<div id="apDiv1"><img src="images/cancelled.png" style="opacity:0.3" /></div>
<?php
}
?>
<form id="frmRptGeneralGatePass" name="frmRptGeneralGatePass" method="post" autocomplete="off">
<table width="1000" align="center">
	<tr>
    	<td colspan="3"><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
    	<td colspan="3" style="text-align:center">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="3" style="text-align:center"><strong>GENERAL GATE PASS REPORT</strong></td>
    </tr>
    <?php
		include "presentation/report_approve_status_and_buttons.php"
     ?>
    <tr>
    	<td colspan="3">
            <table width="100%" border="0">
            	<tr class="normalfnt">
                	<td width="16%">GatePass No</td>
                    <td width="2%">:</td>
                    <td width="33%"><?php echo $gatePassNo.' / '.$gatePassYear?></td>
                    <td width="16%">GatePass Date</td>
                    <td width="2%">:</td>
                    <td width="31%"><?php echo $header_array["DATE"]?></td>
                </tr>
            	<tr class="normalfnt">
            	  <td>Attention To</td>
            	  <td>:</td>
            	  <td><?php echo $header_array["ATTENSION_TO"]?></td>
            	  <td>Through</td>
            	  <td>:</td>
            	  <td><?php echo $header_array["throughName"]?></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>Gate Pass To</td>
            	  <td>:</td>
            	  <td><?php echo $header_array["GATEPASS_TO"]?></td>
            	  <td>Remarks</td>
            	  <td>:</td>
            	  <td><?php echo $header_array["REMARK"]?></td>
          	  </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="3">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="3">
        	<table width="100%" border="0" class="rptBordered" id="tblMain">
                <thead> 
                <tr class="normalfnt">
                    <th width="5%">No.</th>
                    <th width="36%">Description</th>
                    <th width="14%">Size</th>
                    <th width="15%">Serial No</th>
                    <th width="8%">Unit</th>
                    <th width="10%">Qty</th>
                    <th width="12%">Return Date</th>
                </tr>
                </thead>
                <tbody>
				<?php
                $total = 0;
                while($row = mysqli_fetch_array($detail_result))
                {
					$total += $row['QTY'];
                ?>
                <tr class="normalfnt">
                	<td style="text-align:center"><?php echo ++$i; ?></td>
                    <td style="text-align:left"><?php echo $row['DESCRIPTION']; ?></td>
                    <td style="text-align:left"><?php echo ($row['SIZE']==''?'&nbsp;':$row['SIZE']); ?></td>
                    <td style="text-align:left"><?php echo ($row['SERIAL_NO']==''?'&nbsp;':$row['SERIAL_NO']); ?></td>
                    <td style="text-align:left"><?php echo $row['strCode']; ?></td>
                    <td style="text-align:right"><?php echo ($row['QTY']==''?'&nbsp;':$row['QTY']); ?></td>
                    <td style="text-align:center"><?php echo ($row['RETURNABLE_DATE']==''?'&nbsp;':$row['RETURNABLE_DATE']); ?></td>
                <?php
				}
				?>
                <tr class="normalfnt">
                    <td colspan="5" style="text-align:center"><b>Total :</b></td>
                    <td style="text-align:right"><b><?php echo number_format($total,2); ?></b></td>
                    <td style="text-align:center">&nbsp;</td>
                  </tr>
               </tbody>
            </table>
        </td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr>
        <td>
			<?php
			$creator		= $header_array['CREATOR'];
   			$createdDate	= $header_array['CREATED_DATE'];
            $resultA 		= $obj_gatepass_get->getRptApproveDetails($gatePassNo,$gatePassYear);
            include "presentation/report_approvedBy_details.php"
            ?>
        </td>
    </tr>
    <tr height="40">
<?Php 
	$createdBy	= $header_array['CREATED_BY'];
	
	$url  = "presentation/sendToApproval.php";		// * file name
	$url .= "?status=$intStatus";										// * set recent status
	$url .= "&approveLevels=$levels";								// * set approve levels in order header
	$url .= "&programCode=$programCode";								// * program code (ex:P10001)
	$url .= "&program=General Gate Pass";									// * program name (ex:Purchase Order)
	$url .= "&companyId=$companyId";									// * created company id
	$url .= "&createUserId=$createdBy";	
	
	$url .= "&field1=gatePass No";												 
	$url .= "&field2=gatePass Year";	
	$url .= "&value1=$gatePassNo";												 
	$url .= "&value2=$gatePassYear";	
	
	$url .= "&subject=GENERAL GATE PASS FOR APPROVAL ('$gatePassNo'/'$gatePassYear')";	
	
	$url .= "&statement1=Please Approve this";	
	$url .= "&statement2=to Approve this";	
							// * doc year
	$url .= "&link=".urlencode(base64_encode($mainPath."presentation/general/general_gate_pass/rptGeneralGatePass.php?gatePassNo=$gatePassNo&gatePassYear=$gatePassYear&mode=Confirm"));
?>
  <td align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"  ></iframe></td>
</tr> 
<tr>
  <td align="center" class="normalfntMid">&nbsp;</td>
</tr>
</table>
<?php 
if($intStatus!=1)
{
	//$count	=$count;
	$top	=17;
?>
<div id="apDiv2" style="margin-top: <?php echo $top; ?>%;margin-left:12%"><img src="images/invalid.png" width="279" height="111" align="bottom"  /></div>
<?php
}
?>
</form>
</body>
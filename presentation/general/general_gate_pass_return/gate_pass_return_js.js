var basePath	= "presentation/general/general_gate_pass_return/";
var reportId	= 925;
var MenuId		= 884;

$(document).ready(function(e) {
	$("#frmGatePassReturn").validationEngine();
	$('#frmGatePassReturn #butSave').die('click').live('click',updateReturn);
	$('#frmGatePassReturn #butReport').die('click').live('click',reportOpen);
	$('#frmGatePassReturn #butApprove').die('click').live('click',ConfirmreportOpen);
	$('#frmGatePassReturn #butNew').die('click').live('click',clearAll);
	 });

function reportOpen()
{
	if($('#frmGatePassReturn #txtgatepassReturnNo').val()=='')
	{
		$('#frmGatePassReturn #butReport').validationEngine('showPrompt','No GatePass no to view Report','fail');
		return;	
	}
	var url  = "?q="+reportId+"&gatePassReturnNo="+$('#frmGatePassReturn #txtgatepassReturnNo').val();
	    url += "&gatePassReturnYear="+$('#frmGatePassReturn #txtgatepassReturnYear').val();
	window.open(url,'rptGatepassReturned.php');
}
function ConfirmreportOpen()
{
	if($('#frmGatePassReturn #txtgatepassReturnNo').val()=='')
	{
		$('#frmGatePassReturn #butReport').validationEngine('showPrompt','No GatePass no to view Report','fail');
		return;	
	}
	var url  = "?q="+reportId+"&gatePassReturnNo="+$('#frmGatePassReturn #txtgatepassReturnNo').val();
	    url += "&gatePassReturnYear="+$('#frmGatePassReturn #txtgatepassReturnYear').val();
		url += "&mode=Confirm";
	window.open(url,'rptGatepassReturned.php');
}

function updateReturn()
{
	if(!$('#frmGatePassReturn').validationEngine('validate'))
	{
		return;
	}
		var headerArray	= "{";
			headerArray += '"GatePassReturnNo":"'+$('#txtgatepassReturnNo').val()+'",';
			headerArray += '"GatePassReturnyear":"'+$('#txtgatepassReturnYear').val()+'",';
			headerArray += '"returnRemark":"'+$('#returnRemarks').val()+'",';
			headerArray += '"GatePassNo":"'+$('#txtgatepassNo').val()+'",';
			headerArray += '"GatePassYear":"'+$('#txtgatepassYear').val()+'"';
			headerArray += "}";
			
		
		var detailArray = "[ ";
		$('#tblMain .clsQty').each(function() {
			if($(this).parent().parent().find('.cls_check').children().is(':checked'))
			{
				detailArray += "{";
				detailArray += '"itemID":"'+$(this).parent().parent().attr('id')+'",';
				detailArray += '"qty":"'+$(this).val()+'",';	
				detailArray += '"returnDate":"'+$('#tblMain .clsReturnDate').val()+'"';		
				detailArray += "},";
			}
		  });
		detailArray = detailArray.substr(0,detailArray.length-1);
		detailArray += "]";
		
		var url		= basePath+"gate_pass_return_db.php?requestType=updateData";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:'&HeaderArray='+headerArray+'&DetailArray='+detailArray,
				async:false,
				success: function(json)
				{
					$('#frmGatePassReturn #butSave').validationEngine('showPrompt', json.msg,json.type);
					if(json.type=='pass')
					{
						$('#txtgatepassReturnNo').val(json.returnNo);
						$('#txtgatepassReturnYear').val(json.returnYear);
						$('#butApprove').show();
						//$('#frmGatePassReturn').get(0).reset();
						
					}				
				},
				error: function(xhr,status)
				{
					$('#frmGatePassReturn #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					return;
				}
				});

}
function clearAll()
{
	document.location.href = '?q='+MenuId;
}

	
var basePath	= "presentation/general/general_gate_pass_return/";

$(document).ready(function() {

	$('#frmRptGatepassReturn #butRptConfirm').die('click').live('click',ConfirmRpt);
	$('#frmRptGatepassReturn #butRptReject').die('click').live('click',RejectRpt);
 
 });
 
 
 
 function ConfirmRpt(){
	 
	 
	var val = $.prompt('Are you sure you want to approve this Return Note ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"gate_pass_return_db.php"+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptGatepassReturn #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									//var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptGatepassReturn #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								//var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
					   hideWaiting();
				}});
  }
  
  
  function RejectRpt(){
	  
	var val = $.prompt('Are you sure you want to reject this Return Note ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				showWaiting();
				var url = basePath+"gate_pass_return_db.php"+window.location.search+'&requestType=reject';
				var obj = $.ajax({
					url:url,
					type:'post',
					dataType: "json",  
					data:'',
					async:false,
					
					success:function(json){
							$('#frmRptGatepassReturn #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								//var t=setTimeout("alertx()",1000);
								window.location.href = window.location.href;
								window.opener.location.reload();//reload listing page
								return;
							}
						},
					error:function(xhr,status){
							
							$('#frmRptGatepassReturn #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							//var t=setTimeout("alertx()",3000);
						}		
					});
				////////////
					}
					hideWaiting();
			}});
	  
  }

 


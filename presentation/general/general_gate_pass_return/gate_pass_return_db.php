<?php
//ini_set('display_errors',1);
session_start();
$backwardseperator 		= "../../../../";
$userId 				= $_SESSION['userId'];
$locationId	  			= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$requestType 			= $_REQUEST['requestType'];
$programCode			= "P0809";

include_once "../../../dataAccess/Connector.php";
//include_once "../../../class/cls_commonFunctions_get.php";
//include_once  "../../../class/cls_commonErrorHandeling_get.php";
include_once "../../../class/general/generalGatePassReturn/cls_gatePassReturn_get.php";
include_once "../../../class/general/generalGatePassReturn/cls_gatePassReturn_set.php"; 

$obj_comfunc_get		= new cls_commonFunctions_get($db);
$obj_gatePassReturnSet	= new GatePassReturn_Set($db);
$obj_gatePassReturnGet	= new gatePassReturn_get($db);


if($requestType=='updateData')
{
	$headerArray 	= json_decode($_REQUEST["HeaderArray"],true);
	$detailArray 	= json_decode($_REQUEST["DetailArray"],true);

	if($headerArray["GatePassReturnNo"] == "")
	{		
		echo saveReturn($headerArray,$detailArray);
	}
	else
	{
		echo updateReturn($headerArray,$detailArray);
	}
}

else if($requestType == 'approve')
{
	$gatePassReturnNo			= $_REQUEST["gatePassReturnNo"];
	$gatePassReturnYear			= $_REQUEST["gatePassReturnYear"];
	echo approve($gatePassReturnNo,$gatePassReturnYear);
	
}
else if($requestType == 'reject')
{
	$gatePassReturnNo			= $_REQUEST["gatePassReturnNo"];
	$gatePassReturnYear			= $_REQUEST["gatePassReturnYear"];
	echo reject($gatePassReturnNo,$gatePassReturnYear);
	
}


function updateReturn($headerArray,$detailArray)
{
	global $db;
	global $obj_comfunc_get;
	global $companyId;
	global $programCode;
	global $locationId;
	global $userId;
	global $obj_gatePassReturnSet;
	global $obj_gatePassReturnGet;
	
	$db->begin();
	$savedStatus			= true;
	$savedMasseged			= "";
	$error_sql				= "";
	$gatepassreturnNo		= ($headerArray["GatePassReturnNo"]);
	$returnYear				= ($headerArray["GatePassReturnyear"]);
	$gatePassNo				= ($headerArray['GatePassNo']);
	$gatePassYear			= ($headerArray['GatePassYear']);
	$returnRemarks			= ($headerArray['returnRemark']);
	$date					= date("Y-m-d");
	$approveLevel			= $obj_comfunc_get->getApproveLevels_new($programCode);
	//$status					= $approveLevel["ApproLevel"]+1;
	$validate				= $obj_gatePassReturnGet->validateSave($gatepassreturnNo,$returnYear,$programCode,$userId);
	if($validate['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= $validate['savedMassege'];
		$error_sql		= $validate['error_sql'];
	}
	
	$dataArr	= $obj_gatePassReturnSet->updateReturnHeader($gatepassreturnNo,$returnYear,$date,$returnRemarks,$status,$userId);
	if($dataArr['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= $dataArr['savedMassege'];
		$error_sql		= $dataArr['error_sql'];
	}
	
	$dataArr1	= $obj_gatePassReturnSet->updateMaxStatus($gatepassreturnNo,$returnYear);
	if($dataArr1['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= $dataArr1['savedMassege'];
		$error_sql		= $dataArr1['error_sql'];
	}

	$dataArr2	= $obj_gatePassReturnSet->deleteDetail($gatepassreturnNo,$returnYear);

		if($dataArr2['savedStatus']=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= $dataArr2['savedMassege'];
			$error_sql		= $dataArr2['error_sql'];
		}
		foreach($detailArray as $detailArray)
		{
			$qty		= $detailArray["qty"];
			$itemID		= $detailArray["itemID"];
			$returnDate	= $detailArray["returnDate"];
			
			$balQtyArr		= $obj_gatePassReturnGet->BalQty($gatePassNo,$gatePassYear,$itemID);
			$balQty			= $balQtyArr['BAL_RETURN_QTY'];
		
			if($balQty < $detailArray["qty"])
			{
				$savedStatus	= false;
				$savedMasseged	= 'Return Qty exceeds remaining Qty';
			}
			
			$dataArr1	= $obj_gatePassReturnSet->saveReturnDetail($gatepassreturnNo,$returnYear,$itemID,$qty,$returnDate);
			if($dataArr1['savedStatus']=='fail' && ($savedStatus))
			{
				$savedStatus	= false;
				$savedMasseged	= $dataArr1['savedMassege'];
				$error_sql		= $dataArr1['error_sql'];
			}	
		}
		if($savedStatus)
		{
			$db->commit();
			$response['type'] 			= "pass";
			$response['msg']			= "Updated Successfully.";
			$response['returnNo']		= $gatepassreturnNo;
			$response['returnYear']		= $returnYear;
		}
		else
		{
			$db->rollback;
			$response['type'] 	= "fail";
			$response['msg']	= $savedMasseged;
			$response['error']	= $error_sql;
		}
		return json_encode($response);
		
	
}

function saveReturn($headerArray,$detailArray)
{
	global $db;
	global $obj_comfunc_get;
	global $companyId;
	global $programCode;
	global $locationId;
	global $userId;
	global $obj_gatePassReturnSet;
	global $obj_gatePassReturnGet;
	
	$db->begin();
	$savedStatus			= true;
	$savedMasseged			= "";
	$error_sql				= "";
	$returnRemarks			= ($headerArray['returnRemark']);
	$gatePassNo				= ($headerArray['GatePassNo']);
	$gatePassYear			= ($headerArray['GatePassYear']);
	$date					= date("Y-m-d");
	$appLevelArr			= $obj_comfunc_get->getApproveLevels_new($programCode);
	$approveLevel			= $appLevelArr['ApproLevel'];
	$status					= $approveLevel+1;
		
	$result = $obj_comfunc_get->GetSystemMaxNo('GENERAL_GATEPASS_RETURN_NO',$locationId);
	if($result['rollBackFlag']==1)
	{
		$savedStatus	= false;
		$savedMasseged	= $result['msg'];
		$error_sql		= $result['q'];
	}
	$returnNo		= $result["max_no"];
	$returnYear		= date("Y");

	$dataArr				= $obj_gatePassReturnSet->saveReturnHeader($gatePassNo,$returnNo,$gatePassYear,$returnYear,$date,$returnRemarks,$approveLevel,$status,$companyId,$locationId,$userId);
	if($dataArr['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= $dataArr['savedMassege'];
		$error_sql		= $dataArr['error_sql'];
	}
	
	foreach($detailArray as $detailArray)
	{
		$qty		= $detailArray["qty"];
		$itemID		= $detailArray["itemID"];
		$returnDate	= $detailArray["returnDate"];
		
		$balQtyArr		= $obj_gatePassReturnGet->BalQty($gatePassNo,$gatePassYear,$itemID);
		$balQty			= $balQtyArr['BAL_RETURN_QTY'];

		if($balQty < $qty)
		{
			$savedStatus	= false;
			$savedMasseged	= 'Return Qty exceeds remaining Qty';
		}
		
		$dataArr1	= $obj_gatePassReturnSet->saveReturnDetail($returnNo,$returnYear,$itemID,$qty,$returnDate);
		if($dataArr1['savedStatus']=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= $dataArr1['savedMassege'];
			$error_sql		= $dataArr1['error_sql'];
		}	
	}
	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		$response['msg']			= "Saved Successfully.";
		$response['returnNo']		= $returnNo;
		$response['returnYear']		= $returnYear;
	}
	else
	{
		$db->rollback;
		$response['type'] 	= "fail";
		$response['msg']	= $savedMasseged;
		$response['error']	= $error_sql;
	}
	return json_encode($response);
	
}
function approve($gatePassReturnNo,$gatePassReturnYear)
{
	global $db;
	global $obj_comfunc_get;
	global $companyId;
	global $programCode;
	global $locationId;
	global $userId;
	global $obj_gatePassReturnSet;
	global $obj_gatePassReturnGet;
	
	
	$db->begin();
	$savedStatus			= true;
	$savedMasseged			= "";
	$error_sql				= "";
	
	$dataArr1 = $obj_gatePassReturnSet->updateStatus($gatePassReturnNo,$gatePassReturnYear,'');
	if($dataArr1['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= $dataArr1['savedMassege'];
		$error_sql		= $dataArr1['error_sql'];
	}	
			
	$header_array	= $obj_gatePassReturnGet->get_rpt_header_array($gatePassReturnNo,$gatePassReturnYear,'RunQuery2');
	$status			= $header_array['STATUS'];
	$savedLevels	= $header_array['LEVELS'];
	$approval		= $savedLevels+1-$status;
	
	if($status==1)
	{
		$dataArr1 = $obj_gatePassReturnSet->updateQTY($gatePassReturnNo,$gatePassReturnYear);
		if($dataArr1['savedStatus']=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= $dataArr1['savedMassege'];
			$error_sql		= $dataArr1['error_sql'];
		}	
	}
		
	$dataArr1 = $obj_gatePassReturnSet->insertApprovedByReturn($gatePassReturnNo,$gatePassReturnYear,$userId,$approval);
	if($dataArr1['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= $dataArr1['savedMassege'];
		$error_sql		= $dataArr1['error_sql'];
	}	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		$response['msg']			= "Approved Successfully.";
	}
	else
	{
		$db->rollback;
		$response['type'] 	= "fail";
		$response['msg']	= $savedMasseged;
		$response['error']	= $error_sql;
	}
	return json_encode($response);
			
}
function reject($gatePassReturnNo,$gatePassReturnYear)
{
	global $db;
	global $obj_comfunc_get;
	global $companyId;
	global $programCode;
	global $locationId;
	global $userId;
	global $obj_gatePassReturnSet;
	global $obj_gatePassReturnGet;
	
	
	$db->begin();
	$savedStatus			= true;
	$savedMasseged			= "";
	$error_sql				= "";	
	
	$dataArr1 = $obj_gatePassReturnSet->updateStatus($gatePassReturnNo,$gatePassReturnYear,'0');
	if($dataArr1['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= $dataArr1['savedMassege'];
		$error_sql		= $dataArr1['error_sql'];
	}	
			
	$header_array	= $obj_gatePassReturnGet->get_rpt_header_array($gatePassReturnNo,$gatePassReturnYear,'RunQuery2');			
	$dataArr1 		= $obj_gatePassReturnSet->insertApprovedByReturn($gatePassReturnNo,$gatePassReturnYear,$userId,0);
	if($dataArr1['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= $dataArr1['savedMassege'];
		$error_sql		= $dataArr1['error_sql'];
	}	
	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		$response['msg']			= "Rejected Successfully.";
		
	}
	else
	{
		$db->rollback;
		$response['type'] 	= "fail";
		$response['msg']	= $savedMasseged;
		$response['error']	= $error_sql;
	}
	return json_encode($response);

}

?>
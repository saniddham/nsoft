<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
//include_once "class/cls_commonErrorHandeling_get.php"; 
include_once "class/general/generalGatePassReturn/cls_gatePassReturn_get.php";

$obj_gatePassReturn_get		= new gatePassReturn_get($db);
$obj_commonErr				= new cls_commonErrorHandeling_get($db);

$programCode				= 'P0809';
//$gatePassNo					= $_REQUEST["gatePassNo"];
//$gatePassYear				= $_REQUEST["gatePassYear"];
$gatePassReturnNo			= $_REQUEST["gatePassReturnNo"];
$gatePassReturnYear			= $_REQUEST["gatePassReturnYear"];
$mode						= $_REQUEST["mode"];

$locationId					= $_SESSION["CompanyID"];
$companyId					= $_SESSION["headCompanyId"];
$session_userId 			= $_SESSION["userId"];
$thisFilePath 				= $_SERVER['PHP_SELF'];

$header_array 				= $obj_gatePassReturn_get->get_rpt_header_array($gatePassReturnNo,$gatePassReturnYear,'RunQuery');

$detail_result 				= $obj_gatePassReturn_get->get_details_result($gatePassReturnNo,$gatePassReturnYear,'RunQuery');

//print_r($header_array);
$permition_arr		= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$programCode,'RunQuery');
$permision_save		= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$programCode,'RunQuery');
$permision_confirm	= $permition_arr['permision'];
$permision_reject	= $permision_confirm;


?>
<head>
<title>General Gatepass Return Approval Report</title>
<link rel="stylesheet" type="text/css" href="css/button.css"/>
<link rel="stylesheet" type="text/css" href="css/promt.css"/>
<script type="text/javascript" src="presentation/general/general_gate_pass_return/rptGatepassReturned-js.js"></script>
<style>
#apDiv1 {
	position: absolute;
	left: 468px;
	top: 181px;
	width: 650px;
	height: 322px;
	z-index: 1;
	visibility: hidden;
}
.APPROVE {
	font-size: 16px;
	font-weight:bold;
	font-family: "Arial Black", Gadget, sans-serif;
	color: #36F;
}
#apDiv2 {
	position: absolute;
	left: 407px;
	top: 240px;
	width: 294px;
	height: 113px;
	z-index: 1;
}
</style>
</head>

<body>
<form id="frmRptGatepassReturn" name="frmRptGatepassReturn" method="post" >
<table width="100%" align="center">
	<tr>
    	<td colspan="3"><?php include 'reportHeader.php'?></td>
    </tr>
   	<tr>
   	  <td colspan="3" style="text-align:center">&nbsp;</td></tr>
    <tr>
    <td colspan="3" style="text-align:center"><strong>GENERAL GATE PASS RETURN REPORT</strong></td>
    </tr>
    <?php
		include "presentation/report_approve_status_and_buttons.php"
     ?>
    <tr>
    	<td colspan="3">
            <table width="100%" border="0">
            	<tr class="normalfnt">
                	<td width="18%">Gate Pass Return No</td>
                    <td width="2%">:</td>
                    <td width="29%"><?php echo $header_array['GATEPASS_RETURN_NO']?></td>
                    <td width="20%">Gate Pass Return Date</td>
                    <td width="2%">:</td>
                    <td width="29%"><?php echo $header_array["DATE"]?></td>
                </tr>
                <tr class="normalfnt">
                	<td width="16%">Gate Pass No</td>
                    <td width="2%">:</td>
                    <td width="31%"><?php echo $header_array["GATEPASS_NO"]?></td>
                    <td width="18%">Gate Pass Date</td>
                    <td width="2%">:</td>
                    <td width="31%"><?php echo $header_array["GATEPASS_DATE"]?></td>
                </tr>
            	<tr class="normalfnt">
            	  <td>Attention To</td>
            	  <td>:</td>
            	  <td><?php echo $header_array['ATTENSION_TO']?></td>
                  <td>Gate Pass To</td>
            	  <td>:</td>
            	  <td><?php echo $header_array['GATEPASS_TO']?></td>
           	    </tr>
            	<tr class="normalfnt">
            	  <td>Remarks</td>
            	  <td>:</td>
            	  <td><?php echo $header_array['REMARK']?></td>
          	  </tr>
              </table>
              </td></tr>
        <tr>
          <td colspan="3">&nbsp;</td>
    	</tr>
   	    <tr>
    		<td colspan="3">
        	<table width="100%" border="0" class="rptBordered" id="tblMain">
                <thead> 
                <tr class="normalfnt">
                    <th width="5%">No.</th>
                    <th width="36%">Description</th>
                    <th width="14%">Size</th>
                    <th width="15%">Serial No</th>
                    <th width="8%">Unit</th>
                    <th width="10%">Qty</th>
                    <th width="12%">Returned Date</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $total = 0;
				$i	= 0;
                while($row = mysqli_fetch_array($detail_result))
                {
					$total += $row['QTY'];
                ?>
                <tr class="normalfnt">
                	<td style="text-align:center"><?php echo $i++; ?></td>
                    <td style="text-align:left"><?php echo $row['DESCRIPTION']; ?></td>
                    <td style="text-align:left"><?php echo ($row['SIZE']==''?'&nbsp;':$row['SIZE']); ?></td>
                    <td style="text-align:left"><?php echo ($row['SERIAL_NO']==''?'&nbsp;':$row['SERIAL_NO']); ?></td>
                    <td style="text-align:left"><?php echo $row['strCode']; ?></td>
                    <td style="text-align:right"><?php echo ($row['QTY']==''?'&nbsp;':$row['QTY']); ?></td>
                    <td style="text-align:center"><?php echo ($row['RETURN_DATE']==''?'&nbsp;':$row['RETURN_DATE']); ?></td>
                <?php
				}
				?>
                </tbody>
                </table>
             </table>
        </td>
    </tr>
    <tr>
    <td>
    <?php
			$creator		= $header_array['CREATOR'];
			$createdDate	= $header_array['CREATED_DATE'];
 			$resultA 		= $obj_gatePassReturn_get->get_Report_approval_details_result($gatePassReturnNo,$gatePassReturnYear,'RunQuery');
			include "presentation/report_approvedBy_details.php"
 	?>
    </td>
    </tr>
</table>
</form> 
</body>

<?php
require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";
$session_locationId = $_SESSION["CompanyID"];
$intUser  			= $_SESSION["userId"];
$formID				= 884;

$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];

$where_string = '';
$where_array = array(
				'CONCAT_GATEPASS'=>"CONCAT(gd.GATEPASS_NO,'/',gd.GATEPASS_YEAR)",
				'DATE'=>'gh.DATE',
				'GATEPASS_TO'=>'gh.GATEPASS_TO',
				'ATTENSION_TO'=>'gh.ATTENSION_TO',
				'Through'=>'gt.NAME'
				);
			
//$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
if($arr)
{
	foreach($arr as $k=>$v)
	{
		/*if($v['field']=='Status')
		{
			if($arr_status[$v['data']]==2)
				$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
			else
				$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
		}*/
		if($where_array[$v['field']])
			$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
	}
}
if(!count($arr)>0)					 
	$where_string .= "AND gh.DATE = '".date('Y-m-d')."'";

$sql	= " SELECT SUB_1.* FROM
			( SELECT	
			DISTINCT(CONCAT(gd.GATEPASS_NO,'/',gd.GATEPASS_YEAR))	AS CONCAT_GATEPASS,
			gd.GATEPASS_NO											AS GATEPASS_NO,
			gd.GATEPASS_YEAR										AS GATEPASS_YEAR,
			gh.DATE													AS DATE,
			gh.GATEPASS_TO											AS GATEPASS_TO, 
			gh.ATTENSION_TO											AS ATTENSION_TO, 
			gt.NAME 												AS Through,
			gh.REMARK												AS REMARK
			
			FROM 
			general_gate_pass_header gh
			
			INNER JOIN general_gate_pass_details gd ON gh.GATEPASS_NO = gd.GATEPASS_NO AND gh.GATEPASS_YEAR = gd.GATEPASS_YEAR
			INNER JOIN mst_gatepass_through_list gt ON gh.THROUGH_ID = gt.ID
			WHERE
			gd.RETURNABLE_FLAG = 1 AND
			gh.`STATUS` = 1 AND
			gh.LOCATION_ID = '$session_locationId'
			$where_string
			) as SUB_1 WHERE 1=1";
// echo $sql;
$jq		= new jqgrid('',$db);
$col	= array();

$col["title"]			= "Serial No";
$col["name"]			= "GATEPASS_NO";
$col["width"]			= "1";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$col["hidden"]			= true;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Gate Pass Year";
$col["name"]			= "GATEPASS_YEAR";
$col["width"]			= "1";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$col["hidden"]			= true;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Gate Pass No";
$col["name"]			= "CONCAT_GATEPASS";
$col["width"]			= "3";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$col["link"]			= '?q='.$formID.'&gatePassNo={GATEPASS_NO}&gatePassYear={GATEPASS_YEAR}';
$col["linkoptions"]		= "target='invoice.php'";
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Date";
$col["name"]			= "DATE";
$col["width"]			= "2";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Gate Pass To";
$col["name"]			= "GATEPASS_TO";
$col["width"]			= "2";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Attention To";
$col["name"]			= "ATTENSION_TO";
$col["width"]			= "2";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			   		= $col;
$col					= NULL;

$col["title"]			= "Through";
$col["name"]			= "Through";
$col["width"]			= "2";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Remark";
$col["name"]			= "REMARK";
$col["width"]			= "2";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

$grid["caption"]		= "General Gate Pass Returnable List";
$grid["multiselect"]	= false;
$grid["rowNum"]			= 20;
/*$grid["sortname"]		= "GATEPASS_NO";
$grid["sortorder"]		= "DESC";*/
$grid["autowidth"]		= true;
$grid["multiselect"]	= false;
$grid["search"]			= true;
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command=$sql;
$jq->set_columns($cols);
$jq->set_actions(array(
	"add"=>false,
	"edit"=>false,
	"delete"=>false,	
	"rowactions"=>false,
	"search"=>"advance",
	"export"=>true));
	
$out	= $jq->render('list1');

?>
<head>
<?php 
		include "include/listing.html";
?>

</head>

<body>
<form id="returnListing" name="returnListing" action="" method="post">
            	<div align="center" style="margin:10px;">
                	<?php echo $out ?>
                </div>
</form>
</body>

<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
//ini_set('display_errors',0);
$locationId				= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$session_userId 		= $_SESSION['userId'];
$programCode			= 'P0809';


//include_once  "class/cls_commonErrorHandeling_get.php";
//include_once "class/cls_commonFunctions_get.php";
include_once "class/finance/cls_common_get.php";
include_once "class/general/generalGatePassReturn/cls_gatePassReturn_get.php";

$obj_commonErrHandle	= new cls_commonErrorHandeling_get($db);
$obj_common				= new cls_commonFunctions_get($db);

 

$obj_common_get			= new Cls_Common_Get($db);
$obj_gatePassReturn		= new gatePassReturn_get($db); 

$gatePassNo			= $_REQUEST['gatePassNo'];
$gatePassYear		= $_REQUEST['gatePassYear'];
$gatePassReNo		= $_REQUEST['gatePassReturnNo'];
$gatePassReYear		= $_REQUEST['gatePassReturnYear'];
$tableResult;

if($gatePassReNo!='' and $gatePassReYear!='')
{
	$returnResult_arr		= $obj_gatePassReturn->loadReturnHeader($gatePassReNo,$gatePassReYear);
	$header_arr				= $obj_gatePassReturn->get_rpt_header_array($gatePassReNo,$gatePassReYear,'RunQuery');
	
	$gatePassNo				= $returnResult_arr['GATEPASS_NO'];
	$gatePassYear			= $returnResult_arr['GATEPASS_YEAR'];
	
	$intStatus				= $header_arr['STATUS'];
	$levels					= $header_arr['LEVELS'];
	
	$searchResult_arr 		= $obj_gatePassReturn->loadHeader($gatePassNo,$gatePassYear);
	$tableResult			= $obj_gatePassReturn->load_reTable($gatePassReNo,$gatePassReYear);
	//$tableResult1			= $obj_gatePassReturn->loadTable($gatePassNo,$gatePassYear);
		//die('OK');
	$returnQTYGet			= $obj_gatePassReturn->get_details_result($gatePassReNo,$gatePassReYear,'RunQuery');
	$resultQTY				= mysqli_fetch_array($returnQTYGet);
	$returnQTY				= $resultQTY['QTY'];
		
	$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_save($intStatus,$levels,$session_userId,$programCode,'RunQuery');
	$permision_save			= $permition_arr['permision'];
		
	$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_reject($intStatus,$levels,$session_userId,$programCode,'RunQuery');
	$permision_reject		= $permition_arr['permision'];
	
	$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$session_userId,$programCode,'RunQuery');
	$permision_confirm		= $permition_arr['permision'];
	
	$dateChangeMode 		= $obj_common->ValidateSpecialPermission('31',$session_userId,'RunQuery');
		
		
}
else
{
	$searchResult_arr 		= $obj_gatePassReturn->loadHeader($gatePassNo,$gatePassYear);
	$tableResult			= $obj_gatePassReturn->loadTable($gatePassNo,$gatePassYear);
	$permision_save			= 1;
}
?>

<title>General GatePass Return</title>
<form id="frmGatePassReturn" name="frmGatePassReturn" autocomplete="off" method="post">
<div align="center">
<div class="trans_layoutD" style="width:1100px;">
<div class="trans_text">General Gate Pass Return</div>
<table width="100%">
	<tr>
    	<td width="100%" colspan="2">
        <fieldset class="tableBorder">
        <legend class="normalfnt">Gate Pass Return Header:</legend>
        <table width="100%" border="0" class="normalfnt">
        	<tr>
            	<td width="16%">Gate Pass Return No:</td>
                <td width="36%"><input type="text" name="txtgatepassReturnNo" value="<?php echo $gatePassReNo; ?>" id="txtgatepassReturnNo" style="width:100px" disabled="disabled"/>
                	<input type="text" name="txtgatepassReturnYear" value="<?php echo $gatePassReYear; ?>" id="txtgatepassReturnYear" style="width:50px" disabled="disabled" /> </td>
                 <td width="16%">Gate Pass Return Date:</td>
                 <td><input type="text" name="txtgatepassDate" id="txtgatepassDate" style="width:100px" disabled="disabled" value="<?php echo($returnResult_arr['DATE']==''?date("Y-m-d"):$returnResult_arr['DATE']); ?>" /></td>
            </tr>
            <tr>
            <td>Remarks:</td>
            <td><textarea style="width:200px;" id="returnRemarks"><?php echo $returnResult_arr['REMARK']; ?></textarea></td>
            </tr>
        </table>
        </fieldset>
        <fieldset class="tableBorder" >
        <legend class="normalfnt">Gate Pass Header:</legend>
        <table width="100%" border="0" class="normalfnt">
        	<tr>
            	<td>Gate Pass No:</td>
                <td><input type="text" name="txtgatepassNo" value="<?php echo $gatePassNo?>" id="txtgatepassNo" style="width:100px" disabled="disabled"/>
                	<input type="text" name="txtgatepassYear" value="<?php echo $gatePassYear?>" id="txtgatepassYear" style="width:50px" disabled="disabled" /> </td>
                 <td>Gate Pass Date</td>
                 <td><input type="text" name="txtgatepassDate" id="txtgatepassDate" style="width:100px" disabled="disabled" value="<?php echo $searchResult_arr['DATE'] ?>" /></td>
            </tr>
            <tr>
            	 <td width="16%">Attention To</td>
                <td width="36%">
                  <input type="text" name="txtAttentionTo" id="txtAttentionTo" style="width:280px" disabled="disabled" value="<?php echo $searchResult_arr['ATTENSION_TO'] ?>"/></td>
           	    <td width="16%">Through</td>
                <td width="36%"><input type="text" name="txtThrough" id="txtThrough" style="width:280px" disabled="disabled" value="<?php echo $searchResult_arr['NAME'] ?>"/></td>
              </tr>
              <tr>
              	<td>Gate Pass To</td>
                <td><textarea name="txtGatepassTo" id="txtGatepassTo" style="width:280px;height:50px" disabled="disabled"><?php echo $searchResult_arr['GATEPASS_TO'] ?></textarea></td>
                <td>Remarks</td>
                <td><textarea name="txtRemarks" id="txtRemarks" class="txtRemarks" style="width:280px;height:50px" disabled="disabled"><?php echo $searchResult_arr['REMARK'] ?></textarea></td>
                
              </tr>
              </table>
             </fieldset>
        </td>
    </tr>
    <tr class="bordered">
    	<th colspan="8">Gate Pass</th>
    </tr>
    <tr>
    	<td colspan="2" ><div style="overflow:scroll;width:1100px;height:250px;">
              <table width="100%%" border="0" class="bordered" id="tblMain">
                <thead>
                  <tr>
                    <th width="3%">&nbsp;</th>
                    <th width="32%">Description <span class="compulsoryRed">*</span></th>
                    <th width="14%">Size</th>
                    <th width="14%">Serial No</th>
                    <th width="10%">Unit</th>
                    <th width="4%">Remaining Qty</th>
                    <th width="4%">Return Qty</th>
                    <th width="9%">Returnable Date</th>
                    <th width="10%">Return Date</th>
                  </tr>
                </thead>
                <tbody id="tblbody">
                <?php
				while($row	= mysqli_fetch_array($tableResult))
					{?>
					<tr id="<?php echo $row['ITEM_ID']?>">
                    	<td class="cls_check" style="text-align:center"><input type="checkbox" id="chk_select"  name="group[group]" class="validate[minCheckbox[1]] mouseover" /></td>
                      <td class="description"><?php echo $row['DESCRIPTION'];?></td>
                      <td width="14%" class="cls_size"><?php echo $row['SIZE']; ?></td>
                      <td width="14%" class="cls_serialNo"><?php echo $row['SERIAL_NO'];?></td>
                        <td width="10%" class="cls_unit" ><?php echo $row['strName'];?></td> 
                      <td width="7%" class="cls_qty"><?php echo $row['BAL_RETURN_QTY'];?></td>
                      <td width="7%" class="cls_qty"><input type="text" name="txt_qty" id="txt_qty" style="width:100px;text-align:right" class="clsQty" value="<?php echo ($gatePassReNo!='' & $gatePassReYear!='')?$returnQTY:$row['BAL_RETURN_QTY'];?>" /></td>
                      <td class="cls_returnableDate"><?php echo $row['RETURNABLE_DATE'];?> </td>
                      <td style="text-align:left"><input name="txtReturnDate" id="txtReturnDate" type="text" class="clsReturnDate" style="width:110px;" disabled="disabled" value=" <?php echo ($gatePassReNo!='' & $gatePassReYear!='')?$row['RETURN_DATE']:date("Y-m-d");?>" />
                      </td>
                  </tr>
				
				<?php
					}
				 ?>
		                         
                </tbody>
                </table>
                </div>
                </td>
    </tr>
    <tr align="center"><td>
    <a name="butNew"  class="button white medium" id="butNew">New</a>
    <?php
                if(($form_permision['add']||$form_permision['edit']) and $permision_save == 1)
				{
				?>
    	<a class="button white medium" id="butSave" name="butSave" >Save</a>
              <?php }?>  
              <a class="button white medium" id="butReport" name="butReport" >Report</a><a class="button white medium" id="butApprove" name="butApprove" <?php if($permision_confirm!=1){ ?> style="display:none"<?php } ?> >Approve</a>
              <a href="main.php"  class="button white medium" id="butClose">Close</a>
              </td>
              
        </tr>
</table>
</div>
</div>
</form>


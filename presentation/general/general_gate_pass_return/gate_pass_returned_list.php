<?php
require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$intUser  			= $_SESSION["userId"];
$programCode		= "P0809";
$reportID			= 925;
$formID				= 884;

$approveLevel = (int)getMaxApproveLevel();
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];

$where_string = '';
$where_array = array(
				'Status'=>'GRH.STATUS',
				'CONCAT_GATEPASS_RETURN_NO'=>"CONCAT(GRH.GATEPASS_RETURN_NO,'/',GRH.GATEPASS_RETURN_YEAR)",	
				'CONCAT_GATEPASS_NO'=>"CONCAT(GRH.GATEPASS_NO,'/',GRH.GATEPASS_YEAR )",
				'DATE'=>'GRH.DATE',
				'GATEPASS_TO'=>'GH.GATEPASS_TO',
				'ATTENSION_TO'=>'GH.ATTENSION_TO'
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
if($arr)
{
	foreach($arr as $k=>$v)
	{
		if($v['field']=='Status')
		{
			if($arr_status[$v['data']]==2)
				$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
			else
				$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
		}
		else if($where_array[$v['field']])
			$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
	}
}
if(!count($arr)>0)					 
	$where_string .= "AND GRH.DATE = '".date('Y-m-d')."'";

$sql = "SELECT SUB_1.* FROM
			(SELECT
			  CONCAT(GRH.GATEPASS_RETURN_NO,'/',GRH.GATEPASS_RETURN_YEAR )		AS CONCAT_GATEPASS_RETURN_NO,
			  GRH.GATEPASS_RETURN_NO											AS GATEPASS_RETURN_NO,
			  GRH.GATEPASS_RETURN_YEAR											AS GATEPASS_RETURN_YEAR,
			  GRH.GATEPASS_NO													AS GATEPASS_NO, 
			  GRH.GATEPASS_YEAR													AS GATEPASS_YEAR,
			  CONCAT(GRH.GATEPASS_NO,'/',GRH.GATEPASS_YEAR )					AS CONCAT_GATEPASS_NO,
			  GRH.DATE															AS DATE,
			  GH.GATEPASS_TO 													AS GATEPASS_TO,
			  GH.ATTENSION_TO 													AS ATTENSION_TO,
			  GRH.REMARK														AS REMARK,		 
			 
			  IF(GRH.STATUS=1,'Approved',IF(GRH.STATUS=10,'Canceled',IF(GRH.STATUS=0,'Rejected',IF(GRH.STATUS=-1,'Revised','Pending')))) AS Status,
			  (SELECT ROUND(SUM(GRD.QTY),2)						 		
			   FROM general_gate_pass_return_details GRD
				WHERE GRD.GATEPASS_RETURN_NO = GRH.GATEPASS_RETURN_NO
				  AND GRD.GATEPASS_RETURN_YEAR = GRH.GATEPASS_RETURN_YEAR)		AS QTY,
			  (SELECT MIN(GRD.RETURN_DATE)						 		
			   FROM general_gate_pass_return_details GRD
				WHERE GRD.GATEPASS_RETURN_NO = GRH.GATEPASS_RETURN_NO
				  AND GRD.GATEPASS_RETURN_YEAR = GRH.GATEPASS_RETURN_YEAR)		AS RETURN_DATE, ";
				  
		$sql .= "IFNULL((
				SELECT
				concat(U.strUserName,'(',max(GRA.APPROVED_DATE),')' )
				FROM
				general_gate_pass_return_approvedby GRA
				Inner Join sys_users U
					ON GRA.APPROVED_BY = U.intUserId
				WHERE
				GRA.GATEPASS_RETURN_NO  = GRH.GATEPASS_RETURN_NO AND
				GRA.GATEPASS_RETURN_YEAR =  GRH.GATEPASS_RETURN_YEAR AND
				GRA.APPROVE_LEVEL_NO =  '1' AND
				GRA.STATUS =  '0'
				),IF(((SELECT
				MP.int1Approval 
				FROM menupermision MP
				Inner Join menus M ON MP.intMenuId = M.intId
				WHERE
				M.strCode = '$programCode' AND
				MP.intUserId =  '$intUser')=1 AND GRH.STATUS>1),'Approve', '')) as `1st_Approval`, ";
				
	for($i=2; $i<=$approveLevel; $i++){		
		if($i==2){
			$approval	= "2nd_Approval";
		}
		else if($i==3){
			$approval	= "3rd_Approval";
		}
		else {
			$approval	= $i."th_Approval";
		}
		
		
		$sql .= "IFNULL(
		(
		SELECT
		concat(U.strUserName,'(',max(GRA.APPROVED_DATE),')' )
		FROM
		general_gate_pass_return_approvedby GRA
		Inner Join sys_users U ON GRA.APPROVED_BY = U.intUserId
		WHERE
		GRA.GATEPASS_RETURN_NO  = GRH.GATEPASS_RETURN_NO AND
		GRA.GATEPASS_RETURN_YEAR = GRH.GATEPASS_RETURN_YEAR AND
		GRA.APPROVE_LEVEL_NO =  '$i' AND
		GRA.STATUS =  '0' 
		),
		IF(
		((SELECT
		MP.int".$i."Approval 
		FROM menupermision MP
		Inner Join menus M 
			ON MP.intMenuId = M.intId
		WHERE
			M.strCode = '$programCode' AND
			MP.intUserId =  '$intUser')=1 AND (GRH.STATUS>1) AND (GRH.STATUS<=GRH.APPROVE_LEVEL) AND ((SELECT
		concat(U.strUserName )
		FROM
		general_gate_pass_return_approvedby GRA
		Inner Join sys_users U ON GRA.APPROVED_BY = U.intUserId
		WHERE
		GRA.GATEPASS_RETURN_NO = GRH.GATEPASS_RETURN_NO AND
		GRA.GATEPASS_RETURN_YEAR = GRH.GATEPASS_RETURN_YEAR AND
		GRA.APPROVE_LEVEL_NO = ($i-1) AND 
		GRA.STATUS = '0' )<>'')),		
		'Approve',
		if($i>GRH.APPROVE_LEVEL,'-----',''))
		
		) as `".$approval."`, "; 
		
		}
	
	$sql .= "IFNULL((SELECT
			concat(sys_users.strUserName,'(',max(general_gate_pass_return_approvedby.APPROVED_DATE),')' )
			FROM
			general_gate_pass_return_approvedby
			Inner Join sys_users ON general_gate_pass_return_approvedby.APPROVED_BY = sys_users.intUserId
			WHERE
			general_gate_pass_return_approvedby.GATEPASS_RETURN_NO  = GRH.GATEPASS_RETURN_NO AND
			general_gate_pass_return_approvedby.GATEPASS_RETURN_YEAR =  GRH.GATEPASS_RETURN_YEAR AND
			general_gate_pass_return_approvedby.APPROVE_LEVEL_NO =  '-2' AND
			general_gate_pass_return_approvedby.STATUS =  '0'
			),IF(((SELECT
			menupermision.intCancel 
			FROM menupermision 
			Inner Join menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId =  '$intUser')=1 AND 
			GRH.STATUS=1),'Cancel', '')) as `Cancel`,";	
	
	$sql .= "IFNULL((SELECT
			concat(sys_users.strUserName,'(',max(general_gate_pass_return_approvedby.APPROVED_DATE),')' )
			FROM
			general_gate_pass_return_approvedby
			Inner Join sys_users ON general_gate_pass_return_approvedby.APPROVED_BY = sys_users.intUserId
			WHERE
			general_gate_pass_return_approvedby.GATEPASS_RETURN_NO  = GRH.GATEPASS_RETURN_NO AND
			general_gate_pass_return_approvedby.GATEPASS_RETURN_YEAR =  GRH.GATEPASS_RETURN_YEAR AND
			general_gate_pass_return_approvedby.APPROVE_LEVEL_NO =  '-1' AND
			general_gate_pass_return_approvedby.STATUS =  '0'
			),IF(((SELECT
			menupermision.intRevise 
			FROM menupermision 
			Inner Join menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId =  '$intUser')=1 AND 
			GRH.STATUS=1),'Revise', '')) as `Revise`,";
					
				  
				  
	$sql .= " 'View'											AS VIEW,
			  'Report'											AS REPORT
			  FROM general_gate_pass_return_header GRH
			  
			  INNER JOIN
			  general_gate_pass_header GH
			WHERE GRH.GATEPASS_NO = GH.GATEPASS_NO AND GRH.GATEPASS_YEAR = GH.GATEPASS_YEAR
			$where_string
		)  
		AS SUB_1 WHERE 1=1";
//echo $sql;

$jq		= new jqgrid('',$db);
$col	= array();

$col["title"] 			= "Status";
$col["name"] 			= "Status";
$col["width"] 			= "2"; 						// not specifying width will expand to fill space
$col["align"] 			= "left";
$col["sortable"] 		= true; 						// this column is not sortable
$col["search"] 			= true; 						// this column is not searchable
$col["editable"] 		= false;
$col["stype"] 			= "select";
$str 					= "Pending:Pending;Approved:Approved;Rejected:Rejected;Revised:Revised;Canceled:Canceled;:All";  // all row, blank row, then all db values. ; is separator
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;

$col["title"]			= "Serial No";
$col["name"]			= "GATEPASS_RETURN_NO";
$col["width"]			= "1";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$col["hidden"]			= true;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Gate Pass Return Year";
$col["name"]			= "GATEPASS_RETURN_YEAR";
$col["width"]			= "1";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$col["hidden"]			= true;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Gate Pass Return No";
$col["name"]			= "CONCAT_GATEPASS_RETURN_NO";
$col["width"]			= "3";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$col["link"]			= '?q='.$formID.'&gatePassReturnNo={GATEPASS_RETURN_NO}&gatePassReturnYear={GATEPASS_RETURN_YEAR}';
$col["linkoptions"]		= "target='gate_pass_return.php'";
$cols[]			    	= $col;
$col					= NULL;

$col["title"] 			= "Gate Pass No";
$col["name"] 			= "CONCAT_GATEPASS_NO";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"]			= "Returned Date";
$col["name"]			= "DATE";
$col["width"]			= "2";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Gate Pass To";
$col["name"]			= "GATEPASS_TO";
$col["width"]			= "2";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Attention To";
$col["name"]			= "ATTENSION_TO";
$col["width"]			= "2";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			   		= $col;
$col					= NULL;

$col["title"]			= "Remark";
$col["name"]			= "REMARK";
$col["width"]			= "2";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;


$col["title"] 			= "Qty";
$col["name"] 			= "QTY";
$col["width"] 			= "2";
$col["align"] 			= "right";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

//BEGIN - FIRST APPROVAL {
$col["title"] 			= "1st Approval";
$col["name"] 			= "1st_Approval";
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportID.'&gatePassReturnNo={GATEPASS_RETURN_NO}&gatePassReturnYear={GATEPASS_RETURN_YEAR}&mode=Confirm';
$col["linkoptions"] 	= "target='rptGatepassReturned.php'";
$col['linkName']		= 'Approve';
$cols[] 				= $col;	
$col					= NULL;
//END	- FIRST APPROVEL }

//BEGIN - SECOND & MORE APPROVAL {
for($i=2; $i<=$approveLevel; $i++)
{
	if($i==2){
		$ap		= "2nd Approval";
		$ap1	= "2nd_Approval";
	}
	else if($i==3){
		$ap		= "3rd Approval";
		$ap1	= "3rd_Approval";
	}
	else {
		$ap		= $i."th Approval";
		$ap1	= $i."th_Approval";
	}

$col["title"] 			= $ap; // caption of column
$col["name"] 			= $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportID.'&gatePassReturnNo={GATEPASS_RETURN_NO}&gatePassReturnYear={GATEPASS_RETURN_YEAR}&mode=Confirm';
$col["linkoptions"] 	= "target='rptGatepassReturned.php'";
$col['linkName']		= 'Approve';
$cols[] 				= $col;	
$col					= NULL;
}

$d	=date('Y-m-d');
$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"},{"field":"DATE","op":"eq","data":"$d"}
     ]

}
SEARCH_JSON;

$grid["postData"] = array("filters" => $sarr ); 

$grid["caption"]		= "General Gate Pass Returned List";
$grid["multiselect"]	= false;
$grid["rowNum"]			= 20;
/*$grid["sortname"]		= "GATEPASS_NO";
$grid["sortorder"]		= "DESC";*/
$grid["autowidth"]		= true;
$grid["multiselect"]	= false;
$grid["search"]			= true;
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command=$sql;
$jq->set_columns($cols);
$jq->set_actions(array(
	"add"=>false,
	"edit"=>false,
	"delete"=>false,	
	"rowactions"=>false,
	"search"=>"advance",
	"export"=>true));
	
$out	= $jq->render('list1');

?>
<title>General Gate Pass Return List</title>

<form id="returnListing" name="returnListing" action="" method="post">
           	<div align="center" style="margin:10px;">
                	<?php echo $out ?>
                </div>
</form>

<?php
function getMaxApproveLevel()
{
	global $db;
	$appLevel = 0;
	
	$sqlp = "SELECT
			MAX(general_gate_pass_return_header.APPROVE_LEVEL) AS appLevel
			FROM general_gate_pass_return_header";					
	
	$resultp 	= $db->RunQuery($sqlp);
	$rowp		= mysqli_fetch_array($resultp);			 
	return $rowp['appLevel'];
}
?>

var para;
var objName; 
var IntervalId ;
var autoSaveStatus = 1;

jQuery(document).ready(function() {
	
	
	//IntervalId = setInterval("autoSave()",300000);
	/*//begin - {
  	jQuery("#frmMeeting").validationEngine();
	
	//-------------------------
     var select, selects, _i, _len, _results;
    if (Prototype.Browser.IE && (Prototype.BrowserFeatures['Version'] === 6 || Prototype.BrowserFeatures['Version'] === 7)) {
      return;
    }

	var a1=".msCombo";
	var a2=".msCombo-deselect";
	//alert(a1);
	
    selects = $$(a1);
    _results = [];
    for (_i = 0, _len = selects.length; _i < _len; _i++) {
      select = selects[_i];
      _results.push(new Chosen(select));
    }
    deselects = $$(a2);
    for (_i = 0, _len = deselects.length; _i < _len; _i++) {
      select = deselects[_i];
      _results.push(new Chosen(select,{allow_single_deselect:true}));
    }
   // return _results;	
	//---------------------------------
	
	jQuery('#butAddNewTask').live('click',addNewRow);	
	jQuery('#butNew').live('click',function(){	
		window.location.href = 'meetingMinutes.php';
	});

/*jQuery('textarea').live('keypress', function (event) {
		  var theEvent = event || window.event;
          var key      = theEvent.keyCode || theEvent.which;

          var keychar  = String.fromCharCode(key);
          //alert(keychar);
          var keycheck = /[a-zA-Z0-9]/;  

          if (!(key == 8   ||  
                key == 27  ||
                key == 46  || 
                key == 37  ||
				key == 32  || //space
				key == 34  || // " '
				key == 64  || // @
				key == 35  || // @
				
				key == 126  || // ~
				key == 96   || // `
				key == 96   || // !
				key == 33   || // !
				key == 36   || // $
				key == 94   || // ^
				key == 38   || // &
				key == 42   || // *
				key == 40   || // (
				key == 41   || // )
				key == 45   || // -
				key == 95   || // _
				
				key == 61   || // =
				key == 43   || // +
				key == 91   || // [
				key == 93   || // ]
				key == 123  || // {
				key == 125  || // }
				key == 124   || // |
				key == 92   || // \
				key == 58   || // :
				key == 59   || // ;
				key == 44   || // ,
				
				key == 60   || // <
				key == 62   || // >
				key == 63   || // ?
				key == 47   || // /
				
                key == 39 

				
				)) { // backspace delete  escape arrows

                if (!keycheck.test(keychar)) {          
                	theEvent.returnValue = false; //for IE

                    if (theEvent.preventDefault) 
                         theEvent.preventDefault(); //Firefox
						 //alert(key);
                    //alert ("key allowed");
                	}   
					
						
           }  
 });

*/
//end }*/
	jQuery("#frmMeeting").validationEngine();
	jQuery("#frmMeeting .chosen-select").chosen({allow_single_deselect: false , no_results_text: "Oops, nothing found!"});
	
	jQuery('#frmMeeting #butAddNewTask').die('click').live('click',addNewRow);	
	jQuery('#frmMeeting #butNew').die('click').live('click',clearForm);
	jQuery('#frmMeeting #butSave').die('click').live('click',saveData);
	jQuery('#frmMeeting #butComplete_1').die('click').live('click',complete_level1);
	jQuery('#frmMeeting #butComplete').die('click').live('click',complete);
	jQuery('#frmMeeting #butDelete').live('click',deleteData);
	jQuery('#frmMeeting #butReport').live('click',viewReport);
});

function clearForm()
{
	window.location.href = '?q=718';
}

function autoSave()
{
	if(jQuery('#hidStatus').val()==1)
		return;
	
	var minuteNo				= jQuery('#txtSerialNo').val();
	var subject					= jQuery('#txtSubject').val();
	var meetingPlace			= jQuery('#txtMeetinPlace').val();
	var arrCalledBy				= jQuery('.txtCalled').val();
	var arrAttendance			= jQuery('.txtAttendees').val();
	var arrDistribution			= jQuery('.txtDistribution').val();
	var arrOtherDistribution	= jQuery('#txtOtherDistribution').val();
	var date					= jQuery('#dtDate').val();
	var userId					= jQuery('#hidUserId').val();
	
	if(subject=='' || meetingPlace=='')
		return;
	
	showWaiting_auto();
	var data 	   = "requestType=autoSave";
	
	var arrHeader  = "{";
		arrHeader += '"minuteNo":"'+minuteNo+'",' ;
		arrHeader += '"subject":'+URLEncode_json(subject)+',' ;
		arrHeader += '"meetingPlace":'+URLEncode_json(meetingPlace)+',' ;
		arrHeader += '"arrCalledBy":"'+arrCalledBy+'",' ;
		arrHeader += '"arrAttendance":"'+arrAttendance+'",' ;
		arrHeader += '"arrDistribution":"'+arrDistribution+'",' ;
		arrHeader += '"arrOtherDistribution":"'+arrOtherDistribution+'",' ;
		arrHeader += '"userId":"'+userId+'",' ;
		arrHeader += '"date":"'+date+'"' ;				
		arrHeader += ' }';
	
	var i					= 0;
	var rcds				= 0;
	var arrDetails			= "";
	
	jQuery('.concern').each(function(){
		
		i++;
		var no 				= jQuery(this).parent().parent().find('.no').attr('id');
		var completed		= jQuery(this).parent().parent().find('.delC').attr('id');
		var concern 		= jQuery(this).val();
		var recommendation 	= jQuery(this).parent().parent().find('.recommendation').val();
		var actionPlan 		= jQuery(this).parent().parent().find('.actionPlan').val();
		var actionBy 		= jQuery(this).parent().parent().find('.txtActionBy'+i).val();
		var concernBy 		= jQuery(this).parent().parent().find('.txtConcernRaisedBy'+i).val();
		var dueDate 		=jQuery(this).parent().parent().find('.date').val();
		
		if((concern!='') &&(completed !=1))
		{
			rcds++;
			arrDetails += "{";
			arrDetails += '"concern":'+ URLEncode_json( concern )+',' ;
			arrDetails += '"recommendation":'+ URLEncode_json(recommendation) +',' ;
			arrDetails += '"actionPlan":'+ URLEncode_json(actionPlan) +',' ;
			arrDetails += '"actionBy":"'+ (actionBy==null?'':actionBy) +'",' ;
			arrDetails += '"concernBy":"'+ (concernBy==null?'':concernBy) +'",' ;
			arrDetails += '"dueDate":"'+ dueDate +'",' ;
			arrDetails += '"no":"'+ no +'"' ;
			arrDetails +=  '},';
		}
		
	});
	
	arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
	
	var arrHeader	= arrHeader;
	var arrDetails	= '['+arrDetails+']';
	
	data		   += "&arrHeader="+arrHeader+"&arrDetails="+arrDetails;

	var url 		= "presentation/meetingMinutes/addNew/meetingMinutes-db-set.php";
	jQuery.ajax({
			url:url,
			async:false,
			dataType:'json',
			type:'post',
			data:data,
	success:function(json){
			if(json.type=='pass')
			{
				jQuery('#txtSerialNo').val(json.serialNo);
				showCompleteButton();
				hideWaiting_c();
				jQuery("#butReport").show();  
				return;
			}
			hideWaiting_c();
		},
	error:function(xhr,status){
			hideWaiting_c();
			return;
		}		
	});
}
function saveData()
{	
	showWaiting_c();
	if(!validateData())
		return;
		
	var data 		= "requestType=save";
	
	var arrHeader  = "{";
		arrHeader += '"minuteId":"'+ jQuery('#txtSerialNo').val()+'",' ;
		arrHeader += '"subject":'+ URLEncode_json(jQuery('#txtSubject').val()) +',' ;
		arrHeader += '"meetingPlace":'+ URLEncode_json(jQuery('#txtMeetinPlace').val()) +',' ;
		arrHeader += '"arrCalledBy":"'+ jQuery('.txtCalled').val() +'",' ;
		arrHeader += '"arrAttendance":"'+ jQuery('.txtAttendees').val() +'",' ;
		arrHeader += '"arrDistribution":"'+ jQuery('.txtDistribution').val() +'",' ;
		arrHeader += '"arrOtherDistribution":"'+ jQuery('#txtOtherDistribution').val() +'",' ;
		arrHeader += '"userId_bkp":"'+ jQuery('#hidUserId').val() +'",' ;
		arrHeader += '"date":"'+ jQuery('#dtDate').val() +'"' ;				
		arrHeader += ' }';
	
	var i					= 0;
	var rcds				= 0;
	var errorDescFlag		= 0;
	var errorActionByFlag	= 0;
	var arrDetails			= "";
	
	
	jQuery('.concern').each(function(){
		i++;
		var no 				=	jQuery(this).parent().parent().find('.no').attr('id');
		var completed		=	jQuery(this).parent().parent().find('.delC').attr('id');
		var concern 		=	jQuery(this).val();
		var recommendation 	=	jQuery(this).parent().parent().find('.recommendation').val();
		var actionPlan 		=	jQuery(this).parent().parent().find('.actionPlan').val();
		var actionBy 		=	jQuery(this).parent().parent().find('.txtActionBy'+i).val();
		var concernBy 		=	jQuery(this).parent().parent().find('.txtConcernRaisedBy'+i).val();
		
		if(actionBy==null){actionBy='';}
		if(concernBy==null){concernBy='';}
		
		var dueDate =jQuery(this).parent().parent().find('.date').val();

		if((concern!='') &&(completed !=1))
		{
			rcds++;
			arrDetails += "{";
			arrDetails += '"concern":'+ URLEncode_json( concern )+',' ;
			arrDetails += '"recommendation":'+ URLEncode_json(recommendation) +',' ;
			arrDetails += '"actionPlan":'+ URLEncode_json(actionPlan) +',' ;
			arrDetails += '"actionBy":"'+ actionBy +'",' ;
			arrDetails += '"concernBy":"'+ concernBy +'",' ;
			arrDetails += '"dueDate":"'+ dueDate +'",' ;
			arrDetails += '"no":"'+ no +'"' ;
			arrDetails +=  '},';
		}
	});
	
	if(rcds==0){
		alert("There is no none-completed 'Concerns' to save");
		hideWaiting_c();
		return false;
	}
	
	arrDetails = arrDetails.substr(0,arrDetails.length-1);
	
	var arrHeader	= arrHeader;
	var arrDetails	= '['+arrDetails+']';
	
	data		   += "&arrHeader="	+arrHeader+"&arrDetails="	+	arrDetails;
	
	var url = "presentation/meetingMinutes/addNew/meetingMinutes-db-set.php";
	
	jQuery.ajax({
			url:url,
			async:false,
			dataType:'json',
			type:'post',
			data:data,
	success:function(json){
			jQuery('#frmMeeting #butSave').validationEngine('showPrompt', json.msg,json.type );
			if(json.type=='pass')
			{
				jQuery('#txtSerialNo').val(json.serialNo);
				//showCompleteButton();
				window.clearInterval(IntervalId);
				var t=setTimeout("alertx()",1000);
				hideWaiting_c();
				jQuery("#butReport").show();  
				return;
			}
			hideWaiting_c();
		},
	error:function(xhr,status){
			jQuery('#frmMeeting #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			var t=setTimeout("alertx()",1000);
			hideWaiting_c();
			return;
		}		
	});
}

function deleteData()
{	
	var result = confirm("Are you sure you want to Delete this ?");
	
	if(!result) 
		showWaiting_c();
	
	var minute	= jQuery('#txtSerialNo').val();
	var no 		= jQuery(this).parent().parent().find('.no').attr('id');
	var obj		= this
	
	if(jQuery(this).parent().parent().find('.concern').val()==''){
		deleteRow(obj);
		hideWaiting_c();
		return false;	
	}
	
	var data 	 = "requestType=delete";
		data	+= "&minute="+minute;
		data	+= "&no="+no;
	
	var url = "presentation/meetingMinutes/addNew/meetingMinutes-db-set.php";
	
	jQuery.ajax({
			url:url,
			async:false,
			dataType:'json',
			type:'post',
			data:data,
		success:function(json){
			jQuery(obj).parent().parent().find('#butDelete').validationEngine('showPrompt', json.msg,json.type );
			if(json.type=='pass')
			{
				var t=setTimeout("alertx()",1000);
				deleteRow(obj);
				hideWaiting_c();
				return;
			}
			hideWaiting_c();
		},
		error:function(xhr,status){
			jQuery(obj).parent().parent().find('#butDelete').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			var t=setTimeout("alertx()",1000);
			hideWaiting_c();
				return;
		}		
	});
}

function complete()
{	
	var result = confirm("Are you sure you want to Complete this ?");
	
	if (result==true) { 
		
	if (jQuery('#frmMeeting').validationEngine('validate'))   
    { 
		showWaiting_c();
		
  		var minute=jQuery('#txtSerialNo').val();
     	var no =jQuery(this).parent().parent().find('.no').attr('id');
		var obj=this;
		var obje=jQuery(this).parent().parent();
		
		var data 	 = "requestType=complete";
 			data	+= "&minute="+	minute;
			data	+= "&no="    +	no;
		
		var url = "presentation/meetingMinutes/addNew/meetingMinutes-db-set.php";
		
			jQuery.ajax({
					url:url,
					async:false,
					dataType:'json',
					type:'post',
					data:data,
				success:function(json){
					jQuery(obj).parent().parent().find('#butComplete').validationEngine('showPrompt', json.msg,json.type );
					if(json.type=='pass')
					{
						obje.find('.complete').html(json.html);
						//disableFields(obje);
						obje.find('.delC').html('');
						obje.find('.concern').attr('disabled',true);
						obje.find('.recommendation').attr('disabled',true);
						obje.find('.actionPlan').attr('disabled',true);
						obje.find('.date').attr('disabled',true);
						obje.find('.default').attr('disabled',true);
						obje.find('.resp').attr('disabled',true);
						
						obje.find('.delC').attr('id',1);
						
						var t=setTimeout("alertx()",1000);
						hideWaiting_c();
						return;
					}
						hideWaiting_c();
				},
				error:function(xhr,status){
					jQuery(obj).parent().parent().find('#butComplete').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",1000);
					hideWaiting_c();
						return;
				}
				
			});
		}
	}	
}

function complete_level1()
{	
	var result = confirm("Are you sure you want to Complete this ?");
	
	if (result==true) { 
		
	if (jQuery('#frmMeeting').validationEngine('validate'))   
    { 
		showWaiting_c();
		
  		var minute=jQuery('#txtSerialNo').val();
     	var no =jQuery(this).parent().parent().find('.no').attr('id');
		var obj=this;
		var obje=jQuery(this).parent().parent();
		
		var data 	 = "requestType=complete_level1";
 			data	+= "&minute="+	minute;
			data	+= "&no="    +	no;
		
		var url = "presentation/meetingMinutes/addNew/meetingMinutes-db-set.php";
		
			jQuery.ajax({
					url:url,
					async:false,
					dataType:'json',
					type:'post',
					data:data,
				success:function(json){
					jQuery(obj).parent().parent().find('#butComplete_1').validationEngine('showPrompt', json.msg,json.type );
					if(json.type=='pass')
					{
						obje.find('.complete_1').html(json.html);
						//disableFields(obje);
						obje.find('.delC').html('');
						obje.find('.concern').attr('disabled',true);
						obje.find('.recommendation').attr('disabled',true);
						obje.find('.actionPlan').attr('disabled',true);
						obje.find('.date').attr('disabled',true);
						obje.find('.default').attr('disabled',true);
						obje.find('.resp').attr('disabled',true);
						
						obje.find('.delC').attr('id',1);
						
						var t=setTimeout("alertx()",1000);
						hideWaiting_c();
						return;
					}
						hideWaiting_c();
				},
				error:function(xhr,status){
					jQuery(obj).parent().parent().find('#butComplete_1').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",1000);
					hideWaiting_c();
						return;
				}
				
			});
		}
	}	
}

function viewReport()
{
	if(jQuery('#txtSerialNo').val()!=''){
		window.open('?q=995&serialNo='+jQuery('#txtSerialNo').val());	
	}
	else
		alert("There is no Meeting Minute to view");
}

function alertx()
{
	jQuery('#frmMeeting #butSave').validationEngine('hide')	;
	jQuery('#frmMeeting #butComplete').validationEngine('hide')	;
	jQuery('#frmMeeting #butDelete').validationEngine('hide')	;
	
}
function pageSubmitOnChange()
{
	var date 		= jQuery('#dtDate').val();
	var location 	= jQuery('#cboLocation').val();
	
	if(date==''){
		alert('Please select the date');
		return false;
	}
	else if(location==''){
		alert('Please select the location');
		return false;
	}
	
	if(date!='' && location!='')
	{
		document.getElementById('frmMeeting').submit();
	}
}
 
function deleteRow(obj)
{
 		jQuery(obj).parent().parent().remove();
} 

function clearGrid()
{
	var rowCount = document.getElementById('tblPopup').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblPopup').deleteRow(1);
	}
}

function addNewRow()
{
	var tempId=1;
	jQuery('.no').each(function(){
	 var no =parseFloat(jQuery(this).attr('id'))	;
		if(no>tempId){
			tempId=no;
		}
	});
	
	var no			= parseFloat(tempId)+1;	
	var names		= jQuery('#divSuggestNames').html();
	var ids			= jQuery('#divSuggestIds').html();

	var rowCount 	= document.getElementById('tblMain').rows.length;
	var a 			= rowCount-1;
	var p 			= rowCount-2;
	var i			= a;
	var str			= '<select data-placeholder="Select Some Options..." class="txtActionBy'+a+' resp msCombo chosen-select" multiple style="width:188px; height:50px" >'+jQuery('#divSuggestStr').html()+'</select>';
	var str1		= '<select data-placeholder="Select Some Options..." class="txtConcernRaisedBy'+a+'  cons msCombo chosen-select" multiple style="width:188px; height:50px" >'+jQuery('#divSuggestStr').html()+'</select>';

	var divah		= 'txtActionBy'+no;
	var divph		= 'txtActionBy'+tempId;
	var diva		= '.txtActionBy'+no;
	var divp		= '.txtActionBy'+tempId;
	
	var divah1		= 'txtConcernRaisedBy'+no;
	var divph1		= 'txtConcernRaisedBy'+tempId;
	var diva1		= '.txtConcernRaisedBy'+no;
	var divp1		= '.txtConcernRaisedBy'+tempId;
	
	document.getElementById('tblMain').insertRow(rowCount-1);
	rowCount 	 	= document.getElementById('tblMain').rows.length;
	//document.getElementById('tblMain').rows[rowCount-2].innerHTML = document.getElementById('tblMain').rows[rowCount-3].innerHTML;
	document.getElementById('tblMain').rows[rowCount-2].innerHTML = loadRow();
	jQuery('#tblMain tr:eq('+a+')').find('.no').html(rowCount-2);
 	jQuery('#tblMain tr:eq('+a+')').find(divp).attr('class',divah);
  	jQuery('#tblMain tr:eq('+a+')').find('.action').html(str);
 	jQuery('#tblMain tr:eq('+a+')').find('.desc').val('');
	jQuery('#tblMain tr:eq('+a+')').find('.complete').html('&nbsp;')
 	jQuery('#tblMain tr:eq('+a+')').find('.no').attr('id',no);
 	jQuery('#tblMain tr:eq('+a+')').find('.no').html(no);
	jQuery('#tblMain tr:eq('+a+')').find('.delC').html('<img border="0" src="images/del.png" alt="Delete" name="butDelete" class="clsDel mouseover" id="butDelete" tabindex="25"/>')
 	jQuery('#tblMain tr:eq('+a+')').find('.concern').html('');
 	jQuery('#tblMain tr:eq('+a+')').find('.recommendation').html('');
 	jQuery('#tblMain tr:eq('+a+')').find('.actionPlan').html('');
  	jQuery('#tblMain tr:eq('+a+')').find('.concern').removeAttr('disabled');
 	jQuery('#tblMain tr:eq('+a+')').find('.recommendation').removeAttr('disabled');
 	jQuery('#tblMain tr:eq('+a+')').find('.actionPlan').removeAttr('disabled');
 	jQuery('#tblMain tr:eq('+a+')').find('.date').removeAttr('disabled');	
	jQuery('#tblMain tr:eq('+a+')').find('.date').attr('id','date'+(rowCount-2));
	jQuery('#tblMain tr:eq('+a+')').find('.delC').attr('id','');
   	jQuery('#tblMain tr:eq('+a+')').find('.raisedBy').html(str1);
	//comboAttr1(i);
	//comboAttr2(i);
	jQuery("#frmMeeting .chosen-select").chosen({allow_single_deselect: false , no_results_text: "Oops, nothing found!"});
  }

function add_new_row(table,rowcontent){
        if (jQuery(table).length>0){
            if (jQuery(table+' > tbody').length==0) jQuery(table).append('<tbody />');
            (jQuery(table+' > tr').length>0)?jQuery(table).children('tbody:last').children('tr:last').append(rowcontent):jQuery(table).children('tbody:last').append(rowcontent);
        }
    }
//------------------------------------------------------------------------------
function showWaiting_c()
{
	var popupbox = document.createElement("div");
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var scrollH = (document.body.scrollHeight);
	popupbox.id = "divBackGroundBalck";
	popupbox.style.position = 'absolute';
	popupbox.style.zIndex = 100;
	popupbox.style.textAlign = 'center';
	popupbox.style.left = 0 + 'px';
	popupbox.style.top = 0 + 'px'; 
	popupbox.style.background="#000000"; 
	popupbox.style.width = screen.width + 'px';
	popupbox.style.height =  (scrollH)+ 'px';
	popupbox.style.opacity = 0.5;
	popupbox.style.color = "#FFFFFF";
	document.body.appendChild(popupbox);
	var popupbox1 = document.createElement("div");
	popupbox1.id = "divBackgroundImg";
	popupbox1.style.position = 'absolute';
	popupbox1.style.zIndex = 101;
	popupbox1.style.verticalAlign = 'center';
	popupbox1.style.left =  windowWidth/2-100/2 +'px';
	popupbox1.style.top = (jQuery(window).scrollTop()+200) + 'px'; 
	popupbox1.style.width = '100px';
	popupbox1.style.height =  '100px';
	popupbox1.style.opacity = 1;
	popupbox1.style.color = "#FFFFFF";
	document.body.appendChild(popupbox1);
	document.getElementById('divBackgroundImg').innerHTML = "<img src=\"images/loading_go.gif\" /><span class=\"normalfnt\" style=\"color:white;\">Please Wait...</span>";	
}

function showWaiting_auto()
{
	var popupbox = document.createElement("div");
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var scrollH = (document.body.scrollHeight);
	popupbox.id = "divBackGroundBalck";
	popupbox.style.position = 'absolute';
	popupbox.style.zIndex = 100;
	popupbox.style.textAlign = 'center';
	popupbox.style.left = 0 + 'px';
	popupbox.style.top = 0 + 'px'; 
	// popupbox.style.background="#000000"; 
	popupbox.style.width = screen.width + 'px';
	popupbox.style.height =  (scrollH)+ 'px';
	//popupbox.style.opacity = 0.5;
	popupbox.style.color = "#FFFFFF";
	document.body.appendChild(popupbox);
	var popupbox1 = document.createElement("div");
	popupbox1.id = "divBackgroundImg";
	popupbox1.style.position = 'absolute';
	popupbox1.style.zIndex = 101;
	popupbox1.style.verticalAlign = 'center';
	popupbox1.style.left =  windowWidth/2-100/2 +'px';
	popupbox1.style.top = (jQuery(window).scrollTop()+200) + 'px'; 
	popupbox1.style.width = '100px';
	popupbox1.style.height =  '100px';
	popupbox1.style.opacity = 1;
	popupbox1.style.color = "#000000";
	document.body.appendChild(popupbox1);
	document.getElementById('divBackgroundImg').innerHTML = "<img src=\"images/auto_save.gif\" /><br><span class=\"normalfnt\" style=\"color:black;\">Auto Saving....</span>";	
}

function hideWaiting_c()
{
	try
	{
		var box = document.getElementById('divBackGroundBalck');
		box.parentNode.removeChild(box);
		
		var box1 = document.getElementById('divBackgroundImg');
		box1.parentNode.removeChild(box1);
		
	}
	catch(err)
	{        
	}	
}

function validateData()
{
  	if((jQuery('#txtSubject').val()=='') || (jQuery('#txtSubject').val()==null)){
			alert("Please Enter 'Subject'");
			jQuery('#txtSubject').focus();
			hideWaiting_c();
			return false;
		}
 	else if((jQuery('#txtMeetinPlace').val()=='') || (jQuery('#txtMeetinPlace').val()==null)){
			alert("Please Enter 'Meeting Place'");
			jQuery('#txtMeetinPlace').focus();
			hideWaiting_c();
			return false;
		}
/* 	else if((jQuery('.txtCalled').val()=='') || (jQuery('.txtCalled').val()==null)){
			alert("Please Enter 'Called By'");
			hideWaiting_c();
			return false;
		}
 	else if((jQuery('.txtAttendees ').val()=='') || (jQuery('.txtAttendees').val()==null)){
			alert("Please Enter 'Attendees'");
			hideWaiting_c();
			return false;
		}
*/		
		var i=0;
		jQuery('.concern').each(function(){
 			var concern 		=	jQuery(this).val();
 			if((concern!=''))
			{
				i++;
			}
		});
		
		if(i==0){
			alert("Please enter atleast one 'Concern' to save");
			hideWaiting_c();
			return false;
		}
		
	
	return true;
}

function disableFields(obje){
}

function comboAttr1(i){
 // document.observe('dom:loaded', function(evt2) {
	  
     var select, selects, _i, _len, _results;
    if (Prototype.Browser.IE && (Prototype.BrowserFeatures['Version'] === 6 || Prototype.BrowserFeatures['Version'] === 7)) {
      return;
    }
	var a1=".txtActionBy"+i;
	var a2=".txtActionBy"+i+"-deselect";
	
 	//-----------------------
    selects = $$(a1);
    _results = [];
    for (_i = 0, _len = selects.length; _i < _len; _i++) {
      select = selects[_i];
      _results.push(new Chosen(select));
    }
    deselects = $$(a2);
    for (_i = 0, _len = deselects.length; _i < _len; _i++) {
      select = deselects[_i];
      _results.push(new Chosen(select,{allow_single_deselect:true}));
    }
    return _results;
 	//-----------------------
}

function comboAttr2(i){
	//----------------------------------------
     var select, selects, _i, _len, _results;
    if (Prototype.Browser.IE && (Prototype.BrowserFeatures['Version'] === 6 || Prototype.BrowserFeatures['Version'] === 7)) {
      return;
    }
	var a1=".txtConcernRaisedBy"+i;
	var a2=".txtConcernRaisedBy"+i+"-deselect";
	
  
    selects = $$(a1);
    _results = [];
    for (_i = 0, _len = selects.length; _i < _len; _i++) {
      select = selects[_i];
      _results.push(new Chosen(select));
    }
    deselects = $$(a2);
    for (_i = 0, _len = deselects.length; _i < _len; _i++) {
      select = deselects[_i];
      _results.push(new Chosen(select,{allow_single_deselect:true}));
    }
    return _results;
 	//-----------------------
}

function showCompleteButton(){
 		jQuery('.concern').each(function(){
 			if((jQuery(this).parent().parent().find('.delC').attr('id')!=1) && (jQuery(this).val() !='')){
			 jQuery(this).parent().parent().find('.complete').html('<a id="butComplete" class="button green medium" style="" name="butComplete"> Complete </a>');
			}
 		});
	
}

function quote(string)
{
	string = string.replace(/'/gi,"\\'");
	string = string.replace(/"/gi,'\\"');
	return string;	
}

function loadRow(){
		var rowTemplate;
		var data 	= "requestType=loadRow";
		var url 	= "presentation/meetingMinutes/addNew/meetingMinutes-db-set.php";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:data,
			async:false,
			success:function(json){
					 rowTemplate		=	json.html;
					// return rowTemplate;
			}
		});
					 return rowTemplate;
}
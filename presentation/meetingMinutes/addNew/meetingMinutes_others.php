<script type="application/javascript" src="presentation/meetingMinutes/addNew/meetingMinutes-js_others.js"></script>
<title>Meeting Minutes</title>

<form id="frmMeeting" name="frmMeeting" autocomplete="off" action="" method="post">
  <div align="center">
    <div class="trans_layoutXL" style="width:1300px"  align="center">
      <div class="trans_text">Meeting Minutes</div>
      <table width="100%" border="0" class="">
        <tr>
          <td  class="normalfnt" colspan="3" align="center"><fieldset class="tableBorder_allRound">
            <table width="100%">
              <tr>
                <td width="3%">&nbsp;</td>
                <td width="13%" class="normalfnt"><b>Meeting minute</b></td>
                <td width="1%" align="center" valign="middle"><strong>:</strong></td>
                <td width="10%" class="normalfnt" id="serialNo"><input <?php if($editMode==0){ ?>disabled<?php } ?>  id="txtSerialNo" class="normalfnt" style="width:90px;text-align:left" type="text" value="<?php echo $serialDisplay ?>" readonly/><input type="hidden" id="hidStatus" name="hidStatus" value="<?php echo($serialDisplay==''?0:1); ?>" /></td>
                <td width="4%">&nbsp;</td>
                <td width="2%" align="center" valign="middle">&nbsp;</td>
                <td width="22%">&nbsp;</td>
                <td width="8%" class="normalfnt"><b>Create Date</b></td>
                <td width="1%" align="center" valign="middle"><strong>:</strong></td>
                <td width="31%"><?php echo $createdDate; ?></td>
                <td width="5%">&nbsp;</td>
              </tr>
              <tr>
                <td >&nbsp;</td>
                <td class="normalfnt"><b>Subject</b></td>
                <td align="center" valign="middle"><strong>:</strong></td>
                <td colspan="4" ><span class="normalfnt"><?php echo $subject; ?></span>                  <div id="loginName" style="display:none"><?php //echo $_SESSION["email"]; ?></div></td>
                <td class="normalfnt" ><b>Meeting Date</b></td>
                <td align="center" ><strong>:</strong></td>
                <td ><?php echo $date; ?></td>
                <td >&nbsp;</td>
                </tr>
              <tr>
                <td >&nbsp;</td>
                <td class="normalfnt"><b>Meeting Place</b></td>
                <td align="center" valign="middle"><strong>:</strong></td>
                <td ><span class="normalfnt"><?php echo $place; ?></span></td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td ><b>Create By</b></td>
                <td align="center" ><strong>:</strong></td>
                <td ><?php echo $createdByName; ?></td>
                <td >&nbsp;</td>
              </tr>
              <tr>
                <td >&nbsp;</td>
                <td class="normalfnt"><b>Called by</b></td>
                <td align="center" valign="middle"><strong>:</strong></td>
                  <?php
					$sql = $objmemget->userList();
					$calledBy = "";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
							foreach($calledByArr as $x)
							{
								if($x==$row['intUserId']){
									if($calledBy=='')
									$calledBy .= $row['strFullName'];
									else
									$calledBy .= ", ".$row['strFullName'];
								}
							}
					} ?>
                 <td colspan="8" ><span class="normalfnt"><?php echo $calledBy; ?></span></td>
              </tr>
              <tr>
                <td >&nbsp;</td>
                <td  class="normalfnt"><b>Attendees</b></td>
                <td align="center" valign="middle"><strong>:</strong></td>
                  <?php
					$sql = $objmemget->userList();
					$attendees = "";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
							foreach($attendeesArr as $x)
							{
								if($x==$row['intUserId']){
									if($attendees=='')
									$attendees .= $row['strFullName'];
									else
									$attendees .= ", ".$row['strFullName'];
								}
							}
					} ?>
                 <td colspan="8" ><span class="normalfnt"><?php echo $attendees; ?></span></td>
              </tr>
              <tr>
                <td >&nbsp;</td>
                <td  class="normalfnt"><b>Distribution</b></td>
                <td align="center" valign="middle"><strong>:</strong></td>
                  <?php
					$sql = $objmemget->userList();
					$distribution = "";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
							foreach($distributionArr as $x)
							{
								if($x==$row['intUserId']){
									if($distribution=='')
									$distribution .= $row['strFullName'];
									else
									$distribution .= ", ".$row['strFullName'];
								}
							}
					} ?>
                <td colspan="4" ><span class="normalfnt"><?php echo $distribution; ?></span></td>
                <td ><span class="normalfnt"></span></td>
                <td ><span class="normalfnt"></span></td>
                <td >&nbsp;</td>
                <td ><span id="spanCreateBy" class="normalfnt"></span></td>
              </tr>
              <tr>
                <td >&nbsp;</td>
                <td  class="normalfnt"><b>Other Distribution</b></td>
                <td align="center" valign="middle"><strong>:</strong></td>
                <td colspan="4" ><span class="normalfnt"><?php echo $otherDistribution; ?></span></td>
				  <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
           </table>
          </fieldset></td>
        </tr>
        <tr>
          <td colspan="3" align="center"><table width="100%" class="bordered" id="tblMain" >
            <thead>
              <tr class="gridHeader">
                <th width="3%" style="display: none" >Del</th>
                <th width="2%" >No</th>
                <th width="9%" >Concern <span class="compulsoryRed">*</span></th>
                <th width="9%" >Recommendation</th>
                <th width="15%" >Raised By</th>
                <th width="11%" >Action Plan</th>
                <th width="15%" >Responsibility</th>
                  <th width="8%" >Due Date <span class="compulsoryRed">*</span></th>
                <th width="9%" nowrap="nowrap" >Initial Due Date</th>
                <th width="9%" >Complete (1st level)</th>
                <th width="10%" >Complete (2nd level)</th>
              </tr>
            </thead>
            <tbody>
              <?php
			  		$sql=$objmemget->formDetails($serialNo);
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$i++;
					$completePermission_level_1=$objmemget->getCompletePermission($intUser,$serialNo,$row['TASK_ID']);	
					$completePermission=$objmemget->getFinalCompletePermision($intUser,'718');	
					$actionByArr = explode(",", $row['RESPONSIBLE_LIST']);
					$concernByArr = explode(",", $row['CONCERN_RAISED_BY']);
  					
					$disable='';

					if($row['COMPLETED_FLAG']==1){ 
						$disable='disabled="disabled"';
						$compdBy=$row['strFullName'];
						$compDate=$row['COMPETED_DATE'];
						$comp=$compdBy.' ('.$compDate.')';
					}
					if($row['COMPLETED_FLAG_LEVEL_1']==1){ 
						$disable='disabled="disabled"';
						$compdBy_level_1=$row['strFullName_level_1'];
						$compDate_level_1=$row['COMPETED_DATE_LEVEL_1'];
						$comp_level_1=$compdBy_level_1.' ('.$compDate_level_1.')';
					}
						
					if($editMode==0){
						$disable='disabled="disabled"';
					}
				  ?>
              <tr id="<?php echo $row['TASK_ID'] ?>" class="normalfnt">
                <td  style="display: none" bgcolor="#FFFFFF" class="normalfntMid delC" id="<?php echo $row['COMPLETED_FLAG']; ?>"><?php if(($deleteMode==1) &&  ($row['COMPLETED_FLAG']!=1) ){  ?>
                  <img <?php /*?> style="display:none" <?php */?>border="0" src="images/del.png" alt="Delete" name="butDelete" class="clsDel mouseover" id="butDelete" tabindex="25"/>
                  <?php } ?>
                  &nbsp;</td>
                <td bgcolor="#FFFFFF" class="normalfntMid no"  id="<?php echo $row['TASK_ID']; ?>"><?php echo $row['TASK_ID']; ?>&nbsp;</td>
                <td bgcolor="#FFFFFF" class="normalfnt"><textarea <?php echo $disable  ?> name="txtRemarks" id="txtRemarks" cols="15" rows="5" class="concern<?php /*?> validate[required]<?php */?>"><?php
				  echo	str_replace("<br>", "\n",$row['CONCERN']);  
				  ?></textarea></td>
                <td bgcolor="#FFFFFF" class="normalfnt"  valign="middle"><textarea <?php echo $disable  ?> name="txtRecommendation" id="txtRecommendation" cols="15" rows="5" class="recommendation<?php /*?> validate[required]<?php */?>"><?php echo $row['RECOMMENDATION']; ?></textarea></td>
                <td bgcolor="#FFFFFF" class="normalfnt raisedBy"  valign="middle"><select <?php echo $disable  ?> name="select" multiple="multiple" class="txtConcernRaisedBy<?php echo $i; ?> cons msCombo chosen-select" style="width:188px; height:50px" data-placeholder="">
                  <?php
					$sqlu = $objmemget->userList();
			
		$html = "<option value=\"\"></option>";
		$resultu = $db->RunQuery($sqlu);
		while($rowu=mysqli_fetch_array($resultu))
		{
				$selected=0;
				foreach($concernByArr as $x)
				{
					if($x==$rowu['intUserId']){
					  $selected=1;	
					}
				}
					if($selected==1){
						$html .= "<option value=\"".$rowu['intUserId']."\" selected=\"selected\">".$rowu['strFullName']."</option>";
					}
					else{
						$html .= "<option value=\"".$rowu['intUserId']."\">".$rowu['strFullName']."</option>";
					}
 		}
		echo $html;
						?>
                </select></td>
                <td bgcolor="#FFFFFF" class="normalfnt"  valign="middle"><textarea <?php echo $disable  ?> name="txtActionPlan" id="txtActionPlan" cols="20" rows="5" class="actionPlan<?php /*?> validate[required]<?php */?>"><?php echo $row['ACTION_PLAN']; ?></textarea></td>
                <td bgcolor="#FFFFFF" class="normalfnt action"  valign="middle"><select <?php echo $disable  ?> name="select" multiple="multiple" class="txtActionBy<?php echo $i; ?> resp msCombo chosen-select" style="width:188px; height:50px" data-placeholder="">
                  <?php
		$sqlu = $objmemget->userList();
			
		$html = "<option value=\"\"></option>";
		$resultu = $db->RunQuery($sqlu);
		while($rowu=mysqli_fetch_array($resultu))
		{
				$selected=0;
 				foreach($actionByArr as $x)
				{
					if($x==$rowu['intUserId']){
					  $selected=1;	
					}
				}
					if($selected==1){
						$html .= "<option value=\"".$rowu['intUserId']."\" selected=\"selected\">".$rowu['strFullName']."</option>";
					}
					else{
						$html .= "<option value=\"".$rowu['intUserId']."\">".$rowu['strFullName']."</option>";
					}
		}
		echo $html;
						?>
                </select></td>
                <td bgcolor="#FFFFFF" class="normalfntMid" valign="middle" nowrap="nowrap"><input <?php echo $disable  ?> name="dtDate<?php echo $row['TASK_ID']; ?>" type="text" value="<?php echo $row['DUE_DATE'] ?>" class="txtbox date validate[required]" id="date<?php echo $row['TASK_ID']; ?>" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="<?php /*?>return ControlableKeyAccess(event);<?php */?>"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td bgcolor="#FFFFFF" class="normalfntMid iniDueDate" align="centet" nowrap="nowrap"><input <?php echo $disable  ?> name="dtDateExt<?php echo $row['TASK_ID']; ?>" type="text" value="<?php echo $row['INITIAL_DUE_DATE'] ?>" class="txtbox dateExt" id="dateExt<?php echo $row['TASK_ID']; ?>" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="<?php /*?>return ControlableKeyAccess(event);<?php */?>"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td bgcolor="#FFFFFF" class="normalfntMid complete_1" align="centet">&nbsp;<?php if($row['COMPLETED_FLAG_LEVEL_1']==1){   echo $comp_level_1;    } else {  ?>
                  <?php if($completePermission_level_1==1){  ?>
                  <a id="butComplete_1" class="button pink medium" style="" name="butComplete_1"><font color="#000000">Complete</font></a>
                 <?php } ?>
                 <?php } ?></td>
                <td bgcolor="#FFFFFF" class="normalfntMid complete" align="centet">&nbsp;
                  <?php if($row['COMPLETED_FLAG']==1){   echo $comp;    } else {  ?>
                  <?php if($completePermission==1){  ?>
                  <a id="butComplete" class="button green medium" style="" name="butComplete">Complete</a>
                  <?php } ?>
                  <?php } ?></td>
              </tr>
              <?php
					}
                    $savedRows=$i;
				  ?>
              <?php
				  if($i==0){
					  for($i=1; $i<6; $i++)
					  {
 					 ?>
              <?php
					  }
                    $savedRows=$i;
				  }
				  
				  ?>
              <tr>
                <td colspan="11" align="left"></td>
              </tr>
            </tbody>
          </table></td>
        </tr>
        <tr>
          <td height="34" colspan="3"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            <tr>
              <td width="100%" align="center" bgcolor="">
                <a id="butReport" class="button white medium" <?php if($serialNo==''){ ?>style="display:none"<?php } ?> name="butReport"  > Report </a>
                <a id="butClose" class="button white medium" name="butClose" href="main.php">Close</a></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td colspan="3" class="normalfnt"><?php
		$sql = $objmemget->userList();
			
 					$result = $db->RunQuery($sql);
					$id="'";
					while($row=mysqli_fetch_array($result))
					{		
							$id=$row['intUserId'];
							$name=$row['strFullName'];
						
							$names .=  $row['strFullName'].",";
							$ids .=  $row['intUserId'].",";
 							 $str .=" { 
							 id: '$id',
								userName: '$name'
							},";
					}
				$names = substr($names, 0, -1);	
				$ids = substr($ids, 0, -1);	
				$str = substr($str, 0, -1);	
 				?>
              
   
	<script type="text/javascript">
             </script>
  
             <?php
		$sqlu = $objmemget->userList();
			
		$html = "<option value=\"\"></option>";
		$resultu = $db->RunQuery($sqlu);
		while($rowu=mysqli_fetch_array($resultu))
		{
				$html .= "<option value=\"".$rowu['intUserId']."\">".$rowu['strFullName']."</option>";
		}
		  $html;
?>
            <div style="display:none" id="divSuggestNames"><?php echo $names; ?></div>
            <div style="display:none" id="divSuggestIds"><?php echo $ids; ?></div>
            <div style="display:none" id="divSuggestStr"><?php echo $html; ?></div></td>
        </tr>
      </table>
    </div>
  </div>
</form>
<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"> 
  <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>--> 
</div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
<script type="application/javascript" src="presentation/meetingMinutes/addNew/meetingMinutes-js.js"></script>
<title>Meeting Minutes</title>

<form id="frmMeeting" name="frmMeeting" autocomplete="off" action="" method="post">
  <div align="center">
    <div class="trans_layoutXL" style="width:1450px"  align="center">
      <div class="trans_text">Meeting Minutes</div>
      <table width="100%" border="0" class="">
        <tr>
          <td  class="normalfnt" colspan="3" align="center"><fieldset class="tableBorder_allRound">
            <table width="100%" border="0" class="">
              <tr>
                <td width="10%" class="normalfnt">Minute No</td>
                <td width="66%"><input <?php if($editMode==0){ ?>disabled<?php } ?>  id="txtSerialNo" class="normalfnt" style="width:90px;text-align:left" type="text" value="<?php echo $serialDisplay ?>" readonly/><input type="hidden" id="hidStatus" name="hidStatus" value="<?php echo($serialDisplay==''?0:1); ?>" /></td>
                <td width="9%" class="normalfnt">Create Date</td>
                <td width="15%" colspan="2"><input disabled="disabled" name="dtDateC" type="text" value="<?php if($createdDate){ echo $createdDate; }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtDateC" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
              <tr>
                <td class="normalfnt">Subject <span class="compulsoryRed">*</span></td>
                <td><textarea name="txtSubject" id="txtSubject" cols="100" rows="1" class="validate[required]" <?php if($editMode==0){ ?>disabled<?php } ?>><?php echo $subject; ?></textarea>&nbsp;</td>
                <td class="normalfnt">Meeting Date</td>
                <td colspan="2" ><input <?php if($editMode==0){ ?>disabled<?php } ?> name="dtDate" type="text" value="<?php if($date){ echo $date; }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtDate" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
              <tr>
                <td  class="normalfnt">Meeting Place <span class="compulsoryRed">*</span></td>
                <td colspan="4" valign="middle"  ><textarea <?php if($editMode==0){ ?>disabled<?php } ?> name="txtMeetinPlace" id="txtMeetinPlace" cols="100" rows="1" class="validate[required]"><?php echo $place; ?></textarea></td>
               </tr>
              <tr>
                <td class="normalfnt">Called by</td>
                <td colspan="4" ><select <?php if($editMode==0){ ?>disabled<?php } ?> name="select" multiple="multiple" class="txtCalled normalfnt msCombo chosen-select" style="width:733px; height:50px" tabindex="4" >
                  <?php
					$sql = $objmemget->userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected=0;
   				foreach($calledByArr as $x)
				{
					if($x==$row['intUserId']){
					  $selected=1;	
					}
				}
					if($selected==1){
 						$html .= "<option value=\"".$row['intUserId']."\" selected=\"selected\">".$row['strFullName']."</option>";
					}
					else{
						$html .= "<option value=\"".$row['intUserId']."\">".$row['strFullName']."</option>";
					}
 		}
		echo $html;
						?>
                </select></td>
              </tr>
              <tr>
                <td class="normalfnt">Attendees</td>
                <td colspan="4" ><select <?php if($editMode==0){ ?>disabled<?php } ?> name="select" multiple="multiple" class="txtAttendees normalfnt msCombo chosen-select" style="width:733px; height:50px" tabindex="4" data-placeholder="">
                  <?php
					$sql = $objmemget->userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected=0;
  				foreach($attendeesArr as $x)
				{
					if($x==$row['intUserId']){
					  $selected=1;	
					}
				}
					if($selected==1){
 						$html .= "<option value=\"".$row['intUserId']."\" selected=\"selected\">".$row['strFullName']."</option>";
					}
					else{
						$html .= "<option value=\"".$row['intUserId']."\">".$row['strFullName']."</option>";
					}
 		}
		echo $html;
						?>
                </select></td>
              </tr>
              <tr>
                <td class="normalfnt">Distribution</td>
                <td><select <?php if($editMode==0){ ?>disabled<?php } ?> name="select" multiple="multiple" class="txtDistribution normalfnt msCombo chosen-select" style="width:733px; height:50px" tabindex="4" data-placeholder="">
                  <?php
					$sql = $objmemget->userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected=0;
 				foreach($distributionArr as $x)
				{
					if($x==$row['intUserId']){
					  $selected=1;	
					}
				}
					if($selected==1){
 						$html .= "<option value=\"".$row['intUserId']."\" selected=\"selected\">".$row['strFullName']."</option>";
					}
					else{
						$html .= "<option value=\"".$row['intUserId']."\">".$row['strFullName']."</option>";
					}
 		}
		echo $html;
						?>
                </select></td>
                <td class="normalfnt"><input type="hidden" id="hidUserId" name="hidUserId" value="<?php echo $intUser; ?>" /></td>
                <td class="normalfnt" colspan="2" ></td>
              </tr>
<tr>
                <td class="normalfnt"></td>
                <td ><textarea <?php if($editMode==0){ ?>disabled<?php } ?> name="txtOtherDistribution" id="txtOtherDistribution" cols="100" rows="1" class="" title="Type other distribution e-mails list here"><?php echo $otherDistribution; ?></textarea></td>
                <td class="normalfnt"><?php if($serialNo!='') {?>Created By<?php } ?></td>
                <td class="normalfnt" colspan="2" ><input name="created"<?php if($serialNo=='') {?> style="display:none; width:150px"<?php } ?> type="text" readonly disabled="disabled" value="<?php echo $createdByName; ?>" /></td>
              </tr>
              </table>
          </fieldset></td>
        </tr>
        <tr>
          <td colspan="3" align="center"><table width="100%" class="bordered" id="tblMain" >
            <thead>
              <tr class="gridHeader">
                <th width="3%" >Del</th>
                <th width="2%" >No</th>
                <th width="18%" >Concern <span class="compulsoryRed">*</span></th>
                <th width="18%" >Recommendation</th>
                <th width="9%" >Raised By</th>
                <th width="15%" >Action Plan</th>
                <th width="19%" >Responsibility</th>
                <th width="10%" >Due Date <span class="compulsoryRed">*</span></th>
                <th width="6%" nowrap="nowrap" >Initial Due Date</th>
                <th width="6%" style="display: none" >Complete</th>
              </tr>
            </thead>
            <tbody>
              <?php
			  		$sql=$objmemget->formDetails($serialNo);
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$i++;
					$completePermission=$objmemget->getCompletePermission($intUser,$serialNo,$row['TASK_ID']);	
					$actionByArr = explode(",", $row['RESPONSIBLE_LIST']);
					$concernByArr = explode(",", $row['CONCERN_RAISED_BY']);
  					
					$disable='';

					if($row['COMPLETED_FLAG']==1){ 
						$disable='disabled="disabled"';
						$compdBy=$row['strFullName'];
						$compDate=$row['COMPETED_DATE'];
						$comp=$compdBy.' ('.$compDate.')';
					}
					if($editMode==0){
						$disable='disabled="disabled"';
					}
				  ?>
              <tr id="<?php echo $row['TASK_ID'] ?>" class="normalfnt">
                <td bgcolor="#FFFFFF" class="normalfntMid delC" id="<?php echo $row['COMPLETED_FLAG']; ?>"><?php if(($deleteMode==1) &&  ($row['COMPLETED_FLAG']!=1) ){  ?>
                  <img <?php /*?> style="display:none" <?php */?>border="0" src="images/del.png" alt="Delete" name="butDelete" class="clsDel mouseover" id="butDelete" tabindex="25"/>
                  <?php } ?>
                  &nbsp;</td>
                <td bgcolor="#FFFFFF" class="normalfntMid no"  id="<?php echo $row['TASK_ID']; ?>"><?php echo $row['TASK_ID']; ?>&nbsp;</td>
                <td bgcolor="#FFFFFF" class="normalfnt"><textarea <?php echo $disable  ?> name="txtRemarks" id="txtRemarks" cols="30" rows="3" class="concern<?php /*?> validate[required]<?php */?>"><?php
				  echo	str_replace("<br>", "\n",$row['CONCERN']);  
				  ?></textarea></td>
                <td bgcolor="#FFFFFF" class="normalfnt"  valign="middle"><textarea <?php echo $disable  ?> name="txtRecommendation" id="txtRecommendation" cols="30" rows="3" class="recommendation<?php /*?> validate[required]<?php */?>"><?php echo $row['RECOMMENDATION']; ?></textarea></td>
                <td bgcolor="#FFFFFF" class="normalfnt raisedBy"  valign="middle"><select <?php echo $disable  ?> name="select" multiple="multiple" class="txtConcernRaisedBy<?php echo $i; ?> cons msCombo chosen-select" style="width:188px; height:50px" data-placeholder="">
                  <?php
					$sqlu = $objmemget->userList();
			
		$html = "<option value=\"\"></option>";
		$resultu = $db->RunQuery($sqlu);
		while($rowu=mysqli_fetch_array($resultu))
		{
				$selected=0;
				foreach($concernByArr as $x)
				{
					if($x==$rowu['intUserId']){
					  $selected=1;	
					}
				}
					if($selected==1){
						$html .= "<option value=\"".$rowu['intUserId']."\" selected=\"selected\">".$rowu['strFullName']."</option>";
					}
					else{
						$html .= "<option value=\"".$rowu['intUserId']."\">".$rowu['strFullName']."</option>";
					}
 		}
		echo $html;
						?>
                </select></td>
                <td bgcolor="#FFFFFF" class="normalfnt"  valign="middle"><textarea <?php echo $disable  ?> name="txtActionPlan" id="txtActionPlan" cols="20" rows="3" class="actionPlan<?php /*?> validate[required]<?php */?>"><?php echo $row['ACTION_PLAN']; ?></textarea></td>
                <td bgcolor="#FFFFFF" class="normalfnt action"  valign="middle"><select <?php echo $disable  ?> name="select" multiple="multiple" class="txtActionBy<?php echo $i; ?> resp msCombo chosen-select" style="width:188px; height:50px" data-placeholder="">
                  <?php
		$sqlu = $objmemget->userList();
			
		$html = "<option value=\"\"></option>";
		$resultu = $db->RunQuery($sqlu);
		while($rowu=mysqli_fetch_array($resultu))
		{
				$selected=0;
 				foreach($actionByArr as $x)
				{
					if($x==$rowu['intUserId']){
					  $selected=1;	
					}
				}
					if($selected==1){
						$html .= "<option value=\"".$rowu['intUserId']."\" selected=\"selected\">".$rowu['strFullName']."</option>";
					}
					else{
						$html .= "<option value=\"".$rowu['intUserId']."\">".$rowu['strFullName']."</option>";
					}
		}
		echo $html;
						?>
                </select></td>
                <td bgcolor="#FFFFFF" class="normalfntMid" valign="middle" nowrap="nowrap"><input <?php echo $disable  ?> name="dtDate<?php echo $row['TASK_ID']; ?>" type="text" value="<?php echo $row['DUE_DATE'] ?>" class="txtbox date validate[required]" id="date<?php echo $row['TASK_ID']; ?>" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="<?php /*?>return ControlableKeyAccess(event);<?php */?>"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td bgcolor="#FFFFFF" class="normalfntMid iniDueDate" align="centet" nowrap="nowrap"><input <?php echo $disable  ?> name="dtDateExt<?php echo $row['TASK_ID']; ?>" type="text" value="<?php echo $row['INITIAL_DUE_DATE'] ?>" class="txtbox dateExt" id="dateExt<?php echo $row['TASK_ID']; ?>" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="<?php /*?>return ControlableKeyAccess(event);<?php */?>"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td bgcolor="#FFFFFF" class="normalfntMid complete" align="centet" style="display: none">&nbsp;<?php if($row['COMPLETED_FLAG']==1){   echo $comp;    } else {  ?>
                  <?php if($completePermission==1){  ?>
                  <a id="butComplete" class="button green medium" style="" name="butComplete">Complete</a>
                 <?php } ?>
                 <?php } ?></td>
              </tr>
              <?php
					}
                    $savedRows=$i;
				  ?>
              <?php
				  if($i==0){
					  for($i=1; $i<6; $i++)
					  {
 					 ?>
              <tr id="<?php echo $row['METER_ID'] ?>" class="normalfnt">
                <td bgcolor="#FFFFFF" class="normalfntMid delC"><?php if($deleteMode==1){  ?>
                  <img <?php /*?> style="display:none" <?php */?>border="0" src="images/del.png" alt="Delete" name="butDelete" class="clsDel mouseover" id="butDelete" tabindex="25"/>
                  <?php }  ?></td>
                <td bgcolor="#FFFFFF" class="normalfntMid no"  id="<?php echo $i; ?>"><?php echo $i; ?>&nbsp;</td>
                <td bgcolor="#FFFFFF" class="normalfnt"><textarea name="txtRemarks" id="txtRemarks" cols="30" rows="3" class="concern<?php /*?> validate[required]<?php */?>"><?php echo $row['CONCERN']; ?></textarea></td>
                <td bgcolor="#FFFFFF"  class="normalfnt" valign="middle"><textarea name="txtRecommendation" id="txtRecommendation" cols="30" rows="3" class="recommendation<?php /*?> validate[required]<?php */?>"><?php echo $row['RECOMMENDATION']; ?></textarea></td>
                <td bgcolor="#FFFFFF" class="normalfnt raisedBy"  valign="middle"><select name="select" multiple="multiple" class="txtConcernRaisedBy<?php echo $i; ?> cons msCombo chosen-select" style="width:188px; height:50px" data-placeholder="">
                  <?php
		$sqlu = $objmemget->userList();
			
		$html = "<option value=\"\"></option>";
		$resultu = $db->RunQuery($sqlu);
		while($rowu=mysqli_fetch_array($resultu))
		{
  						$html .= "<option value=\"".$rowu['intUserId']."\">".$rowu['strFullName']."</option>";
 		}
		echo $html;
						?>
                </select></td>
                <td bgcolor="#FFFFFF"  class="normalfnt" valign="middle"><textarea name="txtActionPlan" id="txtActionPlan" cols="20" rows="3" class="actionPlan<?php /*?> validate[required]<?php */?>"><?php echo $row['ACTION_PLAN']; ?></textarea></td>
                <td bgcolor="#FFFFFF"  class="normalfnt action" valign="middle"><select name="select" multiple="multiple" class="txtActionBy<?php echo $i; ?> resp msCombo chosen-select" style="width:188px; height:50px" data-placeholder="" >
                  <?php
		$sqlu = $objmemget->userList();
			
		$html = "<option value=\"\"></option>";
		$resultu = $db->RunQuery($sqlu);
		while($rowu=mysqli_fetch_array($resultu))
		{
				$html .= "<option value=\"".$rowu['intUserId']."\">".$rowu['strFullName']."</option>";
		}
		echo $html;
						?>
                </select></td>
                <td bgcolor="#FFFFFF" class="normalfntMid" valign="middle" nowrap="nowrap"><input name="dtDate<?php echo $i; ?>" type="text" value="" class="txtbox date validate[required]" id="dtDate<?php echo $i; ?>" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="<?php /*?>return ControlableKeyAccess(event);<?php */?>"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td bgcolor="#FFFFFF" class="normalfntMid iniDueDate" align="centet"><input name="dtDateExt<?php echo $i; ?>" type="text" value="" class="txtbox dateExt" id="dtDateExt<?php echo $i; ?>" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="<?php /*?>return ControlableKeyAccess(event);<?php */?>"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td bgcolor="#FFFFFF" class="normalfntMid complete" align="centet" style="display: none">&nbsp;</td>
              </tr>
              <?php
					  }
                    $savedRows=$i;
				  }
				  
				  ?>
              <tr>
                <td colspan="10" align="left"><a id="butAddNewTask" class="button white medium" style="" name="butAddNewTask"> Add </a></td>
              </tr>
            </tbody>
          </table></td>
        </tr>
        <tr>
          <td height="34" colspan="3"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            <tr>
              <td width="100%" align="center" bgcolor=""><a id="butNew"  class="button white medium" style="" name="butAddNewTask"> New </a>
                <?php if($editMode==1){ ?>
                <a id="butSave" class="button white medium" style="" name="butSave"> Save </a>
                <?php } ?>
                <a id="butReport" class="button white medium" <?php if($serialNo==''){ ?>style="display:none"<?php } ?> name="butReport"  > Report </a>
                <a id="butClose" class="button white medium" name="butClose" href="main.php">Close</a></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td colspan="3" class="normalfnt"><?php
		$sql = $objmemget->userList();
			
 					$result = $db->RunQuery($sql);
					$id="'";
					while($row=mysqli_fetch_array($result))
					{		
							$id=$row['intUserId'];
							$name=$row['strFullName'];
						
							$names .=  $row['strFullName'].",";
							$ids .=  $row['intUserId'].",";
 							 $str .=" { 
							 id: '$id',
								userName: '$name'
							},";
					}
				$names = substr($names, 0, -1);	
				$ids = substr($ids, 0, -1);	
				$str = substr($str, 0, -1);	
 				?>
              
   
	<script type="text/javascript">
             </script>
  
             <?php
		$sqlu = $objmemget->userList();
			
		$html = "<option value=\"\"></option>";
		$resultu = $db->RunQuery($sqlu);
		while($rowu=mysqli_fetch_array($resultu))
		{
				$html .= "<option value=\"".$rowu['intUserId']."\">".$rowu['strFullName']."</option>";
		}
		  $html;
?>
            <div style="display:none" id="divSuggestNames"><?php echo $names; ?></div>
            <div style="display:none" id="divSuggestIds"><?php echo $ids; ?></div>
            <div style="display:none" id="divSuggestStr"><?php echo $html; ?></div></td>
        </tr>
      </table>
    </div>
  </div>
</form>
<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"> 
  <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>--> 
</div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
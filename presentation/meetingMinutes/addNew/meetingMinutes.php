<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$intUser  		= $sessions->getUserId();

require_once 	 	"class/tables/menupermision.php";			$menupermision			= new menupermision($db);

if($main_menuId!=''){
	$menus->set($main_menuId);
	if(!$menupermision->checkPermission($main_menuId,$sessions->getUserId())){
		include 'dataAccess/permissionDenied.php';exit;
	}

//check root menu permision
if(!$menus->getintStatus()){
	include 'dataAccess/invalidFileName.php';exit;
}

//check user menu permision
if(!$menupermision->getintView()){
	include 'dataAccess/permissionDenied.php';exit;	
}
}

require_once ("class/meetingMinutes/cls_meetingMinutes_db.php");
$objmemget			= new cls_meetingMinutes_db($db);

$programCode		= 'P0718';

$serialDisplay		= $_REQUEST['serialNo'];
$serialNo 			= substr($_REQUEST['serialNo'],3,4);
$curr_date			= date('Y-m-d');
$to_complete		= $_REQUEST['to_complete'];
//$to_complete		=0;

$row=$objmemget->formHeader($serialNo);
$subject			= 	$row['SUBJECT'];
$place 				= 	$row['MEETING_PLACE'];
$attendees 			= 	$row['ATTENDEES_LIST'];
$calledBy 			= 	$row['CALLED_BY_LIST'];
$distribution		= 	$row['DISTRIBUTION_LIST'];
$otherDistribution	= 	$row['OTHER_DISTRIBUTION_EMAILS'];
$date 				= 	$row['DATE'];
$createdDate		= substr($row['CREATED_DATE'],0,10);
$createdBy			= 	$row['CREATED_BY'];
$createdByName		= 	$row['createdByName'];
$calledByArr 		= explode(",", $calledBy);
$attendeesArr 		= explode(",", $attendees);
$distributionArr	= explode(",", $distribution);

$editMode1			=$objmemget->loadEditMode($programCode,$intUser,$serialNo,$createdBy);
$deleteMode1		=$objmemget->loadDeleteMode($programCode,$intUser);
$flag_any_completion=$objmemget->loadCompletionFlag($serialNo);

$editMode			=0;
$deleteMode			=0;

if(($serialNo!='' && $createdBy==$intUser) || ($serialNo=='')){
	$editMode		=$editMode1;
	$deleteMode		=$deleteMode1;
}
if($flag_any_completion==1){
	$editMode		=0;
	$deleteMode		=0;
}
if($to_complete==1){
	$editMode		=0;
	$deleteMode		=0;
}
	

if(($createdBy==$intUser || $createdBy=='') &&($to_complete!=1)){
	include_once ("meetingMinutes_created.php");
}
else{
	include_once ("meetingMinutes_others.php");
}

?>

<?php 
ini_set('display_errors',0);
session_start();

include '../../../dataAccess/DBManager3.php'; 									$db 									=  new DBManager2(1);
include_once "../../../class/error_handler.php";								$error_handler 							= new error_handler();	
require_once "../../../class/sessions.php";										$sessions								= new sessions();
//require_once "../../../class/meetingMinutes/cls_meetingMinutes_db.php";
require_once "../../../class/tables/other_meeting_minutes_backup.php";			$other_meeting_minutes_backup			= new other_meeting_minutes_backup($db);
require_once "../../../class/tables/other_meeting_minutes_edit_permission.php"; $other_meeting_minutes_edit_permission	= new other_meeting_minutes_edit_permission($db);
require_once "../../../class/tables/other_meeting_minutes_header.php";			$other_meeting_minutes_header			= new other_meeting_minutes_header($db);
require_once "../../../class/tables/menupermision.php";							$menupermision							= new menupermision($db);
require_once "../../../class/dateTime.php";										$dateTimes								= new dateTimes($db);
require_once "../../../class/tables/other_meeting_minutes_header_history.php";	$other_meeting_minutes_header_history	= new other_meeting_minutes_header_history($db);
require_once "../../../class/tables/other_meeting_minutes_details.php";			$other_meeting_minutes_details			= new other_meeting_minutes_details($db);
require_once ("../../../class/meetingMinutes/cls_meetingMinutes_db.php");				$objmemget								= new cls_meetingMinutes_db($db);

$userId				=	$sessions->getUserId();
//$objmemget			=	new cls_meetingMinutes_db($db);

//$response			=	array('type'=>'', 'msg'=>'');

$requestType 		=	$_REQUEST['requestType'];
$arrHeader 			=	$_REQUEST['arrHeader'];
$arrDetails			=	$_REQUEST['arrDetails'];

$programName		=	'Meeting Minutes';
$programCode		=	'P0718';

if($requestType	=='autoSave')
{
	try
	{
		$db->connect();
		
		$arrHeader 				= json_decode($arrHeader,true);
		$arrDetails 			= json_decode($arrDetails,true);
		
		$subject 				= $arrHeader['subject'];
		$serialNo 				= substr($arrHeader['minuteNo'],3);
		$meetingPlace 			= $arrHeader['meetingPlace'];
		$arrCalledBy 			= ($arrHeader['arrCalledBy']==''?'null':$arrHeader['arrCalledBy']);
		$arrAttendance 			= ($arrHeader['arrAttendance']==''?'null':$arrHeader['arrAttendance']);
		$arrDistribution 		= ($arrHeader['arrDistribution']==''?'null':$arrHeader['arrDistribution']);
		$arrOtherDistribution 	= $arrHeader['arrOtherDistribution'];
		$arrUserId			 	= $arrHeader['userId'];
		$date 					= ($arrHeader['date']==''?'null':$arrHeader['date']);
		
		if($serialNo == '')
		{
			$editMode		= false;
			$createdDate	= date('Y-m-d h:m:s');
			$creator		= $arrUserId;
			
			$serialNo		= getNextSerialNo();
			saveHeader($serialNo,$subject,$meetingPlace,$arrCalledBy,$arrAttendance,$arrDistribution,$arrOtherDistribution,$date,$arrUserId);
		}
		else
		{
			$editMode		= true;
			updateHeader($serialNo,$subject,$meetingPlace,$arrCalledBy,$arrAttendance,$arrDistribution,$arrOtherDistribution,$date,$arrUserId);
		}
		
		foreach($arrDetails as $arrVal)
		{
			$concern			= $arrVal['concern'];
			$recommendation		= $arrVal['recommendation'];
			$actionPlan			= $arrVal['actionPlan'];
			$actionBy			= $arrVal['actionBy'];
			$concernBy			= $arrVal['concernBy'];	
			$dueDate			= ($arrVal['dueDate']==''?'null':$arrVal['dueDate']);
			$no 				 = $arrVal['no'];
			
			deleteDetail($serialNo,$no);
			saveDetail($serialNo,$no,$concern,$recommendation,$concernBy,$actionPlan,$actionBy,$dueDate,$dueDate);		
		}	
		
		$db->commit();
		$response['type'] 			= 'pass';
		if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
		else
			$response['msg'] 		= 'Saved successfully.';
		
		$response['serialNo'] 		= 'MOM'.str_pad($serialNo,4,0,STR_PAD_LEFT);
	}
	catch(Exception $e)
	{
		$db->rollback();
		$response['msg'] 	=  $e->getMessage();;
		$response['error'] 	=  $error_handler->jTraceEx($e);
		$response['type'] 	=  'fail';
		$response['sql']	=  $db->getSql();
	}
	echo json_encode($response);
	$db->disconnect();
}
if($requestType	==	'save')
{
	try
	{
		// open connection
		$db->connect();
		// save mom backup
		saveOtherMeetingBackup($arrHeader,$arrDetails);
		
		$arrHeader 				= json_decode($arrHeader,true);
		$arrDetails 			= json_decode($arrDetails,true);
		
		$subject 				= $arrHeader['subject'];
		$serialNo 				= substr($arrHeader['minuteId'],3);
		$meetingPlace 			= $arrHeader['meetingPlace'];
		$arrCalledBy 			= $arrHeader['arrCalledBy'];
		$arrAttendance 			= $arrHeader['arrAttendance'];
		$arrDistribution 		= $arrHeader['arrDistribution'];
		$arrOtherDistribution 	= $arrHeader['arrOtherDistribution'];
		$date 					= $arrHeader['date'];
		$userId_bkp 			= $arrHeader['userId_bkp'];
		
		if($date==''){$date = 'null';}
		if($arrCalledBy=='null'){$arrCalledBy = '';}
		if($arrAttendance=='null'){$arrAttendance = '';}
		if($arrDistribution=='null'){$arrDistribution = '';}
		
		if($userId=='')
			$userId=$userId_bkp;
		
		if($userId=='')
			throw new Exception('Your session has expired. Please log in to system using seperate tab');
		
		
		if($serialNo == '')
		{
			$editMode		= false;
			$mode			= 'INSERT';
			$createdDate	= date('Y-m-d h:m:s');
			$creator		= $userId;
			//get new serial no
			$serialNo		= getNextSerialNo();
			//save header
			saveHeader($serialNo,$subject,$meetingPlace,$arrCalledBy,$arrAttendance,$arrDistribution,$arrOtherDistribution,$date,$userId);
			//save history data
			saveHistoryHeader($serialNo,$subject,$meetingPlace,$arrCalledBy,$arrAttendance,$arrDistribution,$arrOtherDistribution,$date,$createdDate,$creator,$userId,$mode);
		}
		else
		{
			$editMode		= true;
			$mode			= 'UPDATE';
			
			$other_meeting_minutes_header->set($serialNo); 

			$creator		= $other_meeting_minutes_header->getCREATED_BY();
			$createdDate	= $other_meeting_minutes_header->getCREATED_DATE();
			// check user permission to edit
			checkUserPermission($programCode,$userId,$creator);
			//update header
			updateHeader($serialNo,$subject,$meetingPlace,$arrCalledBy,$arrAttendance,$arrDistribution,$arrOtherDistribution,$date,$userId);
		}
		
		foreach($arrDetails as $arrVal)
		{
			$concern			= $arrVal['concern'];
			$recommendation		= $arrVal['recommendation'];
			$actionPlan			= $arrVal['actionPlan'];
			$actionBy			= $arrVal['actionBy'];
			$concernBy			= $arrVal['concernBy'];	
			$dueDate			= $arrVal['dueDate'];
			$no 				 = $arrVal['no'];
			
			if($dueDate==''){$dueDate = 'null';}
			
			if($editMode)
			{
				$detailCount = getDetailSavedCount($serialNo,$no);
				if($detailCount>0)
				{
					//set mom detail data
					$other_meeting_minutes_details->set($serialNo,$no,$userId);
					$dueDate_ini	= $other_meeting_minutes_details->getINITIAL_DUE_DATE();
					//$saved_no		= $other_meeting_minutes_details->getTASK_ID();
					if($dueDate_ini == '' || $dueDate_ini == NULL || $dueDate_ini == 'NULL' ){
						$dueDate_ini = $dueDate;
					}
					//validate before save with permission
					validateBeforeSave($serialNo,$no);
					//update detail data
					/*if($saved_no==''){
					saveDetail($serialNo,$no,$concern,$recommendation,$concernBy,$actionPlan,$actionBy,$dueDate,$dueDate);
					}
				    else{ */	updateDetail($serialNo,$no,$concern,$recommendation,$concernBy,$actionPlan,$actionBy,$dueDate,$dueDate_ini);
					//}
				}
				else 
				{
					saveDetail($serialNo,$no,$concern,$recommendation,$concernBy,$actionPlan,$actionBy,$dueDate,$dueDate);
				}
				
			}
			else{
			//save detail data
				saveDetail($serialNo,$no,$concern,$recommendation,$concernBy,$actionPlan,$actionBy,$dueDate,$dueDate);
			}
				
		}	
		
		$db->commit();
	
		$response['type'] 			= 'pass';
		if($editMode	==	1)
		$response['msg'] 			= 'Updated successfully.';
		else
		$response['msg'] 			= 'Saved successfully.';
		
		$response['serialNo'] 		= 'MOM'.str_pad($serialNo,4,0,STR_PAD_LEFT);
		//$response['year'] 			= $year;
	}
	catch(Exception $e)
	{
		$db->rollback();
		$response['msg'] 	=  $e->getMessage();;
		$response['error'] 	=  $error_handler->jTraceEx($e);
		$response['type'] 	=  'fail';
		$response['sql']	=  $db->getSql();
	}
	echo json_encode($response);
	$db->disconnect();
}
else if($requestType	==	'complete_level1')
{
	$minute		= $_REQUEST['minute'];
	$minute		= substr($minute,3,4);
	$no			= $_REQUEST['no'];
	
	try
	{
		//open connection
		$db->connect();
		//validate before complete
		validateBeforeComplete_level1($minute,$no,$userId);
		//update completed data
		updateCompleted_level1($minute,$no,$userId);
		
		$other_meeting_minutes_details->set($minute,$no,$userId);
		//get copleted by name
		$compdBy			= $other_meeting_minutes_details->getCOMPLETED_BY_NAME_LEVEL_1();
		//get completed date
		$compDate			= $other_meeting_minutes_details->getCOMPETED_DATE_LEVEL_1();
		
		$db->commit();
		
		$response['type'] 	= 'pass';
		$response['msg'] 	= 'Completed successfully.';
		$response['html'] 	= $compdBy.' ('.$compDate.')';
		
		
	}
	catch(Exception $e)
	{
		$db->rollback();
		$response['msg'] 	=  $e->getMessage();;
		$response['error'] 	=  $error_handler->jTraceEx($e);
		$response['type'] 	=  'fail';
		$response['sql']	=  $db->getSql();
	}
	
	echo json_encode($response);
	$db->disconnect();	
}

else if($requestType	==	'complete')
{
	$minute		= $_REQUEST['minute'];
	$minute		= substr($minute,3,4);
	$no			= $_REQUEST['no'];
	
	try
	{
		//open connection
		$db->connect();
		//validate before complete
		validateBeforeComplete($minute,$no,$userId);
		//update completed data
		updateCompleted($minute,$no,$userId);
		
		$other_meeting_minutes_details->set($minute,$no,$userId);
		//get copleted by name
		$compdBy			= $other_meeting_minutes_details->getCOMPLETED_BY_NAME();
		//get completed date
		$compDate			= $other_meeting_minutes_details->getCOMPETED_DATE();
		
		$db->commit();
		
		$response['type'] 	= 'pass';
		$response['msg'] 	= 'Completed successfully.';
		$response['html'] 	= $compdBy.' ('.$compDate.')';
		
		
	}
	catch(Exception $e)
	{
		$db->rollback();
		$response['msg'] 	=  $e->getMessage();;
		$response['error'] 	=  $error_handler->jTraceEx($e);
		$response['type'] 	=  'fail';
		$response['sql']	=  $db->getSql();
	}
	
	echo json_encode($response);
	$db->disconnect();	
}
else if($requestType	==	'delete')
{
	$minute		= $_REQUEST['minute'];
	$minute		= substr($minute,3,4);
	$no			= $_REQUEST['no'];
	
	try
	{
		//open connection
		$db->connect();
		//validate before delete
		validateBeforeDelete($minute,$no,$userId,$programCode);
		// delete detail data
		deleteDetail($minute,$no);
		
		$db->commit();
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Deleted successfully.';
	}
	catch(Exception $e)
	{
		$db->rollback();
		$response['msg'] 	=  $e->getMessage();;
		$response['error'] 	=  $error_handler->jTraceEx($e);
		$response['type'] 	=  'fail';
		$response['sql']	=  $db->getSql();
	}
	
	echo json_encode($response);
	$db->disconnect();
}
else if($requestType	==	'loadRow'){
	
		$db->connect();
		//ob_start();
 		$html_tab	=	'<td bgcolor="#FFFFFF" class="normalfntMid delC">';
		if($deleteMode==1){ 
		$html_tab	.=	'<img  border="0" src="images/del.png" alt="Delete" name="butDelete" class="clsDel mouseover" id="butDelete" tabindex="25"/></td>';
		}
		$html_tab	.=	'<td bgcolor="#FFFFFF" class="normalfntMid no"  id="'.$i.'">'.$i.'&nbsp;</td>';
				

		$html_tab	.=	'<td bgcolor="#FFFFFF" class="normalfnt"><textarea name="txtRemarks" id="txtRemarks" cols="30" rows="3" class="concern" >'.$row['CONCERN'].'</textarea></td>';
		$html_tab	.=	'<td bgcolor="#FFFFFF"  class="normalfnt" valign="middle"><textarea name="txtRecommendation" id="txtRecommendation" cols="30" rows="3" class="recommendation">'.$row['RECOMMENDATION'].'</textarea></td>';
		$html_tab	.=	'<td bgcolor="#FFFFFF" class="normalfnt raisedBy"  valign="middle"><select name="select" multiple="multiple" class="txtConcernRaisedBy'.$i.' cons msCombo chosen-select" style="width:188px; height:50px" data-placeholder="">';

		$sqlu = $objmemget->userList();
		$html_tab .= "<option value=\"\"></option>";
		$resultu = $db->RunQuery($sqlu);
		while($rowu=mysqli_fetch_array($resultu))
		{
  						$html_tab .= "<option value=\"".$rowu['intUserId']."\">".$rowu['strFullName']."</option>";
 		}
		$html_tab	.=	'</select></td>';
		$html_tab	.=	'<td bgcolor="#FFFFFF"  class="normalfnt" valign="middle"><textarea name="txtActionPlan" id="txtActionPlan" cols="20" rows="3" class="actionPlan" >'.$row['ACTION_PLAN'].'</textarea></td>';
		$html_tab	.=	'<td bgcolor="#FFFFFF"  class="normalfnt action" valign="middle"><select name="select" multiple="multiple" class="txtActionBy'.$i.' resp msCombo chosen-select" style="width:188px; height:50px" data-placeholder="" >';
 		
		$sqlu = $objmemget->userList();
			
		$html_tab .= "<option value=\"\"></option>";
		$resultu = $db->RunQuery($sqlu);
		while($rowu=mysqli_fetch_array($resultu))
		{
				$html_tab .= "<option value=\"".$rowu['intUserId']."\">".$rowu['strFullName']."</option>";
		}
		
		

		$html_tab	.=	'</select></td>';
		$html_tab	.=	'<td bgcolor="#FFFFFF" class="normalfntMid" valign="middle" nowrap="nowrap"><input name="dtDate'.$i.'" type="text" value="" class="txtbox date" id="dtDate'.$i.'" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '."'%Y-%m-%d'".');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '."'%Y-%m-%'".');" /></td>';
		
		$html_tab	.=	'<td bgcolor="#FFFFFF" class="normalfntMid" valign="middle" nowrap="nowrap"><input name="dtDateExt'.$i.'" type="text" value="" class="txtbox date" id="dtDateExt'.$i.'" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '."'%Y-%m-%d'".');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '."'%Y-%m-%'".');" /></td>';
		$html_tab	.=	'<td bgcolor="#FFFFFF" class="normalfntMid complete" style="display: none" align="centet">&nbsp;</td>';

	//echo $body 			= ob_get_clean();*/
    $response['html']	=  $html_tab;
 	
	echo json_encode($response);
	$db->disconnect(); 
 }


function saveOtherMeetingBackup($arrHeader,$arrDetails)
{
	global $db;
	global $other_meeting_minutes_backup;
	
	$data  		= array(
						'ARRAY_HEADER'	=> $arrHeader,
						'ARRAY_DETAILS'	=> $arrDetails	
						);
	//insert data to other_meeting_minutes_backup
	$result_arr	= $other_meeting_minutes_backup->insert($data);
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	else
		return true;
}
function checkUserPermission($programCode,$userId,$creator)
{
	global $db;
	global $menupermision;
	global $other_meeting_minutes_edit_permission;
	$editMode 	= false;
	//set menupermision
	$menupermision->set($programCode,$userId);
	//check edit permission
	if($menupermision->getintEdit()==1 && $creator=='')
		$editMode	= true;
	//set other_meeting_minutes_edit_permission
	
	$cols		= " USER_ID ";
	
	$where		= " USER_ID = '".$userId."' ";
	
	$result 	= $other_meeting_minutes_edit_permission->select($cols,$join = null,$where, $order = null, $limit = null);	
	$count		= mysqli_num_rows($result);
	
	if($count>0 || ($userId==$creator))
		$editMode	= true;
	
	if(!$editMode)
		throw new Exception('No Permission to Edit');
	else
		return true;
}
function getNextSerialNo()
{
	global $db;
	global $other_meeting_minutes_header;
	
	$cols		= "(IFNULL(Max(other_meeting_minutes_header.MINUTE_ID),0)+1) AS serial ";
	
	$result 	= $other_meeting_minutes_header->select($cols,$join = null, $where = null, $order = null, $limit = null);
	$row		= mysqli_fetch_array($result);
	$serial		= $row['serial'];
 			
	return $serial;
}
function saveHeader($serialNo,$subject,$meetingPlace,$arrCalledBy,$arrAttendance,$arrDistribution,$arrOtherDistribution,$date,$userId)
{
	global $db;
	global $other_meeting_minutes_header;
	global $dateTimes;
	
	$data  	= array(
					'MINUTE_ID'					=> $serialNo,
					'SUBJECT'					=> $subject,
					'MEETING_PLACE'				=> $meetingPlace,
					'CALLED_BY_LIST'			=> $arrCalledBy,
					'ATTENDEES_LIST'			=> $arrAttendance,
					'DISTRIBUTION_LIST'			=> $arrDistribution,
					'OTHER_DISTRIBUTION_EMAILS'	=> $arrOtherDistribution,
					'DATE'						=> $date,
					'CREATED_BY'				=> $userId,
					'CREATED_DATE'				=> $dateTimes->getCurruntDateTime(),				
					'STATUS'					=> 1	
					);
	
	$result_arr		= $other_meeting_minutes_header->insert($data);
	
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	else
		return true;
}
function saveHistoryHeader($serialNo,$subject,$meetingPlace,$arrCalledBy,$arrAttendance,$arrDistribution,$arrOtherDistribution,$date,$createdDate,$creator,$userId,$mode)
{
	global $db;
	global $other_meeting_minutes_header_history;
	global $dateTimes;
	
	$data  	= array(
					'MINUTE_ID'					=> $serialNo,
					'SUBJECT'					=> $subject,
					'MEETING_PLACE'				=> $meetingPlace,
					'CALLED_BY_LIST'			=> $arrCalledBy,
					'ATTENDEES_LIST'			=> $arrAttendance,
					'DISTRIBUTION_LIST'			=> $arrDistribution,
					'OTHER_DISTRIBUTION_EMAILS'	=> $arrOtherDistribution,
					'DATE'						=> $date,
					'CREATED_BY'				=> $creator,
					'CREATED_DATE'				=> $createdDate,				
					'STATUS'					=> 1,
					'ADD_MODIFIED_BY'			=> $userId,
					'ADD_MODIFIED_DATE'			=> $dateTimes->getCurruntDateTime(),
					'MODE'						=> $mode	
					);
	
	$result_arr		= $other_meeting_minutes_header_history->insert($data);
	
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	else
		return true;					
}
function updateHeader($serialNo,$subject,$meetingPlace,$arrCalledBy,$arrAttendance,$arrDistribution,$arrOtherDistribution,$date,$userId)
{
	global $db;
	global $other_meeting_minutes_header;
	global $dateTimes;
	
	$data  	= array(
					'SUBJECT'					=> $subject,
					'MEETING_PLACE'				=> $meetingPlace,
					'CALLED_BY_LIST'			=> $arrCalledBy,
					'ATTENDEES_LIST'			=> $arrAttendance,
					'DISTRIBUTION_LIST'			=> $arrDistribution,
					'OTHER_DISTRIBUTION_EMAILS'	=> $arrOtherDistribution,
					'DATE'						=> $date,
					'STATUS'					=> 1							
					);
	
	$where		= 'MINUTE_ID = "'.$serialNo.'"' ;
		
	$result_arr	= $other_meeting_minutes_header->update($data,$where);
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	else
		return true;
}
function validateBeforeSave($serialNo,$no)
{
	global $db;
	global $userId;
	global $other_meeting_minutes_details;
	//set other_meeting_minutes_details
	$other_meeting_minutes_details->set($serialNo,$no,$userId);
	
	if($other_meeting_minutes_details->getCOMPLETED_FLAG()==1)
		throw new Exception("Can't update completed tasks");
	else
		return true;
}
function updateDetail($serialNo,$no,$concern,$recommendation,$concernBy,$actionPlan,$actionBy,$dueDate,$dueDateIni)
{
	global $db;
	global $other_meeting_minutes_details;
	
	$data  	= array(
					'CONCERN'			=> $concern,
					'RECOMMENDATION'	=> $recommendation,
					'CONCERN_RAISED_BY'	=> $concernBy,
					'ACTION_PLAN'		=> $actionPlan,
					'RESPONSIBLE_LIST'	=> $actionBy,
					'INITIAL_DUE_DATE'	=> $dueDateIni,			
					'DUE_DATE'			=> $dueDate				
					);	
	
	$where		= 'MINUTE_ID = "'.$serialNo.'" AND TASK_ID = "'.$no.'" ' ;
		
	$result_arr	= $other_meeting_minutes_details->update($data,$where);
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	else
		return true;
}
function saveDetail($serialNo,$no,$concern,$recommendation,$concernBy,$actionPlan,$actionBy,$dueDate,$dueDateIni)
{
	global $db;
	global $other_meeting_minutes_details;
	
	$data  	= array(
					'MINUTE_ID'			=> $serialNo,
					'TASK_ID'			=> $no,
					'CONCERN'			=> $concern,
					'RECOMMENDATION'	=> $recommendation,
					'CONCERN_RAISED_BY'	=> $concernBy,
					'ACTION_PLAN'		=> $actionPlan,
					'RESPONSIBLE_LIST'	=> $actionBy,
					'INITIAL_DUE_DATE'	=> $dueDateIni,
					'DUE_DATE'			=> $dueDate
					);
	
	$result_arr		= $other_meeting_minutes_details->insert($data);
	
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	else
		return true;
}
function validateBeforeComplete_level1($minute,$no,$userId)
{
	global $db;
	global $other_meeting_minutes_details;
	
	$other_meeting_minutes_details->set($minute,$no,$userId);
	
	if($other_meeting_minutes_details->getpermision()==0)
		throw new Exception("No permision to complete.");
	else if($other_meeting_minutes_details->getCREATED_BY_LEVEL_1()=='')
		throw new Exception("Please save before Complete");
	else if($other_meeting_minutes_details->getcompletedFlag_level_1()==1)
		throw new Exception("Already Completed");
	else
		return true;
}
function updateCompleted_level1($minute,$no,$userId)
{
	global $db;
	global $other_meeting_minutes_details;
	global $dateTimes;
	
	$data  	= array(
					'COMPLETED_FLAG_LEVEL_1'	=> 1,
					'COMPLETED_BY_LEVEL_1'		=> $userId,
					'COMPETED_DATE_LEVEL_1'		=> $dateTimes->getCurruntDateTime()
					);
	
	$where		= 'MINUTE_ID = "'.$minute.'" AND TASK_ID = "'.$no.'" ' ;
		
	$result_arr	= $other_meeting_minutes_details->update($data,$where);
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	else
		return true;
}

function validateBeforeComplete($minute,$no,$userId)
{
	global $db;
	global $other_meeting_minutes_details;
	
	$other_meeting_minutes_details->set($minute,$no,$userId);
	
	if($other_meeting_minutes_details->getpermision_final_completion()!=1)
		throw new Exception("No permision to raise final completion");
	else if($other_meeting_minutes_details->getCREATED_BY()=='')
		throw new Exception("Please save before Complete");
	else if($other_meeting_minutes_details->getcompletedFlag_level_1()!=1)
		throw new Exception("First completion must be raised");
	else if($other_meeting_minutes_details->getcompletedFlag()==1)
		throw new Exception("Already Completed");
	else
		return true;
}
function updateCompleted($minute,$no,$userId)
{
	global $db;
	global $other_meeting_minutes_details;
	global $dateTimes;
	
	$data  	= array(
					'COMPLETED_FLAG'	=> 1,
					'COMPLETED_BY'		=> $userId,
					'COMPETED_DATE'		=> $dateTimes->getCurruntDateTime()
					);
	
	$where		= 'MINUTE_ID = "'.$minute.'" AND TASK_ID = "'.$no.'" ' ;
		
	$result_arr	= $other_meeting_minutes_details->update($data,$where);
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	else
		return true;
}

function validateBeforeDelete($minute,$no,$userId,$programCode)
{
	global $db;
	global $other_meeting_minutes_details;
	global $menupermision;
	
	$menupermision->set($programCode,$userId);
	$other_meeting_minutes_details->set($minute,$no,$userId);
	
	if($menupermision->getintDelete()!=1)
		throw new Exception("No Permition to delete");
	else if($other_meeting_minutes_details->getcompletedFlag()==1)
		throw new Exception("Completed.Can't Delete");
	else
		return true;	
}
function deleteDetail($minute,$no)
{
	global $db;
	global $other_meeting_minutes_details;
	
	$where		= 'MINUTE_ID = "'.$minute.'" AND TASK_ID = "'.$no.'" ' ;
	
	$result_arr = $other_meeting_minutes_details->delete($where);
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	else
		return true;
}
function getDetailSavedCount($serialNo,$no)
{
	global $db;
	global $other_meeting_minutes_details;
	
	$cols		= " * ";
	
	$where		= 'MINUTE_ID = "'.$serialNo.'" AND TASK_ID = "'.$no.'" ' ;
	
	$result 	= $other_meeting_minutes_details->select($cols,$join = null,$where, $order = null, $limit = null);	
	//echo $db->getsql();
	$count		= mysqli_num_rows($result);
	
	return $count;
	
	
}
?>
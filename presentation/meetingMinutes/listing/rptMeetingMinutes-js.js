// JavaScript Document
var createdBy;
$(document).ready(function() {
  	
	$('#frmMOMReport #butMailMe').click(function(){
	
 	var val = $.prompt('Are you sure you want to Mail this to u ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					var mom = document.getElementById('serialNo').innerHTML;
					var url = "presentation/meetingMinutes/listing/rptMeetingMinutesEmailSet.php?serialNo="+mom+"&reportType=mailMe";
 					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmMOMReport #butMailMe').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
 									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmMOMReport #butMailMe').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
 					}
 
 				}});
	});
//--------------
	$('#frmMOMReport #butMailAll').click(function(){
	var val = $.prompt('Are you sure you want to Mail this to All ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					var mom = document.getElementById('serialNo').innerHTML;
					var url = "presentation/meetingMinutes/listing/rptMeetingMinutesEmailSet.php?serialNo="+mom+"&reportType=mailAll";
 					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmMOMReport #butMailAll').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
 									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmMOMReport #butMailAll').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
 					}
  				}});
	});

  });
//---------------------------------------------------
 function alertx()
{
	$('#frmMOMReport #butMailMe').validationEngine('hide')	;
	$('#frmMOMReport #butMailAll').validationEngine('hide')	;
}
//--------------------------------------------
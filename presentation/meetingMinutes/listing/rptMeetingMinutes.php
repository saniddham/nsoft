<?php //(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
	$thisFilePath =  $_SERVER['PHP_SELF'];
	//include_once '../../../dataAccess/LoginDBManager.php';
	//include_once "../../../libraries/mail/mail_bcc.php";	
	
	require_once $_SESSION['ROOT_PATH']."class/meetingMinutes/cls_meetingMinutes_db.php";
		
	//$db =  new DBManager2(0);
	$objmemget= new cls_meetingMinutes_db($db);
	 
	$companyId 			= $_SESSION['CompanyID'];
	$locationId			= $_SESSION['CompanyID'];
	$intUser 			= $_SESSION["userId"];
	$mainPath			= $_SESSION['mainPath'];
	$sessEmail			= $_SESSION["email"];
	$thisFilePath		= $_SERVER['PHP_SELF'];
	
	if($intUser=='' && $auto_cron_5 !=1){
		echo "Please open a seperate tab to log in to the system.Then refresh this page.";
		die();
	}
	
	if($MOM==''){
		$MOM = $_REQUEST['serialNo'];
	}
	if($reportType==''){
		$reportType= $_REQUEST['reportType'];
	}
	
	if($serialNo==''){
		$serialNo = substr($MOM,3,4);
	}
	 
	if($auto_cron_5==1)
	$sqlM=$objmemget->getReportDetail_daily_mail_Sql($serialNo,$reportType);
	else
	$sqlM=$objmemget->getReportDetailSql($serialNo,$reportType,$intUser);
	
	$resultM = $db->RunQuery($sqlM);
	$lateMOMs=0; 
	
	while($rowM=mysqli_fetch_array($resultM))
	{
		$MOM=$rowM['Meeting_No'];
		$lateMOMs++;
		
		$subject			= $rowM['SUBJECT'];
		$place 				= $rowM['MEETING_PLACE'];
		$attendees 			= $rowM['Attendees'];
		$calledBy 			= $rowM['Called_by'];
		$distribution		= $rowM['Distribution'];
		$otherDistribution	= $rowM['otherDistribution'];
		$meetingDate 		= $rowM['meetingDate'];
		$createDate 		= $rowM['createDate'];
		$createBy 			= $rowM['createBy'];
		$status 			= $rowM['status'];
		
?>


<?php
if($hideButton!=1){?> <title>Meeting Minutes Report</title><?php }?>
 <style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}


 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}


.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}



.odd{background-color:#F5F5F5;}
.even{background-color:#FFFFFF;}
.mouseover {
	cursor: pointer;
}


.txtNumber{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}
</style>
  
 
<title>MEETING MINUTE REPORT </title> 
<script type="application/javascript" src="presentation/meetingMinutes/listing/rptMeetingMinutes-js.js"></script>

<form id="frmMOMReport" name="frmMOMReport" method="post" action="">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
  
    <table width="1200" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
      <td colspan="2">
          <table width="100%">
              <tr>
                <td width="5%">&nbsp;</td>
                <td width="11%" class="normalfnt">Meeting minute</td>
                <td width="4%" align="center" valign="middle"><strong>:</strong></td>
                <td width="20%" class="normalfnt" id="serialNo"><?php echo $MOM; ?></td>
                <td width="7%">&nbsp;</td>
                <td width="4%" align="center" valign="middle">&nbsp;</td>
                <td width="19%">&nbsp;</td>
                <td width="11%" class="normalfnt">Create Date</td>
                <td width="1%" align="center" valign="middle"><strong>:</strong></td>
                <td width="18%"><span class="normalfnt"><?php echo $createDate; ?></span></td>
              </tr>
              <tr>
                <td >&nbsp;</td>
                <td class="normalfnt">Subject</td>
                <td align="center" valign="middle"><strong>:</strong></td>
                <td colspan="4" ><span class="normalfnt"><?php echo $subject; ?></span>                  <div id="loginName" style="display:none"><?php //echo $_SESSION["email"]; ?></div></td>
                <td class="normalfnt" >Meeting Date</td>
                <td align="center" ><strong>:</strong></td>
                <td ><span class="normalfnt"><?php echo $meetingDate; ?></span></td>
                </tr>
              <tr>
                <td >&nbsp;</td>
                <td class="normalfnt">Meeting Place</td>
                <td align="center" valign="middle"><strong>:</strong></td>
                <td colspan="7" ><span class="normalfnt"><?php echo $place; ?></span></td>
              </tr>
              <tr>
                <td >&nbsp;</td>
                <td class="normalfnt">Called by</td>
                <td align="center" valign="middle"><strong>:</strong></td>
                <td colspan="7" ><span class="normalfnt"><?php echo $calledBy; ?></span></td>
              </tr>
              <tr>
                <td >&nbsp;</td>
                <td  class="normalfnt">Attendees</td>
                <td align="center" valign="middle"><strong>:</strong></td>
                <td colspan="7" ><span class="normalfnt"><?php echo $attendees; ?></span></td>
              </tr>
              <tr>
                <td >&nbsp;</td>
                <td  class="normalfnt">Distribution</td>
                <td align="center" valign="middle"><strong>:</strong></td>
                <td colspan="4" ><span class="normalfnt"><?php echo $distribution; ?></span></td>
                <td ><span class="normalfnt"></span></td>
                <td ><span class="normalfnt"></span></td>
                <td ><span id="spanCreateBy" class="normalfnt"></span></td>
              </tr>
              <tr>
                <td >&nbsp;</td>
                <td  class="normalfnt">Other Distribution</td>
                <td align="center" valign="middle"><strong>:</strong></td>
                <td colspan="4" ><span class="normalfnt"><?php echo $otherDistribution; ?></span></td>
                <td ><span class="normalfnt">Create By</span></td>
                <td ><span class="normalfnt">: </span></td>
                <td ><span id="spanCreateBy" class="normalfnt"><?php echo $createBy; ?></span></td>
              </tr>
           </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
           <table width="100%"  class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <thead>
              <tr class=""  bgcolor="#dce9f9">
                <th width="3%" >No</th>
                <th width="18%" >Concern</th>
                <th width="19%" >Recommendation</th>
                <th width="8%" >Concern Raised By</th>
                <th width="15%" >Action Plan</th>
                <th width="10%" >Responsible</th>
                <th width="6%" >Due Date</th>
                <th width="7%" >Initial Due Date</th>
                <th width="7%" >1st level Completed</th>
                <th width="7%" >1st level Completed By</th>
                <th width="7%" >1st level Completed Date</th>
                <th width="7%" >2nd level Completed</th>
                <th width="7%" >2nd level Completed By</th>
                <th width="7%" >2nd level Completed Date</th>
                </tr>
              </thead>
            <tbody>
              <?php  
 			$sql		=	$objmemget->other_meeting_minutes_report_details_select($serialNo);
			$result1	=	$db->RunQuery($sql);
  			while($row1=mysqli_fetch_array($result1))
			{
  				$no						=$row1['TASK_ID'];
				$concern				=$row1['CONCERN'];
				$recomondation			=$row1['RECOMMENDATION'];
				$actionPlan				=$row1['ACTION_PLAN'];
				$respFromSenior			=$row1['RESPONSE_FROM_SENIOR_MANAGEMENT'];
				$dueDate				=$row1['DUE_DATE'];
				$dueDateInit			=$row1['INITIAL_DUE_DATE'];
				$dueDays				=$row1['DUE_DAYS'];
				$respoList				=$row1['RESPONSIBLE_LIST'];
				$consernRaised			=$row1['CONCERN_RAISED_BY'];
				$completed				=$row1['completed'];
				$completed_level_1		=$row1['completed_level_1'];
				$COMPLETED_FLAG			=$row1['COMPLETED_FLAG'];
				$completed_by			=$row1['COMPLETED_BY'];
				$completed_date			=$row1['COMPETED_DATE'];
				$COMPLETED_FLAG			=$row1['COMPLETED_FLAG'];
				$completed_by_level_1	=$row1['COMPLETED_BY_LEVEL_1'];
				$completed_date_level_1	=$row1['COMPETED_DATE_LEVEL_1'];
				$AFTER_COMPLETED_DAYS	=$row1['AFTER_COMPLETED_DAYS'];
				
				if(($AFTER_COMPLETED_DAYS>1) && ($COMPLETED_FLAG==1) && $boo_run_from_cronjob)
					continue;	
				
				
				if(($dueDays>1) && ($COMPLETED_FLAG!=1)){
					$fntcolor="#FF3333";//red
 				}
				else if(($dueDays==1) && ($COMPLETED_FLAG!=1)){
					$fntcolor="#06F"; //blue
 				}
				else if($COMPLETED_FLAG==1){
					$fntcolor="#008040"; //green
 				}
				else{
					$fntcolor="#000000";
				}
 	  ?>
              <tr class="normalfnt"   bgcolor="#FFFFFF">
                <td align="left" class="normalfnt" style="color:<?Php echo $fntcolor; ?>"   id="<?php echo $orderYear; ?>" ><?php echo $no; ?>&nbsp;</td>
                <td align="left" class="normalfnt" style="color:<?Php echo $fntcolor; ?>"   id="<?php echo $orderYear; ?>" ><?php echo $concern; ?>&nbsp;</td>
                <td align="left" class="normalfnt" style="color:<?Php echo $fntcolor; ?>"   id="<?php echo $orderYear; ?>" ><?php echo $recomondation; ?>&nbsp;</td>
                <td align="left" class="normalfnt" style="color:<?Php echo $fntcolor; ?>"   id="<?php echo $salesOrderNo; ?>" ><?php echo $consernRaised; ?>&nbsp;</td>
                <td align="left" class="normalfnt" style="color:<?Php echo $fntcolor; ?>"   id="<?php echo $orderNo; ?>" ><?php echo $actionPlan; ?>&nbsp;</td>
                <td align="left" class="normalfnt" style="color:<?Php echo $fntcolor; ?>"   id="<?php echo $salesOrderNo; ?>" ><?php echo $respoList; ?>&nbsp;</td>
                <td align="center" class="normalfntMid" style="color:<?Php echo $fntcolor; ?>"   id="<?php echo $salesOrderNo; ?>" ><?php echo $dueDate; ?>&nbsp;</td>
                <td align="center" class="normalfntMid" style="color:<?Php echo $fntcolor; ?>"   id="<?php echo $salesOrderNo; ?>" ><?php echo $dueDateInit; ?></td>
                <td align="center" class="normalfntMid" style="color:<?Php echo $fntcolor; ?>"   id="<?php echo $salesOrderNo; ?>" ><span class="normalfntMid" style="color:<?Php echo $fntcolor; ?>"><?php echo $completed_level_1; ?></span></td>
                <td align="center" class="normalfntMid" style="color:<?Php echo $fntcolor; ?>"   id="<?php echo $salesOrderNo; ?>" ><span class="normalfnt" style="color:<?Php echo $fntcolor; ?>"><?php echo $completed_by_level_1; ?></span></td>
                <td align="center" class="normalfntMid" style="color:<?Php echo $fntcolor; ?>"   id="<?php echo $salesOrderNo; ?>" ><span class="normalfntMid" style="color:<?Php echo $fntcolor; ?>"><?php echo $completed_date_level_1; ?></span></td>
                <td align="center" class="normalfntMid" style="color:<?Php echo $fntcolor; ?>"   id="<?php echo $salesOrderNo; ?>" ><?php echo $completed; ?>&nbsp;</td>
                <td align="left" class="normalfnt" style="color:<?Php echo $fntcolor; ?>"   id="<?php echo $salesOrderNo; ?>" ><?php echo $completed_by; ?>&nbsp;</td>
                <td align="center" class="normalfntMid" style="color:<?Php echo $fntcolor; ?>"   id="<?php echo $salesOrderNo; ?>" ><?php echo $completed_date; ?>&nbsp;</td>
                </tr>              
              <?php 
 			}
	  ?>
               </tbody>
          </table>     
     </td>
</tr>
<?php
if($hideButton!=1){
 ?>
<tr height="40">
  <td width="317" colspan="2" align="center" class="normalfnt" >
  <table width="100%"  cellspacing="0" cellpadding="0">
  <tr>
  <td width="36%" align="center"></td>
  <td align="center"><?php if(($reportType=='') && $status ==1) { ?>
  <a id="butMailMe"  class="button white medium" style="" name="butMailMe"> Mail To Me </a>
  <a id="butMailAll"  class="button white medium" style="" name="butMailAll"> Mail To All </a>
  <?php } ?> </td>
  <td width="33%" align="center"></td>
   </tr>
  </table>
 </td>
 </tr>
<?php
}
?>
 </table>
 </form>
 
 <?php
}
?>
<!-- <script type="text/javascript">
 var createdBy = '<?php //echo $sessEmail;  ?>';
 </script>
-->
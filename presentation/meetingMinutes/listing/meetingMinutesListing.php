<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";
	
	$company	= $_SESSION['headCompanyId'];
	$intUser	= $_SESSION["userId"];

 	$programName	=	'Meeting Minutes Listing';
	$programCode	=	'P0718';

	$app_permission	=  check_user_approval_permission($intUser,$programCode);

    $sql1 			= "SELECT intId,strName FROM 
						mst_companies
						ORDER BY
						mst_companies.strName ASC
						";
	$result1 = $db->RunQuery($sql1);
	$str1='';
	$j=0;
	while($row	=	mysqli_fetch_array($result1))
	{
		if($company	==	$row['intId']){
		$inilocId		=	$row['intId'];
		$inilocation	=	$row['strName'];
		}
		else{
		$locId		=	$row['intId'];
		$location	=	$row['strName'];
		$str1		.= $location.":".$location.";" ;
		}
		$j++;
	}
	
 	$str2	=	$inilocation.":".$inilocation.";" ;
	$str	=	$str2.$str1;
	$str 	.= ":All" ;
 
	$sql = "select * from(SELECT
			concat('MOM',LPAD(other_meeting_minutes_header.`MINUTE_ID`,4,0)) AS Meeting_No,
			other_meeting_minutes_header.MINUTE_ID AS serial_No,
			other_meeting_minutes_header.`SUBJECT` AS `Subject`,
			other_meeting_minutes_header.MEETING_PLACE AS Meeting_Place,
			(SELECT
			GROUP_CONCAT(sys_users.strFullName) as fn 
			FROM `sys_users`
			WHERE
			FIND_IN_SET(sys_users.intUserId, other_meeting_minutes_header.ATTENDEES_LIST)) AS Attendees,
			(SELECT
			GROUP_CONCAT(sys_users.strFullName) as fn 
			FROM `sys_users`
			WHERE
			FIND_IN_SET(sys_users.intUserId, other_meeting_minutes_header.CALLED_BY_LIST)) AS Called_by,
			(SELECT
			GROUP_CONCAT(sys_users.strFullName) as fn 
			FROM `sys_users`
			WHERE
			FIND_IN_SET(sys_users.intUserId, other_meeting_minutes_header.DISTRIBUTION_LIST)) AS Distribution,
			date(other_meeting_minutes_header.DATE) AS Date,
			'View' AS `View`,
			'Click to complete' as comp,
			date(other_meeting_minutes_header.CREATED_DATE) AS createDate,
			sys_users.strFullName AS createBy
			FROM
			other_meeting_minutes_header
			INNER JOIN sys_users ON sys_users.intUserId = other_meeting_minutes_header.CREATED_BY
			where other_meeting_minutes_header.`MINUTE_ID` IN
			(
			SELECT distinct 
			other_meeting_minutes_header.MINUTE_ID
			FROM
			other_meeting_minutes_header
			INNER JOIN other_meeting_minutes_details ON other_meeting_minutes_details.MINUTE_ID = other_meeting_minutes_header.MINUTE_ID
			where 
			find_in_set($intUser,other_meeting_minutes_header.CALLED_BY_LIST) OR
			find_in_set($intUser,other_meeting_minutes_header.ATTENDEES_LIST) OR
			find_in_set($intUser,other_meeting_minutes_header.CREATED_BY) OR
			find_in_set($intUser,other_meeting_minutes_header.DISTRIBUTION_LIST) OR
			find_in_set($intUser,other_meeting_minutes_details.RESPONSIBLE_LIST) OR
 			find_in_set($intUser,other_meeting_minutes_details.COMPLETED_BY) OR
			find_in_set($intUser,other_meeting_minutes_details.CONCERN_RAISED_BY) OR 
			($app_permission =1)
 		)
	)  as t where 1=1
	";
	
$col = array();
  
$col["title"] 			= "Meeting No"; // caption of column
$col["name"] 			= "Meeting_No"; // grid column name 
$col["width"] 			= "2";
$col["align"] 			= "center";
$col['link']			= "?q=718&serialNo={Meeting_No}";	 
$col["linkoptions"] 	= "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$col["title"] 			= "Meeting Date";  
$col["name"] 			= "Date";   
$col["width"] 			= "2";
$col["align"] 			= "center";
$cols[] = $col;	$col=NULL;

$col["title"] 			= "Subject";  
$col["name"] 			= "Subject";   
$col["width"] 			= "5";
$col["align"] 			= "center";
$cols[] = $col;	$col=NULL;

$col["title"] 			= "Meeting Place";  
$col["name"] 			= "Meeting_Place";  
$col["width"] 			= "5";
$col["align"] 			= "center";
$cols[] = $col;	$col=NULL;

$col["title"] 			= "Created By";  
$col["name"] 			= "createBy";  
$col["width"] 			= "5";
$col["align"] 			= "center";
$cols[] = $col;	$col=NULL;

$col["title"] 			= "Created Date";  
$col["name"] 			= "createDate";    
$col["width"] 			= "2";
$col["align"] 			= "center";
$cols[] = $col;	$col=NULL;

$col["title"] 			= "Called by";  
$col["name"] 			= "Called_by"; 
$col["width"] 			= "7";
$col["align"] 			= "center";
$cols[] = $col;	$col=NULL;

$col["title"] 			= "Distribution";  
$col["name"] 			= "Distribution";  
$col["width"] 			= "6";
$col["align"] 			= "center";
$cols[] = $col;	$col=NULL;


$col["title"] 			= "Complete"; // caption of column
$col["name"] 			= "comp"; // grid column name 
$col["width"] 			= "4";
$col["align"] 			= "center";
$col['link']			= "?q=718&serialNo={Meeting_No}&to_complete=1";	 
$col["linkoptions"] 	= "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
 
//VIEW
$reportLink  			= "?q=995&serialNo={Meeting_No}";
$col["title"] 			= "Report";  
$col["name"] 			= "View";   
$col["width"]			= "3";
$col["search"]			= false;
$col["align"] 			= "center";
$col['link']			= $reportLink;
$col["linkoptions"] 	= "target='_blank'";  
$cols[] = $col;	$col=NULL;
 
 
$jq = new jqgrid('',$db);

$grid["caption"] 		= "Meeting Minutes Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Meeting_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
  
// export XLS file
// export to excel parameters - range could be "all" or "filtered"
$grid["export"] = array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);
 $jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);
 
$out = $jq->render("list1");
?>
 <title>Meeting Minutes Listing</title>
<?php
echo $out;
//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(trn_orderheader.intApproveLevelStart) AS appLevel
			FROM trn_orderheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}

function check_user_approval_permission($user,$code){
	
	global $db;
	$permision	=0;
	
	$sqlp = "SELECT
			menupermision.int1Approval,
			menupermision.int2Approval
			FROM
			menupermision
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$code' AND
			menupermision.intUserId = '$user'
			";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	if($rowp['int2Approval']==1)
		$permision	=1;
	
	return $permision;
		
}
?>


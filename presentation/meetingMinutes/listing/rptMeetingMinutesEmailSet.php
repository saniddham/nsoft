<?php
 ini_set('display_errors', 0); 
	session_start();
	date_default_timezone_set('Asia/Colombo');
	ini_set('max_execution_time', 11111111) ;
	
	include_once '../../../dataAccess/DBManager2.php';
	include_once "../../../libraries/mail/mail_bcc.php";	
	require_once $_SESSION['ROOT_PATH']."class/meetingMinutes/cls_meetingMinutes_db.php";
 		
	$db =  new DBManager2(1);
 	$objmemget= new cls_meetingMinutes_db($db);

	$db->connect();//open connection.
	
  	if($requestType==''){
	$requestType 		= $_REQUEST['reportType'];
	}
  	$locationId 	= $_SESSION['CompanyID'];
	$intUser  		= $_SESSION["userId"];
	$mainPath 		= $_SESSION['mainPath'];
	$thisFilePath 	=  $_SERVER['PHP_SELF'];
  	$MOM 			= $_REQUEST['serialNo'];
	$serialNo 		= substr($_REQUEST['serialNo'],3,4);
 	$toEmails;
   
 	$programName='Meeting Minutes';
	$programCode='P0718';
 
//------------save---------------------------	
	if($requestType=='mailMe')
	{
		ob_start();
		
		$hideButton=1;
		include "rptMeetingMinutes.php";
		
		$body = ob_get_clean();
		
		$nowDate = date('Y-m-d');
		$mailHeader= "NEW - MEETING MINUTES REPORT ($MOM - $subject)";
		$toEml=$_SESSION["email"];
		sendMessage($_SESSION["email"],'',$toEml,$mailHeader,$body,$ccEmails,'');
 	
		if($rollBackFlag!=1){
			$response['type'] 		= 'pass';
 			$response['msg'] 		= 'Sent sucessfully.';
 		}
		else{
 			$response['type'] 		= 'fail';
 			$response['msg'] 		= 'Sending failed.';
		}
 	echo json_encode($response);

 	}
	
	else if($requestType=='mailAll')
	{
 		ob_start();

		$hideButton=1;
		include "rptMeetingMinutes.php";
	
		$body = ob_get_clean();
		
		$nowDate = date('Y-m-d');
		$mailHeader= "NEW - MEETING MINUTES REPORT ($MOM - $subject)";
		
		$ccEmails = $objmemget->ccEmails($serialNo);
		$toEmails = $objmemget->toEmails($serialNo);
		sendMessage($_SESSION["email"],'',$toEmails,$mailHeader,$body,$ccEmails,'');
		
		
		if($rollBackFlag!=1){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Sent sucessfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= 'Sending failed.';
		}
		
	echo json_encode($response);

 	}
 ?>
 
 <?php
$db->commit();		
$db->disconnect();
?>
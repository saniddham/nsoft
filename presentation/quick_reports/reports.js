$(document).ready(function(){
	//------------------------
	$('#tblProcumentHead').click(function(){
		
		headShowHide('tblProcumentHead','tblProcumentBody');
		
	});
	
	$('#tblProcumentBody thead').click(function(){
		
		bodyShowHide('tblProcumentHead','tblProcumentBody');
		
	});
	//----------------------
	
	$('#tblWarehouseHead').click(function(){

		headShowHide('tblWarehouseHead','tblWarehouseBody');
		
	});
	
	$('#tblWarehouseBody thead').click(function(){
		
		bodyShowHide('tblWarehouseHead','tblWarehouseBody');
		
	});
	

////////////////////////////////////////////////////////////////
 
	
 //-------------------------------------------- 
  $('.view').click(function(){
	var	id=$(this).attr('id');
	var docYear=$(this).parent().parent().find(".documentYear").val();
	var docNo = $(this).parent().parent().find(".documentNo").val();
	//alert(id);
	
	if(docYear==''){
		alert("Please select Document Year");
		return false;
	}
	else if(docNo==''){
		alert("Please select Document No");
		return false;
	}
	var url='';
	  
		if(id=='prn'){
		url+= "../procurement/purchaseRequisitionNote/listing/rptPurchaseRequisitionNote.php?year="+docYear+"&prnNo="+docNo;
		}
		else if(id=='po'){
		url+= "../procurement/purchaseOrder/listing/rptPurchaseOrder.php?year="+docYear+"&poNo="+docNo;
		}
		else if(id=='grn'){
		url+= "../warehouse/grn/listing/rptGrn.php?year="+docYear+"&grnNo="+docNo;
		}
		else if(id=='srn'){
		url+= "../warehouse/returnToSupplier/listing/rptReturnToSupplier.php?year="+docYear+"&retSupNo="+docNo;
		}
		else if(id=='mrn'){
		url+= "../warehouse/mrn/listing/rptMrn.php?year="+docYear+"&mrnNo="+docNo;
		}
		else if(id=='min'){
		url+= "../warehouse/issue/listing/rptIssue.php?year="+docYear+"&issueNo="+docNo;
		}
		else if(id='retSt'){
		url+= "../warehouse/returnToStores/listing/rptReturnToStores.php?year="+docYear+"&returnNo="+docNo;
		}
		else if(id=='gp'){
		url+= "../warehouse/gatepass/listing/rptGatepass.php?year="+docYear+"&gatePassNo="+docNo;
		}
		else if(id=='gpTIN'){
		url+= "../warehouse/gatepassTransferIn/listing/rptGatepassTransferIn.php?year="+docYear+"&transfNo="+docNo;
		}
		else if(id=='interComp'){
		url+= "../warehouse/invoice/listing/rptInvoice.php?year="+docYear+"&invoiceNo="+docNo;
		}
		else if(id=='newItmAlloc'){
		url+= "../warehouse/newItemAllowcation/listing/rptNewItemAllocation.php?year="+docYear+"&allocationNo="+docNo;
		}
		
		
		window.open(url);
  });
  

});



////////////////////////////////////
function headShowHide(headId,bodyId){
	
		$('#tblProcumentBody').hide('slow');
		$('#tblWarehouseBody').hide('slow');		
		
		$('#tblProcumentHead').show('slow');
		$('#tblWarehouseHead').show('slow');
		
		$('#'+headId).hide('slow');
		$('#'+bodyId).show('slow');
		
}
//------------------------------------
function bodyShowHide(headId,bodyId){
		$('#'+headId).show('slow');
		$('#'+bodyId).hide('slow');
}
////////////////////////////////////////
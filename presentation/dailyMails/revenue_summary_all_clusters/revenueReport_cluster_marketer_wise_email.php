<?php
session_start();

date_default_timezone_set('Asia/Colombo');
ini_set('max_execution_time', 11111111) ;
//ini_set('display_errors',1);
		
		$company 				= '';
		$cronjobId 				= 57;

		$cron_emails_to			=$obj_comm->getCronjobEmailIds($cronjobId,'TO');
		$cron_emails_cc			=$obj_comm->getCronjobEmailIds($cronjobId,'CC');
		$cron_emails_bcc		=$obj_comm->getCronjobEmailIds($cronjobId,'BCC');
		
		$body					= '';
		ob_start();
		include 'revenueReport_cluster_marketer_wise.php';
 		echo $body 				= ob_get_clean();

		$nowDate 				= date('Y-m-d');
		$mailHeader				= "REVENUE REPORT - ALL CLUSTERS ($nowDate)";
		$FROM_NAME				= "";
		$FROM_EMAIL				= '';
		
		$mail_TO				= $obj_comm->getEmailList($cron_emails_to);
		$mail_CC				= $obj_comm->getEmailList($cron_emails_cc);
		$mail_BCC				= $obj_comm->getEmailList($cron_emails_bcc);

		insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
?>
 S
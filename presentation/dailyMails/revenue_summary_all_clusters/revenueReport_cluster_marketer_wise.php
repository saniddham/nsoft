<?php
ini_set('max_execution_time', 11111111) ;
session_start();
//ini_set('display_errors',1);
  
include_once "class/dailyMails/revenue_summary_all_clusters/cls_marketer_revenue_get.php"; 
 
//$db 									= new LoginDBManager();
$obj_revenue_get						= new cls_marketer_revenue_get($db);
 
$printDateArr 							= array();
$dateArr								= array();
$fullDateArr							= array();
$dateNameArray							= array();
$dateNameArray							= array();
$cumulativeDay							= array();
$cumulativeDisp							= array();
$DayWiseDispSumArr 						= array();
$DayWiseClustDispSumArr1_without_Others	= array();
$cumulativeClustDisp_without_Others		= array();//////////////////////////////////////////////
 
$companyId 								= 	$_SESSION['CompanyID'];
$locationId 							= 	$_SESSION['CompanyID'];
$intUser  								= 	$_SESSION["userId"];
$mainPath 								= 	$_SESSION['mainPath'];
$thisFilePath 							=  	$_SERVER['PHP_SELF'];

$toCurrency  							=	1;		//1=USD,2=LKR


$year			= date('Y', strtotime('-1 day', strtotime(date('Y-m-d'))));
$month			= date('m', strtotime('-1 day', strtotime(date('Y-m-d'))));
$day			= date('d', strtotime('-1 day', strtotime(date('Y-m-d'))));


$startDate								=	1;
$monthName								=	$obj_comm->getMonthName($month);

$ts 									= 	strtotime($monthName." ".$year);
$endDate								= 	date('t', $ts); 
$startDate 								= 	date("Y-m-d", mktime(0, 0, 0, $month, $startDate, $year));
$endDate 								= 	date("Y-m-d", mktime(0, 0, 0, $month, $endDate, $year));
$rptDate 								= 	date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))); 



$i					=0;
$temp				='';
while ($temp < $endDate) {
 $temp 				= date("Y-m-d", mktime(0, 0, 0, $month, 1+$i, $year));
 $weekday 			= date('l', strtotime($temp)); // note: first arg to date() is lower-case L
 $printDateArr[$i]	=(1+$i)."-".substr($monthName,0,3);
 $dateArr[$i]		=(1+$i);
 $fullDateArr[$i]	=$temp;
 $dateNameArray[$i]	=$weekday; 
 $i++;
}
$noOfDays 			= $i;
 $currencyName1		=$obj_comm->getCurrencyName($toCurrency);
 ?>
<title>Revenue Report</title>
<style>
.normalfntBlue {
	font-family: Verdana;
	font-size: 10px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfnt {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: right;
}
.compulsoryRed {
	color: #F00;
}
</style>
<div align="center">
  <div style="background-color:#FFF" ><strong>
    <div align="left"><img style="vertical-align:" src="http://accsee.com/nsoft_logo.png"  /></div>
    REVENUE REPORT - <?php echo "  (  ".  $rptDate.' )  Process Time('.date('Y-m-d H:i:s').')' ?></strong><strong></strong></div>
  <div style="background-color:'#FF'" ><strong>(Currency - <?php echo $currencyName1?>) ( <?php echo "ALL CLUSTERS"; ?> )</strong><strong></strong></div>
  <table width="1500" border="0" align="center" bgcolor="">
    <tr>
      <td><table width="1500">
          <tr>
            <td width="10">&nbsp;</td>
            <td colspan="7" class="normalfnt"><table width="1500" border="1" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
                <!----Plant wise header------------------------------------------------------------>
                <tr class="" bgcolor="">
                  <th width="50" colspan="4" class="normalfnt" ><b>Plant Wise Revenue</b></th>
                  <?php
					 for($j=0; $j<$noOfDays ; $j++){ 
					  $bgCol=$reportColor;
					  if($dateNameArray[$j]==$fix_holiday_of_week)
						  $bgCol='#FF99CC';
 					  ?>
				  <th width="80" class="normalfntMid" bgcolor="<?php echo $bgCol ; ?>" ><?php echo substr($dateNameArray[$j],0,3) ?></th>
                  <?php
					 }
				  ?>
                  <th width="80" class="normalfntMid">Total</th>
                </tr>
                <tr class="" bgcolor="<?php echo $reportColor; ?>">
                  <th bgcolor="#006699"><font color="<?php echo $reportColor; ?>">Plant</font></th>
                  <th bgcolor="#006699"><font color="<?php echo $reportColor; ?>">Annual Target</font></th>
                  <th bgcolor="#006699" ><font color="<?php echo $reportColor; ?>">Monthly Target</font></th>
                  <th bgcolor="#006699"><font color="<?php echo $reportColor; ?>">Day Target</font></th>
                  <?php
			 	 for($j=0; $j<$noOfDays ; $j++){
				  $bgCol=$reportColor;
				  if($dateNameArray[$j]==$fix_holiday_of_week)
				  $bgCol='#FF99CC';
				  $cumulativeDay[$j]=1;
				  ?>
                  <th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php echo $bgCol ; ?>" ><?php echo $printDateArr[$j]; ?></th>
                  <?php
			  	}
				  ?>
                  <th width="80">&nbsp;</th>
                </tr>
                <!----End of Plant wise header------------------------------------------------------------>
                <!----Plant wise revnue------------------------------------------------------------------->
                <?php 
				 $result1			=$obj_revenue_get->get_all_cluster_plants($year,$month);  
				 $anualTargtTot 	=0;
				 $monthlyTargtTot 	=0;
				 $dayTargtTot 		=0;
				 $totPlantDispQty	=0;
				 
				while($row	=mysqli_fetch_array($result1))
				{
					$plantId=$row['intPlantId'];
					$plantWiseTotDispQty['$plantId']=0;
	 			 ?>
                <tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
                  <td class="normalfnt" ><?php echo $row['strPlantName'];?></td>
                  <td class="normalfntRight" align="right" ><?php echo number_format($row['TARGET_ANNUALY'],0);?></td>
                  <td class="normalfntRight" align="right" ><?php echo number_format($row['TARGET_MONTHLY'],0);?></td>
                  <td class="normalfntRight" align="right" ><?php echo number_format($row['TARGET_DAILY'],0);?></td>
                  <?php 
				  for($j=0; $j<$noOfDays ; $j++)
				  {
					$date				  = $fullDateArr[$j];
					$dayType			  = '';
					$resultr			  = $obj_revenue_get->get_plant_calender($plantId,$date);  
					$rorw				  = mysqli_fetch_array($resultr);
					$dayType			  = $rorw['intDayType'];
					$dispatchValue        = $obj_revenue_get->get_old_cluster_plant_revenue($plantId,$date,$toCurrency);  
					$DayWiseDispSumArr[$j]+= $dispatchValue;  
				
					if($dateNameArray[$j]==$fix_holiday_of_week)
						$bgCol=" style=\"background-color:#FF99CC\"";
					else
						$bgCol='';
						
 					if($dayType=='0')
						$bgCol=" style=\"background-color:#FFFF33\"";
					if(($dateNameArray[$j]==$fix_holiday_of_week) || ($dayType=='0'))
					{
						$cumulativeDay[$j]=0;
					}
					
					$plantWiseTotDispQty['$plantId'] +=round($dispatchValue);
					$totPlantDispQty                 +=round($dispatchValue);
 				  ?>
					  <td class="normalfntRight"  "<?php echo $bgCol; ?>"><?php echo number_format($dispatchValue,0); ?></td>
				  <?php
				  }
				  ?>
                  <td width="80" class="normalfntRight"><?php echo number_format($plantWiseTotDispQty['$plantId'],0);?></td>
                </tr>
                <?php 
				 $anualTargtTot		 +=$row['TARGET_ANNUALY'];
				 $monthlyTargtTot 	 +=$row['TARGET_MONTHLY'];
				 $dayTargtTot		 +=$row['TARGET_DAILY'];
				}
	           ?>
                <tr class="normalfnt"  bgcolor="#CCCCCC">
                  <td class="normalfnt" >&nbsp;</td>
                  <td class="normalfntRight" ><b><?php echo number_format($anualTargtTot,0); ?></b></td>
                  <td class="normalfntRight" ><b><?php echo number_format($monthlyTargtTot,0); ?></b></td>
                  <td class="normalfntRight" ><b><?php echo number_format($dayTargtTot,0); ?></b></td>
                  <?php
				  for($j=0; $j<$noOfDays ; $j++)
				  {
				  ?>
					  <td class="normalfntRight" ><b><?php echo number_format($DayWiseDispSumArr[$j],0); ?></b></td>
				  <?php
				  }
				  ?>
                    <td class="normalfntRight"  ><?php echo number_format($totPlantDispQty,0); ?></td>
                </tr>
                <!----End of Plant wise revnue----------------------------------------------------------->
                <!----Plant wise Cum.Status-------------------------------------------------------------->
                <tr bgcolor="<?php echo $reportColor; ?>">
                  <td colspan="<?php echo $noOfDays+4; ?>" height="20">&nbsp;</td>
                </tr>
                <tr bgcolor="<?php echo $reportColor; ?>">
                  <td colspan="4" align="right" class="normalfntRight" >Cum.Status</td>
                  <td colspan="<?php echo $noOfDays; ?>" >&nbsp;</td>
                  <td width="80">&nbsp;</td>
                </tr>
                <tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
                  <td class="normalfntRight" colspan="4"  align="right">Cum. Target</td>
                  <?php 
				$k=0;
				for($j=0; $j<$noOfDays ; $j++){
					$date=$fullDateArr[$j];
					if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
						$cumulativeTgt=0;
					}
					else{
						$cumulativeTgt=$dayTargtTot*($k+1);
						$k++;
					}
					$bgCol=$reportColor;
					?>
					<td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($cumulativeTgt,0); ?></td>
					<?php
			  }
			  ?>
                  <td width="80">&nbsp;</td>
                </tr>
                <tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
                  <td class="normalfntRight" colspan="4" align="right" >Cum. Hit</td>
                  <?php 
				for($j=0; $j<$noOfDays ; $j++){
					$date	=$fullDateArr[$j];
 					$bgCol	=$reportColor;
					$sum	=0;
					for($k=0; $k<=$j ; $k++){
						$sum+=$DayWiseDispSumArr[$k];
					}
					$cumulativeDisp[$j]=$sum;
					?>
					<td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($cumulativeDisp[$j],0); ?></td>
					<?php
				}
				?>
                  <td width="80">&nbsp;</td>
                </tr>
                <tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
                  <td class="normalfntRight" colspan="4" align="right" >Cum. Hit%</td>
                  <?php 
					$x=0;
					for($j=0; $j<$noOfDays ; $j++){
						$date=$fullDateArr[$j];
 						if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
							$cumulativeHitPerc=0;
						}
						else{
							$cumulativeHitPerc=$cumulativeDisp[$j]/($dayTargtTot*($x+1))*100;
							$x++;
						}
 						?>
						<td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($cumulativeHitPerc,0); ?>%</td>
						<?php
					}
					?>
                  <td width="80">&nbsp;</td>
                </tr>
                <tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
                  <td class="normalfntRight" colspan="4" align="right" >Dev.</td>
                  <?php 
					$x=0;
					for($j=0; $j<$noOfDays ; $j++){
						$date	=$fullDateArr[$j];
						$bgCol	=$reportColor;
						$cls="normalfntRight";
 						if($cumulativeDay[$j]==0)
						{//sunday or existing holiday atleast in one plant
							$dev1=0;
						}
						else{
							$dev1=$cumulativeDisp[$j]-$dayTargtTot*($x+1);
							$x++;
						}
						$dev= number_format(abs($dev1),0);
						if($dev1<0){
							$dev="(".$dev .")";
							$cls="compulsoryRed";
						}
						?>
						<td class="<?php echo $cls; ?>"  bgcolor="<?php echo $bgCol; ?>"  align="right"><?php echo $dev ?></td>
						<?php
					}
					?>
                  <td width="80">&nbsp;</td>
                </tr>
                <!----End of Plant wise Cum.Status-------------------------------------------------------->

                <!-----------Managers revenue header------------------------------------------------------>
                <tr class="" bgcolor="">
                  <th width="50" colspan="4" class="normalfnt" ><b>Marketer Wise Revenue</b>
                  </th>
                  <?php
					 for($j=0; $j<$noOfDays ; $j++){ 
					  $bgCol=$reportColor;
					  if($dateNameArray[$j]==$fix_holiday_of_week)
						  $bgCol='#FF99CC';
 					  ?>
				  <th width="80" class="normalfntMid" bgcolor="<?php echo $bgCol ; ?>" ><?php echo substr($dateNameArray[$j],0,3) ?></th>
                  <?php
					 }
				  ?>
                  <th width="80" class="normalfntMid">Total</th>
                </tr>
                <tr class="" bgcolor="<?php echo $reportColor; ?>">
                  <th bgcolor="#006699"><font color="<?php echo $reportColor; ?>">Marketer</font></th>
                  <th bgcolor="#006699"><font color="<?php echo $reportColor; ?>">Annual Target</font></th>
                  <th bgcolor="#006699" ><font color="<?php echo $reportColor; ?>">Monthly Target</font></th>
                  <th bgcolor="#006699"><font color="<?php echo $reportColor; ?>">Day Target</font></th>
                  <?php
				  for($j=0; $j<$noOfDays ; $j++){
				  $bgCol=$reportColor;
				  if($dateNameArray[$j]==$fix_holiday_of_week)
				  $bgCol='#FF99CC';
				  ?>
					  <th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php echo $bgCol ; ?>" ><?php echo $printDateArr[$j]; ?></th>
					  <?php
				  }
				  ?>
                  <th width="80">&nbsp;</th>
                </tr>
                <!---------------end of marketer revenue header------------------------>
                <!---------------save marketer revenue in to array--------------------->
                <?php 
				$result1 =$obj_revenue_get->get_all_cluster_marketers_result($date,$month);
				while($row=mysqli_fetch_array($result1))
				{
					$marketerId_list	=$row['intUserId'];
					$plant_list			=$row['plant_list'];
 					  for($j=0; $j<$noOfDays ; $j++)
					  {
 						$date						=$fullDateArr[$j];
						$dispatchValue				=$obj_revenue_get->get_old_cluster_marketer_revenue($marketerId_list,$plant_list,$date,$toCurrency);  
						$DayWiseClustDispSumArr1[$j]+=$dispatchValue; 
						if($row['OTHER_CATEGORY_FLAG']!=1)
						{
							$DayWiseClustDispSumArr1_without_Others[$j]+=$dispatchValue; 
						}
 					  }
				}
 				$result1 								=$obj_revenue_get->get_all_cluster_marketers_result($date,$month);
				$anualClustTargtTot 					=0;
				$anualClustTargtTot_without_others 		=0;
				$monthlyClustTargtTot 					=0;
				$monthlyClustTargtTot_without_others 	=0;
				$dayClustTargtTot 						=0;
				$dayClustTargtTot_without_others 		=0;
				$totClustDispQty						=0;
				$DayWiseClustDispSumArr					='';
				$marketerId_list 						='';
				
				$noOfOthers		 						=0;
 				$marketerId_list_others					='';
				$plant_list_others						='';
				$other_anual							='';
				$other_month							='';
				$other_day								='';

				while($row=mysqli_fetch_array($result1))
				{
					$marketerType		=$row['OTHER_CATEGORY_FLAG'];
					$marketerId			=$row['intUserId'];
					$plant_list			=$row['plant_list'];
					$clustWiseTotDispQty['$marketerId']=0;
					
/*	2013-11-06		$anualClustTargtTot 	+=$row['ANUAL_TARGET'];
					$monthlyClustTargtTot 	+=$row['MONTHLY_TARGET'];
					$dayClustTargtTot 		+=$row['MONTHLY_TARGET']/26;
*/					if($marketerType	!=	1)
                    {
						
						$anualClustTargtTot 					+=$row['TARGET_ANNUALY'];
						$monthlyClustTargtTot 					+=$row['TARGET_MONTHLY'];
						$dayClustTargtTot 						+=$row['TARGET_MONTHLY']/26;
						
						$anualClustTargtTot_without_others 		+=$row['TARGET_ANNUALY'];
						$monthlyClustTargtTot_without_others 	+=$row['TARGET_MONTHLY'];
						$dayClustTargtTot_without_others 		+=$row['TARGET_MONTHLY']/26;
  					
					?>
					<tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
					<td class="normalfnt" ><?php echo $row['marketerName'];?></td>
					<td class="normalfntRight" align="right" ><?php echo number_format($row['TARGET_ANNUALY'],0);?></td>
					<td class="normalfntRight" align="right" ><?php echo number_format($row['TARGET_MONTHLY'],0);?></td>
					<td class="normalfntRight" align="right" ><?php echo number_format($row['TARGET_MONTHLY']/26,0);?></td>
					<?php 
						for($j=0; $j<$noOfDays ; $j++)
						{
							$date=$fullDateArr[$j];
							
							$dispatchValue=$obj_revenue_get->get_old_cluster_marketer_revenue($marketerId,$plant_list,$date,$toCurrency);
							$DayWiseClustDispSumArr[$j]+=$dispatchValue;  
							
							$bgCol=$reportColor;
							if($dateNameArray[$j]==$fix_holiday_of_week)
								$bgCol='#FF99CC';
							$clustWiseTotDispQty['$marketerId'] +=round($dispatchValue);
							$totClustDispQty +=round($dispatchValue);
							?>
							<td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($dispatchValue,0); ?></td>
						<?php
						}
						?>
						<td width="80" class="normalfntRight"><?php echo number_format($clustWiseTotDispQty['$marketerId'],0) ; ?></td>
						</tr>
						<?php 
					}
					else{
						$noOfOthers++;
 						$marketerId_list_others .=$marketerId.","; 
						$plant_list_others		.=$plant_list.","; 
						$other_anual			+=$row['TARGET_ANNUALY'];
						$other_month			+=$row['TARGET_MONTHLY'];
					}
				}//end of marketers
					$marketerId='Others';
					$marketerId_list_others	=substr($marketerId_list_others, 0, -1);
					$plant_list_others	=substr($plant_list_others, 0, -1);
					$other_anual=$other_anual/$noOfOthers;
					$other_month=$other_month/$noOfOthers;
					$other_day=$other_month/26;
				  ?>
                  <?php
				  if($noOfOthers>0){
				  ?>
                  <!----------------Others--------------------------->
                <tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
                <td class="normalfnt" >Others</td>
                <td class="normalfntRight" align="right" >0</td>
                <td class="normalfntRight" align="right" >0</td>
                <td class="normalfntRight" align="right" >0</td>
                <?php 
                    for($j=0; $j<$noOfDays ; $j++){
                        $date=$fullDateArr[$j];
                        
                        $dispatchValue=$obj_revenue_get->get_old_cluster_marketer_revenue($marketerId_list_others,$plant_list_others,$date,$toCurrency);
                        $DayWiseClustDispSumArr[$j]+=$dispatchValue;  
                        
                        $bgCol=$reportColor;
                        if($dateNameArray[$j]==$fix_holiday_of_week)
                            $bgCol='#FF99CC';
                        $clustWiseTotDispQty['$marketerId'] +=round($dispatchValue);
                        $totClustDispQty +=round($dispatchValue);
                        ?>
                        <td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($dispatchValue,0); ?></td>
                    <?php
                    }
                    ?>
                    <td width="80" class="normalfntRight"><?php echo number_format($clustWiseTotDispQty['$marketerId'],0) ; ?></td>
                    </tr>
                  <!------------------------------------------->
                  <?php
				  }
				  ?>
                <tr class="normalfnt"  bgcolor="#CCCCCC">
                  <td class="normalfnt" >&nbsp;</td>
                  <td class="normalfntRight" ><b><?php echo number_format($anualClustTargtTot,0); ?></b></td>
                  <td class="normalfntRight" ><b><?php echo number_format($monthlyClustTargtTot,0); ?></b></td>
                  <td class="normalfntRight" ><b><?php echo number_format($dayClustTargtTot,0); ?></b></td>
                  <?php
					for($j=0; $j<$noOfDays ; $j++){
					?>
					<td class="normalfntRight" ><b><?php echo number_format($DayWiseClustDispSumArr[$j],0); ?></b></td>
					<?php
					}
					?>
                  <td width="80" class="normalfntRight"><?php echo number_format($totClustDispQty,0) ; ?></td>
                </tr>
                <!-----------Cum.status-------->
                <tr bgcolor="<?php echo $reportColor; ?>">
                <td colspan="<?php echo $noOfDays+4; ?>" height="20">&nbsp;</td>
                </tr>
                <tr bgcolor="<?php echo $reportColor; ?>">
                <td colspan="4" align="right" class="normalfntRight" >Cum.Status</td>
                <td colspan="<?php echo $noOfDays; ?>" >&nbsp;</td>
                <td width="80">&nbsp;</td>
                </tr>
                <tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
                <td class="normalfntRight" colspan="4"  align="right">Cum. Target </td>
                  <?php 
				$k=0;
				for($j=0; $j<$noOfDays ; $j++){
					$date=$fullDateArr[$j];
					$bgCol=$reportColor;
					if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
					$cumulativeClustTgt=0;
					$cumulativeClustTgt_without_others=0;
					}
					else{
					$cumulativeClustTgt=$dayClustTargtTot*($k+1);
					$cumulativeClustTgt_without_others=$dayClustTargtTot_without_others*($k+1);
					$k++;
					}
					?>
					<td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($cumulativeClustTgt_without_others,0); ?></td>
				<?php
				}
				?>
                  <td width="80">&nbsp;</td>
                </tr>
                <tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
                  <td class="normalfntRight" colspan="4"  align="right">Cum. Hit </td>
                  <?php 
					$k=0;
					for($j=0; $j<$noOfDays ; $j++){
						$date=$fullDateArr[$j];
						$bgCol=$reportColor;
						$sum=0;
						for($k=0; $k<=$j ; $k++){
							$sum+=$DayWiseClustDispSumArr1_without_Others[$k];
						}
						$cumulativeClustDisp_without_Others[$j]=$sum;
						?>
						<td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($cumulativeClustDisp_without_Others[$j],0); ?></td>
						<?php
					}
					?>
                  <td width="80">&nbsp;</td>
                </tr>
                <tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
                  <td class="normalfntRight" colspan="4"  align="right">Cum. Hit% </td>
                  <?php 
					$x=0;
					for($j=0; $j<$noOfDays ; $j++){
						$date=$fullDateArr[$j];
						$bgCol=$reportColor;
						if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
							$cumulativeHitPercClust=0;
						}
						else{
							$cumulativeHitPercClust=$cumulativeClustDisp_without_Others[$j]/($dayClustTargtTot_without_others*($x+1))*100;
							$x++;
						}
						?>
                        <td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($cumulativeHitPercClust,0); ?>%</td>
					<?php
                    }
                    ?>
                  <td width="80">&nbsp;</td>
                </tr>
                <tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
                  <td class="normalfntRight" colspan="4" align="right" >Dev.</td>
                  <?php 
					$k=0;
					for($j=0; $j<$noOfDays ; $j++){
						$date=$fullDateArr[$j];
						$obj_revenue_get->get_old_cluster_marketer_revenue($marketerId,$plant_list,$date,$toCurrency);
						$bgCol=$reportColor;
						$cls="normalfntRight";
						$dev1=$DayWiseClustDispSumArr1_without_Others[$j]-$dayClustTargtTot_without_others*($j+1);
						if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
							$dev1=0;
						}
						else{
							$dev1=$cumulativeClustDisp_without_Others[$j]-$dayClustTargtTot_without_others*($k+1);
							$k++;
						}
						$dev= number_format(abs($dev1),0);
						if($dev1<0){
							$dev="(".$dev .")";
							$cls="compulsoryRed";
						}
						?>
						<td class="<?php echo $cls; ?>" bgcolor="<?php echo $bgCol; ?>" align="right" ><?php echo $dev ?></td>
						<?php
					  }
					  ?>
                  <td width="80">&nbsp;</td>
                </tr>
                <!----------------------------->
				<?php
                    //include 'revenueReport_cluster_marketer_technique_wise.php';
                ?>
              </table></td>
            <td width="10">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="8" height="15"></td>
          </tr>
        </table></td>
      <td width="10">&nbsp;</td>
    </tr>
  </table>
  </td>
  </tr>
  <tr height="90" >
    <td align="center" class="normalfntMid"></td>
  </tr>
  </table>
</div>
 
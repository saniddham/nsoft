<?php
session_start();

if($_SESSION['userId']=='')
{
	echo "Please log in to the ERP system thruogh a seperate tab, and then refresh this link. Don't Close this tab till downloading is completed";
	die();
}


include '../../../dataAccess/dbManager2.php';
$db =  new DBManager2(1);           
$db->connect();

$year				= $_REQUEST['year'];
$month				= $_REQUEST['month'];
$month_name_last 	= date("F", mktime(0, 0, 0,$month, 10));		
 
ini_set('display_errors',0);
session_start();

//$backwardseperator = "../../../";

 
//include "../../../dataAccess/Connector.php";


/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2012 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2012 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.7, 2012-05-19
 */

/** Error reporting */
//error_reporting(E_ALL);

date_default_timezone_set('Europe/London');

/** Include PHPExcel */
require_once "../../../libraries/excel/Classes/PHPExcel.php";
		
//------------------------------------------------------
 
// Create new PHPExcel object
$objPHPExcel 	= new PHPExcel();

$alphas 		= range('A', 'Z');

$style_header 	= array(                  
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'FFDDDD'),
                ),
                'font' => array(
                    'bold' => true,
                )
                );


// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

//$objPHPExcel->getDefaultStyle()->getFont()->setSize(15);
// Add some data
$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue("$alphas[0]1", 'DEPARTMENT WISE PO REPORT') 
				->setCellValue("$alphas[0]2", 'Month'.' - '.$year." - ".$month_name_last) ;
				$i=4;
 			
			
$objPHPExcel->getActiveSheet()->getStyle("$alphas[3]$i")->getFont()->setSize(13);
$objPHPExcel->getActiveSheet()->getStyle("$alphas[3]$i")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("$alphas[3]$i")->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
			
$objPHPExcel->getActiveSheet()->getStyle("$alphas[0]1:$alphas[0]$i")->getFont()->setSize(13);
$objPHPExcel->getActiveSheet()->getStyle("$alphas[0]1:$alphas[0]$i")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("$alphas[0]1:$alphas[0]$i")->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);

//$objPHPExcel->getActiveSheet()->getStyle("$alphas[0]1:$alphas[0]$i")->getFont()->setColor(COLOR_GREEN,true);

  
$i+=2;
$j=17;
	
	
$l=$j;

$j=0;
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($alphas[$j++].$i, 'PO NUMBER')
            ->setCellValue($alphas[$j++].$i, 'PO YEAR')
			->setCellValue($alphas[$j++].$i, 'PO DATE')
            ->setCellValue($alphas[$j++].$i, 'DEPARTMENT')
            ->setCellValue($alphas[$j++].$i, 'MAIN CATEGORY')
            ->setCellValue($alphas[$j++].$i, 'SUB CATEGORY') 
            ->setCellValue($alphas[$j++].$i, 'ITEM CODE')  
            ->setCellValue($alphas[$j++].$i, 'ITEM')  
            ->setCellValue($alphas[$j++].$i, 'QTY')  
            ->setCellValue($alphas[$j++].$i, 'CURRENCY') 
            ->setCellValue($alphas[$j++].$i, 'UNIT PRICE'); 
            
$objPHPExcel->getActiveSheet()->getStyle("$alphas[0]$i:$alphas[$l]$i")->getFont()->setBold(true);

$p=$i;

$sql = "
		SELECT
trn_poheader.intPONo AS PO_NO,
trn_poheader.intPOYear AS PO_YEAR,
trn_poheader.dtmPODate AS PO_DATE,
mst_department.strName AS DEPARTMENT,
mst_maincategory.strName AS MAINCATEGORY,
mst_subcategory.strName AS SUBCATEGORY,
mst_item.strCode AS ITEM_CODE,
mst_item.strName AS ITEM_NAME,
trn_podetails.dblQty AS  QTY,
mst_financecurrency.strCode as CURRENCY,
trn_podetails.dblUnitPrice AS UNIT_PRICE
FROM
trn_podetails
INNER JOIN trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND
trn_podetails.intPOYear = trn_poheader.intPOYear
LEFT  JOIN mst_department ON mst_department.intId = trn_podetails.CostCenter
INNER JOIN mst_item ON mst_item.intId = trn_podetails.intItem
INNER JOIN mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
INNER JOIN mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
INNER JOIN mst_financecurrency ON mst_financecurrency.intId = trn_poheader.intCurrency

WHERE
trn_poheader.dtmPODate BETWEEN '$year-$month-1' AND '$year-$month-31'";
	
$result 			= $db->RunQuery($sql);
$r					=0;
$i=$p;
$arrAmmount = array();
while($row=mysqli_fetch_array($result))
{
		$i++; 
		$r++;

   		$j=0;
  		$l=9;
	//$department = $row['DEPARTMENT'];
	
	if($row['DEPARTMENT']=="")
	{
		$department = OTHERS;
	}
	else
	{
		$department = $row['DEPARTMENT'];
	}
  
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue($alphas[$j++].$i, $row['PO_NO'])
				->setCellValue($alphas[$j++].$i, $row['PO_YEAR']) 
				->setCellValue($alphas[$j++].$i, $row['PO_DATE']) 
				->setCellValue($alphas[$j++].$i, $department) 
				->setCellValue($alphas[$j++].$i, $row['MAINCATEGORY']) 
				->setCellValue($alphas[$j++].$i, $row['SUBCATEGORY'])
				->setCellValue($alphas[$j++].$i, $row['ITEM_CODE'])  
				->setCellValue($alphas[$j++].$i, $row['ITEM_NAME'])
				->setCellValue($alphas[$j++].$i, $row['QTY'])
				->setCellValue($alphas[$j++].$i, $row['CURRENCY'])
				->setCellValue($alphas[$j++].$i, $row['UNIT_PRICE']);
			//	->setCellValue($alphas[$j++].$i, $row['Ammount']); 
			
  //$objPHPExcel->getActiveSheet()->getStyle("E4:E$i")->getFont()->setBold(true);
}
 
$j=0;
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Department wise po report');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client's web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Monthly Revenue Breakdown.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

	$db->disconnect();
/*
mysqli_close($db);

*/
exit;	

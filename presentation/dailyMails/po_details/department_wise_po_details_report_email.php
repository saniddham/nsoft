<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<style>
.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntsm {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: right;
}
.compulsoryRed {
	color: #F00;
}
table .rptBordered {
    border-spacing: 0;
    width: 100%;
}
.rptBordered {
    border: 1px solid #CCCCCC;
    border-radius: 6px 6px 6px 6px;
    box-shadow: 0 1px 1px #CCCCCC;
    font-family: Verdana;
    font-size: 11px;
}
.rptBordered tr:hover {
    background: none repeat scroll 0 0 #FBF8E9;
    transition: all 0.1s ease-in-out 0s;
}
.rptBordered td {
    border-left: 1px solid #CCCCCC;
    border-top: 1px solid #CCCCCC;
    padding: 2px;
}
.rptBordered th {
    border-left: 1px solid #CCCCCC;
    border-top: 1px solid #CCCCCC;
    padding: 4px;
    text-align: center;
}
.rptBordered th {
    background-color: #CCCCCC;
    background-image: -moz-linear-gradient(center top , #EBF3FC, #CCCCCC);
    border-top: medium none;
    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.8) inset;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
}
.rptBordered td:first-child, .rptBordered th:first-child {
    border-left: medium none;
}
.rptBordered th:first-child {
    border-radius: 6px 0 0 0;
}
.rptBordered th:last-child {
    border-radius: 0 6px 0 0;
}
.rptBordered th:only-child {
    border-radius: 6px 6px 0 0;
}
.rptBordered tr:last-child td:first-child {
    border-radius: 0 0 0 6px;
}
.rptBordered tr:last-child td:last-child {
    border-radius: 0 0 6px 0;
}
</style>

</head>

<body>
<div align="center">
  <table width="100%">
  <tr>
    <td colspan="9" align="center" ><table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="tophead"><table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td width="15%"><img src=https://nsoft.nimawum.com/images/screenline.jpg" alt="" class="mainImage" /></td>
            <td width="1%" class="normalfnt">&nbsp;</td>
            <td align="center" valign="top" width="68%" class="topheadBLACK">              <b>Screenline (Private) Limited</b>
              <p class="normalfntMid">
                                Screenline (Pvt) Ltd,
No 30, Thalwatta, Gonawala, Kelaniya.   Sri Lanka.<br><b>Tel : </b>() <b>Fax : </b>() <br><b>E-Mail : </b> <b>Web : </b>http://www.screenlineholdings.com</p></td>
            <td width="16%" class="tophead">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr><td height="80" class="normalfnt"></td></tr>
  <tr><td class="normalfnt"><a href="<?php echo $actual_link.
  "/presentation/dailyMails/po_details/department_wise_po_details_report_excel.php?year=$year&month=$month_last" ?>"><u><b>Click here to download the excel sheet</b></u></a></td></tr>
  </table>
  </div>
 </body>
</html>
<?php
session_start();
date_default_timezone_set('Asia/Colombo');
ini_set('max_execution_time', 11111111) ;
//ini_set('display_errors',1);
$thisFilePath 			=  $_SERVER['PHP_SELF'];
 
include_once $_SESSION['ROOT_PATH']."class/dailyMails/marketerProjection/cls_sales_project_plan_get.php";
include_once $_SESSION['ROOT_PATH']."class/dailyMails/marketerProjection/cls_sales_project_plan_set.php";
  
$obj_projection_get		= new cls_sales_project_plan_get($db);
$obj_projection_set		= new cls_sales_project_plan_set($db);
  
$toCurrency  			= 1;//1=USD,2=LKR
$current_year			= date('Y');
$current_month			= date('m');

$d						= date('Y-m-d');
$previous_date			= date('Y-m-d', strtotime('-1 day', strtotime($d)));
$current_year			= substr($previous_date, 0, 4);	
$current_month			= substr($previous_date, 5, 2);

 
$cluster_result = $obj_comm->getMainClusters();

while($rowClust = mysqli_fetch_array($cluster_result))
{
	$cluster					= $rowClust['ID'];
  	//$result1 					= $obj_projection_get->get_marketers_result($current_year,$cluster,'');
	$result1 					= $obj_projection_get->load_marketers_of_cluster($current_year,$cluster,$current_month,$reportId);
	while($row=mysqli_fetch_array($result1))
	{
		$marketerId									=$row['intUserId'];
		$managerWiseTotDispQty[$marketerId]		=0;
		$marketerWiseTotRevenueQty[$marketerId]	=0;
	
		$month										= $current_month;
		$year										= $current_year;
		$newPlanQty 								= $obj_projection_get->getMarketerNewPlanQty($marketerId,$cluster,$month,$year);
		$orderWODQty 								= $obj_projection_get->getMarketerOrderQtyWithoutDispatch($marketerId,$cluster,$month,$year,$toCurrency);
		$dispatchValue 								= $obj_projection_get->getMarketerDispatchValue($marketerId,$cluster,$month,$year,$toCurrency);

		//$plannedQty									= $newPlanQty+$orderWODQty; //removed on 2018-05-02
		$plannedQty									= $obj_projection_get->getOrderValue_target_to_disp($marketerId,$cluster,$month,$year);
		
		
		$dispatchValue								= $dispatchValue; 
		if($plannedQty=='')
			$plannedQty=0;
		
		if($dispatchValue=='')
			$dispatchValue=0;
		
		$obj_projection_set->delete_sales_projections_old_data($marketerId,$cluster,$year,$month);
		$obj_projection_set->set_sales_projections_old_data($marketerId,$cluster,$year,$month,$plannedQty,$dispatchValue,$cluster);
	}
}
	   
 ?>
 
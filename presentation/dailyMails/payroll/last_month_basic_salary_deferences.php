<?php
	$cronjobId	= 17;

	$thisFilePath =  $_SERVER['PHP_SELF'];
  	include_once "../../../config.php";
	$path		= MAIN_URL;
	
	ob_start();
?>
<title>Basic Salary Defferences</title>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}


 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}


.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}



.odd{background-color:#F5F5F5;}
.even{background-color:#FFFFFF;}
.mouseover {
	cursor: pointer;
}


.txtNumber{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}



<?php

$This_month = date("m",strtotime("-1 month"));
$Last_month = date("m",strtotime("-2 month"));
$This_year  = date("Y",strtotime("-1 month"));
$last_year  = date("Y",strtotime("-2 month"));

$This_month_name = date("F", mktime(0, 0, 0, $This_month, 10));
$Last_month_name = date("F", mktime(0, 0, 0, $Last_month, 10));

?>

.bordered1 {	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}
.bordered1 {	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}
.bordered2 {	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}
.bordered2 {	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}
</style>
<table class="bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th colspan="9"> Increment Report</th>
  </tr>
  <tr>
    <th width="5%">Photo</th>
    <th width="15%">Company</th>
    <th width="5%">EPF No</th>
    <th width="27%">Employee Name</th>
    <th width="8%">NIC No</th>
    <th width="8%">Joined Date</th>
    <th width="11%">(<?php echo $Last_month_name; ?>) Basic Salary</th>
    <th width="12%">(<?php echo $This_month_name; ?>) Basic Salary</th>
    <th width="9%">Increment</th>
  </tr>
<?php


	 $SQL = "SELECT
		(qpay.trn_monthend_process.dblBasicSalary + qpay.trn_monthend_process.dblBRA) AS thisMonthBasic,
		(qpay.MonthEnd2.dblBasicSalary+MonthEnd2.dblBRA) AS lastMonthBasic,
		((qpay.trn_monthend_process.dblBasicSalary + qpay.trn_monthend_process.dblBRA)- (MonthEnd2.dblBasicSalary+MonthEnd2.dblBRA)) AS increment,
		qpay.mst_employee.strEmployeeCode,
		qpay.mst_employee.intTitle,
		qpay.mst_employee.strEmployeeName,
		qpay.mst_employee.strCallingName,
		qpay.mst_employee.strNIC,
		qpay.mst_employee.strMobileTeleNo,
		qpay.mst_employee.dtmJoinDate,
		qpay.mst_employee.Photo,
		qpay.mst_companies.strName as companyName
		FROM
		qpay.trn_monthend_process
		LEFT JOIN qpay.trn_monthend_process AS MonthEnd2 
			ON MonthEnd2.intYear = $last_year 
			AND MonthEnd2.intEmpId = qpay.trn_monthend_process.intEmpId AND MonthEnd2.intMonth = $Last_month
		INNER JOIN qpay.mst_employee ON qpay.mst_employee.intEmployeeId = qpay.trn_monthend_process.intEmpId
		INNER JOIN qpay.mst_companies ON qpay.mst_companies.intId = mst_employee.intCompanyId
		WHERE
		qpay.trn_monthend_process.intYear  =  $This_year AND
		qpay.trn_monthend_process.intMonth = $This_month and qpay.trn_monthend_process.intMst_companyId <> 9
		having increment>0
		order by qpay.mst_companies.strName,increment desc
		";
$RESULT = $db->RunQuery($SQL);
while($ROW=mysqli_fetch_array($RESULT))
{
?>
  <tr>
    <td><img width="54" height="47" src="http://122.255.62.148/qpay/<?php echo $ROW['Photo']; ?>"/></a></td>
    <td><?php echo $ROW['companyName']; ?></td>
     <td><?php echo $ROW['strEmployeeCode']; ?></td>
    <td><?php echo $ROW['strEmployeeName']; ?></td>
    <td><?php echo $ROW['strNIC']; ?></td>
    <td class="normalfntMid"><?php echo $ROW['dtmJoinDate']; ?></td>
    <td class="normalfntRight"><?php echo number_format($ROW['lastMonthBasic']); ?></td>
    <td class="normalfntRight"><?php echo number_format($ROW['thisMonthBasic']); ?></td>
    <td class="normalfntRight"><?php echo number_format($ROW['increment']);$total_increment+=$ROW['increment']; ?></td>
  </tr>
<?php
}
?>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="normalfntMid">&nbsp;</td>
    <td class="normalfntRight">&nbsp;</td>
    <td class="normalfntRight">Total Increment</td>
    <td class="normalfntRight" style="font-size:14px"><b><?php echo number_format($total_increment); ?></b></td>
  </tr>
</table>

<tr><td>&nbsp;</td></tr>

<table class="bordered" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#CEFDC6">
  <tr>
    <th colspan="9"> New Salary Report</th>
  </tr>
  <tr>
    <th width="5%">Photo</th>
    <th width="15%">Company</th>
    <th width="5%">EPF No</th>
    <th width="27%">Employee Name</th>
    <th width="8%">NIC No</th>
    <th width="8%">Joined Date</th>
    <th width="11%">(<?php echo $Last_month_name; ?>) Basic Salary</th>
    <th width="12%">(<?php echo $This_month_name; ?>) Basic Salary</th>
    <th width="9%">Increment</th>
  </tr>
  <?php


	 $SQL = "SELECT
		(qpay.trn_monthend_process.dblBasicSalary + qpay.trn_monthend_process.dblBRA) AS thisMonthBasic,
		(qpay.MonthEnd2.dblBasicSalary+MonthEnd2.dblBRA) AS lastMonthBasic,
		((qpay.trn_monthend_process.dblBasicSalary + qpay.trn_monthend_process.dblBRA)- (qpay.MonthEnd2.dblBasicSalary+MonthEnd2.dblBRA)) AS increment,
		qpay.mst_employee.strEmployeeCode,
		qpay.mst_employee.intTitle,
		qpay.mst_employee.strEmployeeName,
		qpay.mst_employee.strCallingName,
		qpay.mst_employee.strNIC,
		qpay.mst_employee.strMobileTeleNo,
		qpay.mst_employee.dtmJoinDate,
		qpay.mst_employee.Photo,
		qpay.mst_companies.strName as companyName
		FROM
		qpay.trn_monthend_process
		LEFT JOIN qpay.trn_monthend_process AS MonthEnd2 
			ON qpay.MonthEnd2.intYear = $last_year 
			AND qpay.MonthEnd2.intEmpId = qpay.trn_monthend_process.intEmpId AND qpay.MonthEnd2.intMonth = $Last_month
		INNER JOIN qpay.mst_employee ON qpay.mst_employee.intEmployeeId = qpay.trn_monthend_process.intEmpId
		INNER JOIN qpay.mst_companies ON qpay.mst_companies.intId = qpay.mst_employee.intCompanyId
		WHERE
		qpay.trn_monthend_process.intYear  =  $This_year AND
		qpay.trn_monthend_process.intMonth = $This_month and qpay.trn_monthend_process.intMst_companyId <> 9
		HAVING lastMonthBasic IS NULL
		order by qpay.mst_companies.strName,thisMonthBasic desc
		";
$RESULT = $db->RunQuery($SQL);
while($ROW1=mysqli_fetch_array($RESULT))
{
?>
  <tr>
    <td><img width="54" height="47" src="http://122.255.62.148/qpay/<?php echo $ROW1['Photo']; ?>"/></td>
    <td><?php echo $ROW1['companyName']; ?></td>
    <td><?php echo $ROW1['strEmployeeCode']; ?></td>
    <td><?php echo $ROW1['strEmployeeName']; ?></td>
    <td><?php echo $ROW1['strNIC']; ?></td>
    <td class="normalfntMid"><?php echo $ROW1['dtmJoinDate']; ?></td>
    <td class="normalfntRight"><?php echo number_format(0); ?></td>
    <td class="normalfntRight"><?php echo number_format($ROW1['thisMonthBasic']); ?></td>
    <td class="normalfntRight"><?php echo number_format(0);$total_newsalary+=$ROW1['thisMonthBasic']; ?></td>
  </tr>
  <?php
}
?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="normalfntMid">&nbsp;</td>
    <td class="normalfntRight">&nbsp;</td>
    <td class="normalfntRight">Total New Employee Basic Salary</td>
    <td class="normalfntRight" style="font-size:14px"><b><?php echo number_format($total_newsalary); ?></b></td>
  </tr>
</table>

<tr><td>&nbsp;</td></tr>
<table class="bordered" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FCD1C5">
  <tr>
    <th colspan="9"> Resign Salary Report</th>
  </tr>
  <tr>
    <th width="5%">Photo</th>
    <th width="15%">Company</th>
    <th width="5%">EPF No</th>
    <th width="27%">Employee Name</th>
    <th width="8%">NIC No</th>
    <th width="8%">Joined Date</th>
    <th width="11%">(<?php echo $Last_month_name; ?>) Basic Salary</th>
    <th width="12%">(<?php echo $This_month_name; ?>) Basic Salary</th>
    <th width="9%">Increment</th>
  </tr>
  <?php


	 $SQL = "SELECT
		(qpay.trn_monthend_process.dblBasicSalary + qpay.trn_monthend_process.dblBRA) AS lastMonthBasic ,
		(qpay.MonthEnd2.dblBasicSalary+MonthEnd2.dblBRA) AS thisMonthBasic ,
		((qpay.trn_monthend_process.dblBasicSalary + qpay.trn_monthend_process.dblBRA)- (qpay.MonthEnd2.dblBasicSalary+MonthEnd2.dblBRA)) AS increment,
		qpay.mst_employee.strEmployeeCode,
		qpay.mst_employee.intTitle,
		qpay.mst_employee.strEmployeeName,
		qpay.mst_employee.strCallingName,
		qpay.mst_employee.strNIC,
		qpay.mst_employee.strMobileTeleNo,
		qpay.mst_employee.dtmJoinDate,
		qpay.mst_employee.Photo,
		qpay.mst_companies.strName as companyName
		FROM
		qpay.trn_monthend_process
		LEFT JOIN qpay.trn_monthend_process AS MonthEnd2 
			ON qpay.MonthEnd2.intYear =  $This_year
			AND qpay.MonthEnd2.intEmpId = qpay.trn_monthend_process.intEmpId AND qpay.MonthEnd2.intMonth = $This_month 
		INNER JOIN qpay.mst_employee ON qpay.mst_employee.intEmployeeId = qpay.trn_monthend_process.intEmpId
		INNER JOIN qpay.mst_companies ON qpay.mst_companies.intId = qpay.mst_employee.intCompanyId
		WHERE
		qpay.trn_monthend_process.intYear  =  $last_year  AND
		qpay.trn_monthend_process.intMonth = $Last_month and qpay.trn_monthend_process.intMst_companyId <> 9
		HAVING thisMonthBasic IS NULL
		order by qpay.mst_companies.strName,lastMonthBasic desc
		";
$RESULT = $db->RunQuery($SQL);
while($ROW1=mysqli_fetch_array($RESULT))
{
?>
  <tr>
    <td><img width="54" height="47" src="<?php echo $path;?>../qpay/<?php echo $ROW1['Photo']; ?>"/></td>
    <td><?php echo $ROW1['companyName']; ?></td>
    <td><?php echo $ROW1['strEmployeeCode']; ?></td>
    <td><?php echo $ROW1['strEmployeeName']; ?></td>
    <td><?php echo $ROW1['strNIC']; ?></td>
    <td class="normalfntMid"><?php echo $ROW1['dtmJoinDate']; ?></td>
    <td class="normalfntRight"><?php echo number_format($ROW1['lastMonthBasic']); ?></td>
    <td class="normalfntRight"><?php echo number_format($ROW1['thisMonthBasic']); ?></td>
    <td class="normalfntRight"><?php echo number_format(0);$total_resignsalary+=$ROW1['lastMonthBasic']; ?></td>
  </tr>
  <?php
}
?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="normalfntMid">&nbsp;</td>
    <td class="normalfntRight">&nbsp;</td>
    <td class="normalfntRight">Total Resign Basic Salary</td>
    <td class="normalfntRight" style="font-size:14px"><b><?php echo number_format($total_resignsalary); ?></b></td>
  </tr>
</table>
<table class="bordered" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="67%">&nbsp;</td>
    <td width="25%">&nbsp;</td>
    <td width="8%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><strong>Total Increments</strong></td>
    <td align="right"><strong><?php echo number_format($total_increment); ?></strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><strong>Total New Employee Salaries</strong></td>
    <td align="right"><strong><?php echo number_format($total_newsalary); ?></strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><strong>Total Resign Employee Salaries</strong></td>
    <td align="right" style="color:#F00"><strong><?php echo number_format($total_resignsalary); ?></strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><strong>Total Gain Amount for the Salaries(Basic)</strong></td>
    <td style="font-size:14px" align="right"><strong><?php echo number_format($total_increment+$total_newsalary-$total_resignsalary); ?></strong></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<?php

$day = date('d');
if($day != 11)
die();

		echo $body = ob_get_clean();

		$FROM_NAME			= "$This_month_name SALARY INCREMENT(NSOFT)";
		$FROM_EMAIL			= "";
		 
		$mailHeader= "$This_month_name SALARY INCREMENT";
	
		$cron_emails_to		=$obj_comm->getCronjobEmailIds($cronjobId,'TO');
		$cron_emails_cc		=$obj_comm->getCronjobEmailIds($cronjobId,'CC');
		$cron_emails_bcc	=$obj_comm->getCronjobEmailIds($cronjobId,'BCC');
		
		$mail_TO			= $obj_comm->getEmailList($cron_emails_to);
		$mail_CC			= $obj_comm->getEmailList($cron_emails_cc);
		$mail_BCC			= $obj_comm->getEmailList($cron_emails_bcc);
		
		insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
		
		//sendMessage($FROM_EMAIL,$FROM_NAME,'priyadarshana@screenlineholdings.com',$mailHeader,$body,'nsoftemail@gmail.com,thanuja@screenlineholdings.com','roshan.nsoft@gmail.com');
		//sendMessage($FROM_EMAIL,$FROM_NAME,'roshan.nsoft@gmail.com',$mailHeader,$body,'','');

?>
 
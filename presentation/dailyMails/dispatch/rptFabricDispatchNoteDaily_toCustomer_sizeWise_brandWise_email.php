<?php
$cronjobId	=2;

$thisFilePath =  $_SERVER['PHP_SELF'];

  $sql 	 =	"SELECT DISTINCT
				ware_fabricdispatchheader.intCustLocation,
				trn_orderheader.intCustomer,
				mst_customer.strName AS customerName,
				mst_customer_locations_header.strName AS customerLocationName,
				mst_customer_brand_wise_emails.BRAND_ID,
mst_customer_brand_wise_emails.EMAIL as strEmailAddress,
mst_customer_brand_wise_emails.EMAIL_CC as strCCEmailAddress
			FROM
				ware_fabricdispatchheader
			INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo
				 AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
			INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = ware_fabricdispatchheader.intOrderNo 
				AND trn_orderheader.intOrderYear = ware_fabricdispatchheader.intOrderYear
			INNER JOIN ware_fabricdispatchheader_approvedby ON 
				ware_fabricdispatchheader_approvedby.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo 
				AND ware_fabricdispatchheader_approvedby.intYear = ware_fabricdispatchheader.intBulkDispatchNoYear
			INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
			INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = ware_fabricdispatchheader.intCustLocation
			INNER JOIN mst_customer_locations ON mst_customer.intId = mst_customer_locations.intCustomerId AND mst_customer_locations_header.intId = mst_customer_locations.intLocationId
INNER JOIN mst_customer_brand_wise_emails ON mst_customer_brand_wise_emails.CUSTOMER_ID = trn_orderheader.intCustomer AND mst_customer_brand_wise_emails.LOCATION_ID = ware_fabricdispatchheader.intCustLocation
INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intBrand = mst_customer_brand_wise_emails.BRAND_ID AND trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo AND trn_sampleinfomations.intCustomer = mst_customer_brand_wise_emails.CUSTOMER_ID
WHERE
			
				ware_fabricdispatchheader_approvedby.dtApprovedDate  < CONCAT(DATE(NOW()),' ','05:00:00')  AND  
				ware_fabricdispatchheader_approvedby.dtApprovedDate >= CONCAT(DATE(DATE_ADD(NOW(),INTERVAL -1 DAY )),' ','05:00:00')  AND
				ware_fabricdispatchheader.intStatus = 1 AND 
				mst_customer.intSizeWiseSummary  =  1 		AND
				mst_customer.intBrandWiseEmails  IN (1,2) AND
				
				
				(
				ware_fabricdispatchdetails.dblSampleQty+
				ware_fabricdispatchdetails.dblGoodQty+
				ware_fabricdispatchdetails.dblEmbroideryQty+
				ware_fabricdispatchdetails.dblPDammageQty+
				ware_fabricdispatchdetails.dblFdammageQty+
				ware_fabricdispatchdetails.dblCutRetQty
				) >0
	
			ORDER BY intCustomer";
$result_main  = $db->RunQuery($sql);
while($row_main=mysqli_fetch_array($result_main))
{		 
	
	$customerId 			= $row_main["intCustomer"];
	$customerLocationId 	= $row_main["intCustLocation"];
	$custName			 	= $row_main["customerName"];
	$custLocName		 	= $row_main["customerLocationName"];
	$pub_brandId			= $row_main["BRAND_ID"];
	
	$strEmailAddress		 	= $row_main["strEmailAddress"];
	$strCCEmailAddress	 		= $row_main["strCCEmailAddress"];
	
	ob_start();
	//include "rptFabricDispatchNoteDaily_toCustomer_sizeWise_brandWise_report.php";
	include "rptFabricDispatchNoteDaily_toCustomer_sizeWise_report.php";
	$body = ob_get_clean();
	
	if($haveRecords)
	{
	//$body =  $strEmailAddress.'<br>'. $strCCEmailAddress . '<br>'.  $body;
	echo $body;
	
	//$strCCEmailAddress .=',buddhikak@screenlineholdings.com,jeremy@screenlineholdings.com,pradeep@screenlineholdings.com';

	//Original
	//sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.',$strEmailAddress,$mailHeader,$body,$strCCEmailAddress,'roshan.nsoft@gmail.com,nsoftemail@gmail.com');

		$FROM_NAME			= "NSOFT - SCREENLINE HOLDINGS";
		$FROM_EMAIL			= "";
		 
		$nowDate = date('Y-m-d');
		$mailHeader= "DISPATCH SUMMARY - ALL CLUSTERS($nowDate) - $STR_GRAHPICNO_LIST";
	
		$cron_emails_to		=$obj_comm->getCronjobEmailIds($cronjobId,'TO');
		$cron_emails_cc		=$obj_comm->getCronjobEmailIds($cronjobId,'CC');
		$cron_emails_bcc	=$obj_comm->getCronjobEmailIds($cronjobId,'BCC');
		
		//Original
		$mail_TO	= $strEmailAddress.",".$obj_comm->getEmailList($cron_emails_to);
		$mail_CC	= $strCCEmailAddress.",".$obj_comm->getEmailList($cron_emails_cc);
		$mail_BCC	= $obj_comm->getEmailList($cron_emails_bcc);

		if ($strEmailAddress != null && $strEmailAddress != ''){
            insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
		}

	
	//testing only
	//$body = "to-$strEmailAddress <br> cc-$strCCEmailAddress <br> $body";
	//sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.','roshan.nsoft@gmail.com',$mailHeader,$body,'','');
	}
}
?>
 
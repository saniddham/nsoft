<?php
$cronjobId	= 9;

$thisFilePath =  $_SERVER['PHP_SELF'];
 
$sql 	 =	"SELECT DISTINCT
				ware_fabricdispatchheader.intCustLocation,
				trn_orderheader.intCustomer,
				mst_customer.strName AS customerName,
				mst_customer_locations_header.strName AS customerLocationName,
				mst_customer_locations.strEmailAddress,
				mst_customer_locations.strCCEmailAddress
			FROM
				ware_fabricdispatchheader
			INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo
				 AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
			INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = ware_fabricdispatchheader.intOrderNo 
				AND trn_orderheader.intOrderYear = ware_fabricdispatchheader.intOrderYear
			INNER JOIN ware_fabricdispatchheader_approvedby ON 
				ware_fabricdispatchheader_approvedby.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo 
				AND ware_fabricdispatchheader_approvedby.intYear = ware_fabricdispatchheader.intBulkDispatchNoYear
			INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
			INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = ware_fabricdispatchheader.intCustLocation
			INNER JOIN mst_customer_locations ON mst_customer.intId = mst_customer_locations.intCustomerId AND mst_customer_locations_header.intId = mst_customer_locations.intLocationId
			INNER JOIN mst_locations ON mst_locations.intId = ware_fabricdispatchheader.intCompanyId
			INNER JOIN mst_plant ON mst_plant.intPlantId = mst_locations.intPlant
			WHERE
				ware_fabricdispatchheader_approvedby.dtApprovedDate  < CONCAT(DATE(NOW()),' ','05:00:00')  AND  
				ware_fabricdispatchheader_approvedby.dtApprovedDate >= CONCAT(DATE(DATE_ADD(NOW(),INTERVAL -1 DAY )),' ','05:00:00')  AND
				ware_fabricdispatchheader.intStatus = 1 AND 
				mst_customer.intSizeWiseSummary  =  1 	AND
				mst_customer.intBrandWiseEmails  = 0 	AND
				mst_plant.MAIN_CLUSTER_ID= 2 AND  /* 2 = india plants */
				
				(
				ware_fabricdispatchdetails.dblSampleQty+
				ware_fabricdispatchdetails.dblGoodQty+
				ware_fabricdispatchdetails.dblEmbroideryQty+
				ware_fabricdispatchdetails.dblPDammageQty+
				ware_fabricdispatchdetails.dblFdammageQty+
				ware_fabricdispatchdetails.dblCutRetQty
				) >0
	
			ORDER BY intCustomer";
$result_main  = $db->RunQuery($sql);
while($row_main=mysqli_fetch_array($result_main))
{		 
	
	$customerId 			= $row_main["intCustomer"];
	$customerLocationId 	= $row_main["intCustLocation"];
	$custName			 	= $row_main["customerName"];
	$custLocName		 	= $row_main["customerLocationName"];
	
	$strEmailAddress		 	= $row_main["strEmailAddress"];
	$strCCEmailAddress	 		= $row_main["strCCEmailAddress"];
	
	ob_start();
	include "rptFabricDispatchNoteDaily_toCustomer_sizeWise_report.php";
	$body = ob_get_clean();
	
	if($haveRecords)
	{
		//$body =  $strEmailAddress.'<br>'. $strCCEmailAddress . '<br>'.  $body;
		echo $body;
		
 		
		$nowDate 		= date('Y-m-d');
		$mailHeader		= "DISPATCH SUMMARY - INDIA ($nowDate) - $STR_GRAHPICNO_LIST";
 		$FROM_EMAIL		= "";
	
		$cron_emails_to		=$obj_comm->getCronjobEmailIds($cronjobId,'TO');
		$cron_emails_cc		=$obj_comm->getCronjobEmailIds($cronjobId,'CC');
		$cron_emails_bcc	=$obj_comm->getCronjobEmailIds($cronjobId,'BCC');
		
		$mail_TO			= $strEmailAddress.",".$obj_comm->getEmailList($cron_emails_to);
		$mail_CC			= $strCCEmailAddress.",".$obj_comm->getEmailList($cron_emails_cc);
		$mail_BCC			= $obj_comm->getEmailList($cron_emails_bcc);
		
		//Original
			insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
		
		
		//testing only
		//sendMessage($FROM_EMAIL,$FROM_NAME,'roshan.nsoft@gmail.com',$mailHeader,$body,'','');
	}
}
?>
 
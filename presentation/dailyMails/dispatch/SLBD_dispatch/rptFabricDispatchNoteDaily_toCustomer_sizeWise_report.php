<?php

$thisFilePath =  $_SERVER['PHP_SELF'];
 ?>
<title>Customer / Size wise Dispatch report</title>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}


 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}


.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}



.odd{background-color:#F5F5F5;}
.even{background-color:#FFFFFF;}
.mouseover {
	cursor: pointer;
}


.txtNumber{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}





.bordered1 {	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}
.bordered1 {	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}
</style>
</head>
</head>

<body><form id="frmFabDispatchReport" name="frmFabDispatchReport" method="post" action="rptFabricDispatchNote.php">
<table width="1200" cellpadding="0" cellspacing="0" align="center">
<tr>
<td colspan="3"></td>
</tr>
<tr>
    <td colspan="9" align="center" ><table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="tophead"><table  width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td width="8%" class="normalfnt"><img style="vertical-align:" src="http://accsee.com/screenline.jpg"  /></td>
            <td style="background:" align="center" valign="top" width="75%" class="topheadBLACK"><?php
	 $SQL = "SELECT
*
FROM
mst_companies
Inner Join mst_country ON mst_companies.intCountryId = mst_country.intCountryID
WHERE
mst_companies.intId =  '9'
;";
	
	$result = $db->RunQuery($SQL);
	$row = mysqli_fetch_array($result);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];
	
	?>
              <b><?php echo $companyName.""; ?></b>
              <p class="normalfntMid">
                <?php // (".$locationName.") --> Edited By Lasantha 14th of May 2012?>
                <?php echo $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry."."."<br><b>Tel : </b>".$companyPhone." <b>Fax : </b>".$companyFax." <br><b>E-Mail : </b>".$companyEmail." <b>Web : </b>".$companyWeb;?></p></td>
            <td width="9%" class="tophead"><img src="http://accsee.com/nsoft_logo.png" style="vertical-align:"  /></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ></div>
<table width="1200" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td colspan="2">
  <table width="100%">
  <tr>
    <td colspan="10" align="center"><span style="background-color:#FFF"><div align="left"></div>DISPATCH SUMMARY REPORT</span></td>
    </tr>
  <tr>
    <td width="5%">&nbsp;</td>
    <td width="11%" class="normalfnt">Customer</td>
    <td width="4%" align="center" valign="middle"><strong>:</strong></td>
    <td width="20%"><span class="normalfnt"><?php echo $custName; ?></span></td>
    <td width="7%"><span class="normalfnt">Location</span></td>
    <td width="4%" align="center" valign="middle"><strong>:</strong></td>
    <td width="24%"><span class="normalfnt"><?php echo $custLocName; ?></span></td>
    <td width="6%" class="normalfnt">Date</td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="18%"><span class="normalfnt"><?php echo date('Y-m-d'); ?></span></td>
  </tr>
  
  </table>
  </td>
</tr>
<tr>
  <td colspan="2">
    <table width="100%">
      <tr>
        <td>
          
          <table width="100%"  class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <thead>
              <tr class="">
                <th width="8%" >Plant</th>
                <th width="4%" >Brand</th>
                <th width="6%" >Dispatch No</th>
                <th width="5%" >Dispatch Date/Time</th>
                <th width="4%" >Order No</th>
                <th width="5%" >Sales Order No</th>
                <th width="5%" >Graphic No</th>
                <th width="6%" >Style No</th>
                <th width="14%" >Customer PO</th>
                <th width="4%" >Size</th>
                <th width="5%" >Sample Qty</th>
                <th width="4%" >Good Qty</th>
                <th width="4%" >EMD Qty</th>
                <th width="3%" >P-D Qty</th>
                <th width="4%" >F-D Qty</th>
                <th width="4%" >Cut Ret Qty</th>
                <th width="6%" >Total Qty</th>
                </tr>
              </thead>
            <tbody>
              <?php 
			  $curruntTime=date("Y-m-d H:i:s");
			  $curruntTimeFrom = date("Y-m-d H:i:s",strtotime(date('Y-m-j H:i:s')) - (24 * 60 * 60));//correct
			 //$curruntTimeFrom = date("Y-m-d H:i:s",strtotime(date('Y-m-j H:i:s')) - (2*60*24 * 60 * 60));//temp
			  
			  
			  
			  //comment by roshan for adding customer brand and dispatch no
			  
	  	      /*$sql1 = "SELECT 
trn_orderdetails.strStyleNo,
trn_orderdetails.strGraphicNo, 
trn_orderheader.strCustomerPoNo,
ware_fabricdispatchheader.intOrderNo,
ware_fabricdispatchheader.intOrderYear,
trn_orderdetails.strSalesOrderNo,
sum(ware_fabricdispatchdetails.dblSampleQty) as dblSampleQty,
sum(ware_fabricdispatchdetails.dblGoodQty) as dblGoodQty,
sum(ware_fabricdispatchdetails.dblEmbroideryQty) as dblEmbroideryQty,
sum(ware_fabricdispatchdetails.dblPDammageQty) as dblPDammageQty,
sum(ware_fabricdispatchdetails.dblFDammageQty) as dblFDammageQty 
FROM

trn_orderdetails 
Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear 
Inner Join ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear
Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
left Join mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
left Join mst_colors_ground ON ware_fabricdispatchdetails.intGroundColor = mst_colors_ground.intId 
Inner Join ware_fabricdispatchheader_approvedby ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchheader_approvedby.intBulkDispatchNo 
AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchheader_approvedby.intYear AND ware_fabricdispatchheader_approvedby.intApproveLevelNo=ware_fabricdispatchheader.intApproveLevels 
WHERE  
ware_fabricdispatchheader_approvedby.dtApprovedDate <'$curruntTime' AND  
ware_fabricdispatchheader_approvedby.dtApprovedDate >= '$curruntTimeFrom' AND  
ware_fabricdispatchheader.intStatus =  '1'  
AND trn_orderheader.intCustomer = '$intCustomer'

group by trn_orderdetails.strSalesOrderNo , 
ware_fabricdispatchheader.intOrderNo, 
ware_fabricdispatchheader.intOrderYear  
order by 
ware_fabricdispatchheader.intOrderYear asc , 
ware_fabricdispatchheader.intOrderNo asc , 
trn_orderdetails.strSalesOrderNo asc ,
trn_orderdetails.strStyleNo asc,
trn_orderheader.strCustomerPoNo asc 

";*/
if($pub_brandId!='')
	$para_brand = "AND trn_sampleinfomations.intBrand = $pub_brandId ";
else
	$para_brand = "";
	
			$sql1 = "SELECT
distinct 
ware_fabricdispatchheader.intBulkDispatchNo,
ware_fabricdispatchheader.intBulkDispatchNoYear,
ware_fabricdispatchheader_approvedby.dtApprovedDate,
TRIM(trn_orderdetails.strStyleNo) AS strStyleNo,
TRIM(trn_orderdetails.strGraphicNo) AS strGraphicNo,
trn_orderheader.strCustomerPoNo,
ware_fabricdispatchheader.intOrderNo,
ware_fabricdispatchheader.intOrderYear,
trn_orderdetails.strSalesOrderNo,
ware_fabricdispatchdetails.strSize,
Sum(ware_fabricdispatchdetails.dblSampleQty) AS dblSampleQty,
Sum(ware_fabricdispatchdetails.dblGoodQty) AS dblGoodQty,
Sum(ware_fabricdispatchdetails.dblEmbroideryQty) AS dblEmbroideryQty,
Sum(ware_fabricdispatchdetails.dblPDammageQty) AS dblPDammageQty,
Sum(ware_fabricdispatchdetails.dblFDammageQty) AS dblFDammageQty,
Sum(ware_fabricdispatchdetails.dblCutRetQty) AS dblCutRetQty,
mst_brand.strName AS brandName,
trn_orderdetails.dblDamagePercentage,
mst_plant.strPlantName								 							AS plantName,
CONCAT(mst_plant.strPlantHeadName,'(', mst_plant.strPlantName  ,')',' - ',strPlantHeadContactNo) 						AS PlantHeadName,

ROUND(COALESCE((SELECT
  SUM(dblQty)
FROM ware_fabricreceivedheader FRH
  INNER JOIN ware_fabricreceiveddetails FRD
    ON FRD.intFabricReceivedNo = FRH.intFabricReceivedNo
      AND FRD.intFabricReceivedYear = FRH.intFabricReceivedYear
  INNER JOIN trn_orderdetails SUB_OD 
    ON SUB_OD.intOrderNo = FRH.intOrderNo 
      AND SUB_OD.intOrderYear = FRH.intOrderYear
      AND SUB_OD.intSalesOrderId = FRD.intSalesOrderId 
WHERE 
	FRH.intOrderNo = trn_orderheader.intOrderNo
    AND FRH.intOrderYear = trn_orderheader.intOrderYear
	AND SUB_OD.strGraphicNo = trn_orderdetails.strGraphicNo
	AND FRD.strSize = ware_fabricdispatchdetails.strSize
	AND FRH.intStatus = 1),0))						AS TOTAL_IN,

ROUND(COALESCE((SELECT SUM(dblGoodQty)
FROM ware_fabricdispatchheader FDH
  INNER JOIN ware_fabricdispatchdetails FDD
    ON FDD.intBulkDispatchNo = FDH.intBulkDispatchNo
      AND FDD.intBulkDispatchNoYear = FDH.intBulkDispatchNoYear
  INNER JOIN trn_orderdetails SUB_OD 
    ON SUB_OD.intOrderNo = FDH.intOrderNo 
      AND SUB_OD.intOrderYear = FDH.intOrderYear
      AND SUB_OD.intSalesOrderId = FDD.intSalesOrderId
WHERE 
	FDH.intOrderNo = trn_orderheader.intOrderNo
	AND FDH.intOrderYear = trn_orderheader.intOrderYear
	AND SUB_OD.strGraphicNo = trn_orderdetails.strGraphicNo
	AND FDD.strSize = ware_fabricdispatchdetails.strSize
	AND FDH.intStatus = 1),0))														AS TOTAL_DELEVERY
	
FROM
trn_orderdetails
Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
Inner Join ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear
Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
Left Join mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
Left Join mst_colors_ground ON ware_fabricdispatchdetails.intGroundColor = mst_colors_ground.intId
Inner Join ware_fabricdispatchheader_approvedby ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchheader_approvedby.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchheader_approvedby.intYear AND ware_fabricdispatchheader_approvedby.intApproveLevelNo = ware_fabricdispatchheader.intApproveLevels
Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo
Inner Join mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
INNER JOIN mst_locations 
	ON mst_locations.intId = ware_fabricdispatchheader.intCompanyId
INNER JOIN mst_companies
	ON mst_companies.intId = mst_locations.intCompanyId
INNER JOIN mst_plant 
    ON mst_plant.intPlantId = mst_locations.intPlant
WHERE  
ware_fabricdispatchheader_approvedby.dtApprovedDate < CONCAT(DATE(NOW()),' ','05:00:00')  AND  
ware_fabricdispatchheader_approvedby.dtApprovedDate >= CONCAT(DATE(DATE_ADD(NOW(),INTERVAL -1 DAY )),' ','05:00:00')  AND  
ware_fabricdispatchheader.intStatus =  '1'  
AND trn_orderheader.intCustomer = $customerId 
AND ware_fabricdispatchheader.intCustLocation = $customerLocationId

$para_brand

GROUP BY
	trn_orderdetails.strSalesOrderNo,
	ware_fabricdispatchheader.intOrderNo,
	ware_fabricdispatchheader.intOrderYear,
	trn_sampleinfomations.intBrand,
	ware_fabricdispatchheader.intBulkDispatchNo,
	ware_fabricdispatchheader.intBulkDispatchNoYear,
	ware_fabricdispatchdetails.strSize
ORDER BY
	mst_brand.strName,
	ware_fabricdispatchheader.intOrderYear ASC,
	ware_fabricdispatchheader.intOrderNo ASC,
	trn_orderdetails.strSalesOrderNo ASC,
	trn_orderdetails.strStyleNo ASC,
	trn_orderheader.strCustomerPoNo ASC,
	ware_fabricdispatchheader.intBulkDispatchNo ASC,
	ware_fabricdispatchheader.intBulkDispatchNoYear ASC,
	ware_fabricdispatchdetails.strSize ASC
";
//echo($sql1);
			$result1 = $db->RunQuery($sql1);
			
			$totsampleQty=0;
			$totgoodQty=0;
			$totembroideryQty=0;
			$totpDammageQty=0;
			$totfDammageQty=0;
			$totCutRetQty=0;
			$total=0;
			$totQty=0;
			$totAmmount=0;
			$tot_totalIn		  =0;
			$tot_totalOut		  =0;
			
			
			$i	= 0;
			$haveRecords = false;
			$STR_GRAHPICNO_LIST = '';
			$STR_GRAHPICNO_LIST2 = '';
			$STR_STYLENO_LIST ='';
			$array_in	= array();
			$graphicNo = '';
			
			$s_totalSampleQty = 0;
			$s_totalGoodQty	  = 0;
			$s_embdQty		  = 0;
			$s_pdQty		  = 0;
			$s_fdQty		  = 0;
			$s_cutRetQty	  = 0;
			$s_total 		  = 0; 
        	$s_totalIn		  = 0;
			$s_totalOut		  = 0;
			
			while($row1=mysqli_fetch_array($result1))
			{
				$array_in[]	= $row1["PlantHeadName"];
				$haveRecords = true;
				$intBulkDispatchNo 		= $row1['intBulkDispatchNo'];
				$intBulkDispatchNoYear 	= $row1['intBulkDispatchNoYear'];
				$dispatchNo				 = $intBulkDispatchNoYear . '/'. $intBulkDispatchNo;
				
				$dblDamagePercentage = $row1['dblDamagePercentage'];
				$brandName = $row1['brandName'];
				$plantHeadName	= $row1["PlantHeadName"];
				 
				
				/////////////////concat graphic no's///////////
						if($graphicNo!=$row1['strGraphicNo'])
						{
							$STR_GRAHPICNO_LIST .=$row1['strGraphicNo'] . ' / ';	//for email header (dont remove)
							$STR_GRAHPICNO_LIST2 .= ($STR_GRAHPICNO_LIST2!=''?',':"'").$row1['strGraphicNo'];	//for summary bottom
						}
						if($style!=$row1['strStyleNo'])
						{
							
							$STR_STYLENO_LIST .= ($STR_STYLENO_LIST!=''?',':"'").$row1['strStyleNo'];	//for summary bottom
						}
				///////////////////////////////////////////////
				$style = $row1['strStyleNo'];
				$graphicNo = $row1['strGraphicNo']; 
				$custPO = $row1['strCustomerPoNo'];
				$orderNo = $row1['intOrderNo'];
				$orderYear = $row1['intOrderYear'];
				
				$cutNo=$row1['strCutNo'];
				$salesOrderNo=$row1['strSalesOrderNo'];
				$part=$row1['part'];
				$bgColor=$row1['bgcolor'];
				$lineNo=$row1['strLineNo'];
				$size=$row1['strSize'];
				$sampleQty=$row1['dblSampleQty'];
				$goodQty=$row1['dblGoodQty'];
				$embroideryQty=$row1['dblEmbroideryQty'];
				$pDammageQty=$row1['dblPDammageQty'];
				$fDammageQty=$row1['dblFDammageQty'];
				$cutRetQty=$row1['dblCutRetQty'];
				//dtApprovedDate
				$dtApprovedDate=$row1['dtApprovedDate'];
				
				$totalOut = $row1["TOTAL_DELEVERY"];
				$totalIn	= $row1["TOTAL_IN"];
				
				$remarks=$row1['strRemarks'];
				$tot=(float)($sampleQty)+(float)($goodQty)+(float)($embroideryQty)+(float)($pDammageQty)+(float)($fDammageQty)+ (float)$cutRetQty;
				
	
	if($graphicNo != $graphicNo2 || $salesOrderNo != $salesOrderNo2 || $orderno !=$orderno2 || $orderYear != $orderYear2 || $plant2!=$plant  || $dispatchNo2!=$dispatchNo )
		{
			if(++$i>1)
			{
			/*$graphicNo2 = '';
			$salesOrderNo2 = '' ;
			$orderNo2 = '';
			$orderyear2 = '';
			$plant2 = '';*/
			
	?>
    <tr>
    	<td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo number_format($s_totalSampleQty); ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo number_format($s_totalGoodQty); ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo number_format($s_embdQty); ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo number_format($s_pdQty); ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo number_format($s_fdQty); ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo number_format($s_cutRetQty); ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo number_format($s_total) ; ?></b></td>
        
        <?php 
			$s_totalSampleQty = 0;
			$s_totalGoodQty	  = 0;
			$s_embdQty		  = 0;
			$s_pdQty		  = 0;
			$s_fdQty		  = 0;
			$s_cutRetQty	  = 0;
			$s_total 		  = 0; 
			$s_totalIn		  = 0;
			$s_totalOut		  = 0;
		?>
    </tr>
    <tr>
    	<td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        
    </tr>
    <?php 
			}
		}
	?>
              <tr class="normalfnt"   bgcolor="#FFFFFF">
                <td align="center" class="normalfntMid" id="<?php echo $orderYear; ?>" ><?php echo $row1["plantName"]; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $orderYear; ?>" ><?php echo $brandName; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $orderYear; ?>" ><?php echo $intBulkDispatchNoYear.'/'.$intBulkDispatchNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $orderNo; ?>" ><?php echo $dtApprovedDate; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $orderNo; ?>" ><?php echo $orderYear.'/'.$orderNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $salesOrderNo; ?>" ><?php echo $salesOrderNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $graphicNo; ?>" ><?php echo $graphicNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $style; ?>" ><?php echo $style; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $custPO; ?>" ><?php echo $custPO; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $qty; ?>"><?php echo $size;?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo number_format($sampleQty); ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo number_format($goodQty); ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo number_format($embroideryQty); ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo number_format($pDammageQty); ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo number_format($fDammageQty); ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo number_format($cutRetQty); ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo number_format($tot); ?></td>
                </tr>              
              <?php 
			$totsampleQty+=$sampleQty;
			$totgoodQty+=$goodQty;
			$totembroideryQty+=$embroideryQty;
			$totpDammageQty+=$pDammageQty;
			$totfDammageQty+=$fDammageQty;
			$totCutRetQty+=$cutRetQty;
			$total+=$tot;
			$tot_totalIn		  += $totalIn;
			$tot_totalOut		  += $totalOut;
			
			$graphicNo2 = $graphicNo;
			$dispatchNo2		=	$dispatchNo;
			$salesOrderNo2 = $salesOrderNo;
			$orderNo2 = $orderNo;
			$orderYear2 = $orderYear;
			$plant2 = $plant;
			
			$s_totalSampleQty += $sampleQty;
			$s_totalGoodQty	  += $goodQty;
			$s_embdQty		  += $embroideryQty;
			$s_pdQty		  += $pDammageQty;
			$s_fdQty		  += $fDammageQty;
			$s_cutRetQty	  += $cutRetQty;
			$s_total 		  += $tot;
			$s_totalIn		  += $totalIn;
			$s_totalOut		  += $totalOut;
			
			}
	  ?>
              <tr class="normalfnt"  bgcolor="#CCCCCC">
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfntRight" >&nbsp;</td>
                <td class="normalfntRight" ><b><?php echo number_format($totsampleQty) ?></b></td>
                <td class="normalfntRight" ><b><?php echo number_format($totgoodQty) ?></b></td>
                <td class="normalfntRight" ><b><?php echo number_format($totembroideryQty) ?></b></td>
                <td class="normalfntRight" ><b><?php echo number_format($totpDammageQty) ?></b></td>
                <td class="normalfntRight" ><b><?php echo number_format($totfDammageQty) ?></b></td>
                <td class="normalfntRight" ><b><?php echo number_format($totCutRetQty) ?></b></td>
                <td class="normalfntRight" ><b><?php echo number_format($total) ?></b></td>
                </tr>
              </tbody>
          </table>        </td>
        </tr>
      
      </table>
    </td>
</tr>

<tr height="40">
  <td colspan="2" align="center" class="normalfnt" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="66%"><table class="bordered"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <th colspan="11">Total In - Total Out</th>
        </tr>
        <tr>
          <th width="81">Brand</th>
          <th width="75">Order No</th>
          <th width="66">Sales Order </th>
          <th width="111">Graphic No</th>
          <th width="85">Style No</th>
          <th width="74">Part</th>
          <th width="69">In Qty</th>
          <th width="90">Out Qty</th>
          <th width="64">Total In Qty</th>
          <th width="71">Total Out Qty</th>
        </tr>
        <?PHP
		$STR_GRAHPICNO_LIST2 = $STR_GRAHPICNO_LIST2."'";
		$STR_STYLENO_LIST = $STR_STYLENO_LIST."'";
		$sql2 = "SELECT
				mst_brand.strName AS brandName,
				concat(OD1.intOrderYear,'-',OD1.intOrderNo) AS orderNo,
				OD1.strSalesOrderNo,
				SI1.strGraphicRefNo,
				OD1.strStyleNo,
				mst_part.strName AS partName,
				Sum(TRANS1.dblQty) AS totalIn,
				(
				
					SELECT
				Sum(TRANS2.dblQty *-1)
				FROM
				ware_stocktransactions_fabric AS TRANS2
				INNER JOIN trn_orderheader AS OH2 ON TRANS2.intOrderNo = OH2.intOrderNo AND TRANS2.intOrderYear = OH2.intOrderYear
				INNER JOIN trn_orderdetails AS OD2 ON OH2.intOrderNo = OD2.intOrderNo AND OH2.intOrderYear = OD2.intOrderYear AND TRANS2.intSalesOrderId = OD2.intSalesOrderId
				INNER JOIN mst_locations AS L2 ON L2.intId = TRANS2.intLocationId
				INNER JOIN mst_plant as PL2 ON PL2.intPlantId = L2.intPlant
				INNER JOIN trn_sampleinfomations AS SI2 ON SI2.intSampleNo = OD2.intSampleNo AND SI2.intSampleYear = OD2.intSampleYear AND SI2.intRevisionNo = OD2.intRevisionNo
				INNER JOIN mst_brand as B2 ON  B2.intId = SI2.intBrand
				INNER JOIN mst_part as PR2 ON PR2.intId = OD2.intPart
				WHERE
				SI2.strGraphicRefNo = SI1.strGraphicRefNo AND
				B2.intId 			 = mst_brand.intId AND
				OD2.strStyleNo	= OD1.strStyleNo AND
				OH2.intOrderNo  = OH1.intOrderNo AND 
				OH2.intOrderYear = OH1.intOrderYear AND
				OD2.strSalesOrderNo = OD1.strSalesOrderNo AND
				PR2.intId = mst_part.intId AND
				 FIND_IN_SET(TRANS2.strType,'Dispatched_G,Dispatched_S,Dispatched_E,Dispatched_P,Dispatched_F')
				
				) AS totalOut,
				(
				
					SELECT
				Sum(TRANS2.dblQty)
				FROM
				ware_stocktransactions_fabric AS TRANS2
				INNER JOIN trn_orderheader AS OH2 ON TRANS2.intOrderNo = OH2.intOrderNo AND TRANS2.intOrderYear = OH2.intOrderYear
				INNER JOIN trn_orderdetails AS OD2 ON OH2.intOrderNo = OD2.intOrderNo AND OH2.intOrderYear = OD2.intOrderYear AND TRANS2.intSalesOrderId = OD2.intSalesOrderId
				INNER JOIN mst_locations AS L2 ON L2.intId = TRANS2.intLocationId
				INNER JOIN mst_plant as PL2 ON PL2.intPlantId = L2.intPlant
				INNER JOIN trn_sampleinfomations AS SI2 ON SI2.intSampleNo = OD2.intSampleNo AND SI2.intSampleYear = OD2.intSampleYear AND SI2.intRevisionNo = OD2.intRevisionNo
				INNER JOIN mst_brand as B2 ON  B2.intId = SI2.intBrand
				INNER JOIN mst_part as PR2 ON PR2.intId = OD2.intPart
				WHERE
				trim(SI2.strGraphicRefNo) = trim(SI1.strGraphicRefNo) AND
				B2.intId 			 = mst_brand.intId AND
				trim(OD2.strStyleNo)	= trim(OD1.strStyleNo) AND
				OH2.intOrderNo  = OH1.intOrderNo AND 
				OH2.intOrderYear = OH1.intOrderYear AND
				OD2.strSalesOrderNo = OD1.strSalesOrderNo AND
				PR2.intId = mst_part.intId AND
				TRANS2.strType ='Received' AND
				TRANS2.dtDate < CONCAT(DATE(NOW()),' ','05:00:00')  AND  
				TRANS2.dtDate >= CONCAT(DATE(DATE_ADD(NOW(),INTERVAL -1 DAY )),' ','05:00:00')
				
				) AS dayIn,
				(
				
					SELECT
				Sum(TRANS2.dblQty *-1)
				FROM
				ware_stocktransactions_fabric AS TRANS2
				INNER JOIN trn_orderheader AS OH2 ON TRANS2.intOrderNo = OH2.intOrderNo AND TRANS2.intOrderYear = OH2.intOrderYear
				INNER JOIN trn_orderdetails AS OD2 ON OH2.intOrderNo = OD2.intOrderNo AND OH2.intOrderYear = OD2.intOrderYear AND TRANS2.intSalesOrderId = OD2.intSalesOrderId
				INNER JOIN mst_locations AS L2 ON L2.intId = TRANS2.intLocationId
				INNER JOIN mst_plant as PL2 ON PL2.intPlantId = L2.intPlant
				INNER JOIN trn_sampleinfomations AS SI2 ON SI2.intSampleNo = OD2.intSampleNo AND SI2.intSampleYear = OD2.intSampleYear AND SI2.intRevisionNo = OD2.intRevisionNo
				INNER JOIN mst_brand as B2 ON  B2.intId = SI2.intBrand
				INNER JOIN mst_part as PR2 ON PR2.intId = OD2.intPart
				WHERE
				trim(SI2.strGraphicRefNo) = trim(SI1.strGraphicRefNo) AND
				B2.intId 			 = mst_brand.intId AND
				trim(OD2.strStyleNo)	= trim(OD1.strStyleNo) AND
				OH2.intOrderNo  = OH1.intOrderNo AND 
				OH2.intOrderYear = OH1.intOrderYear AND
				OD2.strSalesOrderNo = OD1.strSalesOrderNo AND
				PR2.intId = mst_part.intId AND
				 FIND_IN_SET(TRANS2.strType,'Dispatched_G,Dispatched_S,Dispatched_E,Dispatched_P,Dispatched_F') AND
				TRANS2.dtDate < CONCAT(DATE(NOW()),' ','05:00:00')  AND  
				TRANS2.dtDate >= CONCAT(DATE(DATE_ADD(NOW(),INTERVAL -1 DAY )),' ','05:00:00')
				
				) AS dayOut
				FROM
				ware_stocktransactions_fabric AS TRANS1
				INNER JOIN trn_orderheader AS OH1 ON TRANS1.intOrderNo = OH1.intOrderNo AND TRANS1.intOrderYear = OH1.intOrderYear
				INNER JOIN trn_orderdetails AS OD1 ON OH1.intOrderNo = OD1.intOrderNo AND OH1.intOrderYear = OD1.intOrderYear AND TRANS1.intSalesOrderId = OD1.intSalesOrderId
				INNER JOIN mst_locations ON mst_locations.intId = TRANS1.intLocationId
				INNER JOIN mst_plant ON mst_plant.intPlantId = mst_locations.intPlant
				INNER JOIN trn_sampleinfomations AS SI1 ON SI1.intSampleNo = OD1.intSampleNo AND SI1.intSampleYear = OD1.intSampleYear AND SI1.intRevisionNo = OD1.intRevisionNo
				INNER JOIN mst_brand ON mst_brand.intId = SI1.intBrand
				INNER JOIN mst_part ON mst_part.intId = OD1.intPart
				WHERE
				 	 FIND_IN_SET(trim(SI1.strGraphicRefNo),$STR_GRAHPICNO_LIST2) AND
					 FIND_IN_SET (trim(OD1.strStyleNo),$STR_STYLENO_LIST) AND
					TRANS1.strType = 'Received' and mst_plant.MAIN_CLUSTER_ID=2
				GROUP BY
					mst_brand.strName,
					SI1.strGraphicRefNo,
					OD1.strStyleNo,
					OD1.strSalesOrderNo,
					mst_part.strName,
					OD1.intOrderNo,OD1.intOrderYear
				order by
					OD1.strGraphicNo,mst_part.strName
";
			$result2 = $db->RunQuery($sql2);
			$sum_dayIn=0;
			$sum_dayOut=0;
			$sum_totalIn=0;
			$sum_totalOut=0;
			while($row2=mysqli_fetch_array($result2))
			{
		?>
        <tr>
          <td><?PHP echo $row2['brandName']; ?></td>
          <td><?PHP echo $row2['orderNo']; ?></td>
          <td><?PHP echo $row2['strSalesOrderNo']; ?></td>
          <td><?PHP echo $row2['strGraphicRefNo']; ?></td>
          <td><?PHP echo $row2['strStyleNo']; ?></td>
          <td><?PHP echo $row2['partName']; ?></td>
          <td class="normalfntRight"><?PHP echo number_format( $row2['dayIn']);$sum_dayIn+=$row2['dayIn']  ?></td>
          <td class="normalfntRight"><?PHP echo number_format( $row2['dayOut']);$sum_dayOut+=$row2['dayOut']  ?></td>
          <td class="normalfntRight"><?PHP echo number_format( $row2['totalIn']);$sum_totalIn+=$row2['totalIn']  ?></td>
          <td class="normalfntRight"><?PHP echo number_format($row2['totalOut']);$sum_totalOut+=$row2['totalOut'] ?></td>
        </tr>
        <?php
			}
		?>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td class="normalfntRight"><b><?PHP echo number_format( $sum_dayIn);?></b></td>
          <td class="normalfntRight"><b><?PHP echo number_format( $sum_dayOut); ?></b></td>
          <td class="normalfntRight"><b><?php echo number_format($sum_totalIn); ?></b></td>
          <td class="normalfntRight"><b><?php echo number_format($sum_totalOut); ?></b></td>
        </tr>
      </table></td>
      <td width="34%">&nbsp;</td>
    </tr>
  </table></td>
  </tr>
<tr height="40">
  <td width="317" align="center" class="normalfnt" ><?php
$plantHeadName = "";
$array_in1 = array_map("unserialize", array_unique(array_map("serialize", $array_in)));
echo "<span style=\"font-weight:bold;text-decoration:underline\">Contact Details</span></br>";
for($loop=0;$loop<count($array_in);$loop++)
{
	if($array_in1[$loop]!="")
		echo $array_in1[$loop].'</br>';
}
?></td>
  <td width="873" align="center" class="normalfntRight"><span class="normalfntMid">Printed Date:<strong> <?php echo date("Y/m/d  H:i:s") ?></strong></span></td>
</tr>

<tr height="40">
  <td width="317" align="center" class="normalfnt" >&nbsp;</td>
  <td width="873" align="center" class="normalfntRight">&nbsp;</td>
</tr>
</table>
</div>        
</form>
 
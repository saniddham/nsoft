<?php
session_start();

date_default_timezone_set('Asia/Colombo');
$thisFilePath 	=  $_SERVER['PHP_SELF'];
$cronjobId 		=  60;

		$result_select = getCustomerOrders();
		while($row = mysqli_fetch_array($result_select))
		{
 		$order_no  		= $row['ORDER_NO'];
		$order_year  	= $row['ORDER_YEAR'];
		$sales_order_list  	= $row['intSalesOrderId'];
		$customer_po 	= $row['strCustomerPoNo'];
		$mail_BCC  		= $row['bccEmail'];
		$mail_CC  		= $row['ccEmail'];
		$mail_TO  		= $row['toEmail'];
		$customer  		= $row['strName'];
		
		////ob_start();
		$body		= '';
		$body_header='';
		include "rptOrderheaderDispatch_report.php";
		echo $body  = $body_header.$body;
		//echo $body = ob_get_clean();

  	
		//Original

	    //if($haveRecords)
  		//sendMessage($FROM_EMAIL,$FROM_NAME,$toEmails,$mailHeader,$body,$cc,$bcc);
		
		$FROM_NAME			= "NSOFT-SCREENLINE HOLDINGS";
		$FROM_EMAIL			= "";
		 
		$nowDate 			= date('Y-m-d');
		$mailHeader			= "	ORDER BASED - DISPATCH SUMMARY ($nowDate) - $customer - $customer_po ";
		
		//$mail_CC			.=',jayawardanasashika@yahoo.com,suwini@nimawum.com';
		insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);

		}


	function getCustomerOrders()
	{
		global $db ;
		
		$sql = 
		"SELECT
		ware_stocktransactions_fabric.intOrderNo as ORDER_NO,
		ware_stocktransactions_fabric.intOrderYear as ORDER_YEAR,
		GROUP_CONCAT( DISTINCT ware_stocktransactions_fabric.intSalesOrderId) AS intSalesOrderId,
		GROUP_CONCAT( DISTINCT trn_orderheader_dispatch_emails.strBCC_EMAIL) AS bccEmail,
		GROUP_CONCAT( DISTINCT trn_orderheader_dispatch_emails.strCC_EMAIL) AS ccEmail,
		GROUP_CONCAT( DISTINCT trn_orderheader_dispatch_emails.strTO_EMAIL) AS toEmail,
		mst_customer.strName,
		trn_orderheader.strCustomerPoNo
		
		FROM
		ware_stocktransactions_fabric
		left JOIN
		trn_orderheader_dispatch_emails ON
		trn_orderheader_dispatch_emails.ORDER_NO = ware_stocktransactions_fabric.intOrderNo AND
		trn_orderheader_dispatch_emails.ORDER_YEAR = ware_stocktransactions_fabric.intOrderYear
		INNER JOIN 
		trn_orderheader ON trn_orderheader.intOrderNo = ware_stocktransactions_fabric.intOrderNo AND
		trn_orderheader.intOrderYear = ware_stocktransactions_fabric.intOrderYear
		INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
		WHERE
		DATE(ware_stocktransactions_fabric.dtDate) = DATE(NOW()) - 1 AND
		ware_stocktransactions_fabric.strType  LIKE  '%Dispatch%'  
		GROUP BY
		ware_stocktransactions_fabric.intOrderNo, 
		ware_stocktransactions_fabric.intOrderYear";
		//echo $sql;
		$result = $db->RunQuery($sql);
		return $result;
		
	}

?>

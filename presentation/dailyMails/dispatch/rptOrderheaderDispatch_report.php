<?php
// ini_set('display_errors',1);
$haveRecords	='';
$body_header	='';
$body			='';

ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Wise Order Report</title>


<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}


 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}


.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}



.odd{background-color:#F5F5F5;}
.even{background-color:#FFFFFF;}
.mouseover {
	cursor: pointer;
}


.txtNumber{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}
</style>
</head>
<body><form id="frmCusorderReport" name="frmCusorderReport" method="post" action="rptFabricDispatchNote.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
    <td colspan="9" align="center" ><table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="tophead"><table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td width="12%">&nbsp;</td>
            <td width="17%" class="normalfnt"><img style="vertical-align:" src="http://accsee.com/screenline.jpg"  /></td>
           
            <td width="16%" class="tophead"><div align="left"><img style="vertical-align:" src="http://accsee.com/nsoft_logo.png"  /></div></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ></div>
<table align="center" width="747"><tr><td width="119">&nbsp;</td><td width="616" align="center">CUSTOMER  ORDER WISE REPORT- <?php echo "$customer"."&nbsp;&nbsp;"."-"."&nbsp;&nbsp;"."$customer_po"."&nbsp;&nbsp;" ?> </td></tr></table>

<table width="1048" align="center">
<tr>

<td width="1040"><table bgcolor="#999999"  class="bordered" width="109%" border="0"   id="" cellspacing="1" cellpadding="2" >
  <thead>
    <tr class="">
      <th colspan="25" bgcolor="#C5D5FE" >Customer Dispatch Summary </th>
      </tr>
    <tr class="normalfntMid">
      <th rowspan="2" width="7%" height="31" bgcolor="#C5D5FE" >Plant</th>
      <th rowspan="2" width="8%" bgcolor="#C5D5FE" >Customer</th>
      <th rowspan="2" width="8%" bgcolor="#C5D5FE" >Brand</th>
      <th rowspan="2" width="8%" bgcolor="#C5D5FE" >Order No</th>
      <th rowspan="2" width="7%" bgcolor="#C5D5FE" >Sales Order No</th>
      <th rowspan="2" width="9%" bgcolor="#C5D5FE" >Graphic No</th>
      <th rowspan="2" width="11%" bgcolor="#C5D5FE" >Style No</th>
      <th rowspan="2" width="13%" bgcolor="#C5D5FE" >Customer PO</th>
      <th rowspan="2" width="5%" bgcolor="#C5D5FE" >Size</th>
      <th rowspan="1" colspan="8"  bgcolor="#C5D5FE" >Dispatched on - <?php echo  date('Y/m/d',strtotime("-1 days")) ?></th>
      <th rowspan="1" colspan="8"  bgcolor="#C5D5FE" >Cumulative figures </th>
    </tr>
    <tr>
      <th rowspan="1" width="6%" bgcolor="#C5D5FE" >Sample Qty</th>
      <th rowspan="1" width="4%" bgcolor="#C5D5FE" >Good Qty</th>
      <th rowspan="1" width="4%" bgcolor="#C5D5FE" >EMD Qty</th>
      <th rowspan="1" width="3%" bgcolor="#C5D5FE" >P-D Qty</th>
      <th rowspan="1" width="4%" bgcolor="#C5D5FE" >F-D Qty</th>
      <th rowspan="1" width="4%" bgcolor="#C5D5FE" >Cut Ret</th>
      <th rowspan="1" width="4%" bgcolor="#C5D5FE" >Total Qty</th>
      <th rowspan="1" width="3%" bgcolor="#C5D5FE" >PD %</th>
      <th rowspan="1" width="6%" bgcolor="#C5D5FE" >Sample Qty</th>
      <th rowspan="1" width="4%" bgcolor="#C5D5FE" >Good Qty</th>
      <th rowspan="1" width="4%" bgcolor="#C5D5FE" >EMD Qty</th>
      <th rowspan="1" width="3%" bgcolor="#C5D5FE" >P-D Qty</th>
      <th rowspan="1" width="4%" bgcolor="#C5D5FE" >F-D Qty</th>
      <th rowspan="1" width="4%" bgcolor="#C5D5FE" >Cut Ret</th>
      <th rowspan="1" width="4%" bgcolor="#C5D5FE" >Total Qty</th>
      <th rowspan="1" width="3%" bgcolor="#C5D5FE" >PD %</th>
    </tr>
  </thead>
  <tbody>
<?php
$curruntTime=date("Y-m-d H:i:s");
$curruntTimeFrom = date("Y-m-d H:i:s",strtotime(date('Y-m-j H:i:s')) - (24 * 60 * 60));//correct
  $sql1 = "SELECT DISTINCT
	ware_fabricdispatchheader_approvedby.dtApprovedDate,
	trn_orderdetails.strStyleNo,
	trn_orderdetails.strGraphicNo,
	trn_orderheader.strCustomerPoNo,
	ware_fabricdispatchheader.intOrderNo,
	ware_fabricdispatchheader.intOrderYear,
	trn_orderdetails.strSalesOrderNo,
	ware_fabricdispatchdetails.strSize,

sum(if(date(ware_fabricdispatchheader_approvedby.dtApprovedDate)= DATE(DATE_ADD(NOW(), INTERVAL - 1 DAY)),ifnull(ware_fabricdispatchdetails.dblSampleQty,0),0)) as today_sample,
sum(ifnull(ware_fabricdispatchdetails.dblSampleQty,0)) as tot_sample,
sum(if(date(ware_fabricdispatchheader_approvedby.dtApprovedDate)= DATE(DATE_ADD(NOW(), INTERVAL - 1 DAY)),ifnull(ware_fabricdispatchdetails.dblGoodQty,0),0)) as today_good,
sum(ifnull(ware_fabricdispatchdetails.dblGoodQty,0)) as tot_good,
sum(if(date(ware_fabricdispatchheader_approvedby.dtApprovedDate)= DATE(DATE_ADD(NOW(), INTERVAL - 1 DAY)),ifnull(ware_fabricdispatchdetails.dblEmbroideryQty,0),0)) as today_Embroidery,
sum(ifnull(ware_fabricdispatchdetails.dblEmbroideryQty,0)) as tot_Embroidery,
sum(if(date(ware_fabricdispatchheader_approvedby.dtApprovedDate)= DATE(DATE_ADD(NOW(), INTERVAL - 1 DAY)),ifnull(ware_fabricdispatchdetails.dblPDammageQty,0),0)) as today_PDammage,
sum(ifnull(ware_fabricdispatchdetails.dblPDammageQty,0)) as tot_PDammage,
sum(if(date(ware_fabricdispatchheader_approvedby.dtApprovedDate)= DATE(DATE_ADD(NOW(), INTERVAL - 1 DAY)),ifnull(ware_fabricdispatchdetails.dblFdammageQty,0),0)) as today_FDammage,
sum(ifnull(ware_fabricdispatchdetails.dblFdammageQty,0)) as tot_FDammage,
sum(if(date(ware_fabricdispatchheader_approvedby.dtApprovedDate)= DATE(DATE_ADD(NOW(), INTERVAL - 1 DAY)),ifnull(ware_fabricdispatchdetails.dblCutRetQty,0),0)) as today_CutRet,
sum(ifnull(ware_fabricdispatchdetails.dblCutRetQty,0)) as tot_CutRet,
mst_brand.strName AS brandName,
mst_customer.strName AS customerName,
trn_orderdetails.dblDamagePercentage,
	(
		SELECT
			GROUP_CONCAT(DISTINCT SUB_P.strPlantName)
		FROM
			ware_fabricdispatchheader SUB_FDH
		INNER JOIN mst_locations SUB_LO ON SUB_LO.intId = SUB_FDH.intCompanyId
		INNER JOIN mst_plant SUB_P ON SUB_P.intPlantId = SUB_LO.intPlant
		WHERE
			SUB_FDH.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo
		AND SUB_FDH.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
	) AS PLANT
FROM 
trn_orderdetails
INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo
AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear 
INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo
AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear
INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo
AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear
AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
INNER JOIN mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
INNER JOIN mst_colors_ground ON ware_fabricdispatchdetails.intGroundColor = mst_colors_ground.intId
INNER JOIN ware_fabricdispatchheader_approvedby ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchheader_approvedby.intBulkDispatchNo
AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchheader_approvedby.intYear
AND ware_fabricdispatchheader_approvedby.intApproveLevelNo = ware_fabricdispatchheader.intApproveLevels 
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo
AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear
AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo
INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
INNER JOIN mst_locations ON ware_fabricdispatchheader.intCompanyId = mst_locations.intId
INNER JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId
WHERE
	/*ware_fabricdispatchheader_approvedby.dtApprovedDate < CONCAT(DATE(NOW()), ' ', '05:00:00')
AND ware_fabricdispatchheader_approvedby.dtApprovedDate >= CONCAT(
	DATE(
		DATE_ADD(NOW(), INTERVAL - 1 DAY)
	),
	' ',
	'05:00:00'
) /* AND date(ware_fabricdispatchheader_approvedby.dtApprovedDate) < '2016-08-01' AND date(ware_fabricdispatchheader_approvedby.dtApprovedDate) > '2016-04-01' */
 
ware_fabricdispatchheader.intStatus 	= '1'
AND trn_orderdetails.intOrderNo 		= $order_no
AND trn_orderdetails.intOrderYear 		= $order_year
AND  FIND_IN_SET(trn_orderdetails.intSalesOrderId,'$sales_order_list')
GROUP BY
	ware_fabricdispatchheader.intOrderNo,
	ware_fabricdispatchheader.intOrderYear,
	trn_orderdetails.strGraphicNo,
	ware_fabricdispatchdetails.strSize
ORDER BY
	ware_fabricdispatchheader.intCompanyId ASC,
	ware_fabricdispatchheader.intOrderYear ASC,
	ware_fabricdispatchheader.intOrderNo ASC,
	trn_orderdetails.strSalesOrderNo ASC,
	trn_orderdetails.strStyleNo ASC,
	trn_orderheader.strCustomerPoNo ASC";
	//if($cluster==8)
	//echo $sql1;
			$result1 = $db->RunQuery($sql1);	
			$totsampleQty_today		= 0;
			$totgoodQty_today		= 0;
			$totembroideryQty_today	= 0;
			$totpDammageQty_today	= 0;
			$totfDammageQty_today	= 0;
			$totCutRetQty_today		= 0;
			$total_today			= 0;
			$totQty_today			= 0;
					
			$totsampleQty		= 0;
			$totgoodQty			= 0;
			$totembroideryQty	= 0;
			$totpDammageQty		= 0;
			$totfDammageQty		= 0;
			$totCutRetQty		= 0;
			$total				= 0;
			$totQty				= 0;
			$totAmmount			= 0;			
			$haveRecords 		= false;
			$i=0;
			while($row1=mysqli_fetch_array($result1))
			{
				
				$haveRecords 			= true;
				$customer 				= $row1['customerName'];
				$intBulkDispatchNo 		= $row1['intBulkDispatchNo'];
				$dblDamagePercentage 	= $row1['dblDamagePercentage'];
				$intBulkDispatchNoYear 	= $row1['intBulkDispatchNoYear'];
				$brandName 				= $row1['brandName'];				
				$style 					= $row1['strStyleNo']; 
				$graphicNo 				= $row1['strGraphicNo']; 
				$custPO 				= $row1['strCustomerPoNo'];
				$orderNo 				= $row1['intOrderNo'];
				$orderYear 				= $row1['intOrderYear'];				
				$cutNo					= $row1['strCutNo'];
				$salesOrderNo			= $row1['strSalesOrderNo'];
				$part					= $row1['part'];
				$bgColor				= $row1['bgcolor'];
				$lineNo					= $row1['strLineNo'];
				$size					= $row1['strSize'];
				
				$sampleQty				= $row1['tot_sample'];
				$goodQty				= $row1['tot_good'];
				$embroideryQty			= $row1['tot_Embroidery'];
				$pDammageQty			= $row1['tot_PDammage'];
				$fDammageQty			= $row1['tot_FDammage'];
				$cutRetQty				= $row1['tot_CutRet'];
				
				$sampleQty_today		= $row1['today_sample'];
				$goodQty_today			= $row1['today_good'];
				$embroideryQty_today	= $row1['today_Embroidery'];
				$pDammageQty_today		= $row1['today_PDammage'];
				$fDammageQty_today		= $row1['today_FDammage'];
				$cutRetQty_today		= $row1['today_CutRet'];
				$remarks				= $row1['strRemarks'];
				$plant					= $row1["PLANT"];
				$tot					= (float)($sampleQty)+(float)($goodQty)+(float)($embroideryQty)+(float)($pDammageQty)+(float)($fDammageQty)+ (float)$cutRetQty;
				$tot_today				=(float)($sampleQty_today)+(float)($goodQty_today)+(float)($embroideryQty_today)+(float)($pDammageQty_today)+(float)($fDammageQty_today)+ (float)$cutRetQty_today;
	  ?>
      
       <?php 
	if(($graphicNo != $graphicNo2 || $salesOrderNo != $salesOrderNo2 || $orderno !=$orderno2 || $orderYear != $orderYear2 || $plant2!=$plant) && $graphicNo!="")
		{
			
			if(++$i>1)
			{
			/*$graphicNo2 = '';
			$salesOrderNo2 = '' ;
			$orderNo2 = '';
			$orderyear2 = '';
			$plant2 = '';*/
			
	?>
    <tr>
      <td bgcolor="#FFFFFF">&nbsp;</td>
      <td bgcolor="#FFFFFF">&nbsp;</td>
      <td bgcolor="#FFFFFF">&nbsp;</td>
      <td bgcolor="#FFFFFF">&nbsp;</td>
      <td bgcolor="#FFFFFF">&nbsp;</td>
      <td bgcolor="#FFFFFF">&nbsp;</td>
      <td bgcolor="#FFFFFF">&nbsp;</td>
      <td bgcolor="#FFFFFF">&nbsp;</td>
      <td bgcolor="#FFFFFF">&nbsp;</td>
      <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $s_totalSampleQty_today; ?></b></td>
      <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $s_totalGoodQty_today; ?></b></td>
      <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $s_embdQty_today; ?></b></td>
      <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $s_pdQty_today; ?></b></td>
      <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $s_fdQty_today; ?></b></td>
      <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $s_cutRetQty_today; ?></b></td>
      <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $s_total_today; ?></b></td>
      <td bgcolor="#BADCDC" class="normalfntRight" style="color:<?php 
		if($s_pdQty_today/$s_total_today*100 >1)
			echo "red";
		?>"><b><?php echo number_format($s_pdQty_today/$s_total_today*100,2) ?>%</b></td>
        
      <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $s_totalSampleQty; ?></b></td>
      <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $s_totalGoodQty; ?></b></td>
      <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $s_embdQty; ?></b></td>
      <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $s_pdQty; ?></b></td>
      <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $s_fdQty; ?></b></td>
      <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $s_cutRetQty; ?></b></td>
      <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $s_total ; ?></b></td>
      <td bgcolor="#E5DFEC" class="normalfntRight" style="color:<?php 
		if($s_pdQty/$s_total*100 >1)
			echo "red";
		?>"><b><?php echo number_format($s_pdQty/$s_total*100,2) ?>%</b></td>
      <?php 
	  		$s_totalSampleQty_today 	= 0;
			$s_totalGoodQty_today	  	= 0;
			$s_embdQty_today		  	= 0;
			$s_pdQty_today		  		= 0;
			$s_fdQty_today		  		= 0;
			$s_cutRetQty_today	  		= 0;
			$s_total_today 		  		= 0; 
	  			
			$s_totalSampleQty = 0;
			$s_totalGoodQty	  = 0;
			$s_embdQty		  = 0;
			$s_pdQty		  = 0;
			$s_fdQty		  = 0;
			$s_cutRetQty	  = 0;
			$s_total 		  = 0; 
		?>
    </tr>
    <?php 
			}
		}
		
	?>
      
 
       <?php 
	if($plant != $plant2 )
		{
			if(++$i>1)
			{
			/*$graphicNo2 = '';
			$salesOrderNo2 = '' ;
			$orderNo2 = '';
			$orderyear2 = '';
			$plant2 = '';*/
			
	?>
    <tr bgcolor="#999999">
    	<td>&nbsp;</td>
    	<td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $p_totalSampleQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $p_totalGoodQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $p_embdQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $p_pdQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $p_fdQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $s_cutRetQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $p_total_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight" style="color:<?php 
		if($p_pdQty_today/$p_total_today*100 >1)
			echo "red";
		?>"><b><?php echo number_format($p_pdQty_today/$p_total_today*100,2) ?>%</b></td>
        
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $p_totalSampleQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $p_totalGoodQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $p_embdQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $p_pdQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $p_fdQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $s_cutRetQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $p_total ; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight" style="color:<?php 
		if($p_pdQty/$p_total*100 >1)
			echo "red";
		?>"><b><?php echo number_format($p_pdQty/$p_total*100,2) ?>%</b></td>
        <?php 
		
			$p_totalSampleQty_today = 0;
			$p_totalGoodQty_today	= 0;
			$p_embdQty_today		= 0;
			$p_pdQty_today		  	= 0;
			$p_fdQty_today		  	= 0;
			$p_cutRetQty_today	  	= 0;
			$p_total_today 		  	= 0; 
			
			$p_totalSampleQty = 0;
			$p_totalGoodQty	  = 0;
			$p_embdQty		  = 0;
			$p_pdQty		  = 0;
			$p_fdQty		  = 0;
			$p_cutRetQty	  = 0;
			$p_total 		  = 0; 
		?>
    </tr>
    <tr>
    	<td bgcolor="#FFFFFF">&nbsp;</td>
    	<td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#BADCDC"><b>&nbsp;</b></td>
        <td bgcolor="#BADCDC">&nbsp;</td>
        <td bgcolor="#BADCDC">&nbsp;</td>
        <td bgcolor="#BADCDC">&nbsp;</td>
        <td bgcolor="#BADCDC">&nbsp;</td>
        <td bgcolor="#BADCDC">&nbsp;</td>
        <td bgcolor="#BADCDC">&nbsp;</td>
        <td bgcolor="#BADCDC">&nbsp;</td>
        <td bgcolor="#E5DFEC"><b>&nbsp;</b></td>
        <td bgcolor="#E5DFEC">&nbsp;</td>
        <td bgcolor="#E5DFEC">&nbsp;</td>
        <td bgcolor="#E5DFEC">&nbsp;</td>
        <td bgcolor="#E5DFEC">&nbsp;</td>
        <td bgcolor="#E5DFEC">&nbsp;</td>
        <td bgcolor="#E5DFEC">&nbsp;</td>
        <td bgcolor="#E5DFEC">&nbsp;</td>
        
    </tr>
    <?php 
			}
		}
		
	?>
      
    <tr class="normalfnt"   bgcolor="#FFFFFF">
      <td align="center" class="normalfnt" ><?php echo $plant?></td>
      <td align="center" class="normalfnt" ><?php echo $customer?></td>
      <td align="center" class="normalfntMid" id="<?php echo $orderYear; ?>2" ><?php echo $brandName; ?></td>
      <td align="center" class="normalfntMid" id="<?php echo $orderNo; ?>2" ><?php echo $orderYear.'/'.$orderNo; ?></td>
      <td align="center" class="normalfntMid" id="<?php echo $salesOrderNo; ?>2" ><?php echo $salesOrderNo; ?></td>
      <td align="center" class="normalfntMid" id="<?php echo $graphicNo; ?>2" ><?php echo $graphicNo; ?></td>
      <td align="center" class="normalfntMid" id="<?php echo $style; ?>2" ><?php echo $style; ?></td>
      <td align="center" class="normalfntMid" id="<?php echo $custPO; ?>2" ><?php echo $custPO; ?></td>
      <td align="center" class="normalfntMid" id="<?php echo $qty; ?>2"><?php echo $size; ?></td>
      
      <td bgcolor="#BADCDC" align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $sampleQty_today; ?></td>
      <td bgcolor="#BADCDC" align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $goodQty_today; ?></td>
      <td bgcolor="#BADCDC" align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $embroideryQty_today; ?></td>
      <td bgcolor="#BADCDC" align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $pDammageQty_today; ?></td>
      <td bgcolor="#BADCDC" align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $fDammageQty_today; ?></td>
      <td bgcolor="#BADCDC" align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $cutRetQty_today; ?></td>
      <td bgcolor="#BADCDC" align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $tot_today; ?></td>
      <td bgcolor="#BADCDC" align="center" class="normalfntRight" style="color:<?php	if( 1 <$pDammageQty_today/$tot_today*100)
		echo "red";
		?>" id="<?php echo $qty; ?>2"><?php echo number_format($pDammageQty_today/$tot_today*100,2); ?>%</td>
      
      <td bgcolor="#E5DFEC" align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $sampleQty; ?></td>
      <td bgcolor="#E5DFEC" align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $goodQty; ?></td>
      <td bgcolor="#E5DFEC" align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $embroideryQty; ?></td>
      <td bgcolor="#E5DFEC" align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $pDammageQty; ?></td>
      <td bgcolor="#E5DFEC" align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $fDammageQty; ?></td>
      <td bgcolor="#E5DFEC" align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $cutRetQty; ?></td>
      <td bgcolor="#E5DFEC" align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $tot; ?></td>
      <td bgcolor="#E5DFEC" align="center" class="normalfntRight" style="color:<?php				
				/*
				Mr.Nishantha requested , if Production damage percentage(PD) is exceeded than 1% , it should be red mark.dont get PD % from order tables , becouse 1% is for this report only.
				*/
				if( 1 <$pDammageQty/$tot*100)
					echo "red";
					
				 ?>" id="<?php echo $qty; ?>2"><?php echo number_format($pDammageQty/$tot*100,2); ?>%</td>
    </tr>
   
  
<?php 
			$graphicNo2 = $graphicNo;
			$salesOrderNo2 = $salesOrderNo;
			$orderNo2 = $orderNo;
			$orderYear2 = $orderYear;
			$plant2 = $plant;
			
			$totsampleQty_today			+= $sampleQty_today;
			$totgoodQty_today			+= $goodQty_today;
			$totembroideryQty_today		+= $embroideryQty_today;
			$totpDammageQty_today		+= $pDammageQty_today;
			$totfDammageQty_today		+= $fDammageQty_today;
			$totCutRetQt_todayy			+= $cutRetQty_today;
			$total_today				+= $tot_today;
			
			$s_totalSampleQty_today 	+= $sampleQty_today;
			$s_totalGoodQty_today	  	+= $goodQty_today;
			$s_embdQty_today		  	+= $embroideryQty_today;
			$s_pdQty_today		  		+= $pDammageQty_today;
			$s_fdQty_today		  		+= $fDammageQty_today;
			$s_cutRetQty_today	  		+= $cutRetQty_today;
			$s_total_today 		  		+= $tot_today;
	
			$p_totalSampleQty_today 	+= $sampleQty_today;
			$p_totalGoodQty_today	  	+= $goodQty_today;
			$p_embdQty_today		  	+= $embroideryQty_today;
			$p_pdQty_today		  		+= $pDammageQty_today;
			$p_fdQty_today		  		+= $fDammageQty_today;
			$p_cutRetQty_today	  		+= $cutRetQty_today;
			$p_total_today 		  		+= $tot_today;
			
			
			$totsampleQty		+= $sampleQty;
			$totgoodQty			+= $goodQty;
			$totembroideryQty	+= $embroideryQty;
			$totpDammageQty		+= $pDammageQty;
			$totfDammageQty		+= $fDammageQty;
			$totCutRetQty		+= $cutRetQty;
			$total				+= $tot;
			
			$s_totalSampleQty += $sampleQty;
			$s_totalGoodQty	  += $goodQty;
			$s_embdQty		  += $embroideryQty;
			$s_pdQty		  += $pDammageQty;
			$s_fdQty		  += $fDammageQty;
			$s_cutRetQty	  += $cutRetQty;
			$s_total 		  += $tot;
	
			$p_totalSampleQty += $sampleQty;
			$p_totalGoodQty	  += $goodQty;
			$p_embdQty		  += $embroideryQty;
			$p_pdQty		  += $pDammageQty;
			$p_fdQty		  += $fDammageQty;
			$p_cutRetQty	  += $cutRetQty;
			$p_total 		  += $tot;
	
			}
			
?>
    <tr>
    	<td bgcolor="#FFFFFF">&nbsp;</td>
    	<td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $s_totalSampleQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $s_totalGoodQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $s_embdQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $s_pdQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $s_fdQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $s_cutRetQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $s_total_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight" style="color:<?php 
		if($s_pdQty_today/$s_total_today*100 >1)
			echo "red";
		?>"><b><?php echo number_format($s_pdQty_today/$s_total_today*100,2) ?>%</b></td>
        
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $s_totalSampleQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $s_totalGoodQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $s_embdQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $s_pdQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $s_fdQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $s_cutRetQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $s_total ; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight" style="color:<?php 
		if($s_pdQty/$s_total*100 >1)
			echo "red";
		?>"><b><?php echo number_format($s_pdQty/$s_total*100,2) ?>%</b></td>
        <?php 
		
			$s_totalSampleQty_today = 0;
			$s_totalGoodQty_today   = 0;
			$s_embdQty_today		= 0;
			$s_pdQty_today		  	= 0;
			$s_fdQty_today		  	= 0;
			$s_cutRetQty_today	  	= 0;
			$s_total_today 		  	= 0; 
			
			$s_totalSampleQty = 0;
			$s_totalGoodQty	  = 0;
			$s_embdQty		  = 0;
			$s_pdQty		  = 0;
			$s_fdQty		  = 0;
			$s_cutRetQty	  = 0;
			$s_total 		  = 0; 
		?>
    </tr>

    <tr bgcolor="#999999">
    	<td>&nbsp;</td>
    	<td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td  bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $p_totalSampleQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $p_totalGoodQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $p_embdQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $p_pdQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $p_fdQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $p_cutRetQty_today; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight"><b><?php echo $p_total_today ; ?></b></td>
        <td bgcolor="#BADCDC" class="normalfntRight" style="color:<?php 
		if($p_pdQty_today/$p_total_today*100 >1)
			echo "red";
		?>"><b><?php echo number_format($p_pdQty_today/$p_total_today*100,2) ?>%</b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $p_totalSampleQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $p_totalGoodQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $p_embdQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $p_pdQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $p_fdQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $p_cutRetQty; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight"><b><?php echo $p_total ; ?></b></td>
        <td bgcolor="#E5DFEC" class="normalfntRight" style="color:<?php 
		if($p_pdQty/$p_total*100 >1)
			echo "red";
		?>"><b><?php echo number_format($p_pdQty/$p_total*100,2) ?>%</b></td>
        <?php 
		
			$p_totalSampleQty_today = 0;
			$p_totalGoodQty_today	= 0;
			$p_embdQty_today		= 0;
			$p_pdQty_today		  	= 0;
			$p_fdQty_today		  	= 0;
			$p_cutRetQty_today	  	= 0;
			$p_total_today 		  	= 0; 
			
			$p_totalSampleQty = 0;
			$p_totalGoodQty	  = 0;
			$p_embdQty		  = 0;
			$p_pdQty		  = 0;
			$p_fdQty		  = 0;
			$p_cutRetQty	  = 0;
			$p_total 		  = 0; 
		?>
    </tr>


    <tr class="normalfnt"  bgcolor="#CCCCCC">
      <td bgcolor="#D5FFD5" class="normalfnt" >&nbsp;</td>
      <td bgcolor="#D5FFD5" class="normalfnt" >&nbsp;</td>
      <td bgcolor="#D5FFD5" class="normalfnt" >&nbsp;</td>
      <td bgcolor="#D5FFD5" class="normalfnt" >&nbsp;</td>
      <td bgcolor="#D5FFD5" class="normalfnt" >&nbsp;</td>
      <td bgcolor="#D5FFD5" class="normalfnt" >&nbsp;</td>
      <td bgcolor="#D5FFD5" class="normalfnt" >&nbsp;</td>
      <td bgcolor="#D5FFD5" class="normalfnt" >&nbsp;</td>
      <td bgcolor="#D5FFD5" class="normalfntRight" >&nbsp;</td>
      <td bgcolor="#BADCDC" class="normalfntRight" ><b><?php echo $totsampleQty_today ?></b></td>
      <td bgcolor="#BADCDC" class="normalfntRight" ><b><?php echo $totgoodQty_today ?></b></td>
      <td bgcolor="#BADCDC" class="normalfntRight" ><b><?php echo $totembroideryQty_today ?></b></td>
      <td bgcolor="#BADCDC" class="normalfntRight" ><b><?php echo $totpDammageQty_today ?></b></td>
      <td bgcolor="#BADCDC" class="normalfntRight" ><b><?php echo $totfDammageQty_today ?></b></td>
      <td bgcolor="#BADCDC" class="normalfntRight" ><b><?php echo $totCutRetQty_today ?></b></td>
      <td bgcolor="#BADCDC" class="normalfntRight" ><b><?php echo $total_today ?></b></td>
      <td bgcolor="#BADCDC" class="normalfntRight" ><b><?php echo number_format($totpDammageQty_today/$total_today*100,2) ?>%</b></td>
      <td bgcolor="#E5DFEC" class="normalfntRight" ><b><?php echo $totsampleQty ?></b></td>
      <td bgcolor="#E5DFEC" class="normalfntRight" ><b><?php echo $totgoodQty ?></b></td>
      <td bgcolor="#E5DFEC" class="normalfntRight" ><b><?php echo $totembroideryQty ?></b></td>
      <td bgcolor="#E5DFEC" class="normalfntRight" ><b><?php echo $totpDammageQty ?></b></td>
      <td bgcolor="#E5DFEC" class="normalfntRight" ><b><?php echo $totfDammageQty ?></b></td>
      <td bgcolor="#E5DFEC" class="normalfntRight" ><b><?php echo $totCutRetQty ?></b></td>
      <td bgcolor="#E5DFEC" class="normalfntRight" ><b><?php echo $total ?></b></td>
      <td bgcolor="#E5DFEC" class="normalfntRight" ><b><?php echo number_format($totpDammageQty/$total*100,2) ?>%</b></td>
    </tr>
  </tbody>
</table></td>
</tr>
</table>
<hr>
<?php
$body_header = ob_get_clean();
?>


 
<?php
session_start();
$thisFilePath 		= $_SERVER['PHP_SELF'];
ini_set('max_execution_time',300);

//BEGIN - SET PARAMETERS	{
$companyId 			= 1;
//END 	- SET PARAMETERS	}

//BEGIN - INCLUDE FILES {
include_once ($_SESSION['ROOT_PATH']."class/masterData/exchange_rate/cls_exchange_rate_get.php");
//END 	- INCLUDE FILES }

$objExchangeRateGet = new cls_exchange_rate_get($db);

$target_currencyId	= 1;

//BEGIN - 
$fromDate		= '2017-01-01';
$toDate			= '2017-12-31';

$startDate 		= strtotime($fromDate);
$endDate   		= strtotime($toDate);

$currentDate 	= $endDate;
$i		= 0;
$array	= array();
while ($currentDate >= $startDate) {
    $array[$i][0]	= date('Y-m',$currentDate);
	$array[$i][1]	= date('Y-M',$currentDate);
	$array[$i][2]	= date('Y-m',$currentDate)==date('Y-m')? '#E2DEF9':'';
    $currentDate 	= strtotime( date('Y/m/01/',$currentDate).' -1 month');
	$i++;
}
krsort($array);		
//END	-
ob_start();
?>
<style>
.break {
	page-break-before: always;
}
 @media print {
.noPrint {
	display: none;
}
}
#apDiv1 {
	position: absolute;
	left: 272px;
	top: 511px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
table .bordered {
 *border-collapse: collapse; /* IE7 and lower */
	border-spacing: 0;
	width: 100%;
}
.bordered {
	border: solid #ccc 1px;
	/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
	/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size: 11px;
	font-family: Verdana;
}
.bordered tr:hover {
	background: #fbf8e9;
	/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
	transition: all 0.1s ease-in-out;
}
.bordered td {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 2px;
}
.bordered th {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 4px;
	text-align: center;
}
.bordered th {
	background-color: #dce9f9;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
	background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -moz-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -ms-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -o-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: linear-gradient(top, #ebf3fc, #dce9f9);
	/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
	box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;
	border-top: none;
	text-shadow: 0 1px 0 rgba(255,255,255,.5);
}
.bordered td:first-child, .bordered th:first-child {
	border-left: none;
}
.bordered th:first-child {
	/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
	border-radius: 6px 0 0 0;
}
.bordered th:last-child {
	/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
	border-radius: 0 6px 0 0;
}
.bordered th:only-child {
	/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
	border-radius: 6px 6px 0 0;
}
.bordered tr:last-child td:first-child {
	/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
	border-radius: 0 0 0 6px;
}
.bordered tr:last-child td:last-child {
	/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
	border-radius: 0 0 6px 0;
}
.bordered {
	border: solid #ccc 1px;
	/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
	/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size: 11px;
	font-family: Verdana;
}
.bordered tr:hover {
	background: #fbf8e9;
	/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
	transition: all 0.1s ease-in-out;
}
.bordered td {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 2px;
}
.bordered th {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 4px;
	text-align: center;
}
.bordered th {
	background-color: #dce9f9;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
	background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -moz-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -ms-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -o-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: linear-gradient(top, #ebf3fc, #dce9f9);
	/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
	box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;
	border-top: none;
	text-shadow: 0 1px 0 rgba(255,255,255,.5);
}
.bordered td:first-child, .bordered th:first-child {
	border-left: none;
}
.bordered th:first-child {
	/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
	border-radius: 6px 0 0 0;
}
.bordered th:last-child {
	/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
	border-radius: 0 6px 0 0;
}
.bordered th:only-child {
	/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
	border-radius: 6px 6px 0 0;
}
.bordered tr:last-child td:first-child {
	/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
	border-radius: 0 0 0 6px;
}
.bordered tr:last-child td:last-child {
	/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
	border-radius: 0 0 6px 0;
}
.odd {
	background-color: #F5F5F5;
}
.even {
	background-color: #FFFFFF;
}
.mouseover {
	cursor: pointer;
}
.txtNumber {
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align: right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText {
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align: left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: right;
}
.reportHeader {
	font-size: 18px;
	/*text-align:center,*/
	font-style: normal;
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
}
</style>
<style type="text/css">
#progress {
	width: 100px;
	border: 1px solid black;
	position: relative;
	padding: 1px;
}
#percent {
	position: absolute;
	left: 50%;
}
#bar {
	height: 15px;
	background-color: #0C0;
}
</style>

<table width="1250" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td colspan="9" align="center" ><table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td class="tophead"><table width="100%" cellpadding="0" cellspacing="0">
              <tr>
                <td width="8%" class="normalfnt"><img style="vertical-align:" src="http://accsee.com/screenline.jpg"  /></td>
                <td align="center" valign="top" width="75%" class="topheadBLACK"><?php
	$result 			= $db->RunQuery($SQL);
	$result				= $obj_comm->loadLoctionComanyDetails(2,'RunQuery');
	$row 				= mysqli_fetch_array($result);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];	
	?>
                  <b><?php echo $companyName.""; ?></b>
                  <p class="normalfntMid"> <?php echo $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry."."."<br><b>Tel : </b>".$companyPhone." <b>Fax : </b>".$companyFax." <br><b>E-Mail : </b>".$companyEmail." <b>Web : </b>".$companyWeb;?></p></td>
                <td width="9%" class="tophead"><img src="http://accsee.com/nsoft_logo.png" style="vertical-align:"/></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td align="center" valign="top" class="reportHeader">Monthly Cash Flow Forecast [USD]</td>
                <td class="tophead">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td colspan="3"></td>
  </tr>
</table>
<table width="1100" border="0" align="center" bgcolor="#FFFFFF">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table  border="0" cellpadding="0" cellspacing="0" class="bordered" width="100%">
        <thead>
          <tr valign="bottom">
            <th width="24%">&nbsp;</th>
            <th width="9%">Opening Balance</th>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <th width="67%" nowrap="nowrap"><?php echo $val[1];?></th>
            <?php 
		}
		?>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td nowrap="nowrap" title="Customer Name" ><u><b>Add - Receipts</b></u></td>
            <td nowrap="nowrap" title="Customer Name" >&nbsp;</td>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <td nowrap="nowrap" bgcolor="<?php echo $val[2];?>">&nbsp;</td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" title="Customer Name" id="CUSTOMER">&nbsp;&nbsp;&nbsp;&nbsp;Monthly Collection From Customers</td>
            <?php
			$amount1			= round(GetCustomerOpeningAmount($fromDate),2);
			$amount2			= round(GetCustomerOpeningReceiptConfirmed($fromDate),2);
			$customerOpening	= $amount1-$amount2;
			?>
            <td rowspan="3" nowrap="nowrap" style="text-align:right;" ><?php echo $customerOpening==0?'-':number_format($customerOpening)?></td>
            <?php 
		foreach ($array as $key => $val)
		{
			$customerAmount			 = round(GetCustomerAmount($val[0]),2);
			$array_total[$val[1]][0] += $customerAmount;
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>" class="cls_td_amount" id="<?php echo $val[1]?>"><?php echo $customerAmount==0?'-':number_format($customerAmount)?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;Actual Monthly Collection From Customers</td>
            <?php 
		foreach ($array as $key => $val)
		{
			$customerAmount			 = round(GetCustomerActualCollection($val[0]),2);
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>" class="cls_td_amount" id="<?php echo $val[1]?>"><?php echo $customerAmount==0?'-':number_format($customerAmount)?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;Receipt Confirmation By Accountant</td>
            <?php 
		foreach ($array as $key => $val)
		{
			$customerAmount			 = round(GetCustomerReceiptConfirmed($val[0]),2);
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>" class="cls_td_amount" id="<?php echo $val[1]?>"><?php echo $customerAmount==0?'-':number_format($customerAmount)?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;Other Sundry Income</td>
            <td nowrap="nowrap" style="text-align:right">-</td>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>" class="cls_td_amount" id="<?php echo $val[1]?>">-</td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap"><u><b>Less - Payment</b></u></td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <td nowrap="nowrap" bgcolor="<?php echo $val[2];?>">&nbsp;</td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" id="SUPPLIER">&nbsp;&nbsp;&nbsp;&nbsp;Foreign Supplier Payments</td>
            <?php
			$amount1			= round(GetSupplierOpeningAmount($fromDate),2);
			$amount2			= round(GetSupplierOpeningVoucherConfirmed($fromDate),2);
			$supplierOpening	= $amount1-$amount2;
			?>
            <td rowspan="3" nowrap="nowrap" style="text-align:right;"><?php echo $supplierOpening==0?'-':number_format($supplierOpening)?></td>
            <?php
		foreach ($array as $key => $val)
		{
			$supplierAmount	= round(GetForeignSupplierAmount($val[0]));
			$array_total[$val[1]][1] += $supplierAmount;
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>" class="cls_td_amount" id="<?php echo $val[1]?>"><?php echo $supplierAmount=='0'?'-':number_format($supplierAmount)?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" id="SUPPLIER2">&nbsp;&nbsp;&nbsp;&nbsp;Local Supplier Payments</td>
            <?php 
		foreach ($array as $key => $val)
		{
			$supplierAmount	= round(GetLocalSupplierAmount($val[0]));
			$array_total[$val[1]][1] += $supplierAmount;
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>" class="cls_td_amount" id="<?php echo $val[1]?>"><?php echo $supplierAmount=='0'?'-':number_format($supplierAmount)?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;Voucher Confirmation By Accountant</td>
            <?php 
		foreach ($array as $key => $val)
		{
			$amount	= round(GetSupplierVoucherConfirmed($val[0]));
			$array_total[$val[1]][1] += $amount;
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>" class="cls_td_amount" id="<?php echo $val[1]?>4"><?php echo $amount=='0'?'-':number_format($amount)?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" id="SUPPLIER3">&nbsp;&nbsp;&nbsp;&nbsp;Staff Salaries &amp; EPF / ETF</td>
            <td nowrap="nowrap" id="SUPPLIER3">&nbsp;</td>
            <?php
		$preMonth	= 0; 
		foreach ($array as $key => $val)
		{
			$glId					  = '605,376,374';
			$amount					  = round(LedgerBalance($val[0],$glId,'JOURNAL_ENTRY'),2);	
			$lastTransDate			  = GetLastJournalDate($glId,'JOURNAL_ENTRY');		

			if($amount==0)
				$amount	= round(LedgerBalance($lastTransDate,$glId,'JOURNAL_ENTRY'),2);	
				
			$array_total[$val[1]][1] += $amount;
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>"><?php echo $amount=='0'?'-':number_format($amount)?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" id="SUPPLIER4">&nbsp;&nbsp;&nbsp;&nbsp;Management Salary</td>
            <td nowrap="nowrap" id="SUPPLIER4">&nbsp;</td>
            <?php
		$preMonth	= 0; 
		foreach ($array as $key => $val)
		{
			$glId					  = '545';
			$amount					  = round(LedgerBalance($val[0],$glId,'JOURNAL_ENTRY'),2);	
			$lastTransDate			  = GetLastJournalDate($glId,'JOURNAL_ENTRY');		

			if($amount==0)
				$amount	= round(LedgerBalance($lastTransDate,$glId,'JOURNAL_ENTRY'),2);			

			$array_total[$val[1]][1] += $amount;
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>"><?php echo $amount=='0'?'-':number_format($amount)?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td height="30" nowrap="nowrap" id="SUPPLIER5">&nbsp;&nbsp;&nbsp;&nbsp;Factory Salaries</td>
            <td nowrap="nowrap" id="SUPPLIER5">&nbsp;</td>
            <?php
		$preMonth	= 0; 
		foreach ($array as $key => $val)
		{
			$glId					  = '381';
			$amount					  = round(LedgerBalance($val[0],$glId,'JOURNAL_ENTRY'),2);	
			$lastTransDate			  = GetLastJournalDate($glId,'JOURNAL_ENTRY');		

			if($amount==0)
				$amount	= round(LedgerBalance($lastTransDate,$glId,'JOURNAL_ENTRY'),2);			

			$array_total[$val[1]][1] += $amount;
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>"><?php echo $amount=='0'?'-':number_format($amount)?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" id="SUPPLIER6">&nbsp;&nbsp;&nbsp;&nbsp;Factory - EPF/ETF</td>
            <td nowrap="nowrap" id="SUPPLIER6">&nbsp;</td>
            <?php
		$preMonth	= 0; 
		foreach ($array as $key => $val)
		{
			$glId					  = '376,374';
			$amount					  = round(LedgerBalance($val[0],$glId,'JOURNAL_ENTRY'),2);	
			$lastTransDate			  = GetLastJournalDate($glId,'JOURNAL_ENTRY');		

			if($amount==0)
				$amount	= round(LedgerBalance($lastTransDate,$glId,'JOURNAL_ENTRY'),2);			

			$array_total[$val[1]][1] += $amount;
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>"><?php echo $amount=='0'?'-':number_format($amount)?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" id="SUPPLIER7">&nbsp;&nbsp;&nbsp;&nbsp;EPF Arrears Payments</td>
            <td nowrap="nowrap" id="SUPPLIER7">&nbsp;</td>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>">-</td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" id="SUPPLIER8">&nbsp;&nbsp;&nbsp;&nbsp;Bonus Payments - Staff</td>
            <td nowrap="nowrap" id="SUPPLIER8">&nbsp;</td>
            <?php
		$preMonth	= 0; 
		foreach ($array as $key => $val)
		{
			$glId					  = '468';
			$amount					  = round(LedgerBalance($val[0],$glId,'JOURNAL_ENTRY'),2);	
			$lastTransDate			  = GetLastJournalDate($glId,'JOURNAL_ENTRY');		

			if($amount==0)
				$amount	= round(LedgerBalance($lastTransDate,$glId,'JOURNAL_ENTRY'),2);			

			$array_total[$val[1]][1] += $amount;
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>"><?php echo $amount=='0'?'-':number_format($amount)?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" id="SUPPLIER8">&nbsp;&nbsp;&nbsp;&nbsp;Bonus Payments - Workers</td>
            <td nowrap="nowrap" id="SUPPLIER8">&nbsp;</td>
            <?php
		$preMonth	= 0; 
		foreach ($array as $key => $val)
		{
			$glId					  = '427';
			$amount					  = round(LedgerBalance($val[0],$glId,'JOURNAL_ENTRY'),2);	
			$lastTransDate			  = GetLastJournalDate($glId,'JOURNAL_ENTRY');		

			if($amount==0)
				$amount	= round(LedgerBalance($lastTransDate,$glId,'JOURNAL_ENTRY'),2);			

			$array_total[$val[1]][1] += $amount;
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>"><?php echo $amount=='0'?'-':number_format($amount)?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" id="SUPPLIER9">&nbsp;&nbsp;&nbsp;&nbsp;Gratuity Payments</td>
            <td nowrap="nowrap" id="SUPPLIER9">&nbsp;</td>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>">-</td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" id="SUPPLIER10">&nbsp;&nbsp;&nbsp;&nbsp;Taxes Payments</td>
            <td nowrap="nowrap" id="SUPPLIER10">&nbsp;</td>
            <?php
		$preMonth	= 0; 
		foreach ($array as $key => $val)
		{
			$glId					  = '539,384,386,380';
			$amount					  = round(LedgerBalance($val[0],$glId,'JOURNAL_ENTRY'),2);	
			$lastTransDate			  = GetLastJournalDate($glId,'JOURNAL_ENTRY');		

			if($amount==0)
				$amount	= round(LedgerBalance($lastTransDate,$glId,'JOURNAL_ENTRY'),2);			

			$array_total[$val[1]][1] += $amount;
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>"><?php echo $amount=='0'?'-':number_format($amount)?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" id="SUPPLIER11">&nbsp;&nbsp;&nbsp;&nbsp;Other OH Payments</td>
            <td nowrap="nowrap" id="SUPPLIER11">&nbsp;</td>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>">-</td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>">&nbsp;</td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap"><b><u><b>Less -</b> Loan Schedule</u></b></td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>">&nbsp;</td>
            <?php
		}
		  ?>
          </tr>
          <?php
		  $result = GetLoanSchedule();
		  while($row = mysqli_fetch_array($result))
		  {
		  ?>
          <tr>
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row["REMARKS"]?> - Unsettle</td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php 
		foreach ($array as $key => $val)
		{
			$amount = GetLoanScheduleUnsettledAmount($val[0],$row["SCHEDULE_NO"],$row["SCHEDULE_YEAR"]);
			$sourceExArray			= $objExchangeRateGet->GetAllValues($row["CURRENCY_ID"],2,$row["SCHEDULE_DATE"],$companyId,'RunQuery');
			$targetExArray			= $objExchangeRateGet->GetAllValues($target_currencyId,2,$row["SCHEDULE_DATE"],$companyId,'RunQuery');
			$amount					= round(($amount/$targetExArray['AVERAGE_RATE'])*$sourceExArray['AVERAGE_RATE'],2);
			$array_total[$val[1]][1] += $amount;
			
			$font = '';
			if($amount!=0)
			{
				if(IsDueLoanPayment($val[0],$row["SCHEDULE_NO"],$row["SCHEDULE_YEAR"]))
					$font = 'red';
				else
					$font = '';
			}
		?>
            <td nowrap="nowrap" style="text-align:right;color:<?php echo $font?>" bgcolor="<?php echo $val[2];?>"><?php echo $amount=='0'?'-':number_format($amount)?></td>
            <?php 
		}
		?>
          </tr>
          <tr style="display:none">
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row["REMARKS"]?> - Settled</td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php 
		foreach ($array as $key => $val)
		{
			$amount = GetLoanScheduleSettledAmount($val[0],$row["SCHEDULE_NO"],$row["SCHEDULE_YEAR"]);
			$sourceExArray			= $objExchangeRateGet->GetAllValues($row["CURRENCY_ID"],2,$row["SCHEDULE_DATE"],$companyId,'RunQuery');
			$targetExArray			= $objExchangeRateGet->GetAllValues($target_currencyId,2,$row["SCHEDULE_DATE"],$companyId,'RunQuery');
			$amount					= round(($amount/$targetExArray['AVERAGE_RATE'])*$sourceExArray['AVERAGE_RATE'],2);
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>"><?php echo $amount=='0'?'-':number_format($amount)?></td>
            <?php 
		}
		?>
          </tr>
          <?php
		  }
		  ?>
          <tr>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php 
			foreach ($array as $key => $val)
			{
			?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>">&nbsp;</td>
            <?php
			}
			?>
          </tr>
          <tr>
            <td nowrap="nowrap"><b><u><b>Less -</b> Current Commitments</u></b></td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php 
			foreach ($array as $key => $val)
			{
			?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>">&nbsp;</td>
            <?php
			}
			?>
          </tr>
          <?php $result = LoadCommitments();
		  while($row = mysqli_fetch_array($result))
		  {
		?>
          <tr>
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row["CHART_OF_ACCOUNT_NAME"]?></td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php 
			foreach ($array as $key => $val)
			{
				$glId					  = $row["CHART_OF_ACCOUNT_ID"];
				$amount					  = round(LedgerBalance($val[0],$glId,'JOURNAL_ENTRY'),2);	
				$lastTransDate			  = GetLastJournalDate($glId,'JOURNAL_ENTRY');		
	
				if($amount==0)
					$amount	= round(LedgerBalance($lastTransDate,$glId,'JOURNAL_ENTRY'),2);	
					
				$array_total[$val[1]][1] += $amount;
			?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>"><?php echo $amount=='0'?'-':number_format($amount)?></td>
            <?php
			}
			?>
          </tr>
          <?php 
		  }
		  ?>
          <tr>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php foreach ($array as $key => $val)
			{?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>">&nbsp;</td>
            <?php
			}
			?>
          </tr>
          <tr>
            <td nowrap="nowrap"><b><u><b>Less -</b> Depreciation </u></b></td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php foreach ($array as $key => $val)
			{?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>">&nbsp;</td>
            <?php
			}
			?>
          </tr>
          <tr>
            <?php
		  		$glId 					  = getDepriciateGL();
								
		  ?>
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;Depreciate Amount</td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php 
			foreach ($array as $key => $val)
			{
				$amount					  = round(LedgerBalance($val[0],$glId,'FIXED_ASSET_DEP'),2);	
				$lastTransDate			  = GetLastJournalDate($glId,'FIXED_ASSET_DEP');		
	
				if($amount==0)
					$amount	= round(LedgerBalance($lastTransDate,$glId,'FIXED_ASSET_DEP'),2);	
					
				$array_total[$val[1]][1] += $amount;
			
				?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>"><?php echo $amount=='0'?'-':number_format($amount)?></td>
            <?php
			}
			?>
          </tr>
          <tr>
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;Depreciate Bank Transfer</td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php 
				$glId	= GetCpanelData('D',1);
				foreach ($array as $key => $val)
				{
					$amount					  = round(LedgerBalance($val[0],$glId,'JOURNAL_ENTRY'),2);	
					$lastTransDate			  = GetLastJournalDate($glId,'JOURNAL_ENTRY');		
	
				if($amount==0)
					$amount	= round(LedgerBalance($lastTransDate,$glId,'JOURNAL_ENTRY'),2);	
					
				$array_total[$val[1]][1] += $amount;
			?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>"><?php echo $amount=='0'?'-':number_format($amount)?></td>
            <?php
			}
			?>
          </tr>
          <tr>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>">&nbsp;</td>
            <?php
		}
			?>
          </tr>
          <tr bgcolor="#BFFFBF">
            <td nowrap="nowrap">Existing OD facility usage</td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>" class="cls_td_amount" id="<?php echo $val[1]?>2">-</td>
            <?php 
		}
		?>
          </tr>
          <tr bgcolor="#BFFFBF">
            <td nowrap="nowrap">Short Term Funding From BNK Consolidated</td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>" class="cls_td_amount" id="<?php echo $val[1]?>3">-</td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap">&nbsp;</td>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>">&nbsp;</td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" >Total - Addition</td>
            <td nowrap="nowrap" style="text-align:right"><?php echo number_format($customerOpening)?></td>
            <?php 
		foreach ($array as $key => $val)
		{
			$total = $array_total[$val[1]][0];
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>" class="cls_td_amount" id="<?php echo $val[1]?>"><?php echo ($total<0?'('.number_format(abs($total)).')':number_format(abs($total)))?></td>
            <?php 
		}
		?>
          </tr>
          
          <tr>
            <td nowrap="nowrap" >Total - Deduction</td>
            <td nowrap="nowrap" style="text-align:right"><?php echo '('.number_format($supplierOpening).')'?></td>
            <?php 
		foreach ($array as $key => $val)
		{
			$total = $array_total[$val[1]][1];
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>" class="cls_td_amount" id="<?php echo $val[1]?>"><?php echo '('.number_format(abs($total)).')'?></td>
            <?php 
		}
		?>
          </tr>
          <?php
		  $totalOpenning = round($customerOpening) - round($supplierOpening);
		  ?>
          <tr>
            <td nowrap="nowrap">Total</td>
            <td nowrap="nowrap" style="text-align:right"><?php echo ($totalOpenning<0?'('.number_format(abs($totalOpenning)).')':number_format(abs($totalOpenning)))?></td>
            <?php 
		foreach ($array as $key => $val)
		{
			$total = $array_total[$val[1]][0] - $array_total[$val[1]][1];
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>" class="cls_td_amount" id="<?php echo $val[1]?>"><?php echo ($total<0?'('.number_format(abs($total)).')':number_format(abs($total)))?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap">Total - Cumulative</td>
            <td nowrap="nowrap" style="text-align:right"><?php echo ($totalOpenning<0?'('.number_format(abs($totalOpenning)).')':number_format(abs($totalOpenning)))?></td>
            <?php 
		foreach ($array as $key => $val)
		{
			$total = $array_total[$val[1]][0] - $array_total[$val[1]][1];
			$totalOpenning += $total;
		?>
            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $val[2];?>" class="cls_td_amount"><?php echo ($totalOpenning<0?'('.number_format(abs($totalOpenning)).')':number_format(abs($totalOpenning)))?></td>
          <?php 
		}
		?>
          </tr>
        </tbody>
      </table></td>
  </tr>
  <tr>
  <tr height="40">
    <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
  </tr>
</table>
<?php
$body = ob_get_clean();

$mailHeader 	= "Monthly Cash Flow Forecast";
$FROM_NAME 		= 'NSOFT - SCREENLINE HOLDINGS.';
$FROM_EMAIL		= '';

//sendMessage($FROM_EMAIL,$FROM_NAME,$CRON_TABLE_TO,$mailHeader,$body,$CRON_TABLE_CC,$CRON_TABLE_BCC);
//----------------
$mail_TO		= $obj_comm->getEmailList($CRON_TABLE_TO);
$mail_CC		= $obj_comm->getEmailList($CRON_TABLE_CC);
$mail_BCC		= $obj_comm->getEmailList($CRON_TABLE_BCC);

		
//insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
insertTable($FROM_EMAIL,$FROM_NAME,'hemanthib@nimawum.com',$mailHeader,$body,"","");
 //-------------------------------------------------

echo $body;

function GetCustomerAmount($date)
{
	global $db;
	global $companyId;
	global $target_currencyId;
	$date	= explode('-',$date);
	$sql = "SELECT 
				SUM(FT.BALANCE_AMOUNT) AS AMOUNT		
			FROM finance_forecast_transaction_customer FT
			WHERE YEAR(FT.DATE) = '$date[0]'
				AND MONTH(FT.DATE) = '$date[1]'	
				AND FT.COMPANY_ID = $companyId ";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}

function GetCustomerOpeningAmount($date)
{
	global $db;
	global $companyId;
	global $target_currencyId;

	$sql = "SELECT 
				SUM(FT.BALANCE_AMOUNT) AS AMOUNT		
			FROM finance_forecast_transaction_customer FT
			WHERE DATE(FT.DATE) < '$date'
				AND FT.COMPANY_ID = $companyId ";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}

function GetCustomerActualCollection($date)
{
	global $db;
	global $companyId;
	$date	= explode('-',$date);

	$sql = "SELECT
			  ROUND(SUM(FCPRD.PAY_AMOUNT),2) AS AMOUNT
			FROM finance_customer_pay_receive_header FCPRH
			  INNER JOIN finance_customer_pay_receive_details FCPRD
				ON FCPRH.RECEIPT_NO = FCPRD.RECEIPT_NO
				  AND FCPRH.RECEIPT_YEAR = FCPRD.RECEIPT_YEAR
			WHERE YEAR(FCPRH.RECEIPT_DATE) = '$date[0]'
				AND MONTH(FCPRH.RECEIPT_DATE) = '$date[1]'
				AND FCPRH.COMPANY_ID = $companyId
				AND FCPRH.STATUS = 1 ";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}

function GetCustomerReceiptConfirmed($date)
{
	global $db;
	global $companyId;
	$date	= explode('-',$date);

	$sql = "SELECT
			  ROUND(SUM(FCPRD.PAY_AMOUNT),2) AS AMOUNT
			FROM finance_customer_pay_receive_header FCPRH
			  INNER JOIN finance_customer_pay_receive_details FCPRD
				ON FCPRH.RECEIPT_NO = FCPRD.RECEIPT_NO
				  AND FCPRH.RECEIPT_YEAR = FCPRD.RECEIPT_YEAR
			WHERE YEAR(FCPRH.RECEIPT_DATE) = '$date[0]'
				AND MONTH(FCPRH.RECEIPT_DATE) = '$date[1]'
				AND FCPRH.COMPANY_ID = $companyId
				AND FCPRH.STATUS = 1 
				AND RECEIPT_CONFIRMATION = 1";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}

function GetCustomerOpeningReceiptConfirmed($date)
{
	global $db;
	global $companyId;

	$sql = "SELECT
			  ROUND(SUM(FCPRD.PAY_AMOUNT),2) AS AMOUNT
			FROM finance_customer_pay_receive_header FCPRH
			  INNER JOIN finance_customer_pay_receive_details FCPRD
				ON FCPRH.RECEIPT_NO = FCPRD.RECEIPT_NO
				  AND FCPRH.RECEIPT_YEAR = FCPRD.RECEIPT_YEAR
			WHERE DATE(FCPRH.RECEIPT_DATE) < '$date'
				AND FCPRH.COMPANY_ID = $companyId
				AND FCPRH.STATUS = 1 
				AND RECEIPT_CONFIRMATION = 1";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}

function GetForeignSupplierAmount($date)
{
	global $db;
	global $companyId;
	global $target_currencyId;
	$date	= explode('-',$date);
	$sql = "SELECT 
				SUM(FT.BALANCE_AMOUNT) AS AMOUNT		
			FROM finance_forecast_transaction_supplier FT
			INNER JOIN mst_supplier MS
			  ON MS.intId = FT.CATEGORY_ID
			WHERE YEAR(FT.DATE) = '$date[0]'
				AND MONTH(FT.DATE) = '$date[1]'
				AND FT.COMPANY_ID = $companyId
				AND MS.intTypeId = 4";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}

function GetLocalSupplierAmount($date)
{
	global $db;
	global $companyId;
	global $target_currencyId;
	$date	= explode('-',$date);
	$sql = "SELECT 
				SUM(FT.BALANCE_AMOUNT) AS AMOUNT		
			FROM finance_forecast_transaction_supplier FT
			INNER JOIN mst_supplier MS
			  ON MS.intId = FT.CATEGORY_ID
			WHERE YEAR(FT.DATE) = '$date[0]'
				AND MONTH(FT.DATE) = '$date[1]'
				AND FT.COMPANY_ID = $companyId
				AND MS.intTypeId <> 4";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}

function GetSupplierOpeningAmount($date)
{
	global $db;
	global $companyId;
	global $target_currencyId;

	$sql = "SELECT 
				SUM(FT.BALANCE_AMOUNT) AS AMOUNT		
			FROM finance_forecast_transaction_supplier FT
			INNER JOIN mst_supplier MS
			  ON MS.intId = FT.CATEGORY_ID
			WHERE DATE(FT.DATE) < '$date'
				AND FT.COMPANY_ID = $companyId";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}

function GetSupplierOpeningVoucherConfirmed($date)
{
	global $db;
	global $companyId;

	$sql = "SELECT
			  ROUND(COALESCE(SUM(FSPD.AMOUNT),0),2) AS AMOUNT
			FROM finance_supplier_payment_header FSPH
			  INNER JOIN finance_supplier_payment_details FSPD
				ON FSPD.PAYMENT_NO = FSPH.PAYMENT_NO
				  AND FSPD.PAYMENT_YEAR = FSPH.PAYMENT_YEAR
			WHERE DATE(FSPH.PAY_DATE) < '$date'
				AND FSPH.COMPANY_ID = $companyId
				AND FSPH.STATUS = 1
				AND FSPH.VOUCHER_CONFIRMATION = 1";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}

function GetSupplierVoucherConfirmed($date)
{
	global $db;
	global $companyId;
	$date	= explode('-',$date);

	$sql = "SELECT
			  ROUND(COALESCE(SUM(FSPD.AMOUNT),0),2) AS AMOUNT
			FROM finance_supplier_payment_header FSPH
			  INNER JOIN finance_supplier_payment_details FSPD
				ON FSPD.PAYMENT_NO = FSPH.PAYMENT_NO
				  AND FSPD.PAYMENT_YEAR = FSPH.PAYMENT_YEAR
			WHERE YEAR(FSPH.PAY_DATE) = '$date[0]'
				AND MONTH(FSPH.PAY_DATE) = '$date[1]'
				AND FSPH.COMPANY_ID = $companyId
				AND FSPH.STATUS = 1
				AND FSPH.VOUCHER_CONFIRMATION = 1";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}



function LedgerBalance($date,$glId,$transType)	
{
	global $db;
	global $companyId;
	global $target_currencyId;
	$amount	= 0;
	$date	= explode('-',$date);
	
	$sql = "SELECT 
				T1.*,
				SUM(lastAmonut) AS AMOUNT 
			FROM(SELECT
					FT.CHART_OF_ACCOUNT_ID,
					COA.CHART_OF_ACCOUNT_NAME,
					DATE(FT.LAST_MODIFIED_DATE) 				AS dtmDate,
					CONCAT(FT.INVOICE_YEAR,'-',FT.INVOICE_NO) 	AS invoiceNo,
					CONCAT(FT.DOCUMENT_YEAR,'-',FT.DOCUMENT_NO) AS documentNo,
					
					ROUND(COALESCE(FT.AMOUNT / (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $target_currencyId AND 
					DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId) * (SELECT SUB_ER.dblExcAvgRate FROM 
					mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID AND 
					DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId),0),2) AS lastAmonut,
					
					FT.AMOUNT as ftAmount,
					FT.INVOICE_NO,
					FT.INVOICE_YEAR,
					FT.DOCUMENT_NO,
					FT.DOCUMENT_YEAR,
					FT.TRANSACTION_TYPE,
					AT.TRANSACTION_ADD_TYPE,
					FT.TRANSACTION_CATEGORY,
					FT.DOCUMENT_NO            AS DOC_NO,
					FT.DOCUMENT_YEAR          AS DOC_YEAR,
					FT.DOCUMENT_TYPE,
					FT.CURRENCY_ID,
					AT.FINANCE_TYPE_ID,
					FT.BANK_REFERENCE_NO,
					FT.LAST_MODIFIED_DATE,
					FT.SERIAL_ID			
				FROM finance_transaction FT
				INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT.CHART_OF_ACCOUNT_ID
				INNER JOIN finance_mst_account_sub_type ST ON ST.SUB_TYPE_ID=COA.SUB_TYPE_ID
				INNER JOIN finance_mst_account_main_type MT ON MT.MAIN_TYPE_ID=ST.MAIN_TYPE_ID
				INNER JOIN finance_mst_account_type AT ON AT.FINANCE_TYPE_ID=MT.FINANCE_TYPE_ID
				WHERE FT.COMPANY_ID = '$companyId' 
					AND COA.CHART_OF_ACCOUNT_ID IN ($glId)
					AND YEAR(FT.LAST_MODIFIED_DATE) = $date[0]
					AND MONTH(FT.LAST_MODIFIED_DATE) = $date[1]
					AND DOCUMENT_TYPE = '$transType' 
					AND FT.TRANSACTION_TYPE = 'C'
				)AS T1";
/*				if($glId == '1'){
					echo $sql;
					echo "<br/>";
				}*/
			$result = $db->RunQuery($sql);
			$row = mysqli_fetch_array($result);
			return $row["AMOUNT"];
}

function GetLastJournalDate($glId,$transType)
{
	global $db;
	
	$sql = "SELECT
			  LAST_MODIFIED_DATE
			FROM finance_transaction FT
			WHERE CHART_OF_ACCOUNT_ID IN($glId)
				AND DOCUMENT_TYPE = '$transType'
				AND FT.TRANSACTION_TYPE = 'C'
			ORDER BY LAST_MODIFIED_DATE DESC
			LIMIT 1";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return $row["LAST_MODIFIED_DATE"];	
}

function GetLoanScheduleUnsettledAmount($date,$scheduleNo,$scheduleYear)
{
	global $db;
	$date	= explode('-',$date);
	
	$sql = "SELECT
			  ROUND(COALESCE(SUM(AMOUNT),0),2)	AS AMOUNT
			FROM finance_loan_schedule_header LSH
			  INNER JOIN finance_loan_schedule_details LSD
				ON LSD.SCHEDULE_NO = LSH.SCHEDULE_NO
				  AND LSD.SCHEDULE_YEAR = LSH.SCHEDULE_YEAR
			WHERE LSH.STATUS = 1
				-- AND LSD.STATUS = 0
				AND YEAR(LSD.PAY_DATE) = $date[0]
				AND MONTH(LSD.PAY_DATE) = $date[1]
				AND LSH.SCHEDULE_NO = $scheduleNo
				AND LSH.SCHEDULE_YEAR = $scheduleYear";
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	return $row["AMOUNT"];	
}

function IsDueLoanPayment($date,$scheduleNo,$scheduleYear)
{
	global $db;
	$date	= explode('-',$date);
	
	$sql = "SELECT
			  ROUND(COALESCE(SUM(AMOUNT),0),2) AS AMOUNT
			FROM finance_loan_schedule_header LSH
			  INNER JOIN finance_loan_schedule_details LSD
				ON LSD.SCHEDULE_NO = LSH.SCHEDULE_NO
				  AND LSD.SCHEDULE_YEAR = LSH.SCHEDULE_YEAR
			WHERE LSH.STATUS = 1
				AND LSD.STATUS = 0
				AND PAY_DATE < DATE(NOW())
				AND YEAR(PAY_DATE) = '$date[0]'
				AND MONTH(PAY_DATE) = '$date[1]'
				AND LSH.SCHEDULE_NO = $scheduleNo
				AND LSH.SCHEDULE_YEAR = $scheduleYear";
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	if($row["AMOUNT"]>0)
		return true;
	else
		return false;
}

function GetLoanScheduleSettledAmount($date,$scheduleNo,$scheduleYear)
{
	global $db;
	$date	= explode('-',$date);
	
	$sql = "SELECT
			   ROUND(COALESCE(SUM(AMOUNT),0),2)	AS AMOUNT
			FROM finance_loan_schedule_header LSH
			  INNER JOIN finance_loan_schedule_details LSD
				ON LSD.SCHEDULE_NO = LSH.SCHEDULE_NO
				  AND LSD.SCHEDULE_YEAR = LSH.SCHEDULE_YEAR
			WHERE LSH.STATUS = 1
				AND LSD.STATUS = 1
				AND YEAR(LSD.PROCESS_DATE) = $date[0]
				AND MONTH(LSD.PROCESS_DATE) = $date[1]
				AND LSH.SCHEDULE_NO = $scheduleNo
				AND LSH.SCHEDULE_YEAR = $scheduleYear";
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	return $row["AMOUNT"];	
}

function GetLoanSchedule()
{
	global $db;
	
	$sql = "SELECT
			  SCHEDULE_NO	AS SCHEDULE_NO,
			  SCHEDULE_YEAR	AS SCHEDULE_YEAR,
			  REMARKS		AS REMARKS,
			  CURRENCY_ID	AS CURRENCY_ID,
			  SCHEDULE_DATE	AS SCHEDULE_DATE
			FROM finance_loan_schedule_header LSH
			WHERE LSH.STATUS = 1
			ORDER BY SCHEDULE_DATE";
	return $db->RunQuery($sql);
}

function LoadCommitments()
{
	global $db;
	$string = "";
	$sql = "SELECT *
			FROM finance_cash_flow_cpanel
			WHERE MAIN_CODE = 'CC'
			ORDER BY ORDER_BY";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		if($row["VALUE"]!="")
			$string .= $row["SUB_NAME"].' ('.$row["VALUE"].') ';
	}
	return $db->RunQuery($string);
}

function getDepriciateGL()
{
	global $db;
	
	$sql = "SELECT GROUP_CONCAT(DEPRECIATION_CREDIT_ID) AS ID 
			FROM finance_mst_chartofaccount_subcategory";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["ID"];
}

function GetCpanelData($mainCode,$id)
{
	global $db;
	
	$sql = "SELECT VALUE 
			FROM finance_cash_flow_cpanel 
			WHERE MAIN_CODE = '$mainCode'
				AND ORDER_BY = $id";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["VALUE"];
}
?>
<?php
$db->commit();		
$db->disconnect();
?>

<?php
$backwardseperator = "../../../../";
#BEGIN 	- INCLUDE FILES {
require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php"; 						$obj_comm 		= new cls_commonFunctions_get($db);	
include_once $_SESSION['ROOT_PATH']."class/dailyMails/dispatch/cls_customer_aging_report_get.php"; $obj_disp_get			= new cls_customer_aging_report_get($db);
include_once $_SESSION['ROOT_PATH']."class/tables/sys_report_cluster_types.php";				$sys_report_cluster_types	= new sys_report_cluster_types($db);
include_once $_SESSION['ROOT_PATH']."class/tables/mst_companies.php";							$mst_companies				= new mst_companies($db);
#END 	- INCLUDE FILES }

$payment_term_array 	= $obj_disp_get->GetPaymentTerm();

##############################################################################################
## Get report type wise email config rows													##
## Report Type = 'CUSTOMER_AGING_REPORT'													##
$result = $sys_report_cluster_types->getReportTypeWiseDetails('CUSTOMER_AGING_REPORT');
##############################################################################################

while($rowDetails = mysqli_fetch_array($result))
{	
	$mst_companies->set($rowDetails["COMPANY_ID"]);	#set company id to mst_companies class object.

ob_start();
?>
<style type="text/css">
table .bordered {
 *border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
}
.bordered {
	border: solid #ccc 1px;
	border-radius: 6px;
	box-shadow: 0 1px 1px #ccc;
	font-size: 11px;
	font-family: Verdana;
}
.bordered tr:hover {
	background: #fbf8e9;
	transition: all 0.1s ease-in-out;
}
.bordered td {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 2px;
}
.bordered th {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 4px;
	text-align: center;
}
.bordered th {
	background-color: #dce9f9;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
	background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -moz-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -ms-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -o-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: linear-gradient(top, #ebf3fc, #dce9f9);
	box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;
	border-top: none;
	text-shadow: 0 1px 0 rgba(255,255,255,.5);
}
.bordered td:first-child, .bordered th:first-child {
	border-left: none;
}
.bordered th:first-child {
	border-radius: 6px 0 0 0;
}
.bordered th:last-child {
	border-radius: 0 6px 0 0;
}
.bordered th:only-child {
	border-radius: 6px 6px 0 0;
}
.bordered tr:last-child td:first-child {
	border-radius: 0 0 0 6px;
}
.bordered tr:last-child td:last-child {
	border-radius: 0 0 6px 0;
}
.bordered {
	border: solid #ccc 1px;
	border-radius: 6px;
	box-shadow: 0 1px 1px #ccc;
	font-size: 11px;
	font-family: Verdana;
}
.bordered tr:hover {
	background: #fbf8e9;
	transition: all 0.1s ease-in-out;
}
.bordered td {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 2px;
}
.bordered th {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 4px;
	text-align: center;
}
.bordered th {
	background-color: #dce9f9;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
	background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -moz-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -ms-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -o-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: linear-gradient(top, #ebf3fc, #dce9f9);
	box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;
	border-top: none;
	text-shadow: 0 1px 0 rgba(255,255,255,.5);
}
.bordered td:first-child, .bordered th:first-child {
	border-left: none;
}
.bordered th:first-child {
	border-radius: 6px 0 0 0;
}
.bordered th:last-child {
	border-radius: 0 6px 0 0;
}
.bordered th:only-child {
	border-radius: 6px 6px 0 0;
}
.bordered tr:last-child td:first-child {
	border-radius: 0 0 0 6px;
}
.bordered tr:last-child td:last-child {
	border-radius: 0 0 6px 0;
}
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: right;
}
.reportHeader {
	font-size: 18px;
	font-style: normal;
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
}
#progress {
	width: 100px;
	border: 1px solid black;
	position: relative;
	padding: 1px;
}
#percent {
	position: absolute;
	left: 50%;
}
#bar {
	height: 15px;
	background-color: #0C0;
}
</style>

<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
  <tr>
    <td><table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td width="8%" class="normalfnt"><img src="http://accsee.com/screenline.jpg" alt="logo2" style="vertical-align:"  /></td>
          <td align="center" valign="top" width="75%" class="topheadBLACK"><span class="reportHeader">CUSTOMER AGING REPORT - <?php echo  strtoupper($mst_companies->getstrName())?></span></td>
          <td width="9%" class="tophead"><img src="http://accsee.com/nsoft_logo.png" alt="logo1" style="vertical-align:"  /></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td width="873"><table width="100%"  class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
        <thead>
          <tr class="">
            <th width="301" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Customer</th>
            <td width="63" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Total LC Amount <br />
              [$]</td>
            <td width="63" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Total Order Qty</td>
            <td width="69" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Total Receive Qty</td>
            <td width="69" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Total Dispatch Qty</td>
            <td width="107" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Customer Approved Dispatch Qty</td>
            <td width="69" style="background:#FFFFFF;color:#F00;font-weight:bold;text-align:center">Qty Variation (Receive vs Dispatch)</td>
            <td width="69" style="background:#FFFFFF;color:#F00;font-weight:bold;text-align:center">Qty Variation (Dispatch vs Invoiced)</td>
            <td width="69" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Dispatch Progress</td>
            <td width="69" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Total Customer Order Amount <br />
              [$]</td>
            <td width="69" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Total Dispatch Amount <br />
              [$]</td>
            <td width="126" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Customer Approved Dispatch Amount <br />
              [$]</td>
            <td width="68" style="background:#FFFFFF;color:#F00;font-weight:bold;text-align:center">Amount Variation (Order vs Dispatch) <br />
              [$]</td>
            <td width="68" style="background:#FFFFFF;color:#F00;font-weight:bold;text-align:center">Amount Variation (Dispatch vs CAD) <br />
              [$]</td>
            <td width="58" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">Invoice Amount <br />
              [$]</td>
            <td width="64" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">To be Invoiced<br />
              [$]</td>
            <td width="70" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">Payment Received Amount<br />
              [$]</td>
            <td width="86" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">Balance to be Received<br />
              [$]</td>
            <?php for($i=0;$i<count($payment_term_array);$i++){?>
            <th colspan="2" style="width:40px;background:#9BC2E6;color:#FFF;font-weight:bold;text-align:center"><?php echo $payment_term_array[$i]?></th>
            <?php }?>
          </tr>
        </thead>
        <tbody>
          <?php 
			$booAvailable							= false;  
			$result1= getDetails($rowDetails["COMPANY_ID"]);
			while($row1=mysqli_fetch_array($result1))
			{
				$booAvailable						= true;
				$customer 							= $row1['CUSTOMER'];
				$totLCAmount 						= $row1['TOT_LCAMOUNT'];
				$totOrderQty 						= $row1['TOT_ORDER_QTY'];
				$totInvoicedQty						= $row1['TOT_INVOICED_QTY'];
				$totReceivedQty						= $row1['TOT_RECEIVED_QTY'];
				$totDispQty 						= $row1['TOT_DISP_QTY'];
				$custAppDispQty 					= 0;				
				$dispatchProgress					= ($row1['DISP_PROGRESS']>100?100:$row1['DISP_PROGRESS']);//OUTER SUB
				$orderAmount						= $row1['ORDER_AMOUNT'];
				$dispatchAmount						= $row1['DISPATCH_AMOUNT'];
				$custAppDispAmount						= 0;
				$invoice 							= $row1["INVOICE_AMOUNT"];
				$balToInvo							= $row1["BAL_TO_INV"];
				$paymentRcvd						= $row1['PAYMENT_RECEIVED_AMOUNT'];
				$balanceToPayRcvd					= $row1['BALANCE_TO_PAYMENT_RECEIVED'];
				$variReceivedVsDispatch				= $totReceivedQty - $totDispQty;
				$variDispatchVsInvoices				= $totDispQty - $totInvoicedQty;
				$variOrderAmountVsDispatchAmount	= $orderAmount - $dispatchAmount;
				$variDispatchAmountVsCADAmount		= 0;
	?>
          <tr>
            <td bgcolor="#FFFFFF" class="normalfnt" nowrap="nowrap" title="Customer Name"><?php echo $customer; ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Total LC Amount"><?php echo number_format($totLCAmount); ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Total Order Qty"><?php echo number_format($totOrderQty); ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Total Fabric Received Qty"><?php echo number_format($totReceivedQty); ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Total Dispatch Qty"><?php echo number_format($totDispQty); ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Customer Approved Dispatch Qty "><?php echo number_format($custAppDispQty); ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Qty Variation (Receive vs Dispatch)"><?php echo number_format($variReceivedVsDispatch); ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Qty Variation (Dispatch vs Invoiced)"><?php echo number_format($variDispatchVsInvoices); ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Dispatch Qty / Order Qty * 100"><div id="progress"><span id="percent"><?php echo $dispatchProgress?>%</span>
                <div id="bar" style="width:<?php echo $dispatchProgress?>px"></div>
              </div></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Total Customer Order Amount [$]"><?php echo number_format($orderAmount); ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Total Dispatch Amount [$]"><?php echo number_format($dispatchAmount); ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Customer Approved Dispatch Amount [$]"><?php echo number_format($custAppDispAmount); ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Amount Variation (Order vs Dispatch) [$]"><?php echo number_format($variOrderAmountVsDispatchAmount); ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Amount Variation (Dispatch vs CAD) [$]"><?php echo number_format($variDispatchAmountVsCADAmount); ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Invoice Amount [$]"><?php echo number_format($invoice); ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Balance To Invoice Amount [$]"><?php echo number_format($balToInvo); ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Payment Received Amount [$]"><?php echo number_format($paymentRcvd); ?></td>
            <td bgcolor="#FFFFFF" class="normalfntRight" title="Balance To Payment Receive Amount [$]"><?php echo number_format($balanceToPayRcvd); ?></td>
            <?php  
			$start 		= 0;
			$agingCSS 	= "color:#F00";
			
		for($i=0;$i<count($payment_term_array);$i++){
			$field1	= "NOT_EXCEED_TREM_QTY$i";
			$fieldN	= $row1[$field1];
			$j		= $i+1;
			$field2	= "EXCEED_TREM_QTY$j";
			$fieldE	= $row1[$field2];
			
			$tot_payment_term_array[$i]['B']	+= $row1[$field1];
			$tot_payment_term_array[$i]['R']	+= $row1[$field2];
			?>
            <td width="42" nowrap="nowrap" class="normalfnt" style="text-align:right" title="<?php echo $payment_term_array[$i]?>"><?php echo ($fieldN==0?'&nbsp;':number_format($fieldN))?></td>
            <td width="42" nowrap="nowrap" class="normalfnt"  style="text-align:right;<?php echo $agingCSS?>" title="<?php echo $payment_term_array[$i]?>"><?php echo ($fieldE==0?'&nbsp;':number_format($fieldE))?></td>
            <?php }?>
            <?php 
		?>
          </tr>
          <?php
				$tot_LCAmonut				+= round($totLCAmount);
				$tot_orderQty				+= round($totOrderQty);
				$tot_receiveQty				+= round($totReceivedQty);
				$tot_DispQty				+= round($totDispQty);
				$tot_custAppDispQty 		+= round($custAppDispQty);
				$tot_orderAmount	 		+= round($orderAmount);
				$tot_dispatchAmount			+= round($dispatchAmount);
				$tot_custAppDispAmount		+= round($custAppDispAmount);
				$tot_invoice				+= round($invoice);
				$tot_balToInvo				+= round($balToInvo);
				$tot_paymentRcvd			+= round($paymentRcvd);
				$tot_balanceToPayRcvd		+= round($balanceToPayRcvd);
			}
	?>
          <tr style="font-weight:bold;text-align:right">
            <td style="text-align:center">Total</td>
            <td><?php echo number_format($tot_LCAmonut);?></td>
            <td><?php echo number_format($tot_orderQty);?></td>
            <td><?php echo number_format($tot_receiveQty);?></td>
            <td><?php echo number_format($tot_DispQty);?></td>
            <td><?php echo number_format($tot_custAppDispQty);?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><?php echo number_format($tot_orderAmount);?></td>
            <td><?php echo number_format($tot_dispatchAmount);?></td>
            <td><?php echo number_format($tot_custAppDispAmount);?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><?php echo number_format($tot_invoice);?></td>
            <td><?php echo number_format($tot_balToInvo);?></td>
            <td><?php echo number_format($tot_paymentRcvd);?></td>
            <td><?php echo number_format($tot_balanceToPayRcvd);?></td>
            <?php for($i=0;$i<count($payment_term_array);$i++){?>
            <td><?php echo ($tot_payment_term_array[$i]['B']==0?'&nbsp;':number_format($tot_payment_term_array[$i]['B']))?></td>
            <td><?php echo ($tot_payment_term_array[$i]['R']==0?'&nbsp;':number_format($tot_payment_term_array[$i]['R']))?></td>
            <?php } ?>
          </tr>
        </tbody>
      </table></td>
  </tr>
  <tr height="40">
    <td>&nbsp;</td>
  </tr>
</table>
<?php
$body = ob_get_clean();

$mailHeader 	= "CUSTOMER AGING REPORT NEW - ".strtoupper($mst_companies->getstrName());
$FROM_NAME 		= 'NSOFT - SCREENLINE HOLDINGS.';
$FROM_EMAIL		= '';

##############################################################################################
## Get email acount from `sys_cronjob` & `sys_report_cluster_types` tables and concat with ',' if email account available in `sys_cronjob` table.
$mail_TO		= $obj_comm->getEmailList($rowDetails["TO_USER_EMAIL_ID_LIST"]).($rowDetails["TO_USER_EMAIL_ID_LIST"]==''?'':',').$obj_comm->getEmailList($CRON_TABLE_TO);
$mail_CC		= $obj_comm->getEmailList($rowDetails["CC_USER_EMAIL_ID_LIST"]).($rowDetails["CC_USER_EMAIL_ID_LIST"]==''?'':',').$obj_comm->getEmailList($CRON_TABLE_CC);
$mail_BCC		= $obj_comm->getEmailList($rowDetails["BCC_USER_EMAIL_ID_LIST"]).($rowDetails["BCC_USER_EMAIL_ID_LIST"]==''?'':',').$obj_comm->getEmailList($CRON_TABLE_BCC);
##############################################################################################


##############################################################################################
## Insert email data row to `sys_emailpool` table											##
## $booAvailable - boolean variable to check whether data available for relevent company.
if($booAvailable)	#check $booAvailable is true, then inset row.
	insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
##############################################################################################


##############################################################################################
## Print HTML																				##
echo $body;
##############################################################################################

##############################################################################################
## Clear variables when company main mail loop over											##
unset($tot_LCAmonut,$tot_orderQty,$tot_receiveQty,$tot_DispQty,$tot_custAppDispQty,$tot_orderAmount,$tot_dispatchAmount,$tot_custAppDispAmount,$tot_invoice,$tot_balToInvo,$tot_paymentRcvd,$tot_balanceToPayRcvd,$tot_payment_term_array);
##############################################################################################
}

function getDetails($companyId)
{
	global $db;

   	$sql = "SELECT 
				CUSTOMER AS CUSTOMER,
				IFNULL(SUM(TOT_LCAMOUNT),0)								AS TOT_LCAMOUNT,
				SUM(TOT_ORDER_QTY)										AS TOT_ORDER_QTY,
				ROUND((ORDER_AMOUNT/TOT_ORDER_QTY),2) 					AS PRICE, 
				SUM(TOT_INVOICED_QTY)									AS TOT_INVOICED_QTY,
				SUM(TOT_RECEIVED_QTY)									AS TOT_RECEIVED_QTY,
				SUM(TOT_DISP_QTY)										AS TOT_DISP_QTY,
				ROUND(SUM(TOT_DISP_QTY)/SUM(TOT_ORDER_QTY)*100) 		AS DISP_PROGRESS,
				SUM(ORDER_AMOUNT)										AS ORDER_AMOUNT,
				SUM(DISPATCH_AMOUNT)									AS DISPATCH_AMOUNT,
				SUM(INVOICE_AMOUNT)										AS INVOICE_AMOUNT,
				SUM(PAYMENT_RECEIVED_AMOUNT)							AS PAYMENT_RECEIVED_AMOUNT,
				SUM(BAL_TO_INV)											AS BAL_TO_INV,
				SUM(BALANCE_TO_PAYMENT_RECEIVED)						AS BALANCE_TO_PAYMENT_RECEIVED,
				
				ROUND(SUM(IF((DIFF_DATES >= 0  && DIFF_DATES <= 15 && DIFF_DATES <= PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0)))	 	AS NOT_EXCEED_TREM_QTY0, 
				ROUND(SUM(IF((DIFF_DATES >= 0  && DIFF_DATES <= 15 && DIFF_DATES > PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) 		AS EXCEED_TREM_QTY1, 
				
				ROUND(SUM(IF((DIFF_DATES >= 16  && DIFF_DATES <= 30 && DIFF_DATES <= PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) 	AS NOT_EXCEED_TREM_QTY1,
				ROUND(SUM(IF((DIFF_DATES >= 16  && DIFF_DATES <= 30 && DIFF_DATES > PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) 		AS EXCEED_TREM_QTY2, 
				
				ROUND(SUM(IF((DIFF_DATES >= 31  && DIFF_DATES <= 45 && DIFF_DATES <= PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) 	AS NOT_EXCEED_TREM_QTY2,
				ROUND(SUM(IF((DIFF_DATES >= 31  && DIFF_DATES <= 45 && DIFF_DATES > PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) 		AS EXCEED_TREM_QTY3,
				
				ROUND(SUM(IF((DIFF_DATES >= 46  && DIFF_DATES <= 60 && DIFF_DATES <= PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) 	AS NOT_EXCEED_TREM_QTY3,
				ROUND(SUM(IF((DIFF_DATES >= 46  && DIFF_DATES <= 60 && DIFF_DATES > PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) 		AS EXCEED_TREM_QTY4, 
				
				ROUND(SUM(IF((DIFF_DATES >= 61  && DIFF_DATES <= 90 && DIFF_DATES <= PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) 	AS NOT_EXCEED_TREM_QTY4,
				ROUND(SUM(IF((DIFF_DATES >= 61  && DIFF_DATES <= 90 && DIFF_DATES > PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) 		AS EXCEED_TREM_QTY5, 

				ROUND(SUM(IF((DIFF_DATES >=91),((0)),0))) 																					AS NOT_EXCEED_TREM_QTY5,
				ROUND(SUM(IF((DIFF_DATES >=91),((BALANCE_TO_PAYMENT_RECEIVED)),0))) 														AS EXCEED_TREM_QTY6
			
		  FROM
				(SELECT 
					TB.CUSTOMER,
					CUSTOMER_ID,
					TB.TOT_LCAMOUNT																												AS TOT_LCAMOUNT,
					TB.TOT_ORDER_QTY 																											AS TOT_ORDER_QTY,
					TB.TOT_INVOICED_QTY																												AS TOT_INVOICED_QTY,
					TB.TOT_RECEIVED_QTY																											AS TOT_RECEIVED_QTY,
					TB.TOT_DISP_QTY 																											AS TOT_DISP_QTY,				
					TB.ORDER_AMOUNT																												AS ORDER_AMOUNT,
					TB.DISPATCH_AMOUNT 																												AS DISPATCH_AMOUNT,
					TB.INVOICE_AMOUNT																											AS INVOICE_AMOUNT,
					(TB.PAYMENT_RECEIVED_AMOUNT + TB.ADVANCE_SETTLED - TB.CREDIT_NOTE_AMOUNT + TB.DEBIT_NOTE_AMOUNT) 							AS PAYMENT_RECEIVED_AMOUNT ,
					(TB.DISPATCH_AMOUNT - TB.INVOICE_AMOUNT) 																					AS BAL_TO_INV,
					DIFF_DATES																													AS DIFF_DATES,
					PAYMENT_TERM 																												AS PAYMENT_TERM,
					(ROUND((TB.INVOICE_AMOUNT),2) - PAYMENT_RECEIVED_AMOUNT - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT + DEBIT_NOTE_AMOUNT) 		AS BALANCE_TO_PAYMENT_RECEIVED 
				FROM (					
					SELECT					
						OH.intCustomer 														AS CUSTOMER_ID,
						mst_customer.strName 												AS CUSTOMER,
						OH.intOrderNo 														AS ORDER_NO,
						OH.intOrderYear 													AS ORDER_YEAR,
						SUM(OD.intQty) 														AS TOT_ORDER_QTY,					
						
						(SELECT 
							ROUND(COALESCE(SUM(OD1.intQty),0))
						FROM trn_orderheader OH1  
						INNER JOIN trn_orderdetails AS OD1
							ON OH1.intOrderNo = OD1.intOrderNo
							AND OH1.intOrderYear = OD1.intOrderYear
						WHERE OH1.intOrderNo = OH.intOrderNo
							AND OH1.intOrderYear = OH.intOrderYear
							AND OH1.LC_STATUS = 6)											AS TOT_LCAMOUNT,
							
						(SELECT
						  SUM(QTY)
						FROM finance_customer_invoice_details CID
						  INNER JOIN finance_customer_invoice_header CIH
							ON CIH.SERIAL_NO = CID.SERIAL_NO
							  AND CIH.SERIAL_YEAR = CID.SERIAL_YEAR
						WHERE CIH.STATUS = 1
							AND CIH.ORDER_NO = OH.intOrderNo
							AND CIH.ORDER_YEAR = OH.intOrderYear)							AS TOT_INVOICED_QTY,
						
						(SELECT 
							ROUND(COALESCE(SUM(FRD.dblQty),0))
						FROM ware_fabricreceiveddetails FRD
						INNER JOIN ware_fabricreceivedheader FRH
							ON FRH.intFabricReceivedNo = FRD.intFabricReceivedNo
							AND FRH.intFabricReceivedNo = FRD.intFabricReceivedNo
						WHERE FRH.intOrderNo = OH.intOrderNo
							AND FRH.intOrderYear = OH.intOrderYear
							AND FRH.intStatus = 1) 											AS TOT_RECEIVED_QTY,
					
						(SELECT 
							ROUND(COALESCE(SUM(FDD.dblGoodQty),0))
						FROM ware_fabricdispatchdetails FDD 
						INNER JOIN ware_fabricdispatchheader FDH 
							ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
							AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
						WHERE FDH.intOrderNo = OH.intOrderNo 
							AND FDH.intOrderYear = OH.intOrderYear 
							AND FDH.intStatus = 1)											AS TOT_DISP_QTY,
						
						ROUND(SUM(OD.intQty * OD.dblPrice)/SUM(OD.intQty),4) 				AS PRICE1,
						
						ROUND((select SUM(OD.intQty * OD3.dblPrice) from 
								trn_orderdetails as OD3  
								WHERE OD3.intOrderNo = OD.intOrderNo 
								AND OD3.intOrderYear = OD.intOrderYear
						 ),0) 																AS ORDER_AMOUNT,
						
						ROUND(COALESCE((SELECT SUM(FDD.dblGoodQty * OD2.dblPrice) 
						FROM ware_fabricdispatchdetails FDD 
						INNER JOIN ware_fabricdispatchheader FDH 
							ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
							AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
						INNER JOIN trn_orderdetails AS OD2 
						ON FDH.intOrderNo = OD2.intOrderNo 
						AND FDH.intOrderYear = OD2.intOrderYear 
						AND FDD.intSalesOrderId = OD2.intSalesOrderId
						WHERE FDH.intOrderNo = OH.intOrderNo 
							AND FDH.intOrderYear = OH.intOrderYear 
							AND FDH.intStatus = 1),0)) AS DISPATCH_AMOUNT, 
						
						FPT.strName       													AS PAYMENT_TERM,

						COALESCE(DATEDIFF(NOW(),(SELECT
							DATE(FIH.INVOICED_DATE)
						FROM finance_customer_invoice_header FIH
						WHERE FIH.ORDER_NO = OH.intOrderNo
							AND FIH.ORDER_YEAR = OH.intOrderYear
							AND FIH.STATUS = 1 
						ORDER BY 
							DATE(FIH.INVOICED_DATE) DESC LIMIT 1)),0) 						AS DIFF_DATES,

						ROUND(COALESCE((SELECT 
							SUM(VALUE) 
						FROM finance_customer_transaction FCT
						WHERE FCT.ORDER_NO = OH.intOrderNo
							AND FCT.ORDER_YEAR = OH.intOrderYear
							AND DOCUMENT_TYPE = 'INVOICE'),0),2)							AS INVOICE_AMOUNT, 
							
						ROUND(COALESCE((SELECT SUM(VALUE) * -1
						FROM finance_customer_transaction FCT
						WHERE FCT.ORDER_NO = OH.intOrderNo
							AND FCT.ORDER_YEAR = OH.intOrderYear
							AND FCT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2)						AS PAYMENT_RECEIVED_AMOUNT,			
						
						ROUND(COALESCE((SELECT 
							SUM(VALUE) * -1
						FROM finance_customer_transaction FCT
						WHERE FCT.ORDER_NO = OH.intOrderNo
							AND FCT.ORDER_YEAR = OH.intOrderYear
							AND FCT.INVOICE_NO IS NOT NULL
							AND FCT.INVOICE_YEAR IS NOT NULL
							AND DOCUMENT_TYPE = 'ADVANCE'),0),2)							AS ADVANCE_SETTLED,
						
						ROUND(COALESCE((SELECT 
							SUM(VALUE) * -1
						FROM finance_customer_transaction FCT
						WHERE FCT.ORDER_NO = OH.intOrderNo
							AND FCT.ORDER_YEAR = OH.intOrderYear
							AND FCT.DOCUMENT_TYPE = 'CREDIT'),0),2)							AS CREDIT_NOTE_AMOUNT,
						
						ROUND(COALESCE((SELECT 
							SUM(VALUE)
						FROM finance_customer_transaction FCT
						WHERE FCT.CUSTOMER_ID = OH.intCustomer
							AND FCT.DOCUMENT_TYPE = 'DEBIT_1'),0),2)						AS DEBIT_NOTE_AMOUNT 
					
					FROM trn_orderheader AS OH
					INNER JOIN trn_orderdetails AS OD 
						ON OH.intOrderNo = OD.intOrderNo 
						AND OH.intOrderYear = OD.intOrderYear
					INNER JOIN mst_financepaymentsterms FPT 
						ON FPT.intId = OH.intPaymentTerm
					INNER JOIN mst_customer 
						ON OH.intCustomer = mst_customer.intId
					INNER JOIN mst_locations LO
						ON LO.intId = OH.intLocationId
					WHERE 1=1
						-- OH.intStatus = 1
						-- AND OH.PAYMENT_RECEIVE_COMPLETED_FLAG = 0
						AND LO.intCompanyId = $companyId
					GROUP BY
					OH.intCustomer,
					OH.intOrderNo,
					OH.intOrderYear) AS TB
				GROUP BY ORDER_NO,
				ORDER_YEAR
				 HAVING(ROUND(BALANCE_TO_PAYMENT_RECEIVED)>0)
				) AS TB1
			GROUP BY CUSTOMER_ID
			-- HAVING(BALANCE_TO_PAYMENT_RECEIVED>0)
			ORDER BY CUSTOMER";
			//echo $sql;
	return $db->RunQuery($sql);
}	
?>

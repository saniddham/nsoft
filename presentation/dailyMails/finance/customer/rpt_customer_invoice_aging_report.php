<?php
$cronjobId	= 45;

session_start();
$backwardseperator 	= "../../../../";
$thisFilePath 		= $_SERVER['PHP_SELF'];
ini_set('max_execution_time',600);

include_once $_SESSION['ROOT_PATH']."class/dailyMails/dispatch/cls_customer_aging_report_get.php";
 
$obj_disp_get			= new cls_customer_aging_report_get($db);

$location 				= $_SESSION['CompanyID'];
$res					= $obj_comm->loadLocationName($location,'RunQuery');
$row 					= mysqli_fetch_array($res);
$locationName			= $row['strName'];
$payment_term_array 	= $obj_disp_get->GetPaymentTerm();
ob_start();
?>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}


 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}


.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}



.odd{background-color:#F5F5F5;}
.even{background-color:#FFFFFF;}
.mouseover {
	cursor: pointer;
}


.txtNumber{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}

.reportHeader {
	font-size: 18px;
	/*text-align:center,*/
	font-style: normal;
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
}




</style>

<style type="text/css">
#progress {
 width: 100px;   
 border: 1px solid black;
 position: relative;
 padding: 1px;
}

#percent {
 position: absolute;   
 left: 50%;
}

#bar {
 height: 15px;
 background-color: #0C0;
}
</style>
<table width="1250" cellpadding="0" cellspacing="0" align="center">
<tr>
<td colspan="3"></td>
</tr>
<tr>
    <td colspan="9" align="center" ><table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="tophead"><table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td width="8%" class="normalfnt"><img style="vertical-align:" src="http://accsee.com/screenline.jpg"  /></td>
            <td align="center" valign="top" width="75%" class="topheadBLACK"><?php
/*	 $SQL = "SELECT
*
FROM
mst_companies
Inner Join mst_country ON mst_companies.intCountryId = mst_country.intCountryID
WHERE
mst_companies.intId =  '1'
;";
*/	
	$result 			= $db->RunQuery($SQL);
	$result				= $obj_comm->loadLoctionComanyDetails($locationId,'RunQuery');
	$row 				= mysqli_fetch_array($result);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];
	
	?>
              <b><?php echo $companyName.""; ?></b>
              <p class="normalfntMid">
                <?php echo $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry."."."<br><b>Tel : </b>".$companyPhone." <b>Fax : </b>".$companyFax." <br><b>E-Mail : </b>".$companyEmail." <b>Web : </b>".$companyWeb;?></p></td>
            <td width="9%" class="tophead"><img src="http://accsee.com/nsoft_logo.png" style="vertical-align:"  /></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ></div>
<table width="1250" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td colspan="2">
  <table width="100%">
  <tr>
    <td colspan="8" align="center" class="reportHeader">CUSTOMER AGING REPORT - BASE INVOICED DATE</td>
    </tr>
  </table>
  </td>
</tr>
<tr>
  <td colspan="2">
    <table width="100%">
      <tr>
        <td>
          
          <table width="100%"  class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <thead>
              <tr class="">
                <th width="301" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Customer</th>
                <td width="63" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Total LC Amount</td>
                <td width="63" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Total Order Qty</td>
                <td width="69" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Total Dispatch Qty</td>
                <td width="107" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Customer Approved Dispatch Qty</td>
                <td width="69" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Dispatch Progress</td>
                <td width="69" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Dispatch Amount</td>
                <td width="126" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Customer Approved Dispatch Amount</td>
                <td width="68" style="background:#FFDA65;color:#FFF;font-weight:bold;text-align:center">variation</td>
                <td width="58" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">Invoice</td>
                <td width="64" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">To be Invoiced</td>
                <td width="70" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">Payment Received</td>
                <td width="70" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">Advance to be Settled</td>
                <td width="70" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">DebitNote to be Settled</td>
                <td width="86" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">Balance to be Received</td>
				<?php for($i=0;$i<count($payment_term_array);$i++){?>
                <th colspan="2" style="width:40px;background:#9BC2E6;color:#FFF;font-weight:bold;text-align:center"><?php echo $payment_term_array[$i]?></th>
                <?php }?>
                </tr>
              </thead>
            <tbody>
            <?php
            $tot_LCAmonut	= 0;
            $tot_orderQty	= 0;
            $tot_DispQty	= 0;
            $tot_custAppDispQty = 0;
            $tot_dispatchAmount = 0;
            $tot_custAppDispAmn = 0;
            $tot_variation = 0;
            $tot_invoice = 0;
            $tot_balToInvo = 0;
            $tot_advTobeSettle = 0;
            $tot_dbnTobeSettle = 0;
            $tot_paymentRcvd	= 0;
            $tot_balanceToPayRcvd	= 0;
			$result_cus = GetCustomers($companyId);
			while($rowCus = mysqli_fetch_array($result_cus))
			{
				$totLCAmount 		= 0;
				$totOrderQty		= 0;
				$totDispQty			= 0;
				$custAppDispQty		= 0;
				$dispatchAmount		= 0;
				$custAppDispAmn		= 0;
				$variation			= 0;
				$invoice			= 0;
				$balToInvo			= 0;
				$paymentRcvd		= 0;
				$balanceToPayRcvd	= 0;
				$adv_settle			= 0;
				$creditNote			= 0;
				//$debitNote			= 0;
				$totPayRcv			= 0;
				$tot_advTobeSettle	= 0;
				$tot_dbnTobeSettle	= 0;
				
				$array	= array(array());
                $payment_Tot_arry = array(array());
				$result_details = $obj_disp_get->get_customers_delivery_rpt_base_invoice_result($rowCus['customerId'],$companyId);
				while($rowD = mysqli_fetch_array($result_details))
				{
					$customer 			 = $rowD['CUSTOMER'];
					$totOrderQty		+= $rowD['TOT_ORDER_QTY'];
					$totDispQty			+= $rowD['TOT_DISP_QTY'];
					$custAppDispQty		+= $rowD['CUST_APP_DISP_QTY'];
					$dispatchAmount		+= $rowD['DISP_AMOUNT'];
					$custAppDispAmn		+= $rowD['CUST_APP_DISP_AMOUNT'];
					$paymentRcvd		+= $rowD['PAYMENT_RECEIVED_AMOUNT'];
					$adv_settle			+= $rowD['ADVANCE_SETTLED'];
					$creditNote			+= $rowD['CREDIT_NOTE_AMOUNT'];
					//$debitNote			+= $rowD['DEBIT_NOTE_AMOUNT'];
					$invoice			+= $rowD['INVOICE_AMOUNT'];
					$amount				 = $rowD['BALANCE_TO_PAYMENT_RECEIVED'];
					
					$start				 = -1;
					$diff_date			 = $rowD['DIFF_DATES'];
					
					for($i=0;$i<count($payment_term_array);$i++)
					{
						if($diff_date>90 && $payment_term_array[$i]=='More')
						{
							$array[$i]["RED"] += $amount;
						}
						else if($diff_date>$start && $diff_date<=$payment_term_array[$i] && $diff_date<=$rowD['PAYMENT_TERM'])
						{
							$array[$i]["BLACK"] += $amount;
						}
						else if($diff_date>$start && $diff_date<=$payment_term_array[$i] && $diff_date>$rowD['PAYMENT_TERM'])
						{
							$array[$i]["RED"] += $amount;
						}
						
						$start = $payment_term_array[$i];
					}

				}
				
				$debitTobeSettled		= getDebitTobeSettledAmount($rowCus['customerId'],1);
				$advanceTobeSettled		= getAdvanceTobeSettledAmount($rowCus['customerId'],1);
				$variation 				= (round($dispatchAmount)-round($custAppDispAmn));
				$totPayRcv				= round($paymentRcvd+$adv_settle+$creditNote);
				$balToInvo				= (round($dispatchAmount)-round($invoice));
				$balanceToPayRcvd		= (round($invoice)-round($totPayRcv));
				$dispatchProgress 		= ((($totDispQty/$totOrderQty)*100)>100?100:($totDispQty/$totOrderQty)*100);
				
				if($totOrderQty <= 0)
					continue;
				if($balanceToPayRcvd <= 0)
					continue;
				
				?>
				<tr>
                    <td bgcolor="#FFFFFF" class="normalfnt" nowrap="nowrap" title="Customer Name"><?php echo $customer; ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight" title="Total LC Amount"><?php echo number_format(round($totLCAmount)); ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight" title="Total Order Qty"><?php echo number_format(round($totOrderQty)); ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight" title="Total Dispatch Qty"><?php echo number_format(round($totDispQty)); ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight" title="Customer Approved Dispatch Qty "><?php echo number_format(round($custAppDispQty)); ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight" title="Dispatch Qty / Order Qty * 100"><div id="progress"><span id="percent"><?php echo round($dispatchProgress)?>%</span><div id="bar" style="width:<?php echo round($dispatchProgress)?>px"></div></div></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight" title="Dispatch Amount"><?php echo number_format(round($dispatchAmount)); ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight" title="Customer Approved Dispatch Amount"><?php echo number_format(round($custAppDispAmn)); ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight" title="Variation"><?php echo number_format($variation); ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight" title="Invoice Amount"><?php echo number_format(round($invoice)); ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight" title="Balance To Invoice"><?php echo number_format($balToInvo); ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight" title="Payment Received"><?php echo number_format(round($totPayRcv)); ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight" title="Advance To be Settled"><?php echo (round($advanceTobeSettled)==0?number_format(round($advanceTobeSettled)):"(".number_format(round($advanceTobeSettled)).")"); ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight" title="DebitNote To be Settled"><?php echo number_format(round($debitTobeSettled)); ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight" title="Balance To Payment Receive"><?php echo number_format($balanceToPayRcvd); ?></td>
					<?php for($i=0;$i<count($payment_term_array);$i++)
                    {
                    ?>
                    	<td width="42" style="width:40px;color:#000;text-align:right" class="normalfnt" nowrap="nowrap"><?php echo (number_format(round($array[$i]["BLACK"]))==0?'&nbsp;':number_format(round($array[$i]["BLACK"]))); ?></td>
                    	<td width="42" style="width:40px;color:#F00;text-align:right" class="normalfnt" nowrap="nowrap"><?php echo (number_format(round($array[$i]["RED"]))==0?'&nbsp;':number_format(round($array[$i]["RED"]))) ?></td>
                    <?php 
                    }
					?>
                </tr>
                <?php
				$tot_LCAmonut				+= round($totLCAmount);
				$tot_orderQty				+= round($totOrderQty);
				$tot_DispQty				+= round($totDispQty);
				$tot_custAppDispQty 		+= round($custAppDispQty);
				$tot_dispatchAmount			+= round($dispatchAmount);
				$tot_custAppDispAmn			+= round($custAppDispAmn);
				$tot_variation				+= $variation;
				$tot_invoice				+= round($invoice);
				$tot_balToInvo				+= $balToInvo;
				$tot_advTobeSettle			+= round($advanceTobeSettled);
				$tot_dbnTobeSettle			+= round($debitTobeSettled);
				$tot_paymentRcvd			+= $totPayRcv;
				$tot_balanceToPayRcvd		+= $balanceToPayRcvd;
				
				for($j=0;$j<count($payment_term_array);$j++)
				{
					$payment_Tot_arry[$j]['BLACK'] 	+= $array[$j]['BLACK'];
					$payment_Tot_arry[$j]['RED'] 	+= $array[$j]['RED'];
				}         
				
			}
			?>
            <tr style="font-weight:bold">
                <td height="23" nowrap="nowrap" bgcolor="#FFFFFF" class="normalfnt" title="Customer Name"><b>Total</b></td>
                <td bgcolor="#FFFFFF" class="normalfntRight" title="Total LC Amount"><b><?php echo number_format($tot_LCAmonut);?></b></td>
                <td bgcolor="#FFFFFF" class="normalfntRight" title="Total Order Qty"><b><?php echo number_format($tot_orderQty);?></b></td>
                <td bgcolor="#FFFFFF" class="normalfntRight" title="Total Dispatch Qty"><b><?php echo number_format($tot_DispQty);?></b></td>
                <td bgcolor="#FFFFFF" class="normalfntRight" title="Customer Approved Dispatch Qty "><b><?php echo number_format($tot_custAppDispQty);?></b></td>
                <td bgcolor="#FFFFFF" class="normalfntRight" title="Dispatch Qty / Order Qty * 100">&nbsp;</td>
                <td bgcolor="#FFFFFF" class="normalfntRight" title="Dispatch Amount"><b><?php echo number_format($tot_dispatchAmount);?></b></td>
                <td bgcolor="#FFFFFF" class="normalfntRight" title="Customer Approved Dispatch Amount"><b><?php echo number_format($tot_custAppDispAmn);?></b></td>
                <td bgcolor="#FFFFFF" class="normalfntRight" title="Variation"><b><?php echo number_format($tot_variation);?></b></td>
                <td bgcolor="#FFFFFF" class="normalfntRight" title="Invoice Amount"><b><?php echo number_format($tot_invoice);?></b></td>
                <td bgcolor="#FFFFFF" class="normalfntRight" title="Balance To Invoice"><b><?php echo number_format($tot_balToInvo);?></b></td>
                <td bgcolor="#FFFFFF" class="normalfntRight" title="Payment Received"><b><?php echo number_format($tot_paymentRcvd);?></b></td>
                <td bgcolor="#FFFFFF" class="normalfntRight" title="Advance To be Settled"><b><?php echo ($tot_advTobeSettle==0?number_format($tot_advTobeSettle):"(".number_format($tot_advTobeSettle).")");?></b></td>
                <td bgcolor="#FFFFFF" class="normalfntRight" title="DebitNote To be Settled"><b><?php echo number_format($tot_dbnTobeSettle);?></b></td>
                <td bgcolor="#FFFFFF" class="normalfntRight" title="Balance To Payment Receive"><b><?php echo number_format($tot_balanceToPayRcvd);?></b></td>
                <?php for($i=0;$i<count($payment_term_array);$i++)
                {
                ?>
                    <td width="42" style="width:40px;color:#000;text-align:right" class="normalfnt" nowrap="nowrap"><b><?php echo (number_format(round($payment_Tot_arry[$i]['BLACK']))==0?'&nbsp;':number_format(round($payment_Tot_arry[$i]['BLACK']))); ?></b></td>
                    <td width="42" style="width:40px;color:#F00;text-align:right" class="normalfnt" nowrap="nowrap"><b><?php echo (number_format(round($payment_Tot_arry[$i]['RED']))==0?'&nbsp;':number_format(round($payment_Tot_arry[$i]['RED']))); ?></b></td>
                <?php 
                }
                ?>
			
    		</tr>
            </tbody>
          </table></td>
        </tr>
      </table>
    </td>
</tr>

 

<tr height="40">
  <td width="317" align="center" class="normalfnt" >&nbsp;</td>
  <td width="873" align="center" class="normalfntRight">&nbsp;</td>
</tr>
</table>
<?php
$body = ob_get_clean();

$mailHeader 		= "CUSTOMER AGING REPORT - BASE INVOICED DATE - ".$companyName;
$FROM_NAME 			= 'NSOFT';
$FROM_EMAIL			= '';

$cron_emails_to		= $obj_comm->getSubEmailIds($cronjobId,'','',$companyId,'TO');
$cron_emails_cc		= $obj_comm->getSubEmailIds($cronjobId,'','',$companyId,'CC');
$cron_emails_bcc	= $obj_comm->getSubEmailIds($cronjobId,'','',$companyId,'BCC');

$mail_TO			= $obj_comm->getEmailList($cron_emails_to);
$mail_CC			= $obj_comm->getEmailList($cron_emails_cc);
$mail_BCC			= $obj_comm->getEmailList($cron_emails_bcc);

insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
//sendMessage($FROM_EMAIL,$FROM_NAME,$CRON_TABLE_TO,$mailHeader,$body,$CRON_TABLE_CC,$CRON_TABLE_BCC);

echo $body;


?>
 
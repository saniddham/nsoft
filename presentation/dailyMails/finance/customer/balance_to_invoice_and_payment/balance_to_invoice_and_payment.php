<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}


 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}


.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}



.odd{background-color:#F5F5F5;}
.even{background-color:#FFFFFF;}
.mouseover {
	cursor: pointer;
}


.txtNumber{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}

.reportHeader {
	font-size: 18px;
	/*text-align:center,*/
	font-style: normal;
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
}




</style>

<style type="text/css">
#progress {
 width: 100px;   
 border: 1px solid black;
 position: relative;
 padding: 1px;
}

#percent {
 position: absolute;   
 left: 50%;
}

#bar {
 height: 15px;
 background-color: #0C0;
}
</style>
<?php
ini_set('display_errors',0);
?>
<div align="center">
  <div style="background-color:#FFF" ></div>
  <table width="100%" border="0" align="center" bgcolor="">
   
   <!----- BEGINING OF BALANCE TO INVOICE--------------------------------------------------------->
    <tr>
      <td><table width="100%"  class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                 <tr class="normalfnt"><th colspan="8" style="background:#548235;color:#FFF;font-weight:bold;text-align:left">Balance to invoice (Order qty <= Dispatch Qty)
</th></tr>
                 <tr class="normalfnt">
                  <th  style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">No</th>
                  <th  style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Customer</th>
                  <th  style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">PO No</th>
                  <th  style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center" >Order No</th>
                  <th  style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center" >Order Qty</th>
                  <th  style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center" >Last Dispatch Date</th>
                  <th  style="background:#9BC2E6;color:#FFF;font-weight:bold;text-align:center" >Balance to Invoice</th>
                  <th  style="background:#9BC2E6;color:#FFF;font-weight:bold;text-align:center"  >Due Days</th>
                </tr>
              
              <?php 
			  $i=0;
			$result1=$obj_disp_get->load_customers_bal_to_invoice_result($companyId);
			while($row1=mysqli_fetch_array($result1))
			{
				$i++;
				$CUSTOMER 				= $row1['CUSTOMER'];
				$CUSTOMER_PO			= $row1['CUSTOMER_PO'];
				$ORDER_NO				= $row1['ORDER_YEAR']."/".$row1['ORDER_NO'];
				$TOT_ORDER_QTY 			= $row1['TOT_ORDER_QTY'];
				$TOT_DISP_QTY 			= $row1['TOT_DISP_QTY'];
				$CUST_APP_DISP_QTY 		= $row1['CUST_APP_DISP_QTY'];//FROM FINANCE
				$DISP_AMOUNT			= $row1['DISP_AMOUNT'];
				$CUST_APP_DISP_AMOUNT	= $row1['CUST_APP_DISP_AMOUNT'];//FROM FINANCE
				$VARIATION 				= $row1["VARIATION"];
				$INVOICE_AMOUNT 		= $row1["INVOICE_AMOUNT"];
				$BAL_TO_INV				= $row1["BAL_TO_INV"];
				$DIFF_DATES				= $row1["DIFF_DATES"];
				$DISPATCH_DATE			= $row1["DISPATCH_DATE"];
			  ?>
                 <tr class="normalfnt">
                  <td class="normalfnt" ><?php echo $i ; ?>. </td>
                  <td class="normalfnt" nowrap="nowrap" ><?php echo $CUSTOMER ; ?></td>
                  <td class="normalfnt" ><?php echo $CUSTOMER_PO ; ?></td>
                  <td class="normalfnt" ><?php echo $ORDER_NO ; ?></td>
                  <td class="normalfntRight" ><?php echo $TOT_ORDER_QTY ; ?></td>
                  <td class="normalfntMid" ><?php echo $DISPATCH_DATE ; ?></td>
                  <td class="normalfntRight" ><?php echo $BAL_TO_INV ; ?></td>
                  <td class="normalfntRight" ><?php echo $DIFF_DATES ; ?></td>
                </tr>
              <?php
			}
			$bal_to_inv	=$i;
			?>
              </table></td>
   </tr>
   <!----- END OF BALANCE TO INVOICE--------------------------------------------------------->
   <tr height="50"><td></td></tr>
   <!----- BEGINING OF BALANCE TO PAYMENTS--------------------------------------------------------->
    <tr>
      <td><table width="100%"   class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                 <tr class="normalfnt"><th colspan="8" style="background:#DC879C;color:#FFF;font-weight:bold;text-align:left">None-Moving production orders to invoice (over 1 month)

</th></tr>
                 <tr class="normalfnt">
                  <th  style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">No</th>
                  <th  style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Customer</th>
                  <th  style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">PO No</th>
                  <th  style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center" >Order No</th>
                  <th  style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center" >Order Qty</th>
                  <th  style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center" >Last Dispatch Date</th>
                  <th  style="background:#9BC2E6;color:#FFF;font-weight:bold;text-align:center" >Balance to Invoice</th>
                  <th  style="background:#9BC2E6;color:#FFF;font-weight:bold;text-align:center"  >Due Days</th>
                </tr>
              
              <?php 
			  $j=0;
			$result1=$obj_disp_get->load_customers_bal_to_invoice_none_moving_result($companyId);
			while($row1=mysqli_fetch_array($result1))
			{
				$j++;
				$CUSTOMER 				= $row1['CUSTOMER'];
				$CUSTOMER_PO			= $row1['CUSTOMER_PO'];
				$ORDER_NO				= $row1['ORDER_NO']."/".$row1['ORDER_YEAR'];
				$TOT_ORDER_QTY 			= $row1['TOT_ORDER_QTY'];
				$TOT_DISP_QTY 			= $row1['TOT_DISP_QTY'];
				$CUST_APP_DISP_QTY 		= $row1['CUST_APP_DISP_QTY'];//FROM FINANCE
				$DISP_AMOUNT			= $row1['DISP_AMOUNT'];
				$CUST_APP_DISP_AMOUNT	= $row1['CUST_APP_DISP_AMOUNT'];//FROM FINANCE
				$VARIATION 				= $row1["VARIATION"];
				$INVOICE_AMOUNT 		= $row1["INVOICE_AMOUNT"];
				$BAL_TO_INV				= $row1["BAL_TO_INV"];
				$DIFF_DATES				= $row1["DIFF_DATES"];
				$DISPATCH_DATE			= $row1["DISPATCH_DATE"];
			  ?>
                 <tr class="normalfnt">
                  <td class="normalfnt" ><?php echo $j ; ?>. </td>
                  <td class="normalfnt" nowrap="nowrap" ><?php echo $CUSTOMER ; ?></td>
                  <td class="normalfnt" ><?php echo $CUSTOMER_PO ; ?></td>
                  <td class="normalfnt" ><?php echo $ORDER_NO ; ?></td>
                  <td class="normalfntRight" ><?php echo $TOT_ORDER_QTY ; ?></td>
                  <td class="normalfntMid" ><?php echo $DISPATCH_DATE ; ?></td>
                  <td class="normalfntRight" ><?php echo $BAL_TO_INV ; ?></td>
                  <td class="normalfntRight" ><?php echo $DIFF_DATES ; ?></td>
                </tr>
              <?php
			}
			$noneMoving	=$j;
			?>
              </table></td>
   </tr>
   <!----- END OF BALANCE TO PAYMENTS--------------------------------------------------------->
  </table>
</div>

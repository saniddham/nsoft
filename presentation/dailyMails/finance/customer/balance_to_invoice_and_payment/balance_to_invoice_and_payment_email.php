<?php
session_start();

date_default_timezone_set('Asia/Colombo');
ini_set('max_execution_time', 11111111) ;
//ini_set('display_errors',1);

$mainPath 			= $_SESSION['mainPath'];
$thisFilePath 		= $_SERVER['PHP_SELF'];

$toCurrency 		= 1;//1=USD,2=LKR
$currencyName		= $obj_comm->getCurrencyName($toCurrency);

$thisFilePath 		=  $_SERVER['PHP_SELF'];
include_once $_SESSION['ROOT_PATH']."class/tables/sys_report_cluster_types.php";					$sys_report_cluster_types	= new sys_report_cluster_types($db);
include_once $_SESSION['ROOT_PATH']."class/tables/mst_companies.php";								$mst_companies				= new mst_companies($db);
include_once $_SESSION['ROOT_PATH']."class/dailyMails/dispatch/cls_customer_aging_report_get.php";	$obj_disp_get			= new cls_customer_aging_report_get($db);
 	$records=0;
	$cluster_results = $sys_report_cluster_types->getReportTypeWiseDetails('BALANCE TO INVOICE/BALANCE TO PAYMENT');
	while($row_cluster	= mysqli_fetch_array($cluster_results))
	{
 		$records++;
		$body			= '';
		$companyId		= $row_cluster["COMPANY_ID"];
		$mst_companies->set($companyId);	#set company id to mst_companies class object.
		$companyName	= $mst_companies->getstrName();
 
 		$body			= '';
 		ob_start();
			include 'balance_to_invoice_and_payment_header.php';
		$body			.= ob_get_clean();

		ob_start();
 			 	include 'balance_to_invoice_and_payment.php';
		$body		 	.= ob_get_clean();
		 
        echo $body;
		//---------
 		$nowDate 		= date('Y-m-d');
  		$mailHeader		= "BALANCE TO INVOICE REPORT (".$companyName.") - ($nowDate)";
		$FROM_NAME		= "NSOFT";
		$FROM_EMAIL		= '';
		
		$toEmails 		= $obj_comm->getEmailList($row_cluster['TO']);
		$cc 			= $obj_comm->getEmailList($row_cluster['CC']);
		$bcc 			= $obj_comm->getEmailList($row_cluster['BCC']);
		
		$mail_TO		= $toEmails.",".$obj_comm->getEmailList($CRON_TABLE_TO);
		$mail_CC		= $cc.",".$obj_comm->getEmailList($CRON_TABLE_CC);
		$mail_BCC		= $bcc.",".$obj_comm->getEmailList($CRON_TABLE_BCC);
		
		if($bal_to_inv!=0 || $noneMoving!=0)
		insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);

	}

  ?>
 
<?php
session_start();
//BEGIN - SET PARAMETERS	{
$deci				= 2;
//END 	- SET PARAMETERS	}

//BEGIN - INCLUDE FILES {
 
$days					= GetSysDays();
$res					= $obj_comm->loadLocationName(2,'RunQuery');
$row 					= mysqli_fetch_array($res);

ob_start();
?>

<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}


 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}


.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}



.odd{background-color:#F5F5F5;}
.even{background-color:#FFFFFF;}
.mouseover {
	cursor: pointer;
}


.txtNumber{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}

.reportHeader {
	font-size: 18px;
	/*text-align:center,*/
	font-style: normal;
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
}
</style>

<style type="text/css">
#progress {
 width: 100px;   
 border: 1px solid black;
 position: relative;
 padding: 1px;
}

#percent {
 position: absolute;   
 left: 50%;
}

#bar {
 height: 15px;
 background-color: #0C0;
}
</style>
<table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td width="8%" class="normalfnt"><img style="vertical-align:" src="http://accsee.com/screenline.jpg"  /></td>
            <td align="center" valign="top" width="75%" class="topheadBLACK"><?php
/*	 $SQL = "SELECT
*
FROM
mst_companies
Inner Join mst_country ON mst_companies.intCountryId = mst_country.intCountryID
WHERE
mst_companies.intId =  '1'
;";
*/	
	$result 			= $db->RunQuery($SQL);
	$result				= $obj_comm->loadLoctionComanyDetails(2,'RunQuery');
	$row 				= mysqli_fetch_array($result);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];
	
	?>
              <b><?php echo $companyName.""; ?></b>
              <p class="normalfntMid">
                <?php // (".$locationName.") --> Edited By Lasantha 14th of May 2012?>
                <?php echo $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry."."."<br><b>Tel : </b>".$companyPhone." <b>Fax : </b>".$companyFax." <br><b>E-Mail : </b>".$companyEmail." <b>Web : </b>".$companyWeb;?></p></td>
            <td width="9%" class="tophead"><img src="http://accsee.com/nsoft_logo.png" style="vertical-align:"  /></td>
          </tr>
</table>
  <div style="background-color:#FFF;" align="center"><strong>Customer Invoice [<?php echo $days?> Days] Delay Report</strong><strong></strong></div>
<table width="1100" border="0" align="center" bgcolor="#FFFFFF">
 <tr>
   <td><table  border="0" cellpadding="0" cellspacing="0" class="bordered" width="100%">
     <thead>
       <tr valign="bottom">
         <th>Company</th>
         <th>Customer</th>
         <th>PONo</th>
         <th>Order No</th>
         <th>Order Date</th>
         <th>Graphic No's</th>
         <th>Order Qty</th>
         <th>Dispatch Qty</th>
         <th>Rate</th>
         <th>Dispatched Amount</th>
         <th>Invoiced Amount</th>
         <th>Balance To Invoice</th>
         <th>Last Dispatch Date</th>
         <th>Invoice Due Dates</th>
         </tr>
       </thead>
     <tbody>
  <?php
$result = GetMainDetails($days);
while($row = mysqli_fetch_array($result))
{
	$progress 			= (round((round($row["DISPATCH_QTY"])/round($row["ORDER_QTY"]))*100,2)>100?100:round((round($row["DISPATCH_QTY"])/round($row["ORDER_QTY"]))*100,2));
?>
       <tr>
         <td nowrap="nowrap" title="Customer Name" ><?php echo $row["COMPANY_NAME"]?></td>
         <td nowrap="nowrap" title="Customer Name" ><?php echo $row["CUSTOMER_NAME"]?></td>
         <td nowrap="nowrap" title="PONo"><?php echo $row["CUSTOMER_PONO"]?></td>
         <td nowrap="nowrap" title="Order No"><?php echo $row["ORDER_NO"]?></td>
         <td nowrap="nowrap" title="Order Date"><?php echo $row["ORDER_DATE"]?></td>
         <td nowrap="nowrap" title="Graphic No's"><?php echo $row["GRAPHIC_NO"]?></td>
         <td nowrap="nowrap" style="text-align:right" title="Order Qty"><?php echo number_format($row["ORDER_QTY"])?></td>
         <td nowrap="nowrap" style="text-align:right" title="Dispatch Qty"><?php echo number_format($row["DISPATCH_QTY"])?></td>
         <td nowrap="nowrap" style="text-align:right" title="Rate"><?php echo number_format($row["ORDER_RATE"],4)?></td>
         <td nowrap="nowrap" style="text-align:right" title="Dispatched Amount"><?php echo number_format($row["DISPATCH_AMOUNT"],$deci)?></td>
         <td nowrap="nowrap" style="text-align:right" title="Invoiced Amount"><?php echo number_format($row["INVOICE_AMOUNT"],$deci)?></td>        
         <td nowrap="nowrap" style="text-align:right" title="Balance To Invoice"><?php echo number_format($row["BALANCE_TO_INVOICE"],$deci)?></td>
         <td nowrap="nowrap" style="text-align:right" title="Last Dispatch Date"><?php echo $row["DISPATCH_DATE"]?></td>
         <td nowrap="nowrap" style="text-align:right;color:#F00" title="Credit Term"><?php echo $row["DIFF_DATES"]?></td>
         </tr>
  <?php
$tot_order_qty						+= round($row["ORDER_QTY"]);
$tot_dispatch_qty					+= round($row["DISPATCH_QTY"]);
$tot_dispatched_amount 				+= round($row["DISPATCH_AMOUNT"],$deci);
$tot_invoiced_amount 				+= round($row["INVOICE_AMOUNT"],$deci);
$tot_payment_received_amount		+= round($row["PAYMENT_RECEIVED_AMOUNT"],$deci);
$tot_credit_note_amount				+= round($row["CREDIT_NOTE_AMOUNT"],$deci);
$tot_debit_note_amount				+= round($row["DEBIT_NOTE_AMOUNT"],$deci);
$tot_advance_not_settled		    += round($row["ADVANCE_NOT_SETTLED"],$deci);
$tot_advance_settled				+= round($row["ADVANCE_SETTLED"],$deci);
$tot_balance_to_invoice				+= round($row["BALANCE_TO_INVOICE"],$deci);
$tot_balance_to_payment_received	+= round($row["BALANCE_TO_PAYMENT_RECEIVED"],$deci);
}
?>
  </tbody>
  <tfoot>
    <tr style="font-weight:bold;text-align:right">
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap"><?php echo number_format($tot_order_qty)?></td>
      <td nowrap="nowrap"><?php echo number_format($tot_dispatch_qty)?></td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap"><?php echo number_format($tot_dispatched_amount,$deci)?></td>
      <td nowrap="nowrap"><?php echo number_format($tot_invoiced_amount,$deci)?></td>
      <td nowrap="nowrap"><?php echo number_format($tot_balance_to_invoice,$deci)?></td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      </tr>
    <tr style="color:#F00;font-weight:bold;text-align:right">
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      </tr>
    </tfoot>  
     </table></td>
</tr>
            <tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
<?php
$body = ob_get_clean();
echo $body;

$mailHeader 	= "CUSTOMER INVOICE BALANCE REPORT";
$FROM_NAME 		= 'NSOFT - SCREENLINE HOLDINGS.';
$FROM_EMAIL		= '';

$mail_TO		= $obj_comm->getEmailList($CRON_TABLE_TO);
$mail_CC		= $obj_comm->getEmailList($CRON_TABLE_CC);
$mail_BCC		= $obj_comm->getEmailList($CRON_TABLE_BCC);

insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);

//sendMessage($FROM_EMAIL,$FROM_NAME,$CRON_TABLE_TO,$mailHeader,$body,$CRON_TABLE_CC,$CRON_TABLE_BCC);

function GetMainDetails($days)
{
	global $db;
	global $deci;
	
	$sql = "SELECT 
	SUB_1.*, 
	ROUND((DISPATCH_AMOUNT1),2) AS DISPATCH_AMOUNT, 
	ROUND((DISPATCH_AMOUNT1),2) - (INVOICE_AMOUNT) AS BALANCE_TO_INVOICE,
	ROUND((ORDER_AMOUNT/ORDER_QTY),2) AS ORDER_RATE 
	FROM (
		SELECT 
		C.strName	AS COMPANY_NAME,
		OH.intCurrency AS CURRENCY_ID, 
		CU.intId AS CUSTOMER_ID, 
		CU.strName AS CUSTOMER_NAME, 
		OH.dtDate AS ORDER_DATE, 
		CONCAT(OH.intOrderYear,' - ',OH.intOrderNo) AS ORDER_NO, 
		OH.strCustomerPoNo AS CUSTOMER_PONO, 
		SUM(OD.intQty) AS ORDER_QTY, 
		
		ROUND((select SUM(OD.intQty * OD3.dblPrice) from 
				trn_orderdetails as OD3  
				WHERE OD3.intOrderNo = OD.intOrderNo 
				AND OD3.intOrderYear = OD.intOrderYear
		 ),0) AS ORDER_AMOUNT,
		 
		ROUND(SUM(OD.intQty * OD.dblPrice)/SUM(OD.intQty),4) AS ORDER_RATE1,
		 
		SUBSTRING(GROUP_CONCAT(DISTINCT OD.strGraphicNo),1,20) AS GRAPHIC_NO,
		FPT.strName AS PAYMENT_TERM,
		
		ROUND(COALESCE((SELECT SUM(FDD.dblGoodQty) 
		FROM ware_fabricdispatchdetails FDD 
		INNER JOIN ware_fabricdispatchheader FDH 
		ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
		AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear 
		WHERE FDH.intOrderNo = OH.intOrderNo 
		AND FDH.intOrderYear = OH.intOrderYear 
		AND FDH.intStatus = 1),0)) AS DISPATCH_QTY, 
		
		ROUND(COALESCE((SELECT SUM(FDD.dblGoodQty * OD2.dblPrice) 
		FROM ware_fabricdispatchdetails FDD 
		INNER JOIN ware_fabricdispatchheader FDH 
		ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
		AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear 
		INNER JOIN trn_orderdetails AS OD2 
		ON FDH.intOrderNo = OD2.intOrderNo 
		AND FDH.intOrderYear = OD2.intOrderYear 
		AND FDD.intSalesOrderId = OD2.intSalesOrderId
		WHERE FDH.intOrderNo = OH.intOrderNo 
		AND FDH.intOrderYear = OH.intOrderYear 
		AND FDH.intStatus = 1),0)) AS DISPATCH_AMOUNT1, 
	
	
		SUBSTRING((SELECT GROUP_CONCAT(DISTINCT CSIH.INVOICE_NO) 
		FROM finance_customer_invoice_header CSIH 
		WHERE CSIH.ORDER_NO = OH.intOrderNo 
		AND CSIH.ORDER_YEAR = OH.intOrderYear 
		AND CSIH.STATUS = 1),1,20) AS INVOICE_NO,
		
		ROUND(COALESCE((SELECT SUM(VALUE) FROM finance_customer_transaction FCT 
		WHERE FCT.ORDER_NO = OH.intOrderNo 
		AND FCT.ORDER_YEAR = OH.intOrderYear 
		AND DOCUMENT_TYPE = 'INVOICE'),0),2) AS INVOICE_AMOUNT, 	
		
		(SELECT DATE(FDH.dtmdate) FROM ware_fabricdispatchdetails FDD 
		INNER JOIN ware_fabricdispatchheader FDH ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
		AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear 
		WHERE FDH.intOrderNo = OH.intOrderNo AND FDH.intOrderYear = OH.intOrderYear AND FDH.intStatus = 1 
		ORDER BY DATE(FDH.dtmdate) DESC LIMIT 1) AS DISPATCH_DATE, 
		
		DATEDIFF(NOW(),(SELECT DATE(FDH.dtmdate)
		FROM ware_fabricdispatchdetails FDD 
		INNER JOIN ware_fabricdispatchheader FDH ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
		AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear 
		WHERE FDH.intOrderNo = OH.intOrderNo AND FDH.intOrderYear = OH.intOrderYear AND FDH.intStatus = 1 
		ORDER BY DATE(FDH.dtmdate) DESC LIMIT 1)) AS DIFF_DATES 
		
		FROM trn_orderheader OH 
		INNER JOIN trn_orderdetails OD ON OD.intOrderNo = OH.intOrderNo AND OD.intOrderYear = OH.intOrderYear 
		INNER JOIN mst_customer CU ON CU.intId = OH.intCustomer 
		INNER JOIN mst_financepaymentsterms FPT ON FPT.intId = OH.intPaymentTerm 
		INNER JOIN mst_locations L ON L.intId = OH.intLocationId
		INNER JOIN mst_companies C ON C.intId = L.intCompanyId
		WHERE 1 = 1 
		AND OH.intStatus NOT IN  ('-2','-10')
		GROUP BY OH.intOrderYear,OH.intOrderNo 
		HAVING 1=1 
		AND ROUND((ROUND(DISPATCH_QTY)/ROUND(ORDER_QTY))*100,2) >= 100 
		ORDER BY DIFF_DATES) AS SUB_1
	HAVING 1=1
	AND BALANCE_TO_INVOICE > 0
	AND DIFF_DATES > $days";
	//die($sql);
	return $db->RunQuery($sql);
}

function GetSysDays()
{
	global $db;
	
	$sql = "SELECT 
				CUSTOMER_INVOICE_DELAY_DAYS AS FIELD
			FROM sys_config 
			WHERE intCompanyId = 1";
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	return $row["FIELD"];
}
?>

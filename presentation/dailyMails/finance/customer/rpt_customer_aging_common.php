<?php
/**
 * Created by PhpStorm.
 * User: saniddha
 * Date: 5/6/19
 * Time: 4:10 PM
 */

//ini_set('display_errors',1);
session_start();

date_default_timezone_set('Asia/Colombo');
ini_set('max_execution_time', 11111111) ;

$SCREENLINE_COMPANY = 1;
$IDS_COMPANY = 43;
$SCREENLINE_LOCATION = 2;
$IDS_LOCATION = 121;

function GetCustomers($companyId)
{
    global $db;

    $sql = "SELECT OH.intCustomer AS customerId,
			MC.strName AS customerName,
			FPT.strName AS custmer_payment_term
			FROM trn_orderheader OH
			INNER JOIN mst_customer MC ON MC.intId = OH.intCustomer
			INNER JOIN mst_financepaymentsterms FPT ON FPT.intId = OH.intPaymentTerm
			INNER JOIN mst_locations LO ON LO.intId = OH.intLocationId
			WHERE LO.intCompanyId = $companyId
			GROUP BY OH.intCustomer
			ORDER BY MC.strName ";
//	echo $sql;
    $result = $db->RunQuery($sql);
    return $result;
}
function getAdvanceTobeSettledAmount($customerId,$companyId)
{
    global $db;

    $sql = "SELECT COALESCE(SUM(FCT.VALUE)*-1,0) AS ADVANCE_TO_SETTLE
			FROM finance_customer_transaction FCT
			WHERE 
			FCT.CUSTOMER_ID = '$customerId' AND
			FCT.DOCUMENT_TYPE = 'ADVANCE' AND
			FCT.INVOICE_NO IS NULL AND
			FCT.INVOICE_YEAR IS NULL AND
			FCT.COMPANY_ID = '$companyId' ";

    $result = $db->RunQuery($sql);
    $row 	= mysqli_fetch_array($result);

    return $row['ADVANCE_TO_SETTLE'];
}
function getDebitTobeSettledAmount($customerId,$companyId)
{
    global $db;

    $sql = "SELECT COALESCE(SUM(FCT.VALUE),0) AS DEBIT_TO_SETTLE
			FROM finance_customer_transaction FCT
			WHERE
			FCT.CUSTOMER_ID = '$customerId' AND
			FCT.DOCUMENT_TYPE IN ('DEBIT','DEBIT_RECEIVE') AND
			FCT.COMPANY_ID = '$companyId' ";

    $result = $db->RunQuery($sql);
    $row 	= mysqli_fetch_array($result);

    return $row['DEBIT_TO_SETTLE'];
}

$companyId = $IDS_COMPANY;
$locationId = $IDS_LOCATION;
include 'rpt_customer_invoice_aging_report.php';


  ?>

<?php
$cronjobId	=25;

session_start();

//BEGIN - SET PARAMETERS	{
$backwardseperator = "../../../../";
$companyId 			= $_SESSION['headCompanyId'];
$deci				= 2;
$laskWeek			= date('Y-M-d',(strtotime('-7 day',strtotime(date('Y-m-d')))));
$today				= date('Y-M-d');
//END 	- SET PARAMETERS	}

ob_start();
?>
<style type="text/css">
.break {
	page-break-before: always;
}
 @media print {
.noPrint {
	display: none;
}
}
#apDiv1 {
	position: absolute;
	left: 272px;
	top: 511px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
table .bordered {
 *border-collapse: collapse; /* IE7 and lower */
	border-spacing: 0;
	width: 100%;
}
.bordered {
	border: solid #ccc 1px;
	/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
	/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size: 11px;
	font-family: Verdana;
}
.bordered tr:hover {
	background: #fbf8e9;
	/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
	transition: all 0.1s ease-in-out;
}
.bordered td {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 2px;
}
.bordered th {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 4px;
	text-align: center;
}
.bordered th {
	background-color: #dce9f9;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
	background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -moz-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -ms-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -o-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: linear-gradient(top, #ebf3fc, #dce9f9);
	/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
	box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;
	border-top: none;
	text-shadow: 0 1px 0 rgba(255,255,255,.5);
}
.bordered td:first-child, .bordered th:first-child {
	border-left: none;
}
.bordered th:first-child {
	/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
	border-radius: 6px 0 0 0;
}
.bordered th:last-child {
	/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
	border-radius: 0 6px 0 0;
}
.bordered th:only-child {
	/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
	border-radius: 6px 6px 0 0;
}
.bordered tr:last-child td:first-child {
	/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
	border-radius: 0 0 0 6px;
}
.bordered tr:last-child td:last-child {
	/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
	border-radius: 0 0 6px 0;
}
.bordered {
	border: solid #ccc 1px;
	/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
	/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size: 11px;
	font-family: Verdana;
}
.bordered tr:hover {
	background: #fbf8e9;
	/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
	transition: all 0.1s ease-in-out;
}
.bordered td {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 2px;
}
.bordered th {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 4px;
	text-align: center;
}
.bordered th {
	background-color: #dce9f9;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
	background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -moz-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -ms-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -o-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: linear-gradient(top, #ebf3fc, #dce9f9);
	/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
	box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;
	border-top: none;
	text-shadow: 0 1px 0 rgba(255,255,255,.5);
}
.bordered td:first-child, .bordered th:first-child {
	border-left: none;
}
.bordered th:first-child {
	/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
	border-radius: 6px 0 0 0;
}
.bordered th:last-child {
	/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
	border-radius: 0 6px 0 0;
}
.bordered th:only-child {
	/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
	border-radius: 6px 6px 0 0;
}
.bordered tr:last-child td:first-child {
	/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
	border-radius: 0 0 0 6px;
}
.bordered tr:last-child td:last-child {
	/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
	border-radius: 0 0 6px 0;
}
.odd {
	background-color: #F5F5F5;
}
.even {
	background-color: #FFFFFF;
}
.mouseover {
	cursor: pointer;
}
.txtNumber {
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align: right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText {
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align: left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: right;
}
.reportHeader {
	font-size: 18px;
	/*text-align:center,*/
	font-style: normal;
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
}
</style>
<table width="100%" cellpadding="0" cellspacing="0">
  <tr>
    <td width="8%" class="normalfnt"><img style="vertical-align:" src="http://accsee.com/screenline.jpg"  /></td>
    <td align="center" valign="top" width="75%" class="topheadBLACK"><?php
	$result 			= $db->RunQuery($SQL);
	$result				= $obj_comm->loadLoctionComanyDetails(2,'RunQuery');
	$row 				= mysqli_fetch_array($result);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];
	
	?>
      <b><?php echo $companyName.""; ?></b>
      <p class="normalfntMid">
        <?php echo $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry."."."<br><b>Tel : </b>".$companyPhone." <b>Fax : </b>".$companyFax." <br><b>E-Mail : </b>".$companyEmail." <b>Web : </b>".$companyWeb;?></p></td>
    <td width="9%" class="tophead"><img src="http://accsee.com/nsoft_logo.png" style="vertical-align:"  /></td>
  </tr>
</table>
<div align="center" style="font-family:Verdana, Geneva, sans-serif;font-size:18;"><strong>Customer Completed Graphic List</strong></div>
<div align="center" style="font-family:Verdana, Geneva, sans-serif;font-size:11;"><strong>Last Dispatch Date Between <?php echo $laskWeek .' and '. $today?></strong></div>
<table width="1100" border="0" align="center" bgcolor="#FFFFFF">
  <tr>
    <td><table  border="0" cellpadding="0" cellspacing="0" class="bordered" width="100%">
        <thead>
          <tr valign="bottom">
            <th width="12%">Customer Name</th>
            <th width="14%">Customer Location</th>
            <th width="14%">Production Location</th>
            <th width="5%">PONo</th>
            <th width="8%">Order No</th>
            <th width="8%">Order Date</th>
            <th width="10%">Invoice No</th>
            <th width="13%">Graphic No's</th>
            <th width="9%">Order Qty</th>
            <th width="9%">Dispatch Qty</th>
            <th width="12%">Last Dispatch Date</th>
          </tr>
        </thead>
        <tbody>
          <?php
$result = GetMainDetails();
while($row = mysqli_fetch_array($result))
{
	$progress 			= (round((round($row["DISPATCH_QTY"])/round($row["ORDER_QTY"]))*100,2)>100?100:round((round($row["DISPATCH_QTY"])/round($row["ORDER_QTY"]))*100,2));
?>
          <tr>
            <td nowrap="nowrap" title="Customer Name" ><?php echo $row["CUSTOMER_NAME"]?></td>
            <td nowrap="nowrap" title="PONo"><?php echo $row["CUSTOMER_LOCATION"]?></td>
            <td nowrap="nowrap" title="PONo"><?php echo $row["PRODUCTION_LOCATION"]?></td>
            <td nowrap="nowrap" title="PONo"><?php echo $row["CUSTOMER_PONO"]?></td>
            <td nowrap="nowrap" title="Order No"><?php echo $row["ORDER_NO"]?></td>
            <td nowrap="nowrap" style="text-align:center" title="Order Date"><?php echo $row["ORDER_DATE"]?></td>
            <td nowrap="nowrap" title="Invoice No"><?php echo ($row["INVOICE_NO"]==""?'&nbsp;':$row["INVOICE_NO"])?></td>
            <td nowrap="nowrap" title="Graphic No's"><?php echo $row["GRAPHIC_NO"]?></td>
            <td nowrap="nowrap" style="text-align:right" title="Order Qty"><?php echo number_format($row["ORDER_QTY"])?></td>
            <td nowrap="nowrap" style="text-align:right" title="Dispatch Qty"><?php echo number_format($row["DISPATCH_QTY"])?></td>
            <td nowrap="nowrap" style="text-align:center" title="Last Dispatch Date"><?php echo $row["DISPATCH_DATE"]?></td>
          </tr>
          <?php
$tot_order_qty						+= round($row["ORDER_QTY"]);
$tot_dispatch_qty					+= round($row["DISPATCH_QTY"]);
$tot_dispatched_amount 				+= round($row["DISPATCH_AMOUNT"],$deci);
$tot_invoiced_amount 				+= round($row["INVOICE_AMOUNT"],$deci);
$tot_payment_received_amount		+= round($row["PAYMENT_RECEIVED_AMOUNT"],$deci);
$tot_credit_note_amount				+= round($row["CREDIT_NOTE_AMOUNT"],$deci);
$tot_debit_note_amount				+= round($row["DEBIT_NOTE_AMOUNT"],$deci);
$tot_advance_not_settled		    += round($row["ADVANCE_NOT_SETTLED"],$deci);
$tot_advance_settled				+= round($row["ADVANCE_SETTLED"],$deci);
$tot_balance_to_invoice				+= round($row["BALANCE_TO_INVOICE"],$deci);
$tot_balance_to_payment_received	+= round($row["BALANCE_TO_PAYMENT_RECEIVED"],$deci);
}
?>
        </tbody>
        <tfoot>
          <tr style="font-weight:bold;text-align:right">
            <td colspan="8" nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap"><?php echo number_format($tot_order_qty)?></td>
            <td nowrap="nowrap"><?php echo number_format($tot_dispatch_qty)?></td>
            <td nowrap="nowrap">&nbsp;</td>
          </tr>
        </tfoot>
      </table></td>
  </tr>
  <tr>
  <tr height="40">
    <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y-m-d") ?></strong></span></td>
  </tr>
</table>
<?php
$body = ob_get_clean();
echo $body;
$mailHeader 		= "CUSTOMER COMPLETED GRAPHIC REPORT";
$FROM_NAME 			= 'NSOFT - SCREENLINE HOLDINGS.';
$FROM_EMAIL			= '';

$cron_emails_to		=$obj_comm->getCronjobEmailIds($cronjobId,'TO');
$cron_emails_cc		=$obj_comm->getCronjobEmailIds($cronjobId,'CC');
$cron_emails_bcc	=$obj_comm->getCronjobEmailIds($cronjobId,'BCC');

$mail_TO			= $obj_comm->getEmailList($cron_emails_to);
$mail_CC			= $obj_comm->getEmailList($cron_emails_cc);
$mail_BCC			= $obj_comm->getEmailList($cron_emails_bcc);

insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
//sendMessage($FROM_EMAIL,$FROM_NAME,$CRON_TABLE_TO,$mailHeader,$body,$CRON_TABLE_CC,$CRON_TABLE_BCC);

function GetMainDetails()
{
	global $db;
	global $deci;
	global $companyId;
	
	$sql = "SELECT SUB_1.*
			FROM
			(SELECT
			  CU.strName														AS CUSTOMER_NAME,
			  CL.strName														AS CUSTOMER_LOCATION,
			  OH.dtDate 														AS ORDER_DATE,
			  CONCAT(OH.intOrderYear,' - ',OH.intOrderNo) 						AS ORDER_NO,
			  OH.strCustomerPoNo 												AS CUSTOMER_PONO,
			  SUM(OD.intQty)          											AS ORDER_QTY,
			  SUBSTRING(GROUP_CONCAT(DISTINCT OD.strGraphicNo),1,20)			AS GRAPHIC_NO,
			  FPT.strName        												AS PAYMENT_TERM,
			  
			  ROUND(COALESCE((SELECT SUM(FDD.dblGoodQty)
			  FROM ware_fabricdispatchdetails FDD 
			  INNER JOIN ware_fabricdispatchheader FDH 
				ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
				AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			  WHERE FDH.intOrderNo = OH.intOrderNo 
			  	AND FDH.intOrderYear = OH.intOrderYear 
				AND FDH.intStatus = 1),0))										AS DISPATCH_QTY,
			  
			  SUBSTRING((SELECT GROUP_CONCAT(DISTINCT CSIH.INVOICE_NO)
			   FROM finance_customer_invoice_header CSIH  
			   WHERE CSIH.ORDER_NO = OH.intOrderNo
			   AND CSIH.ORDER_YEAR = OH.intOrderYear
			   AND CSIH.STATUS = 1),1,20)										AS INVOICE_NO,
			   
			   (SELECT
				 DATE(FDH.dtmdate)
			   FROM ware_fabricdispatchdetails FDD
				 INNER JOIN ware_fabricdispatchheader FDH
				   ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
					 AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			   WHERE FDH.intOrderNo = OH.intOrderNo
				   AND FDH.intOrderYear = OH.intOrderYear
				   AND FDH.intStatus = 1 
			   ORDER BY DATE(FDH.dtmdate) DESC LIMIT 1)							AS DISPATCH_DATE,
			    
			   DATEDIFF(NOW(),(SELECT
				 DATE(FDH.dtmdate)
			   FROM ware_fabricdispatchdetails FDD
				 INNER JOIN ware_fabricdispatchheader FDH
				   ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
					 AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			   WHERE FDH.intOrderNo = OH.intOrderNo
				   AND FDH.intOrderYear = OH.intOrderYear
				   AND FDH.intStatus = 1 
			   ORDER BY DATE(FDH.dtmdate) DESC LIMIT 1)) 						AS DIFF_DATES,
			   
			(SELECT
			   GROUP_CONCAT(DISTINCT LO.strName)
			 FROM ware_fabricdispatchheader FDH
			   INNER JOIN mst_locations LO
				 ON LO.intId = FDH.intCompanyId
			 WHERE FDH.intStatus = 1
				 AND FDH.intOrderNo = OH.intOrderNo
				 AND FDH.intOrderYear = OH.intOrderYear) 							AS PRODUCTION_LOCATION
   
			FROM trn_orderheader OH
			INNER JOIN trn_orderdetails OD
				ON OD.intOrderNo = OH.intOrderNo
				AND OD.intOrderYear = OH.intOrderYear
			INNER JOIN mst_customer CU
				ON CU.intId = OH.intCustomer
			INNER JOIN mst_financepaymentsterms FPT
    			ON FPT.intId = OH.intPaymentTerm
			INNER JOIN mst_locations L 
				  ON L.intId = OH.intLocationId
			LEFT JOIN mst_customer_locations_header CL
				ON CL.intId = OH.intCustomerLocation
			 WHERE 1 = 1 
			 	AND OH.intStatus <> '-2' ";
				
	$sql .= "GROUP BY OH.intOrderYear,OH.intOrderNo
			 HAVING
			 	round((round(DISPATCH_QTY)/round(ORDER_QTY))*100,2) >= 100 
			 	AND DATE(DISPATCH_DATE) BETWEEN DATE(DATE_SUB(DATE(NOW()),INTERVAL 6 DAY)) AND DATE(NOW())";

	$sql .= "ORDER BY L.strName) AS SUB_1";

	return $db->RunQuery($sql);
}
?>
 
<?php
$cronjobId	=15;

session_start();
$backwardseperator = "../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];
include_once $_SESSION['ROOT_PATH']."class/dailyMails/dispatch/cls_customer_aging_report_get.php";
 
$obj_disp_get			= new cls_customer_aging_report_get($db);

$location 				= $_SESSION['CompanyID'];
$res					= $obj_comm->loadLocationName($location,'RunQuery');
$row 					= mysqli_fetch_array($res);
$locationName			= $row['strName'];
$payment_term_array 	= $obj_disp_get->GetPaymentTerm();
ob_start();
?>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}


 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}


.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}



.odd{background-color:#F5F5F5;}
.even{background-color:#FFFFFF;}
.mouseover {
	cursor: pointer;
}


.txtNumber{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}

.reportHeader {
	font-size: 18px;
	/*text-align:center,*/
	font-style: normal;
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
}




</style>

<style type="text/css">
#progress {
 width: 100px;   
 border: 1px solid black;
 position: relative;
 padding: 1px;
}

#percent {
 position: absolute;   
 left: 50%;
}

#bar {
 height: 15px;
 background-color: #0C0;
}
</style>
<table width="1250" cellpadding="0" cellspacing="0" align="center">
<tr>
<td colspan="3"></td>
</tr>
<tr>
    <td colspan="9" align="center" ><table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="tophead"><table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td width="8%" class="normalfnt"><img style="vertical-align:" src="http://accsee.com/screenline.jpg"  /></td>
            <td align="center" valign="top" width="75%" class="topheadBLACK"><?php
/*	 $SQL = "SELECT
*
FROM
mst_companies
Inner Join mst_country ON mst_companies.intCountryId = mst_country.intCountryID
WHERE
mst_companies.intId =  '1'
;";
*/	
	$result 			= $db->RunQuery($SQL);
	$result				= $obj_comm->loadLoctionComanyDetails(2,'RunQuery');
	$row 				= mysqli_fetch_array($result);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];
	
	?>
              <b><?php echo $companyName.""; ?></b>
              <p class="normalfntMid">
                <?php echo $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry."."."<br><b>Tel : </b>".$companyPhone." <b>Fax : </b>".$companyFax." <br><b>E-Mail : </b>".$companyEmail." <b>Web : </b>".$companyWeb;?></p></td>
            <td width="9%" class="tophead"><img src="http://accsee.com/nsoft_logo.png" style="vertical-align:"  /></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ></div>
<table width="1250" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td colspan="2">
  <table width="100%">
  <tr>
    <td colspan="8" align="center" class="reportHeader">CUSTOMER AGING REPORT</td>
    </tr>
  </table>
  </td>
</tr>
<tr>
  <td colspan="2">
    <table width="100%">
      <tr>
        <td>
          
          <table width="100%"  class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <thead>
              <tr class="">
                <th width="301" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Customer</th>
                <td width="63" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Total LC Amount</td>
                <td width="63" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Total Order Qty</td>
                <td width="69" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Total Dispatch Qty</td>
                <td width="107" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Customer Approved Dispatch Qty</td>
                <td width="69" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Dispatch Progress</td>
                <td width="69" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Dispatch Amount</td>
                <td width="126" style="background:#2F75B5;color:#FFF;font-weight:bold;text-align:center">Customer Approved Dispatch Amount</td>
                <td width="68" style="background:#FFDA65;color:#FFF;font-weight:bold;text-align:center">variation</td>
                <td width="58" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">Invoice</td>
                <td width="64" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">To be Invoiced</td>
                <td width="70" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">Payment Received</td>
                 <td width="70" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">Advance to be Settled</td>
                <td width="70" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">DebitNote to be Settled</td>
                <td width="86" style="background:#548235;color:#FFF;font-weight:bold;text-align:center">Balance to be Received</td>
				<?php for($i=0;$i<count($payment_term_array);$i++){?>
                <th colspan="2" style="width:40px;background:#9BC2E6;color:#FFF;font-weight:bold;text-align:center"><?php echo $payment_term_array[$i]?></th>
                <?php }?>
                </tr>
              </thead>
            <tbody>
              <?php 
			$tot_LCAmonut				= 0;
			$tot_orderQty				= 0;
			$tot_DispQty				= 0;
			$tot_custAppDispQty 		= 0;
			$tot_dispatchAmount			= 0;
			$tot_custAppDispAmn			= 0;
			$tot_variation				= 0;
			$tot_invoice				= 0;
			$tot_balToInvo				= 0;
			$tot_paymentRcvd			= 0;
			$tot_balanceToPayRcvd		= 0;
			$tot_advTobeSettle			= 0;
			$tot_dbnTobeSettle			= 0;
				
			$result1=$obj_disp_get->get_customers_delivery_rpt_result($payment_term_array,'screenLine');
			while($row1=mysqli_fetch_array($result1))
			{
				$customer 			= $row1['CUSTOMER'];
				$customer_id		= $row1['CUSTOMER_ID'];
				$totLCAmount 		= round($row1['TOT_LCAMOUNT']);
				$totOrderQty 		= round($row1['TOT_ORDER_QTY']);
				$totDispQty 		= round($row1['TOT_DISP_QTY']);
				$custAppDispQty 	= round($row1['CUST_APP_DISP_QTY']);//FROM FINANCE
				$dispatchProgress	= ($row1['DISP_PROGRESS']>100?100:$row1['DISP_PROGRESS']);//OUTER SUB
				$dispatchAmount		= round($row1['DISP_AMOUNT']);
				$custAppDispAmn		= round($row1['CUST_APP_DISP_AMOUNT']);//FROM FINANCE
				$variation 			= $dispatchAmount-$custAppDispAmn;
				$invoice 			= round($row1["INVOICE_AMOUNT"]);
				$balToInvo			= round($row1["BAL_TO_INV"]);
				$paymentRcvd		= round($row1['PAYMENT_RECEIVED_AMOUNT']);
				$balanceToPayRcvd	= $invoice-$paymentRcvd;
				
				$debitTobeSettled	= getDebitTobeSettledAmount($customer_id,1);
				$advanceTobeSettled	= getAdvanceTobeSettledAmount($customer_id,1);
				
	?>
    <tr>
        <td bgcolor="#FFFFFF" class="normalfnt" nowrap="nowrap" title="Customer Name"><?php echo $customer; ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Total LC Amount"><?php echo number_format($totLCAmount); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Total Order Qty"><?php echo number_format($totOrderQty); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Total Dispatch Qty"><?php echo number_format($totDispQty); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Customer Approved Dispatch Qty "><?php echo number_format($custAppDispQty); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Dispatch Qty / Order Qty * 100"><div id="progress"><span id="percent"><?php echo $dispatchProgress?>%</span><div id="bar" style="width:<?php echo $dispatchProgress?>px"></div></div></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Dispatch Amount"><?php echo number_format($dispatchAmount); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Customer Approved Dispatch Amount"><?php echo number_format($custAppDispAmn); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Variation"><?php echo number_format($variation); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Invoice Amount"><?php echo number_format($invoice); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Balance To Invoice"><?php echo number_format($balToInvo); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Payment Received"><?php echo number_format($paymentRcvd); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Advance To be Settled"><?php echo (round($advanceTobeSettled)==0?number_format(round($advanceTobeSettled)):"(".number_format(round($advanceTobeSettled)).")"); ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight" title="DebitNote To be Settled"><?php echo number_format(round($debitTobeSettled)); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Balance To Payment Receive"><?php echo number_format($balanceToPayRcvd); ?></td>
		<?php  
			$start = 0;
			$agingCSS = "color:#F00";
			
		for($i=0;$i<count($payment_term_array);$i++){
			$field1	= "NOT_EXCEED_TREM_QTY$i";
			$fieldN	= round($row1[$field1]);
			$j		= $i+1;
			$field2	= "EXCEED_TREM_QTY$j";
			$fieldE	= round($row1[$field2]);
			
			$tot_payment_term_array[$i]['B']	+= round($row1[$field1]);
			$tot_payment_term_array[$i]['R']	+= round($row1[$field2]);
			?>
        <td width="42" nowrap="nowrap" class="normalfnt" style="text-align:right" title="<?php echo $payment_term_array[$i]?>"><?php echo ($fieldN==0?'&nbsp;':number_format($fieldN))?></td>
        <td width="42" nowrap="nowrap" class="normalfnt"  style="text-align:right;<?php echo $agingCSS?>" title="<?php echo $payment_term_array[$i]?>"><?php echo ($fieldE==0?'&nbsp;':number_format($fieldE))?></td>
        <?php }?>
                
        <?php 
		?>
    </tr>
    <?php
				$tot_LCAmonut				+= round($totLCAmount);
				$tot_orderQty				+= round($totOrderQty);
				$tot_DispQty				+= round($totDispQty);
				$tot_custAppDispQty 		+= round($custAppDispQty);
				$tot_dispatchAmount			+= round($dispatchAmount);
				$tot_custAppDispAmn			+= round($custAppDispAmn);
				$tot_variation				+= round($variation);
				$tot_invoice				+= round($invoice);
				$tot_balToInvo				+= round($balToInvo);
				$tot_paymentRcvd			+= round($paymentRcvd);
				$tot_balanceToPayRcvd		+= round($balanceToPayRcvd);
				$tot_advTobeSettle			+= round($advanceTobeSettled);
				$tot_dbnTobeSettle			+= round($debitTobeSettled);
			}
	?>    
    <tr style="font-weight:bold">
        <td bgcolor="#FFFFFF" class="normalfnt" nowrap="nowrap" title="Customer Name"><b>Total</b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Total LC Amount"><b><?php echo number_format($tot_LCAmonut);?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Total Order Qty"><b><?php echo number_format($tot_orderQty);?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Total Dispatch Qty"><b><?php echo number_format($tot_DispQty);?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Customer Approved Dispatch Qty "><b><?php echo number_format($tot_custAppDispQty);?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Dispatch Qty / Order Qty * 100">&nbsp;</td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Dispatch Amount"><b><?php echo number_format($tot_dispatchAmount);?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Customer Approved Dispatch Amount"><b><?php echo number_format($tot_custAppDispAmn);?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Variation"><b><?php echo number_format($tot_variation);?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Invoice Amount"><b><?php echo number_format($tot_invoice);?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Balance To Invoice"><b><?php echo number_format($tot_balToInvo);?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Payment Received"><b><?php echo number_format($tot_paymentRcvd);?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Advance To be Settled"><b><?php echo ($tot_advTobeSettle==0?number_format($tot_advTobeSettle):"(".number_format($tot_advTobeSettle).")");?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="DebitNote To be Settled"><b><?php echo number_format($tot_dbnTobeSettle);?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" title="Balance To Payment Receive"><b><?php echo number_format($tot_balanceToPayRcvd);?></b></td>
		<?php for($i=0;$i<count($payment_term_array);$i++){?>
        <td width="42" nowrap="nowrap" class="normalfnt" style="text-align:right" title="<?php echo $payment_term_array[$i]?>"><b><?php echo ($tot_payment_term_array[$i]['B']==0?'&nbsp;':number_format($tot_payment_term_array[$i]['B']))?></b></td>
        <td width="42" nowrap="nowrap" class="normalfnt"  style="text-align:right;<?php echo $agingCSS?>" title="<?php echo $payment_term_array[$i]?>"><b><?php echo ($tot_payment_term_array[$i]['R']==0?'&nbsp;':number_format($tot_payment_term_array[$i]['R']))?></b></td>
    	<?php } ?>
    </tr>
               </tbody>
          </table>        </td>
        </tr>
      
      </table>
    </td>
</tr>

 

<tr height="40">
  <td width="317" align="center" class="normalfnt" >&nbsp;</td>
  <td width="873" align="center" class="normalfntRight">&nbsp;</td>
</tr>
</table>
<?php
$body = ob_get_clean();

$mailHeader 		= "CUSTOMER AGING REPORT";
$FROM_NAME 			= 'NSOFT - SCREENLINE HOLDINGS.';
$FROM_EMAIL			= '';

$cron_emails_to		= $obj_comm->getCronjobEmailIds($cronjobId,'TO');
$cron_emails_cc		= $obj_comm->getCronjobEmailIds($cronjobId,'CC');
$cron_emails_bcc	= $obj_comm->getCronjobEmailIds($cronjobId,'BCC');

$mail_TO			= $obj_comm->getEmailList($cron_emails_to);
$mail_CC			= $obj_comm->getEmailList($cron_emails_cc);
$mail_BCC			= $obj_comm->getEmailList($cron_emails_bcc);

insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
//sendMessage($FROM_EMAIL,$FROM_NAME,$CRON_TABLE_TO,$mailHeader,$body,$CRON_TABLE_CC,$CRON_TABLE_BCC);

echo $body;

function getDebitTobeSettledAmount($customer_id,$companyId)
{
	global $db;
	
	$sql = "SELECT COALESCE(SUM(FCT.VALUE),0) AS DEBIT_TO_SETTLE
			FROM finance_customer_transaction FCT
			WHERE
			FCT.CUSTOMER_ID = '$customer_id' AND
			FCT.DOCUMENT_TYPE IN ('DEBIT','DEBIT_RECEIVE') AND
			FCT.COMPANY_ID = '$companyId' ";
	
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	
	return $row['DEBIT_TO_SETTLE'];
}
function getAdvanceTobeSettledAmount($customerId,$companyId)
{
	global $db;
	
	$sql = "SELECT COALESCE(SUM(FCT.VALUE)*-1,0) AS ADVANCE_TO_SETTLE
			FROM finance_customer_transaction FCT
			WHERE 
			FCT.CUSTOMER_ID = '$customerId' AND
			FCT.DOCUMENT_TYPE = 'ADVANCE' AND
			FCT.INVOICE_NO IS NULL AND
			FCT.INVOICE_YEAR IS NULL AND
			FCT.COMPANY_ID = '$companyId' ";
	
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	
	return $row['ADVANCE_TO_SETTLE'];
}
?>
 
<?php
$cronjobId	= 48;
$orderDate	= '2016-01-01';

ob_start();
?>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}


 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}


.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}



.odd{background-color:#F5F5F5;}
.even{background-color:#FFFFFF;}
.mouseover {
	cursor: pointer;
}


.txtNumber{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}

.reportHeader {
	font-size: 18px;
	/*text-align:center,*/
	font-style: normal;
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
}




</style>

<style type="text/css">
#progress {
 width: 100px;   
 border: 1px solid black;
 position: relative;
 padding: 1px;
}

#percent {
 position: absolute;   
 left: 50%;
}

#bar {
 height: 15px;
 background-color: #0C0;
}
</style>
<table width="1250" cellpadding="0" cellspacing="0" align="center">
<tr>
<td colspan="3"></td>
</tr>
<tr>
    <td colspan="9" align="center" ><table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="tophead"><table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td width="8%" class="normalfnt"><img style="vertical-align:" src="http://accsee.com/screenline.jpg"  /></td>
            <td align="center" valign="top" width="75%" class="topheadBLACK"><?php
	
	$result				= $obj_comm->loadLoctionComanyDetails(2,'RunQuery');
	$row 				= mysqli_fetch_array($result);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];
	
	?>
              <b><?php echo $companyName.""; ?></b>
              <p class="normalfntMid">
                <?php echo $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry."."."<br><b>Tel : </b>".$companyPhone." <b>Fax : </b>".$companyFax." <br><b>E-Mail : </b>".$companyEmail." <b>Web : </b>".$companyWeb;?></p></td>
            <td width="9%" class="tophead"><img src="http://accsee.com/nsoft_logo.png" style="vertical-align:"  /></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ></div>
<table width="1250" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td colspan="2">
  <table width="100%">
  <tr>
    <td colspan="8" align="center" class="reportHeader">NON CLOSED SALES ORDERS - 100% DISPATCHED </td>
    </tr>
  </table>
  </td>
</tr>
<tr>
    <td colspan="2">
    <table width="100%" class="bordered">
    <thead>
        <tr class="">
            <th width="8%" >Order No</th>
            <th width="12%" >Sales Order No</th>
            <th width="12%" >Graphic No</th>
            <th width="8%" >Sample No</th>
            <th width="12%" >Style</th>
            <th width="10%" >Combo</th>
            <th width="10%" >Part</th>
            <th width="9%" >Print</th>
            <th width="5%" >Revision</th>
            <th width="7%" >Order Qty</th>
            <th width="7%" >Dispatched Qty</th>
        </tr>
    </thead>
    <tbody>
    <?php
	$sql = "SELECT OD.intOrderNo,
			OD.intOrderYear,
			OD.strSalesOrderNo,
			OD.strGraphicNo,
			CONCAT(OD.intSampleYear,'/',OD.intSampleNo) AS sample_no,
			OD.strStyleNo,
			OD.strCombo,
			MP.strName AS part,
			OD.strPrintName,
			OD.intRevisionNo,
			OD.intQty AS ORDER_QTY,
			SUM(FDD.dblSampleQty+FDD.dblGoodQty+FDD.dblEmbroideryQty+FDD.dblPDammageQty+FDD.dblFdammageQty+FDD.dblCutRetQty) AS DISPATCHED_QTY
			FROM ware_fabricdispatchdetails FDD
			INNER JOIN ware_fabricdispatchheader FDH ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
			AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			INNER JOIN trn_orderdetails OD ON FDH.intOrderNo = OD.intOrderNo
			AND FDH.intOrderYear = OD.intOrderYear
			AND FDD.intSalesOrderId = OD.intSalesOrderId
			INNER JOIN trn_orderheader OH ON OH.intOrderNo = OD.intOrderNo AND
			OH.intOrderYear = OD.intOrderYear
			INNER JOIN mst_part MP ON MP.intId = OD.intPart
			INNER JOIN mst_locations ML ON ML.intId = OH.intLocationId
			INNER JOIN mst_plant MPL ON MPL.intPlantId = ML.intPlant
			WHERE
			FDH.intStatus = 1 AND
			OH.intStatus = 1 AND
			IFNULL(OD.STATUS,1) <> -10 AND
			OH.dtDate >= '$orderDate' AND
			MPL.MAIN_CLUSTER_ID = 1
			GROUP BY FDH.intOrderYear,FDH.intOrderNo,OD.intSalesOrderId
			HAVING DISPATCHED_QTY >= ORDER_QTY
			ORDER BY FDH.intOrderYear,FDH.intOrderNo,OD.intSalesOrderId";
		
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
	?>
    	<tr>
         <td bgcolor="#FFFFFF" class="normalfnt" nowrap="nowrap" title="Order No"><?php echo $row['intOrderYear'].'/'.$row['intOrderNo']; ?></td>
         <td bgcolor="#FFFFFF" class="normalfnt" nowrap="nowrap" title="Sales Order No"><?php echo $row['strSalesOrderNo']; ?></td>	
         <td bgcolor="#FFFFFF" class="normalfnt" nowrap="nowrap" title="Graphic No"><?php echo $row['strGraphicNo']; ?></td>
         <td bgcolor="#FFFFFF" class="normalfnt" nowrap="nowrap" title="Sample No"><?php echo $row['sample_no']; ?></td>
         <td bgcolor="#FFFFFF" class="normalfnt" nowrap="nowrap" title="Style"><?php echo $row['strStyleNo']; ?></td>
         <td bgcolor="#FFFFFF" class="normalfnt" nowrap="nowrap" title="Combo"><?php echo $row['strCombo']; ?></td>
         <td bgcolor="#FFFFFF" class="normalfnt" nowrap="nowrap" title="Part"><?php echo $row['part']; ?></td>
         <td bgcolor="#FFFFFF" class="normalfnt" nowrap="nowrap" title="Print"><?php echo $row['strPrintName']; ?></td>
         <td bgcolor="#FFFFFF" class="normalfnt" style="text-align:right" nowrap="nowrap" title="Revision"><?php echo $row['intRevisionNo']; ?></td>	
         <td bgcolor="#FFFFFF" class="normalfnt" style="text-align:right" nowrap="nowrap" title="Order Qty"><?php echo $row['ORDER_QTY']; ?></td>
         <td bgcolor="#FFFFFF" class="normalfnt" style="text-align:right" nowrap="nowrap" title="Dispatched Qty"><?php echo $row['DISPATCHED_QTY']; ?></td>
    <?php
	}
	?>
    </tbody>
    </table>
    </td>
</tr>

 

<tr height="40">
  <td width="317" align="center" class="normalfnt" >&nbsp;</td>
  <td width="873" align="center" class="normalfntRight">&nbsp;</td>
</tr>
</table>
<?php
$body = ob_get_clean();

$mailHeader 		= "NON CLOSED SALES ORDERS - 100% DISPATCHED";
$FROM_NAME 			= 'NSOFT - SCREENLINE HOLDINGS.';
$FROM_EMAIL			= '';

$cron_emails_to		= $obj_comm->getCronjobEmailIds($cronjobId,'TO');
$cron_emails_cc		= $obj_comm->getCronjobEmailIds($cronjobId,'CC');
$cron_emails_bcc	= $obj_comm->getCronjobEmailIds($cronjobId,'BCC');

$mail_TO			= $obj_comm->getEmailList($cron_emails_to);
$mail_CC			= $obj_comm->getEmailList($cron_emails_cc);
$mail_BCC			= $obj_comm->getEmailList($cron_emails_bcc);

insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
//sendMessage($FROM_EMAIL,$FROM_NAME,$CRON_TABLE_TO,$mailHeader,$body,$CRON_TABLE_CC,$CRON_TABLE_BCC);
echo $body;
?>
 
<?php
$cronjobId	=21;
$argument	= '';
$headerLine	= '';
if($CRON_ID == 21){
	$cronjobId	= 21;
	$argument = '>='; // over due payment
	$headerLine	= '(LKR-OVERDUE)';
}
if($CRON_ID == 56){
	$cronjobId	= 56;
	$argument = '<'; //due payment
	$headerLine	= '(LKR-DUE)';
}

$reportId	=7;

session_start();
$thisFilePath 		= $_SERVER['PHP_SELF'];
ini_set('max_execution_time',300);

$currency			= 2;//convert all amount to LKR
$payment_term_array = GetPaymentTerm();

$company_result 	= GetCompanyWise();
while($row_comp = mysqli_fetch_array($company_result))
{
	ob_start();
?>

<style>

 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
#progress {
 width: 100px;   
 border: 1px solid black;
 position: relative;
 padding: 1px;
}

#percent {
 position: absolute;   
 left: 50%;
}

#bar {
 height: 15px;
 background-color: #0C0;
}
</style>
<table width="1250" border="0" align="center" bgcolor="#FFFFFF">
<tr>
	<td>
        <table width="100%" border="0" align="center">
            <tr>
                <td class="tophead">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="10%" class="normalfnt"><img style="vertical-align:" src="http://accsee.com/screenline.jpg"  /></td>
                            <?php
								$row 				= getLocation($row_comp["COMPANY_ID"]);
								$companyName		= $row["strName"];
								$locationName		= $row["locationName"];
								$companyAddress1	= $row["strAddress"];
								$companyStreet		= $row["strStreet"];
								$companyCity		= $row["strCity"];
								$companyCountry		= $row["strCountryName"];
								$companyZipCode		= $row["strZip"];
								$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
								$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
								$companyEmail		= $row["strEMail"];
								$companyWeb		 	= $row["strWebSite"];
							?>	
                            <td align="center" valign="top" width="80%" class="topheadBLACK">
                            <b><?php echo $companyName.""; ?></b>
              <p class="normalfntMid">
                <?php echo $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry."."."<br><b>Tel : </b>".$companyPhone." <b>Fax : </b>".$companyFax." <br><b>E-Mail : </b>".$companyEmail." <b>Web : </b>".$companyWeb;?></p></td>
                 <td width="10%" class="tophead" style="text-align:right"><img src="http://accsee.com/nsoft_logo.png" style="vertical-align:"  /></td>
                        </tr>
                    </table></td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td align="center"> SUPPLIER AGING <?php echo $headerLine; ?> REPORT (<b>LKR</b>)</td>
    </tr>
	
<tr>
  <td class="normalfnt"><span style="color:#F00">*</span> Note - Service Suppliers mark in Blue color.</td>
</tr>
<tr>
  <td class="normalfnt"><span style="color:#F00">*</span> Note - This report is based on supplier POs,  which have been raised on or after 2017/04/01. (POs currency is LKR only)</td>
</tr>
    <tr>
        <td>
            <table  border="0" cellpadding="0" cellspacing="0" class="bordered" width="100%">
            <thead>
                <tr>
                	<td width="339" style="background:#2F75B5;color:#FFF;text-align:center;border-radius: 6px 0 0 0;"><b>Supplier</b></td>
                    <td width="98" style="background:#2F75B5;color:#FFF;text-align:center"><b>Total Due Amount</b></td>
                    <td width="77" style="background:#2F75B5;color:#FFF;text-align:center"><b>GRN Progress</b></td>
                    <td width="87" style="background:#2F75B5;color:#FFF;text-align:center"><b>GRN Amount</b></td>
                    <td width="84" style="background:#548235;color:#FFF;text-align:center"><b>Invoice</b></td>
                    <td width="95" style="background:#548235;color:#FFF;text-align:center"><b>Balance to Invoice</b></td>
                    <td width="95" style="background:#548235;color:#FFF;text-align:center"><b>Payment</b></td>
                    <td width="113" style="background:#548235;color:#FFF;text-align:center"><b>Balance to Payment</b></td>
                    <td width="113" style="background:#2F75B5;color:#FFF;text-align:center"><b>Credit Term [Days]</b></td>
                    <?php for($i=0;$i<count($payment_term_array);$i++)
					{
						
					?>
                    	<td colspan="2" width="151" style="width:40px;background:#9BC2E6;color:#FFF;text-align:center;"><b><?php echo $payment_term_array[$i]?></b></td>
                    <?php 
					}
					?>
                    <td width="50" style="background:#9BC2E6;color:#9BC2E6;text-align:center;border-radius: 0 6px 0 0"></td>
                </tr>
            </thead>
            <tbody>
            <?php
			
				$result = GetSppliers();
				while($rowSup = mysqli_fetch_array($result))
				{
					$totPOQty			= 0;
					$totPOAmount		= 0;
					$totGRNQty			= 0;
					$totGRNAmount		= 0;
					$totInvoiceAmount	= 0;
					$totPaidAmount		= 0;
					$totBalToInvoice	= 0;
					$totBalToPaid		= 0;
					$totDueAmount		= 0;
					
					if($rowSup['intTypeId']==5)
						$supFontColor	= '#020EF9';
					else
						$supFontColor	= '#000';
					
					$result_details = getDetails($rowSup['supplierId'],$row_comp["COMPANY_ID"],$argument);
					while($rowD = mysqli_fetch_array($result_details))
					{
						$savedCurRate 		= getCurrencyRate($rowD['CURRENCY'],$rowD['PODATE'],$row_comp["COMPANY_ID"]);
						$targetCurRate  	= getCurrencyRate($currency,$rowD['PODATE'],$row_comp["COMPANY_ID"]);
						
						$POVal				= ($rowD["POVALUE"]/$targetCurRate)*$savedCurRate;
						$grnVal				= ($rowD["GRNVALUE"]/$targetCurRate)*$savedCurRate;
						$invoiceAmount		= ($rowD["INVOICEAMOUNT"]/$targetCurRate)*$savedCurRate;
						$paidAmount			= ($rowD["PAIDAMOUNT"]/$targetCurRate)*$savedCurRate;
						
						$totPOQty 			+= $rowD['POQty'];
						$totPOAmount 		+= $POVal;
						$totGRNQty 			+= $rowD['GRNQty'];
						$totGRNAmount 		+= $grnVal;
						$totInvoiceAmount 	+= $invoiceAmount;
						$totPaidAmount 		+= $paidAmount;
						$totBalToInvoice 	+= ($grnVal-$invoiceAmount);
						$totBalToPaid 		+= ($grnVal-$paidAmount);
						$start				 = -1;

						
						$amount				 = ($grnVal-$paidAmount);
						
						for($i=0;$i<count($payment_term_array);$i++)
						{
							$blackQty		= 0;
							$RedQty			= 0;
							$diff_date		= $rowD['DIFF_DATES'];
							
							if($diff_date>$start && $diff_date<=$payment_term_array[$i] && $diff_date<=$rowD['CREDITTERM'])
							{
								$array[$i]["BLACK"] += $amount;
							}
							else if($diff_date>$start && $diff_date<=$payment_term_array[$i] && $diff_date>$rowD['CREDITTERM'])
							{
								$array[$i]["RED"] += $amount;
							}
							
							$start = $payment_term_array[$i];
						}
						
					}
					$progress = ((($totGRNQty/$totPOQty)*100)>100?100:($totGRNQty/$totPOQty)*100);
					$totDueAmount = $totGRNAmount - $totPaidAmount;
					if(round($totPOAmount,0)<=0)
						continue;
					
					if(round($totBalToPaid,0)<=0)
						continue;
					
					if(round($totDueAmount,0)<=0)
						continue;
					?>
                    <tr>
                    	<td style="text-align:left;color:<?php echo $supFontColor; ?>" nowrap="nowrap"><?php echo $rowSup['supplierName']; ?></td>
                    	<td style="text-align:right;color:<?php echo $supFontColor; ?>"><?php echo number_format($totDueAmount,0); ?></td>
                    	<td nowrap="nowrap"><div id="progress"><span id="percent"><?php echo round($progress,2);?>%</span><div id="bar" style="width:<?php echo $progress?>px"></div></div></td>
                    	<td style="text-align:right;color:<?php echo $supFontColor; ?>"><?php echo number_format($totGRNAmount,0); ?></td>
                    	<td style="text-align:right;color:<?php echo $supFontColor; ?>"><?php echo number_format($totInvoiceAmount,0); ?></td>
                    	<td style="text-align:right;color:<?php echo $supFontColor; ?>"><?php echo number_format($totBalToInvoice,0); ?></td>
                    	<td style="text-align:right;color:<?php echo $supFontColor; ?>"><?php echo number_format($totPaidAmount,0); ?></td>
                    	<td style="text-align:right;color:<?php echo $supFontColor; ?>"><?php echo number_format($totBalToPaid,0); ?></td>
                    	<td style="text-align:right;color:<?php echo $supFontColor; ?>"><?php echo $rowSup["SUPPLIER_PAYMENT_TERM"]?>&nbsp;</td>
                        <?php for($i=0;$i<count($payment_term_array);$i++)
						{
						?>
							<td width="151" style="width:40px;color:#000;text-align:right" nowrap="nowrap"><?php echo (number_format($array[$i]["BLACK"])==0?'&nbsp;':number_format($array[$i]["BLACK"])); ?></td>
                            <td width="151" style="width:40px;color:#F00;text-align:right" nowrap="nowrap"><?php echo (number_format($array[$i]["RED"])==0?'&nbsp;':number_format($array[$i]["RED"])) ?></td>
						<?php 
						}
						?>
                        <td>&nbsp;</td>
                    </tr>
                    <?php
					
					$tot_PO_qty						+= $totPOQty;
					$tot_PO_amount					+= $totPOAmount;
					$tot_GRN_qty					+= $totGRNQty ;
					$tot_GRN_amount 				+= $totGRNAmount ;
					$tot_invoiced_amount 			+= $totInvoiceAmount;
					$tot_Paid_amount				+= $totPaidAmount;
					$tot_balance_to_invoice			+= $totBalToInvoice;
					$tot_balance_to_paid			+= $totBalToPaid ;
					$tot_Due_Amount					+= $totDueAmount ;
					
					for($j=0;$j<count($payment_term_array);$j++)
					{
						$payment_Tot_arry[$j]['BLACK'] 	+= $array[$j]['BLACK'];
						$payment_Tot_arry[$j]['RED'] 	+= $array[$j]['RED'];
					}
					$array	= array();
				}
			?>
            <tr>
                <td style="text-align:left" nowrap="nowrap">&nbsp;</td>
                <td style="text-align:right"><b><?php echo number_format($tot_Due_Amount,0); ?></b></td>
                <td nowrap="nowrap">&nbsp;</td>
                <td style="text-align:right"><b><?php echo number_format($tot_GRN_amount,0); ?></b></td>
                <td style="text-align:right"><b><?php echo number_format($tot_invoiced_amount,0); ?></b></td>
                <td style="text-align:right"><b><?php echo number_format($tot_balance_to_invoice,0); ?></b></td>
                <td style="text-align:right"><b><?php echo number_format($tot_Paid_amount,0); ?></b></td>
                <td style="text-align:right"><b><?php echo number_format($tot_balance_to_paid,0); ?></b></td>
                <td style="text-align:right">&nbsp;</td>
                <?php for($i=0;$i<count($payment_term_array);$i++)
                {
                ?>
                    <td width="151" style="width:40px;color:#000;text-align:right" nowrap="nowrap"><b><?php echo (number_format($payment_Tot_arry[$i]['BLACK'])==0?'&nbsp;':number_format($payment_Tot_arry[$i]['BLACK'])); ?></b></td>
                    <td width="151" style="width:40px;color:#F00;text-align:right" nowrap="nowrap"><b><?php echo (number_format($payment_Tot_arry[$i]['RED'])==0?'&nbsp;':number_format($payment_Tot_arry[$i]['RED'])); ?></b></td>
                <?php 
                }
                ?>
                <td>&nbsp;</td>
        </tr>
            </tbody>
            </table>
        </td>
    </tr>
</table>
<?php
$body = ob_get_clean();
$mailHeader 	= "SUPPLIER AGING ".$headerLine." REPORT - [".$row_comp["COMPANY_NAME"].']';
$FROM_NAME 		= 'NSOFT - SCREENLINE HOLDINGS.';
$FROM_EMAIL		= '';

//--------------------------------------------------
//$mailArray		= GetEmail_List($row_comp['COMPANY_ID']);
//sendMessage($FROM_EMAIL,$FROM_NAME,$mailArray["TO"],$mailHeader,$body,$mailArray["CC"],$mailArray["BCC"]);
//----------------

$company			=$row_comp['COMPANY_ID'];
$cron_emails_to		=$obj_comm->getCronjobEmailIds($cronjobId,'TO');
$cron_emails_cc		=$obj_comm->getCronjobEmailIds($cronjobId,'CC');
$cron_emails_bcc	=$obj_comm->getCronjobEmailIds($cronjobId,'BCC');

$sub_emails_to		=$obj_comm->getSubEmailIds($reportId,'','',$company,'TO');
$sub_emails_cc		=$obj_comm->getSubEmailIds($reportId,'','',$company,'CC');
$sub_emails_bcc		=$obj_comm->getSubEmailIds($reportId,'','',$company,'BCC');

/*$mail_TO			= $obj_comm->getEmailList($sub_emails_to).",".$obj_comm->getEmailList($cron_emails_to);
$mail_CC			= $obj_comm->getEmailList($sub_emails_cc).",".$obj_comm->getEmailList($cron_emails_cc);
$mail_BCC			= $obj_comm->getEmailList($sub_emails_bcc).",".$obj_comm->getEmailList($cron_emails_bcc);*/

$mail_TO			= $obj_comm->getEmailList($cron_emails_to);
$mail_CC			= $obj_comm->getEmailList($cron_emails_cc);
$mail_BCC			= $obj_comm->getEmailList($cron_emails_bcc);
	
//insertTable($FROM_EMAIL,$FROM_NAME,"hemanthib@nimawum.com,priyadarshana@screenlineholdings.com,krishantham@nimawum.com","testing-".$mailHeader,$body,"","");
//insertTable($FROM_EMAIL,$FROM_NAME,"hemanthib@nimawum.com,priyadarshana@screenlineholdings.com,amila@screenlineholdings.com",$mailHeader,$body,"","");
insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
 //-------------------------------------------------

echo $body;
unset($tot_PO_amount,$tot_GRN_amount,$tot_invoiced_amount,$tot_balance_to_invoice,$tot_Paid_amount,$tot_balance_to_paid,$payment_Tot_arry,$tot_Due_Amount);
}

function GetSppliers()
{
	global $db;
	
	$sql = "SELECT MS.intTypeId,POH.intSupplier AS supplierId,MS.strName AS supplierName,
			FPT.strName AS SUPPLIER_PAYMENT_TERM
			FROM trn_poheader POH
			INNER JOIN mst_supplier MS ON MS.intId = POH.intSupplier
			INNER JOIN ware_grnheader GH ON GH.intPoNo = POH.intPONo AND GH.intPoYear = POH.intPOYear
			LEFT JOIN mst_financepaymentsterms FPT ON FPT.intId = MS.intPaymentsTermsId
			WHERE GH.intStatus = 1 and
			-- MS.intTypeId NOT IN (4)
			POH.intCurrency = 2 
			GROUP BY POH.intSupplier
			ORDER BY CASE MS.intTypeId
			WHEN 5 THEN 1
			ELSE 0
			END ASC ,MS.intTypeId,MS.strName";
	
	$result = $db->RunQuery($sql);
	return $result;
}
function GetPaymentTerm()
{
	global $db;
	$i 		= 0;
	$sql 	= " SELECT DISTINCT
				strName
				FROM mst_financepaymentsterms
				WHERE strName <> 0 
				ORDER BY strName ";
	
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$new_array[$i++] = $row["strName"];
	}
	
	$new_array[$i++] = "More";
	return $new_array;
}
function getDetails($supplierId,$companyId,$argument)
{
	global $db;
	
	$sql = "	SELECT 
				ROUND(AVG(POD.dblQty)) AS POQty,
				POH.intPONo	,
				POH.intPOYear	,
				POH.intCurrency	AS CURRENCY,
				DATE(POH.dtmPODate) AS PODATE,
				
				ROUND(COALESCE((SELECT SUM(GD.dblGrnQty - GD.dblRetunSupplierQty)
				FROM ware_grndetails GD
				INNER JOIN ware_grnheader GRH ON GRH.intGrnNo=GD.intGrnNo AND GRH.intGrnYear=GD.intGrnYear
				WHERE GRH.intPoNo = POH.intPONo
				AND GRH.intPoYear = POH.intPOYear
				AND GRH.intStatus = 1
				),0)) AS GRNQty,
				ROUND(SUM((POD.dblUnitPrice*POD.dblQty)*(100-POD.dblDiscount)/100),2)+ROUND(SUM(POD.dblTaxAmmount),2) AS POVALUE1,
				
				(SELECT ROUND(SUM((PD_SUB.dblUnitPrice*PD_SUB.dblQty)*(100-PD_SUB.dblDiscount)/100),2)+ROUND(SUM(PD_SUB.dblTaxAmmount),2)
				FROM trn_podetails PD_SUB 
				WHERE  PD_SUB.intPoNo = POH.intPONo
				AND PD_SUB.intPoYear = POH.intPOYear
				GROUP BY PD_SUB.intPoNo,PD_SUB.intPoYear )AS POVALUE,
				
				COALESCE((SELECT ROUND(SUM((GD.dblGrnQty - GD.dblRetunSupplierQty) * GD.dblGrnRate),2)+ROUND(SUM((PD.dblTaxAmmount/(PD.dblUnitPrice*PD.dblQty*(100-PD.dblDiscount)/100))*((GD.dblGrnQty - GD.dblRetunSupplierQty) * GD.dblGrnRate)),2)				
				FROM ware_grndetails GD
				INNER JOIN ware_grnheader GRH ON GRH.intGrnNo=GD.intGrnNo AND GRH.intGrnYear=GD.intGrnYear
				INNER JOIN (SELECT * FROM trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem ) PD ON PD.intPONo=GRH.intPoNo AND PD.intPOYear=GRH.intPoYear AND GD.intItemId=PD.intItem
				WHERE GRH.intPoNo = POH.intPONo
				AND GRH.intPoYear = POH.intPOYear
				AND GRH.intStatus = 1),0) AS GRNVALUE,
				
				COALESCE((SELECT ROUND(SUM(ST.VALUE),2)
				FROM finance_supplier_purchaseinvoice_header SPIH
				INNER JOIN finance_supplier_transaction ST ON ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
				ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR
				WHERE ST.PO_NO = POH.intPONo
				AND ST.PO_YEAR = POH.intPOYear
				-- AND SPIH.INVOICE_NO = GH.strInvoiceNo
				AND ST.DOCUMENT_TYPE = 'INVOICE'),0) AS INVOICEAMOUNT,
				
				COALESCE((SELECT ROUND(SUM((ST.VALUE)*-1),2)
				FROM finance_supplier_purchaseinvoice_header SPIH
				INNER JOIN finance_supplier_transaction ST ON ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
				ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR
				WHERE ST.PO_NO = POH.intPONo
				AND ST.PO_YEAR = POH.intPOYear
				-- AND SPIH.INVOICE_NO = GH.strInvoiceNo
				AND ST.DOCUMENT_TYPE IN ('PAYMENT','ADVANCE')),0) AS PAIDAMOUNT	,
				
				FPT.strName AS CREDITTERM,
				GH.datdate AS GRNDATE,
				DATEDIFF(NOW(),GH.datdate) AS DIFF_DATES
			
				FROM trn_poheader POH
				INNER JOIN trn_podetails POD ON POH.intPONo = POD.intPONo AND POH.intPOYear = POD.intPOYear
				INNER JOIN ware_grnheader GH ON GH.intPoNo = POH.intPONo AND GH.intPoYear = POH.intPOYear
				INNER JOIN mst_financepaymentsterms FPT ON FPT.intId = POH.intPaymentTerm
				INNER JOIN mst_locations ML ON ML.intId=POH.intCompany
				INNER JOIN mst_supplier MS ON MS.intId = POH.intSupplier
				WHERE POH.intStatus = 1 AND
				GH.intStatus = 1 AND
				POH.PAYMENT_COMPLETED_FLAG = 0 AND
				POH.intSupplier = '$supplierId' AND
                date(POH.dtmPODate) >='2017-04-01' AND
				ML.intCompanyId = '$companyId' AND 
				-- MS.intTypeId NOT IN (4) and 
				POH.intShipmentTerm <> 4 AND 
				POH.intCurrency = 2 
				GROUP BY POH.intSupplier,ML.intCompanyId,POH.intPOYear,POH.intPONo HAVING GRNVALUE > PAIDAMOUNT 
				AND (DATEDIFF(NOW(),GH.datdate) $argument  CREDITTERM)";

	$result = $db->RunQuery($sql);
	return $result;
}
function getLocation($companyId)
{
	global $db;
	
	$sql = "SELECT
			mst_companies.strName,
			mst_locations.strName as locationName,
			mst_locations.strAddress,
			mst_locations.strStreet,
			mst_locations.strCity,
			mst_country.strCountryName,
			mst_locations.strPhoneNo,
			mst_locations.strFaxNo,
			mst_locations.strEmail,
			mst_companies.strWebSite,
			mst_locations.strZip,
			mst_companies.strVatNo,
			mst_companies.strSVatNo
			FROM
			mst_locations
			Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
			Inner Join mst_country ON mst_companies.intCountryId = mst_country.intCountryID
			WHERE
			mst_locations.intCompanyId ='$companyId'
			AND HEAD_OFFICE_FLAG = 1 ";
	
	$result = $db->RunQuery($sql);
	
	return mysqli_fetch_array($result);
}

function GetCompanyWise()
{
	global $db;
	
	$sql = "SELECT DISTINCT
			  LO.intCompanyId 	AS COMPANY_ID,
			  CO.strName	 	AS COMPANY_NAME
			FROM trn_poheader POH
			  INNER JOIN mst_supplier MS
				ON MS.intId = POH.intSupplier
			  INNER JOIN ware_grnheader GH
				ON GH.intPoNo = POH.intPONo
				  AND GH.intPoYear = POH.intPOYear
			  INNER JOIN mst_locations LO
				ON LO.intId = POH.intCompany
			  INNER JOIN mst_companies CO
			    ON CO.intId = LO.intCompanyId
			WHERE GH.intStatus = 1
				-- AND MS.intTypeId NOT IN (4) 
				AND POH.intCurrency = 2 
				AND LO.intCompanyId NOT IN (5,9) 
				AND LO.intCompanyId =1 
			GROUP BY LO.intCompanyId
			ORDER BY LO.intCompanyId";
	return $db->RunQuery($sql);
}

function GetEmailList($companyId)
{
	global $db;
	
	$sql = "SELECT
			  CT.TO,
			  CT.CC,
			  CT.BCC
			FROM sys_report_cluster_types CT
			WHERE CT.COMPANY_ID = $companyId
				AND CT.REPORT_TYPE = 'SUPPLIER_AGING_OTHER'";
	$result_ct = $db->RunQuery($sql);
	return 	mysqli_fetch_array($result_ct);
}

function GetEmail_List($companyId)
{
	global $db;
	
	$sql = "SELECT
			  CT.TO,
			  CT.CC,
			  CT.BCC
			FROM sys_report_cluster_types CT
			WHERE CT.COMPANY_ID = $companyId
				AND CT.REPORT_TYPE = 'SUPPLIER_AGING_OTHER'";
	$result_ct = $db->RunQuery($sql);
	return 	mysqli_fetch_array($result_ct);
}

function getCurrencyRate($currency,$date,$companyId)
{
	global $db;
	
	$sql = "SELECT dblExcAvgRate
			FROM mst_financeexchangerate
			WHERE intCurrencyId='$currency' AND
			dtmDate='$date' AND
			intCompanyId='$companyId' ";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);

	return $row['dblExcAvgRate'];
}

?>
 
<?php
session_start();
//print_r($_SESSION);
if($_SESSION['userId']==''){
	echo "Please log in to the ERP system thruogh a seperate tab, and then refresh this link. Don't Close this tab till downloading is completed";
	die();
}
//else
//	echo "Please wait, and don't close the tab";

include '../../../dataAccess/dbManager2.php';
$db =  new DBManager2(1);
//include "../../../dataAccess/Connector.php";
$db->connect();

require_once "../../../class/tables/sys_main_clusters.php";
$sys_main_clusters 		= new sys_main_clusters($db);

$main_cluster		= $_REQUEST['main_cluster'];
$sys_main_clusters->set($main_cluster);
$clusterName	=$sys_main_clusters->getNAME();
$year				= $_REQUEST['year'];
$month				= $_REQUEST['month'];

$month_name = date("F", mktime(0, 0, 0, $month, 10));	
 //echo "ddd";
//die();
////////////////////////////////
/////  H. B. G. KORALA  ////////
////     20-11-2015  ///////////
///////////////////////////////
ini_set('display_errors',0);
ini_set('memory_limit', '-1');
session_start();
ini_set('max_execution_time',6000000);
//$backwardseperator = "../../../";

 
//include "../../../dataAccess/Connector.php";


/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2012 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2012 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.7, 2012-05-19
 */

/** Error reporting */
//error_reporting(E_ALL);

date_default_timezone_set('Europe/London');

/** Include PHPExcel */
require_once "../../../libraries/excel/Classes/PHPExcel.php";
		
//------------------------------------------------------
 
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

$alphas = range('A', 'Z');

$style_header = array(                  
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'FFDDDD'),
                ),
                'font' => array(
                    'bold' => true,
                )
                );


// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

//$objPHPExcel->getDefaultStyle()->getFont()->setSize(15);
// Add some data
 	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue("$alphas[0]1", 'Monthly Revenue Breakdown') 
				->setCellValue("$alphas[0]2", 'Cluster'.' - '.$sys_main_clusters->getNAME()) 
				->setCellValue("$alphas[0]3", 'Month'.' - '.$year." - ".$month_name) ;
				$i=4;
 			
			
			
$objPHPExcel->getActiveSheet()->getStyle("$alphas[3]$i")->getFont()->setSize(13);
$objPHPExcel->getActiveSheet()->getStyle("$alphas[3]$i")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("$alphas[3]$i")->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
			
$objPHPExcel->getActiveSheet()->getStyle("$alphas[0]1:$alphas[0]$i")->getFont()->setSize(13);
$objPHPExcel->getActiveSheet()->getStyle("$alphas[0]1:$alphas[0]$i")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("$alphas[0]1:$alphas[0]$i")->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);

//$objPHPExcel->getActiveSheet()->getStyle("$alphas[0]1:$alphas[0]$i")->getFont()->setColor(COLOR_GREEN,true);

  
  $i+=2;
  $j=17;
	
	
 	$l=$j;

$j=0;
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($alphas[$j++].$i, 'MARKETER')
            ->setCellValue($alphas[$j++].$i, 'CUSTOMER')
			->setCellValue($alphas[$j++].$i, 'CUSTOMER LOCATION')
            ->setCellValue($alphas[$j++].$i, 'LOCATION')
            ->setCellValue($alphas[$j++].$i, 'PLANT')
            ->setCellValue($alphas[$j++].$i, 'ORDER NO') 
            ->setCellValue($alphas[$j++].$i, 'ORDER  DATE')  
            ->setCellValue($alphas[$j++].$i, 'CUSTOMER PO')  
            ->setCellValue($alphas[$j++].$i, 'TECHNIQUE GROUP')  
            ->setCellValue($alphas[$j++].$i, 'SALES ORDER NO') 
            ->setCellValue($alphas[$j++].$i, 'PRINT') 
            ->setCellValue($alphas[$j++].$i, 'PART') 
            ->setCellValue($alphas[$j++].$i, 'COMBO') 
            ->setCellValue($alphas[$j++].$i, 'SHOTS') 
            ->setCellValue($alphas[$j++].$i, 'GRAPHIC NO') 
            ->setCellValue($alphas[$j++].$i, 'STYLE NO')
            ->setCellValue($alphas[$j++].$i, 'BRAND')
            ->setCellValue($alphas[$j++].$i, 'CURRENCY')
            ->setCellValue($alphas[$j++].$i, 'ORDER QTY')
            ->setCellValue($alphas[$j++].$i, 'ORDER AMOUNT')
            ->setCellValue($alphas[$j++].$i, 'UNIT PRICE')
            ->setCellValue($alphas[$j++].$i, 'DISPATCHED QTY(GOOD)')
            ->setCellValue($alphas[$j++].$i, 'REVENUE')
            ->setCellValue($alphas[$j++].$i, '')
            ->setCellValue($alphas[$j++].$i, 'DISPATCH DATES');
			
			
			
$objPHPExcel->getActiveSheet()->getStyle("$alphas[0]$i:$alphas[$l]$i")->getFont()->setBold(true);

$p=$i;

 		
 

 	
$sql = "
		select 
		sys_users.strFullName AS MARKETER ,
		mst_customer.strName as CUSTOMER,
		mst_customer_locations_header.strName as CUSTOMER_LOCATION,
		mst_locations.strName AS LOCATION,
		mst_plant.strPlantName AS PLANT ,
		trn_orderheader.intOrderNo AS ORDER_NO,
		trn_orderheader.intOrderYear AS ORDER_YEAR,
		trn_orderheader.dtDate AS DATE,
		trn_orderheader.strCustomerPoNo AS CUSTOMER_PO,
		mst_technique_groups.TECHNIQUE_GROUP_NAME AS STECHNIQUE_GRP,
		trn_orderdetails.strSalesOrderNo AS SALES_ORDER_NO,
		
		trn_orderdetails.strGraphicNo AS GRAPHIC_NO,
		
		trn_orderdetails.strStyleNo AS STYLE,
		mst_brand.strName as BRAND ,
		 mst_financecurrency.strCode as CURRENCY,
		
		(select sum(T.intQty) from trn_orderdetails as T 
		WHERE trn_orderdetails.intOrderNo=T.intOrderNo 
		AND trn_orderdetails.intOrderYear = T.intOrderYear
		AND trn_orderdetails.intSalesOrderId = T.intSalesOrderId 
		 AND trn_orderdetails.strGraphicNo = T.strGraphicNo 
		AND trn_orderdetails.strStyleNo = T.strStyleNo 
		) AS ORDER_QTY, 
		(select sum(T.intQty*T.dblPrice) from trn_orderdetails as T 
		WHERE trn_orderdetails.intOrderNo=T.intOrderNo 
		AND trn_orderdetails.intOrderYear = T.intOrderYear
		AND trn_orderdetails.intSalesOrderId = T.intSalesOrderId 
		 AND trn_orderdetails.strGraphicNo = T.strGraphicNo 
		AND trn_orderdetails.strStyleNo = T.strStyleNo 
		) AS AMOUNT, 
		sum(if(ware_stocktransactions_fabric.strType='Dispatched_P',ware_stocktransactions_fabric.dblQty*-1,0)) AS DISPATCH_PD,
		sum(if(ware_stocktransactions_fabric.strType='Dispatched_G',ware_stocktransactions_fabric.dblQty*-1,0)) AS DISPATCH_GOOD,
		sum(if(ware_stocktransactions_fabric.strType='Dispatched_G',ware_stocktransactions_fabric.dblQty*trn_orderdetails.dblPrice*-1,0)) AS REVENUE,
		
		
		'' as x,
		group_concat(distinct if(ware_stocktransactions_fabric.strType LIKE '%Dispatched%',date(ware_stocktransactions_fabric.dtDate),'')) AS DISP_DATE,
		
		
		trn_orderheader.intCurrency,
		trn_orderdetails.dblPrice AS PRICE,
		trn_orderheader.dtDate AS ORDER_DATE ,
		trn_orderheader.intCustomer ,
		trn_orderdetails.strPrintName AS PRINT,
		trn_orderdetails.strCombo AS COMBO,
		mst_part.strName AS PART,
		(select sum(TECH.intNoOfShots) as shots from 
		trn_sampleinfomations_details_technical as TECH
		where 
		TECH.intSampleNo = trn_orderdetails.intSampleNo and 
		TECH.intSampleYear = trn_orderdetails.intSampleYear and 
		TECH.intRevNo = trn_orderdetails.intRevisionNo and 
		TECH.strComboName = trn_orderdetails.strCombo and 
		TECH.strPrintName = trn_orderdetails.strPrintName) as shots 
		 
						FROM
						ware_stocktransactions_fabric
						left JOIN mst_locations ON ware_stocktransactions_fabric.intLocationId = mst_locations.intId
						left JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId
						left JOIN trn_orderheader ON ware_stocktransactions_fabric.intOrderNo = trn_orderheader.intOrderNo AND ware_stocktransactions_fabric.intOrderYear = trn_orderheader.intOrderYear
						left JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId 
						LEFT JOIN mst_part ON mst_part.intId=trn_orderdetails.intPart
						left JOIN trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
					-- LEFT JOIN mst_customer_po_techniques ON mst_customer_po_techniques.ID = trn_orderheader.TECHNIQUE_TYPE
		left JOIN mst_marketer ON trn_orderheader.intMarketer = mst_marketer.intUserId
		left JOIN sys_users ON trn_orderheader.intMarketer = sys_users.intUserId
		left JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
		left JOIN mst_financecurrency ON trn_orderheader.intCurrency = mst_financecurrency.intId
		 left JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
		LEFT JOIN mst_technique_groups ON trn_orderdetails.TECHNIQUE_GROUP_ID= mst_technique_groups.TECHNIQUE_GROUP_ID 
		INNER JOIN mst_customer_locations_header ON trn_orderheader.intCustomerLocation = mst_customer_locations_header.intId

		WHERE  
							YEAR(ware_stocktransactions_fabric.dtDate) 	= '$year' AND MONTH(ware_stocktransactions_fabric.dtDate) 	= '$month'
					AND	ware_stocktransactions_fabric.strType 		LIKE '%Dispatched%' 
		 and mst_plant.MAIN_CLUSTER_ID = '$main_cluster'  
						
						GROUP BY 
		trn_orderheader.intOrderNo,
		trn_orderheader.intOrderYear,
		trn_orderdetails.intSalesOrderId,
		trn_orderheader.intMarketer,
		trn_sampleinfomations.intBrand,
		trn_orderdetails.strStyleNo,
		trn_orderdetails.strGraphicNo,
		ware_stocktransactions_fabric.intLocationId  
		ORDER BY
		mst_locations.strName ASC , 
		sys_users.strFullName ASC, 
		mst_customer.strName ASC, 
		trn_orderheader.intOrderNo ASC,
		trn_orderheader.intOrderYear ASC,
		trn_orderdetails.strSalesOrderNo ASC
				  ";
				  
				 // echo $sql;	
				 
$result = $db->RunQuery($sql);
$tempMain_Category='';
$tempSub_Category='';
$r=0;
$i=$p;
$arrAmmount = array();
while($row=mysqli_fetch_array($result))
{
	$i++; 
	$r++;

   $j=0;
  $l=9;
  
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue($alphas[$j++].$i, $row['MARKETER'])
				->setCellValue($alphas[$j++].$i, $row['CUSTOMER']) 
				->setCellValue($alphas[$j++].$i, $row['CUSTOMER_LOCATION']) 
				->setCellValue($alphas[$j++].$i, $row['LOCATION']) 
				->setCellValue($alphas[$j++].$i, $row['PLANT']) 
				->setCellValue($alphas[$j++].$i, $row['ORDER_YEAR'].'/'.$row['ORDER_NO'])
				->setCellValue($alphas[$j++].$i, $row['DATE'])  
				->setCellValue($alphas[$j++].$i, $row['CUSTOMER_PO'])
				->setCellValue($alphas[$j++].$i, $row['STECHNIQUE_GRP'])
				->setCellValue($alphas[$j++].$i, $row['SALES_ORDER_NO'])
				->setCellValue($alphas[$j++].$i, $row['PRINT'])
				->setCellValue($alphas[$j++].$i, $row['PART'])
				->setCellValue($alphas[$j++].$i, $row['COMBO'])
				->setCellValue($alphas[$j++].$i, $row['shots'])
				->setCellValue($alphas[$j++].$i, $row['GRAPHIC_NO']) 
				->setCellValue($alphas[$j++].$i, $row['STYLE']) 
				->setCellValue($alphas[$j++].$i, $row['BRAND']) 
				->setCellValue($alphas[$j++].$i, $row['CURRENCY'])
				->setCellValue($alphas[$j++].$i, $row['ORDER_QTY'])  
				->setCellValue($alphas[$j++].$i, $row['AMOUNT'])  
				->setCellValue($alphas[$j++].$i, $row['PRICE'])  
				->setCellValue($alphas[$j++].$i, $row['DISPATCH_GOOD'])
				->setCellValue($alphas[$j++].$i, $row['REVENUE'])
				->setCellValue($alphas[$j++].$i, '')
				->setCellValue($alphas[$j++].$i, $row['DISP_DATE']) ;
			//	->setCellValue($alphas[$j++].$i, $row['Ammount']); 
			
  //$objPHPExcel->getActiveSheet()->getStyle("E4:E$i")->getFont()->setBold(true);
}
 
$j=0;
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(15);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Monthly Revenue Breakdown - '.$mainClusterName);


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client's web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Monthly Revenue Breakdown.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

	$db->disconnect();
/*
mysqli_close($db);

*/
exit;	

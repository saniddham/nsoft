<?php
session_start();
date_default_timezone_set('Asia/Colombo');
ini_set('max_execution_time', 11111111) ;
//ini_set('display_errors',1);
$thisFilePath 			=  $_SERVER['PHP_SELF'];
 
include_once $_SESSION['ROOT_PATH']."class/dailyMails/customerRevenue/cls_customer_revenue_get.php";
include_once $_SESSION['ROOT_PATH']."class/dailyMails/customerRevenue/cls_customer_revenue_set.php";
include_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
  
$obj_revenue_get	= new cls_customer_revenue_get($db);
$obj_revenue_set	= new cls_customer_revenue_set($db);
$obj_common			= new cls_commonFunctions_get($db);
  
   
$toCurrency  		= 1;//1=USD,2=LKR

$year			 	= $_REQUEST['year'];
$month			 	= $_REQUEST['month'];

if($month==''){
 	$year			= date('Y', strtotime('-1 day', strtotime(date('Y-m-d'))));
	$month			= date('m', strtotime('-1 day', strtotime(date('Y-m-d'))));
}

 	$result = $obj_common->getClusters('REVENUE_WITH_RM_COST'); 
	while($row_c=mysqli_fetch_array($result))
	{ 
		$cluster		=$row_c['NO'];
		$result1		= $obj_revenue_get->get_customers_result($month,$year,$toCurrency,$cluster);
		while($row=mysqli_fetch_array($result1))
		{
				$customerId		=$row['intId'];
				$revenue 		= $obj_revenue_get->getCustomerRevenue($customerId,$month,$year,$toCurrency,$cluster);
				$RMcost	 		= $obj_revenue_get->getCustomerRM($customerId,$month,$year,$toCurrency,$cluster);
				$obj_revenue_set->delete_customer_revenue_old_data($customerId,$year,$month,$cluster);
				$obj_revenue_set->set_customer_revenue_old_data($customerId,$year,$month,$revenue,$RMcost,$cluster);
		}
	}
	   
 ?>
 
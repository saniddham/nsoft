<?php
ini_set('max_execution_time', 11111111) ;
session_start();
//ini_set('display_errors',1);
  
include_once $_SESSION['ROOT_PATH']."class/dailyMails/customerRevenue/cls_customer_revenue_get.php";
include_once $_SESSION['ROOT_PATH']."class/dailyMails/customerRevenue/cls_customer_revenue_set.php";
include_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";

$obj_revenue_get						= new cls_customer_revenue_get($db);
$obj_revenue_set						= new cls_customer_revenue_set($db);
$obj_common								= new cls_commonFunctions_get($db);

$customerArr							= array();
$customerNameArr						= array();
$monthNameArr							= array();
$monthArr								= array();
$yearArr								= array();

$custMonthDispArr 						= array();
$custMonthRMArr 						= array();
$monthWiseDispSumArr 					= array();
$monthWiseRMSumArr 						= array();
$custWiseDispSumArr 					= array();
$custWiseRMSumArr 						= array();
 
$companyId 								= 	$_SESSION['CompanyID'];
$locationId 							= 	$_SESSION['CompanyID'];
$intUser  								= 	$_SESSION["userId"];
$mainPath 								= 	$_SESSION['mainPath'];
$thisFilePath 							=  	$_SERVER['PHP_SELF'];

$toCurrency  							=	1;		//1=USD,2=LKR
$month  								= 	date('m');
$year  									= 	date('Y');
//$month=3; 
//$cluster	=1;//comment this


$i										=0;
$temp									='';


if($month>=4){
	for($j=$month; $j>3 ; $j--){
	  $monthNameArr[$i]	=$obj_common->getMonthName($j);
	  $monthArr[$i]	=$j;
	  $yearArr[$i]	=$year;
	  $i++;
	}
}
else{
	for($j=$month; $j>0 ; $j--){
	  $monthNameArr[$i]	=$obj_common->getMonthName($j);
	  $monthArr[$i]	=$j;
	  $yearArr[$i]	=$year;
	  $i++;
	}
	for($j=12; $j>3 ; $j--){
	  $monthNameArr[$i]	=$obj_common->getMonthName($j);
	  $monthArr[$i]	=$j;
	  $yearArr[$i]	=$year-1;
	  $i++;
	}
}


$noOfMonths 			= $i;

$currencyName1		=$obj_common->getCurrencyName($toCurrency);
 ?>
<title>Customer Revenue Report</title>
<style>
.normalfntBlue {
	font-family: Verdana;
	font-size: 10px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfnt {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: right;
}
.compulsoryRed {
	color: #F00;
}
</style>
<div align="center">
  <div style="background-color:#FFF" ><strong>
    <div align="left"><img style="vertical-align:" src="http://accsee.com/nsoft_logo.png"  /></div>
    CUSTOMER REVENUE REPORT - <?php echo ' Process Time('.date('Y-m-d H:i:s').')' ?></strong><strong></strong></div>
  <div style="background-color:'#FF'" ><strong>(Currency - <?php echo $currencyName1?>)  ( <?php echo $clusterName; ?> )</strong><strong></strong></div>
  <table width="2100" border="0" align="center" bgcolor="">
    <tr>
      <td><table width="2100">
          <tr>
            <td width="10">&nbsp;</td>
            <td colspan="7" class="normalfnt"><table width="2100" border="1" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
                <!-----------Managers revenue header------------------------------------------------------>
                <tr class="" bgcolor="<?php echo $reportColor; ?>">
                  <th width="1050" rowspan="2" bgcolor="#006699"><font color="<?php echo $reportColor; ?>">Customer</font></th>
                  <?php
 					  	for($m=0; $m<$noOfMonths ; $m++){
				  ?>
                  <th colspan="4" align="center" bgcolor="<?php echo $bgCol ; ?>"  class="normalfntBlue normalfntMid" ><b><?php echo $yearArr[$m].'-'.$monthNameArr[$m]; ?></b></th>
                 <?php
						}
				 ?>
                  <th colspan="5" align="center" bgcolor="#B3E7FF"  class="normalfntBlue normalfntMid" ><b>Total</b></th>
                </tr>
                <tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
                  <?php
 					  	for($m=0; $m<$noOfMonths ; $m++){
				  ?>
                    <td bgcolor="<?php echo $bgCol; ?>" class="normalfntRight" ><b>Revenue</b></td>
                    <td bgcolor="<?php echo $bgCol; ?>" class="normalfntRight" ><b>RM Cost</b></td>
                    <td bgcolor="<?php echo $bgCol; ?>" class="normalfntRight" ><b>Contribution</b></td>
                    <td bgcolor="<?php echo $bgCol; ?>" class="normalfntRight" ><b>Contribution %</b></td>
                 <?php
						}
				 ?>
                    <td bgcolor="#B3E7FF" class="normalfntRight" ><b>Revenue</b></td>
                    <td bgcolor="#B3E7FF" class="normalfntRight" ><b>RM Cost</b></td>
                    <td bgcolor="#B3E7FF" class="normalfntRight" ><b>Contribution</b></td>
                    <td bgcolor="#B3E7FF" class="normalfntRight" ><b>Contribution %</b></td>
                    <td width="1000" bgcolor="#B3E7FF" class="normalfntRight" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
              
              
                <!---------------end of customer revenue header------------------------>
                <!---------------save customer revenue in to array--------------------->
                <?php 
				$result1 =$obj_revenue_get->get_saved_customers_result($monthArr,$yearArr,$cluster);
				$c=0;
				while($row=mysqli_fetch_array($result1))
				{
					$custMonthDispArr[$c]		=array();
					$custMonthRMArr[$c]			=array();
					
					$customerId	=$row['intId'];
					$customerArr[$c]			=$customerId;
					$customerNameArr[$c]		=$row['strName'];
					
 					  	for($j=0; $j<$noOfMonths ; $j++){
							$dispatchData					=$obj_revenue_get->get_old_customer_revenue($customerId,$monthArr[$j],$yearArr[$j],$toCurrency,$cluster);  
							$dispatchValue					=$dispatchData['REVENUE'];			
							$dispatchRM						=$dispatchData['RM_COST'];
							$custMonthDispArr[$c][$j]		+=$dispatchValue;
							$custMonthRMArr[$c][$j]			+=$dispatchRM;
							$monthWiseDispSumArr[$j]		+=$dispatchValue; 
							$monthWiseRMSumArr[$j]			+=$dispatchRM; 
							$custWiseDispSumArr[$c]			+=$dispatchValue; 
							$custWiseRMSumArr[$c]			+=$dispatchRM; 
						}
					$c++;
				}
				$noOfCustomers		=$c;

				for($k=0; $k<$noOfCustomers ; $k++){
					?>
					<tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
					<td class="normalfnt" nowrap="nowrap" ><?php echo $customerNameArr[$k];?></td>
					<?php 
						for($j=0; $j<$noOfMonths ; $j++){
							?>
							<td class="normalfntRight" nowrap="nowrap" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format(round($custMonthDispArr[$k][$j],2),2); ?></td>
							<td class="normalfntRight" nowrap="nowrap" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format(round($custMonthRMArr[$k][$j],2),2); ?></td>
							<td class="normalfntRight" nowrap="nowrap" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format((round($custMonthDispArr[$k][$j],2)-round($custMonthRMArr[$k][$j],2)),2); ?></td>
							<td class="normalfntRight" nowrap="nowrap" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format(((round($custMonthDispArr[$k][$j],2)-round($custMonthRMArr[$k][$j],2))/round($custMonthDispArr[$k][$j],2))*100,2).'%'; ?></td>
                    
                    	<?php
						}
						?>
							<td class="normalfntRight" nowrap="nowrap" bgcolor="#B3E7FF" ><?php echo number_format(round($custWiseDispSumArr[$k],2),2); ?></td>
							<td class="normalfntRight" nowrap="nowrap" bgcolor="#B3E7FF" ><?php echo number_format(round($custWiseRMSumArr[$k],2),2); ?></td>
							<td class="normalfntRight" nowrap="nowrap" bgcolor="#B3E7FF" ><?php echo number_format((round($custWiseDispSumArr[$k],2)-round($custWiseRMSumArr[$k],2)),2); ?></td>
							<td class="normalfntRight" nowrap="nowrap" bgcolor="#B3E7FF" ><?php echo number_format(((round($custWiseDispSumArr[$k],2)-round($custWiseRMSumArr[$k],2))/round($custWiseDispSumArr[$k],2))*100,2).'%'; ?></td>
							<td class="normalfntRight" nowrap="nowrap" bgcolor="#B3E7FF" >&nbsp;</td>
						</tr>
						<?php 
				}//end of customers
				  ?>
                <tr class="normalfnt"  bgcolor="#CCCCCC">
                  <td class="normalfnt" >&nbsp;</td>
                  <?php
				  	$totDisp	=0;
					$totRM		=0;
					$totContri	=0;

					for($j=0; $j<$noOfMonths ; $j++){
					?>
                  <td class="normalfntRight" nowrap="nowrap" ><b><?php echo number_format(round($monthWiseDispSumArr[$j],2),2); ?></b></td>
                  <td class="normalfntRight" nowrap="nowrap" ><b><?php echo number_format(round($monthWiseRMSumArr[$j],2),2); ?></b></td>
                  <td class="normalfntRight" nowrap="nowrap" ><b><?php echo number_format((round($monthWiseDispSumArr[$j],2)-round($monthWiseRMSumArr[$j],2)),2); ?></b></td>
				  <td class="normalfntRight" nowrap="nowrap" ><b><?php echo number_format(((round($monthWiseDispSumArr[$j],2)-round($monthWiseRMSumArr[$j],2))/round($monthWiseDispSumArr[$j],2))*100,2).'%'; ?></b></td>
                	<?php
					$totDisp	+=	$monthWiseDispSumArr[$j];
					$totRM		+=	$monthWiseRMSumArr[$j];
					$totContri	+=	round($monthWiseDispSumArr[$j],2)-round($monthWiseRMSumArr[$j],2);
					}
					?>
                  <td class="normalfntRight"  nowrap="nowrap" bgcolor="#B3E7FF"><b><?php echo number_format(round($totDisp,2),2); ?></b></td>
                  <td class="normalfntRight"  nowrap="nowrap" bgcolor="#B3E7FF"><b><?php echo number_format(round($totRM,2),2); ?></b></td>
                  <td class="normalfntRight"  nowrap="nowrap" bgcolor="#B3E7FF"><b><?php echo number_format(round($totContri,2),2); ?></b></td>
				  <td class="normalfntRight"  nowrap="nowrap" bgcolor="#B3E7FF"><b><?php echo number_format((($totContri)/round($totDisp,2))*100,2).'%'; ?></b></td>
				  <td class="normalfntRight"  nowrap="nowrap" bgcolor="#B3E7FF">&nbsp;</td>
                  
                  
                </tr>
                <!----------------------------->
              </table></td>
            <td width="10">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="8" height="15"></td>
          </tr>
        </table></td>
      <td width="10">&nbsp;</td>
    </tr>
  </table>
  </td>
  </tr>
  <tr height="90" >
    <td align="center" class="normalfntMid"></td>
  </tr>
  </table>
</div>

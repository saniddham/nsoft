<?php 
	$toMails		= '';
	$ccMails		= '';
	$bccMails		= '';
	$recordStatus	= 1;
	//get to user id
	$sql_to		= " SELECT
					sys_cronjob_emails_to.`TO` AS mailTO
					FROM
					sys_cronjob_emails_to
					WHERE
					sys_cronjob_emails_to.CRONJOB_ID = '39'";
	$result_to	= $db->RunQuery($sql_to);
	while($row_to	= mysqli_fetch_array($result_to))
	{
		$toMails	.= $row_to['mailTO'].',';
	}
	$trimToMails	 = rtrim($toMails,',');
	//get cc user ids
	$sql_cc			= " SELECT
						sys_cronjob_emails_cc.CC AS mailCC
						FROM
						sys_cronjob_emails_cc
						WHERE
						sys_cronjob_emails_cc.CRONJOB_ID = '39'";
						
	$result_cc	= $db->RunQuery($sql_cc);
	while($row_cc	= mysqli_fetch_array($result_cc))
	{
		$ccMails	.= $row_cc['mailCC'].',';
	}
	$trimCcMails	 = rtrim($ccMails,',');
	//get bcc user ids
	$sql_bcc			= " SELECT
						sys_cronjob_emails_bcc.BCC AS mailBcc
						FROM
						sys_cronjob_emails_bcc
						WHERE
						sys_cronjob_emails_bcc.CRONJOB_ID = '39'";
						
	$result_bcc	= $db->RunQuery($sql_bcc);
	while($row_bcc	= mysqli_fetch_array($result_bcc))
	{
		$bccMails	.= $row_bcc['mailBcc'].',';
	}
	$trimBccMails	 = rtrim($bccMails,',');	
	
	$sql_err		= "SELECT
						COUNT(sys_users_disabled.ID) AS recordCount
						FROM
						sys_users_disabled
						WHERE
						MONTH(sys_users_disabled.DATE) = MONTH(NOW()) AND 
						YEAR(sys_users_disabled.DATE)	= YEAR(NOW())";
	$result_err		= $db->RunQuery($sql_err);
	$row_err	= mysqli_fetch_array($result_err);
	if($row_err['recordCount'] == 0)
		$recordStatus	= 0;
	ob_start();
		
	include "rptInvalidLoginAttempt.php";		
	$body = ob_get_clean();

	//$body =  $strEmailAddress.'<br>'. $strCCEmailAddress . '<br>'.  $body;
	echo $body;

	$FROM_NAME			= "NSOFT - SCREENLINE HOLDINGS";
	$FROM_EMAIL			= "";
	 
	$currMonth = date('M');
	$mailHeader= "INVALID USER LOGIN ATTEMPT - ".$currMonth;
	//Original
	$mail_TO	= $obj_comm->getEmailList($trimToMails);
	$mail_CC	= $obj_comm->getEmailList($trimCcMails);
	$mail_BCC	= $obj_comm->getEmailList($trimBccMails);
	if($recordStatus == 1)
		insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);

?>
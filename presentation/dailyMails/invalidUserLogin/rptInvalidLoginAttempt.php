<?php
ini_set('display_errors',0);
session_start();
ob_start();	
 
?>
<style type="text/css">
/*BEGIN - REPORT TABLE*/
.rptBordered {
	border: solid #ccc 1px;
	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;
	border-radius: 6px;
	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; 
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}
.rptBordered tfoot td{
	font-weight:bold;
}

.rptBordered tbody tr:nth-child(even) {background: #F7F7F7}
.rptBordered tbody tr:nth-child(odd) {background: #FFF}

.rptBordered tbody tr:hover {
   /*<!-- background: #fbf8e9; -->*/
    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;
    transition: all 0.1s ease-in-out;
	color:#127FD2;  
}    
    
.rptBordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
    text-align: left;    
}

.rptBordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.rptBordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image: linear-gradient(top, #ebf3fc, #dce9f9);
    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset;  
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.rptBordered td:first-child, .rptBordered th:first-child {
    border-left: none;
}

.rptBordered th:first-child {
    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;
    border-radius: 6px 0 0 0;
}

.rptBordered th:last-child {
    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;
    border-radius: 0 6px 0 0;
}

.rptBordered th:only-child{
    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;
    border-radius: 6px 6px 0 0;
}

.rptBordered tr:last-child td:first-child {
    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;
    border-radius: 0 0 0 6px;
}

.rptBordered tr:last-child td:last-child {
    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;
    border-radius: 0 0 6px 0;
}
/*END - REPORT TABLE*/
</style>
  <table width="700" border="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr><?php 	$month		= date('m');
				$month_name = date("F", mktime(0, 0, 0, $month, 10));?>
        <td align="center" style="font-size:24px"><b>Invalid Login Attempts of <?PHP echo $month_name?></b></td>
    </tr>
    <tr>
        <td align="center" height="20"></td>
    </tr>
    <tr>
      <td><table width="700" border="0" class="rptBordered">
          <thead>
              
            <tr  bgcolor="#DCE9F9">
            <th width="37%">User Name</th>
            <th width="37%">Full Name</th>
            <th width="37%">Department</th>
            <th width="37%">Designation</th>
            <th width="37%">Contact No</th>
            <th width="63%">Attemps</th>
            </tr>
          </thead>
          <tbody>
          <?php
		 
			$sql	= " SELECT
						Count(sys_users_disabled.USER_ID) AS errorTimes,
						sys_users.strUserName AS userName,
						sys_users.strFullName AS fullName,
						mst_department.strName AS department,
						sys_users.strContactNo AS contactNo,
						sys_users.strDesignation AS designation
						FROM
						sys_users_disabled
						INNER JOIN sys_users ON sys_users_disabled.USER_ID = sys_users.intUserId
						INNER JOIN mst_department ON sys_users.intDepartmentId = mst_department.intId
						WHERE
						MONTH(sys_users_disabled.DATE) = MONTH(NOW()) AND 
						YEAR(sys_users_disabled.DATE)	= YEAR(NOW()) AND
						sys_users_disabled.USER_ID <> 2
						GROUP BY
						sys_users_disabled.USER_ID";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
		?>
    	<tr>
        	<td><?php echo $row['userName']; ?></td>
        	<td><?php echo $row['fullName']; ?></td>
            <td><?php echo $row['department']; ?></td>
            <td><?php echo $row['designation']; ?></td>
            <td style="text-align:left"><?php echo $row['contactNo']; ?></td>
            <td style="text-align:left"><?php echo $row['errorTimes']; ?></td>
        	
        </tr>
        <?php
		}
		?>
	    </tbody>
        </table></td>
    </tr>
  </table>


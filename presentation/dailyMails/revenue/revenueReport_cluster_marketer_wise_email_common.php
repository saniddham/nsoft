<?php

		$sub_emails_to	=$obj_comm->getDailyMailsRecipients($reportId,$mainCluster,$subCluster,$company,'TO');
		$sub_emails_cc	=$obj_comm->getDailyMailsRecipients($reportId,$mainCluster,$subCluster,$company,'CC');
		$sub_emails_bcc	=$obj_comm->getDailyMailsRecipients($reportId,$mainCluster,$subCluster,$company,'BCC');
		
	
		$body					= '';
		$clusterId				= $rowClust['ID'];
		$clusterName 			= $rowClust['NAME'];
		$sub_clusterId			= $rowSubClust['ID'];
		$sub_clusterName 		= $rowSubClust['NAME'];
		
		$fix_holiday_of_week	= $rowClust['WEEKLY_HOLIDAY'];
		
		ob_start();
		
		if($reportType==1)	
			include 'revenueReport_cluster_marketer_wise.php';
		else if($reportType==2)
			include 'revenueReport_sub_cluster_marketer_wise.php';
		
 		echo $body 			= ob_get_clean();
 		//---------
		
		$nowDate 		= date('Y-m-d');
 		
		if($reportType==1)	
			$mailHeader		= "REVENUE REPORT - $clusterName ($nowDate)";
		else if($reportType==2)
			$mailHeader		= "REVENUE REPORT - $sub_clusterName ($nowDate)";
		
		$FROM_NAME		= "";
		$FROM_EMAIL		= '';
		
 	
		$mail_TO	= $obj_comm->getEmailList($sub_emails_to);
		$mail_CC 	= $obj_comm->getEmailList($sub_emails_cc);
		$mail_BCC	= $obj_comm->getEmailList($sub_emails_bcc);
		insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
		//insertTable($FROM_EMAIL,$FROM_NAME,'',$mailHeader,$body,'nish@screenlineholdings.com','');
?>
<?php
session_start();
date_default_timezone_set('Asia/Colombo');
ini_set('max_execution_time', 11111111) ;
ini_set('display_errors',0);
$thisFilePath 			=  $_SERVER['PHP_SELF'];
 
include_once $_SESSION['ROOT_PATH']."class/dailyMails/revenue/cls_marketer_revenue_get.php";
include_once $_SESSION['ROOT_PATH']."class/dailyMails/revenue/cls_marketer_revenue_set.php";
   
 $obj_revenue_get	= new cls_marketer_revenue_get($db);
$obj_revenue_set	= new cls_marketer_revenue_set($db);
  
   
$toCurrency  		= 1;//1=USD,2=LKR

echo $year			 	= $_REQUEST['year'];
$month			 	= $_REQUEST['month'];
$day			 	= $_REQUEST['day'];
$cyear			 	= $_REQUEST['year'];
$cmonth			 	= $_REQUEST['month'];
$cday			 	= $_REQUEST['day'];


if($day==''){
 	$year			= date('Y', strtotime('-1 day', strtotime(date('Y-m-d'))));
	$month			= date('m', strtotime('-1 day', strtotime(date('Y-m-d'))));
	$day			= date('d', strtotime('-1 day', strtotime(date('Y-m-d'))));
	$cyear			= date('Y', strtotime(date('Y-m-d')));
	$cmonth			= date('m', strtotime(date('Y-m-d')));
	$cday			= date('d', strtotime(date('Y-m-d')));
}

 $noOfDays				= $obj_comm->days_in_month($month,$year);
 
$plant_result = $obj_comm->getPlants();
 
//while($rowPlant = mysqli_fetch_array($plant_result))
//{
// 	$plant						= $rowPlant['intPlantId'];
// 	$cluster					= $rowPlant['MAIN_CLUSTER_ID'];
//   	$result1 					= $obj_revenue_get->get_marketers_and_targets_result($year,$month,$day,$plant,$cluster);
//
//	while($row=mysqli_fetch_array($result1))
//	{
// 			 $marketerId		=$row['intUserId'];
//
//			$result_tech 	= $obj_comm->get_group_techniques('RunQuery');
//			while($row_tech=mysqli_fetch_array($result_tech))
//			{
//				$techniqueId	= $row_tech['ID'];
//  				$revenue 		= 0;
//  				$sticker_pressing_cost = 0;
//				//if($cluster==2)
//				//echo $plant."/".$cluster."/".$techniqueId."/".$marketerId."/".$revenue."</br>";
//
//				$obj_revenue_set->delete_marketer_revenue_old_data($techniqueId,$marketerId,$plant,$year,$month,$day);
//				$obj_revenue_set->set_marketer_revenue_old_data($techniqueId,$marketerId,$plant,$year,$month,$day,$revenue,$sticker_pressing_cost);
//			}
//	}
	
//}

	$result1 		= $obj_revenue_get->getMarketerRevenue_stock($month,$year,$day,$cmonth,$cyear,$cday);

	while($row=mysqli_fetch_array($result1))
	{
 			$techniqueId	=$row['TECHNIQUE_GROUP_ID']; 
 			$marketerId		=$row['intUserId']; 
 			$plant			=$row['intPlant'];
			$revenue		=$row['REVENUE'];
            $sticker_pressing_cost		=$row['sticker_pressing_cost'];
        	$locationId		=$row['intId'];
			
			$obj_revenue_set->delete_marketer_revenue_old_data($techniqueId,$marketerId,$plant,$locationId,$year,$month,$day);
			$obj_revenue_set->set_marketer_revenue_old_data($techniqueId,$marketerId,$plant,$locationId,$year,$month,$day,$revenue,$sticker_pressing_cost);
	
	}

	   
 ?>
 
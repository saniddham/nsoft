 <?php
 
	ini_set('max_execution_time', 11111111) ;
	session_start();
	//ini_set('display_errors',1);
	  
	include_once $_SESSION['ROOT_PATH']."class/dailyMails/revenue/cls_marketer_revenue_get.php";
	include_once $_SESSION['ROOT_PATH']."class/dailyMails/revenue/cls_marketer_revenue_set.php";

	//$db 									=	new LoginDBManager();
	$obj_revenue_get						=	new cls_marketer_revenue_get($db);
	$obj_revenue_set						=	new cls_marketer_revenue_set($db);
 
 
	$companyId 								=	$_SESSION['CompanyID'];
	$locationId 							= 	$_SESSION['CompanyID'];
	$intUser  								= 	$_SESSION["userId"];
	$mainPath 								= 	$_SESSION['mainPath'];
	$thisFilePath 							=  	$_SERVER['PHP_SELF'];
	
	$toCurrency  							=	1;		//1=USD,2=LKR
 
	//$printDateArr_tach 					=	array();
	//$dateArr_tach							=	array();
	//$fullDateArr_tach						=	array();
	//$dateNameArr_tach						=	array();
	//$dateNameArr_tach						=	array();
 
 	$res_po_tech							= $obj_comm->get_group_techniques('RunQuery');
	
	while($row_tech=mysqli_fetch_array($res_po_tech))
	{
		
		$cumulativeDay_tech						=	array();
		$DayWiseDispSumArr_tach 				=	array();
		
		$technique		= $row_tech['ID'];
		$technique_name	= $row_tech['NAME'];
		if($technique > 0 )
		{
 ?>
                <tr class="" bgcolor="">
               <th colspan="<?php echo($noOfDays+5);  ?>"  class="normalfntRight" ></th>
                </tr>
                <tr class="" bgcolor="" height="20">
               <th colspan="<?php echo($noOfDays+5);  ?>"  class="normalfnt" ><b>Plant Wise (Technique - <?php echo $technique_name?>)  Revenue </b></th></th>
                </tr>
                <tr class="" bgcolor="<?php echo $reportColor; ?>">
                  <th bgcolor="#006699"  colspan="4" ><font color="<?php echo $reportColor; ?>">Plant</font></th>
                  <?php
			 	 for($j=0; $j<$noOfDays ; $j++){
				  $bgCol=$reportColor;
				  if($dateNameArray[$j]==$fix_holiday_of_week)
				  $bgCol='#FF99CC';
				  $cumulativeDay_tech[$j]=1;
				  ?>
                  <th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php echo $bgCol ; ?>" ><?php echo $printDateArr[$j]; ?></th>
                  <?php
		}
				  ?>
                  <th class="normalfntMid">Total</th>
                </tr>
                <!----End of Plant wise header------------------------------------------------------------>
                <!----Plant wise revnue------------------------------------------------------------------->
                <?php 
				 $result1=$obj_revenue_get->get_main_cluster_locations($clusterId,$year,$month);
				 $anualTargtTot =0;
				 $monthlyTargtTot =0;
				 $dayTargtTot =0;
				 $totPlantDispQty=0;
				 
				while($row=mysqli_fetch_array($result1))
				{
					$plantId=$row['intPlantId'];
					$locationId = $row['intId'];
					$plantWiseTotDispQty[$locationId]=0;
	 			 ?>
                <tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
                  <td class="normalfnt"  colspan="4"  ><?php echo $row['strName'];?></td>
                  <?php 
				  for($j=0; $j<$noOfDays ; $j++)
				  {
					$date=$fullDateArr[$j];
					$dayType='';
					$resultr=$obj_revenue_get->get_plant_calender($plantId,$date);  
					$rorw=mysqli_fetch_array($resultr);
					$dayType=$rorw['intDayType'];
					$dispatchValue=$obj_revenue_get->get_old_cluster_location_technique_revenue($technique,$plantId,$locationId,$date,$toCurrency);
					$DayWiseDispSumArr_tach[$j]+=$dispatchValue;  
				
					if($dateNameArray[$j]==$fix_holiday_of_week)
						$bgCol=" style=\"background-color:#FF99CC\"";
					else
						$bgCol='';
						
 					if($dayType=='0')
						$bgCol=" style=\"background-color:#FFFF33\"";
					if(($dateNameArray[$j]==$fix_holiday_of_week) || ($dayType=='0')){
						$cumulativeDay_tech[$j]=0;
					}
					
					$plantWiseTotDispQty[$locationId] +=round($dispatchValue);
					$totPlantDispQty +=round($dispatchValue);
 				  ?>
					  <td class="normalfntRight"  "<?php echo $bgCol; ?>"><?php echo number_format($dispatchValue,0); ?></td>
					  <?php
				  }
				  ?>
                  <td width="80" class="normalfntRight"><?php echo number_format($plantWiseTotDispQty[$locationId],0);?></td>
                </tr>
                <?php 
				 $anualTargtTot		 +=$row['dblAnualaTarget'];
				 $monthlyTargtTot 	+=$row['dblMonthlyTarget'];
				 $dayTargtTot		 +=$row['dblDayTarget'];
				}
	  ?>
                <tr class="normalfnt"  bgcolor="#CCCCCC">
                  <td class="normalfnt"  colspan="4"  >&nbsp;</td>
                  <?php
				  for($j=0; $j<$noOfDays ; $j++){
				  ?>
					  <td class="normalfntRight" ><b><?php echo number_format($DayWiseDispSumArr_tach[$j],0); ?></b></td>
					  <?php
				  }
				  ?>
                  <td class="normalfntRight"  ><?php echo number_format($totPlantDispQty,0); ?></td>
                </tr>
                <!----End of Plant wise revnue----------------------------------------------------------->
                <tr bgcolor="<?php echo $reportColor; ?>">
                  <td align="right" class="normalfntRight" colspan="4" ></td>
                  <td colspan="<?php echo $noOfDays; ?>" >&nbsp;</td>
                  <td width="80">&nbsp;</td>
                </tr>
                <!----End of Plant wise Cum.Status-------------------------------------------------------->

<?php
		}
	}
?>


<?php
ini_set('max_execution_time', 11111111) ;
session_start();
//ini_set('display_errors',1);
include_once $_SESSION['ROOT_PATH']."class/tables/costing_sample_header.php";
//print_r($_SERVER);
 
//$db 									= new LoginDBManager();
$costing_sample_header					= new costing_sample_header($db);
  
$companyId 								= 	$_SESSION['CompanyID'];
$locationId 							= 	$_SESSION['CompanyID'];
$intUser  								= 	$_SESSION["userId"];
$mainPath 								= 	$_SESSION['mainPath'];
$thisFilePath 							=  	$_SERVER['PHP_SELF'];

$toCurrency  							=	1;		//1=USD,2=LKR


$year			= date('Y', strtotime('-1 day', strtotime(date('Y-m-d'))));
$month			= date('m', strtotime('-1 day', strtotime(date('Y-m-d'))));
$day			= date('d', strtotime('-1 day', strtotime(date('Y-m-d'))));

$startDate								=	1;
$monthName								=	$obj_comm->getMonthName($month);

$ts 									= 	strtotime($monthName." ".$year);
$endDate								= 	date('t', $ts); 
$startDate 								= 	date("Y-m-d", mktime(0, 0, 0, $month, $startDate, $year));
$endDate 								= 	date("Y-m-d", mktime(0, 0, 0, $month, $endDate, $year));
$rptDate 								= 	date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))); 



$i										=0;
$temp									='';
while ($temp < $endDate) {
 $temp 				= date("Y-m-d", mktime(0, 0, 0, $month, 1+$i, $year));
 $weekday 			= date('l', strtotime($temp)); // note: first arg to date() is lower-case L
 $printDateArr[$i]	=(1+$i)."-".substr($monthName,0,3);
 $dateArr[$i]		=(1+$i);
 $fullDateArr[$i]	=$temp;
 $dateNameArray[$i]	=$weekday; 
 $i++;
}
$noOfDays 			= $i;
 $currencyName1		=$obj_comm->getCurrencyName($toCurrency);
 //print_r($_SERVER);
 //echo $x		= $_SERVER['REQUEST_URI'];
 //$arr_proj	=explode('/',$x);
 //print_r($arr_proj);
 //echo "ppppppppp";
 ?>
<title>Costing Report</title>
<style>
.normalfntBlue {
	font-family: Verdana;
	font-size: 10px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfnt {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: right;
}
.compulsoryRed {
	color: #F00;
}
</style>
<div align="center">
  <div style="background-color:#FFF" ><strong>
    <div align="left"><img style="vertical-align:" src="http://accsee.com/nsoft_logo.png"  /></div>
     <?php echo $mailHeader ?></strong><strong></strong></div>
  <div style="background-color:'#FF'" ><strong></strong><strong></strong></div>
  <table width="1500" border="0" align="center" bgcolor="">
    <tr>
      <td><table width="1500">
          <tr>
            <td width="10">&nbsp;</td>
            <td colspan="7" class="normalfnt"><table width="1500" border="1" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="normalfntMid"  bgcolor="#C4C4FF" height="30" style="font-size:15px;">
          	<th>Status</th>
            <th>Sample No</th>
            <th>Print</th>
            <th>Combo</th>
            <th>Brand</th>
            <th>Fabric Type</th>
            <th>Technique</th>
            <?php if($cronjobId!=52 && $cronjobId!=53){ ?><th>Customer</th> <?php }?>
            <th>Graphic</th>
            <th>Graphic H(Inches)</th>
            <th>Graphic W(Inches)</th>
            <?php if($cronjobId!=52 && $cronjobId!=53){ ?><th>RM cost($)</th> <?php }?>
            <th>No of Shots</th>
            <th>No of Ups</th>
            <th>Print Method</th>
            <th>Print price($)</th>
            <?php if($cronjobId!=52 && $cronjobId!=53){ ?><th>View Costing</th> <?php }?>
            <?php
			if($cronjobId==52 || $cronjobId==53){ ?>
            <th>Graphic</th>
            <th>User</th>
			<?php 
			}
           	?>
            </tr>
            <?php

				$i=0;
				$rec_count		=0;
				$resultr=$costing_sample_header->get_costing_sample($time_from,$time_to,$CLUSTER,$COMPANIES_INCLUDED,$COMPANIES_EXCLUDED,$BRAND_CATEGORY_INCLUDED,$BRAND_CATEGORY_EXCLUDED,$CUSTOMERS_INCLUDED,$CUSTOMERS_EXCLUDED); 
				
				//echo $costing_sample_header->db->getsql();	
				//$resultr=$costing_sample_header->get_costing_sample('2016-08-20 12:00:00','2016-09-30 12:00:00'); 
				while($row=mysqli_fetch_array($resultr)){
				
					$sample_no		=$row['SAMPLE_NO'];
					$sample_year	=$row['SAMPLE_YEAR'];
					$sample_rev		=$row['REVISION'];
					$sample_combo	=$row['COMBO'];
					$sample_print	=$row['PRINT'];
			
					$i++;
					$rec_count++;
					if($i%2 == 0){
						$bg_col	= '#CCCCCC';
					}
					else
						$bg_col	= '#FFFFFF';
						
					$fabType	= getFabricType($row['SAMPLE_NO'],$row['SAMPLE_YEAR'],$row['REVISION'],$row['COMBO'],$row['PRINT']);
					$price = $row['APPROVED_PRICE'];
                    if ($row['QTY_PRICES'] ==1){
                         $sql  = "SELECT max(PRICE) as maxPrice, min(PRICE) as minPrice FROM costing_sample_qty_wise
								WHERE
									SAMPLE_NO 	=  '$sample_no' AND
									SAMPLE_YEAR 	=  '$sample_year' AND
									REVISION 	=  '$sample_rev' AND
									PRINT  =  '$sample_print' AND
									COMBO 		=  '$sample_combo' 
					    ";
                        $price = "";
                        $qtyPrices = $db->RunQuery($sql);
                         while ($qtyRow = mysqli_fetch_array($qtyPrices)) {
                            $upperPrice = $qtyRow['maxPrice'];
                            $lowerPrice = $qtyRow['minPrice'];
                            $price .= $lowerPrice."-".$upperPrice;
                       }
               }
	
			?>
 		<tr class="normalfntMid" height="25" bgcolor="<?php echo $bg_col; ?>">
            <td><?php echo $row['STATUS_STR']; ?>&nbsp;</td>
            <td><?php echo $row['SAMPLE_NO'].'/'.$row['SAMPLE_YEAR'].'/'.$row['REVISION']; ?>&nbsp;</td>
            <td><?php echo $row['PRINT']."/".$row['part']; ?>&nbsp;</td>
            <td><?php echo $row['COMBO']; ?>&nbsp;</td>
            <td><?php echo $row['brand']; ?>&nbsp;</td>
            <td><?php echo $fabType; ?>&nbsp;</td>
            <td><?php echo $row['TECHNIQUE_NAME']; ?>&nbsp;</td>
            <?php if($cronjobId!=52 && $cronjobId!=53){ ?><td><?php echo $row['strName']; ?>&nbsp;</td><?php }?>
            <td nowrap style="text-align:center"><?php
			//echo "<img id=\"saveimg\" width=\"100\" height=\"100\" src=\"http://".$_SERVER['HTTP_HOST']."/documents/sampleinfo/samplePictures/".$row['SAMPLE_NO']."-".$row['SAMPLE_YEAR']."-".$row['REVISION']."-".substr($row['PRINT'], 6).".png\" />";	
				  echo "<img src=\"documents/sampleinfo/samplePictures/".$row['SAMPLE_NO']."-".$row['SAMPLE_YEAR']."-".$row['REVISION']."-".substr($row['PRINT'], 6).".png\" width=\"100\" height=\"100\"/>";
			//local
			//echo "<img id=\"saveimg\" width=\"100\" height=\"100\" src=\"http://".$_SERVER['HTTP_HOST']."/".$arr_proj[0]."/documents/sampleinfo/samplePictures/".$row['SAMPLE_NO']."-".$row['SAMPLE_YEAR']."-".$row['REVISION']."-".substr($row['PRINT'], 6).".png\" />";
                $link= $_SESSION['MAIN_URL']."?q=1212&graphicNo=&sampleyear=".$row['SAMPLE_YEAR']."&sampleNo=".$row['SAMPLE_NO']."&revNo=".$row['REVISION']."&combo=".$row['COMBO']."&print=".$row['PRINT'];
			 ?></td>
            <td align="right"><?php echo $row['intWidth']; ?>&nbsp;</td>
            <td align="right"><?php echo $row['intHeight']; ?>&nbsp;</td>
            <?php if($cronjobId!=52 && $cronjobId!=53){ ?><td align="right"><?php echo ($row['SPECIAL_RM_COST']+$row['INK_COST']); ?>&nbsp;</td><?php } ?>
            <td align="right"><?php echo $row['SHOTS']; ?>&nbsp;</td>
            <td align="right"><?php echo $row['UPS']; ?>&nbsp;</td>
            <td><?php echo $row['strProcess']; ?>&nbsp;</td>
            <td align="right"><?php echo $price; ?>&nbsp;</td>
            <?php if($cronjobId!=52 && $cronjobId!=53){ ?><td><a href="<?php echo $link; ?>" ><u>View</u></a>&nbsp;</td><?php } ?>
            <?php
			if($cronjobId==52 || $cronjobId==53){ ?>
            <td><?php echo $row['graphic']; ?>&nbsp;</td>
            <td><?php echo $row['user']; ?>&nbsp;</td>
			<?php 
			}
           	?>
            </tr>
            <?php
				}
			?>
           </table></td>
            <td width="10">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="8" height="15"></td>
          </tr>
        </table></td>
      <td width="10">&nbsp;</td>
    </tr>
  </table>
  </td>
  </tr>
  <tr height="90" >
    <td align="center" class="normalfntMid"></td>
  </tr>
  </table>
</div>
 <?php

 ?>
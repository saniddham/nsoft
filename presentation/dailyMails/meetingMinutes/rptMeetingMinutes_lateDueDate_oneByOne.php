<?php
$cronjobId	= 5;
$auto_cron_5= 1;
	//session_start();
	//ini_set('display_errors',1);
	date_default_timezone_set('Asia/Colombo');
	require_once $_SESSION['ROOT_PATH']."class/meetingMinutes/cls_meetingMinutes_db.php";
	
	$objmomget= new cls_meetingMinutes_db($db);
	//$currunt_date=date('Y-m-d');
		
	$sqlMM="SELECT DISTINCT
		concat('MOM',LPAD(other_meeting_minutes_header.`MINUTE_ID`,4,0))  as Meeting_No,
		other_meeting_minutes_header.`MINUTE_ID` as serialNo, 
		other_meeting_minutes_details.COMPLETED_FLAG
		FROM
		other_meeting_minutes_header
		INNER JOIN other_meeting_minutes_details ON other_meeting_minutes_header.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID
		WHERE 
		other_meeting_minutes_header.STATUS = 1 AND 
		other_meeting_minutes_details.DUE_DATE < DATE(now()) 
		AND
		other_meeting_minutes_details.DUE_DATE IS NOT NULL
		HAVING
		IFNULL(other_meeting_minutes_details.COMPLETED_FLAG,0) <> 1
		ORDER BY
		other_meeting_minutes_header.MINUTE_ID ASC";
			
	$resultMM = $db->RunQuery($sqlMM);
	$lateMOMs=0;
		
	while($rowMM=mysqli_fetch_array($resultMM))
	{
		$lateMOMs++;
 		$MOM=$rowMM['Meeting_No'];
 		$serialNo=$rowMM['serialNo'];
		
		ob_start(); 
		
		$hideButton=1;
		$boo_run_from_cronjob = true;
 		include  $_SESSION['ROOT_PATH']."presentation/meetingMinutes/listing/rptMeetingMinutes.php";
		
		echo $body = ob_get_clean();
		
		//$nowDate = date('Y-m-d');
		$mailHeader= "MINUTES OF MEETING($MOM - $subject)";
		$FROM_NAME = 'MOM(NSOFT)';
		$FROM_EMAIL= '';
 		
		if($body != ''){
			//ORIGINAL
			//sendMessage($FROM_EMAIL,$FROM_NAME,$toEmails,$mailHeader,$body,$ccEmails,'');
		$strEmailAddress	= $objmomget->toEmails($serialNo);
		$strCCEmailAddress 	= $objmomget->ccEmails($serialNo);
		$strBCCmailAddress	= '';
		
		$cron_emails_to		=$obj_comm->getCronjobEmailIds($cronjobId,'TO');
		$cron_emails_cc		=$obj_comm->getCronjobEmailIds($cronjobId,'CC');
		$cron_emails_bcc	=$obj_comm->getCronjobEmailIds($cronjobId,'BCC');
	
		$mail_TO	= $strEmailAddress.",".$obj_comm->getEmailList($cron_emails_to);
		$mail_CC	= $strCCEmailAddress.",".$obj_comm->getEmailList($cron_emails_cc);
		$mail_BCC	= $strBCCmailAddress.",".$obj_comm->getEmailList($cron_emails_bcc);
			
		$mail_TO			= str_replace("nish@screenlineholdings.com","nish@screenlineholdings.com,chairmanoffice@screenlineholdings.com",$mail_TO);
		$mail_CC			= str_replace("nish@screenlineholdings.com","nish@screenlineholdings.com,chairmanoffice@screenlineholdings.com",$mail_CC);
		$mail_BCC			= str_replace("nish@screenlineholdings.com","nish@screenlineholdings.com,chairmanoffice@screenlineholdings.com",$mail_BCC);
			
			
		//insertTable($FROM_EMAIL,$FROM_NAME,'hemanthib@nimawum.com',$mailHeader,$body,'','');
		insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
			//TESTING
			//sendMessage($FROM_EMAIL,$FROM_NAME,'hemanthib@nimawum.com',$mailHeader,$body,'','');
		}
 	}
  ?>
 
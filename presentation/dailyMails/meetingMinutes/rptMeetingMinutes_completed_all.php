<?php
$cronjobId	= 7;
	
	//ini_set('display_errors',1);
	date_default_timezone_set('Asia/Colombo');
	$thisFilePath =  $_SERVER['PHP_SELF'];
 	
	ini_set('max_execution_time', 11111111) ;
	session_start();
	
	$companyId = $_SESSION['CompanyID'];
	$locationId = $_SESSION['CompanyID'];
	$intUser  = $_SESSION["userId"];
	$mainPath = $_SESSION['mainPath'];
	$thisFilePath =  $_SERVER['PHP_SELF'];
	
	$programName='Meeting Minutes';
	$programCode='P0718';
	
	$currunt_date=date('Y-m-d');
	$day_before = date( 'Y-m-d', strtotime( $currunt_date . ' -1 day' ) );
	
	ob_start();
?>
 <style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
 
 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}
 
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}

.odd{background-color:#F5F5F5;}
.even{background-color:#FFFFFF;}
.mouseover {
	cursor: pointer;
}


.txtNumber{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}
</style>

<div align="center">
<div style="background-color:#FFF" >
<div align="left" >
  <table width="100%" border="0" cellspacing="1">
    <tr>
      <td width="5%">&nbsp;</td>
      <td width="87%"><img style="vertical-align:" src="http://accsee.com/nsoft_logo.png"  /></td>
      <td width="8%">&nbsp;</td>
    </tr>
</table>
</div>
<strong style="color:green">MEETING OF MINUTES - COMPLETED REPORT (ALL) ON - <?php echo $day_before; ?></strong></div>
<table width="1200" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2">
           <table width="100%"  class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <thead>
              <tr class="">
                <th width="5%" >Meeting No</th>
                <th width="4%" >Index</th>
                <th width="10%" >Subject</th>
                <th width="11%" >Meeting Place</th>
                <th width="14%" >Concern</th>
                <th width="14%" >Recommendation</th>
                <th width="8%" >Concern Raised By</th>
                <th width="9%" >Action Plan</th>
                <th width="8%" >Responsible</th>
                <th width="6%" >Due Date</th>
                <th width="4%" >Initial Due Date</th>
                <th width="4%" >Completed By</th>
                <th width="7%" >Completed Date</th>
                </tr>
              </thead>
            <tbody>
              <?php 
		   $sql="SELECT
				concat('MOM',LPAD(other_meeting_minutes_header.`MINUTE_ID`,4,0))  as Meeting_No,
				other_meeting_minutes_details.TASK_ID,
				other_meeting_minutes_header.`SUBJECT`,
				other_meeting_minutes_header.MEETING_PLACE,
				other_meeting_minutes_details.CONCERN,
				other_meeting_minutes_details.RECOMMENDATION,
				other_meeting_minutes_details.ACTION_PLAN,
				other_meeting_minutes_details.RESPONSE_FROM_SENIOR_MANAGEMENT,
				other_meeting_minutes_details.DUE_DATE,
				/*IFNULL((
					SELECT
					other_meeting_minutes_details_history.DUE_DATE
					FROM `other_meeting_minutes_details_history`
					WHERE
					other_meeting_minutes_details_history.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID AND
					other_meeting_minutes_details_history.TASK_ID = other_meeting_minutes_details.TASK_ID
					ORDER BY
					other_meeting_minutes_details_history.ID ASC
					limit 1	
				),other_meeting_minutes_details.DUE_DATE) as DUE_DATE_INIT ,*/
				other_meeting_minutes_details.INITIAL_DUE_DATE as DUE_DATE_INIT, 
				other_meeting_minutes_details.COMPLETED_FLAG,
				(date(now()) - other_meeting_minutes_details.DUE_DATE) as DUE_DAYS,
				(SELECT
				GROUP_CONCAT(su.strFullName) as fn 
				FROM `sys_users` as su
				WHERE
				FIND_IN_SET(su.intUserId, other_meeting_minutes_details.RESPONSIBLE_LIST)) AS RESPONSIBLE_LIST,
				(SELECT
				GROUP_CONCAT(su.strFullName) as fn 
				FROM `sys_users` as su
				WHERE
				FIND_IN_SET(su.intUserId, other_meeting_minutes_details.CONCERN_RAISED_BY)) AS CONCERN_RAISED_BY,
				IF(other_meeting_minutes_details.COMPLETED_FLAG=1,'Yes','No') AS completed,
				(SELECT
				(su.strFullName) as fn 
				FROM `sys_users` as su
				WHERE
				su.intUserId = other_meeting_minutes_details.COMPLETED_BY) AS COMPLETED_BY,
				other_meeting_minutes_details.COMPETED_DATE
				FROM
				other_meeting_minutes_header
				INNER JOIN other_meeting_minutes_details ON other_meeting_minutes_header.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID
				left JOIN sys_users ON other_meeting_minutes_details.COMPLETED_BY = sys_users.intUserId
				WHERE 
				other_meeting_minutes_header.STATUS = 1 AND 
				date(COMPETED_DATE) = '$day_before'
				HAVING
				IFNULL(other_meeting_minutes_details.COMPLETED_FLAG,0) = 1
				ORDER BY
				other_meeting_minutes_details.TASK_ID ASC
 						";
			$result1 = $db->RunQuery($sql);
			$recordes = 0;
 			while($row1=mysqli_fetch_array($result1))
			{
  				$recordes++;
				$no					=$row1['TASK_ID'];
				$meetingNo			=$row1['Meeting_No'];
				$MEETING_PLACE		=$row1['MEETING_PLACE'];
				$SUBJECT			=$row1['SUBJECT'];
				$RECOMMENDATION		=$row1['RECOMMENDATION'];
				$CONCERN			=$row1['CONCERN'];
				$recomondation		=$row1['RECOMMENDATION'];
				$actionPlan			=$row1['ACTION_PLAN'];
				$respFromSenior		=$row1['RESPONSE_FROM_SENIOR_MANAGEMENT'];
				$dueDate			=$row1['DUE_DATE'];
				$dueDateInit		=$row1['DUE_DATE_INIT'];
				$dueDays			=$row1['DUE_DAYS'];
				$respoList			=$row1['RESPONSIBLE_LIST'];
				$consernRaised		=$row1['CONCERN_RAISED_BY'];
				$completed			=$row1['completed'];
				$completed_by		=$row1['COMPLETED_BY'];
				$completed_date		=$row1['COMPETED_DATE'];
				
				$fntcolor="#008040";
 	  ?>
              <tr class="normalfnt"    bgcolor="#FFFFFF" >
                <td align="left" style="color:<?Php echo $fntcolor; ?>"  class="normalfntMid" ><?php echo $meetingNo; ?></td>
                <td align="left" style="color:<?Php echo $fntcolor; ?>" class="normalfntMid"  ><?php echo $no; ?>&nbsp;</td>
                <td align="left" style="color:<?Php echo $fntcolor; ?>" class="normalfnt"  ><?php echo $SUBJECT; ?></td>
                <td align="left" style="color:<?Php echo $fntcolor; ?>" class="normalfnt"  ><?php echo $MEETING_PLACE; ?></td>
                <td align="left" style="color:<?Php echo $fntcolor; ?>" class="normalfnt"  ><?php echo $CONCERN; ?>&nbsp;</td>
                <td align="left" style="color:<?Php echo $fntcolor; ?>" class="normalfnt" ><?php echo $RECOMMENDATION; ?>&nbsp;</td>
                <td align="left" style="color:<?Php echo $fntcolor; ?>" class="normalfnt"  ><?php echo $consernRaised; ?>&nbsp;</td>
                <td align="left" style="color:<?Php echo $fntcolor; ?>" class="normalfnt"  ><?php echo $actionPlan; ?>&nbsp;</td>
                <td align="left" style="color:<?Php echo $fntcolor; ?>" class="normalfnt"  ><?php echo $respoList; ?>&nbsp;</td>
                <td align="center" style="color:<?Php echo $fntcolor; ?>" class="normalfntMid" ><?php echo $dueDate; ?>&nbsp;</td>
                <td align="center" style="color:<?Php echo $fntcolor; ?>" class="normalfntMid"  ><?php echo $dueDateInit; ?></td>
                <td align="center" style="color:<?Php echo $fntcolor; ?>" class="normalfntMid"  ><?php echo $completed_by; ?>&nbsp;</td>
                <td align="center" style="color:<?Php echo $fntcolor; ?>" class="normalfntMid"  ><?php echo $completed_date; ?>&nbsp;</td>
            </tr>              
              <?php 
 			}
	  ?>
               </tbody>
          </table>     
     </td>
    </tr>
     <tr height="40">
      <td width="317" align="center" class="normalfnt" > </td>
      <td width="873" align="center" class="normalfntRight"><span class="normalfntMid">Printed Date:<strong> <?php echo date("Y/m/d  H:i:s") ?></strong></span></td>
    </tr>
     <tr height="40">
      <td width="317" align="center" class="normalfnt" >&nbsp;</td>
      <td width="873" align="center" class="normalfntRight">&nbsp;</td>
    </tr>
</table>
    
 </div>
 <?php
      		echo  $body = ob_get_clean();

			$nowDate 			= date('Y-m-d');
 			$mailHeader			= "All-COMPLETED-MINUTES OF MEETING";
			$FROM_NAME			= "MOM-ALL DUE(NSOFT)";
			$FROM_EMAIL			= '';
		
			$cron_emails_to		=$obj_comm->getCronjobEmailIds($cronjobId,'TO');
			$cron_emails_cc		=$obj_comm->getCronjobEmailIds($cronjobId,'CC');
			$cron_emails_bcc	=$obj_comm->getCronjobEmailIds($cronjobId,'BCC');
		
			$mail_TO			= $obj_comm->getEmailList($cron_emails_to);
			$mail_CC			= $obj_comm->getEmailList($cron_emails_cc);
			$mail_BCC			= $obj_comm->getEmailList($cron_emails_bcc);
		
	  		$mail_TO			= str_replace("nish@screenlineholdings.com","nish@screenlineholdings.com,chairmanoffice@screenlineholdings.com",$mail_TO);
	  		$mail_CC			= str_replace("nish@screenlineholdings.com","nish@screenlineholdings.com,chairmanoffice@screenlineholdings.com",$mail_CC);
	  		$mail_BCC			= str_replace("nish@screenlineholdings.com","nish@screenlineholdings.com,chairmanoffice@screenlineholdings.com",$mail_BCC);
		  
		  if($recordes>0){
				//live
				insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
				//testing
				//sendMessage($FROM_EMAIL,$FROM_NAME,'roshan.nsoft@gmail.com',$mailHeader,$body,'','');
			}
 ?>
 
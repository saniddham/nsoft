<?php
ini_set('max_execution_time', 11111111) ;
session_start();
//ini_set('display_errors',1);
  
include_once $_SESSION['ROOT_PATH']."class/dailyMails/revenue/cls_marketer_revenue_get.php";
include_once $_SESSION['ROOT_PATH']."class/dailyMails/revenue/cls_marketer_revenue_set.php";
 
//$db 									= new LoginDBManager();
$obj_revenue_get						= new cls_marketer_revenue_get($db);
$obj_revenue_set						= new cls_marketer_revenue_set($db);
 
$printDateArr 							= array();
$dateArr								= array();
$fullDateArr							= array();
$dateNameArray							= array();
$dateNameArray							= array();
$cumulativeDay							= array();
$cumulativeDisp							= array();
$DayWiseDispSumArr 						= array();
$DayWiseClustDispSumArr1_without_Others	= array();
$cumulativeClustDisp_without_Others		= array();//////////////////////////////////////////////
 
$companyId 								= 	$_SESSION['CompanyID'];
$locationId 							= 	$_SESSION['CompanyID'];
$intUser  								= 	$_SESSION["userId"];
$mainPath 								= 	$_SESSION['mainPath'];
$thisFilePath 							=  	$_SERVER['PHP_SELF'];

$toCurrency  							=	1;		//1=USD,2=LKR


$year			= date('Y', strtotime('-1 day', strtotime(date('Y-m-d'))));
$month			= date('m', strtotime('-1 day', strtotime(date('Y-m-d'))));
$day			= date('d', strtotime('-1 day', strtotime(date('Y-m-d'))));

$startDate								=	1;
$monthName								=	$obj_comm->getMonthName($month);

$ts 									= 	strtotime($monthName." ".$year);
$endDate								= 	date('t', $ts); 
$startDate 								= 	date("Y-m-d", mktime(0, 0, 0, $month, $startDate, $year));
$endDate 								= 	date("Y-m-d", mktime(0, 0, 0, $month, $endDate, $year));
$rptDate 								= 	date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))); 



$i										=0;
$temp									='';
while ($temp < $endDate) {
 $temp 				= date("Y-m-d", mktime(0, 0, 0, $month, 1+$i, $year));
 $weekday 			= date('l', strtotime($temp)); // note: first arg to date() is lower-case L
 $printDateArr[$i]	=(1+$i)."-".substr($monthName,0,3);
 $dateArr[$i]		=(1+$i);
 $fullDateArr[$i]	=$temp;
 $dateNameArray[$i]	=$weekday; 
 $i++;
}
$noOfDays 			= $i;
 $currencyName1		=$obj_comm->getCurrencyName($toCurrency);
 ?>
<title>Revenue Report</title>
<style>
.normalfntBlue {
	font-family: Verdana;
	font-size: 10px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntHeader {
	font-family: Verdana;
	font-size: 15px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
	font-style:bold;
}
.normalfnt {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: right;
}
.compulsoryRed {
	color: #F00;
}
</style>
<div align="center">
  <div style="background-color:#FFF" ><strong>
    <div align="left"><img style="vertical-align:" src="http://accsee.com/nsoft_logo.png"  /></div>
    DAILY DISPATCHED SUMMERY WITH DEFECT % - <?php echo "  (  ".  $rptDate.' )  Process Time('.date('Y-m-d H:i:s').')' ?></strong><strong></strong></div>
  <div style="background-color:'#FF'" ><strong></strong></div>
  <table width="1500" border="0" align="center" bgcolor="">
    <tr>
      <td><table width="1500">
          <tr>
            <td width="10">&nbsp;</td>
            <td colspan="7" class="normalfnt"><table width="1705" border="1" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="normalfntHeader">
              <th colspan="12" bgcolor="#F9F9F9">&nbsp;</th>
              <th colspan="8" align="center" bgcolor="#BADCDC" ><strong>Today's Qty</strong></th>
              <th colspan="8" align="center" bgcolor="#E5DFEC"><strong>Cumulative Qty</strong></th>
              </tr>
            
            <tr class="normalfntHeader">
            <th width="42" rowspan="2" bgcolor="#F9F9F9"><strong>Style</strong></th>
            <th width="61" rowspan="2" bgcolor="#F9F9F9"><strong>Graphic</strong></th>
            <th width="52" rowspan="2" bgcolor="#F9F9F9"><strong>Colour</strong></th>
            <th width="60" rowspan="2" bgcolor="#F9F9F9"><strong>Print Part</strong></th>
            <th width="78" rowspan="2" bgcolor="#F9F9F9"><strong>Print Combo</strong></th>
            <th width="114" rowspan="2" bgcolor="#F9F9F9"><strong>Production Plant</strong> </th>
            <th width="57" rowspan="2" bgcolor="#F9F9F9"><strong>Customer Location</strong></th>
            <th width="57" rowspan="2" bgcolor="#F9F9F9"><strong>Schedule No</strong></th>
            <th width="71" rowspan="2" bgcolor="#F9F9F9"><strong>Customer PO</strong></th>
            <th width="71" rowspan="2" bgcolor="#F9F9F9"><strong>Sales Order</strong></th>
            <th width="71" rowspan="2" bgcolor="#F9F9F9"><strong>Order Qty</strong></th>
            <th width="41" rowspan="2" bgcolor="#F9F9F9"><strong>In Qty</strong></th>
            <th width="111" rowspan="2" bgcolor="#BADCDC"><strong>Dispatched Qty</strong></th>
            <th width="80" rowspan="2" bgcolor="#BADCDC"><strong>Sample Qty</strong></th>
            <th width="61" rowspan="2" bgcolor="#BADCDC"><strong>Good Qty</strong></th>
            <th colspan="2" bgcolor="#BADCDC"><strong>P-D</strong></th>
            <th colspan="2" bgcolor="#BADCDC"><strong>F-D</strong></th>
            <th width="63" rowspan="2" bgcolor="#BADCDC"><strong>Cut Ret Qty</strong></th>
            <th width="109" rowspan="2" bgcolor="#E5DFEC"><strong>Dispatched Qty</strong></th>
            <th width="80" rowspan="2" bgcolor="#E5DFEC"><strong>Sample Qty</strong></th>
            <th width="61" rowspan="2" bgcolor="#E5DFEC"><strong>Good Qty</strong></th>
            <th colspan="2" bgcolor="#E5DFEC"><strong>P-D Qty</strong></th>
            <th colspan="2" bgcolor="#E5DFEC"><strong>F-D Qty</strong></th>
            <th width="101" rowspan="2" bgcolor="#E5DFEC"><strong>Cut Return Qty</strong></th>
            </tr>
<tr class="normalfntHeader">
              <th width="30" bgcolor="#BADCDC"><strong>Qty</strong></th>
              <th width="19" bgcolor="#BADCDC"><strong> %</strong></th>
              <th width="30" bgcolor="#BADCDC"><strong>Qty</strong></th>
              <th width="19" bgcolor="#BADCDC"><strong>%</strong></th>
              <th width="33" bgcolor="#E5DFEC"><strong>Qty</strong></th>
              <th width="21" bgcolor="#E5DFEC"><strong> %</strong></th>
              <th width="33" bgcolor="#E5DFEC"><strong>Qty</strong></th>
              <th width="21" bgcolor="#E5DFEC"><strong> %</strong></th>
            </tr> 
            <?php 
			$records	=0;
			$result	=getDispatchSummery($customer,$dateFrom,$dateTo);
			while($row 	= mysqli_fetch_array($result)){
				$records++;
			?>           
                <tr class="normalfnt">
                <td nowrap="nowrap" bgcolor="#F9F9F9">&nbsp;<?php echo $row['strStyleNo']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#F9F9F9">&nbsp;<?php echo $row['strGraphicNo']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#F9F9F9">&nbsp;<?php echo $row['colour']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#F9F9F9">&nbsp;<?php echo $row['part']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#F9F9F9">&nbsp;<?php echo $row['strCombo']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#F9F9F9">&nbsp;<?php echo $row['plant']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#F9F9F9">&nbsp;<?php echo $row['dispatchPlant']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#F9F9F9">&nbsp;<?php echo $row['orderNo']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#F9F9F9">&nbsp;<?php echo $row['customerPO']; ?></td>
                <td nowrap="nowrap" bgcolor="#F9F9F9">&nbsp;<?php echo $row['salesOrder']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#F9F9F9" align="right">&nbsp;<?php echo $row['poQty']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#F9F9F9" align="right">&nbsp;<?php echo $row['fabInQty']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#BADCDC" align="right">&nbsp;<?php echo $row['todayDispQty']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#BADCDC" align="right">&nbsp;<?php echo $row['todaySampleqty']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#BADCDC" align="right">&nbsp;<?php echo $row['todayGoodqty']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#BADCDC" align="right">&nbsp;<?php echo $row['todayPDqty']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="<?php if(round(($row['todayPDqty']/$row['todayDispQty']*100),2)>1)  { echo '#FF4A4A';  } else { echo '#BADCDC';  } ?>" align="right">&nbsp;<?php echo round($row['todayPDqty']/$row['todayDispQty']*100,2); ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#BADCDC" align="right">&nbsp;<?php echo $row['todayFDqty']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#BADCDC" align="right">&nbsp;<?php echo round($row['todayFDqty']/$row['todayDispQty']*100,2); ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#BADCDC" align="right">&nbsp;<?php echo $row['todayCutRetqty']; ?>&nbsp;</td>
              
                <td nowrap="nowrap" bgcolor="#E5DFEC" align="right">&nbsp;<?php echo $row['dispCumulative']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#E5DFEC" align="right">&nbsp;<?php echo $row['dispSample']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#E5DFEC" align="right">&nbsp;<?php echo $row['dispGood']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#E5DFEC" align="right">&nbsp;<?php echo $row['dispPDQty']; ?>&nbsp;</td>
                <td nowrap="nowrap"  bgcolor="<?php if(round(($row['dispPDQty']/$row['dispCumulative']*100),2)>1)  { echo '#FF4A4A';  } else { echo '#E5DFEC';  } ?>" align="right">&nbsp;<?php echo round($row['dispPDQty']/$row['dispCumulative']*100,2); ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#E5DFEC" align="right">&nbsp;<?php echo $row['dispFDQty']; ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#E5DFEC" align="right">&nbsp;<?php echo round($row['dispFDQty']/$row['dispCumulative']*100,2); ?>&nbsp;</td>
                <td nowrap="nowrap" bgcolor="#E5DFEC" align="right">&nbsp;<?php echo $row['dispCutRetQty']; ?>&nbsp;</td>
                </tr>
            <?php
			}
			?>
            </table></td>
            <td width="10">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="8" height="15"></td>
          </tr>
        </table></td>
      <td width="10">&nbsp;</td>
    </tr>
  </table>
  </td>
  </tr>
  <tr height="90" >
    <td align="center" class="normalfntMid"></td>
  </tr>
  </table>
</div>
 
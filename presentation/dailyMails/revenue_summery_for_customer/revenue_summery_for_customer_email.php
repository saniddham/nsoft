<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

date_default_timezone_set('Asia/Colombo');
ini_set('max_execution_time', 11111111) ;
//ini_set('display_errors',1);
include_once $_SESSION['ROOT_PATH']."class/tables/mst_customer.php";

$mst_customer			= new mst_customer($db);
$cronjobId				= 54;
$dateTo					= date('Y-m-d').' 07:00:00';
$dateFrom				= date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))).' 07:00:00';

$result = $mst_customer->getCustomers_to_send_dispatch_summery();
while($rowCust = mysqli_fetch_array($result))
{
	$customer	= $rowCust['customerId'];
	include 'revenue_summery_for_customer_email_common.php';
}

function getDispatchSummery($customer,$dateFrom,$dateTo){
	
	global $db;
	$sql	="SELECT
trn_orderdetails.strStyleNo,
trn_orderdetails.strGraphicNo,
trn_orderheader.strCustomerPoNo as customerPO,
trn_orderdetails.strSalesOrderNo as salesOrder,
(SELECT
mst_colors_ground.strName
FROM
trn_sampleinfomations_details
INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
WHERE
trn_sampleinfomations_details.intSampleNo = trn_orderdetails.intSampleNo AND
trn_sampleinfomations_details.intSampleYear = trn_orderdetails.intSampleYear AND
trn_sampleinfomations_details.intRevNo = trn_orderdetails.intRevisionNo AND
trn_sampleinfomations_details.strPrintName = trn_orderdetails.strPrintName AND
trn_sampleinfomations_details.strComboName = trn_orderdetails.strCombo limit 1) AS colour,
mst_part.strName AS part,
trn_orderdetails.strCombo,
mst_plant.strPlantName AS plant,
-- mst_customer_locations_header.strName AS dispatchPlant, 
(SELECT
mst_customer_locations_header.strName
FROM
mst_customer_locations
Inner Join mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId
WHERE
mst_customer_locations.intCustomerId =  trn_orderheader.intCustomer and 
mst_customer_locations.intLocationId =  ware_fabricdispatchheader.intCustLocation) as dispatchPlant,
concat(trn_orderdetails.intOrderNo,'/',trn_orderdetails.intOrderYear) as orderNo,
(select sum(s_od.intQty) from trn_orderdetails as s_od 
where s_od.intOrderNo = trn_orderdetails.intOrderNo AND s_od.intOrderYear = trn_orderdetails.intOrderYear 
AND s_od.intSalesOrderId = trn_orderdetails.intSalesOrderId 
) AS poQty,
((SELECT
sum(ware_fabricreceiveddetails.dblQty)
FROM
ware_fabricreceiveddetails
INNER JOIN ware_fabricreceivedheader ON ware_fabricreceiveddetails.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo AND ware_fabricreceiveddetails.intFabricReceivedYear = ware_fabricreceivedheader.intFabricReceivedYear
WHERE
ware_fabricreceivedheader.intOrderNo = trn_orderdetails.intOrderNo AND
ware_fabricreceivedheader.intOrderYear = trn_orderdetails.intOrderYear AND
ware_fabricreceiveddetails.intSalesOrderId = trn_orderdetails.intSalesOrderId AND 
ware_fabricreceivedheader.intStatus=1    )) AS fabInQty,
((SELECT
sum(ifnull(ware_fabricdispatchdetails.dblGoodQty,0)+ifnull(ware_fabricdispatchdetails.dblEmbroideryQty,0)+ifnull(ware_fabricdispatchdetails.dblPDammageQty,0)+ifnull(ware_fabricdispatchdetails.dblFdammageQty,0)+ifnull(ware_fabricdispatchdetails.dblSampleQty,0)+ifnull(ware_fabricdispatchdetails.dblCutRetQty,0)) dblDispQty
FROM
ware_fabricdispatchdetails
INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear AND 
ware_fabricdispatchheader.intStatus=1    
WHERE
ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND
ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND
ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId     AND 
ware_fabricdispatchheader.intStatus=1    
)) AS dispCumulative,
((SELECT
sum(ifnull(ware_fabricdispatchdetails.dblSampleQty,0)) dblDispQty
FROM
ware_fabricdispatchdetails
INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
WHERE
ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND
ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND
ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId   AND 
ware_fabricdispatchheader.intStatus=1      
)) AS dispSample,
((SELECT
sum(ifnull(ware_fabricdispatchdetails.dblGoodQty,0)) dblDispQty
FROM
ware_fabricdispatchdetails
INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
WHERE
ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND
ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND
ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId  AND 
ware_fabricdispatchheader.intStatus=1       
)) AS dispGood,
((SELECT
sum(ifnull(ware_fabricdispatchdetails.dblPDammageQty,0)) dblDispQty
FROM
ware_fabricdispatchdetails
INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
WHERE
ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND
ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND
ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId  AND 
ware_fabricdispatchheader.intStatus=1        
)) AS dispPDQty,
((SELECT
sum(ifnull(ware_fabricdispatchdetails.dblFdammageQty,0)) dblDispQty
FROM
ware_fabricdispatchdetails
INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
WHERE
ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND
ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND
ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId  AND 
ware_fabricdispatchheader.intStatus=1        
)) AS dispFDQty,
((SELECT
sum(ifnull(ware_fabricdispatchdetails.dblCutRetQty,0)) dblDispQty
FROM
ware_fabricdispatchdetails
INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
WHERE
ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND
ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND
ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId  AND 
ware_fabricdispatchheader.intStatus=1        
)) AS dispCutRetQty,
Sum(ifnull(ware_stocktransactions_fabric.dblQty*-1,0)) AS todayDispQty,
Sum(if(ware_stocktransactions_fabric.strType='Dispatched_S',ifnull(ware_stocktransactions_fabric.dblQty*-1,0),0)) AS todaySampleqty,
Sum(if(ware_stocktransactions_fabric.strType='Dispatched_G',ifnull(ware_stocktransactions_fabric.dblQty*-1,0),0)) AS todayGoodqty,
Sum(if(ware_stocktransactions_fabric.strType='Dispatched_P',ifnull(ware_stocktransactions_fabric.dblQty*-1,0),0)) AS todayPDqty,
Sum(if(ware_stocktransactions_fabric.strType='Dispatched_F',ifnull(ware_stocktransactions_fabric.dblQty*-1,0),0)) AS todayFDqty,
Sum(if(ware_stocktransactions_fabric.strType='Dispatched_CUT_RET',ifnull(ware_stocktransactions_fabric.dblQty*-1,0),0)) AS todayCutRetqty

FROM
ware_stocktransactions_fabric
INNER JOIN trn_orderdetails ON ware_stocktransactions_fabric.intOrderNo = trn_orderdetails.intOrderNo 
AND ware_stocktransactions_fabric.intOrderYear = trn_orderdetails.intOrderYear 
AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
INNER JOIN trn_orderheader on trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo and trn_orderdetails.intOrderYear
INNER JOIN ware_fabricdispatchheader ON ware_stocktransactions_fabric.intDocumentNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_stocktransactions_fabric.intDocumentYear = ware_fabricdispatchheader.intBulkDispatchNoYear
LEFT JOIN mst_part ON ware_stocktransactions_fabric.intPart = mst_part.intId
LEFT JOIN mst_locations ON ware_stocktransactions_fabric.intLocationId = mst_locations.intId
LEFT JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId 
-- LEFT JOIN mst_customer_locations on ware_fabricdispatchheader.intCustLocation = mst_customer_locations.intLocationId 
-- LEFT Join mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId
WHERE ware_stocktransactions_fabric.strType LIKE '%Disp%' AND 
(LEFT(trn_orderheader.strCustomerPoNo,1) = '4' OR 
LEFT(trn_orderheader.strCustomerPoNo,2) = '04') AND 
trn_orderheader.intCustomer = '$customer' AND 
ware_stocktransactions_fabric.dtDate > '$dateFrom' AND 
ware_stocktransactions_fabric.dtDate <= '$dateTo' 
group by trn_orderdetails.intOrderNo, trn_orderdetails.intOrderYear, trn_orderdetails.intSalesOrderId
";
	return $result 		= $db->RunQuery($sql);
}

function customerLocationEmails($customer,$cronjobId,$type){

	global $db;
	$sql			="SELECT 
mst_customer_event_emails.RECIPIENT_TYPE, 	
GROUP_CONCAT(mst_customer_event_emails.EMAIL) AS EMAILS 
FROM
mst_customer_event_emails
WHERE
mst_customer_event_emails.CUSTOMER = '$customer' AND
mst_customer_event_emails.CRONJOB_ID = '$cronjobId' AND
mst_customer_event_emails.RECIPIENT_TYPE = '$type'
GROUP BY
mst_customer_event_emails.CUSTOMER,
mst_customer_event_emails.CRONJOB_ID,
mst_customer_event_emails.RECIPIENT_TYPE
";
	$result	= $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row['EMAILS'];
	
	
	
	
}

  ?>


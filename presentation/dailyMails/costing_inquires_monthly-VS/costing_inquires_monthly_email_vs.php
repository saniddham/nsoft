<?php

session_start();

date_default_timezone_set('Asia/Colombo');
ini_set('max_execution_time', 11111111) ;
ini_set('display_errors',0);

$thisFilePath           =  $_SERVER['PHP_SELF'];
$cronjobId 		=   63;
include_once $_SESSION['ROOT_PATH']."class/dailyMails/costingInquiriesMonthly/cls_costing_inquiries_monthly.php";

$obj_costing_inquiries_monthly	= new cls_costing_inquiries_monthly($db);

	
		ob_start();
		
		$current_year			= date('Y');			
                $current_month			= date('M');
                $current_month_num		= date('m');
                
                include "costing_inquires_monthly_vs.php";	
		
		$body = ob_get_clean();
		
		echo $body;
		//---------
 		$nowDate 		= date('Y-m-d');
  		$mailHeader		= " COUNT OF PRICING INQUIRIES (VS) FOR THE MONTH OF "." ".strtoupper(date('F')). " " .(date('Y')-1).'-'.(date('Y')).'  '.'('.(date("d-m-Y")).')'."";
		$FROM_NAME		= "NSOFT";
		$FROM_EMAIL		= '';
		
		$cron_emails_to	= $obj_comm->getCronjobEmailIds($cronjobId,'TO');
                $cron_emails_cc	= $obj_comm->getCronjobEmailIds($cronjobId,'CC');

                $mail_TO		= $obj_comm->getEmailList($cron_emails_to);
                $mail_CC		= $obj_comm->getEmailList($cron_emails_cc);
                $mail_BCC		= $obj_comm->getEmailList($cron_emails_bcc);

                insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
	

  ?>
 
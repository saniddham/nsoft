<?php

$pervious_year			= $current_year-1;

/*if($current_month==4)
	$noOfMonths			= 1;
else if($current_month>4)
	$noOfMonths			=$current_month-3;
else
	$noOfMonths			=$current_month+9; */
	$noOfMonths			=12;
 /*
for($j=0; $j<=$noOfMonths ; $j++){
	$month				=$current_month-$j;
	$year 				= $current_year;
	if($month<=0){
		$month			=$month+12;
		$year 			= $pervious_year;
	}
	$monthArr[$j]		=$month;
	$monthNameArr[$j]	=$obj_common->getMonthName($month);
	$yearArr[$j]		=$year;
	$headerMonthArr[$j]	=$year."-".substr($monthNameArr[$j],0,3);
} */

for($j=0; $j<=$noOfMonths ; $j++){
	$month				=$current_month+$j;
	$year 				= $pervious_year;
	if($month > 12){
		$month			=$month-12;
		$year 			= $current_year;
	}
	$monthArr[$j]		=$month;
	$monthNameArr[$j]	=$obj_comm->getMonthName($month);
	$yearArr[$j]		=$year;
	$headerMonthArr[$j]	=$year."-".substr($monthNameArr[$j],0,3);
}

?>
<div align="center">
  <div style="background-color:#FFF" ><strong>
  <?php echo "  (  ".  $current_year.'/'.($current_year-1).' ) ' ?></strong><strong></strong></div>
  <table width="1500" border="0" align="center" bgcolor="">
    <tr>
      <td><table width="1500">
          <tr>
            <td width="10">&nbsp;</td>
            <td colspan="7" class="normalfnt"><table width="1500" border="1" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
                <!----Plant wise header------------------------------------------------------------>
                <tr class="" bgcolor="">
                  <th width="50" colspan="3" class="normalfnt" ><b>Plant Wise Revenue Report</b></th>
                  <?php
					 for($j=0; $j<$noOfMonths ; $j++){ 
					  $bgCol=$reportColor;
  					  ?>
				  <th width="80" class="normalfntMid" bgcolor="<?php echo $bgCol ; ?>" ><?php //echo $headerMonthArr[$j] ?></th>
                  <?php
					 }
				  ?>
                  <th width="80" class="normalfntMid">Total</th>
                </tr>
                <tr class="" bgcolor="<?php echo $reportColor; ?>">
                  <th bgcolor="#006699"><font color="<?php echo $reportColor; ?>">Plant</font></th>
                  <th bgcolor="#006699"><font color="<?php echo $reportColor; ?>">Annual Target</font></th>
                  <th bgcolor="#006699" ><font color="<?php echo $reportColor; ?>">Monthly Target</font></th>
                  <?php
			 	 for($j=0; $j<$noOfMonths ; $j++){
				  $bgCol=$reportColor;
 				  $cumulativeMonth[$j]=1;
				  ?>
                  <th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php echo $bgCol ; ?>" ><?php echo $headerMonthArr[$j]; ?></th>
                  <?php
			  	}
				  ?>
                  <th width="80">&nbsp;</th>
                </tr>
                <!----End of Plant wise header------------------------------------------------------------>
               
            <!----while clusters???????????------------------------>
           <?php
				//$records=0;
				//$cluster_result = $obj_common->getClusters('REVENUE');
               // while($rowClust = mysqli_fetch_array($cluster_result))
               // {
				//	$records++;
               //     $clusterId		= $rowClust['NO'];
               //     $clusterName 	= $rowClust['CLUSTER_TYPE'];
         
					$cumulativeMonth							= array();
					$cumulativeDisp								= array();
					$MonthWiseDispSumArr 						= array();
					$MonthWiseClustDispSumArr1_without_Others	= array();
					$cumulativeClustDisp_without_Others			= array();
		   ?>
                <!----Plant wise revnue------------------------------------------------------------------->
                <?php 
				 $result1=$obj_revenue_get->get_cluster_plants($clusterId);  
				 $anualTargtTot =0;
				 $monthlyTargtTot =0;
				 $monthTargtTot =0;
				 $totPlantDispQty=0;
				?> 
             <!----------------------------------------------->
                <tr class="normalfnt"  bgcolor="#E2E2E2">
                  <td class="normalfnt" colspan="<?php echo $noOfMonths+4; ?>" ><b><?php echo $clusterName; ?></b></td>
                </tr>
             <!---------------------------------------------->
				<?php
				while($row=mysqli_fetch_array($result1))
				{
					$plantId=$row['intPlantId'];
					$plantWiseTotDispQty['$plantId']=0;
	 			 ?>
                <tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
                  <td class="normalfnt" ><?php echo $row['strPlantName'];?></td>
                  <td class="normalfntRight" align="right" ><?php echo number_format($row['dblAnualaTarget'],0);?></td>
                  <td class="normalfntRight" align="right" ><?php echo number_format($row['dblMonthlyTarget'],0);?></td>
                  <?php 
				  for($j=0; $j<$noOfMonths ; $j++){
					$bgCol								='';
 					$month								=$monthArr[$j];
					$year								=$yearArr[$j];
 					$dispatchValue						=$obj_revenue_get->get_old_cluster_plant_monthly_revenue($plantId,$year,$month,$toCurrency);  
					$MonthWiseDispSumArr[$j]			+=$dispatchValue;  
					$plantWiseTotDispQty['$plantId']	+=round($dispatchValue);
					$totPlantDispQty 					+=round($dispatchValue);
 				  ?>
					  <td class="normalfntRight"  "<?php echo $bgCol; ?>"><?php echo number_format($dispatchValue,0); ?></td>
					  <?php
				  }
				  ?>
                  <td width="80" class="normalfntRight"><?php echo number_format($plantWiseTotDispQty['$plantId'],0);?></td>
                </tr>
                <?php 
				 $anualTargtTot		 +=$row['dblAnualaTarget'];
				 $monthlyTargtTot 	+=$row['dblMonthlyTarget'];
				 $monthTargtTot		 +=$row['dblMonthTarget'];
				}
	  ?>
                <tr class="normalfnt"  bgcolor="#CCCCCC">
                  <td class="normalfnt" >&nbsp;</td>
                  <td class="normalfntRight" ><b><?php echo number_format($anualTargtTot,0); ?></b></td>
                  <td class="normalfntRight" ><b><?php echo number_format($monthlyTargtTot,0); ?></b></td>
                  <?php
				  for($j=0; $j<$noOfMonths ; $j++){
				  ?>
					  <td class="normalfntRight" ><b><?php echo number_format($MonthWiseDispSumArr[$j],0); ?></b></td>
					  <?php
				  }
				  ?>
                  <td class="normalfntRight"  ><?php echo number_format($totPlantDispQty,0); ?></td>
                </tr>
                <!----End of Plant wise revnue----------------------------------------------------------->
                <?php
               // }
				?>
                <!----End of while cluster----------------------------------------------------------->
               
              </table></td>
            <td width="10">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="8" height="15"></td>
          </tr>
        </table></td>
      <td width="10">&nbsp;</td>
    </tr>
  </table>
  </td>
  </tr>
  <tr height="90" >
    <td align="center" class="normalfntMid"></td>
  </tr>
  </table>
</div>

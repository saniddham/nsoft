<?php
         
					$cumulativeMonth							= array();
					$cumulativeDisp								= array();
					$MonthWiseDispSumArr 						= array();
					$MonthWiseClustDispSumArr1_without_Others	= array();
					$cumulativeClustDisp_without_Others			= array();
		   ?>
                <!----Plant wise revnue------------------------------------------------------------------->
                <?php 
				 $result1=$obj_revenue_get->get_sub_cluster_plants($sub_clusterId,$year);  
				 $anualTargtTot =0;
				 $monthlyTargtTot =0;
				 $monthTargtTot =0;
				 $totPlantDispQty=0;
				?> 
             <!----------------------------------------------->
                <tr class="normalfnt"  bgcolor="#E2E2E2">
                  <td class="normalfnt" colspan="<?php echo $noOfMonths+4; ?>" ><b><?php echo $sub_clusterName; ?></b></td>
                </tr>
             <!---------------------------------------------->
				<?php
				while($row=mysqli_fetch_array($result1))
				{
					$plantId=$row['intPlantId'];
					$plantWiseTotDispQty['$plantId']=0;
	 			 ?>
                <tr class="normalfnt"  bgcolor="<?php echo $reportColor; ?>">
                  <td class="normalfnt" ><?php echo $row['strPlantName'];?></td>
                  <td class="normalfntRight" align="right" ><?php echo number_format($row['TARGET_ANUAL'],0);?></td>
                  <td class="normalfntRight" align="right" ><?php echo number_format($row['TARGET_MONTHLY'],0);?></td>
                  <?php 
				  for($j=0; $j<$noOfMonths ; $j++){
					$bgCol								='';
 					$month								=$monthArr[$j];
					$year								=$yearArr[$j];
 					$dispatchValue						=$obj_revenue_get->get_old_cluster_plant_monthly_revenue($plantId,$year,$month,$toCurrency);  
					$MonthWiseDispSumArr[$j]			+=$dispatchValue;  
					$plantWiseTotDispQty['$plantId']	+=round($dispatchValue);
					$totPlantDispQty 					+=round($dispatchValue);
 				  ?>
					  <td class="normalfntRight"  "<?php echo $bgCol; ?>"><?php echo number_format($dispatchValue,0); ?></td>
					  <?php
				  }
				  ?>
                  <td width="80" class="normalfntRight"><?php echo number_format($plantWiseTotDispQty['$plantId'],0);?></td>
                </tr>
                <?php 
				 $anualTargtTot		 +=$row['TARGET_ANUAL'];
				 $monthlyTargtTot 	+=$row['TARGET_MONTHLY'];
				 $monthTargtTot		 +=$row['TARGET_MONTHLY'];
				}
	  ?>
                <tr class="normalfnt"  bgcolor="#CCCCCC">
                  <td class="normalfnt" >&nbsp;</td>
                  <td class="normalfntRight" ><b><?php echo number_format($anualTargtTot,0); ?></b></td>
                  <td class="normalfntRight" ><b><?php echo number_format($monthlyTargtTot,0); ?></b></td>
                  <?php
				  for($j=0; $j<$noOfMonths ; $j++){
				  ?>
					  <td class="normalfntRight" ><b><?php echo number_format($MonthWiseDispSumArr[$j],0); ?></b></td>
					  <?php
				  }
				  ?>
                  <td class="normalfntRight"  ><b><?php echo number_format($totPlantDispQty,0); ?></b></td>
                </tr>
                <!----End of Plant wise revnue----------------------------------------------------------->

<?php
ini_set('max_execution_time', 11111111) ;
session_start();
//ini_set('display_errors',1);
  
include_once $_SESSION['ROOT_PATH']."class/dailyMails/revenue_monthly/cls_marketer_revenue_get.php";

$obj_revenue_get						= new cls_marketer_revenue_get($db);
 
 
$printMonthArr 							= array();
$monthArr								= array();
$monthNameArray							= array();
$monthNameArray							= array();
 
$companyId 								= 	$_SESSION['CompanyID'];
$locationId 							= 	$_SESSION['CompanyID'];
$intUser  								= 	$_SESSION["userId"];
$mainPath 								= 	$_SESSION['mainPath'];
$thisFilePath 							=  	$_SERVER['PHP_SELF'];

$toCurrency  			= 1;//1=USD,2=LKR
$currencyName			= '';
$currencyName1			= $obj_comm->getCurrencyName($toCurrency);
$pervious_year			= $current_year-1;

if($current_month==4)
	$noOfMonths			= 1;
else if($current_month>4)
	$noOfMonths			=$current_month-3;
else
	$noOfMonths			=$current_month+9;
 
 
for($j=0; $j<=$noOfMonths ; $j++){
	$month				=$current_month-$j;
	$year 				= $current_year;
	if($month<=0){
		$month			=$month+12;
		$year 			= $pervious_year;
	}
	$monthArr[$j]		=$month;
	$monthNameArr[$j]	=$obj_comm->getMonthName($month);
	$yearArr[$j]		=$year;
	$headerMonthArr[$j]	=$year."-".substr($monthNameArr[$j],0,3);
}

	$row 				= $obj_comm->getClusterReportCompany($cluster);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];
 ?>
 <title>Revenue Report</title>
<style>
.normalfntBlue {
	font-family: Verdana;
	font-size: 10px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfnt {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: right;
}
.compulsoryRed {
	color: #F00;
}
</style>
<div align="center">
  <div style="background-color:#FFF" ><strong>
    <div align="left"><img style="vertical-align:" src="http://accsee.com/nsoft_logo.png"  /></div>
    MONTHILY REVENUE REPORT - <?php echo "  (  ".  $current_year.'/'.($current_year-1).' )  Process Time('.date('Y-m-d H:i:s').')' ?></strong><strong></strong></div>
  <div style="background-color:'#FF'" ><strong>(Currency - <?php echo $currencyName1?>) </strong><strong></strong></div>
  <table width="1500" border="0" align="center" bgcolor="">
    <tr>
      <td><table width="1500">
          <tr>
            <td width="10">&nbsp;</td>
            <td colspan="7" class="normalfnt"><table width="1500" border="1" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
                <!----Plant wise header------------------------------------------------------------>
                <tr class="" bgcolor="">
                  <th colspan="<?php echo $noOfMonths+4  ?>" class="normalfnt" ><b>Plant Wise Revenue Report</b></th>
                </tr>
                <tr class="" bgcolor="<?php echo $reportColor; ?>">
                  <th width="328" height="20" bgcolor="#006699"><font color="<?php echo $reportColor; ?>">Plant</font></th>
                  <th width="240" bgcolor="#006699"><font color="<?php echo $reportColor; ?>">Annual Target</font></th>
                  <th width="244" bgcolor="#006699" ><font color="<?php echo $reportColor; ?>">Monthly Target</font></th>
                  <?php
			 	 for($j=0; $j<$noOfMonths ; $j++){
				  $bgCol=$reportColor;
 				  $cumulativeMonth[$j]=1;
				  ?>
                  <th width="532" align="center" bgcolor="<?php echo $bgCol ; ?>"  class="normalfntBlue normalfntMid" ><b><?php echo $headerMonthArr[$j]; ?></b></th>
                  <?php
			  	}
				  ?>
                  <th class="normalfntMid" width="144"><b>Total</b></th>
                </tr>
                <!----End of Plant wise header------------------------------------------------------------>
               
            <!----while clusters???????????------------------------>
           <?php
		   
				$records=0;
				$cluster_result = $obj_comm->getMainClusters();
                while($rowClust = mysqli_fetch_array($cluster_result))
                {
					
					$reportId   	=5;
					$company 		='';
					$cronjobId 		=33;
					$clusterId		= $rowClust['ID'];
					$clusterName 	= $rowClust['NAME'];
					
					$reportType = $obj_comm->getMainClusterReportType($rowClust['ID'],$reportId);
					
				
					if($reportType==1){
						
						$records++;
						include 'revenueReport_main_cluster_marketer_wise.php';
						
					}
					else if($reportType==2){
						
						$records++;
						$sub_cluster_result = $obj_comm->getSubClusters($clusterId);
						
						while($rowSubClust = mysqli_fetch_array($sub_cluster_result))
						{	
							$sub_clusterId		= $rowSubClust['ID'];
							$sub_clusterName 	= $rowSubClust['NAME'];
							include 'revenueReport_sub_cluster_marketer_wise.php';
						}
						
					}
					
                }
				?>
                <!----End of while cluster----------------------------------------------------------->
               
              </table></td>
            <td width="10">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="8" height="15"></td>
          </tr>
        </table></td>
      <td width="10">&nbsp;</td>
    </tr>
  </table>
  </td>
  </tr>
  <tr height="90" >
    <td align="center" class="normalfntMid"></td>
  </tr>
  </table>
</div>
<?php
function getMonthName($month){
	global $db;
	
	$sqlp = "SELECT
			mst_month.strMonth
			FROM `mst_month`
			WHERE
			mst_month.intMonthId = $month
			";	
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	return $rowp['strMonth'];
}

?>
 
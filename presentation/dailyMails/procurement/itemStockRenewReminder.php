<?php

$reportColor = "#DAF7A6";
$fieldColor = "#CC3542";
$today = date('Y-m-d');
$curYear = date('Y');
?>
<style>
    .normalfntHeader {
        font-family: Verdana;
        font-size: 12px;
        color: #006699;
        margin: 0px;
        font-weight: normal;
        text-align: center;
    }

    .normalfnt {
        font-family: Verdana;
        font-size: 10px;
        color: #000000;
        margin: 0px;
        font-weight: normal;
        text-align: left;
    }

    .normalfntsm {
        font-family: Verdana;
        font-size: 10px;
        color: #000000;
        margin: 0px;
        font-weight: normal;
        text-align: left;
    }

    .normalfntMid {
        font-family: Verdana;
        font-size: 10px;
        color: #000000;
        margin: 0px;
        font-weight: normal;
        text-align: center;
    }

    .noResultText {
        font-family: Verdana;
        font-size: 12px;
        color: #000000;
        margin: 0px;
        font-weight: normal;
        text-align: center;
    }
</style>
<div align="center">
    <div style="background-color:#FFF" ><strong>
            <div align="left"><img style="vertical-align:" src="http://accsee.com/nsoft_logo.png"/></div>
            <?php echo strtoupper($companyName); ?> - Item Wise Stock Order Reminder - <?php echo "  (  ".$today.' )  </br> Process Time ('.date('Y-m-d H:i:s').')' ?></strong></div>
    <table width="1500" border="0" align="center" bgcolor="">
        <tr>
            <td><table width="1500">
                    <tr>
                        <td width="10">&nbsp;</td>
                        <td colspan="7" class="normalfnt">
                            <table width="1500" border="1" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
                                <tr class="" bgcolor="<?php echo $reportColor; ?>">
                                    <th width="8%" class="normalfntHeader">Main Category</th>
                                    <th width="8%" class="normalfntHeader">Sub Category</th>
                                    <th width="8%" class="normalfntHeader">Item</th>
                                    <th width="8%" class="normalfntHeader">Item Code</th bgcolor="#006699">
                                    <th width="5%" class="normalfntHeader">Re-order Qty</th>
                                    <th width="5%" class="normalfntHeader">Stock In Hand</th>
                                    <th width="5%" class="normalfntHeader">Bal to Stock</th>
                                </tr>
                                <?php
                                $result=$obj_item_stock_renew->getItemListToBeRenewedSql($company,$curYear);
                                $resultCount = 0;
                                while($row = mysqli_fetch_array($result)){
                                    $stock_in_hand = (round($obj_item_stock_renew->getHOStock($row['ITEM'],$head_office_loc),4));
                                    $bal_in_stock	= $stock_in_hand-(round((($row['REQUIRED_TODATE'])-($row['ISSUE_QTY_ALL_HO']+MAX($row['ISSUE_QTY_ALL_NON_HO'],($row['GP_FROM_HO_ALL'])))),4)) ;
                                    $stock_in_hand = $stock_in_hand < 0 ?0:$stock_in_hand;
                                    $rolling  = $row['rolQty'];
                                    if($bal_in_stock <= $rolling){
                                        $resultCount++;
                                        ?>
                                <tr class="normalfnt"  bgcolor="#ffffff">
                                    <td class="normalfntMid" ><?php echo $row['mainCatName'];?></td>
                                    <td class="normalfntMid" align="right" ><?php echo $row['subCatName'];?></td>
                                    <td class="normalfntMid" align="right" ><?php echo $row['itemName'];?></td>
                                    <td class="normalfntMid" align="right" ><?php echo $row['strCode'];?></td>
                                    <td class="normalfntMid" align="right" ><?php echo $row['rolQty'];?></td>
                                    <td class="normalfntMid" align="right" ><?php echo $stock_in_hand;?></td>
                                    <td class="normalfntMid" align="right" ><?php echo $bal_in_stock;?></td>
                                </tr>
                                <?php
                                    }
                                }
                                if($resultCount == 0){
                                    ?>
                                   <tr>
                                       <td colspan="7" class="noResultText">All item stocks exceed re-order level</td>
                                   </tr>
                                <?php
                                }
                                ?>
                            </table>
                        </td>
                    </tr>
                </table></td></tr></table></div>
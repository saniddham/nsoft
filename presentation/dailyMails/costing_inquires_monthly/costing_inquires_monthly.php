<?php
ini_set('display_errors',0);

$pervious_year		=  $current_year-1;
$noOfMonths		=  12;
$reportColor            =   "#DAF7A6";
$fieldColor             =   "#CC3542";
$current_month_color    =   "#B0E713";
$vs_flag                = 0; // non vs


for($j=1; $j<=$noOfMonths ; $j++)
{
$monthArr[$j]		=	$month;
$monthNameArr[$j]	=	$obj_comm->getMonthName($j);
$yearArr[$j]		=	$year;
$yearno                 = 	$pervious_year;   
$year                   = 	$current_year;
 

$headerMonthArr[$j]	=       $yearno."-".substr($monthNameArr[$j],0,3);

$headerMonthArr2[$j]	=       $year."-".substr($monthNameArr[$j],0,3);
}
?>

<div align="center">
<div style="background-color:#FFF" ><strong>
<?php echo " COUNT OF PRICING INQUIRIES (Non-VS) FOR THE MONTH OF "." ".strtoupper(date('F')). " " .(date('Y')-1).'-'.(date('Y'))."" ?></strong><strong></strong></div>
<table width="1500" border="0" align="center" bgcolor="">
<tr>
<td><table width="1500">
<tr>
<td width="10">&nbsp;</td>
<td colspan="7" class="normalfnt"><table width="1500" border="1" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">

<tr class="" bgcolor="">
<th width="50" colspan="3"  ><b>Marketer wise</b></th>

<?php
for($j=1; $j<=$noOfMonths ; $j++)
{
$bgCol      =$reportColor;
?>
<th width="80" class="normalfntMid" bgcolor="<?php echo $bgCol ; ?>" ><?php //echo $headerMonthArr[$j] ?></th>
<th width="80" class="normalfntMid" bgcolor="<?php if($current_month_num == $j )echo $current_month_color; else echo $bgCol ; ?>" ><?php //echo $headerMonthArr[$j] ?></th>
<?php
}
?>

</tr>
<tr class="" bgcolor="<?php echo $reportColor; ?>">
<th width="50" colspan="3"  bgcolor="#006699"><font color="<?php echo $reportColor; ?>">Marketer</font></th>

<?php
for($j=1; $j<=$noOfMonths ; $j++)
{
$bgCol  =   $reportColor;

?>
<th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php echo $bgCol ; ?>" ><?php echo $headerMonthArr[$j]; ?></th>
<th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php if($current_month_num == $j )echo $current_month_color; else echo $bgCol ; ?>"><font color="red"><?php echo $headerMonthArr2[$j]; ?></font></th>

<?php
}
?>
</tr>



<?php
$result1            =   $obj_costing_inquiries_monthly->get_cluster_marketers($vs_flag,$pervious_year,$current_year);


$marketer   = '';
$x          = 1;
$rows = 0;

while ($row = mysqli_fetch_array($result1)) {
    
    $year       = $row['year'];
    $count_1    = $row['count'];
    $month      = $row['month'];
  
    
    if ($marketer != $row['user']) {
        if($rows>15)
        { 
            break;
        }
        $marketer = $row['user'];
        if($x>1&&$x<=24)
            
        { 
            for($j=$x;$j<=24;$j++)
            { 
            ?>
            <td class="normalfntRight" align="center"
                bgcolor = "<?php if ($datenow_month == $month)
                    {
                            echo $fieldColor;
                    } else {
                            echo $bgCol;
                           } ?>"> -
            </td> 
            <?php
            
            
            }
            
        }
        $x = 1;
     
        ?>   
   <tr class="normalfnt"  bgcolor="    <?php echo $reportColor; ?>">
   <td class="normalfnt" colspan="3">  <?php echo $marketer; ?></td>
  
   
    <?php
     $rows = $rows+1;
    }
       $count    = get_count($year,$month);
    
        if($x==$count)
        { 
            ?>
            <td class="normalfntRight" align="center"
                bgcolor = "<?php if ($current_month_num == $month && $year == $current_year)
                    {   
                            echo $current_month_color;
                    } else {
                            echo $bgCol;
                           } ?>"><?php echo  $count_1; ?>
            </td>
            <?php    
             $x  =  $x +1;
             
        } else 
            
        {
            //echo $month;
           for($i=$x;$i<$count;$i++)
           {
            ?>
            <td class="normalfntRight" align="center"
                bgcolor = "<?php if ($current_month_num == $count && $year == $current_year)
                    {
                             echo $current_month_color;
                    } else {
                            echo $bgCol;
                           } ?>"> -
            </td>
            
            <?php
               $x=$x+1; 
           }
           ?>
             <td class="normalfntRight" align="center"
                bgcolor = "<?php if ($current_month_num == $month && $year == $current_year)
                    {
                            echo $current_month_color;
                    } else {
                            echo $bgCol;
                           } ?>"><?php echo  $count_1 ; ?>
            </td>
            <?php
               
                $x  = $x +1;
               
        }
  
    }
    
    
    if($x>1&&$x<=24)
            
        {
            for($j=$x;$j<=24;$j++)
            { //echo $j;
            ?>
            <td class="normalfntRight" align="center"
                bgcolor = "<?php if ($current_month_num == $j && $year == $current_year)
                    {
                             echo $current_month_color;
                    } else {
                            echo $bgCol;
                           } ?>"> -
            </td> 
            <?php
            
            
            }
            
        }
        ?>
        
    
</tr>
<?php
 //if($rows == 15)
  //  {
        ?>
       <tr class="normalfnt"  bgcolor="    <?php echo $reportColor; ?>">
   <td class="normalfnt" colspan="3">Other</td> 
   <?php
    for ($j = 1; $j <= $noOfMonths; $j++) {
    $bgCol = $reportColor;
    ?>
    <th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php echo $bgCol; ?>" ><?php echo $obj_costing_inquiries_monthly->get_marketer_other($pervious_year,$pervious_year,$current_year,$j,$vs_flag);  ?></th>
    <th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php if($current_month_num == $j )echo $current_month_color; else echo $bgCol ;?>" ><font color="red"><?php echo $obj_costing_inquiries_monthly->get_marketer_other($current_year,$pervious_year,$current_year,$j,$vs_flag); ?></font></th>
    
    <?php
    }
    ?>
    
    </tr>
   
   <?php
    
   // }
    
    
    ?>
    <tr class="" bgcolor="<?php echo $reportColor; ?>">
    <th width="50" colspan="3"  bgcolor="#006699"><font color="<?php echo $reportColor; ?>">SUM</font></th>

<?php
    for ($j = 1; $j <= $noOfMonths; $j++) {
    $bgCol = $reportColor;
    ?>
    <th  class="normalfntBlue normalfntMid" value ="" align="center" bgcolor="<?php echo $bgCol; ?>" ><?php echo $obj_costing_inquiries_monthly->get_marketer_tot($pervious_year,$pervious_year,$current_year,$j,$vs_flag);  ?></th>
    <th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php if($current_month_num == $j )echo $current_month_color; else echo $bgCol ; ?>" ><font color="red"><?php echo $obj_costing_inquiries_monthly->get_marketer_tot($current_year,$pervious_year,$current_year,$j,$vs_flag); ?></font></th>
    
    <?php
    }
    ?>
    
    </tr>
   
    
    
   

<tr class="normalfnt"  bgcolor="#CCCCCC">

</tr>
<!----End of marketere wise inquiries----------------------------------------------------------->
<?php
// }
?>
<!----End of while cluster----------------------------------------------------------->

</table></td>
<td width="10">&nbsp;</td>
</tr>
<tr>
<td colspan="8" height="15"></td>
</tr>
</table></td>
<td width="10">&nbsp;</td>
</tr>
</table>
</td>
<!-------------------brand wise (VS)-------------------------------------------------->


<table width="1500">
<tr>
<td width="10">&nbsp;</td>
<td colspan="7" class="normalfnt"><table width="1500" border="1" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">

<tr class="" bgcolor="">
<th width="50" colspan="3" class="normalfnt" ><b>Brand wise</b></th>

<?php
for($j=1; $j<=$noOfMonths ; $j++)
{
$bgCol      =$reportColor;
?>
<th width="80" class="normalfntMid" bgcolor="<?php echo $bgCol ; ?>" ><?php //echo $headerMonthArr[$j] ?></th>
<th width="80" class="normalfntMid" bgcolor="<?php if($current_month_num == $j )echo $current_month_color; else echo $bgCol ; ?>" ><?php //echo $headerMonthArr[$j] ?></th>
<?php
}
?>

</tr>
<tr class="" bgcolor="<?php echo $reportColor; ?>">
<th width="50" colspan="3"  bgcolor="#006699"><font color="<?php echo $reportColor; ?>">Brand</font></th>

<?php
for($j=1; $j<=$noOfMonths ; $j++)
{
$bgCol  =   $reportColor;

?>
<th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php  echo $bgCol; ?>" ><?php echo $headerMonthArr[$j]; ?></th>
<th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php  if($current_month_num == $j )echo $current_month_color; else echo $bgCol ;  ?>" ><font color="red"><?php echo $headerMonthArr2[$j]; ?></font></th>

<?php
}
?>
</tr>



<?php
$result1            =   $obj_costing_inquiries_monthly->get_brand($vs_flag,$pervious_year,$current_year);

?>
<!----------------------------------------------->

<!---------------------------------------------->
<?php
$brand    = '';
$x        = 1;
$rows = 0;
while ($row = mysqli_fetch_array($result1)) {
    //$marketer      =     $row['user'];
    $year       = $row['YEAR'];
    $count_1    = $row['count'];
    $month      = $row['MONTH'];
    ?>

    <?php
    //echo $marketer;
    if ($brand != $row['brand']) {
        if($rows>11)
        { 
            break;
        }
        $brand = $row['brand'];
        if($x>1&&$x<=24)
            
        {
            for($j=$x;$j<=24;$j++)
            { //echo $j;
            ?>
            <td class="normalfntRight" align="center"
                bgcolor = "<?php if ($datenow_month == $month)
                    {
                            echo $fieldColor;
                    } else {
                            echo $bgCol;
                           } ?>"> -
            </td> 
            <?php
            
            
            }
            
        }
        $x = 1;
        ?>   
   <tr class="normalfnt"  bgcolor="    <?php echo $reportColor; ?>">
   <td class="normalfnt" colspan="3">  <?php echo $brand; ?></td>
    <?php
     $rows = $rows+1;
    }
    
        $count    = get_count($year,$month);
    
        if($x==$count)
        { 
            ?>
            <td class="normalfntRight" align="center"
                bgcolor = "<?php if ($current_month_num == $month && $year == $current_year)
                    {
                            echo $current_month_color;
                    } else {
                            echo $bgCol;
                           } ?>"><?php echo  $count_1 ; ?>
            </td>
            <?php    
             $x  =  $x +1;
            // echo $x;
        } else 
            
        {
            
           for($i=$x;$i<$count;$i++)
           {
            ?>
            <td class="normalfntRight" align="center"
                bgcolor = "<?php if ($current_month_num == $count && $year == $current_year)
                    {
                            echo $current_month_color;
                    } else {
                            echo $bgCol;
                           } ?>"> -
            </td>
            
            <?php
               $x=$x+1; 
           }
           ?>
             <td class="normalfntRight" align="center"
                bgcolor = "<?php if ($current_month_num == $month && $year == $current_year)
                    {
                            echo $current_month_color;
                    } else {
                            echo $bgCol;
                           } ?>"><?php echo  $count_1 ; ?>
            </td>
            <?php
               
                $x  = $x +1;
        }
        
       
    
    }
    
    if($x>1&&$x<=24)
            
        {
            for($j=$x;$j<=24;$j++)
            { //echo $j;
            ?>
            <td class="normalfntRight" align="center"
                bgcolor = "<?php if ($current_month_num == $j && $year == $current_year)
                    {
                            echo $current_month_color;
                    } else {
                            echo $bgCol;
                           } ?>"> -
            </td> 
            <?php
            
            
            }
            
        }
    
    
    ?>
   </tr>
    <?php
    //if($rows == 11)
    //{
        ?>
       <tr class="normalfnt"  bgcolor="    <?php echo $reportColor; ?>">
   <td class="normalfnt" colspan="3"> Other</td> 
   <?php
    for ($j = 1; $j <= $noOfMonths; $j++) {
    $bgCol = $reportColor;
    ?>
    <th  class="normalfntBlue normalfntMid" value ="" align="center" bgcolor="<?php echo $bgCol; ?>" ><?php echo $obj_costing_inquiries_monthly->get_brand_other($pervious_year,$pervious_year,$current_year,$j,$vs_flag);  ?></th>
    <th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php if($current_month_num == $j )echo $current_month_color; else echo $bgCol ; ?>" ><font color="red"><?php echo $obj_costing_inquiries_monthly->get_brand_other($current_year,$pervious_year,$current_year,$j,$vs_flag); ?></font></th>
    
    <?php
    }
    ?>
    
    </tr>
     <?php
    
   // }
    
    ?>
   
     <tr class="" bgcolor="<?php echo $reportColor; ?>">
    <th width="50" colspan="3"  bgcolor="#006699"><font color="<?php echo $reportColor; ?>">SUM</font></th>

<?php
    for ($j = 1; $j <= $noOfMonths; $j++) {
    $bgCol = $reportColor;
    ?>
    <th  class="normalfntBlue normalfntMid" value ="" align="center" bgcolor="<?php echo $bgCol; ?>" ><?php echo $obj_costing_inquiries_monthly->get_brand_tot($pervious_year,$pervious_year,$current_year,$j,$vs_flag);  ?></th>
    <th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php if($current_month_num == $j )echo $current_month_color; else echo $bgCol ; ?>" ><font color="red"><?php echo $obj_costing_inquiries_monthly->get_brand_tot($current_year,$pervious_year,$current_year,$j,$vs_flag); ?></font></th>
    
    <?php
    }
    ?>
    
    </tr>
   

   
   
             


<tr class="normalfnt"  bgcolor="#CCCCCC">

</tr>
<!----End of Plant wise revnue----------------------------------------------------------->
<?php
// }
?>
<!----End of while cluster----------------------------------------------------------->

</table></td>

<td width="10">&nbsp;</td>
</tr>
<tr>
<td colspan="8" height="15"></td>
</tr>
</table></td>
<td width="10">&nbsp;</td>
</tr>
</table>
</td>


<!----------customer wise-------------------------------------------->


<table width="1500">
<tr>
<td width="10">&nbsp;</td>
<td colspan="7" class="normalfnt"><table width="1500" border="1" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">

<tr class="" bgcolor="">
<th width="50" colspan="3" class="normalfnt" ><b>Customer wise</b></th>

<?php
for($j=1; $j<=$noOfMonths ; $j++)
{
$bgCol      =$reportColor;
?>
<th width="80" class="normalfntMid" bgcolor="<?php echo $bgCol ; ?>" ><?php //echo $headerMonthArr[$j] ?></th>
<th width="80" class="normalfntMid" bgcolor="<?php if($current_month_num == $j )echo $current_month_color; else echo $bgCol ; ?>" ><?php //echo $headerMonthArr[$j] ?></th>
<?php
}
?>

</tr>
<tr class="" bgcolor="<?php echo $reportColor; ?>">
<th width="50" colspan="3"  bgcolor="#006699"><font color="<?php echo $reportColor; ?>">customer</font></th>

<?php
for($j=1; $j<=$noOfMonths ; $j++)
{
$bgCol  =   $reportColor;

?>
<th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php  echo $bgCol; ?>" ><?php echo $headerMonthArr[$j]; ?></th>
<th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php if($current_month_num == $j )echo $current_month_color; else echo $bgCol ;?>" ><font color="red"><?php echo $headerMonthArr2[$j]; ?></font></th>

<?php
}
?>
</tr>



<?php
$resultp           =   $obj_costing_inquiries_monthly->get_customer($vs_flag,$pervious_year,$current_year);

?>
<!----------------------------------------------->

<!---------------------------------------------->
<?php

$customer       = '';
$x              = 1;
$rows           = 0;
while ($row = mysqli_fetch_array($resultp)) {
    //$marketer      =     $row['user'];
    $year       = $row['YEAR'];
    $count_1    = $row['count'];
    $month      = $row['MONTH'];
    ?>

    <?php
    //echo $marketer;
    if ($customer != $row['cusName']) {
        if($rows>11)
        { 
            break;
        }
        $customer = $row['cusName'];
        if($x>1&&$x<=24)
            
        {
            for($j=$x;$j<=24;$j++)
            { //echo $j;
            ?>
            <td class="normalfntRight" align="center"
                bgcolor = "<?php if ($datenow_month == $month)
                    {
                            echo $fieldColor;
                    } else {
                            echo $bgCol;
                           } ?>"> -
            </td> 
            <?php
            
            
            }
            
        }
        $x = 1;
        ?>   
   <tr class="normalfnt"  bgcolor="    <?php echo $reportColor; ?>">
   <td class="normalfnt" colspan="3">  <?php echo $customer; ?></td>
    <?php
     $rows = $rows+1;
    }
    
        $count    = get_count($year,$month);
    
        if($x==$count)
        { 
            ?>
            <td class="normalfntRight" align="center"
                bgcolor = "<?php if ($current_month_num == $month && $year == $current_year)
                    {
                            echo $current_month_color;
                    } else {
                            echo $bgCol;
                           } ?>"><?php echo  $count_1 ; ?>
            </td>
            <?php    
             $x  =  $x +1;
            // echo $x;
        } else 
            
        {
            
           for($i=$x;$i<$count;$i++)
           {
            ?>
            <td class="normalfntRight" align="center"
                bgcolor = "<?php if ($current_month_num == $count && $year == $current_year)
                    {
                            echo $current_month_color;
                    } else {
                            echo $bgCol;
                           } ?>"> -
            </td>
            
            <?php
               $x=$x+1; 
           }
           ?>
             <td class="normalfntRight" align="center"
                bgcolor = "<?php if ($current_month_num == $month && $year == $current_year)
                    {
                            echo $current_month_color;
                    } else {
                            echo $bgCol;
                           } ?>"><?php echo  $count_1 ; ?>
            </td>
            <?php
               
                $x  = $x +1;
        }
        
       
    
    }
    
    if($x>1&&$x<=24)
            
        {
            for($j=$x;$j<=24;$j++)
            { //echo $j;
            ?>
            <td class="normalfntRight" align="center"
                bgcolor = "<?php if ($current_month_num == $j && $year == $current_year)
                    {
                            echo $current_month_color;
                    } else {
                            echo $bgCol;
                           } ?>"> -
            </td> 
            <?php
            
            
            }
            
        }
    
    
    ?>
   </tr>
    <?php
    //if($rows == 11)
   // {
        ?>
       <tr class="normalfnt"  bgcolor="    <?php echo $reportColor; ?>">
   <td class="normalfnt" colspan="3"> Other</td> 
   <?php
    for ($j = 1; $j <= $noOfMonths; $j++) {
    $bgCol = $reportColor;
    ?>
    <th  class="normalfntBlue normalfntMid" value ="" align="center" bgcolor="<?php echo $bgCol; ?>" ><?php echo $obj_costing_inquiries_monthly->get_cus_other($pervious_year,$pervious_year,$current_year,$j,$vs_flag);  ?></th>
    <th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php if($current_month_num == $j )echo $current_month_color; else echo $bgCol ; ?>" ><font color="red"><?php echo $obj_costing_inquiries_monthly->get_cus_other($current_year,$pervious_year,$current_year,$j,$vs_flag); ?></font></th>
    
    <?php
    }
    ?>
    
    </tr>
     <?php
    
   // }
    
    ?>
   <tr class="" bgcolor="<?php echo $reportColor; ?>">
    <th width="50" colspan="3"  bgcolor="#006699"><font color="<?php echo $reportColor; ?>">SUM</font></th>

<?php
    for ($j = 1; $j <= $noOfMonths; $j++) {
    $bgCol = $reportColor;
    ?>
    <th  class="normalfntBlue normalfntMid" value ="" align="center" bgcolor="<?php echo $bgCol; ?>" ><?php echo $obj_costing_inquiries_monthly->get_customer_tot($pervious_year,$pervious_year,$current_year,$j,$vs_flag);  ?></th>
    <th  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php if($current_month_num == $j )echo $current_month_color; else echo $bgCol ; ?>" ><font color="red"><?php echo $obj_costing_inquiries_monthly->get_customer_tot($current_year,$pervious_year,$current_year,$j,$vs_flag); ?></font></th>
    
    <?php
    }
    ?>
    
    </tr>
<tr class="normalfnt"  bgcolor="#CCCCCC">

</tr>
<!----End of Plant wise revnue----------------------------------------------------------->
<?php
// }
?>
<!----End of while cluster----------------------------------------------------------->

</table></td>

<td width="10">&nbsp;</td>
</tr>
<tr>
<td colspan="8" height="15"></td>
</tr>
</table></td>
<td width="10">&nbsp;</td>
</tr>
</table>
</td>

</tr>
<tr height="90" >
<td align="center" class="normalfntMid"></td>
</tr>
</table>
</div>

<?php

function get_count($year,$month)
{
   $current_yr = date('Y');
   $previous_yr = $current_yr -1;
    
 if($year==$previous_yr)   
 {   
     return ($month*2)-1;
    
 }
 else {
     
     return $month *2;
      
 }
    
    
    
}
?>
<?php
date_default_timezone_set('Asia/Colombo');
ini_set('max_execution_time', 11111111) ;
//ini_set('display_errors',1);

$thisFilePath 			=  $_SERVER['PHP_SELF'];

include_once $_SESSION['ROOT_PATH']."class/dailyMails/marketerProjection_Base_Invoiced/cls_sales_project_base_invoiced_plan_get.php";
//include_once $_SESSION['ROOT_PATH']."class/dailyMails/marketerProjection/cls_sales_project_plan_set.php";
include_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";

$obj_projection_get		= new cls_sales_project_base_invoiced_plan_get($db);
//$obj_projection_set		= new cls_sales_project_plan_set($db);  
  
$toCurrency  			= 1;//1=USD,2=LKR
$currencyName			= '';
$currencyName1			= $obj_comm->getCurrencyName($toCurrency);
$current_year			= date('Y');
$current_month			= date('m');
$pervious_year			= $current_year-1;

if($current_month==4)
	$noOfMonths			= 1;
else if($current_month>4)
	$noOfMonths			=$current_month-3;
else
	$noOfMonths			=$current_month+9;
 
for($j=0; $j<$noOfMonths ; $j++){ 
	$month				=$current_month-$j;
	$year 				= $current_year;
	if($month<=0){
		$month			=$month+12;
		$year 			= $pervious_year;
	}
	$monthArr[$j]		=$month;
	$yearArr[$j]		=$year;
	$yearMonthArr[$j]	=$year.'/'.$month;
	$monthNameArr[$j]	=$obj_comm->getMonthName($month);
	$headerMonthArr[$j]	=$year."-".substr($monthNameArr[$j],0,3);
}

	/*$row 				= $obj_comm->getClusterReportCompany($cluster);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];*/
 ?>
<title>Sales Targets</title>
<style>
.normalfntBlue {font-family: Verdana;font-size: 10px;color: #0B3960;margin: 0px;font-weight: normal;text-align:left;}
.normalfnt {font-family: Verdana;font-size: 10px;color: #000000;margin: 0px;font-weight: normal;text-align:left;}
.normalfntsm {font-family: Verdana;font-size: 10px;color: #000000;margin: 0px;font-weight: normal;text-align:left;}
.normalfntMid {font-family: Verdana;font-size: 10px;color: #000000;margin: 0px;font-weight: normal;text-align:center;}
.normalfntRight {font-family: Verdana;font-size: 10px;color: #000000;margin: 0px;font-weight: normal;text-align:right;}
.compulsoryRed{color:#F00;}
</style>

<div align="center">
  <div style="background-color:#FFF" ><strong>
    <div align="left"><img style="vertical-align:" src="http://accsee.com/nsoft_logo.png"  /></div>
    Marketers'  Sales  Projections Vs. Actual Invoiced - <?php echo $clusterType; ?> (<?php echo $current_year.')  Process Time('.date('Y-m-d H:i:s').')' ?></strong><strong></strong></div>
 <div style="background-color:#FFF" ><strong>(Currency - <?php echo $currencyName1?>) ( <?php echo $clusterName; ?> )</strong></div><table width="1500" border="0" align="center" bgcolor="#FFFFFF">
<tr><td>
    <table width="1500">
        <tr><td width="10">&nbsp;</td><td colspan="7" class="normalfnt">
          <table  border="1" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0" width="100%">
            <tr style="height:20px" class="" bgcolor="#FFFFFF">
              <td width="111" rowspan="2" bgcolor="#AECE84" class="normalfntMid"><b>Marketer</b></td>
              <td width="58" rowspan="2" bgcolor="#AECE84" class="normalfntMid"><b>Annual Target</b></td>
               <td bgcolor="#FFFFAA" colspan="2" class="normalfntMid" >YTD Summary</td>
 			  <?php for($j=0; $j<$noOfMonths ; $j++){
			  $bgCol='#FFFFFF';
			  ?><td bgcolor="#AECE84" colspan="4" class="normalfntMid" align="center" ><?php echo $headerMonthArr[$j]; ?></td><?php
			  }?>
            </tr>
           <tr>
                <td width="58" height="31" align="center"  bgcolor="#FFFFAA" class="normalfntMid" >Total Actual</td> 
                <td width="59" height="31" align="center"  bgcolor="#FFFFAA" class="normalfntMid" >Gain/Loss Against YTD  Targets</td>
                 
                <?php for($j=0; $j<$noOfMonths ; $j++){
                $bgCol='#FFFFFF';?>
                <td width="51" height="31" align="center"  bgcolor="#AECE84" class="normalfntMid" >Monthly Target</td>
                <td width="51" height="31" align="center"  bgcolor="#AECE84" class="normalfntMid" >PO Projected</td> 
                <td width="50" class="normalfntMid" align="center"  bgcolor="#AECE84" >Invoiced Actual</td>
                <td width="54" align="center"  bgcolor="#AECE84" class="normalfntMid" >Variance Against Targets</td>
				 <?php
                }  ?>
     		</tr>
 			<?php 
  			//$result1 					= $obj_projection_get->load_marketers_of_cluster($current_year,$cluster,$current_month,$reportId);
			$result1 					= $obj_projection_get->load_marketers_of_cluster_monthly($current_year,$cluster,$current_month,$reportId);
 			$anualMarketerTargtTot 		=0;
			$monthlyMarketerTargtTot 	=0;
			$dayMarketerTargtTot 		=0;
			$totMarketerDispQty			=0;
			$totMarketerRevenueQty		=0;
 			$tot_monthly_total			=0;
			$tot_loss					=0;
			$r							=0;
			$arr_row					=array(); 
			$monthWiseMarketerPlannedSumArr		=array();
			$monthWiseMarketerDispSumArr		=array(); 
			$monthWiseMarketerVarianceSumArr	=array();
			$monthWiseMarketerTotTargetArr		=array();  
			
 			while($row=mysqli_fetch_array($result1))
			{
 				$marketerId								=$row['intUserId'];
			    $managerWiseTotDispQty[$marketerId]		=0;
			    $marketerWiseTotRevenueQty[$marketerId]	=0;
	  ?>
    <!--projection-->
    <tr class="normalfnt"  bgcolor="#FFFFFF"><td class="normalfnt" ><b><?php echo $row['strFullName'];?></b></td>
      <td style="height:20px" class="normalfntRight" align="right" ><?php echo number_format($row['ANUAL_TARGET'],0);?></td> 
     <?php
		ob_start();
	 ?>
       <!--actuals & projections-->
      <?php 
	  			$tot_monthly_target		= 0;
			  for($j=0; $j<$noOfMonths ; $j++){

				$month									= $monthArr[$j];
				$year									= $yearArr[$j];
  				
  				$old_plan_data							= $obj_projection_get->getOldPlanValue($marketerId,$cluster,$month,$year,$toCurrency);
				$monthly_target							= $obj_projection_get->getMarketerMonthlyTarget($marketerId,$cluster,$month,$year,$reportId);
				$tot_monthly_target						+= $monthly_target;
				$plannedQty								= $old_plan_data['PROJECTION']; 
				$dispatchValue							= $old_plan_data['ACTUAL']; 
				
				$monthWiseMarketerPlannedSumArr[$j]		+=$plannedQty;  
				$monthWiseMarketerDispSumArr[$j]		+=$dispatchValue;  
				$monthWiseMarketerVarianceSumArr[$j]	+=$dispatchValue-$monthly_target;  
				$monthWiseMarketerTotTargetArr[$j]		+=$monthly_target;
 				if($j%2==0)
					$bgCol	='#FFFFFF';
				else
					 $bgCol	='#D1E9FA';
					 
 				$fntColor	='#000000';
				if($dispatchValue-$monthly_target<0)
					$fntColor	='#FF1515';
					
 				$marketerWiseTotDispQty[$marketerId] 	+=round($dispatchValue);
				$totMarketerDispQty 					+=round($dispatchValue);
 			  ?>
      <td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($monthly_target,0); ?></td>
      <td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($plannedQty,0); ?></td>
      <td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($dispatchValue,0); ?></td>
      <td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><font color="<?php echo $fntColor?>"><?php 
 		  $vari =  $dispatchValue-$monthly_target; 
			  if($vari<0)
				echo '('. number_format($vari * -1) .')';
			  else
				echo number_format($vari);
 	  ?></font></td>
      <?php 
	   }	  ?>
     <?php
 		$body1	= ob_get_clean();
	 ?>
     <!--summery-->
      <td class="normalfntRight" align="right" ><?php $monthly_total = $marketerWiseTotDispQty[$marketerId];
	  echo number_format($monthly_total,0);$tot_monthly_total +=$monthly_total;
	  $loss = $monthly_total - ((int)$tot_monthly_target);
	  ?></td>
      <td style="color:<?php echo ($loss<0?'red':'black') ?>" class="normalfntRight" align="right" ><?php 
	  			if($loss<0)
	  				echo '('.number_format($loss*-1) .')';
				else
					echo number_format($loss) ;
					
				$tot_loss += $loss;
 	  ?></td>
      <?php
      echo $body1;
      ?>
       <!--end of revenue-->
      <?php 
	  		 $anualMarketerTargtTot 	+=$row['ANUAL_TARGET'];
	  		 $monthlyMarketerTargtTot 	+=$tot_monthly_target;
	  		// $dayMarketerTargtTot 		+=$row['dblDayTarget'];
			}//end of while
	  ?><tr class="normalfnt" bgcolor="#F8F8F8"  >
        <td height="34" class="normalfnt" >&nbsp;</td>
        <td class="normalfntRight" ><b><?php echo number_format($anualMarketerTargtTot,0); ?></b></td>
        <td class="normalfntRight" ><b><?php echo number_format($tot_monthly_total,0); ?></b></td>
        <td class="normalfntRight" style="color:<?php echo ($tot_loss<0?'red':'black') ?>"  ><b><?php
			if($tot_loss<0)
				echo '('.number_format($tot_loss*-1).')'; 
			else
				echo number_format($tot_loss);
 			?></b></td>
        
        <?php
 			  for($j=0; $j<$noOfMonths ; $j++){
				  $fntVColor	='#000000';
				  if($monthWiseMarketerVarianceSumArr[$j]<0)
				  	$fntVColor	='#FF1515'; 
			  ?>
        <td class="normalfntRight" ><b><?php echo number_format($monthWiseMarketerTotTargetArr[$j],0); ?></b></td>
        <td class="normalfntRight" ><b><?php echo number_format($monthWiseMarketerPlannedSumArr[$j],0); ?></b></td>
        <td class="normalfntRight" ><b><?php echo number_format($monthWiseMarketerDispSumArr[$j],0); ?></b></td>
        <td class="normalfntRight" ><b><font color="<?php echo $fntVColor?>"><?php 
		
 	  if($monthWiseMarketerVarianceSumArr[$j]<0)
	  	echo '('. number_format($monthWiseMarketerVarianceSumArr[$j]*-1) .')';
	  else
	  	echo number_format($monthWiseMarketerVarianceSumArr[$j]);
		?></font></b></td>
        <?php
		}
		?></tr>
    <!-----------Cum.status-------->
    <tr bgcolor="#FFFFFF">
      <td colspan="<?php echo $noOfMonths*3+4; ?>" height="20"></td>
      </tr>
    <!-----------------------------> 
    </table>
  </td>
  <td width="10">&nbsp;</td>
  </tr>
 </table>
 <td width="10">&nbsp;</td>
</tr>        

</table>
</td>
</tr>
<tr height="90" >
  <td align="center" class="normalfntMid"></td>
</tr>
</table>
 
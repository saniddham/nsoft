<?php


$thisFilePath =  $_SERVER['PHP_SELF'];
include_once '../../../dataAccess/LoginDBManager.php';	
$db =  new LoginDBManager();
$plantId	 = $_REQUEST['plantId'];

ob_start();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Fabric Dispatch Report</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />

<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}


 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:15px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}


.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:15px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}



.odd{background-color:#F5F5F5;}
.even{background-color:#FFFFFF;}
.mouseover {
	cursor: pointer;
}


.txtNumber{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}

.normalfnt {
	font-family: Verdana;
	font-size: 25px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntsm {
	font-family: Verdana;
	font-size:25px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntBlue {
	font-family: Verdana;
	font-size: 25px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 25px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 25px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}
</style>
</head>
<body><form id="frmFabDispatchReport" name="frmFabDispatchReport" method="post" action="rptFabricDispatchNote.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
    <td colspan="9" align="center" ><table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="tophead"><table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td width="12%">&nbsp;</td>
            <td width="17%" class="normalfnt"><img style="vertical-align:" src="http://accsee.com/screenline.jpg"  /></td>
            <td align="center" valign="top" width="55%" class="topheadBLACK"><?php
			
	 $SQL = "SELECT
*
FROM
mst_companies
Inner Join mst_country ON mst_companies.intCountryId = mst_country.intCountryID
WHERE
mst_companies.intId =  '1'
;";
	
	$result = $db->RunQuery($SQL);
	$row = mysqli_fetch_array($result);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];
	
	?>
              <b><?php echo $companyName.""; ?></b>
              <p class="normalfntMid">
                <?php // (".$locationName.") --> Edited By Lasantha 14th of May 2012?>
                <?php echo $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry."."."<br><b>Tel : </b>".$companyPhone." <b>Fax : </b>".$companyFax." <br><b>E-Mail : </b>".$companyEmail." <b>Web : </b>".$companyWeb;?></p></td>
            <td width="16%" class="tophead"><div align="left"><img style="vertical-align:" src="http://accsee.com/nsoft_logo.png"  /></div></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ></div>
<table align="center" width="747"><tr><td width="119">&nbsp;</td><td width="616" align="center">DISPATCH SUMMARY REPORT</td></tr></table>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
  <tr>
  <td width="100%" class="xbutton">Plant - Kelaniya</td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td>
          
          <table width="100%"  class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <thead>
              <tr class="">
                <th width="6%" >Brand</th>
                <th width="8%" >Graphic No</th>
                <th width="6%" >Style No</th>
                <th width="12%" >Customer PO</th>
                <th width="6%" >Sample Qty</th>
                <th width="5%" >Good Qty</th>
                <th width="5%" >EMD-D Qty</th>
                <th width="5%" >P-D Qty</th>
                <th width="5%" >F-D Qty</th>
                <th width="4%" >Cut Ret</th>
                <th width="6%" >Total Qty</th>
                <th width="5%" >PD %</th>
                </tr>
              </thead>
            <tbody>
              <?php 
			  $curruntTime=date("Y-m-d H:i:s");
			  $curruntTimeFrom = date("Y-m-d H:i:s",strtotime(date('Y-m-j H:i:s')) - (24 * 60 * 60));//correct

			$sql1 = "SELECT
distinct 
mst_plant.strPlantName		AS plantName,
ware_fabricdispatchheader_approvedby.dtApprovedDate,
trn_orderdetails.strStyleNo,
trn_orderdetails.strGraphicNo,
trn_orderheader.strCustomerPoNo,
ware_fabricdispatchheader.intOrderNo,
ware_fabricdispatchheader.intOrderYear,
trn_orderdetails.strSalesOrderNo,
Sum(ware_fabricdispatchdetails.dblSampleQty) AS dblSampleQty,
Sum(ware_fabricdispatchdetails.dblGoodQty) AS dblGoodQty,
Sum(ware_fabricdispatchdetails.dblEmbroideryQty) AS dblEmbroideryQty,
Sum(ware_fabricdispatchdetails.dblPDammageQty) AS dblPDammageQty,
Sum(ware_fabricdispatchdetails.dblFDammageQty) AS dblFDammageQty,
Sum(ware_fabricdispatchdetails.dblCutRetQty) AS dblCutRetQty,
mst_brand.strName AS brandName,
trn_orderdetails.dblDamagePercentage
FROM
trn_orderdetails
Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
Inner Join ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear
Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
Left Join mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
Left Join mst_colors_ground ON ware_fabricdispatchdetails.intGroundColor = mst_colors_ground.intId
Inner Join ware_fabricdispatchheader_approvedby ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchheader_approvedby.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchheader_approvedby.intYear AND ware_fabricdispatchheader_approvedby.intApproveLevelNo = ware_fabricdispatchheader.intApproveLevels
Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo
Inner Join mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
INNER JOIN mst_locations 
	ON mst_locations.intId = ware_fabricdispatchheader.intCompanyId
INNER JOIN mst_plant 
    ON mst_plant.intPlantId = mst_locations.intPlant
WHERE  
ware_fabricdispatchheader_approvedby.dtApprovedDate < CONCAT(DATE(NOW()),' ','05:00:00')   AND  
ware_fabricdispatchheader_approvedby.dtApprovedDate >= CONCAT(DATE(DATE_ADD(NOW(),INTERVAL -1 DAY )),' ','05:00:00')  AND  
ware_fabricdispatchheader.intStatus =  '1'  AND mst_plant.intPlantId = '$plantId'
GROUP BY
trn_orderdetails.strSalesOrderNo,
ware_fabricdispatchheader.intOrderNo,
ware_fabricdispatchheader.intOrderYear,
trn_sampleinfomations.intBrand
ORDER BY
ware_fabricdispatchheader.intOrderYear ASC,
ware_fabricdispatchheader.intOrderNo ASC,
trn_orderdetails.strSalesOrderNo ASC,
trn_orderdetails.strStyleNo ASC,
trn_orderheader.strCustomerPoNo ASC
";
			$result1 = $db->RunQuery($sql1);
			
			$totsampleQty=0;
			$totgoodQty=0;
			$totembroideryQty=0;
			$totpDammageQty=0;
			$totfDammageQty=0;
			$totCutRetQty=0;
			$total=0;
			$totQty=0;
			$totAmmount=0;
			
			$haveRecords = false;
			$STR_GRAHPICNO_LIST = '';
			while($row1=mysqli_fetch_array($result1))
			{
				$haveRecords = true;	
				$intBulkDispatchNo = $row1['intBulkDispatchNo'];
				$dblDamagePercentage = $row1['dblDamagePercentage'];
				$intBulkDispatchNoYear = $row1['intBulkDispatchNoYear'];
				$brandName = $row1['brandName'];
				
				$style = $row1['strStyleNo']; 
				
				/////////////////concat graphic no's///////////
						if($graphicNo!=$row1['strGraphicNo'])
						{
							$STR_GRAHPICNO_LIST .=$row1['strGraphicNo'] . ' / ';	
						}
				///////////////////////////////////////////////
				$graphicNo 		= $row1['strGraphicNo']; 
				$custPO 		= $row1['strCustomerPoNo'];
				$orderNo 		= $row1['intOrderNo'];
				$orderYear 		= $row1['intOrderYear'];
				
				$cutNo=$row1['strCutNo'];
				$salesOrderNo=$row1['strSalesOrderNo'];
				$part=$row1['part'];
				$bgColor=$row1['bgcolor'];
				$lineNo=$row1['strLineNo'];
				$size=$row1['strSize'];
				$sampleQty=$row1['dblSampleQty'];
				$goodQty=$row1['dblGoodQty'];
				$embroideryQty=$row1['dblEmbroideryQty'];
				$pDammageQty=$row1['dblPDammageQty'];
				$fDammageQty=$row1['dblFDammageQty'];
				$cutRetQty=$row1['dblCutRetQty'];
				$remarks=$row1['strRemarks'];
				$tot=(float)($sampleQty)+(float)($goodQty)+(float)($embroideryQty)+(float)($pDammageQty)+(float)($fDammageQty)+(float)($cutRetQty);
				
	  ?>
              <tr class="normalfnt"   bgcolor="#FFFFFF">
                <td align="center" class="normalfntMid" id="<?php echo $orderYear; ?>" ><?php echo $brandName; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $graphicNo; ?>" ><?php echo $graphicNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $style; ?>" ><?php echo $style; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $custPO; ?>" ><?php echo $custPO; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $sampleQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $goodQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $embroideryQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $pDammageQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $fDammageQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $cutRetQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $tot; ?></td>
                <td align="center" class="normalfntRight" style="color:<?php
				
				/*
				Mr.Nishantha requested , if Production damage percentage(PD) is exceeded than 2% , it should be red mark.dont get PD % from order tables , becouse 2% is for this report only.
				*/
				/* Mr.Nishantha requested set it to 1% 2013-07-03 */
				if( 1 <$pDammageQty/$tot*100)
					echo "red";
					
				 ?>" id="<?php echo $qty; ?>"><?php echo number_format($pDammageQty/$tot*100,2); ?>%</td>
                </tr>              
              <?php 
			$totsampleQty+=$sampleQty;
			$totgoodQty+=$goodQty;
			$totembroideryQty+=$embroideryQty;
			$totpDammageQty+=$pDammageQty;
			$totfDammageQty+=$fDammageQty;
			$totCutRetQty +=$cutRetQty;
			$total+=$tot;
			
			
			
			}
	  ?>
              <tr class="normalfnt"  bgcolor="#CCCCCC">
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfntRight" ><b><?php echo $totsampleQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $totgoodQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $totembroideryQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $totpDammageQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $totfDammageQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $totCutRetQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $total ?></b></td>
                <td class="normalfntRight" ><b><?php echo number_format($totpDammageQty/$total*100,2) ?>%</b></td>
                </tr>
              </tbody>
          </table>        </td>
        </tr>
      
      </table>
    </td>
</tr>
</table>
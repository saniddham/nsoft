<?php
session_start();

$status 				= 	$_REQUEST['status'];
$approveLevels 			= 	$_REQUEST['approveLevels'];
$programCode			= 	$_REQUEST['programCode'];
$companyId				= 	$_REQUEST['companyId'];
$program				= 	$_REQUEST['program'];
$field1					= 	$_REQUEST['field1'];
$field2					= 	$_REQUEST['field2'];
$field3					= 	$_REQUEST['field3'];
$field4					= 	$_REQUEST['field4'];
$field5					= 	$_REQUEST['field5'];
$value1					= 	$_REQUEST['value1'];
$value2					= 	$_REQUEST['value2'];
$value3					= 	$_REQUEST['value3'];
$value4					= 	$_REQUEST['value4'];
$value5					= 	$_REQUEST['value5'];
$subject				= 	$_REQUEST['subject'];
$statement1				= 	$_REQUEST['statement1'];
$statement2				= 	$_REQUEST['statement2'];

$remarksField			= 	$_REQUEST['remarksField'];
$remarksValue			= 	$_REQUEST['remarksValue'];

$link					= 	$_REQUEST['link'];
$createUserId			=	$_REQUEST['createUserId'];

require_once  	"../dataAccess/Connector.php";
require_once  	"../config.php";
require_once 	"../class/cls_mail.php";
$objMail = new cls_create_mail($db);
if (isset($_REQUEST['cboUser']))
{
		//ini_set('display_errors',1);
		$userId					= 	$_REQUEST['cboUser'];
		echo "<span style=\"color:green\">Email has sent.</span>";
			$sql = "SELECT
						sys_users.strFullName,
						sys_users.strEmail
					FROM sys_users
					WHERE
						sys_users.intUserId =  '$userId'";	
		 	$result = $db->RunQuery($sql);
			$row	=mysqli_fetch_array($result);
			$enterUserName 	= $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			 
			//send mails ////
			$_REQUEST = NULL;
			
			$_REQUEST['program']=$program;
			$_REQUEST['field1']=$field1;
			$_REQUEST['field2']=$field2;
			$_REQUEST['field3']=$field3;
			$_REQUEST['field4']=$field4;
			$_REQUEST['field5']=$field5;
			$_REQUEST['value1']=$value1;
			$_REQUEST['value2']=$value2;
			$_REQUEST['value3']=$value3;
			$_REQUEST['value4']=$value4;
			$_REQUEST['value5']=$value5;
			
			$_REQUEST['subject']=$subject;
			
			$_REQUEST['statement1']=$statement1;
			$_REQUEST['statement2']=$statement2;
			
			$_REQUEST['remarksField']=$remarksField;
			$_REQUEST['remarksValue']=$remarksValue;
			
			$_REQUEST['link']=$link;

			$objMail->send_latest_Mail('presentation/mail_approval_template.php',$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$subject,$enterUserEmail,$enterUserName);
			
			
			
		//	$objMail->send_Response_Mail('mail_approval_template.php',$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
		
}

// check if user have at least 1 approve level.
$permisionToSendToApproval = false;
for($i=1;$i<=5;$i++)
{
	$sql = "SELECT
				menupermision.int{$i}Approval
			FROM
				menupermision
			Inner Join menus ON menus.intId = menupermision.intMenuId
			WHERE
				menus.strCode 				=  '$programCode' AND
				menupermision.intUserId 	=  '".$_SESSION["userId"]."' AND
				menupermision.int{$i}Approval = 1
	";
	$result = $db->RunQuery($sql);
	
	while($row=mysqli_fetch_array($result))
	{
		$permisionToSendToApproval = true;	
	}
	
}
if((!$permisionToSendToApproval &&($createUserId !=$_SESSION["userId"]))||($status==1)||($status==0))
		die();

?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload Files</title>
<link href="../css/mainstyle.css" rel="stylesheet" type="text/css" />
<script type="application/javascript">
	function submit()
	{
			document.frmSendToApproval.submit();
	}
</script>
</head>

<body>
<form id="frmSendToApproval" name="frmSendToApproval" method="post" action="send_to_approval_latest.php" enctype="multipart/form-data">

<table class="bordered" width="477" height="48" >
<thead>

  <tr class="normalfnt">
    <th width="97" bgcolor="" class="normalfntMid">      Send a mail to</th>
    <th width="199" bgcolor="" class="normalfntMid"><select style="width:100%" name="cboUser" id="cboUser">
    <?php
	$field = "int".(($approveLevels+2)-$status)."Approval";
		$sql = "SELECT DISTINCT
					menupermision.intUserId,
					sys_users.strFullName
				FROM
					menus
					Inner Join menupermision ON menupermision.intMenuId = menus.intId
					Inner Join sys_users ON sys_users.intUserId = menupermision.intUserId
					Inner Join mst_locations_user ON mst_locations_user.intUserId = sys_users.intUserId
					Inner Join mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
				WHERE
					mst_locations.intCompanyId 	=  '$companyId' AND
					menus.strCode				=  '$programCode' AND
					menupermision.$field 		=  '1' AND 
					menupermision.intUserId<>".$_SESSION["userId"]." AND
					sys_users.intStatus			= 1
				ORDER BY sys_users.strFullName ASC
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			if($userId==$row['intUserId'])
				echo "<option selected=\"selected\" value=".$row['intUserId'].">".$row['strFullName']."</option>";	
			else
				echo "<option value=".$row['intUserId'].">".$row['strFullName']."</option>";	
		}
	?>
    </select></th>
    <th width="90" bgcolor="" class="normalfntMid">for approval.</th>
    <th width="71" bgcolor="" class="normalfntMid mouseover"><input style="visibility:hidden;width:10px" name="txtFolder" type="text" id="txtFolder" /><img src="../images/mail_send2.png" width="45" id="butSend" height="37" onclick="submit();" title="Send to Approval" class="mouseover" /></th>
  </tr>
</thead>
<tbody>
<tr style="visibility:hidden" class="normalfnt">
    <td colspan="4" bgcolor="" class="normalfntMid"><span class="normalfntMid mouseover">
    <input style="visibility:hidden;width:10px" value="<?Php echo $status; ?>" name="status" type="text" id="status" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $approveLevels; ?>" name="approveLevels" type="text" id="approveLevels" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $programCode; ?>" name="programCode" type="text" id="programCode" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $companyId; ?>" name="companyId" type="text" id="companyId" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $program; ?>" name="program" type="text" id="program" />
    
    <input style="visibility:hidden;width:10px" value="<?Php echo $field1; ?>" name="field1" type="text" id="field1" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $field2; ?>" name="field2" type="text" id="field2" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $field3; ?>" name="field3" type="text" id="field3" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $field4; ?>" name="field4" type="text" id="field4" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $field5; ?>" name="field5" type="text" id="field5" />
 
    <input style="visibility:hidden;width:10px" value="<?Php echo $value1; ?>" name="value1" type="text" id="value1" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $value2; ?>" name="value2" type="text" id="value2" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $value3; ?>" name="value3" type="text" id="value3" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $value4; ?>" name="value4" type="text" id="value4" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $value5; ?>" name="value5" type="text" id="value5" />
    
    <input style="visibility:hidden;width:10px" value="<?Php echo $subject; ?>" name="subject" type="text" id="subject" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $statement1; ?>" name="statement1" type="text" id="statement1" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $statement2; ?>" name="statement2" type="text" id="statement" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $link; ?>" name="link" type="text" id="link" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $createUserId; ?>" name="createUserId" type="text" id="createUserId;" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $remarksValue; ?>" name="remarksValue" type="text" id="remarksValue;" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $remarksField; ?>" name="remarksField" type="text" id="remarksField;" />
  
    </span></td>
    </tr>
  </tbody>
</table>
</form>
</body>
</html>
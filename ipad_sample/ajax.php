<?php
session_name('screenline_brandixIpad');
session_start();
date_default_timezone_set('Asia/Colombo');

$mainPath 		= (isset($_SESSION["mainPath"])?$_SESSION["mainPath"]:'');
$root_path		= (isset($_SESSION["ROOT_PATH"])?$_SESSION["ROOT_PATH"]:'');
$type			= (isset($_REQUEST["q"])?$_REQUEST["q"]:'');
$userType		= (isset($_SESSION["iPadUserType"])?$_SESSION["iPadUserType"]:'');

if($type=='dash')
{
	include_once $root_path."presentation/customerAndOperation/sample/ipad/dashBoard/index_db.php";
}
else if($type=='requisition')
{
	if($userType=='B')
		include_once $root_path."presentation/customerAndOperation/sample/ipad/sampleRequisitionBr/sampleRequisition_db.php";
	else
		include_once $root_path."presentation/customerAndOperation/sample/ipad/sampleRequisitionMe/sampleRequisition_db.php";
}
else if($type=='requisition_list')
{
	include_once "../presentation/customerAndOperation/sample/ipad/requitition_listing/rpt_requisition_db.php";
}
else if($type=='report')
{
	include_once $root_path."presentation/customerAndOperation/sample/ipad/sampleRequestForm/rpt_sampleRequest.php";
}
?>
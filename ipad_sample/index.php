<?php
session_name('screenline_brandixIpad');
session_start();
date_default_timezone_set('Asia/Colombo');
//ini_set('display_errors',1);

$mainPath 		= (isset($_SESSION["mainPath"])?$_SESSION["mainPath"]:'');
$root_path		= (isset($_SESSION["ROOT_PATH"])?$_SESSION["ROOT_PATH"]:'');
$type			= (isset($_REQUEST["q"])?$_REQUEST["q"]:'');
$userType		= (isset($_SESSION["iPadUserType"])?$_SESSION["iPadUserType"]:'');

if($type=='dash')
{
	include_once "../presentation/customerAndOperation/sample/ipad/dashBoard/index.php";
}
else if($type=='requisition')
{
	if($userType=='B')
		include_once "../presentation/customerAndOperation/sample/ipad/sampleRequisitionBr/sampleRequest.php";
	else
		include_once "../presentation/customerAndOperation/sample/ipad/sampleRequisitionMe/sampleRequest.php";
}
else if($type=='requisition_list')
{
	include_once "../presentation/customerAndOperation/sample/ipad/requitition_listing/rpt_requisition.php";
}
else if($type=='report')
{
	include_once "../presentation/customerAndOperation/sample/ipad/sampleRequestForm/rpt_sampleRequest.php";
}
else if($type=="logout")
{
	include_once "../dataAccess/logout_ipad_sample.php";
}
else if($type=="password_change")
{
	include_once "../presentation/administration/changeiPadPassword/change_expire_password.php";
}
else if($type=="login")
{
	include_once "../login_ipad_sample.php";
}
else
{
	header("Location:?q=login");
}
?>
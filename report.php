<?php
try{
##########################
#### set doc type ########
##########################
print '<!DOCTYPE html>';


##########################
#### config file #########
##########################
require_once "config.php";


##########################
##### include files ######
##########################
require_once "include/javascript.html";
require_once "include/css.html";
require_once "include.html";
require_once "report_body_css.html";

##########################
#### cash library ########
##########################
require_once("libraries/phpfastcache/phpfastcache.php");

##########################
#### error format lib ####
##########################
require_once 		"class/error_handler.php";					$error_handler 			= new error_handler();


##########################
#### session object   ####
##########################
require_once 		"class/sessions.php";						$sessions 				= new sessions();  


##########################
#### Connection       ####
##########################
require_once 		"dataAccess/DBManager2.php";				$db						= new DBManager2();


##########################
#### menu table       ####
##########################
require_once 		"class/tables/menus.php";					$menus					= new menus($db);

##########################
#### menu reports     ####
##########################
require_once 		"class/tables/menu_reports.php";			$menu_reports		    = new menu_reports($db);


##########################
####  open connection ####
##########################
$db->connect();


##########################
#### create cash object ##
##########################
$cache = phpFastCache();


##########################
# set para to header_db  #
##########################
$reportId			= $_REQUEST['q']; 


##########################
##### include page  ######
##########################
$menus->set($main_menuId);
require_once $menus->getstrURL();

include $menu_reports->getREPORT_URL();


}catch(Exception $e){
	$db->disconnect();
	$response['msg'] 			=  $e->getMessage();;
	$response['error'] 			=  $error_handler->jTraceEx($e);
	$response['type'] 			=  'fail';
	$response['sql']			=  $db->getSql();
	$response['mysql_error']	=  $db->getMysqlError();
	//if(SHOW_TRY_CATCH_ERRORS)
		//echo json_encode($response);
}
?>
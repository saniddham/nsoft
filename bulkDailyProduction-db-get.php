<?php

session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$location = $_SESSION['CompanyID'];
$company = $_SESSION['headCompanyId'];
$requestType = $_REQUEST['requestType'];
include "{$backwardseperator}dataAccess/Connector.php";


$programName = 'Bulk Daily Production';
$programCode = 'P1285';

require_once "{$backwardseperator}class/customerAndOperation/cls_textile_stores.php";

$requestType = isset($_REQUEST['requestType']) ? $_REQUEST['requestType'] : null;
if (isset($_REQUEST["val"])) {
    $val = preg_replace('/[^A-Za-z0-9\-]/', '', $_REQUEST["val"]);
    if (!isset($_REQUEST["page"])) {
        $_REQUEST["page"] = 1;
    }
    if ($val == 'ALLSELECT') {
        $val = '';
    }

    $page = $_REQUEST["page"];
    $year = $_REQUEST["sampleYear"];
    $start = 10 * $_REQUEST["page"] - 10;
    $end = $_REQUEST["page"] * 10;

    $sql = "SELECT DISTINCT CONCAT(trn_orderdetails.strGraphicNo,'/',trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strStyleNo ) as searchString

FROM
	trn_orderdetails
		INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
		AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
		WHERE
			trn_orderheader.intLocationId = '$location'
		AND trn_orderdetails.strGraphicNo LIKE '%$val%' || trn_orderdetails.strSalesOrderNo LIKE '%$val%' || trn_orderdetails.strStyleNo LIKE '%$val%'
		";
    $limitsql = $sql . " LIMIT $start,$end";

    $total = $db->RunQuery($sql);
    $rowcount = mysqli_num_rows($total);
    $data['total_count'] = $rowcount;
    $data['items'] = array();

    $result = $db->RunQuery($limitsql);
    while ($row = mysqli_fetch_array($result)) {
        $valUes['id'] = $row['searchString'];
        $valUes['sampleNO'] = $row['searchString'];

        array_push($data['items'], $valUes);
    }

    echo json_encode($data);
    die;
}

///////////////
elseif ($requestType == 'loadCustomer') {
    $desc = $_REQUEST['desc'];
    $splitedString = explode('/', $desc);


    if ($desc != '') {
        $para = " and trn_orderdetails.strGraphicNo LIKE  '$splitedString[0]' || trn_orderdetails.strSalesOrderNo LIKE  '$splitedString[1]' || trn_orderdetails.strStyleNo LIKE  '$splitedString[2]'";
        $sql = "SELECT DISTINCT
	mst_customer.intId,
	mst_customer.strName
FROM
	mst_customer
INNER JOIN trn_orderheader ON trn_orderheader.intCustomer = mst_customer.intId
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
WHERE
	mst_customer.intStatus = 1
					 $para
				ORDER BY mst_customer.strName
				";
        echo $sql;
        $result = $db->RunQuery($sql);
        echo "<option value=\"\"></option>";
        while ($row = mysqli_fetch_array($result)) {
            echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
        }
    } else {
        $sql = "SELECT DISTINCT
                                                                        mst_customer.intId,
                                                                        mst_customer.strName
                                                                    FROM
                                                                        mst_customer
                                                                    WHERE
                                                                         mst_customer.intStatus=1 ORDER BY mst_customer.strName
				";
        $result = $db->RunQuery($sql);
        echo "<option value=\"\"></option>";
        while ($row = mysqli_fetch_array($result)) {
            echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
        }
    }
}
//////////////
else if ($requestType == 'loadGraphicNo') {
    $intCustomer = $_REQUEST['intCustomer'];
    $desc = $_REQUEST['desc'];
    $splitedString = explode('/', $desc);
    if ($desc != '') {
        $sqlp = "SELECT DISTINCT
	trn_orderdetails.strGraphicNo
FROM
	mst_customer
INNER JOIN trn_orderheader ON trn_orderheader.intCustomer = mst_customer.intId
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
WHERE
	mst_customer.intStatus = 1
AND trn_orderdetails.strGraphicNo LIKE '%$splitedString[0]%'
AND mst_customer.intId = '$intCustomer'
ORDER BY
	mst_customer.strName";

        $html = "<option value=\"\"></option>";
        $resultp = $db->RunQuery($sqlp);
        while ($row = mysqli_fetch_array($resultp)) {

            $html .= "<option value=\"" . $row['strGraphicNo'] . "\">" . $row['strGraphicNo'] . "</option>";
        }
    } else {
        $sql = "SELECT DISTINCT
                                                                        trn_orderdetails.strGraphicNo,
                                                                        trn_orderdetails.intOrderYear
                                                                    FROM
                                                                        trn_orderdetails
                                                                    INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo=trn_orderheader.intOrderNo AND
                                                                    trn_orderdetails.intOrderYear=trn_orderheader.intOrderYear
                                                                    WHERE
                                                                         trn_orderheader.intCustomer= '$intCustomer'  
                                                                    ORDER BY trn_orderdetails.intOrderYear DESC ";

        $html = "<option value=\"\"></option>";
        $result = $db->RunQuery($sql);
        while ($row = mysqli_fetch_array($result)) {

            $html .= "<option value=\"" . $row['strGraphicNo'] . "\">" . $row['strGraphicNo'] . "</option>";
        }
    }
    echo($html);
} //------------------------------
else if ($requestType == 'loadOrderNo') {
    $graphicNo = $_REQUEST['graphicNo'];

    $sql = "SELECT DISTINCT
                                                                        CONCAT(
                                                                                trn_orderdetails.intOrderNo,
                                                                                '/',
                                                                                trn_orderdetails.intOrderYear
                                                                        ) AS intOrderNo,
                                                                        trn_orderheader.intStatus,
                                                                        trn_orderheader.PO_TYPE
                                                                FROM
                                                                        trn_orderdetails
                                                                INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo
                                                                WHERE
                                                                        trn_orderdetails.strGraphicNo = '$graphicNo'
                                                                AND trn_orderheader.PO_TYPE != 1
                                                                AND trn_orderheader.intStatus NOT IN (- 10 ,- 2 ,- 1, 4, 0)";
    //echo $sql;
//    $sql="SELECT DISTINCT
//                                                                        CONCAT(
//                                                                                trn_orderdetails.intOrderNo,
//                                                                                '/',
//                                                                        trn_orderdetails.intOrderYear 
//                                                                       ) AS intOrderNo
//                                                               FROM
//                                                                        trn_orderdetails 
//                                                               INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo 
//                                                               WHERE
//                                                                        trn_orderdetails.strGraphicNo = '$graphicNo' ";
    // echo $sql;
    $html = "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {
        $html .= "<option value=\"" . $row['intOrderNo'] . "\">" . $row['intOrderNo'] . "</option>";
    }
    echo($html);
} //------------------------------
else if ($requestType == 'loadSalesOrderNo') {
    $graphicNo = $_REQUEST['graphicNo'];
    $orderNoArr = explode('/', $_REQUEST['orderNo']);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];


    $sql = "SELECT DISTINCT
            trn_orderdetails.strSalesOrderNo
            FROM
              trn_orderdetails
            WHERE
              trn_orderdetails.strGraphicNo = '$graphicNo'
            AND trn_orderdetails.intOrderNo = '$orderNo'";
    echo $sql;

    $html = "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {

        $html .= "<option value=\"" . $row['strSalesOrderNo'] . "\">" . $row['strSalesOrderNo'] . "</option>";
    }
    echo($html);
} else if ($requestType == 'loadStyleNo') {
    $graphicNo = $_REQUEST['graphicNo'];
    $orderNoArr = explode('/', $_REQUEST['orderNo']);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $salesOrderNo = $_REQUEST['salesOrderNo'];


    $sql = "SELECT DISTINCT
          IFNULL(trn_orderdetails.strStyleNo,'null') as strStyleNo
          FROM
              trn_orderdetails
          INNER JOIN trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo
          AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear
          AND trn_orderdetails.strGraphicNo = trn_sampleinfomations.strGraphicRefNo
          WHERE
              trn_orderdetails.strGraphicNo = '$graphicNo'
              AND trn_orderdetails.intOrderNo = '$orderNo'
              AND trn_orderdetails.strSalesOrderNo='$salesOrderNo'";
    echo $sql;
    $html = "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {

        $html .= "<option value=\"" . $row['strStyleNo'] . "\">" . $row['strStyleNo'] . "</option>";
    }
    echo($html);
} //----------------------------------
else if ($requestType == 'loadGroundColor') {

    $graphicNo = $_REQUEST['graphicNo'];
    $orderNoArr = explode('/', $_REQUEST['orderNo']);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $salesOrderNo = $_REQUEST['salesOrderNo'];


    $sql = "SELECT DISTINCT
	trn_sampleinfomations_details.intGroundColor,
	mst_colors_ground.strName
FROM
	trn_sampleinfomations_details
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo
AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear
AND trn_orderdetails.strGraphicNo = trn_sampleinfomations.strGraphicRefNo
INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo
AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo
WHERE
	trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
            ";
    echo $sql;


    $html .= "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {

        $html .= "<option value=\"" . $row['intGroundColor'] . "\">" . $row['strName'] . "</option>";
    }
    echo($html);
} //----------------------------------
else if ($requestType == 'loadPrintPart') {

    $graphicNo = $_REQUEST['graphicNo'];
    $orderNoArr = explode('/', $_REQUEST['orderNo']);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $salesOrderNo = $_REQUEST['salesOrderNo'];


    $sql = "SELECT DISTINCT
	trn_orderdetails.strPrintName
FROM
	trn_orderdetails
INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_orderdetails.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_orderdetails.intSampleYear
WHERE
	trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'";
    echo $sql;

    $html .= "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {

        $html .= "<option value=\"" . $row['strPrintName'] . "\">" . $row['strPrintName'] . "</option>";
    }
    echo($html);
} //--------------------------------------------------------------
else if ($requestType == 'loadCombo') {
    $graphicNo = $_REQUEST['graphicNo'];
    $orderNoArr = explode('/', $_REQUEST['orderNo']);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $salesOrderNo = $_REQUEST['salesOrderNo'];
    $print = $_REQUEST['part'];



    $sql = "SELECT DISTINCT

	trn_orderdetails.strCombo
FROM
	trn_orderdetails
WHERE
	trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.intOrderNo = '$orderNo'
 AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo' AND trn_orderdetails.strPrintName='$print'";
    //echo $sql;
    echo "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {
        $html .= "<option value=\"" . $row['strCombo'] . "\">" . $row['strCombo'] . "</option>";
    }
    echo($html);
    //$response['combo']=$html;
    // echo json_encode($response);
} //-------------------------------------------------------------- 
else if ($requestType == 'loadPrintColor') {
    $getOrderNo = $_REQUEST['orderNo'];
    $orderNoArr = explode('/', $getOrderNo);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $combo = $_REQUEST['combo'];
    $print = $_REQUEST['part'];
    $graphicNo = $_REQUEST['graphicNo'];
    $salesorderNo = $_REQUEST['salesOrderNo'];

    $rev = getMaxRev($orderNo, $graphicNo, $salesorderNo, $print, $combo);

    $html = "<option value=\"-1/0\" >ALL</option>";
    $sql = "SELECT DISTINCT
	trn_sampleinfomations_details.intColorId,
	mst_colors.strName AS color,
	trn_sampleinfomations_details_technical.intInkTypeId,
	mst_inktypes.strName AS inktype
FROM
	trn_sampleinfomations_details
INNER JOIN trn_orderdetails ON trn_sampleinfomations_details.intSampleNo = trn_orderdetails.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_orderdetails.intSampleYear
INNER JOIN trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = trn_sampleinfomations_details.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear
AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
INNER JOIN mst_colors ON trn_sampleinfomations_details.intColorId = mst_colors.intId
INNER JOIN mst_inktypes ON trn_sampleinfomations_details_technical.intInkTypeId = mst_inktypes.intId
AND trn_sampleinfomations_details_technical.strComboName = trn_sampleinfomations_details.strComboName
AND trn_sampleinfomations_details_technical.strPrintName = trn_sampleinfomations_details.strPrintName
AND trn_sampleinfomations_details.intRevNo = trn_orderdetails.intRevisionNo
AND trn_sampleinfomations_details.strComboName = trn_orderdetails.strCombo
AND trn_sampleinfomations_details.strPrintName = trn_orderdetails.strPrintName
AND trn_sampleinfomations_details_technical.intRevNo = trn_sampleinfomations_details.intRevNo
WHERE
	trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.strSalesOrderNo = '$salesorderNo'
AND trn_orderdetails.strPrintName = '$print'
AND trn_orderdetails.strCombo = '$combo'
AND trn_orderdetails.intRevisionNo = '$rev'";
   //echo $sql;
    // $html = "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
    $rowcount = mysqli_num_rows($result);

    while ($row = mysqli_fetch_array($result)) {


        $html .= "<option value=\"" . $row['intColorId'] . '/' . $row['intInkTypeId'] . "\">" . $row['color'] . ' - ' . $row['inktype'] . "</option>";
    }

    $response['colorsHTML'] = $html;
    $response['count'] = $rowcount;
    echo json_encode($response);
}
////////////////////////////////////////////////////////////////////
else if ($requestType == 'loadQty') {
    $graphicNo = $_REQUEST['graphicNo'];
    $getOrderNo = $_REQUEST['orderNo'];
    $orderNoArr = explode('/', $getOrderNo);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $salesOrderNo = $_REQUEST['salesOrderNo'];
    $combo = $_REQUEST['combo'];
    $print = $_REQUEST['part'];

    //first array
    $getprintcolorInktype = $_REQUEST['printColor'];

    function explodeX($delimiters, $string) {
        return explode(chr(1), str_replace($delimiters, chr(1), $string));
    }

    $count = count(explode(',', $getprintcolorInktype));
    $exploded = explodeX(array(',', '/'), $getprintcolorInktype);
    $inktype = array();
    $printColor = array();
    foreach ($exploded as $k => $v) {
        if ($k % 2 == 0) {
            $printColor[] = $v;
        } else {
            $inktype[] = $v;
           }
    }


    $salesOrderId = getSalesOrderId($graphicNo, $orderNo, $salesOrderNo, $combo, $print);
    $fabricInQty = getFabricInQty($orderNo, $salesOrderId);
    $response['fabricInQty'] = $fabricInQty;

    if ($fabricInQty >= $totProductionQty) {      
              $totProductionQty = getActualProductionQty($getOrderNo, $salesOrderNo, $graphicNo, $combo, $print, $location, $printColor, $inktype);
              $response['productionQty'] = $totProductionQty;
      
    }


    $totDispatchQty = getDispatchQty($orderNo, $salesOrderId);
    $response['dispatchQty'] = $totDispatchQty;

    if ($totProductionQty == $fabricInQty) {
        $balQty = 0;
        $response['balQty'] = $balQty;
    } else {
        if ($fabricInQty > $totProductionQty) {
            $balQty = ($fabricInQty - $totProductionQty);
        } else {
            $balQty = $fabricInQty;
        }
        $response['balQty'] = $balQty;
    }

    echo json_encode($response);
} else if ($requestType == 'loadModules') {
    $section = $_REQUEST['section'];


    $sql = "SELECT intId,strName FROM mst_module WHERE mst_module.intSection='$section' AND mst_module.intLocation='$location'  AND mst_module.intStatus<>0";

    $html = "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {
        $html .= "<option  value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
    }
    echo($html);
} else if ($requestType == 'loadShots') {

    $graphicNo = $_REQUEST['graphicNo'];
    $getOrderNo = $_REQUEST['orderNo'];
    $orderNoArr = explode('/', $getOrderNo);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $salesOrderNo = $_REQUEST['salesOrderNo'];
    $combo = $_REQUEST['combo'];
    $print = $_REQUEST['part'];
    $salesOrderId = getSalesOrderId($graphicNo, $orderNo, $salesOrderNo, $combo, $print);


    $shots = getShots($orderNo, $salesOrderId);
    $html .= $shots;
    $response['shots'] = $html;


    echo json_encode($response);
} else if ($requestType == 'loadUps') {

    $graphicNo = $_REQUEST['graphicNo'];
    $getOrderNo = $_REQUEST['orderNo'];
    $orderNoArr = explode('/', $getOrderNo);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $salesOrderNo = $_REQUEST['salesOrderNo'];
    $combo = $_REQUEST['combo'];
    $print = $_REQUEST['part'];


    $salesOrderId = getSalesOrderId($graphicNo, $orderNo, $salesOrderNo, $combo, $print);
    $ups = getUps($graphicNo, $orderNo, $salesOrderId);
    $html .= $ups;
    $response['ups'] = $html;


    echo json_encode($response);
}

//---------------------------------------------------------------
////--------------------------------------------------------
function getFabricInQty($orderNo, $salesOrderId) {
    global $db;

    $sql = "SELECT 
	IFNULL(SUM(ware_fabricreceiveddetails.dblQty),0) AS fabricInQty
FROM
	ware_fabricreceiveddetails
INNER JOIN ware_fabricreceivedheader ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo
AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = ware_fabricreceivedheader.intOrderNo
AND trn_orderdetails.intOrderYear = ware_fabricreceivedheader.intOrderYear
AND trn_orderdetails.intSalesOrderId=ware_fabricreceiveddetails.intSalesOrderId
WHERE
	ware_fabricreceivedheader.intOrderNo = '$orderNo'
AND ware_fabricreceiveddetails.intSalesOrderId = '$salesOrderId'
AND ware_fabricreceivedheader.intStatus = 1

";

    $result = $db->RunQuery($sql);
    $rows = mysqli_fetch_array($result);
    $fabricInQty = $rows['fabricInQty'];
    return $fabricInQty;
}

//--------------------------------------------------------
function getDispatchQty($orderNo, $salesOrderId) {
    global $db;

    $sql = "SELECT
	IFNULL(
		sum(
			ware_fabricdispatchdetails.dblGoodQty
		),
		0
	) AS dispatchQty
FROM
	ware_fabricdispatchdetails
INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo
AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = ware_fabricdispatchheader.intOrderNo
AND trn_orderdetails.intOrderYear = ware_fabricdispatchheader.intOrderYear
WHERE
	ware_fabricdispatchheader.intOrderNo = '$orderNo'
AND trn_orderdetails.intSalesOrderId = '$salesOrderId'
AND ware_fabricdispatchheader.intStatus = 1
";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $dispatchQty = $row['dispatchQty'];
    return $dispatchQty;
}

////--------------------------------------------------------
function getActualProductionQty($orderNo, $salesOrderNo, $graphicNo, $combo, $print, $location, $printColor, $inktype) {
    global $db;
    
    $sql = "SELECT DISTINCT
	IFNULL(SUM(trn_bulkdailyproduction.intTodayProductionQty),
		0
	) AS qty
FROM
	trn_bulkdailyproduction
WHERE
	trn_bulkdailyproduction.strGraphicNo = '$graphicNo'
AND trn_bulkdailyproduction.intOrderNo = '$orderNo'
AND trn_bulkdailyproduction.strSalesOrderNo = '$salesOrderNo'
AND trn_bulkdailyproduction.productionLocation = '$location'
AND trn_bulkdailyproduction.strCombo = '$combo'
AND trn_bulkdailyproduction.strPrintName = '$print'
AND trn_bulkdailyproduction.intchkProduction = '-2'";
//AND trn_bulkdailyproduction.intPrintColor IN (" . implode(',', $printColor) . ") AND trn_bulkdailyproduction.intInkTypeId IN (" . implode(',', $inktype) . ")";
    //echo $sql;

    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $productionQty = $row['qty'];
    return $productionQty;
}

function getShots($orderNo, $salesOrderId) {
    global $db;
    $sqlp = "SELECT
	trn_orderdetails.intSampleNo,
	trn_orderdetails.intSampleYear,
	trn_orderdetails.strCombo,
	trn_orderdetails.strPrintName,
	Max(trn_orderdetails.intRevisionNo) as revNo
FROM
	trn_orderdetails
WHERE
	trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.intSalesOrderId = '$salesOrderId'";

    $resultP = $db->RunQuery($sqlp);
    $rowP = mysqli_fetch_array($resultP);
    $sampleNo = $rowP['intSampleNo'];
    $sampleYear = $rowP['intSampleYear'];
    $revNo = $rowP['revNo'];
    $print = $rowP['strPrintName'];
    $combo = $rowP['strCombo'];



    $sql = "SELECT
	IFNULL(
		SUM(
			trn_sampleinfomations_details_technical.intNoOfShots
		),
		0
	) AS intNoOfShots
FROM
	`trn_sampleinfomations_details_technical`
WHERE
	trn_sampleinfomations_details_technical.intSampleNo = '$sampleNo'
AND trn_sampleinfomations_details_technical.intSampleYear = '$sampleYear'
AND trn_sampleinfomations_details_technical.intRevNo = '$revNo'
AND trn_sampleinfomations_details_technical.strComboName = '$combo'
AND trn_sampleinfomations_details_technical.strPrintName = '$print'
GROUP BY
	trn_sampleinfomations_details_technical.strComboName
AND trn_sampleinfomations_details_technical.strPrintName";

    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $shots = $row['intNoOfShots'];
    return $shots;
}

function getUps($graphicNo, $orderNo, $salesOrderNo) {
    global $db;
    $sql = "SELECT
                                                                        MAX(
                                                                             trn_sampleinfomations_combo_print_routing.REVISION
                                                                        ),
                                                                        IFNULL(trn_sampleinfomations_combo_print_routing.NO_OF_UPS,0) as ups
                                                                    FROM
                                                                        `trn_sampleinfomations_combo_print_routing`
                                                                    INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_combo_print_routing.SAMPLE_NO
                                                                    AND trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear
                                                                    INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo
                                                                    AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear
                                                                    AND trn_orderdetails.strGraphicNo = trn_sampleinfomations.strGraphicRefNo
                                                                    WHERE
                                                                        trn_orderdetails.strGraphicNo = '$graphicNo'
                                                                    AND trn_orderdetails.intOrderNo = '$orderNo'
                                                                    AND trn_orderdetails.intSalesOrderId = '$salesOrderNo'";

    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $ups = $row['ups'];
    return $ups;
}

function getMaxRev($orderNo, $graphicNo, $salesorderNo, $print, $combo) {
    global $db;
//    $get_rev="SELECT 
//	MAX(
//		trn_sampleinfomations_details.intRevNo
//	) AS revision
//FROM
//	trn_sampleinfomations_details
//INNER JOIN trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = trn_sampleinfomations_details.intSampleNo
//AND trn_sampleinfomations_details_technical.intSampleYear = trn_sampleinfomations_details.intSampleYear
//INNER JOIN trn_orderdetails ON trn_sampleinfomations_details.intSampleNo = trn_orderdetails.intSampleNo
//AND trn_sampleinfomations_details.intSampleYear = trn_orderdetails.intSampleYear
//WHERE 	trn_orderdetails.intOrderNo = '$orderNo'
//AND trn_orderdetails.strGraphicNo = '$graphicNo'
//AND trn_orderdetails.strSalesOrderNo = '$salesorderNo'
//AND trn_orderdetails.strPrintName = '$print'
//AND trn_orderdetails.strCombo = '$combo'";
    $get_rev = "SELECT
	MAX(
		trn_orderdetails.intRevisionNo
	) AS revision
FROM
trn_orderdetails
WHERE
	trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.strSalesOrderNo = '$salesorderNo'
AND trn_orderdetails.strPrintName = '$print'
AND trn_orderdetails.strCombo = '$combo'";
    // echo $get_rev;
    $result_rev = $db->RunQuery($get_rev);
    $row = mysqli_fetch_array($result_rev);
    $rev = $row['revision'];
    return $rev;
}

function getSalesOrderId($graphicNo, $orderNo, $salesOrderNo, $combo, $print) {
    global $db;
    $sql = "SELECT
	trn_orderdetails.intSalesOrderId
FROM
	trn_orderdetails
WHERE
	trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
AND trn_orderdetails.strCombo = '$combo'
AND trn_orderdetails.strPrintName = '$print'";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $salesOrderId = $row['intSalesOrderId'];
    return $salesOrderId;
}
?>
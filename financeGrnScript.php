<?php
session_start();
ini_set('max_execution_time',100000);
include_once 'dataAccess/DBManager.php';

require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";

$objAzure = new cls_azureDBconnection();
$azure_connection = $objAzure->connectAzureDB();
$NBT_taxcodes = array(2,3,22);

$db = new DBManager();
$db->SetConnectionString($_SESSION["Server"], $_SESSION["UserName"], $_SESSION["Password"], $_SESSION["Database"], $_SESSION['userId']);
$db->OpenConnection();

        $sql_select = "SELECT
                    ware_grnheader.intGrnNo AS GRN_NO,
                    ware_grnheader.intGrnYear AS GRN_YEAR,
                    mst_supplier.strName AS supplier,
                    mst_supplier.strCode AS supplier_code,
                    mst_financecurrency.strCode AS currency_code,
                    trn_poheader.intPaymentTerm AS payTerm,
                    trn_poheader.intPaymentMode AS payMode,
                    trn_poheader.intShipmentTerm AS shipmentTerm,
                    trn_poheader.intShipmentMode AS shipmentMode,
                    trn_poheader.dtmDeliveryDate AS deliveryDate,
                    trn_poheader.strRemarks AS narration,
                    trn_poheader.dtmPODate AS orderDate,
                    trn_poheader.intStatus,
                    trn_poheader.PRINT_COUNT,
                    trn_poheader.intApproveLevels,
                    trn_poheader.intUser,
                    ware_grnheader.intCompanyId AS poRaisedLocationId,
                    sys_users.strUserName,
                    trn_poheader.intReviseNo,
                    trn_poheader.PO_TYPE,
                    CONCAT(
                        ware_grnheader.intGrnNo,
                        '/',
                        ware_grnheader.intGrnYear
                    ) AS receiptNo,
                    CONCAT(
                        trn_poheader.intPONo,
                        '/',
                        trn_poheader.intPOYear
                    ) AS poString,
                    MAX(
                        ware_grnheader_approvedby.dtApprovedDate
                    ) AS postingDate
                FROM
                    trn_poheader
                INNER JOIN mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
                INNER JOIN mst_locations ON trn_poheader.intCompany = mst_locations.intId
                INNER JOIN sys_users ON trn_poheader.intUser = sys_users.intUserId
                INNER JOIN ware_grnheader ON ware_grnheader.intPoNo = trn_poheader.intPONo
                AND ware_grnheader.intPoYear = trn_poheader.intPOYear
                INNER JOIN ware_grnheader_approvedby ON ware_grnheader_approvedby.intGrnNo = ware_grnheader.intGrnNo
                AND ware_grnheader_approvedby.intYear = ware_grnheader.intGrnYear
                INNER JOIN mst_financecurrency ON mst_financecurrency.intId = trn_poheader.intCurrency
                WHERE
                    ware_grnheader.intGrnYear = '2019'
                AND trn_poheader.intPOYear = '2019'
                AND ware_grnheader.intStatus = 1
                AND mst_locations.intCompanyId = 1
                GROUP BY
                    ware_grnheader.intGrnNo,
                    ware_grnheader.intGrnYear";

$result = $db->RunQuery2($sql_select);
while ($row = mysqli_fetch_array($result)) {
    $grnNo = $row['GRN_NO'];
    $grnYear = $row['GRN_YEAR'];
    $supplier = $row['supplier'];
    $supplier_code = $row['supplier_code'];
    $currency = ($row['currency_code'] == 'EURO')?"Eur":($row['currency_code'] == 'LKR'?"":$row['currency_code']);
    $payTerm = $row['payTerm'];
    $payMode = $row['payMode'];
    $shipmentMode = $row['shipmentMode'];
    $deliveryDate = $row['deliveryDate'];
    $locationId = $row['poRaisedLocationId'];
    $orderDate = $row['orderDate'];
    $receiptNo = $row['receiptNo'];
    $pOBy = $row['strUserName'];
    $remarks = $row['strRemarks'];
    $intStatus = $row['intStatus'];
    $savedLevels = $row['intApproveLevels'];
    $user = $row['intUser'];
    $intCreatedUser = $row['intUser'];
    $reviseNo = $row['intReviseNo'];
    $PRINT_COUNT = $row['PRINT_COUNT'];
    $po_type = $row['shipmentTerm'];
    $poString = $row['poString'];
    $postingDate = $row['postingDate'];
    $version_no = 0;
    $narration = trim($row['narration']);
    $narration_sql = $db->escapeString($narration);
    $narration = str_replace("'", '', $narration);
    $successHeader = '0';


    $sql = "SELECT
                  CONCAT(ware_grndetails.intGrnNo,'/',ware_grndetails.intGrnYear) AS receipt_no,
                  ware_grndetails.intItemId,
                  ware_grndetails.dblGrnQty AS dblGrnQty,
                  trn_podetails.SVAT_ITEM,
                  round(trn_podetails.dblUnitPrice,6) AS dblUnitPrice,
                    sum(trn_podetails.dblQty) AS dblQty,
                    trn_podetails.intTaxCode,
                    trn_podetails.dblDiscount,
                    mst_financetaxgroup.strCode AS taxCode,
                    mst_item.strCode AS itemCode,
                    mst_item.strName AS itemName,
                    mst_maincategory.strName AS mainCategory,
                    mst_subcategory.strName AS subCategory
                    FROM
                    ware_grndetails
                INNER JOIN ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo
                AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
                INNER JOIN trn_podetails ON ware_grnheader.intPoNo = trn_podetails.intPONo
                AND ware_grnheader.intPoYear = trn_podetails.intPOYear
                AND ware_grndetails.intItemId = trn_podetails.intItem
                LEFT JOIN mst_financetaxgroup ON trn_podetails.intTaxCode = mst_financetaxgroup.intId
                INNER JOIN mst_item ON ware_grndetails.intItemId = mst_item.intId
                INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
                INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                WHERE
                    ware_grndetails.intGrnNo = '$grnNo'
                AND ware_grndetails.intGrnYear = '$grnYear'
                GROUP BY
                ware_grndetails.intItemId
                HAVING
                    dblQty > 0";

    $result_data = $db->RunQuery2($sql);
    $i = 0;
    while ($row_data = mysqli_fetch_array($result_data)){
        $line_no = $row_data['intItemId'];
        $item_code = $row_data['itemCode'];
        $description = $row_data['itemName'];
        $description = str_replace("'","''",$description);
        $item_category = $row_data['mainCategory'];
        $item_subcategory = $row_data['subCategory'];
        $qty = round($row_data['dblGrnQty'],5);
        $unit_price = $row_data['dblUnitPrice'];
        $tax_code = $row_data['intTaxCode'];
        $discount = $row_data['dblDiscount'];
        $successDetails = '0';
        $i++;
        if(in_array($tax_code,$NBT_taxcodes) ||  ($item_code == 'SERFNBT' && $tax_code != 0)){
            $tax_amount = calculateNewTaxAmount($tax_code,$qty,$unit_price,$discount,$db);
            $tax_amount = round($tax_amount, 2);
        }
        else{
            $tax_amount = 0;
        }
        $vat_group = ($tax_code != 0)?"VAT":"NO_VAT";

        $sql_azure_details = "INSERT into PurchaseLine (Transaction_type, Purchase_Order_No, Revised_Count, Line_No, Reciept_No, Item_Code, Narration, Item_Category, Item_SubCategory, Quantity, Unit_Cost, VAT_Prod_Posting_Group, NBT_Amount) VALUES ('PO_GRN','$poString', '$reviseNo','$line_no','$receiptNo','$item_code','$description','$item_category','$item_subcategory','$qty','$unit_price','$vat_group','$tax_amount')";
        if($azure_connection) {
            $getResults = $objAzure->runQuery($azure_connection, $sql_azure_details);
            if($getResults != FALSE){
                $successDetails = '1';
            }
        }
        $sql_db_details = "INSERT into trn_financemodule_purchaseline (Transaction_type, Purchase_Order_No, Revised_Count, Line_No, Receipt_No, Item_Code, Narration, Item_Category, Item_SubCategory, Quantity, Unit_Cost, VAT_Prod_Posting_Group, NBT_Amount, deliveryStatus, deliveryDate) VALUES ('PO_GRN','$poString', '$reviseNo','$line_no', '$receiptNo','$item_code', '$description','$item_category','$item_subcategory','$qty','$unit_price','$vat_group', '$tax_amount', '$successDetails', NOW())";
        $result_db_details = $db->RunQuery2($sql_db_details);
    }
    $sql_azure_header = "INSERT INTO PurchaseHeader (Transaction_type, Document_Type, Receipt_No, Purchase_Order_No, Revised_Count, Vendor_Code, Vendor_Name, Posting_Date, Order_Date, Revised_Date,  Currency_Code, Payment_Term, Payment_Method, Shipment_Mode, Delivery_Date, Location_Code, Narration, PO_Type, No_of_Lines) VALUES ('PO_GRN', 'Order', '$receiptNo', '$poString', '$reviseNo', '$supplier_code', '$supplier', '$postingDate', '$orderDate', '', '$currency', '$payTerm', '$payMode', '$shipmentMode','$deliveryDate','$locationId',CONVERT(varchar(250),'$narration'), '$po_type', '$i')";
    if($azure_connection) {
        $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
        if($getResultsHeader != FALSE){
            $successHeader = '1';
        }
    }
    $sql_db_header = "INSERT INTO trn_financemodule_purchaseheader (Transaction_type, Document_Type, Receipt_No, Purchase_Order_No, Revised_Count, Vendor_Code, Vendor_Name, Posting_Date, Order_Date, Revised_Date, Currency_Code, Payment_Term, Payment_Method, Shipment_Mode, Delivery_Date, Location_Code, Narration, PO_Type, No_of_Lines, version_no, deliveryStatus, deliveryDate) VALUES ('PO_GRN', 'Order','$receiptNo', '$poString', '$reviseNo','$supplier_code', '$supplier', '$postingDate', '$orderDate', '', '$currency', '$payTerm', '$payMode', '$shipmentMode','$deliveryDate','$locationId','$narration_sql', '$po_type', '$i', '$version_no', '$successHeader', NOW())";
    $result_db_header = $db->RunQuery2($sql_db_header);
}

echo "JOB DONE";


function calculateNewTaxAmount($taxId, $quantity, $unitPrice, $discount, $db){
    $amount=$quantity*$unitPrice;
    $amount = $amount * (100 - $discount) / 100;
    $sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxId'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $taxProcess = $row['strProcess'];
    $arrTax = explode("/", $taxProcess);
    $operation = '';

    //(3)capture calculation method(isolated/inclusive/exclusive) and the array 'jsonTaxCode' contains only tax ids
    $jsonTaxCode = "[ ";
    if (count($arrTax) == 1)//If strProcess contain only one value (Example $row['strProcess'] = 5)
    {
        $operation = 'Isolated';
        $jsonTaxCode .= '{ "taxId":"' . $taxProcess . '"}';
    } else if (count($arrTax) > 1) //IF there are multiple tax ids in the tax group(example strProcess = {4/Inclusive/7})
    {
        $operation = $arrTax[1];//this should be inclusive/exclusive
        for ($i = 0; $i < count($arrTax); $i = $i + 2) {
            $jsonTaxCode .= '{ "taxId":"' . $arrTax[$i] . '"},'; //create a json array geting 0 and 2 value from the array
        }

        $jsonTaxCode = $jsonTaxCode . substr(0, count($jsonTaxCode) - 1);
    }
    $jsonTaxCode .= " ]";
    $taxCodes = json_decode($jsonTaxCode, true);

    //(4)get tax rates for all tax ids in the tax group ( tax array)
    if (count($taxCodes) != 0) {
        foreach ($taxCodes as $taxCode) {
            //get tax rates from the mst_financetaxisolated table sending taxId to the callTaxValue function which got by json array
            $codeValues[] = callTaxValue($taxCode['taxId'],$db);
        }
    }

    if (count($codeValues) > 1) // if there are more than one tax types in the tax group (this can be identified from mst_financetaxgroup.strProcess field)
    {
        if ($operation == 'Inclusive') {
            //step 1: po amount will be multiplied by the first tax rate
            //step 2 : result of the step 1(po amount+tax ammount), will be multiplied by the second tax rate.
            $firstVal = ($amount * $codeValues[0]) / 100;
            $withTaxVal = $firstVal + ((($amount + $firstVal) * $codeValues[1]) / 100);
            $val1 = ($amount * $codeValues[0]) / 100;
            $val2 = ((($amount + $firstVal) * $codeValues[1]) / 100);
        } else if ($operation == 'Exclusive') {
            //get the summation of the two tax rates and multiply it from the amount
            $withTaxVal = ($amount * ($codeValues[0] + $codeValues[1])) / 100;
            $val1 = ($amount * $codeValues[0]) / 100;
            $val2 = ($amount * $codeValues[1]) / 100;
        }
    }
    else if(count($codeValues) == 1 && $operation == 'Isolated')//there is only one tax type for the tax group
    {
        $withTaxVal = ($amount*$codeValues[0])/100;
        $val1 = ($amount*$codeValues[0])/100;
    }

    return $withTaxVal;
}

function callTaxValue($taxId, $db)
{
    $sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $taxVal = $row['dblRate'];
    return $taxVal;
}




<?php
session_start();
ini_set('max_execution_time',100000);
include_once 'dataAccess/DBManager.php';

require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";

$objAzure = new cls_azureDBconnection();
$azure_connection = $objAzure->connectAzureDB();
$NBT_taxcodes = array(2,3,22);

$db = new DBManager();
$db->SetConnectionString($_SESSION["Server"], $_SESSION["UserName"], $_SESSION["Password"], $_SESSION["Database"], $_SESSION['userId']);
$db->OpenConnection();

            $sql_select = "SELECT
                            CONCAT(
                                trn_poheader.intPONo,
                                '/',
                                trn_poheader.intPOYear
                            ) AS poString,
                            CONCAT(
                                ware_grnheader.intGrnNo,
                                '/',
                                ware_grndetails.intGrnYear
                            ) AS receipt_no,
                            ware_grndetails.intItemId,
                            ware_grndetails.dblGrnQty AS dblGrnQty,
                            SUM(SPD.QTY) AS invoicedQty,
                            trn_poheader.intReviseNo
                        FROM
                            ware_grndetails
                        INNER JOIN ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo
                        AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
                        INNER JOIN trn_poheader ON trn_poheader.intPONo = ware_grnheader.intPONo
                        AND trn_poheader.intPOYear = ware_grnheader.intPOYear
                        INNER JOIN finance_supplier_purchaseinvoice_header SPH ON SPH.INVOICE_NO = ware_grnheader.strInvoiceNo
                        AND SPH.SUPPLIER_ID = trn_poheader.intSupplier
                        AND SPH.`STATUS` = 1
                        INNER JOIN finance_supplier_purchaseinvoice_details SPD ON SPH.PURCHASE_INVOICE_NO = SPD.PURCHASE_INVOICE_NO
                        AND SPH.PURCHASE_INVOICE_YEAR = SPD.PURCHASE_INVOICE_YEAR
                        AND SPD.ITEM_ID = ware_grndetails.intItemId
                        INNER JOIN mst_locations ON trn_poheader.intCompany = mst_locations.intId
                        WHERE
                            ware_grndetails.intGrnYear = '2019'
                        AND trn_poheader.intPOYear = '2019'
                        AND mst_locations.intCompanyId = 1
                        AND ware_grnheader.intStatus = 1
                        GROUP BY
                            ware_grndetails.intGrnNo,
                            ware_grndetails.intGrnYear,
                            ware_grndetails.intItemId";

$result = $db->RunQuery2($sql_select);
while ($row = mysqli_fetch_array($result)) {
    $poString = $row['poString'];
    $receiptNo = $row['receipt_no'];
    $revise_no = $row['intReviseNo'];
    $poArray = explode('/',$poString);
    $poNo = $poArray[0];
    $poYear = $poArray[1];
    $item = $row['intItemId'];
    $qty = round($row['dblGrnQty'] - $row['invoicedQty'],5);
    if($qty <0) {
        $qty = 0;
    }



        $sql = "SELECT	
                round(
                    trn_podetails.dblUnitPrice,
                    6
                ) AS dblUnitPrice,
                sum(trn_podetails.dblQty) AS dblQty,
                trn_podetails.intTaxCode,
                trn_podetails.dblDiscount,
              mst_item.strCode AS item_code	
            FROM
                trn_podetails
            INNER JOIN mst_item ON trn_podetails.intItem = mst_item.intId
            WHERE trn_podetails.intPONo = '$poNo'
            AND trn_podetails.intPOYear = '$poYear'
            AND trn_podetails.intItem = '$item'";

        $result_data = $db->RunQuery2($sql);
        $i = 0;
        while ($row_data = mysqli_fetch_array($result_data)) {

            $unit_price = $row_data['dblUnitPrice'];
            $tax_code = $row_data['intTaxCode'];
            $discount = $row_data['dblDiscount'];
            $item_code = $row_data['item_code'];

            if (in_array($tax_code, $NBT_taxcodes) || ($item_code == 'SERFNBT' && $tax_code != 0)) {
                $tax_amount = calculateNewTaxAmount($tax_code, $qty, $unit_price, $discount, $db);
                $tax_amount = round($tax_amount, 2);
            } else {
                $tax_amount = 0;
            }


            $sql_azure_details = "UPDATE PurchaseLine SET Quantity='$qty', NBT_Amount = '$tax_amount' WHERE Transaction_Type='PO_GRN' AND Purchase_Order_No='$poString' AND Line_No='$item' AND Reciept_No='$receiptNo' AND Revised_Count='$revise_no'";
            if ($azure_connection) {
                $getResults = $objAzure->runQuery($azure_connection, $sql_azure_details);
            }
            $sql_db_details = "UPDATE trn_financemodule_purchaseline SET Quantity='$qty', NBT_Amount = '$tax_amount' WHERE Transaction_Type='PO_GRN' AND Purchase_Order_No='$poString' AND Line_No='$item' AND Reciept_No='$receiptNo' AND Revised_Count='$revise_no'";
            $result_db_details = $db->RunQuery2($sql_db_details);
        }
    }



echo "JOB DONE";


function calculateNewTaxAmount($taxId, $quantity, $unitPrice, $discount, $db){
    $amount=$quantity*$unitPrice;
    $amount = $amount * (100 - $discount) / 100;
    $sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxId'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $taxProcess = $row['strProcess'];
    $arrTax = explode("/", $taxProcess);
    $operation = '';

    //(3)capture calculation method(isolated/inclusive/exclusive) and the array 'jsonTaxCode' contains only tax ids
    $jsonTaxCode = "[ ";
    if (count($arrTax) == 1)//If strProcess contain only one value (Example $row['strProcess'] = 5)
    {
        $operation = 'Isolated';
        $jsonTaxCode .= '{ "taxId":"' . $taxProcess . '"}';
    } else if (count($arrTax) > 1) //IF there are multiple tax ids in the tax group(example strProcess = {4/Inclusive/7})
    {
        $operation = $arrTax[1];//this should be inclusive/exclusive
        for ($i = 0; $i < count($arrTax); $i = $i + 2) {
            $jsonTaxCode .= '{ "taxId":"' . $arrTax[$i] . '"},'; //create a json array geting 0 and 2 value from the array
        }

        $jsonTaxCode = $jsonTaxCode . substr(0, count($jsonTaxCode) - 1);
    }
    $jsonTaxCode .= " ]";
    $taxCodes = json_decode($jsonTaxCode, true);

    //(4)get tax rates for all tax ids in the tax group ( tax array)
    if (count($taxCodes) != 0) {
        foreach ($taxCodes as $taxCode) {
            //get tax rates from the mst_financetaxisolated table sending taxId to the callTaxValue function which got by json array
            $codeValues[] = callTaxValue($taxCode['taxId'],$db);
        }
    }

    if (count($codeValues) > 1) // if there are more than one tax types in the tax group (this can be identified from mst_financetaxgroup.strProcess field)
    {
        if ($operation == 'Inclusive') {
            //step 1: po amount will be multiplied by the first tax rate
            //step 2 : result of the step 1(po amount+tax ammount), will be multiplied by the second tax rate.
            $firstVal = ($amount * $codeValues[0]) / 100;
            $withTaxVal = $firstVal + ((($amount + $firstVal) * $codeValues[1]) / 100);
            $val1 = ($amount * $codeValues[0]) / 100;
            $val2 = ((($amount + $firstVal) * $codeValues[1]) / 100);
        } else if ($operation == 'Exclusive') {
            //get the summation of the two tax rates and multiply it from the amount
            $withTaxVal = ($amount * ($codeValues[0] + $codeValues[1])) / 100;
            $val1 = ($amount * $codeValues[0]) / 100;
            $val2 = ($amount * $codeValues[1]) / 100;
        }
    }
    else if(count($codeValues) == 1 && $operation == 'Isolated')//there is only one tax type for the tax group
    {
        $withTaxVal = ($amount*$codeValues[0])/100;
        $val1 = ($amount*$codeValues[0])/100;
    }

    return $withTaxVal;
}

function callTaxValue($taxId, $db)
{
    $sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $taxVal = $row['dblRate'];
    return $taxVal;
}
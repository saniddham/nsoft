<title>Color Room Process</title>
<link href="../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../css/button.css" rel="stylesheet" type="text/css" />
<form id="frmPopUp" name="frmPopUp" method="post">
  <div align="center">
    <div style="width:auto;height:auto; background:#000;padding-right:15px;
	padding-top:10px;
	padding-left:20px;
	padding-bottom:10px;">
      <table width="100%" border="0">
        <tr>
        <td align="right"><a href="../../presentation/customerAndOperation/sample/colorRecipes/touchScreen/index.php" class="button pink medium" style="font-size:18px;height:30px;padding-top:15px" id="butClose" name="butClose">SAMPLE</a><a class="button red medium" style="font-size:18px;height:30px;padding-top:15px" id="butClose" name="butClose">LOG OUT</a></td>

      </tr>
      <tr>
        <td>
  <fieldset>
  <legend class="normalfnt" style="color:#FFF;font-size:18px">Color Room Main Form </legend>
  <table width="100%" style="height:auto"  border="0" >
 <tr height="100"><td align="center"> <fieldset>
  <table>
  <tr>
    <td align="center"><a class="button" id="butProcess" name="butProcess" style="font-size:18px;background:#C10000;color:#FFF;height:35px;padding-top:25px">Ink Creation</a></td>
   </tr>
   <tr>
    <td align="center" class="normalfntMid" style="color:#FFF;font-size:18px">Color Room Ink Creation</td>  
  </tr>
  </table>
  </fieldset></td>
  <td align="center"> <fieldset>
  <table width="283">
  <tr>
    <td width="146" align="center"><a class="button" id="butProcess" name="butProcess" style="font-size:18px;background:#FF8080;color:#FFF;height:35px;padding-top:25px">CRN</a></td>
  </tr>
   <tr>
    <td align="center" class="normalfntMid" style="color:#FFF;font-size:18px">Color Room Requisition Note</td>
  </tr>
  </table>
  </fieldset></td>
    <td align="center"> <fieldset>
  <table width="247">
  <tr>
    <td width="161" align="center"><a href="" class="button" id="butProcess" name="butProcess" style="font-size:18px;background:#A40;color:#FFF;height:35px;padding-top:25px">Item Return</a></td>
  </tr>
   <tr>
    <td align="center" class="normalfntMid" style="color:#FFF;font-size:18px">Items Return to Stock</td>
  </tr>
  </table>
  </fieldset></td>
  
  </tr>
  
   <tr height="100">
   <td height="85" align="center"></td>
    
    <td align="center" ><fieldset>
  <table>
  <tr>
    <td align="center"><a href="../sample/touch_process.php" class="button" id="butProcess" name="butProcess" style="font-size:18px;background:#936;color:#FFF;height:35px;padding-top:25px">Process</a></td>
   </tr>
   <tr>
    <td align="center" class="normalfntMid" style="color:#FFF;font-size:18px">Color Room Day End Process</td>  
  </tr>
  </table>
  </fieldset></td>
    <td align="center"></td>
  
  </tr>
  <tr height="100">
   <td align="center"> <fieldset>
  <table>
  <tr>
    <td align="center"><a class="button" id="butProcess" name="butProcess" style="font-size:18px;background:#564729;color:#FFF;height:35px;padding-top:25px">Ink Issue</a></td>
   </tr>
   <tr>
    <td align="center" class="normalfntMid" style="color:#FFF;font-size:18px">Ink Issue to Production</td>  
  </tr>
  </table>
  </fieldset></td>
    
    <td align="center"><fieldset>
  <table>
  <tr>
    <td align="center"><a class="button" id="butProcess" name="butProcess" style="font-size:18px;background:#F0CF13;color:#FFF;height:35px;padding-top:25px">Ink Return</a></td>
   </tr>
   <tr>
    <td align="center" class="normalfntMid" style="color:#FFF;font-size:18px">Ink Return to Color Room</td>  
  </tr>
  </table>
  </fieldset></td>
    <td align="center"><fieldset>
  <table>
  <tr>
    <td align="center"><a href="../sample/touch_inkWastage.php" class="button" id="butProcess" name="butProcess" style="font-size:18px;background:#06C;color:#FFF;height:35px;padding-top:25px">Ink Wastage</a></td>
   </tr>
   <tr>
    <td align="center" class="normalfntMid" style="color:#FFF;font-size:18px">Ink Wastage of Color Room</td>  
  </tr>
  </table>
  </fieldset></td>
  
  </tr>
</table>
   </fieldset>

</td>
      </tr>
      
    </table>
    </div>
  </div>
</form>


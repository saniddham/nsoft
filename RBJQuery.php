<?php
class get {
	public $script;
	public $control;
	public $form;
	
	public $event_control ;
	public $event_name;
	public $event_function_name;
	
	public $ajax_json;
	public $ajax_data;
	public $ajax_control_data;
	
	public $validate;
	public $removeValidate;
	public $ajax_enable=false;
	
	public $confirm_event=false;
	public $success_script = '';
	public $success_innerHTML_script = '';

    public $script_function;
	public $J_DATA;

    public $comboId;

	function __construct() {
		//$this->test = $test;
	}
	
	
	
	function focus(){
		$control = $this->control;
		$form = $this->form;
		 $this->script .= "\n\t\t $('#$form #$control').focus();";
	}
	function validate($rules){
		$form = $this->form;
		$control = $this->control;
		$this->validate.="\n\n\t   $('#$form #$control').addClass(\"validate[$rules]\");";	
		$this->removeValidate.="\n\n\t   $('#$form #$control').removeClass(\"validate[$rules]\");";	
	}
	
	function html($html){
		$control = $this->control;
		$this->success_innerHTML_script .= "$('#$control').html(json.html_{$control});";
		
		$this->J_DATA["html_$control"]=$html;
		//$data['html_ID']=$mst_color_wastage_reasons->getCombo();
		//$this->success_innerHTML_script .= "alert(1);";
	}	
	
	function val(){
		$this->script .= "\n\t\t var $this->control = getValue('$this->control');";
		$this->ajax_data .=" \"$this->control\":'+URLEncode_json($this->control)+',";
		
		$value =  $this->ajax_json[$this->control];
		return "$value";
	}
	function null(){
		$this->script .= "\n\t\t var $this->control = getValue('$this->control');";
		$this->ajax_data .=" \"$this->control\":\"'+$this->control+'\",";
		
		$value =  $this->ajax_json[$this->control];
		return $value==''?'NULL':$value;
	}
	
	
	
	
	
	function id(){
		return $this->test;
	}
	function setEvent($value){
		$this->event_control = $value;
	}
	function render($programId)
	{
		$removeValidation  = $this->removeValidate;
		return  " 
$(document).ready(function() {
	var main_program_id = $programId
	$this->script;
	
	function getValue(control){
		if($('#'+control).prop('type')=='checkbox'){
			return ($('#'+control).prop('checked')?1:0);	
		}else{
			return $('#'+control).val();		
		}
		
	}
	
	function setValue(control,value){
		if($('#'+control).prop('type')=='checkbox'){
			$('#'+control).prop('checked',(value==0?false:true))	
		}else{
			$('#'+control).val(value);	
		}
	}
	
	function removeValidation(){

			$this->removeValidate;

			$('#$this->form').validationEngine('hideAll');
			//alert(1);

	}
});
				";
		
	}
}

class jquery {
	public $get;
	
	function __construct() {
		$this->get = new get();
	}
	function get($value){
		$this->get->control = $value;	
		return $this->get;
	}
	function resetForm(){
		$form = $this->get->form;
			$this->get->script .= "\n\n\t $('#$form').find(\"input[type=text], textarea,select\").val('');

			";
	}
	function setForm($form){
		$this->get->form = $form;	
		return $this->get;
	}
	function setControl($data){
		foreach($data as $k=>$v){
			$this->get->ajax_control_data.="setValue('$k',json.".$k.");\n\t\t\t\t\t\t";
		}
		$this->get->ajax_control_data;
	}
	
	function onSuccessEvent($event){
			$this->get->success_script .=" $event();";
	}

   /* function callFunction($function){
        $this->get->script_function .= "\n\n\t $function();";
    }*/

    function setComboId($comboId){
        $this->get->comboId = $comboId;
    }
	
	function regEvent($control,$event,$function,$ajax){
		
		$this->get->event_control 		= $control;
		$this->get->event_name 	  		= $event;
		$this->get->event_function_name = $function;
		$this->get->ajax_enable 		= $ajax;
		//$this->get->ajax_control_data = 'xxxxxxxxxxxxxxxxxxxxxxxx';
		
		$form = $this->get->form;
		$this->get->script .= "\n\t $('#$form #$control').die('$event').live('$event',$function);";
		$this->get->script .= "\n\n\t //$function function \n\t function $function(){  ";
		//$this->get->script .= " $('#$form').validationEngine('hide');";

        $this->get->script .="\n\n\t removeValidation();";

		//################ json #############
		if(isset($_REQUEST['json'])){
			$jsonArr= json_decode($_REQUEST['json'],true);
			$this->get->ajax_json =  $jsonArr[0];
		}
		#####################################
	}
	function alert(){
		$this->get->script .= "\n\t\t alert(".$this->get->control.");  ";
	}
	function addString($string){
		$this->get->script .= "\n\t\t $string;  ";
	}
	
	function confirm($msg){
		$this->get->confirm_event = true;
		$this->get->script .= "\n\t\t 
		
		var msg = $.prompt('".$msg."',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										
									
		
		";	
	}
	
	function json(){
		return $this->get->ajax_json;
		/*if(isset($_REQUEST['json'])){
			$jsonArr= json_decode($_REQUEST['json'],true);
			foreach($jsonArr[0] as $k=>&$v){
				$v = "'$v'";
			}	
			return $jsonArr[0];
		}
		return '';*/
		
	}
	function showWait(){
		$this->get->script .= ' showWaiting();';	
	}
	function hideWait(){
		$this->get->script .= ' showWaiting();';	
	}
	function closeEvent(){
		$funcName 	= $this->get->event_function_name;
		$form	  	= $this->get->form;
		$evControl 	= $this->get->event_control;
		$ajax_data 	= substr($this->get->ajax_data,0,-1);
		$ajax_control_data= $this->get->ajax_control_data;
		$success_script = $this->get->success_script;
		$success_innerHTML_script = $this->get->success_innerHTML_script;
        $comboId     = $this->get->comboId;
        //$script_function = $this->get->script_function;

		if($this->get->ajax_enable){
		$this->get->script .= "\n\t\t



		 $.ajax({
			url:'controller.php?q='+main_program_id+'&nojs=1',
			type:\"post\",
			dataType: \"json\",  
			data:'json=[{{$ajax_data}}]'+'&requestType=$funcName',
			async:false,
			success:function(json){
					hideWaiting();
					$success_innerHTML_script
					
					if(json.type=='pass')
					{
						$ajax_control_data
						
						$success_script

						$('#$form #$comboId').html(json.combo);
						
					}
					$('#$form #$evControl').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				},
			error:function(xhr){
					hideWaiting();
					$('#$form #$evControl').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
				}		
			});";
		}
		if($this->get->confirm_event){
		//add close brackets for delete confirm function 
		$this->get->script .='}}});';
		}
		$this->get->confirm_event = false;
		
		$this->get->script .= "hideWaiting();";
		$this->get->script .= "\n\t   }";
		$this->get->ajax_data='';
		$this->get->ajax_control_data='';	
		$this->get->validate='';
		$this->get->success_script='';
		$this->get->success_innerHTML_script='';
		return $this->get->J_DATA;
	}
	function validate(){
		$form	  = $this->get->form;
		$validate = $this->get->validate;
		if($validate!='')
			//$validate = 'removeValidation();'.$validate;

			$this->get->script .="
			$validate

		if (!$('#$form').validationEngine('validate')){
                    //alert('validate');

          setTimeout(removeValidation, 1000);
          return;
		}

			";
	}
	
}
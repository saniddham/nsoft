<?php
//ini_set('display_errors',1);

session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$companyId = $_SESSION['CompanyID'];
$company = $_SESSION['headCompanyId'];

require_once $_SESSION['ROOT_PATH'] . "class/cls_mail.php";
require_once $_SESSION['ROOT_PATH'] . "class/customerAndOperation/cls_textile_stores.php";
include "{$backwardseperator}dataAccess/Connector.php";

$objMail = new cls_create_mail($db);
$objtexttile = new cls_texttile();


$response = array('type' => '', 'msg' => '');
$requestType = $_REQUEST['requestType'];
$arr = json_decode($_REQUEST['arr'], true);

$serialNo=$_REQUEST['serialNo'];
$year=$_REQUEST['Year'];
$id=$_REQUEST['id'];

$programName = 'Bulk Daily Production';
$programCode = 'P1285';

if ($requestType == 'save') {

    //---------------------------
    $arr = json_decode($_REQUEST['arr'], true);

    $year = date('Y');
    $rollBackFlag = 0;
    $rollBackMsg = '';
    $intSavedStatus = 0;
    $savableFlag=1;
    $editMode = loadEditMode($programCode, $intSavedStatus, $userId);
    if ($serialNo == '') {
        $editMode=0;
        $serialNo = getNextBulkProductionNo($db,$companyId);
        $year = date('Y');

    }else{
        $editMode = 1;
        $savableFlag = getSaveStatus($serialNo, $year);//check whether already saved

        $sql = "DELETE 
                FROM
                    trn_bulkdailyproduction
                WHERE
                    trn_bulkdailyproduction.intBulkProductionNo = '$serialNo'
                AND trn_bulkdailyproduction.intBulkProductionYear = '$year'";
        //echo $sql;
        
        $result = $db->RunQuery2($sql);
        if (!$result){
            $rollBackFlag = 1;
        }
   }

    foreach ($arr as $arrVal) {
        $location = $_SESSION['CompanyID'];
        $graphicNo = $arrVal['graphicNo'];
        $orderNo = $arrVal['orderNo'];
        $OrderArr=explode('/',$orderNo);
        $intOrderNo=$OrderArr[0];
        $intOrderYear=$OrderArr[1];
        $strSalesOrderNo = $arrVal['salesOrderNo'];
        $part = $arrVal['part'];
        $combo = $arrVal['combo'];
        $getprintColor = $arrVal['printColor'];

        $salesOrderNo = getSalesOrderId($intOrderNo, $intOrderYear, $part, $combo, $graphicNo ,$strSalesOrderNo);

        $count=count(explode(',',$getprintColor));
        $exploded = explodeX( array(',', '/' ), $getprintColor );
        $printColorArray = array();
        $inktypeArray = array();
        foreach ($exploded as $k => $v) {
            if ($k % 2 == 0) {
                $printColorArray[] = $v;
            }
            else {
                $inktypeArray[] = $v;
            }
        }
        for($i=0;$i<$count;$i++){
            $groundColor=$arrVal['groundColor'];
            $fabricInQty = $arrVal['fabricInQty'];
            $productionInQty = $arrVal['productionInQty'];//
            $dispatchQty = $arrVal['dispatchQty'];
            $actualQty = $arrVal['actualQty'];
            $balQty = $arrVal['balQty'];
            $dateOfProduction = $arrVal['dateOfProduction'];
            $shots = $arrVal['noOfShots'];
            $noOfUps = $arrVal['noOfUps'];
            $plannedShots = $arrVal['plannedNoOfShots'];
            $plannedUps = $arrVal['plannedNoOfUps'];
            $styleNo = $arrVal['styleNo'];
            $intSavedStatus = 1;
            $intUser=$_SESSION['userId'];
            $customer=$arrVal['customer'];
            $strSalesOrderNo=$arrVal['salesOrderNo'];
            $chkStatus=$arrVal['chkProduction'];
            $section=$arrVal['section'];
            $module=$arrVal['module'];


            $sql = "INSERT INTO `nsoft`.`trn_bulkdailyproduction` (
                        `intBulkProductionNo`,`intBulkProductionYear`,`intSection`,
                        `strModuleCode`,`intCustomer`,`strGraphicNo`,`intOrderNo`,
                        `intOrderYear`,`intSalesOrderNo`,`strSalesOrderNo`,`strStyleNo`,
                        `strPrintName`,`strCombo`,`intGroundColor`,`intPrintColor`,`intShots`,`noOfUps`,
                        `intTotalFabricInQty`,`intTodayProductionQty`,`intInkTypeId`,`dateOfProduction`,
                        `productionLocation`,`intSavedStatus`,`intDayEndProcess`,`intColorRoomProcess`,
                        `intchkProduction`,`intUser`, `plannedUps`,`plannedShots`,`intTotalDispatchedQty`,`intTotalProductionMarkedQty`) VALUES (
                        '$serialNo','$year','$section','$module','$customer','$graphicNo','$intOrderNo',
                        '$intOrderYear','$salesOrderNo','$strSalesOrderNo','$styleNo','$part',
                        '$combo','$groundColor','$printColorArray[$i]','$shots','$noOfUps',
                        '$fabricInQty',	'$actualQty','$inktypeArray[$i]','$dateOfProduction',
                        '$location','$intSavedStatus','0','0','$chkStatus','$intUser','$plannedUps','$plannedShots','$dispatchQty','$productionInQty')";
            $result = $db->RunQuery2($sql);
            
            if($chkStatus==0){
            $sql2 = "INSERT INTO `nsoft`.`trn_bulkdailyproduction` (
                        `intBulkProductionNo`,`intBulkProductionYear`,`intSection`,
                        `strModuleCode`,`intCustomer`,`strGraphicNo`,`intOrderNo`,
                        `intOrderYear`,`intSalesOrderNo`,`strSalesOrderNo`,`strStyleNo`,
                        `strPrintName`,`strCombo`,`intGroundColor`,`intPrintColor`,`intShots`,`noOfUps`,
                        `intTotalFabricInQty`,`intTodayProductionQty`,`intInkTypeId`,`dateOfProduction`,
                        `productionLocation`,`intSavedStatus`,`intDayEndProcess`,`intColorRoomProcess`,
                        `intchkProduction`,`intUser`, `plannedUps`,`plannedShots`,`intTotalDispatchedQty`,`intTotalProductionMarkedQty`) VALUES (
                        '$serialNo','$year','$section','$module','$customer','$graphicNo','$intOrderNo',
                        '$intOrderYear','$salesOrderNo','$strSalesOrderNo','$styleNo','$part',
                        '$combo','$groundColor','$printColorArray[$i]','$shots','$noOfUps',
                        '$fabricInQty',	'$actualQty','$inktypeArray[$i]','$dateOfProduction',
                        '$location','$intSavedStatus','0','0','-2','$intUser','$plannedUps','$plannedShots','$dispatchQty','$productionInQty')";
            $result2 = $db->RunQuery2($sql2);
            }
            if (!$result){
                $rollBackFlag = 1;
            }
            //echo $sql;

        }
    }

    if(!$result){
        $rollBackFlag=1;
        $rollBackMsg="No items to save";

    }


    if ($rollBackFlag == 1) {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $rollBackMsg;
        $response['q'] = '';
    }

    if($result) {
        $response['type'] = 'pass';
        if($editMode==1)
            $response['msg'] = 'Updated successfully.';
        else
        $response['msg'] = 'Saved successfully.';
        $response['serialNo'] = $serialNo;
        $response['year'] = $year;
        $response['id']=$id;
    }


     else {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    }

    $db->CloseConnection();

//----------------------------------------
echo json_encode($response);

}

else if($requestType == 'DayEnd') {
    foreach ($arr as $arrVal) {
        $location = $_SESSION['CompanyID'];
        $date = $arrVal['date'];

        $sql = "UPDATE trn_bulkdailyproduction
SET trn_bulkdailyproduction.intDayEndProcess = 1
WHERE
	trn_bulkdailyproduction.dateOfProduction = '$date'
AND trn_bulkdailyproduction.productionLocation = '$location'
AND trn_bulkdailyproduction.intSavedStatus = '$intsavedStatus'
AND trn_bulkdailyproduction.intColorRoomProcess = 0";

        echo($sql);
        $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);
        if ($row > 0) {
            echo 'true';
        } else {
            echo 'false';
        }


    }
    if ($rollBackFlag == 1) {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $rollBackMsg;
        $response['q'] = '';
    }
    if ($rollBackFlag != 1) {

        $response['type'] = 'pass';
        $response['msg'] = 'Day End Processed' . $msg1;

//        $response['serialNo'] = $serialNo;
//        $response['year'] = $year;
    }
       else {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    }

    echo json_encode($response);



}


//------------------------------function load loadEditMode---------------------

function explodeX( $delimiters, $string)
        {
            return explode( chr( 1 ), str_replace( $delimiters, chr( 1 ), $string ) );
        }
function loadEditMode($programCode, $intStatus, $intUser)
{
    global $db;

    $editMode = 0;
    $sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";


    $resultp = $db->RunQuery2($sqlp);
    $rowp = mysqli_fetch_array($resultp);

    if ($rowp['intEdit'] == 1) {
        if ($intStatus == 1) {
            $editMode = 1;
        }
    }

    return $editMode;
}

//------------------------------function generate bulkProductionNo---------------------

function getNextBulkProductionNo($db, $companyId)
{
    $sql_select = "SELECT
                    sys_no.intBulkProductionNo
                    FROM sys_no
                    WHERE
                    sys_no.intCompanyId =  '$companyId'";

    $result = $db->RunQuery2($sql_select);
    $row = mysqli_fetch_array($result);

    $nextBulkProductionNo = $row['intBulkProductionNo'];

    if ($nextBulkProductionNo == 0) {
        $nextBulkProductionNo = $companyId . '00000';
    }

    $sql = "UPDATE `sys_no` SET intBulkProductionNo=$nextBulkProductionNo+1 WHERE (`intCompanyId`='$companyId')  ";
    $db->RunQuery2($sql);

    return $nextBulkProductionNo;
}
function getSaveStatus($serialNo,$year)
{
    global $db;
    $sql = "SELECT
                intSavedStatus    
            FROM
                trn_bulkdailyproduction
            WHERE
                intBulkProductionNo = '$serialNo'
            AND intBulkProductionYear = '$year'";
    $results = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($results);
    $status = $row['intSavedStatus'];

    if($status==1){
        $editableFlag=1	;
    }
    else{
        $editableFlag=0;
    }
   // echo $editableFlag;

    return $editableFlag;
}


function getSalesOrderId($orderNo,$year, $print, $combo, $graphic, $soName)
{
    global $db;
    $sql = "SELECT
                intSalesOrderId    
            FROM
                trn_orderdetails
            WHERE
                intOrderNo = '$orderNo' AND 
                intOrderYear = '$year' AND 
                strPrintName = '$print' AND 
                strCombo = '$combo' AND 
                strGraphicNo = '$graphic' AND 
                strSalesOrderNo = '$soName'
            ORDER BY  intSalesOrderId DESC limit 1    
                ";
    $results = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($results);
    $salesOrderId = $row['intSalesOrderId'];
    return $salesOrderId;
}
?>



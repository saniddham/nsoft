/*
Navicat MySQL Data Transfer

Source Server         : mysqli
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : nsoft

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-04-24 11:27:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for trn_financemodule_purchase_header
-- ----------------------------
DROP TABLE IF EXISTS `trn_financemodule_purchase_header`;
CREATE TABLE `trn_financemodule_purchase_header` (
  `transaction_type` varchar(50) DEFAULT NULL,
  `purchase_order_no` varchar(20) DEFAULT NULL,
  `document_type` varchar(20) DEFAULT NULL,
  `Receipt_No` varchar(20) DEFAULT NULL,
  `Vendor_Code` varchar(20) DEFAULT NULL,
  `Vendor_Name` varchar(50) DEFAULT NULL,
  `Posting_Date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Order_Date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Revised_Date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Currency_Code` varchar(20) DEFAULT NULL,
  `Payment_Term` varchar(20) DEFAULT NULL,
  `Payment_Method` varchar(20) DEFAULT NULL,
  `Shipment_Mode` varchar(20) DEFAULT NULL,
  `Narration` varchar(20) DEFAULT NULL,
  `Delivery_Date` datetime DEFAULT NULL,
  `Location_Code` varchar(20) DEFAULT NULL,
  `PO_Type` varchar(20) DEFAULT NULL,
  `No_of_Lines` int(11) DEFAULT NULL,
  `Nav_Updated` bit(1) DEFAULT NULL,
  `Nav_Update_DateTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

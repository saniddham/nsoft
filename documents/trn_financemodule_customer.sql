/*
Navicat MySQL Data Transfer

Source Server         : mysqli
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : nsoft

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-04-26 14:09:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for trn_financemodule_customer
-- ----------------------------
DROP TABLE IF EXISTS `trn_financemodule_customer`;
CREATE TABLE `trn_financemodule_customer` (
  `intId` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `strCode` varchar(255) DEFAULT NULL,
  `strName` varchar(255) DEFAULT NULL,
  `strAddress` varchar(255) DEFAULT NULL,
  `strCity` varchar(255) DEFAULT NULL,
  `strEmail` varchar(255) DEFAULT NULL,
  `strPhoneNo` varchar(255) DEFAULT NULL,
  `strVatNo` varchar(255) DEFAULT NULL,
  `strSVatNo` varchar(255) DEFAULT NULL,
  `currencyCode` varchar(255) DEFAULT '',
  `vatBustGroup` varchar(255) DEFAULT NULL,
  `cusPostingGroup` varchar(255) DEFAULT NULL,
  `deliveryDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deliveryStatus` int(255) DEFAULT NULL,
  `isUpdating` bit(1) DEFAULT b'0',
  PRIMARY KEY (`intId`),
  KEY `customerId` (`customerId`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

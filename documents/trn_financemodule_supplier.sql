/*
Navicat MySQL Data Transfer

Source Server         : mysqli
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : nsoft

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-04-29 15:34:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for trn_financemodule_supplier
-- ----------------------------
DROP TABLE IF EXISTS `trn_financemodule_supplier`;
CREATE TABLE `trn_financemodule_supplier` (
  `intId` int(11) DEFAULT NULL,
  `supplierId` int(11) DEFAULT NULL,
  `strCode` varchar(255) DEFAULT NULL,
  `strName` varchar(255) DEFAULT NULL,
  `strAddress` varchar(255) DEFAULT NULL,
  `strCity` varchar(255) DEFAULT NULL,
  `strEmail` varchar(255) DEFAULT NULL,
  `strPhoneNo` varchar(255) DEFAULT NULL,
  `vatBustGroup` varchar(255) DEFAULT NULL,
  `strVatNo` varchar(255) DEFAULT NULL,
  `strSVatNo` varchar(255) DEFAULT NULL,
  `currencyCode` varchar(255) DEFAULT NULL,
  `vendorPostingGroup` varchar(255) DEFAULT NULL,
  `deliveryDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deliveryStatus` varchar(255) DEFAULT NULL,
  `isUpdating` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

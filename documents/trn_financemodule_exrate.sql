/*
Navicat MySQL Data Transfer

Source Server         : mysqli
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : nsoft

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-04-26 14:09:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for trn_financemodule_exrate
-- ----------------------------
DROP TABLE IF EXISTS `trn_financemodule_exrate`;
CREATE TABLE `trn_financemodule_exrate` (
  `intId` int(11) NOT NULL AUTO_INCREMENT,
  `intCurrencyId` int(10) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `exchange_rate` float DEFAULT NULL,
  `intStatus` int(11) DEFAULT NULL,
  `baseCurrency` int(11) DEFAULT '2',
  `createdDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `createdBy` int(11) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`intId`),
  KEY `mst_financemodule_exrate_createdBy` (`createdBy`) USING BTREE,
  KEY `mst_financemodule_exrate_currency` (`intCurrencyId`) USING BTREE,
  KEY `trn_financemodule_exrate_ibfk_2` (`modifiedBy`),
  CONSTRAINT `trn_financemodule_exrate_ibfk_1` FOREIGN KEY (`createdBy`) REFERENCES `sys_users` (`intUserId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trn_financemodule_exrate_ibfk_2` FOREIGN KEY (`modifiedBy`) REFERENCES `sys_users` (`intUserId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

<?php
session_start();
ini_set('max_execution_time',100000);
include_once 'dataAccess/DBManager.php';


require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";


$objAzure = new cls_azureDBconnection();
$azure_connection = $objAzure->connectAzureDB();

$db = new DBManager();
$db->SetConnectionString($_SESSION["Server"], $_SESSION["UserName"], $_SESSION["Password"], $_SESSION["Database"], $_SESSION['userId']);
$db->OpenConnection();


$sql_header = "SELECT DISTINCT
                    trn_orderheader.intOrderNo AS Order_No,
                    trn_orderheader.intOrderYear AS Order_Year,                
                IF (
                    trn_orderheader.intStatus = 1,
                    'SO_New',
                    'SO_EditStart'
                ) AS Transaction_Type,
                 trn_orderheader.intReviseNo,
                 MAX(trn_orderheader_approvedby.dtApprovedDate) AS Posting_Date,
                 trn_orderheader.intMarketer AS marketer,
                 trn_orderheader.intStatus,
                 trn_orderheader.strRemark,
                 trn_orderheader.intApproveLevelStart,
                 trn_orderheader.strCustomerPoNo,
                 trn_orderheader.intLocationId,
                 trn_orderheader.intCustomer,
                 trn_orderheader.intPaymentTerm,
                 trn_orderheader.dtmCreateDate AS order_date,
                 mst_customer.strName AS customer,
                 mst_customer.strCode AS customerCode,
                 trn_orderheader.strCustomerPoNo,
                 mst_customer_locations_header.strName AS customer_location,
                 mst_financecurrency.strCode AS curr_code,
                 marketer.strUserName,
                 (
                    SELECT
                        CAST(
                            MAX(
                                ware_stocktransactions_fabric.dtDate
                            ) AS DATE
                        )
                    FROM
                        ware_stocktransactions_fabric
                    WHERE
                        ware_stocktransactions_fabric.intOrderNo = trn_orderheader.intOrderNo
                    AND ware_stocktransactions_fabric.intOrderYear = trn_orderheader.intOrderYear
                    AND ware_stocktransactions_fabric.strType LIKE '%Dispatched%'
                    GROUP BY
                        ware_stocktransactions_fabric.intOrderNo,
                        ware_stocktransactions_fabric.intOrderYear
                ) AS lastDispatchDate
                FROM
                    trn_orderheader
                INNER JOIN mst_financecurrency ON mst_financecurrency.intId = trn_orderheader.intCurrency
                INNER JOIN sys_users AS marketer ON marketer.intUserId = trn_orderheader.intMarketer
                INNER JOIN mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
                LEFT JOIN mst_customer_locations_header ON trn_orderheader.intCustomerLocation = mst_customer_locations_header.intId
                INNER JOIN trn_orderheader_approvedby ON trn_orderheader.intOrderNo = trn_orderheader_approvedby.intOrderNo
                AND trn_orderheader.intOrderYear = trn_orderheader_approvedby.intYear
                AND trn_orderheader_approvedby.intApproveLevelNo = 3
                LEFT JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
                WHERE
                    trn_orderheader.intOrderYear = 2019
                AND PO_TYPE != 1
                AND trn_orderheader.intStatus NOT IN (- 10 ,- 2)
                AND mst_locations.intCompanyId = 1
                GROUP BY
                    trn_orderheader.intOrderNo,
                    trn_orderheader.intOrderYear";

$result_header = $db->RunQuery($sql_header);
while ($row_header = mysqli_fetch_array($result_header)) {
    $orderNo = $row_header['Order_No'];
    $orderYear = $row_header['Order_Year'];
    $poString = $orderNo . '/' . $orderYear;
    $customer = trim($row_header['customer']);
    $cus_location = trim($row_header['customer_location']);
    $customerCode = $row_header['customerCode'];
    $strCustomerPoNo = trim($row_header['strCustomerPoNo']);
    $strRemark = trim($row_header['strRemark']);
    $strRemark_sql = $strRemark;
    $orderDate = $row_header['order_date'];
    $curr_code = ($row_header['curr_code'] == 'EURO')?"Eur":($row_header['curr_code'] == 'LKR'?"":$row_header['curr_code']);
    $payment_term = $row_header['intPaymentTerm'];
    $marketer = $row_header['marketer'];
    $location = $row_header['intLocationId'];
    $revise_no = $row_header['intReviseNo'];
    $disp_date = $row_header['lastDispatchDate'];
    $postingDate = $row_header['Posting_Date'];
    $successHeader = 0;
    $i = 0;
    $transactionType = $row_header['Transaction_Type'];

    $sql_select = "SELECT
			OD.intSalesOrderId,
			OD.strSalesOrderNo,
			OD.intOrderNo,
			OD.intOrderYear,
			OD.strLineNo,
			OD.dtDeliveryDate AS soDeliverydate,
			OD.dtPSD AS PSD,
			OD.strGraphicNo,
			OD.intSampleNo,
			OD.intSampleYear,
			OD.strStyleNo,
			OD.strPrintName,
			OD.dblOverCutPercentage,
			OD.dblDamagePercentage,
			OD.strCombo,
			OD.TECHNIQUE_GROUP_ID, 
			(
			  select TECHNIQUE_GROUP_NAME from mst_technique_groups where mst_technique_groups.TECHNIQUE_GROUP_ID = OD.TECHNIQUE_GROUP_ID
			)AS technique,
			OD.dtPSD,
			OD.intQty AS poqty,
			OD.dblPrice AS price,
			OD.intOrderYear,
			OD.intPart,
			mst_part.strName as partName,
			OD.dblDamagePercentage,
			mst_brand.strName AS brand,
			trn_sampleinfomations_details.intGroundColor,
			mst_colors_ground.strName as groundColor,
			(
				SELECT
					sum(finance_customer_invoice_details.QTY)
				FROM
					finance_customer_invoice_details
				INNER JOIN finance_customer_invoice_header ON finance_customer_invoice_header.SERIAL_NO = finance_customer_invoice_details.SERIAL_NO 
				AND finance_customer_invoice_header.SERIAL_NO = finance_customer_invoice_details.SERIAL_NO
				AND finance_customer_invoice_header.STATUS = 1
				WHERE
					finance_customer_invoice_header.ORDER_NO = OD.intOrderNo
				AND finance_customer_invoice_header.ORDER_YEAR = OD.intOrderYear
				AND finance_customer_invoice_details.SALES_ORDER_ID = OD.intSalesOrderId
			) AS invoiced_qty
			FROM
			trn_orderdetails OD
			INNER JOIN mst_part ON mst_part.intId = OD.intPart
			INNER JOIN trn_sampleinfomations_details ON OD.intSampleNo = trn_sampleinfomations_details.intSampleNo
			AND OD.intSampleYear = trn_sampleinfomations_details.intSampleYear
			AND OD.intRevisionNo = trn_sampleinfomations_details.intRevNo
			AND OD.strCombo = trn_sampleinfomations_details.strComboName
			AND OD.strPrintName = trn_sampleinfomations_details.strPrintName
			INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
			AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
			AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
			INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
			INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
			WHERE
			OD.intOrderNo = '$orderNo'
			AND OD.intOrderYear = '$orderYear'
			GROUP BY
			OD.intOrderNo,
			OD.intOrderYear,
			OD.intSalesOrderId";


    $result1 = $db->RunQuery($sql_select);
    while ($row = mysqli_fetch_array($result1)) {
        $intSalesOrderId = $row['intSalesOrderId'];
        $strSalesOrderNo = trim($row['strSalesOrderNo']);
        $line_no = intval($row['strLineNo']);
        $strGraphicNo = trim($row['strGraphicNo']);
        $strCombo = trim($row['strCombo']);
        $intSampleNo = $row['intSampleNo'];
        $intSampleYear = $row['intSampleYear'];
        $groundColor = trim($row['groundColor']);
        $strStyleNo = trim($row['strStyleNo']);
        $strPrintName = trim($row['strPrintName']);
        $partName = trim($row['partName']);
        $brand = trim($row['brand']);
        $sampleNo = $intSampleNo.'/'.$intSampleYear;
        $psd = $row['PSD'];
	$technique = $row['technique'];
	if (strpos($technique, 'Plotter') !== false) {
            $technique='Plotter/Lazer cut';
        }
        $successDetails = 0;
        $poqty = $row['poqty'];
        $invoiced_qty = $row['invoiced_qty'];
        if($invoiced_qty == ''){
            $invoiced_qty = 0;
        }
        $totalQty = $poqty + ceil($poqty *($row['dblOverCutPercentage']+$row['dblDamagePercentage'])/100) + 2 - $invoiced_qty ;
        if($totalQty < 0){
            $totalQty = 0;
        }
        $amount = round($row['price'], 5);
        if($poqty > 0) {
            $i++;
            $sql_azure_details = "INSERT into SalesLine (Transaction_type, Order_No, Sales_Order_No, Line_No, Sales_Order_Name, Item_Code, Description, Item_Category, Item_SubCategory, Quantity, Original_Quantity, Pre_Invoiced_Quantity, Amount, VAT_Prod_Posting_Group, Technique_Revenue_Code, Graphic_No, Style_No, [Print], Combo, Part, Brand, Sample_No, Production_Start_Date, Revised_Count) VALUES ('$transactionType','$poString','$intSalesOrderId','$line_no', '$strSalesOrderNo', 'PRINT','PRINT','PRINT','PRINT','$totalQty','$poqty', '$invoiced_qty','$amount', 'VAT', '$technique', '$strGraphicNo','$strStyleNo','$strPrintName','$strCombo','$partName','$brand','$sampleNo','$psd','$revise_no')";
            if ($azure_connection) {
                $getResults = $objAzure->runQuery($azure_connection, $sql_azure_details);
                if($getResults != FALSE){
                    $successDetails = 1;
                }
               if($transactionType == 'SO_EditStart'){
                   $sql_azure_details_copy = "INSERT into SalesLine (Transaction_type, Order_No, Sales_Order_No, Line_No, Sales_Order_Name, Item_Code, Description, Item_Category, Item_SubCategory, Quantity, Original_Quantity, Pre_Invoiced_Quantity, Amount, VAT_Prod_Posting_Group, Technique_Revenue_Code, Graphic_No, Style_No, [Print], Combo, Part, Brand, Sample_No, Production_Start_Date, Revised_Count) VALUES ('SO_New','$poString','$intSalesOrderId','$line_no', '$strSalesOrderNo', 'PRINT','PRINT','PRINT','PRINT','$totalQty','$poqty', '$invoiced_qty','$amount', 'VAT', '$technique', '$strGraphicNo','$strStyleNo','$strPrintName','$strCombo','$partName','$brand','$sampleNo','$psd','$revise_no')";
                   $getResultsCopy = $objAzure->runQuery($azure_connection, $sql_azure_details_copy);
               }
            }
            $sql_details = "INSERT into trn_financemodule_salesline(Transaction_type, Order_No, Sales_Order_No, Line_No, Sales_Order_Name, Item_Code, Description, Item_Category, Item_SubCategory, Quantity, Original_Quantity, Amount, VAT_Prod_Posting_Group, Technique_Revenue_Code, Graphic_No, Style_No, Print, Combo, Part, Brand, Sample_No, Production_Start_Date, Revised_Count, deliveryStatus, deliveryDate) VALUES ('$transactionType','$poString','$intSalesOrderId','$line_no', '$strSalesOrderNo', 'PRINT','PRINT','PRINT','PRINT','$totalQty','$poqty','$amount', 'VAT', '$technique', '$strGraphicNo','$strStyleNo','$strPrintName','$strCombo','$partName','$brand','$sampleNo','$psd','$revise_no','$successDetails',NOW())";
            $result_details = $db->RunQuery($sql_details);
            if($transactionType == 'SO_EditStart'){
                $sql_details_copy = "INSERT into trn_financemodule_salesline(Transaction_type, Order_No, Sales_Order_No, Line_No, Sales_Order_Name, Item_Code, Description, Item_Category, Item_SubCategory, Quantity, Original_Quantity, Amount, VAT_Prod_Posting_Group, Technique_Revenue_Code, Graphic_No, Style_No, Print, Combo, Part, Brand, Sample_No, Production_Start_Date, Revised_Count, deliveryStatus, deliveryDate) VALUES ('SO_New','$poString','$intSalesOrderId','$line_no', '$strSalesOrderNo', 'PRINT','PRINT','PRINT','PRINT','$totalQty','$poqty','$amount', 'VAT', '$technique', '$strGraphicNo','$strStyleNo','$strPrintName','$strCombo','$partName','$brand','$sampleNo','$psd','$revise_no','$successDetails',NOW())";
                $result_details_copy = $db->RunQuery($sql_details_copy);

            }
        }
    }


    if($i > 0) {
        $sql_azure_header = "INSERT into SalesHeader (Transaction_type, Order_No, Document_Type, Customer_Code, Customer_Name, Posting_Date, Order_Date, Currency_Code, Revised_Date, Narration, Payment_Terms, Customer_PO_Number, Sales_Person, Location_Code, No_of_Lines, Revised_Count, Last_Dispatched_Date, Customer_Location) VALUES ('$transactionType','$poString','Order','$customerCode','$customer', '$postingDate', '$orderDate','$curr_code','$revisedDate','$strRemark','$payment_term','$strCustomerPoNo', '$marketer', '$location', '$i', '$revise_no', '$disp_date', '$cus_location')";
        if ($azure_connection) {
            $getResults = $objAzure->runQuery($azure_connection, $sql_azure_header);
            if ($getResults != FALSE) {
                $successHeader = '1';
            }
            if ($transactionType == 'SO_EditStart') {
                $sql_azure_header_copy = "INSERT into SalesHeader (Transaction_type, Order_No, Document_Type, Customer_Code, Customer_Name, Posting_Date, Order_Date, Currency_Code, Revised_Date, Narration, Payment_Terms, Customer_PO_Number, Sales_Person, Location_Code, No_of_Lines, Revised_Count, Last_Dispatched_Date, Customer_Location) VALUES ('SO_New','$poString','Order','$customerCode','$customer', '$postingDate', '$orderDate','$curr_code','$revisedDate','$strRemark','$payment_term','$strCustomerPoNo', '$marketer', '$location', '$i', '$revise_no', '$disp_date', '$cus_location')";
                $getResultsHeaderCopy = $objAzure->runQuery($azure_connection, $sql_azure_header_copy);
            }
        }

        $sql = "INSERT into trn_financemodule_salesheader (Transaction_type, Order_No, Document_Type, Customer_Code, Customer_Name, Posting_Date, Order_Date, Currency_Code, Revised_Date, Narration, Payment_Terms, Customer_PO_Number, Sales_Person, Location_Code, No_of_Lines, Revised_Count, Last_Dispatched_Date, Customer_Location, deliveryStatus, deliveryDate ) VALUES ('$transactionType','$poString','Order','$customerCode','$customer', NOW(), '$orderDate','$curr_code','$revisedDate','$strRemark_sql','$payment_term','$strCustomerPoNo', '$marketer', '$location', '$i', '$revise_no', '$disp_date', '$cus_location', '$successHeader', NOW())";
        $new_result_header = $db->RunQuery($sql);
        if ($transactionType == 'SO_EditStart') {
            $sql_copy = "INSERT into trn_financemodule_salesheader(Transaction_type, Order_No, Document_Type, Customer_Code, Customer_Name, Posting_Date, Order_Date, Currency_Code, Revised_Date, Narration, Payment_Terms, Customer_PO_Number, Sales_Person, Location_Code, No_of_Lines, Revised_Count, Last_Dispatched_Date, Customer_Location, deliveryStatus, deliveryDate ) VALUES ('$transactionType','$poString','Order','$customerCode','$customer', NOW(), '$orderDate','$curr_code','$revisedDate','$strRemark_sql','$payment_term','$strCustomerPoNo', '$marketer', '$location', '$i', '$revise_no', '$disp_date', '$cus_location', '$successHeader', NOW())";
            $new_result_header_copy = $db->RunQuery($sql_copy);
        }
    }

}

echo "JOB DONE";
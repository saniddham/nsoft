<?php

include 'libraries/excel/Classes/PHPExcel/IOFactory.php';
$inputFileName = './documents/Book1.xlsx';

try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
}

$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();
$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);



$data_final = array();
for ($row = 2; $row <= $highestRow; $row++) {
    $temp = array();
    $data = array();
   for($col = 0; $col < $highestColumnIndex; ++$col){
        $column = PHPExcel_Cell::stringFromColumnIndex($col);
       $columnName = $sheet->getCell($column . '1')->getValue();
       $columnName = trim($columnName);
        $rowData = $sheet->getCell($column . $row)->getValue();
        $rowData = trim($rowData);
        $temp[$columnName] = $rowData;
   }
   $data['value'] = $temp;
   array_push($data_final,$data);
}
$fp = fopen('./documents/book1.json', 'w');
var_dump($highestColumn);
fwrite($fp, json_encode($data_final));
fclose($fp);


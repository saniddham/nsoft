$(document).ready(function() {
	$('.cls_txt_DateCalExchangeRate').live('blur',GetCommonExchangeRate);
	$('.cls_cbo_CurrencyCalExchangeRate').live('change',GetCommonExchangeRate);

	$(".cls_input_number_validation").die("keypress keyup").live("keypress keyup",function (event) {
		if (event.which && (event.which < 46 || event.which > 57 || event.which == 47) && event.keyCode != 8) {
			event.preventDefault();
		}
		
		if (event.which == 46 && $(this).val().indexOf('.') != -1) {
			event.preventDefault();
		}
	});

});

function errormsg(status)
{
	 switch (status) {
	 case 404:
		 return('File not found');
	 case 500:
		 return('Server error');
	 case 0:
		 return('Request aborted');
	 default:
		 return('Unknown error  :' + status);
	} 	
}

function chk(value)
{
	return (value==1?true:false);
}
////////////////////////COMMON LOAD COMBO FUNCTIONS       ////////////////////////////////////////////////
function combo_brand(customerId)
{
		var url 		= mainPath+"libraries/php/loadCombo.php?type=brand&customerId="+customerId;
		var httpobj 	= $.ajax({url:url,async:false})
		return httpobj.responseText;	
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////// popup box alert //////////////////////////////////////////////
/***************************/
//@Author: Adrian "yEnS" Mato Gondelle
//@website: www.yensdesign.com
//@email: yensamg@gmail.com
//@license: Feel free to use it, but keep this credits please!					
/***************************/

//SETTING UP OUR POPUP
//0 means disabled; 1 means enabled;
var popupStatus = 0;

//loading popup with jQuery magic!
var cboFrom ='';
var cboTo	 = '';
var pageTo = '';
var x  = '';

function popupWindow2(i,from,to,page){
	
	//loads popup only if it is disabled
	$('#frmItem').validationEngine('hide');
	cboFrom = from;
	cboTo = to;
	pageTo = page;
	x = i;
	centerPopup();
		
	if(popupStatus==0){
		$("#backgroundPopup").css({
			"opacity": "0.7"
		});
		//$("#backgroundPopup").fadeIn("slow");
		//$("#popupContact"+x).fadeIn("slow");
		$("#backgroundPopup").fadeIn("slow");
		$("#popupContact"+x).fadeIn("slow");
		popupStatus = 1;
	}
	
		$("#iframeMain"+x).contents().find("#header").hide();
		$("#iframeMain"+x).contents().find("#butClose").parent().attr('href','');
		$("#iframeMain"+x).contents().find("#butClose").parent().click(disablePopup);

}

function popupWindow(i){
	//loads popup only if it is disabled
	$('#frmItem').validationEngine('hide');
	x = i;
	centerPopup();
		
	if(popupStatus==0){
		$("#backgroundPopup").css({
			"opacity": "0.7"
		});
		//$("#backgroundPopup").fadeIn("slow");
		//$("#popupContact"+x).fadeIn("slow");
		$("#backgroundPopup").fadeIn("slow");
		$("#popupContact"+x).fadeIn("slow");
		popupStatus = 1;
	}
		$("#iframeMain"+x).contents().find("#header").hide();
		$("#iframeMain"+x).contents().find("#butClose").parent().attr('href','');
		$("#iframeMain"+x).contents().find("#butClose").click(disablePopup2);
}

function popupWindow3(i){
	//loads popup only if it is disabled
	
	x = i;
	centerPopup();
		
	if(popupStatus==0){
		$("#backgroundPopup").css({
			"opacity": "0.7"
		});
		//$("#backgroundPopup").fadeIn("slow");
		//$("#popupContact"+x).fadeIn("slow");
		$("#backgroundPopup").fadeIn("slow");
		$("#popupContact"+x).fadeIn("slow");
		popupStatus = 1;
	}
		//$("#iframeMain"+x).contents().find("#header").hide();
		//$("#iframeMain"+x).contents().find("#butClose").parent().attr('href','');
		//$("#iframeMain"+x).contents().find("#butClose").parent().click(disablePopup2);

}

//disabling popup with jQuery magic!
function disablePopup(){
	//disables popup only if it is enabled
	if(popupStatus==1){
		//$("#backgroundPopup").fadeOut("slow");
		//$("#popupContact"+x).fadeOut("slow");
		$("#backgroundPopup").hide();
		$("#popupContact"+x).hide() ;
		popupStatus = 0;
		
		//var x = $("#iframeMain").contents().find("#butSearch").attr('val');
		//alert(x);
		var m = $("#iframeMain"+x).contents().find(cboFrom).html();
		//alert(m);
		$(pageTo+" "+cboTo).html(m);
		//$('#frmItem').validationEngine('hide');
	}
}
function disablePopup2(){
	//disables popup only if it is enabled
	if(popupStatus==1){
		//$("#backgroundPopup").fadeOut("slow");
		//$("#popupContact"+x).fadeOut("slow");
		$("#backgroundPopup").hide();
		$("#popupContact"+x).hide();
		popupStatus = 0;
		
		//var x = $("#iframeMain").contents().find("#butSearch").attr('val');
		//alert(x);
		//var m = $("#iframeMain"+x).contents().find("#"+cboFrom).html();
		//$("#"+pageTo+" #"+cboTo).html(m);
		$('#frmItem').validationEngine('hide');
	}
	closePopUp();
}

function showWaiting()
{
		var popupbox = document.createElement("div");
		var windowWidth = document.documentElement.clientWidth;
		var windowHeight = document.documentElement.clientHeight;
		var scrollH = (document.body.scrollHeight);
		//alert(windowHeight);
   popupbox.id = "divBackGroundBalck";
   popupbox.style.position = 'absolute';
   popupbox.style.zIndex = 100;
   popupbox.style.textAlign = 'center';
   popupbox.style.left = 0 + 'px';
   popupbox.style.top = 0 + 'px'; 
   popupbox.style.background="#000000"; 
   popupbox.style.width = screen.width + 'px';
   popupbox.style.height =  (scrollH)+ 'px';
   popupbox.style.opacity = 0.2;
   popupbox.style.color = "#FFFFFF";
	document.body.appendChild(popupbox);
	//document.getElementById('divBackGroundBalck').innerHTML = "this is text code";
	var popupbox1 = document.createElement("div");
	 popupbox1.id = "divBackgroundImg";
   popupbox1.style.position = 'absolute';
   popupbox1.style.zIndex = 101;
   popupbox1.style.verticalAlign = 'center';
   popupbox1.style.left =  windowWidth/2-100/2 +'px';
   popupbox1.style.top = ($(window).scrollTop()+200) + 'px'; 
   popupbox1.style.width = '100px';
   popupbox1.style.height =  '100px';
   popupbox1.style.opacity = 1;
   popupbox1.style.color = "#FFFFFF";
	document.body.appendChild(popupbox1);
	
	//alert(mainPath);
	document.getElementById('divBackgroundImg').innerHTML = "<img src=\"images/loading_go.gif\" /><span class=\"normalfnt\" style=\"color:white;\">Please Wait...</span>";
	
}

function hideWaiting()
{
	try
	{
		var box = document.getElementById('divBackGroundBalck');
		box.parentNode.removeChild(box);
		
		var box1 = document.getElementById('divBackgroundImg');
		box1.parentNode.removeChild(box1);
		
	}
	catch(err)
	{        
	}	
}
//centering popup
function centerPopup(){
	//request data for centering
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	//alert(windowHeight);
	
	var popupHeight = $("#popupContact"+x).height();
	var popupWidth = $("#popupContact"+x).width();
	//alert(window.outerHeight);
	//centering
	$("#popupContact"+x).css({
		"position": "absolute",
		//"top": windowHeight/2-popupHeight/2,
		"left": windowWidth/2-popupWidth/2,
		"top":$(window).scrollTop()+30
		
		
	});
	
	//$("#popupContact").html("<iframe  id=\"iframeMain\" name=\"iframeMain\""+
				//"src=\""+url+"\" style=\"width:800;height:400;border:0\">"+
    			//"</iframe>");
	//only need force for IE6
	
	$("#backgroundPopup").css({
		"height": window.outerHeight//"height": windowHeight
	});
	
}

function removeLastChar(value)
{
	return value.substr(0,value.length-1);
}


//CONTROLLING EVENTS IN jQuery
$(document).ready(function(){
	
	//LOADING POPUP
	//Click the button event!

	//CLOSING POPUP
	//Click the x event!
	$("#popupContactClose").click(function(){
		disablePopup();
		
	});
	//Click out event!
	$("#backgroundPopup").click(function(){
		disablePopup();
	});
	//Press Escape event!
	$(document).keypress(function(e){
		if(e.keyCode==27 && popupStatus==1){
			disablePopup();
			
		}
	});
	
	

});

function inc(filename)
{				
	var body = document.getElementsByTagName('body').item(0);				
	script = document.createElement('script');				
	script.src = filename;
	script.type = 'text/javascript';				
	body.appendChild(script);
}	

function URLEncode(url)//URLEncode
{
	//url = url.replace(/^[a-zA-Z0-9-\<\>\@\!\#\$\%\^\&\*\(\)\_\+\[\]\{\}\?\:\;\|\'\\\"\\\\\,\.\/\~\`\-\= ]*$/,'');
	
	url = url.replace(/\\/gi,"\\\\");
	url = url.replace(/"/gi,'\\"');
	url = url.replace(/'/gi,"\\'");
	

	//alert(url);
	//strBuyerPo = strBuyerPo.replace(/#/gi,"***");
	
	url = "" + url + "";
	url = url.replace("&amp;", "&");
	// The Javascript escape and unescape functions do not correspond
	// with what browsers actually do...
	var SAFECHARS = "0123456789" +					// Numeric
					"ABCDEFGHIJKLMNOPQRSTUVWXYZ" +	// Alphabetic
					"abcdefghijklmnopqrstuvwxyz" +
					"-_.!~*'()";					// RFC2396 Mark characters
	var HEX = "0123456789ABCDEF";

	var plaintext = url;
	var encoded = "";
	for (var i = 0; i < plaintext.length; i++ ) {
		var ch = plaintext.charAt(i);
	    if (ch == " ") {
		    encoded += "+";				// x-www-urlencoded, rather than %20
		} else if (SAFECHARS.indexOf(ch) != -1) {
		    encoded += ch;
		} else {
		    var charCode = ch.charCodeAt(0);
			if (charCode > 255) {
				/*
			    alert( "Unicode Character '" 
                        + ch 
                        + "' cannot be encoded using standard URL encoding.\n" +
				          "(URL encoding only supports 8-bit characters.)\n" +
						  "A space (+) will be substituted." );
				encoded += "+";
				*/
			} else {
				encoded += "%";
				encoded += HEX.charAt((charCode >> 4) & 0xF);
				encoded += HEX.charAt(charCode & 0xF);
			}
		}
	} // for

	//document.URLForm.F2.value = encoded;
  //document.URLForm.F2.select();
	return encoded;
}

function URLEncode_json(url)
{
	
	url = JSON.stringify(url);
	url = url.replace(/#/gi,"%23");
	url = url.replace(/&/gi,"%26");
	return url;
}

 function DisableContextMenu()
 {
   return false;
 }
 
 function EnableRightClickEvent()
 {
	 document.oncontextmenu = null;
	  return false;
 }
 
 function DisableRightClickEvent()
 {
	  document.oncontextmenu = DisableContextMenu;
	  return false;
 }
  function ControlableKeyAccess(evt)
  {
	 var charCode = (evt.which) ? evt.which : evt.keyCode;


	 if (charCode == 9)
		return true;

	 return false;

  }
  
function RoundNumber(num,dec)
{
	num = parseFloat(num).toFixed(parseFloat(dec)+4);
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return parseFloat(result.toFixed(dec));
}

function GetCommonExchangeRate()
{
	var url 		= mainPath+"libraries/php/common.php?RequestType=URLGetExchangeRate";
		url 	   += "&Date="+$('.cls_txt_DateCalExchangeRate').val();
		url 	   += "&CurrId="+$('.cls_cbo_CurrencyCalExchangeRate').val();
		
	var httpobj 	= $.ajax({url:url,async:false})
	$('.cls_txt_exchangeRate').val(httpobj.responseText);
}

function IsProcessMonthLocked_date(date)
{
	var url  = mainPath+"libraries/php/common.php?RequestType=URLIsProcessMonthLocked_Date";
		url += "&Date="+date;
	var urlobj 	= $.ajax({url:url,dataType:'json',async:false,
		success:function(json){
			status = json.Status;
		}
	});	
	
	if(status=='1'){
		OpenMonthEndLoac_PopUp();
		return false;
	}	
	else
		return true;	
}

function OpenMonthEndLoac_PopUp()
{
	$('body').append("<div style=\"width:800px; position: absolute;display:none;z-index:100\" id=\"popupContact1\"></div>");
	$('body').append("<div style=\"height: 0px; opacity: 0.7; display: none;\" id=\"backgroundPopup\"></div>");
	popupWindow3('1');
	$('#popupContact1').html("");	
	$('#popupContact1').load(mainPath+'dataAccess/popup_monthend_lock.php?',function(){
	$('#popupContact1 .butClose1').click(disablePopup);
	});
}

function validateDecimalPlaces(value,decimalPoints,evt)
{	
	decimalPoints = parseInt(decimalPoints);
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	
	var positionCount = parseInt(value.indexOf('.'));
	if((positionCount>0) && (charCode=='46'))
		return false;
	
	if (evt.shiftKey && charCode==37 ){
		return false;
	}
	if (evt.shiftKey && charCode==39 ){
		return false;
	}
	
	var AllowableCharators	= new Array("46","8","9","37","39");
			
	var allowableCharacters = new Array(46,9,45,36,35);
	
	for (var loop = 0 ; loop < allowableCharacters.length ; loop ++ )
	{
		if (charCode == allowableCharacters[loop] )
		{
			return true;
		}
	}	
	
	for (x in AllowableCharators)
	  {
		  if (AllowableCharators[x] == charCode)
		  return true;		
	  }
	
	if (charCode==46 && value.indexOf(".") >-1)
		return false;
	else if (charCode==46)
		return true;
	
	if (value.indexOf(".") > -1 && value.substring(value.indexOf("."),value.length).length > decimalPoints)
		return false;
	
	 if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;

	 return true;
}
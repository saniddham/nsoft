<?php
session_start();
//BEGIN - INCLUDE FILES {
include_once ($_SESSION['ROOT_PATH']."dataAccess/Connector.php");
include_once ($_SESSION['ROOT_PATH']."class/masterData/exchange_rate/cls_exchange_rate_get.php");
//END	- INCLUDE FILES }

//BEGIN - ASSIGN PARAMETERS {
$session_companyId	= $_SESSION["headCompanyId"];
$requestType		= $_REQUEST["RequestType"];
//END 	- ASSIGN PARAMETERS }

//BEGIN - CREATE OBJECT {
$obj_exchange_rate_get = new cls_exchange_rate_get($db);
//END   - CREATE OBJECT }

if($requestType=="URLGetExchangeRate")
{
	$result = $obj_exchange_rate_get->GetAllValues($_REQUEST["CurrId"],2,$_REQUEST["Date"],$session_companyId,'RunQuery');
	echo $result["AVERAGE_RATE"];
}

if($requestType=="URLIsProcessMonthLocked_Date")
{
	$date		= $_REQUEST["Date"];
	$status 	= 0;
	
	$sql 		= "SELECT
					  PROCESS_STATUS
					FROM finance_month_end_process
					WHERE YEAR = YEAR('$date')
					AND MONTH = MONTH('$date')
					AND PROCESS_STATUS = 1
					AND COMPANY_ID = $session_companyId";
	$result 	= $db->RunQuery($sql);
	while($row	= mysqli_fetch_array($result))
	{
		$status	= $row["PROCESS_STATUS"];
	}

	$response['Status'] 	= $status;
	echo json_encode($response);
}
?>
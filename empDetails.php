<?php
	include_once 'dataAccess/LoginDBManager.php';
	$db 			= new LoginDBManager();
	$HRDB 			= $db->getHRDatabase();
	
	$thisFilePath =  $_SERVER['PHP_SELF'];
	include_once "libraries/mail/mail_bcc.php";	
	ob_start();	
?>
<title>Employee Details</title>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}


 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}


.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}



.odd{background-color:#F5F5F5;}
.even{background-color:#FFFFFF;}
.mouseover {
	cursor: pointer;
}


.txtNumber{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}
</style>
<link href="css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="libraries/javascript/script.js"></script>

<link rel="stylesheet" href="libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="libraries/validate/template.css" type="text/css">


<body>
<form id="frmEmplyeeDetails" name="frmEmplyeeDetails" autocomplete="off" action="" method="post">
<script src="libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
<div class="trans_layoutXL">
<div class="trans_text">Employee Details</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
            	<td colspan="2">
                <table width="100%" class="bordered" >
                    <thead>
                     <tr>
                       <th width="107" >No</th>
                       <th width="107" height="19" >EPF No</th>
                       <th width="204" height="19" >Company</th>
                       <th width="144" height="19" >Location</th>
                       <th width="202" height="19" >Designation</th>
                       <th width="474" height="19" >Emp Name</th>
                       <th width="77" height="19" >Picture</th>
                     </tr>
                    </thead>
                    <tbody>
                    <?php
					$sql = "SELECT ME.intEmployeeId,ME.strEmployeeCode,MC.strName AS company,
							ML.strName AS location,MG.strDesignation,ME.strEmployeeName,ME.Photo
							FROM $HRDB.mst_employee ME
							INNER JOIN $HRDB.mst_companies MC ON MC.intId=ME.intCompanyId
							INNER JOIN $HRDB.mst_locations ML ON ML.intId=ME.intLocationId
							INNER JOIN $HRDB.mst_designation MG ON MG.intDesignation=ME.intDesignationId
							WHERE ME.Active=1
							ORDER BY MC.strName,ML.strName ,ME.strEmployeeName";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$empId = $row['intEmployeeId'];
						if(! (file_exists("../qpay/uploadDocument/registration/photo/$empId.jpg") || file_exists("../qpay/uploadDocument/registration/photo/$empId.JPG")) )
						 {
							 $i++;
					?>
                     <tr bgcolor="#FFFFFF" class="normalfnt" id="<?php echo $row['intEmployeeId'];?>">
                       <td align="center" ><?php echo $row['intEmployeeId']; ?></td>
                       <td align="center" ><?php echo $row['strEmployeeCode']; ?></td>
                       <td align="left" ><?php echo $row['company']; ?></td>
                       <td align="left"  ><?php echo $row['location']; ?></td>
                       <td align="left" ><?php echo $row['strDesignation']; ?></td>
                       <td align="left" ><?php echo $row['strEmployeeName']; ?></td>
                       <td style="text-align:center"><?php echo '<img src="../qpay/'.$row['Photo'].'" width="60" height="60" />' ?></td>
                     </tr>
                    <?php
						 }
					}
					echo "Record count:$i";
					?>
                    </tbody>
                    </table>
                    
                </td>
           	  </tr>
            </table>
        </td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    </tr>
    
</table>
</div>
</div>
</form>
</body>
<?php
	echo $body = ob_get_clean();
	$mailHeader = "Pending - Photo upload list";
	sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.','roshan@screenlineholdings.com',$mailHeader,$body,'','roshan@screenlineholdings.com');
?>
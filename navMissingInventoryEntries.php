<?php
session_start();
ini_set('max_execution_time',100000);
include_once 'dataAccess/DBManager.php';

require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";

$objAzure = new cls_azureDBconnection();
$azure_connection = $objAzure->connectAzureDB();
$db = new DBManager();
$db->SetConnectionString($_SESSION["Server"], $_SESSION["UserName"], $_SESSION["Password"], $_SESSION["Database"], $_SESSION['userId']);
$db->OpenConnection();

$sql_select = "SELECT * FROM trn_financemodule_inventory WHERE deliveryStatus=0";

$result = $db->RunQuery2($sql_select);
$table = "Inventory_Test";
$i = 0;
while ($row = mysqli_fetch_array($result)) {
    $documentCategory = $row['Document_Category'];
    $documentNo = $row['Document_No'];
    $lineNo = $row['Line_No'];
    $externalDocNo = $row['External_Doc_No'];
    $postingDate = $row['Posting_Date'];
    $itemCategory = $row['Item_Category'];
    $itemSubCategory = $row['Item_Sub_Category'];
    $itemCode = $row['Item_Code'];
    $orderNo = $row['Order_No'];

    $graphicNo = $row['Graphic_No'];
    $salesOrderNo = $row['Sales_Order_No'];
    $description = $row['Description'];
    $amount = $row['Amount'];
    $location = $row['Location'];
    $depCode = $row['Department_Code'];
    $cusNo = $row['Customer_No'];
    $vendorNo = $row['Vendor_No'];
    $i++;


        $sql_azure_header = "INSERT INTO $table (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location, Department_Code, Customer_No, Vendor_No) VALUES ('$documentCategory', '$documentNo', '$lineNo', '$externalDocNo', '$postingDate', '$itemCategory', '$itemSubCategory', '$itemCode','$description', '$amount','$location','$depCode','$cusNo','$vendorNo')";
        if ($azure_connection) {
            $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
            if ($getResultsHeader != FALSE) {
                $successHeader = '4';
            } else {
                echo $getResultsHeader;
            }
        }
        $sql_db_header = "UPDATE trn_financemodule_inventory SET deliveryStatus='$successHeader' WHERE Document_Category='$documentCategory' AND Document_No='$documentNo' AND Line_No='$lineNo' AND Amount='$amount' AND deliveryStatus='0'";
        $result_db_header = $db->RunQuery2($sql_db_header);
}

echo "JOB DONE NUMBER ".$i;

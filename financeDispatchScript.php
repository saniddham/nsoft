<?php
session_start();
ini_set('max_execution_time',100000);
include_once 'dataAccess/DBManager.php';

require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";

$objAzure = new cls_azureDBconnection();
$azure_connection = $objAzure->connectAzureDB();

$db = new DBManager();
$db->SetConnectionString($_SESSION["Server"], $_SESSION["UserName"], $_SESSION["Password"], $_SESSION["Database"], $_SESSION['userId']);
$db->OpenConnection();


$sql_header = "SELECT
                  TH.intReviseNo,
                  TH.intOrderNo,
                  TH.intOrderYear,
                  TH.intStatus,
                  CONCAT(ware_fabricdispatchheader.intBulkDispatchNo,'/',ware_fabricdispatchheader.intBulkDispatchNoYear) AS shipmentNO,
                  ware_fabricdispatchheader.strRemarks,
                  MAX(ware_fabricdispatchheader_approvedby.dtApprovedDate) AS posting_date,
                    TH.intApproveLevelStart,
                    mst_customer_locations_header.strName AS customer_location,
                    TH.strCustomerPoNo,
                    ware_fabricdispatchheader.intCompanyId AS location,
                    TH.intCustomer,
                    TH.intPaymentTerm,
                    TH.dtmCreateDate AS order_date,
                    mst_customer.strName AS customer,
                    mst_customer.strCode AS customerCode,
                    TH.strCustomerPoNo,
                    mst_financecurrency.strCode AS curr_code,
                    TH.intMarketer AS marketer,
                    (
                        SELECT
                            CAST(
                                MAX(
                                    ware_stocktransactions_fabric.dtDate
                                ) AS DATE
                            )
                        FROM
                            ware_stocktransactions_fabric
                        WHERE
                            ware_stocktransactions_fabric.intOrderNo = TH.intOrderNo
                        AND ware_stocktransactions_fabric.intOrderYear = TH.intOrderYear
                        AND ware_stocktransactions_fabric.strType LIKE '%Dispatched%'
                        GROUP BY
                            ware_stocktransactions_fabric.intOrderNo,
                            ware_stocktransactions_fabric.intOrderYear
                    ) AS lastDispatchDate
                FROM
                    ware_fabricdispatchheader
                INNER JOIN trn_orderheader TH ON TH.intOrderNo = ware_fabricdispatchheader.intOrderNo
                INNER JOIN mst_locations ON TH.intLocationId = mst_locations.intId
                AND TH.intOrderYear = ware_fabricdispatchheader.intOrderYear
                INNER JOIN ware_fabricdispatchheader_approvedby ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchheader_approvedby.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchheader_approvedby.intYear
                INNER JOIN mst_customer ON mst_customer.intId = TH.intCustomer
                INNER JOIN mst_financecurrency ON mst_financecurrency.intId = TH.intCurrency
                LEFT JOIN mst_customer_locations_header ON ware_fabricdispatchheader.intCustLocation = mst_customer_locations_header.intId
                WHERE
                    ware_fabricdispatchheader.intStatus = 1
                AND TH.intOrderYear = 2019
                AND TH.intOrderNo = '182004368'
                AND TH.intStatus NOT IN (- 10 ,- 2)
                AND mst_locations.intCompanyId = 1
                GROUP BY
                    TH.intOrderNo,
                    TH.intOrderYear";

$result_header = $db->RunQuery($sql_header);
while ($row_header = mysqli_fetch_array($result_header)) {
    $orderNo = $row_header['intOrderNo'];
    $orderYear = $row_header['intOrderYear'];
    $poString = $orderNo.'/'.$orderYear;
    $customer = trim($row_header['customer']);
    $cus_location = trim($row_header['customer_location']);
    $customerCode = $row_header['customerCode'];
    $strCustomerPoNo = trim($row_header['strCustomerPoNo']);
    $strRemark = trim($row_header['strRemarks']);
    $strRemark_sql = $strRemark;
    $orderDate = $row_header['order_date'];
    $curr_code = ($row_header['curr_code'] == 'EURO')?"Eur":($row_header['curr_code'] == 'LKR'?"":$row_header['curr_code']);
    $payment_term = $row_header['intPaymentTerm'];
    $marketer = $row_header['marketer'];
    $location = $row_header['location'];
    $revise_no = $row_header['intReviseNo'];
    $lastDispatchDate = $row_header['lastDispatchDate'];
    $shipmentNo = $row_header['shipmentNO'];
    $postingDate = $row_header['posting_date'];
    $successHeader = 0;
    $i = 0;


    $sql_select = "SELECT
                    OD.strStyleNo,
                    OD.strGraphicNo,
                    OD.strSalesOrderNo,
                    OD.intSalesOrderId,
                    OD.strPrintName,
                    OD.dtPSD AS PSD,
                    OD.strCombo,
                    OD.TECHNIQUE_GROUP_ID AS technique,
                    CONCAT(
                        OD.intSampleNo,
                        '/',
                        OD.intSampleYear
                    ) AS sampleString,
                    OD.dblPrice AS price,
                    ware_fabricdispatchdetails.strCutNo,
                    mst_part.strName AS partName,
                    ware_fabricdispatchdetails.strLineNo,
                    SUM(
                        ware_fabricdispatchdetails.dblGoodQty
                    ) AS dblGoodQty,
                    (
                        SELECT
                            sum(
                                finance_customer_invoice_details.QTY
                            )
                        FROM
                            finance_customer_invoice_details
                        INNER JOIN finance_customer_invoice_header ON finance_customer_invoice_header.SERIAL_NO = finance_customer_invoice_details.SERIAL_NO
                        AND finance_customer_invoice_header.SERIAL_NO = finance_customer_invoice_details.SERIAL_NO
                        WHERE
                            finance_customer_invoice_header.ORDER_NO = OD.intOrderNo
                        AND finance_customer_invoice_header.ORDER_YEAR = OD.intOrderYear
                        AND finance_customer_invoice_details.SALES_ORDER_ID = OD.intSalesOrderId
                    ) AS invoiced_qty
                FROM
                    trn_orderdetails OD
                INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = OD.intOrderNo
                AND ware_fabricdispatchheader.intOrderYear = OD.intOrderYear
                INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo
                AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear
                AND ware_fabricdispatchdetails.intSalesOrderId = OD.intSalesOrderId
                LEFT JOIN mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
                WHERE
                     ware_fabricdispatchheader.intStatus = 1 AND
                    ware_fabricdispatchheader.intOrderNo = '$orderNo'
                AND ware_fabricdispatchheader.intOrderYear = '$orderYear'
                GROUP BY
                    OD.intOrderNo,
                    OD.intOrderYear,
                    OD.intSalesOrderId";


    $result1 = $db->RunQuery($sql_select);
    while ($row = mysqli_fetch_array($result1)) {
        $intSalesOrderId = $row['intSalesOrderId'];
        $strSalesOrderNo = trim($row['strSalesOrderNo']);
        $line_no = intval($row['strLineNo']);
        $strGraphicNo = trim($row['strGraphicNo']);
        $strCombo = trim($row['strCombo']);
        $sampleNo = trim($row['sampleString']);
        $strStyleNo = trim($row['strStyleNo']);
        $strPrintName = trim($row['strPrintName']);
        $poqty = $row['dblGoodQty'];
        $invoiced_qty = $row['invoiced_qty'];
        if($invoiced_qty == ''){
            $invoiced_qty = 0;
        }
        $qty = $poqty - $invoiced_qty;
        if($qty < 0){
            $qty = 0;
        }
        $partName = trim($row['partName']);
        $brand = trim($row['brand']);
        $amount = round( $row['price'], 5);
        $psd = $row['PSD'];
        $technique = ($row['technique'] == '6')?"Heat Seal":"Print";
        $successDetails = 0;
        if($qty > 0) {
            $i++;
            $sql_azure_details = "INSERT into SalesLine (Transaction_type, Order_No, Sales_Order_No, Line_No, Shipment_No, Sales_Order_Name, Item_Code, Description, Item_Category, Item_SubCategory, Quantity, Original_Quantity, Pre_Invoiced_Quantity, Amount, VAT_Prod_Posting_Group, Technique_Revenue_Code, Graphic_No, Style_No, [Print], Combo, Part, Brand, Sample_No, Production_Start_Date, Revised_Count) VALUES ('SO_Shipment','$poString','$intSalesOrderId','$line_no', '$shipmentNo', '$strSalesOrderNo', 'PRINT','PRINT','PRINT','PRINT','$qty','$qty','$invoiced_qty','$amount', 'VAT', '$technique', '$strGraphicNo','$strStyleNo','$strPrintName','$strCombo','$partName','$brand','$sampleNo','$psd', '$revise_no')";
            if ($azure_connection) {
                $getResults = $objAzure->runQuery($azure_connection, $sql_azure_details);
                if($getResults != FALSE){
                    $successDetails = '1';
                }
            }
            $sql_details = "INSERT into trn_financemodule_salesline (Transaction_type, Order_No, Sales_Order_No, Line_No, Shipment_No, Sales_Order_Name, Item_Code, Description, Item_Category, Item_SubCategory, Quantity, Original_Quantity,  Amount, VAT_Prod_Posting_Group, Technique_Revenue_Code, Graphic_No, Style_No, Print, Combo, Part, Brand, Sample_No, Production_Start_Date, Revised_Count, deliveryStatus, deliveryDate) VALUES ('SO_Shipment','$poString','$intSalesOrderId','$line_no', '$shipmentNo', '$strSalesOrderNo', 'PRINT','PRINT','PRINT','PRINT','$qty', '$qty','$amount', 'VAT', '$technique', '$strGraphicNo','$strStyleNo','$strPrintName','$strCombo','$partName','$brand','$sampleNo','$psd', '$revise_no', '$successDetails',NOW())";
            $result_details = $db->RunQuery($sql_details);
        }
    }


    if($i>0) {
        $sql_azure_header = "INSERT into SalesHeader (Transaction_type, Order_No, Document_Type, Shipment_No, Customer_Code, Customer_Name, Posting_Date, Order_Date, Currency_Code, Narration, Payment_Terms, Customer_PO_Number, Sales_Person, Location_Code, No_of_Lines, Last_Dispatched_Date, Revised_Count, Customer_Location) VALUES ('SO_Shipment','$poString','Order','$shipmentNo','$customerCode','$customer', '$postingDate', '$orderDate','$curr_code','$strRemark','$payment_term','$strCustomerPoNo', '$marketer', '$location', '$i', '$lastDispatchDate', '$revise_no', '$cus_location')";
        if ($azure_connection) {
            $getResults = $objAzure->runQuery($azure_connection, $sql_azure_header);
        }
        $sql = "INSERT into trn_financemodule_salesheader (Transaction_type, Order_No, Document_Type, Shipment_No, Customer_Code, Customer_Name, Posting_Date, Order_Date, Currency_Code, Narration, Payment_Terms, Customer_PO_Number, Sales_Person, Location_Code, No_of_Lines, Last_Dispatched_Date, Revised_Count, Customer_Location,  deliveryStatus, deliveryDate ) VALUES ('SO_Shipment','$poString','Order', '$shipmentNo', '$customerCode','$customer', NOW(), '$orderDate','$curr_code','$strRemark_sql','$payment_term','$strCustomerPoNo', '$marketer', '$location', '$i', '$lastDispatchDate', '$revise_no', '$cus_location', '$successHeader', NOW())";
        $result_header_new = $db->RunQuery($sql);
    }

}

echo "JOB DONE";
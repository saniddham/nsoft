<?php if(define('UNLOCKPAGE',true)) {
			header('Location:./');
	  };
?>
	
    <script type="text/javascript" src="abc_files/jsnow.js"></script>

    <!--<style type="text/css">

        #body {background:#000; background:url(9small.png) top left no-repeat #000000;color:#ccc; height:800px; padding:150px; font-family:Verdana, Arial, 'Helvetica', sans-serif}
        h1 {color:#fff;}

    </style>
    <script type="text/javascript">
        $(function() {
            $().jSnow();
        });
    </script>-->

 <?php
try{
	######################
	## load headers	    ##
	include_once "dataAccess/DBManager2.php"; 				$db_login 			= new DBManager2(1);
	include_once "class/tables/sys_users.php";				$sys_users			= new sys_users($db_login);
	include_once "class/sessions.php";						$sessions			= new sessions();
	include_once "class/tables/mst_locations_user.php";		$mst_locations_user	= new mst_locations_user($db_login);
	include_once "class/tables/mst_locations.php";			$mst_locations		= new mst_locations($db_login);
	include_once "class/tables/menus.php";					$menus				= new menus($db_login);
	include_once "class/tables/sys_config.php";				$sys_config			= new sys_config($db_login);
	include_once "class/dateTime.php";						$dateTimes 			= new dateTimes($db_login);
	include_once "class/tables/sys_users_disabled.php";		$sys_users_disabled = new sys_users_disabled($db_login);
	include_once "config.php";
	include_once "class/UserTracking.php";                        $userAcess = new UserAcess($db_login);
	include_once "class/tables/sys_useraccess.php";         $userAcessLog = new sys_useraccess($db_login);

	//$userAcessCheck->insertRec($UserAccessData['userID'],$UserAccessData['login'],$UserAccessData['logout'],$UserAccessData['logout'],$UserAccessData['IPLong'],$UserAccessData['browser'],$UserAccessData['browserVersion'],$UserAccessData['os']);

	##########################
	#### create cash object ##
	##########################

	$cache = phpFastCache();

	######################
	######################
	## opend connection ##

	$db_login->connect();
	$db_login->begin();

	######################

	######################
	## if submit        ##r
	if(isset($_POST['username'])){

		$userName 	= $_POST['username'];
		$password	= md5(md5($_POST['password']));
		$message	= '';

		###################
		### set root ######
		$sys_users->set_byName('root');

		$root_pass = $sys_users->getstrPassword();
		###################

		$userArrResult 			= $sys_users->set_byName($userName);
		$user_password	 		= $sys_users->getstrPassword();
		$error_atmpt_left		= $sys_users->getPASSWORD_ERROR_ATTEMPTS_LEFT();


		
		$sys_config->set(1);
			
		$pswExpireDays 			= $sys_config->getPASSWORD_EXPIRE_DAYS();
		$pswErrorAttempts 		= $sys_config->getPASSWORD_ERROR_ATTEMPTS();

		
		if((strtoupper($sys_users->getstrUserName())==strtoupper($userName) && $sys_users->getintStatus()==1 && ($sys_users->getstrPassword()==$password)||($password==$root_pass)) && count($userArrResult)>0 && $sys_users->getintStatus()==1)
		{
			$userId 			= $sys_users->getintUserId();
			
			$start 				= strtotime($sys_users->getPASSWORD_LAST_MODIFIED_DATE());
            $end 				= strtotime($dateTimes->getCurruntDate());
            $days_between 		= ceil(abs($end - $start) / 86400);
			$userIdCP			= base64_encode($userId);
			
			if(($days_between>=$pswExpireDays) || $sys_users->getPASSWORD_LAST_MODIFIED_DATE()=='')
			{
				header("Location:presentation/administration/changePassword/change_expire_password.php?userId=".$userIdCP."&error_attempts=".base64_encode($pswErrorAttempts)."");
				return;
			}
			
			$sessions->setAuth(true);
			$result = $mst_locations_user->select('*',null,"intUserId=$userId");
			$row = mysqli_fetch_array($result);
			$locationId = $row['intLocationId'];
			$mst_locations->set($locationId);
			
			$_SESSION["userId"] 		= $userId;
			$_SESSION["systemUserName"]	= $sys_users->getstrUserName();
			$_SESSION["CompanyID"] 		= $locationId;
			$_SESSION["email"] 			= $sys_users->getstrEmail();
			$_SESSION["pub_excessGrn"] 	= true;
			$_SESSION["headCompanyId"] 	= $mst_locations->getintCompanyId();
			$_SESSION['ROOT_PATH']		= MAIN_ROOT;		
			$PATH = dirname ($_SERVER['PHP_SELF']);
//User IP Address
			$Ip = $_SERVER['REMOTE_ADDR'];
			//add this for delete user wise menu cache
			//{
			$menu_result				= $menus->getMainMenuIdList();
			while($rowMI = mysqli_fetch_array($menu_result))
			{
				$menuId	 = $rowMI['intId'];
				$cache->delete("MENU<<{$userId}<<{$menuId}");
			}
			$cache->delete("HEADER<<{$locationId}<<{$userId}");
			//}
			
			$sys_users->set($userId);
			
			$sys_users->setPASSWORD_ERROR_ATTEMPTS_LEFT($pswErrorAttempts);
			
			$sys_users->commit();

            ######################################
            ##### save to access log #############
            $USER_LOGIN=true;
			if($USER_LOGIN){
			$UserAccessData = $userAcess->method1();
			$userAcessLog->insertRec($UserAccessData['userID'],$UserAccessData['IP'], $UserAccessData['IPLong'], $UserAccessData['browser'],$UserAccessData['browserVersion'],$UserAccessData['os']);
			}

            ######################################
			$db_login->commit();
			header("Location:".$_SERVER["REQUEST_URI"]); 
			//exit;	
		}
		else
		{
			if((count($userArrResult)<=0 || $sys_users->getintStatus()==0) && $message=='')
			{
				$message = "Invalid UserName or Password.";
			}
			else if($sys_users->getintStatus()==10 && $message=='')
			{
				$message = "Sorry! Your account has been disabled.";
			}
			else
			{
				if($error_atmpt_left=='')
				{
					$error_atmpt_left = $pswErrorAttempts-1;
				}
				else
				{
					$error_atmpt_left = $error_atmpt_left-1;
				}
				
				$userId		= $sys_users->getintUserId();
				
				$sys_users->set($userId);
				$sys_users->setPASSWORD_ERROR_ATTEMPTS_LEFT($error_atmpt_left);
				
				if($error_atmpt_left<=0)
				{
					$sys_users->setintStatus(10);
					$sys_users_disabled->insertRec($userId,$dateTimes->getCurruntDateTime());
				}
				
				$sys_users->commit();
				
				if($message=='')
				{
					if($error_atmpt_left>0)
						$message 	= "Invalid UserName or Password. You are left with $error_atmpt_left more attempts";
					else
						$message 	= "Sorry! Your account has been disabled.";
				}
				
			}
			
			$db_login->commit();
			require_once "login_template.php";
			return;		
		}	
	}
	else
	{
	    	##call login template
			require_once "login_template.php";
	}
	######################
$db_login->disconnect();
}catch(Exception $e){
	$db_login->disconnect();
	require_once "login_template.php";
	$message = "Invalid UserName or Password.";
}
?>